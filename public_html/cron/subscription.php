<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$mailer=new mailer();
//maile do klientów
$up=array();
$date=date("Y-m-d 00:00:00",time()+7*86400);

$r=$DB->Execute("SELECT id,login, mail, sub_end FROM users WHERE sub_end<'".$date."' AND sub_send='0'");
if($r->fields['id'])
{
	while(!$r->EOF)
	{
		//wyciągnięcie maila
		$mail=$r->fields['mail'];
		if($mail)
		{
			$user=array();
			$user[]=array("mail"=>$mail,"name"=>$r->fields['login']);
			$x=array();
			$x['name']=$func->show_with_html($r->fields['login']);
			$x['date']=$func->show_with_html(date("Y-m-d",strtotime($r->fields['sub_end'])));
			$result=$mailer->send($user,"subscription",$x,null);
			if($result) { $up[]=$r->fields['id']; }
		}
		$up[]=$r->fields['id'];
		$r->MoveNext();
	}
	$DB->Execute("UPDATE `users` SET `sub_send`='1' WHERE `id` in (".implode(",",$up).")");
}
/*$user[]=array("mail"=>"sylwek@egrupa.pl","name"=>"Sylwek");
			$x=array();
			$x['name']=$func->show_with_html("Sylwek");
			$x['date']=$func->show_with_html(date("Y-m-d",strtotime($date)));
			$result=$mailer->send($user,"subscription",$x,null);*/
?>