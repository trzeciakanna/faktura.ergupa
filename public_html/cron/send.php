<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$mailer=new mailer();
//maile do klientów
$up=array();
$r=$DB->Execute("SELECT clients_msg.topic, clients_msg.text, clients.email, clients_send.id FROM clients_msg INNER JOIN clients_send ON clients_msg.id=clients_send.msg_id INNER JOIN clients ON clients_send.client_id=clients.id WHERE clients_send.send='0' ORDER BY clients_msg.date ASC LIMIT 0,10");
if($r->fields['id'])
{
	while(!$r->EOF)
	{
		//wyciągnięcie maila
		$mail="";
		$t=preg_split("/[,\s]+/",$r->fields['email']);
		foreach($t as $m)
		{ if($func->is_mail($m)) { $mail=$m; break; } }
		if($mail)
		{
			$user=array();
			$user[]=array("mail"=>$mail,"name"=>$mail);
			$x=array();
			$x['title']=$func->show_with_html($r->fields['topic']);
			$x['text']=$func->show_with_html($r->fields['text']);
			$result=$mailer->send($user,"clients_msg",$x,null);
			if($result) { $up[]=$r->fields['id']; }
		}
		$r->MoveNext();
	}
	$DB->Execute("UPDATE `clients_send` SET `send`='1' WHERE `id` in (".implode(",",$up).")");
}

//maile do użystkowników
$up=array();
$r=$DB->Execute("SELECT newsletter_msg.topic, newsletter_msg.text, users.mail, newsletter_send.id FROM newsletter_msg INNER JOIN newsletter_send ON newsletter_msg.id=newsletter_send.msg_id INNER JOIN users ON newsletter_send.user_id=users.id WHERE newsletter_send.send='0' ORDER BY newsletter_msg.date ASC LIMIT 0,10");
if($r->fields['id'])
{
	while(!$r->EOF)
	{
		if($r->fields['mail'])
		{
			$user=array();
			$user[]=array("mail"=>$r->fields['mail'],"name"=>$r->fields['mail']);
			$x=array();
			$x['title']=$func->show_with_html($r->fields['topic']);
			$x['text']=$func->show_with_html($r->fields['text']);
			$result=$mailer->send($user,"newsletter_msg",$x,null);
			if($result) { $up[]=$r->fields['id']; }
		}
		$r->MoveNext();
	}
	$DB->Execute("UPDATE `newsletter_send` SET `send`='1' WHERE `id` in (".implode(",",$up).")");
}
?>