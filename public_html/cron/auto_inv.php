<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."includes/search.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$inv=new invoice();
$search=new search();

$date=date("Y-m-d");

$r=$DB->Execute("SELECT * FROM `invoice_auto` WHERE `date_next`<='".date("Y-m-d")."'");
if($r->fields['id'])
{
	while(!$r->EOF)
	{
		//utworzenie faktury
		list($data,$count)=$inv->get_to_pdf($r->fields['invoice_id'],$r->fields['user_id']);
		$data['date_create']=$date;
		$data['date_sell']=$date;
		$data['number']=($inv->counter($r->fields['user_id'])+1).date("/m/Y");
		$inv->add($r->fields['user_id'],$data,$count,true);
		//przepisanie
		$date_next=date("Y-m-d",strtotime(date("Y-m-d")." +".$r->fields['value']." ".$r->fields['period'].($r->fields['value']>1?"s":"")));
		$DB->Execute("UPDATE `invoice_auto` SET `date_next`='".$date_next."' WHERE `id`='".$r->fields['id']."'");
		$r->MoveNext();
	}
}
?>