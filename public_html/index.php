<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',6600);
ini_set('memory_limit','128M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");

/*include("tech.php");
exit();*/

/* zdefiniowanie głównych stałych */
$tmp=explode("/",$_SERVER['SCRIPT_FILENAME']);
$tmp2=explode("/",$_SERVER['REQUEST_URI']);
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
//define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("PAGE_URL",str_replace(".php","",$tmp[sizeof($tmp)-1]));
define("MENU_URL",str_replace(".php","",$tmp2[1]));
define("CACHE_TIME",60);

if($_COOKIE['lang']=="en") { define("LANG","en"); }
else { define("LANG","pl"); }

define("LAYOUT","standard");
$uploader_count=0;
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
require(ROOT_DIR."module/navigation/class.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$smart_conf=new smarty_conf();
$smarty=$smart_conf->initialize();
$users=new users();
//pasek navigacji/metadane
require(ROOT_DIR."module/navigation/".LANG.".php");
global $nlang;
$navi=new navi($nlang);
/* sprawdzenie czy zalogowany */
if($_SESSION['user_id']) { $my=$users->read_one($_SESSION['user_id'],false); }
elseif($_COOKIE['user_id']) { $my=$users->read_one_by_hash($_COOKIE['user_id']); }
if($my->id)
{
	define("USER_ID",$my->id);
	define("USER_LOGIN",$my->login);
	define("USER_RANG",$my->rang);
	define("USER_MAIL",$my->mail);
	define("USER_SUB",$my->sub_end);
}
else
{
	define("USER_ID","");
	define("USER_LOGIN","");
	define("USER_RANG","");
	define("USER_MAIL","");
	define("USER_SUB","");
}
/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
/* oczyszczenie zmiennych i pierwszy punk pomiaru czasu */
$conf=array();
$_SESSION['microtime']=array();
//$func->microtime_point();
/* przesłanie do szablonu głównych parametrów */
$smarty->assign(array(
	"root_dir"=>ROOT_DIR,
	"root_url"=>ROOT_URL,
	"page_url"=>PAGE_URL,
	"menu_url"=>MENU_URL,
	"user_login"=>USER_LOGIN,
	"user_rang"=>USER_RANG,
	"meta_title"=>$navi->title,
	"meta_desc"=>$navi->desc,
	"meta_keys"=>$navi->keys,
	"cf_lang"=>LANG,
	"user_data"=>$my
));
/* wyświetlenie nagłówka */
$smarty->assign("meta",$func->meta_read());
$smarty->display("head/".LAYOUT.".html");
/* wyświetlenie menu */
require(ROOT_DIR."module/menu/module.php");
// require(ROOT_DIR."module/login/module.php");

require(ROOT_DIR."module/navigation/module.php");
/* główny silnik strony */
require(ROOT_DIR."module/news/class.php");
$news=new newses();
$conf['news']=$func->read_config("module/news/config.xml");
require(ROOT_DIR."module/dymek/module.php");
switch($_GET['par1'])
{
	case "news":
		require(ROOT_DIR."module/news/module.php");
		break;
	case "invoice":
		require(ROOT_DIR."module/invoice/module.php");
		break;
	case "o-programie":
		require(ROOT_DIR."module/o-programie/module.php");
		break;
	case "cennik":
		require(ROOT_DIR."module/cennik/module.php");
		break;
	case "cennik2":
		require(ROOT_DIR."module/cennik2/module.php");
		break;
	case "faktura-zaliczkowa":
		require(ROOT_DIR."module/faktura-zaliczkowa/module.php");
		break;
	case "faktura-pro-forma":
		require(ROOT_DIR."module/faktura-pro-forma/module.php");
		break;
	case "faktura-korygujaca":
		require(ROOT_DIR."module/faktura-korygujaca/module.php");
		break;
	case "nota-korygujaca":
		require(ROOT_DIR."module/nota-korygujaca/module.php");
		break;
	case "rachunek":
		require(ROOT_DIR."module/rachunek/module.php");
		break;
	case "contact":
		require(ROOT_DIR."module/contact/module.php");
		require(ROOT_DIR."module/contact_send/module.php");

		break;
	case "reg":
		require(ROOT_DIR."module/reg/module.php");
		break;
	case "panel":
		require(ROOT_DIR."module/panel/module.php");
		break;
	case "faq":
		require(ROOT_DIR."module/faq/module.php");
		break;
	case "promocje":
		require(ROOT_DIR."module/promocje/module.php");
		break;
	case "pomoc":
		require(ROOT_DIR."module/pomoc/module.php");
		break;
	case "funkcje":
		require(ROOT_DIR."module/funkcje/module.php");
		break;
	case "bezpieczenstwo":
		require(ROOT_DIR."module/bezpieczenstwo/module.php");
		break;
	case "regulamin":
		require(ROOT_DIR."module/regulamin/module.php");
		break;
	case "polityka":
		require(ROOT_DIR."module/polityka/module.php");
		break;
	case "logout":
		unset($_SESSION['user_id']);
		setcookie("user_id","",time()-3600,"/");
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit();
		break;
	case "lost_pass":
	  require(ROOT_DIR."module/lost_pass/module.php");

    break;
	default:
   // require(ROOT_DIR."module/news/class.php");
    //odczyt konfiguracji
   // $news=new newses();
   // $conf['news']=$func->read_config("module/news/config.xml");
// tutej było logowanie
		require(ROOT_DIR."module/main/module.php");
			require(ROOT_DIR."module/probe/module.php");
		if(!USER_LOGIN){
			require(ROOT_DIR."module/reg2/module.php");
		}

			require(ROOT_DIR."module/news/list2/module.php");
			require(ROOT_DIR."module/contact_send/module.php");


		break;
}

/* wyświetlenie stopki */

$smarty->display("foot/".LAYOUT.".html");
/* rozłączenie z bazą danych */
$conn->disconect($DB);
/* wyświetlenie punktów pomiatu czasu */
//$func->microtime_point();
//$func->microtime_show();

?>
