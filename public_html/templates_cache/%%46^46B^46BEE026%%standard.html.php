<?php /* Smarty version 2.6.19, created on 2015-02-28 13:59:36
         compiled from panel/data_number/standard.html */ ?>
<script type="text/javascript" src="module/panel/data_number/class.js"></script>
<div id="wyszukiwarka_faktur" class="ramka">
	<h3><?php echo $this->_tpl_vars['lang']['nagl0']; ?>
</h3>
	<div class="btn-group">
		<a href="panel/data/number/" title="" class="btn <?php if ($_GET['par4'] == ''): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay']; ?>
 <?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</a>
		<a href="panel/data/number/1/" title="" class="btn <?php if ($_GET['par4'] == 1): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay1']; ?>
</a>
		<a href="panel/data/number/2/" title="" class="btn <?php if ($_GET['par4'] == 2): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay2']; ?>
</a>
		<a href="panel/data/number/3/" title="" class="btn <?php if ($_GET['par4'] == 3): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay3']; ?>
</a>
		<a href="panel/data/number/4/" title="" class="btn <?php if ($_GET['par4'] == 4): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay4']; ?>
</a>
		<a href="panel/data/number/5/" title="" class="btn <?php if ($_GET['par4'] == 5): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay5']; ?>
</a>
	  	</div>

	<form action="" onsubmit="javascript: return panel_data_number_obj.submit(<?php echo $this->_tpl_vars['user_id']; ?>
,'<?php echo $_GET['par4']; ?>
');" id="data_number_save">
		<div class="invoice_menu" style="font-size:12px;">
		<br>
		<?php echo $this->_tpl_vars['lang']['nagl4']; ?>

			<span id="example" style="font-size:20px;font-weight:bold;"></span>
			<br><br>
		</div>
		<div class="alert alert-success" id="save_ok_error"></div>
		<div class="alert alert-danger" id="save_bad_error"></div>
		<table class="table table-bordered">

			<tr>
				<td class="col-sm-6">
					<?php echo $this->_tpl_vars['lang']['nr_last']; ?>

					<?php if ($_GET['par4'] != 0): ?>
					<input type="text" name="nr_last" value="<?php echo $this->_tpl_vars['dane']->nr_last; ?>
" class="form-control" />
					<?php else: ?>
					<input type="text"  class="form-control" disabled />
					<?php endif; ?>
				</td>
	
				<td class="col-sm-6" colspan="2">
					<span class="naglowek"><?php echo $this->_tpl_vars['lang']['letter']; ?>
</span>
					<input type="text" name="letter" id="letter" value="<?php echo $this->_tpl_vars['dane']->nr_letter; ?>
" <?php if ($this->_tpl_vars['format'] != "x/m/X/y"): ?>disabled="disabled"<?php endif; ?> onkeypress="javascript: return panel_data_number_obj.letter(event);"  class="form-control"/>
				</td>

			</tr>
			<tr>
				<td rowspan="2" class="col-sm-4">
					<span class="naglowek"><?php echo $this->_tpl_vars['lang']['format']; ?>
</span>
					<div class="radio"><label><input type="radio" name="format" id="format1" value="n/d/m/y" <?php if ($this->_tpl_vars['format'] == "x/d/m/y"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</label></div>
					<div class="radio"><label><input type="radio" name="format" id="format2" value="n/m/y" <?php if ($this->_tpl_vars['format'] == "x/m/y"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</label></div>
					<div class="radio"><label><input type="radio" name="format" id="format3" value="n/y" <?php if ($this->_tpl_vars['format'] == "x/y"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</label></div>
					<div class="radio"><label><input type="radio" name="format" id="format4" value="y/m/d/n" <?php if ($this->_tpl_vars['format'] == "y/m/d/x"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</label></div>
					<div class="radio"><label> <input type="radio" name="format" id="format5" value="y/m/n" <?php if ($this->_tpl_vars['format'] == "y/m/nx"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl9']; ?>
</label></div>
					<div class="radio"><label> <input type="radio" name="format" id="format6" value="y/n" <?php if ($this->_tpl_vars['format'] == "y/x"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</label></div>
					<div class="radio"><label> <input type="radio" name="format" id="format7" value="n/m/A/y" <?php if ($this->_tpl_vars['format'] == "x/m/X/y"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl11']; ?>
</label></div>
					<div class="radio"><label><input type="radio" name="format" id="format8" value="n/A/y" <?php if ($this->_tpl_vars['format'] == "x/X/y"): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['nagl12']; ?>
</label></div>
				</td>
				<td class="col-sm-4">
					<span class="naglowek"> <?php echo $this->_tpl_vars['lang']['day']; ?>
</span>
					<div class="radio">
						<label><input type="radio" name="day" id="day1" value="j" <?php if (str_replace ( 'j' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['day1']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="day" id="day2" value="d" <?php if (str_replace ( 'd' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['day2']; ?>
</label>
					</div>
				</td>
				<td class="col-sm-4">
					<span class="naglowek"><?php echo $this->_tpl_vars['lang']['month']; ?>
</span>
					<div class="radio">
						<label><input type="radio" name="month" id="month1" value="n" <?php if (str_replace ( 'n' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /><?php echo $this->_tpl_vars['lang']['month1']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="month" id="month2" value="m" <?php if (str_replace ( 'm' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /><?php echo $this->_tpl_vars['lang']['month2']; ?>
</label>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<span class="naglowek"><?php echo $this->_tpl_vars['lang']['year']; ?>
</span>
					<div class="radio">
						<label><input type="radio" name="year" id="year1" value="y" <?php if (str_replace ( 'y' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['year1']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="year" id="year2" value="Y" <?php if (str_replace ( 'Y' , "" , $this->_tpl_vars['form'] ) != $this->_tpl_vars['form']): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['year2']; ?>
</label>
					</div>
				</td>
				<td>
					<span class="naglowek"><?php echo $this->_tpl_vars['lang']['number']; ?>
</span>
					<div class="radio">
						<label><input type="radio" name="number" id="number1" value="1" <?php if ($this->_tpl_vars['dane']->nr_type == 1): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['number1']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="number" id="number2" value="2" <?php if ($this->_tpl_vars['dane']->nr_type == 2): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['number2']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="number" id="number3" value="3" <?php if ($this->_tpl_vars['dane']->nr_type == 3): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['number3']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="number" id="number4" value="4" <?php if ($this->_tpl_vars['dane']->nr_type == 4): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['number4']; ?>
</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="number" id="number5" value="5" <?php if ($this->_tpl_vars['dane']->nr_type == 5): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['number5']; ?>
</label>
					</div>
				</td>
			</tr>
		</table>
		
				<div class="text-right">
					<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
					<input type="hidden" name="type" value="<?php echo $_GET['par4']; ?>
" />
										<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn orangeButton" />
				</div>
			</div>
		</div>
	</form>
</div>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>

<script type="text/javascript">panel_data_number_obj.init();panel_data_number_obj.example();</script>