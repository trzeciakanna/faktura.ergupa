<?php /* Smarty version 2.6.19, created on 2015-03-01 09:10:54
         compiled from panel/client_send/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'panel/client_send/standard.html', 31, false),)), $this); ?>
<div  class="ramka" >
<script type="text/javascript" src="module/panel/client_send/class.js"></script>

<?php if ($this->_tpl_vars['action'] == 'send'): ?>

	<form action="" onsubmit="javascript: return panel_client_send_obj.submit();" id="client_send_form">
		<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
 </h3>
		<div id="send_error_error" class="alert alert-danger"></div>
		<div id="send_ok_error" class="alert alert-success"></div>
		<div class="row">
			<div class="col-sm-5">
				<?php if ($_GET['par5']): ?><a href="panel/client/send/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a><?php endif; ?>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['topic']; ?>
:</label>
					<input type="text" name="topic" id="topic" value="<?php echo $this->_tpl_vars['msg']->topic; ?>
" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['text']; ?>
:</label>
					<textarea cols="" rows="10" name="text" id="text" class="form-control" ><?php echo $this->_tpl_vars['msg']->text; ?>
</textarea>
				</div>
			</div> 
			<div class="col-sm-6 col-sm-offset-1">
				<div class="form-group">
					<label>Wyszukaj kontrahenta:</label>
					<input type="text" value="" class="form-control" onkeyup="javascript: panel_client_send_obj.search(this.value);" />
				</div>
				<div class="form-group">
					<label><input type="checkbox" onclick="javascript: panel_client_send_obj.select_all(this.checked)" /> <?php echo $this->_tpl_vars['lang']['all']; ?>
</label>
				</div>
						<div class="row_info" id="count_users"><?php echo ((is_array($_tmp=$this->_tpl_vars['lang']['count'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[count]", '0') : smarty_modifier_replace($_tmp, "[count]", '0')); ?>
</div>
				<div id="item_list">
										<div class="page">
						<div class="row">
							<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
								<label for="mail_<?php echo $this->_tpl_vars['i']->id; ?>
" class="col-sm-4 text-left"><input type="checkbox" name="user_send[]" id="mail_<?php echo $this->_tpl_vars['i']->id; ?>
" value="<?php echo $this->_tpl_vars['i']->id; ?>
" <?php if (in_array ( $this->_tpl_vars['i']->id , $this->_tpl_vars['user'] )): ?>checked="checked"<?php endif; ?> class="client_send_check" onclick="javascript: panel_client_send_obj.count();" /> <?php echo $this->_tpl_vars['i']->name; ?>
</label>
							<?php if (( ($this->_foreach['a']['iteration']-1)+1 ) % 3 == 0 && ($this->_foreach['a']['iteration']-1) != 0 && ( ($this->_foreach['a']['iteration']-1)+1 ) % 6 != 0): ?></div><div class="row"><?php endif; ?>
							<?php if (( ($this->_foreach['a']['iteration']-1)+1 ) % 21 == 0 && ($this->_foreach['a']['iteration']-1) != 0): ?>
							</div></div>
							<div style="display:none;" class="page">
								<div class="row">
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?>
					</div>
				</div>
				<ul id="pagination" class="pagination">
				</ul>
				<script type="text/javascript">createLinks()</script>
			</div>
		</div>
		<div class="text-right col-sm-12">
			<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="btn btn-danger"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" class="btn orangeButton"  />
			<br>
			<br>
			<br>
		</div>
	</form>
		<div class="row_hidden">
		<input type="hidden" id="count" value="<?php echo $this->_tpl_vars['lang']['count']; ?>
" />
		<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
		<input type="hidden" id="err_topic" value="<?php echo $this->_tpl_vars['lang']['err_topic']; ?>
" />
		<input type="hidden" id="err_text" value="<?php echo $this->_tpl_vars['lang']['err_text']; ?>
" />
		<input type="hidden" id="err_users" value="<?php echo $this->_tpl_vars['lang']['err_users']; ?>
" />
		<input type="hidden" id="send_ok" value="<?php echo $this->_tpl_vars['lang']['send_ok']; ?>
" />
		<input type="hidden" id="send_error" value="<?php echo $this->_tpl_vars['lang']['send_error']; ?>
" />
	</div>
	<script type="text/javascript">panel_client_send_obj.count();</script>
	
<?php elseif ($this->_tpl_vars['action'] == 'last'): ?>


<h3><?php echo $this->_tpl_vars['lang']['last']; ?>
 </h3>

<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
	<div class="text-right">
		<div class="btn-group alphabet">
			<?php if (! $_GET['par6']): ?>
			<a href="panel/client/send/last/" title="<?php echo $this->_tpl_vars['lang']['all_last']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['lang']['all_last']; ?>
</a>
			<?php else: ?>
			<a href="panel/client/send/last/" title="<?php echo $this->_tpl_vars['lang']['all_last']; ?>
" class="btn btn-default btn-xs"><?php echo $this->_tpl_vars['lang']['all_last']; ?>
</a>
			<?php endif; ?>
		<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<?php if ($this->_tpl_vars['i'] == $_GET['par6']): ?><a href="panel/client/send/last/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
			<?php else: ?><a href="panel/client/send/last/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-default btn-xs" ><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</div>
	</div>
	<table class="table table-striped table-hover table-bordered lista">
		<tr>
			<th><?php echo $this->_tpl_vars['lang']['table_nagl1']; ?>
</th>
			<th><?php echo $this->_tpl_vars['lang']['table_nagl2']; ?>
</th>
			<th class="text-center col-sm-3"><?php echo $this->_tpl_vars['lang']['table_nagl4']; ?>
</th>
		</tr>
		<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
		<tr>
			<td><a href="panel/client/send/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><span class="glyphicon glyphicon-eye-open"></span>   <?php echo $this->_tpl_vars['i']->topic; ?>
</a></td>
			<td><a href="panel/client/send/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
" ><?php echo $this->_tpl_vars['i']->date; ?>
</a></td>
			<td class="text-center"><a href="panel/client/send/resend/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['resend']; ?>
"><span class="glyphicon glyphicon-envelope"></span></a></td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/client/send/last/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
</div>

<?php elseif ($this->_tpl_vars['action'] == 'view'): ?>
<div class="row_action"><a href="panel/client/send/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<div class="row_action"><a href="panel/client/send/resend/<?php echo $_GET['par5']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['resend']; ?>
"><span class="glyphicon glyphicon-envelope"></span> <?php echo $this->_tpl_vars['lang']['resend']; ?>
</a></div>
<div class="row_head"><?php echo $this->_tpl_vars['msg']->topic; ?>
</div>
<div class="small"><?php echo $this->_tpl_vars['lang']['sended']; ?>
 <strong><?php echo $this->_tpl_vars['msg']->date; ?>
</strong></div>
<div class="row_auto"><?php echo $this->_tpl_vars['msg']->text; ?>
</div>
<div class="row_action"><?php echo $this->_tpl_vars['lang']['client']; ?>
</div>
<div class="row_auto">
<?php $_from = $this->_tpl_vars['user']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><div class="col_item"><?php echo $this->_tpl_vars['i']->name; ?>
</div><?php endforeach; endif; unset($_from); ?>
</div>
<?php endif; ?>
</div>