<?php /* Smarty version 2.6.19, created on 2017-09-11 13:54:45
         compiled from lost_pass/standard.html */ ?>
<div id="strona_glowna"  class="ramka">
<script type="text/javascript" src="module/lost_pass/class.js"></script>
	<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>
	<form action="" onsubmit="javascript: return lost_pass_obj.submit();" id="lost_pass_form">
		<div class="row">
			<fieldset class="col-sm-4">
				<div class="alert alert-danger" id="lost_pass_error_error"></div>
				<div class="alert alert-success" id="lost_pass_ok_error"></div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['login']; ?>
:</label>
					<input type="text" name="login" id="loginLost" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
					<input type="text" name="mail" id="mailLost" class="form-control"  />
				</div>
							<div class="text-right">
					<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" style="clear:left;margin-left:308x;" class="btn orangeButton" />
					<br><br>
				</div>
			</fieldset>
		</div>
	</form>
		<div class="row_hidden">
		<input type="hidden" id="lost_pass_bad" value="<?php echo $this->_tpl_vars['lang']['log_bad']; ?>
" />
		<input type="hidden" id="lost_pass_ok" value="<?php echo $this->_tpl_vars['lang']['log_ok']; ?>
" />
		<input type="hidden" id="lost_pass_send" value="<?php echo $this->_tpl_vars['lang']['log_send']; ?>
" />
		<input type="hidden" id="lost_pass_empty_captcha" value="<?php echo $this->_tpl_vars['lang']['empty_captcha']; ?>
" />
	</div>
</div>