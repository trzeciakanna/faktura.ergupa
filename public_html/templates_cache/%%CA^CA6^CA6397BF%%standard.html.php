<?php /* Smarty version 2.6.19, created on 2017-02-07 12:46:23
         compiled from panel/data_general/standard.html */ ?>
<div class="ramka">
	<script type="text/javascript" src="module/panel/data_general/class.js"></script>
	<script type="text/javascript" src="jscript/valid_data.js"></script>


		<div class="row">
		<div class="col-sm-6">
			<form action="" onsubmit="javascript: return panel_data_gen_obj.submit();" id="data_gen_save" class="form-horizontal">
								<h3>Dane sprzedawcy wyświetlane na fakturze</h3>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['name']; ?>
</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="name" name="name" class="form-control"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
					</div>	
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['address']; ?>
</label>
					<div class="col-sm-8">					
						<textarea cols="" rows="2" id="address" name="address" class="form-control"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['nip']; ?>
</label>
					<div class="col-sm-8">
						<input type="text" name="nip" id="nip" value="<?php echo $this->_tpl_vars['user']->nip; ?>
" class="form-control" <?php if ($this->_tpl_vars['user']->nip): ?>onkeypress="javascript: return false"<?php endif; ?> />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['phone']; ?>
</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="phone" name="phone" class="form-control"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['email']; ?>
</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="email" name="email" class="form-control"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['krs']; ?>
</label>
					<div class="col-sm-8">
						<input type="text" name="krs" id="krs" value="<?php echo $this->_tpl_vars['user']->krs; ?>
" maxlength="10" class="form-control" onkeypress="javascript: return valid_data.check_krs(event);" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['place']; ?>
</label>
					<div class="col-sm-8">
					<input type="text" name="place" id="place" value="<?php echo $this->_tpl_vars['user']->place; ?>
" maxlength="64" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['www']; ?>
</label>
					<div class="col-sm-8">
						<input type="text" name="www" id="www" value="<?php echo $this->_tpl_vars['user']->www; ?>
" maxlength="127" class="form-control" />
					</div>
				</div>
								
				<div class="text-right">
					<br>
					<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />

					<button type="submit" class="btn orangeButton" ><?php echo $this->_tpl_vars['lang']['save']; ?>
</button>
				<div id="save_error_error" class="dane_error"></div>
				<div id="dane_bledy_error" class="dane_error"></div>

				<div id="save_ok_error"></div>
				</div>
			</form>
		</div>
		<div class="col-sm-6">
			<!-- pierwsze jest domyślne -->

			  <h3><?php echo $this->_tpl_vars['lang']['account_change']; ?>
</h3>	


			 <?php if ($_SESSION['saveAc'] != 0): ?> <?php echo $this->_tpl_vars['lang']['saveAc']; ?>
 <?php endif; ?> 
				<?php $_from = $this->_tpl_vars['user']->account; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
				<div class="form-horizontal">
				<?php if (($this->_foreach['a']['iteration']-1) == 0): ?><div class="row"><div class="col-sm-8 col-sm-offset-4"><strong>Konto domyślne</strong></div></div><?php endif; ?>
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
</label>
					<div class="col-sm-8">
						<input type="text" name="bank_name<?php echo $this->_tpl_vars['k']; ?>
" id="bank_name<?php echo $this->_tpl_vars['k']; ?>
" value="<?php echo $this->_tpl_vars['i'][1]; ?>
" class="form-control" />
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-4"><?php echo $this->_tpl_vars['lang']['account']; ?>
</label>
					<div class="col-sm-8">
						<input type="text" name="account<?php echo $this->_tpl_vars['k']; ?>
" id="account<?php echo $this->_tpl_vars['k']; ?>
" value="<?php echo $this->_tpl_vars['i'][0]; ?>
" maxlength="50" class="form-control" />
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4" >
						<button onclick="javascript: panel_data_gen_obj.delAc(<?php echo $this->_tpl_vars['user']->id; ?>
,<?php echo $this->_tpl_vars['k']; ?>
);" class="btn btn-danger"><?php echo $this->_tpl_vars['lang']['del']; ?>
</button>
					</div>	
					<div class="col-sm-4 text-right">
						<button onclick="javascript: panel_data_gen_obj.saveAc(<?php echo $this->_tpl_vars['user']->id; ?>
,<?php echo $this->_tpl_vars['k']; ?>
);" class="btn orangeButton"><?php echo $this->_tpl_vars['lang']['save']; ?>
</button>
					</div>
				</div>
				</div>
				<hr>
			  <?php endforeach; endif; unset($_from); ?>
			   <?php if ($_SESSION['addAc'] != 0): ?><?php echo $this->_tpl_vars['lang']['addAc']; ?>
<?php endif; ?>
				<h4><?php echo $this->_tpl_vars['lang']['add_account']; ?>
</h4>
				 <label><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
</label><input type="text" name="bank_name" id="bank_name" value="" class="form-control" />
			  <label><?php echo $this->_tpl_vars['lang']['account']; ?>
</label><input type="text" name="account" id="account" value="" maxlength="50" class="form-control" />

			  <div class="text-right">
				<br>
				<button onclick="javascript: panel_data_gen_obj.addAc(<?php echo $this->_tpl_vars['user']->id; ?>
);" class="btn orangeButton"><?php echo $this->_tpl_vars['lang']['add']; ?>
</button>
				<br><br><br>
			</div>
		</div>
	</div>




<div class="row_hidden">
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>
<div class="clear:both"></div>
</div>