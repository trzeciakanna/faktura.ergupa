<?php /* Smarty version 2.6.19, created on 2015-02-27 11:03:23
         compiled from panel/invoice_org/standard.html */ ?>
<script type="text/javascript" src="module/panel/invoice_org/class.js"></script>
<script type="text/javascript" src="jscript/calendar.js"></script>
<div class="ramka">
<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>
<div id="save_ok_error" class="alert alert-success"></div>
<div id="save_error_error" class="alert alert-danger"></div>


<form action="" id="invoice_org_from" onsubmit="javascript: return panel_invoice_org_obj.submit()">

<table class="table table-striped table-hover table-bordered lista">
	<tr>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</th>
	<th class="text-center">Nazwa firmy</th>	
	<th>Co ile?</th>
	<th>Częstotliwość</th>
	<th><?php echo $this->_tpl_vars['lang']['time_start']; ?>
</th>
	<th>Wystawi się</th>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</th>
	</tr>
	<?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
	<tr>
			<td class="text-center"><a href="invoice/<?php echo $this->_tpl_vars['i']->invoice_id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><span class="glyphicon glyphicon-eye-open"></span></a></td>
			<td><a href="panel/invoice/list/client/<?php echo $this->_tpl_vars['i']->client_url; ?>
/" title="<?php echo $this->_tpl_vars['i']->client; ?>
"><?php echo $this->_tpl_vars['i']->client; ?>
</a></td>
			<td style="width:10%;"><input type="text" value="<?php echo $this->_tpl_vars['i']->value; ?>
" name="period_value<?php echo $this->_tpl_vars['k']; ?>
" id="period_value<?php echo $this->_tpl_vars['k']; ?>
" onkeypress="javascript: return panel_invoice_org_obj.insert_prize(event,this.id);" class="form-control" /></td>
			<td><select name="period_time<?php echo $this->_tpl_vars['k']; ?>
" class="form-control">
				<option value="day" <?php if ($this->_tpl_vars['i']->period == 'day'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lang']['time_day']; ?>
</option>
				<option value="week" <?php if ($this->_tpl_vars['i']->period == 'week'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lang']['time_week']; ?>
</option>
				<option value="month" <?php if ($this->_tpl_vars['i']->period == 'month'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lang']['time_month']; ?>
</option>
				<option value="year" <?php if ($this->_tpl_vars['i']->period == 'year'): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['lang']['time_year']; ?>
</option>
			</select></td>
			<td style="max-width:420px;width:25%"><div style="width:400px"><script type="text/javascript">calendar.create('period_start<?php echo $this->_tpl_vars['k']; ?>
','<?php echo $this->_tpl_vars['i']->date_start; ?>
');</script></div></td>
			<input type="hidden" name="period_id<?php echo $this->_tpl_vars['k']; ?>
" value="<?php echo $this->_tpl_vars['i']->id; ?>
" />
		<td class="text-center"><?php echo $this->_tpl_vars['i']->date_next; ?>
</td>

		<td class="text-center"><a href="panel/invoice/org/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
<div class="text-right">
	<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn orangeButton" />
	<br><br><br>
</div>
</form>
</div>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
</div>