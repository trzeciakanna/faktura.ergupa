<?php /* Smarty version 2.6.19, created on 2015-02-27 14:54:54
         compiled from news/list/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'news/list/standard.html', 22, false),array('modifier', 'truncate', 'news/list/standard.html', 22, false),)), $this); ?>
<div id="invoice" class="news_list row">
	<?php if (count ( $this->_tpl_vars['dane'] )): ?>
						<div class="col-sm-9">
			<div class="lista_newsow">
				<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
					<div class="news">
												<div class="news_text" >
							<h2 class="text-left">
								<a href="news/<?php echo $this->_tpl_vars['i']->url; ?>
/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->title; ?>
"><?php echo $this->_tpl_vars['i']->title; ?>
</a>
							</h2>
														<div class="news_txt"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']->text)) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 500, "...", true) : smarty_modifier_truncate($_tmp, 500, "...", true)); ?>
 </div>
						</div>
						<div class="news_opt">
							<span class="mr10"><a href="news/<?php echo $this->_tpl_vars['i']->url; ?>
/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['more']; ?>
" class="comment"><?php echo $this->_tpl_vars['lang']['comment']; ?>
 <strong>(<?php echo $this->_tpl_vars['i']->count_c; ?>
)</strong> </a></span>
							<span class="mr10"><?php echo $this->_tpl_vars['lang']['date_add']; ?>
: <strong><?php echo $this->_tpl_vars['i']->date_add; ?>
</strong></span>
							<?php if ($this->_tpl_vars['i']->date_add != $this->_tpl_vars['i']->date_mod): ?>
								<span class="mr10"><?php echo $this->_tpl_vars['lang']['date_mod']; ?>
: <strong><?php echo $this->_tpl_vars['i']->date_mod; ?>
</strong></span>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; endif; unset($_from); ?>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "news/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</div>
		</div>
	<?php else: ?>
		<div class="row_error"><?php echo $this->_tpl_vars['lang']['none']; ?>
</div>
	<?php endif; ?>

	<div class="col-sm-3">
		<div class="ramka o_programie">
			<h4><?php echo $this->_tpl_vars['lang']['o_programie_nagl']; ?>
</h4>
			<span class="title"><?php echo $this->_tpl_vars['lang']['o_programie_nagl2']; ?>
</span>
			<p><?php echo $this->_tpl_vars['lang']['o_programie']; ?>
</p>
			<a href="o-programie/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
		</div>
		<br>
		<div class="ramka centrum_pomocy">
			<h4><?php echo $this->_tpl_vars['lang']['pomoc_nagl']; ?>
</h4>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc1_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc1_tresc']; ?>
</p>
				<a href="pomoc/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc2_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc2_tresc']; ?>
</p>
				<a href="contact/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc3_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc3_tresc']; ?>
</p>
				<a href="contact/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
		</div>
	</div>
</div>