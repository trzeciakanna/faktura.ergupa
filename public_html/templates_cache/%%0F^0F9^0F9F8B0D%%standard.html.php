<?php /* Smarty version 2.6.19, created on 2017-02-06 12:01:05
         compiled from login/standard.html */ ?>
<div class="col-sm-6 logowanie">
<div>
	<div class="topBox">
		<?php if ($this->_tpl_vars['login']): ?>
			<a href="panel/data/licence/" class="btn blueButton btn-sm"><?php echo $this->_tpl_vars['lang']['hello']; ?>
 <?php echo $this->_tpl_vars['login']; ?>
</a>&nbsp;
			<a href="/logout/" title="<?php echo $this->_tpl_vars['lang']['logout']; ?>
" class="btn blueButton btn-sm"><?php echo $this->_tpl_vars['lang']['logout']; ?>
</a>
		<?php else: ?>
			<a onclick="document.getElementById('logowanie').style.display='block';" class="btn blueButton btn-sm"><?php echo $this->_tpl_vars['lang']['zaloguj1']; ?>
</a>&nbsp;&nbsp;
			<a href="reg/" class="btn blueButton btn-sm"><?php echo $this->_tpl_vars['lang']['zaloguj3']; ?>
</a> 
		<?php endif; ?>
	</div>
	<div class="topBox">
		<a href="#" onclick="javascript: return changeLang('en');"><i class="england"></i></a>&nbsp;&nbsp;
		<a href="#" onclick="javascript: return changeLang('pl');"><i class="poland"></i></a>
	</div>
</div>
	<?php if ($this->_tpl_vars['login']): ?>
	<div class="row">
	<div class="col-sm-12 text-right">
					  <?php if ($this->_tpl_vars['user_data']->id < 13599): ?> <a href="http://old.faktura.egrupa.pl"><strong>Stary wygląd faktura.egrupa.pl</strong></a><?php endif; ?> <a href="/panel/data/licence/"><?php echo $this->_tpl_vars['lang']['abonament']; ?>
 <strong><?php echo $this->_tpl_vars['user_data']->sub_end; ?>
</strong>  <?php echo $this->_tpl_vars['lang']['przdluzAbonament']; ?>
</a>
	</div>
	</div>
	<?php else: ?>
	<div class="row">
		<div class="col-sm-12 text-right">
						</div>
	</div>
	<?php endif; ?>

<script type="text/javascript" src="module/login/class.js"></script>
<?php if (! $this->_tpl_vars['login']): ?>

<form onsubmit="javascript: return login_obj.submit();">
<fieldset id="logowanie" >
<label>
	<?php echo $this->_tpl_vars['lang']['login']; ?>
:<br />
	<input type="text" name="login" id="login"  onchange="javascript: login_obj.check_login();" />
</label>
<label>
	<?php echo $this->_tpl_vars['lang']['pass']; ?>
:<br />
	<input type="password" name="pass" id="pass"  />
</label>
<div id="login_error" style="font-weight:bold;color:red;padding:4px 0;"></div>
<div style="text-align:left;">
<label>	<input type="checkbox" name="remember" id="remember" value="1" /> <?php echo $this->_tpl_vars['lang']['remember']; ?>
</label>
		<button type="submit"  class="btn orangeButton btn-sm" ><?php echo $this->_tpl_vars['lang']['send']; ?>
</button>
	</div>
  <a href="lost_pass/"><?php echo $this->_tpl_vars['lang']['new_pass']; ?>
</a>

</fieldset>
</form>
<div class="row_hidden">
	<input type="hidden" id="log_bad" value="<?php echo $this->_tpl_vars['lang']['log_bad']; ?>
" />
	<input type="hidden" id="log_error" value="<?php echo $this->_tpl_vars['lang']['log_error']; ?>
" />
	<input type="hidden" id="log_referer" value="<?php echo $_SERVER['HTTP_REFERER']; ?>
" />
</div>
<?php endif; ?>

</div>
</div> 