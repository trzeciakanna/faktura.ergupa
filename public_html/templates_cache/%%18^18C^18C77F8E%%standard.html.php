<?php /* Smarty version 2.6.19, created on 2015-11-03 14:30:27
         compiled from head/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'head/standard.html', 69, false),)), $this); ?>
<!DOCTYPE html>
<html lang="pl">
	<head>
		<base href="<?php echo $this->_tpl_vars['root_url']; ?>
" />
		<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
		<meta name="ROBOTS" content="all" />
		<meta name="author" content="E-GRUPA " />
		<meta name="rating" content="General" />
		<meta name="Generator" content="Zend Studio 6.1" />
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="/css/bootstrap.css?hash=ffffffff" type="text/css" media="screen" />
		<link rel="stylesheet" href="/css/standard.css?hash=ffffffff" type="text/css" media="screen" />
				<script type="text/javascript" src="/jscript/function.js"></script>
		<script type="text/javascript" src="/jscript/mintajax.js"></script>
		
		<meta name="description" content="<?php echo $this->_tpl_vars['meta_desc']; ?>
 - <?php echo $this->_tpl_vars['meta']['description']; ?>
" />
		<meta name="keywords" content="<?php echo $this->_tpl_vars['meta_key']; ?>
, <?php echo $this->_tpl_vars['meta']['keywords']; ?>
" />
		<title><?php if ($this->_tpl_vars['meta_title']): ?><?php echo $this->_tpl_vars['meta_title']; ?>
 <?php else: ?> <?php echo $this->_tpl_vars['meta']['title']; ?>
 <?php endif; ?></title>
		<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25710160-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
	</head>

	<body onkeypress="javascript: return backspace(event);">
	<?php if ($_COOKIE['ie_error'] != 1): ?> 
	<!--[if IE]>
	<div style='width:100%;height:100%;position:fixed;left:0;right;0;top:0;bottom:0;background:#111;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)"; z-index:2;filter: alpha(opacity=50);'></div>
	<div id="ie_error" class="invoice_client" style="display: block;font-size: 17px;font-weight: bold;height: 256px;left: 50%;line-height: 18px;margin-left: -403px;margin-top: -135px;position: fixed;text-align: center;top: 50%;z-index:6;border:7px solid red;text-align:left;padding:30px;"><a class="close" title="zamknij" onclick="javascript: document.getElementById('ie_error').style.display='none';">Zamknij</a>
	Szanowni Państwo, <br><br>
Korzystacie z przeglądarki Internet Explorer, która generuje sporo błędów w naszej aplikacji<br><br>
W celu prawidłowego i bezpiecznego używania naszego programu, zalecamy korzystanie z przeglądarki <a href="http://firefox.pl">Firefox</a>, <a href="http://www.google.com/chrome?hl=pl" >Chrome</a>, <a href="http://www.opera.com/browser/">Opera</a> lub <a href="http://www.apple.com/pl/safari/">Safari</a>.<br><br>
W przypadku korzystania z Internet Explorer nie gwarantujemy bezpieczeństwa danych oraz prawidłowości działania naszego programu.
  
<br>
<br>Pobierz, zainstaluj i używaj nowej przeglądarki! <br><br>
	<form>
		<button style="line-height:20px;" onclick="javascript: document.getElementById('ie_error').style.display='none';">Zamknij to okno</button>&nbsp;&nbsp;&nbsp;
	<label style="text-align:left;font-size:13px;padding:0;">

	<input type="checkbox" name="aaa" onclick="javascript: ie_error(this);" style="margin:2px;padding:0;width:auto"/>Zapamiętaj mój wybór</label>


	</form>
	
	</div>
	<![endif]-->
	<?php endif; ?>
	<!--

</div>
	-->
<div class="container-fluid">
	<div class="row">
		<div id="head" class="col-sm-6 text-left">
						<a href="/" title="faktura VAT online">	<img src="images/layout/logo4.png" alt="faktura online" class="img-responsive" /></a>
						<?php if (count($_GET) == 0): ?>
			<h1 class="topHeading">Faktura vat program online</h1><?php endif; ?>
		</div>