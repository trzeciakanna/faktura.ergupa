<?php /* Smarty version 2.6.19, created on 2015-02-27 10:33:55
         compiled from panel/product_list/standard.html */ ?>
<script type="text/javascript" src="module/panel/product_list/class.js"></script>
<?php if ($this->_tpl_vars['action'] == 'list'): ?><div id="wyszukiwarka_faktur" class="ramka">
	<div class="row">
	<div class="col-sm-3"><h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3></div>
	<div class="col-sm-9">	<a href="panel/product/list/cat/" style="font-size:13px;margin-top:12px;display:inline-block;" title="<?php echo $this->_tpl_vars['lang']['view_cat']; ?>
"><span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['lang']['view_cat']; ?>
</a></div>
	</div>
	<div class="row">
		<div class="form-group col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
			<input type="text" id="name" value="<?php echo $this->_tpl_vars['search']['name']; ?>
" class="form-control" />
		</div>
		<div class="form-group col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['producer']; ?>
:</label>
			<input type="text" id="producer" value="<?php echo $this->_tpl_vars['search']['producer']; ?>
" class="form-control" />
		</div>
		<div class="form-group col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['cat']; ?>
:</label>
			<select name="cat" id="cat" class="form-control">
				<option value="">---</option>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "panel/product_list/standard_list.html", 'smarty_include_vars' => array('tree' => $this->_tpl_vars['tree'],'select' => $this->_tpl_vars['search']['cat'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</select>
		</div>
		
		<div class="product_list_search" id="search" style="display:none;">
			<div class="form-group col-sm-4">
				<label><?php echo $this->_tpl_vars['lang']['number']; ?>
:</label>
				<input type="text" id="number" value="<?php echo $this->_tpl_vars['search']['number']; ?>
" class="form-control" />
			</div>
			<div class="form-group col-sm-4">
				<label><?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
:</label>
				<input type="text" id="pkwiu" value="<?php echo $this->_tpl_vars['search']['pkwiu']; ?>
" class="form-control" />
			</div>
			<div class="form-group col-sm-4">
				<label><?php echo $this->_tpl_vars['lang']['prize']; ?>
:</label>
				<div class="row">
					<div class="col-sm-6">
						<input type="text" id="prize_min" value="<?php echo $this->_tpl_vars['search']['prize_min']; ?>
" class="form-control" onkeypress="javascript: return panel_product_list_obj.insert_prize(event,'prize_min');" placeholder="od" />
					</div>
					<div class="col-sm-6">
						<input type="text" id="prize_max" value="<?php echo $this->_tpl_vars['search']['prize_max']; ?>
" class="form-control" onkeypress="javascript: return panel_product_list_obj.insert_prize(event,'prize_max');" placeholder="do" />
					</div>
				</div>	
			</div>
		</div>
	</div> 
	<div class="row">
		<div class="col-sm-6">
			<a class="zwin_rozwin" id="search_show" onclick="javascript: panel_product_list_obj.show_search();"> <?php echo $this->_tpl_vars['lang']['nagl5']; ?>
 <?php echo $this->_tpl_vars['lang']['show']; ?>
 <span class="glyphicon glyphicon-chevron-down"></span></a>
			<a class="zwin_rozwin" id="search_hidden" style="display:none;" onclick="javascript: panel_product_list_obj.hidden_search();"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
 <?php echo $this->_tpl_vars['lang']['hidden']; ?>
 <span class="glyphicon glyphicon-chevron-up"></span></a>
		</div>
		<div class="col-sm-6 text-right">
			<?php if ($this->_tpl_vars['search']['cat'] || $this->_tpl_vars['search']['number'] || $this->_tpl_vars['search']['pkwiu'] || $this->_tpl_vars['search']['prize_min'] || $this->_tpl_vars['search']['prize_max']): ?><script type="text/javascript">panel_product_list_obj.show_search();</script><?php endif; ?>
			<input type="button" value="<?php echo $this->_tpl_vars['lang']['search']; ?>
" id="wyszukaj_faktury" onclick="javascript: panel_product_list_obj.search();" class="btn orangeButton" />
			<br>
		</div>
	</div>
</div>

<div class="text-right"> 
	<div class="btn-group alphabet">
		<?php if (! $_GET['par5']): ?>
			<a href="panel/product/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
"  class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
		<?php else: ?>
			<a href="panel/product/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
"  class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
		<?php endif; ?>
	<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
		<?php if ($this->_tpl_vars['i'] == $_GET['par5']): ?><a href="panel/product/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
		<?php else: ?><a href="panel/product/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
"  class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	</div>
</div>

<table class="table table-striped table-hover table-bordered lista">
	<tr>
		<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</th>
		<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl11']; ?>
</th>
	</tr>
<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<tr	<?php if (!(( ($this->_foreach['a']['iteration']-1)+1 ) % 10)): ?>class="dziesiaty"<?php endif; ?>>


	<td class="text-center"><a href="panel/product/list/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
"><span class="glyphicon glyphicon-pencil"></span></a></td>
	<td><a href="panel/product/list/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><span class="glyphicon glyphicon-eye-open"></span>&nbsp; <?php echo $this->_tpl_vars['i']->name; ?>
</a></td>
	<td><a href="panel/product/list/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><?php echo $this->_tpl_vars['i']->cat_n; ?>
</a></td>
	<td><a href="panel/product/list/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><?php echo $this->_tpl_vars['i']->brutto; ?>
 zł</a></td>
	<td class="text-center"><a href="panel/product/list/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/product/list/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="row_hidden">
	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
</div>

<?php elseif ($this->_tpl_vars['action'] == 'cat'): ?>
	<div id="wyszukiwarka_faktur" class="ramka">
		<div class="row">
			<div class="col-sm-4">
				<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
 wg kategorii</h3>
			</div>
			<div class="col-sm-3 text-right">
				<a href="panel/product/list/" style="font-size:13px;margin-top:12px;display:inline-block;" title="<?php echo $this->_tpl_vars['lang']['view_list']; ?>
"><span class="glyphicon glyphicon-align-justify"></span>&nbsp;&nbsp;<?php echo $this->_tpl_vars['lang']['view_list']; ?>
</a><?php if ($this->_tpl_vars['cat_n']): ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="panel/product/add/<?php echo $this->_tpl_vars['cat_n']; ?>
/" style="font-size:13px;margin-top:12px;display:inline-block;" title="<?php echo $this->_tpl_vars['lang']['add']; ?>
"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<?php echo $this->_tpl_vars['lang']['add']; ?>
</a><?php endif; ?>
			</div>
		</div>
		
		<?php if ($this->_tpl_vars['cat']->id): ?>
			<?php echo $this->_tpl_vars['lang']['head_cat']; ?>
 - <?php echo $this->_tpl_vars['cat']->name; ?>
 | 
		<?php else: ?>
			<span class="naglowek"><?php echo $this->_tpl_vars['lang']['head_cat']; ?>
:</span>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['cat_n']): ?><a href="panel/product/list/cat/<?php echo $this->_tpl_vars['cat']->parent; ?>
/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><span class="glyphicon glyphicon-arrow-up"></span>&nbsp;&nbsp;Powrót do wyższej kategorii</a><?php endif; ?>

		<div id="produkty_w_panelu">
			<?php $_from = $this->_tpl_vars['cats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<a href="panel/product/list/cat/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['enter']; ?>
"><strong><?php echo $this->_tpl_vars['i']->name; ?>
</strong> [ produków <?php echo $this->_tpl_vars['i']->counter; ?>
]</a>
			<?php endforeach; endif; unset($_from); ?>
		</div>

				<?php if (! $this->_tpl_vars['dane']): ?>
			<div class="row"><div class="col-sm-12"><br><h4>Brak produktów w tej kategorii</h4><br></div></div>
		<?php else: ?>
		<div class="row">
			<div class="col-sm-7">
				<br>
				<table class="table table-striped table-hover table-bordered lista">
					<tr>
						<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
						<th><?php echo $this->_tpl_vars['lang']['nagl12']; ?>
</th>
						<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl11']; ?>
</th>
					</tr>
					<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
					<tr>
						<td class="text-center"><a href="panel/product/list/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="panel/product/list/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><span class="glyphicon glyphicon-eye-open"></span> &nbsp;&nbsp;<?php echo $this->_tpl_vars['i']->name; ?>
</a></td>
						<td class="text-center"><a href="panel/product/list/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
					<?php endforeach; endif; unset($_from); ?>
				</table>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/product/list/cat/".($this->_tpl_vars['cat_n'])."/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="row_hidden">
		<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
	</div>
	
<?php elseif ($this->_tpl_vars['action'] == 'view'): ?><div id="invoice_top">
<span><?php echo $this->_tpl_vars['lang']['head']; ?>
 <a class="pomoc"><span>lorem</span></a></span>
<div id="navibar">

<?php echo $this->_tpl_vars['lang']['nagl2']; ?>
 <a href="" title="<?php echo $this->_tpl_vars['lang']['nagl1']; ?>
"><?php echo $this->_tpl_vars['lang']['nagl1']; ?>
</a>
<?php $_from = $this->_tpl_vars['navi']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><a href="<?php echo $this->_tpl_vars['i']->url; ?>
" title="<?php echo $this->_tpl_vars['i']->name; ?>
"><?php echo $this->_tpl_vars['i']->name; ?>
</a><?php if ($this->_tpl_vars['k'] != count ( $this->_tpl_vars['navi'] ) -1): ?> / <?php endif; ?><?php endforeach; endif; unset($_from); ?>

</div>
</div>
<div id="dane_kontrahenta">
<span class="naglowek"><?php echo $this->_tpl_vars['lang']['head_view']; ?>
 - <?php echo $this->_tpl_vars['dane']->name; ?>
 | 
<a href="panel/product/list/edit/<?php echo $this->_tpl_vars['dane']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
" style="font-size:12px;"><?php echo $this->_tpl_vars['lang']['edit']; ?>
</a>
 | <a href="panel/product/list/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"  style="font-size:12px;"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></span>

<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['cat']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><a href="panel/product/list/cat/<?php echo $this->_tpl_vars['cat']->id; ?>
/" title="<?php echo $this->_tpl_vars['cat']->name; ?>
"><?php echo $this->_tpl_vars['cat']->name; ?>
</a></div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['name']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->name; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['number']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->number; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->pkwiu; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['unit']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->unit; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['vat']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->vat; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['netto']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->netto; ?>
</div>
</div>
<div class="row_auto" style="font-size: 14px;clear:both;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['brutto']; ?>
:</div>
	<div class="insert_right" style="font-weight:bold;width:270px;"><?php echo $this->_tpl_vars['dane']->brutto; ?>
</div>
</div>
</div>

<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>

	<div class="ramka">
		<h3><?php echo $this->_tpl_vars['lang']['head_edit']; ?>
 - <?php echo $this->_tpl_vars['dane']->name; ?>
</h3>
				<form action="" onsubmit="javascript: return panel_product_list_obj.submit(<?php echo $this->_tpl_vars['dane']->id; ?>
);" id="product_list_form">
			<div class="row">
				<div class="col-sm-6">
					<div>
						<label><?php echo $this->_tpl_vars['lang']['name']; ?>
<span style="color:#f00">*</span>:</label>
						<textarea name="name" id="name"  class="form-control"> <?php echo $this->_tpl_vars['dane']->name; ?>
</textarea>
						<div id="name_error" class="alert alert-danger"></div>
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['producer']; ?>
:</label>
						<input type="text" name="producer" id="producer" value="<?php echo $this->_tpl_vars['dane']->producer; ?>
" class="form-control" />
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['cat']; ?>
<span style="color:#f00">*</span>:</label>
						<select name="cat" id="cat" class="form-control">
							<option value="">---</option>
							<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "panel/product_list/standard_list.html", 'smarty_include_vars' => array('tree' => $this->_tpl_vars['tree'],'select' => $this->_tpl_vars['dane']->cat)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
						</select>
						<div id="cat_error" class="alert alert-danger"></div>	
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['number']; ?>
:</label>
						<input type="text" name="number" id="number" value="<?php echo $this->_tpl_vars['dane']->number; ?>
" class="form-control" />
					</div>
				</div>
				<div class="col-sm-6">
					<div>
						<label><?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
:</label>
						<input type="text" name="pkwiu" id="pkwiu" value="<?php echo $this->_tpl_vars['dane']->pkwiu; ?>
" class="form-control" />
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['unit']; ?>
:</label>
						<select name="unit" id="unit" class="form-control">
						<option value="">---</option>
						<?php $_from = $this->_tpl_vars['units']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if ($this->_tpl_vars['dane']->unit == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
 [<?php echo $this->_tpl_vars['k']; ?>
]</option><?php endforeach; endif; unset($_from); ?>
						</select>
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['vat']; ?>
<span style="color:#f00">*</span>:</label>
						<select name="vat" id="vat" class="form-control" onchange="javascript: panel_product_list_obj.change_netto();">
							<option value="">---</option>
							<option value="0" <?php if ($this->_tpl_vars['dane']->vat == 0): ?>selected="selected"<?php endif; ?>>0 %</option>
							<option value="3" <?php if ($this->_tpl_vars['dane']->vat == 3): ?>selected="selected"<?php endif; ?>>3 %</option>
							<option value="4" <?php if ($this->_tpl_vars['dane']->vat == 4): ?>selected="selected"<?php endif; ?>>4 %</option>
							<option value="5" <?php if ($this->_tpl_vars['dane']->vat == 5): ?>selected="selected"<?php endif; ?>>5 %</option>
							<option value="6" <?php if ($this->_tpl_vars['dane']->vat == 6): ?>selected="selected"<?php endif; ?>>6 %</option>
							<option value="7" <?php if ($this->_tpl_vars['dane']->vat == 7): ?>selected="selected"<?php endif; ?>>7 %</option>
							<option value="8" <?php if ($this->_tpl_vars['dane']->vat == 8): ?>selected="selected"<?php endif; ?>>8 %</option>
							<option value="18" <?php if ($this->_tpl_vars['dane']->vat == 18): ?>selected="selected"<?php endif; ?>>18 %</option>
							<option value="19" <?php if ($this->_tpl_vars['dane']->vat == 19): ?>selected="selected"<?php endif; ?>>19 %</option>
							<option value="20" <?php if ($this->_tpl_vars['dane']->vat == 20): ?>selected="selected"<?php endif; ?>>20 %</option>
							<option value="22" <?php if ($this->_tpl_vars['dane']->vat == 22): ?>selected="selected"<?php endif; ?>>22 %</option> 
							<option value="23" <?php if ($this->_tpl_vars['dane']->vat == 23): ?>selected="selected"<?php endif; ?>>23 %</option>
							<option value="NP" <?php if ($this->_tpl_vars['dane']->vat == 'NP'): ?>selected="selected"<?php endif; ?>>NP</option>
							<option value="ZW" <?php if ($this->_tpl_vars['dane']->vat == 'ZW'): ?>selected="selected"<?php endif; ?>>ZW</option>
							<option value="-" <?php if ($this->_tpl_vars['dane']->vat == "-"): ?>selected="selected"<?php endif; ?>>-</option>
						</select>
						<div id="vat_error" class="dane_error"></div>
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['netto']; ?>
<span style="color:#f00">*</span>:</label>
						<input type="text" name="netto" id="netto" value="<?php echo $this->_tpl_vars['dane']->netto; ?>
" class="form-control" onkeypress="javascript: return panel_product_list_obj.insert_prize(event,'netto');" onkeyup="javascript: panel_product_list_obj.change_netto();" />
						<div id="cena_error" class="dane_error"></div>
					</div>
					<div>
						<label><?php echo $this->_tpl_vars['lang']['brutto']; ?>
<span style="color:#f00">*</span>:</label>
						<input type="text" name="brutto" id="brutto" value="<?php echo $this->_tpl_vars['dane']->brutto; ?>
" class="form-control" onkeypress="javascript: return panel_product_list_obj.insert_prize(event,'brutto');" onkeyup="javascript: panel_product_list_obj.change_brutto();" />
					</div><br>
										<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn orangeButton" />&nbsp;    <span style="color:#f00">*</span> <?php echo $this->_tpl_vars['lang']['info']; ?>
<br><br><br>
				</div>
			</div>
		</form>
	</div>
		<div class="row_hidden">
		<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
		<input type="hidden" id="empty_cat" value="<?php echo $this->_tpl_vars['lang']['empty_cat']; ?>
" />
		<input type="hidden" id="empty_name" value="<?php echo $this->_tpl_vars['lang']['empty_name']; ?>
" />
		<input type="hidden" id="empty_vat" value="<?php echo $this->_tpl_vars['lang']['empty_vat']; ?>
" />
		<input type="hidden" id="empty_prize" value="<?php echo $this->_tpl_vars['lang']['empty_prize']; ?>
" />
		<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
		<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	</div>

<?php endif; ?>