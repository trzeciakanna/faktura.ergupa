<?php /* Smarty version 2.6.19, created on 2015-02-27 10:36:45
         compiled from panel/main/standard.html */ ?>
<div id="panel_main" class="row">
	<div class="col-sm-6">
		<div class="box_left box_wiekszy ramka">
			<h3><i class="ico2 panel_main_nagl"></i> <?php echo $this->_tpl_vars['lang']['data_main']; ?>
</h3>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/data/general/"><i class="ico2 data_general_ico"></i> <?php echo $this->_tpl_vars['lang']['data_general']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['data_general_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/data/log/"><i class="ico2 data_log_ico"></i> <?php echo $this->_tpl_vars['lang']['data_log']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['data_log_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/data/licence/"><i class="ico2 data_licence_ico"></i> <?php echo $this->_tpl_vars['lang']['data_licence']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['data_licence_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/data/number/"><i class="ico2 data_number_ico"></i> <?php echo $this->_tpl_vars['lang']['data_number']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['data_number_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/data/extra/"><i class="ico2 data_extra_ico"></i> <?php echo $this->_tpl_vars['lang']['data_extra']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['data_extra_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/szablon/szablon-faktury/"><i class="ico2 data_extra_ico"></i>Szablony faktur</a><span>Tutaj wybierzesz jeden z trzech szablonów faktury oraz dostosujesz ich kolorystykę</span></div>
			</div>
		</div>
		
		<div class="box_left ramka">
			<h3><i class="ico2 panel_main_nagl"></i> <?php echo $this->_tpl_vars['lang']['product_main']; ?>
</h3>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/product/add/"><i class="ico2 product_add_ico"></i> <?php echo $this->_tpl_vars['lang']['product_add']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['product_add_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/product/list/"><i class="ico2 product_list_ico"></i> <?php echo $this->_tpl_vars['lang']['product_list']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['product_list_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6" style="width:100%"><a href="panel/product/cat/"><i class="ico2 product_cat_ico"></i> <?php echo $this->_tpl_vars['lang']['product_cat']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['product_list_info']; ?>
</span></div>
			</div>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="box_right box_wiekszy ramka">
			<h3><i class="ico2 panel_main_nagl"></i> <?php echo $this->_tpl_vars['lang']['invoice_main']; ?>
</h3>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="invoice/"><i class="ico2 invoice_add_ico"></i> <?php echo $this->_tpl_vars['lang']['invoice_add']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['invoice_add_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/invoice/search/"><i class="ico2 invoice_list2_ico"></i> <?php echo $this->_tpl_vars['lang']['invoice_list2']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['invoice_list2_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/invoice/period/"><i class="ico2 invoice_period_ico"></i> <?php echo $this->_tpl_vars['lang']['invoice_period']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['invoice_period_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/invoice/org/"><i class="ico2 invoice_org_ico"></i> <?php echo $this->_tpl_vars['lang']['invoice_org']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['invoice_org_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6" style="width:100%;"><a href="panel/invoice/list/"><i class="ico2 invoice_list_ico"></i> <?php echo $this->_tpl_vars['lang']['invoice_list']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['invoice_list_info']; ?>
</span></div>	
			</div>
		</div>

		<div class="box_right ramka">
			<h3><i class="ico2 panel_main_nagl"></i> <?php echo $this->_tpl_vars['lang']['client_main']; ?>
</h3>
			<div class="row">
				<div class="funkcja col-sm-6"><a href="panel/client/add/"><i class="ico2 client_add_ico"></i> <?php echo $this->_tpl_vars['lang']['client_add']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['client_add_info']; ?>
</span></div>
				<div class="funkcja col-sm-6"><a href="panel/client/list/"><i class="ico2 client_list_ico"></i> <?php echo $this->_tpl_vars['lang']['client_list']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['client_list_info']; ?>
</span></div>
			</div>
			<div class="row">
				<div class="funkcja col-sm-6" style="width:100%"><a href="panel/client/send/"><i class="ico2 client_send_ico"></i> <?php echo $this->_tpl_vars['lang']['client_send']; ?>
</a><span><?php echo $this->_tpl_vars['lang']['client_send_info']; ?>
</span></div>
			</div>
		</div>
	</div>
</div>