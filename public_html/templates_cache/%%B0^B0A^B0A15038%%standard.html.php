<?php /* Smarty version 2.6.19, created on 2015-02-27 10:34:09
         compiled from contact_send/standard.html */ ?>
<script type="text/javascript" src="module/contact_send/class.js"></script>
<div class="row_head" style="  color: #FF7E00;font-size: 18px;font-weight: bold;"><?php echo $this->_tpl_vars['dane']['head']; ?>
</div>
<div style="display:none;">
<div id="zapytanie_schowane">
<form action="" id="contact_send_form" class="form-horizontal">
	<div id="zapytanie_send_ok" class="col-sm-12 alert alert-success"></div>
	<div id="zapytanie_send_error" class="col-sm-12 alert alert-danger"></div>
	<div id="name_error" class="col-sm-12 alert alert-danger"></div>
	<div class="row">
		<div class="col-sm-6">
			<select name="account" title="<?php echo $this->_tpl_vars['lang']['type']; ?>
" class="form-control">
			<option value="0"> <?php echo $this->_tpl_vars['lang']['type']; ?>
 </option>
			<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']['id']; ?>
"><?php echo $this->_tpl_vars['i']['name']; ?>
</option><?php endforeach; endif; unset($_from); ?>
			</select>
		</div>
		<div class="col-sm-6">
			<input type="text" name="name" id="name" placeholder="<?php echo $this->_tpl_vars['lang']['name']; ?>
" class="form-control" />
		</div>
	</div>
	<div id="phone_error" class="col-sm-12 alert alert-danger"></div>
	<div class="row">	
		<div class="col-sm-6">
			<input type="text" name="phone" id="phone" title="<?php echo $this->_tpl_vars['lang']['phone']; ?>
" placeholder="<?php echo $this->_tpl_vars['lang']['phone']; ?>
" class="form-control" />
		</div>
		<div class="col-sm-6">
			<input type="text" name="mail" id="mail"  placeholder="<?php echo $this->_tpl_vars['lang']['mail']; ?>
" title="<?php echo $this->_tpl_vars['lang']['mail']; ?>
" class="form-control" />
		</div>
	</div>	
	<div id="text_error" class="alert alert-danger"></div>
	<div class="row">

		<div class="col-sm-12">
			<textarea name="text" id="text" cols="" rows="6" title="<?php echo $this->_tpl_vars['lang']['text']; ?>
" placeholder="<?php echo $this->_tpl_vars['lang']['text']; ?>
" class="form-control" ></textarea>
		</div>
	</div>	
	

<?php if ($this->_tpl_vars['captcha']): ?><?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "module/captcha/module.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
<?php endif; ?>
	<br>
	<input type="button" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
"  class="btn orangeButton" onclick="javascript: contact_send_add.submit();" />


</form>

<div class="row_hidden">
	<input type="hidden" id="send_ok" value="<?php echo $this->_tpl_vars['lang']['send_ok']; ?>
" />
	<input type="hidden" id="send_error" value="<?php echo $this->_tpl_vars['lang']['send_error']; ?>
" />
	<input type="hidden" id="send_errorc" value="<?php echo $this->_tpl_vars['lang']['send_errorc']; ?>
" />
	<input type="hidden" id="empty_name" value="<?php echo $this->_tpl_vars['lang']['empty_name']; ?>
" />
	<input type="hidden" id="empty_pm" value="<?php echo $this->_tpl_vars['lang']['empty_pm']; ?>
" />
	<input type="hidden" id="empty_text" value="<?php echo $this->_tpl_vars['lang']['empty_text']; ?>
" />
	<input type="hidden" id="empty_captcha" value="<?php echo $this->_tpl_vars['lang']['empty_captcha']; ?>
" />

</div>
</div>
</div>

<script type="text/javascript" >$('miejsce_formularza').appendChild($('zapytanie_schowane'));</script>