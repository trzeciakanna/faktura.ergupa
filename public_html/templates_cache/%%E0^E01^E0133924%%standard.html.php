<?php /* Smarty version 2.6.19, created on 2018-03-30 14:29:26
         compiled from panel/jpk/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlentities', 'panel/jpk/standard.html', 144, false),)), $this); ?>
<script type="text/javascript" src="module/panel/jpk/class.js"></script>
<script type="text/javascript" src="jscript/valid_data.js"></script>
<script type="text/javascript" src="jscript/calendar.js"></script>
<?php if ($this->_tpl_vars['action'] == 'add'): ?>
  <div class="row">
		<div class="col-sm-12"><h3><?php echo $this->_tpl_vars['lang']['headerGen']; ?>
_FA &nbsp;&nbsp;<a href="panel/jpk/list/" class="btn blueButton" ><?php echo $this->_tpl_vars['lang']['headerList']; ?>
_FA</a></h3></div>
  </div><br /><br />
  <div id="generate_error" class="alert alert-success"></div>
	<div id="error_error" class="alert alert-danger"></div>
	<div class="row">
		<div class="col-sm-7">
		<form method="post" id="jpkForm" onsubmit="return jpk_obj.generate()">
			<div class="col-sm-12">
				<div id="ok_error"  class="alert alert-success" ></div>
				<div id="error_error" class="alert alert-danger"></div>
			</div>
			<div class="row form-horizontal dane_osobiste">
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['dateBegin']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<script type="text/javascript">calendar.create('dateBegin','<?php echo $this->_tpl_vars['dateBegin']; ?>
');</script>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['dateEnd']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<script type="text/javascript">calendar.create('dateEnd','<?php echo $this->_tpl_vars['dateEnd']; ?>
');</script>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['currency']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<select  name="currency"><option value="PLN">PLN</option><option value="EUR">EUR</option><option value="USD">USD</option><option value="GBP">GBP</option><option value="AUD">AUD</option><option value="CZK">CZK</option><option value="DKK">DKK</option><option value="EEK">EEK</option><option value="HKD">HKD</option><option value="JPY">JPY</option><option value="CAD">CAD</option><option value="LTL">LTL</option><option value="LVL">LVL</option><option value="NOK">NOK</option><option value="ZAR">ZAR</option><option value="RUB">RUB</option><option value="CHF">CHF</option><option value="SEK">SEK</option><option value="UAH">UAH</option><option value="HUF">HUF</option><option value="XDR">XDR</option><option value="BGN">BGN</option><option value="RON">RON</option><option value="PHP">PHP</option><option value="THB">THB</option><option value="NZD">NZD</option><option value="SGD">SGD</option><option value="ISK">ISK</option><option value="HRK">HRK</option><option value="TRY">TRY</option><option value="MXN">MXN</option><option value="BRL">BRL</option><option value="MYR">MYR</option><option value="IDR">IDR</option><option value="KRW">KRW</option><option value="CNY">CNY</option><option value="ILS">ILS</option><option value="INR">INR</option><option value="CLP">CLP</option></select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['destination']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="radio" name="destination" value="0" id="destinationFirst" checked="checked" /> <label for="destinationFirst" ><?php echo $this->_tpl_vars['lang']['destinationFirst']; ?>
</label><br>
						<input type="radio" name="destination" value="1" id="destinationCorrect" /> <label for="destinationCorrect" ><?php echo $this->_tpl_vars['lang']['destinationCorrect']; ?>
</label>  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['KodUrzedu']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="KodUrzedu" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['KodKraju']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="KodKraju" value="PL" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Wojewodztwo']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="Wojewodztwo" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Powiat']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="Powiat" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Gmina']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="Gmina" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Ulica']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="Ulica" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['NrDomu']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="NrDomu" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['NrLokalu']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="NrLokalu" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Miejscowosc']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="Miejscowosc" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['KodPocztowy']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="KodPocztowy" value="" class="tresc" />  
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['Poczta']; ?>
<em>*</em>:</label>
					<div class="col-sm-8">
						<input type="text" name="Poczta" value="" class="tresc" />  
					</div>
				</div>
			</div>
			<div class="row text-right">				
				<br>
				<button type="submit" class="btn orangeButton"><?php echo $this->_tpl_vars['lang']['gen']; ?>
 i pobierz</button>
				<br>
				<br>			
			</div>
		</form>
		<div class="row_hidden">
			<input type="hidden" id="okValid" value="<?php echo $this->_tpl_vars['lang']['okValid']; ?>
" />
			<input type="hidden" id="okInvalid" value="<?php echo $this->_tpl_vars['lang']['okInvalid']; ?>
" />
			<input type="hidden" id="jpkError" value="<?php echo $this->_tpl_vars['lang']['jpkError']; ?>
" />
		</div>
	</div>
	<div class="col-sm-5">
		<br /><br />
		<h4>Instrukcja generowania JPK_FA:</h4>
		<ol>
			<li>Podaj datę początkową oraz końcową. </li>
			<li>Wybierz walutę dla jakiej generujesz JPK_FA (dla każdej waluty należy wygenerować osobny JPK_FA).</li>
			<li>Podaj cel złożenia.</li>
			<li>Wypełnij wszystkie wymagane pola (oznaczone *).</li>
			<li>Kliknij przycisk "Generuj i pobierz".</li>
		</ol>
	</div>
</div>
  
<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>
  <div class="row">
		<div class="col-sm-12"><h3><?php echo $this->_tpl_vars['lang']['headerEdit']; ?>
 &nbsp;&nbsp;<a href="panel/jpk/list/" class="btn blueButton" ><?php echo $this->_tpl_vars['lang']['headerList']; ?>
</a></h3></div>
	</div>
	<p>Pola z błędami zaznaczone są na żółto.</p>
  <div id="generate_error" class="alert alert-success"></div>
	<div id="error_error" class="alert alert-danger"></div>
  <script src="jscript/ace/ace.js"></script>
  <pre id="jpkEditor" style="width: 1600px; height: 800px;"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']->xml_code)) ? $this->_run_mod_handler('htmlentities', true, $_tmp) : htmlentities($_tmp)); ?>
</pre>
  <script type="text/javascript">
  	var editor = ace.edit("jpkEditor");
      editor.setTheme("ace/theme/eclipse");
      editor.getSession().setMode("ace/mode/xml");
  </script>
  <div id="jpkErrorList"></div>
  <script type="text/javascript">jpk_obj.showErrors('<?php echo $this->_tpl_vars['data']->errors; ?>
')</script>
  
  <form method="post" id="jpkForm" onsubmit="return jpk_obj.saveFile(<?php echo $this->_tpl_vars['data']->id; ?>
)">
    <textarea name="xml" id="jpkXML" style="display: none"></textarea>
    <button type="submit" class="btn orangeButton"><?php echo $this->_tpl_vars['lang']['save']; ?>
</button>
  </form>
  <div class="row_hidden">
		<input type="hidden" id="okValid" value="<?php echo $this->_tpl_vars['lang']['okValid']; ?>
" />
		<input type="hidden" id="okInvalid" value="<?php echo $this->_tpl_vars['lang']['okInvalid']; ?>
" />
		<input type="hidden" id="jpkError" value="<?php echo $this->_tpl_vars['lang']['jpkError']; ?>
" />
	</div>
  
<?php elseif ($this->_tpl_vars['action'] == 'list'): ?>
  <div class="row">
		<div class="col-sm-12"><h3><?php echo $this->_tpl_vars['lang']['headerList']; ?>
_FA &nbsp;&nbsp;<a href="panel/jpk/add/" class="btn blueButton" ><?php echo $this->_tpl_vars['lang']['headerGen']; ?>
_FA</a></h3></div>
  </div>
  <div id="ok_error" class="alert alert-success"></div>
	<div id="error_error" class="alert alert-danger"></div>
  <table class="table table-striped table-hover table-bordered lista">
    <tr>
      <th><?php echo $this->_tpl_vars['lang']['dateCreate']; ?>
</th> 
      <th><?php echo $this->_tpl_vars['lang']['dateBegin']; ?>
</th> 
      <th><?php echo $this->_tpl_vars['lang']['dateEnd']; ?>
</th> 
      <th><?php echo $this->_tpl_vars['lang']['destination']; ?>
</th>
      <th> Rodzaj pliku</th> 
      <th><?php echo $this->_tpl_vars['lang']['isValid']; ?>
</th>
      <th><?php echo $this->_tpl_vars['lang']['isSend']; ?>
</th> 
			<th>Wyślij</th>
			<th>Edycja</th>
    </tr>
    <?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
    <tr>
      <td><a href="panel/jpk/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="edit"><strong><?php echo $this->_tpl_vars['i']->date_create; ?>
</strong></a></td> 
      <td><?php echo $this->_tpl_vars['i']->date_begin; ?>
</td> 
      <td><?php echo $this->_tpl_vars['i']->date_end; ?>
</td> 
      <td><?php if ($this->_tpl_vars['i']->destination == 0): ?><?php echo $this->_tpl_vars['lang']['destinationFirst']; ?>
<?php else: ?><?php echo $this->_tpl_vars['lang']['destinationCorrect']; ?>
 <?php echo $this->_tpl_vars['i']->destination; ?>
<?php endif; ?></td>
      <td><?php if ($this->_tpl_vars['i']->kind == 1): ?>JPK_FA v1<?php else: ?>JPK<?php endif; ?></td> 
      <td><?php if ($this->_tpl_vars['i']->is_valid): ?><?php echo $this->_tpl_vars['lang']['valid']; ?>
<?php else: ?><?php echo $this->_tpl_vars['lang']['invalid']; ?>
 <a href="#" onclick="return jpk_obj.showErrorsPop(this)" data-errors='<?php echo $this->_tpl_vars['i']->errors; ?>
' title="<?php echo $this->_tpl_vars['lang']['errors']; ?>
"><?php echo $this->_tpl_vars['lang']['errors']; ?>
</a><?php endif; ?></td>
      <td><?php if ($this->_tpl_vars['i']->is_send): ?><?php echo $this->_tpl_vars['lang']['sended']; ?>

      <?php $_from = $this->_tpl_vars['i']->send_info; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['s']):
?><br/><?php echo $this->_tpl_vars['s']['date']; ?>
: <?php echo $this->_tpl_vars['s']['mail']; ?>
<?php endforeach; endif; unset($_from); ?>
      <?php else: ?><?php echo $this->_tpl_vars['lang']['notSend']; ?>
<?php endif; ?></td> 
			<td><a href="#" onclick="return jpk_obj.send(<?php echo $this->_tpl_vars['i']->id; ?>
)" title="Wyślij" class="btn btn-xs btn-info">Wyślij</a></td>
      <td><a href="panel/jpk/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="Edytuj" class="btn btn-xs btn-info">Edytuj</a></td>			
    </tr>
    <?php endforeach; endif; unset($_from); ?>
  </table>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/jpk/list/",'surfix' => "")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="invoice_list_mail" id="panel_error" style="position:fixed">
    <a onclick="return jpk_obj.close();" title="<?php echo $this->_tpl_vars['lang']['close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
    <div id="errorsList"></div>
	</div>
  
  <div class="invoice_list_mail" id="panel_mail" style="position:fixed">
	  	
		<h4 style="font-weight:bold;"><?php echo $this->_tpl_vars['lang']['headerMail']; ?>
 <a class="close" onclick="return jpk_obj.close();" title="<?php echo $this->_tpl_vars['lang']['close']; ?>
"><span class="glyphicon glyphicon-remove"></span></a></h4>		
  	<form action="" id="jpkSend" onsubmit="javascript: return jpk_obj.sendEnd();">
  		<div class="col-sm-6">  			
  			<div class="row_auto">
  				<div><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</div>
  				<input type="text" name="mail" class="form-control" />
  			</div>
  		</div>
      <div class="col-sm-6"> 
        <div class="row_auto">
  				<div><?php echo $this->_tpl_vars['lang']['sendSubject']; ?>
:</div>
  				<input type="text" name="subject" class="form-control" value="<?php echo $this->_tpl_vars['lang']['sendSubjectValue']; ?>
" />
  			</div>        
			</div>
			<div class="col-sm-12">
				<div class="row_auto">
					<div><?php echo $this->_tpl_vars['lang']['sendText']; ?>
:</div>
					<textarea name="text" class="form-control" style="min-height:200px"><?php echo $this->_tpl_vars['lang']['sendTextValue']; ?>
</textarea>
				</div>
			</div>
  		<div class="col-sm-12 text-center">
  			<br>
  			<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" class="btn orangeButton" />
  			<br>
  			<br>
  		</div>
  		<div class="row_hidden">
  			<input type="hidden" name="idJpk" id="idJpk" />
  		</div>
  	</form>
  </div>
  <div class="row_hidden">
		<input type="hidden" id="okSend" value="<?php echo $this->_tpl_vars['lang']['okSend']; ?>
" />
		<input type="hidden" id="errMail" value="<?php echo $this->_tpl_vars['lang']['errMail']; ?>
" />
		<input type="hidden" id="errId" value="<?php echo $this->_tpl_vars['lang']['errId']; ?>
" />
	</div>
<?php endif; ?>