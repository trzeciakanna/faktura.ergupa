<?php /* Smarty version 2.6.19, created on 2015-02-28 13:59:19
         compiled from panel/data_extra/standard.html */ ?>
<div class="ramka">
	<div class="row">
		<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
 </h3>
		<script type="text/javascript" src="module/panel/data_extra/class.js"></script>
		<form action="" onsubmit="javascript: return panel_data_extra_obj.submit(<?php echo $this->_tpl_vars['user_id']; ?>
);" id="data_extra_save" class="form-horizontal">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="col-sm-5"><?php echo $this->_tpl_vars['lang']['eheader']; ?>
<br><span class="small"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</span></label>
					<div class="col-sm-7">
						<textarea name="head" cols="" rows="3" class="form-control"><?php echo $this->_tpl_vars['dane']->head; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5"><?php echo $this->_tpl_vars['lang']['efoot']; ?>
<br><span class="small"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</span></label>
					<div class="col-sm-7">
						<textarea name="foot" cols="" rows="3" class="form-control"><?php echo $this->_tpl_vars['dane']->foot; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5"><?php echo $this->_tpl_vars['lang']['einfo']; ?>
<br><span class="small"><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</span></label>
					<div class="col-sm-7">
						<textarea name="info" cols="" rows="3" class="form-control"><?php echo $this->_tpl_vars['dane']->info; ?>
</textarea>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="col-sm-12">
					<label class="col-sm-5"><?php echo $this->_tpl_vars['lang']['elogo']; ?>
<br><span class="small"> <?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</span></label>
					<div class="col-sm-7">
						<?php $this->assign('uploader_type', 'invoice_logo'); ?>
						<?php $this->assign('uploader_what', 'img'); ?>
						<?php $this->assign('uploader_file', $this->_tpl_vars['dane']->logo); ?>
						<?php $this->assign('uploader_name', 'logo'); ?>
						<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "module/uploader/module.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

					</div>
				</div>
				<div class="col-sm-12">
					<hr>
				</div>
				<div class="col-sm-12">
					<label class="col-sm-5">Podpis/Pieczątka<br><span class="small"><?php echo $this->_tpl_vars['lang']['nagl4']; ?>
</span></label>
					<div class="col-sm-7">
						<?php $this->assign('uploader_type', 'invoice_sign'); ?>
						<?php $this->assign('uploader_what', 'img'); ?>
						<?php $this->assign('uploader_file', $this->_tpl_vars['dane']->sign); ?>
						<?php $this->assign('uploader_name', 'sign'); ?>
						<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "module/uploader/module.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

					</div>
				</div>
			</div>
						<div class="col-sm-12 text-center"> 
				<br>
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
				<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="btn btn-primary" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn orangeButton" />
				<br><br><Br>
			</div>
		</form>
	</div>



					<div class="row_hidden">
		<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
		<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	</div>
</div>