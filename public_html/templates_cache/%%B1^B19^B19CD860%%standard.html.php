<?php /* Smarty version 2.6.19, created on 2015-02-28 13:59:33
         compiled from panel/data_log/standard.html */ ?>
<div class="ramka">
	<script type="text/javascript" src="module/panel/data_log/class.js"></script>
	<script type="text/javascript" src="jscript/valid_data.js"></script>
	<form action="" onsubmit="javascript: return panel_data_gen_obj.submit();" id="data_gen_save">

		<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>

		<div class="row">
			<div class="col-sm-5">
				
				<div id="save_error_error" class="alert alert-danger"></div>
				<div id="dane_bledy_error" class="alert alert-danger"></div>
				<div id="save_ok_error" class="alert alert-success"></div>
				<div class="form-group" class="text-right">
					<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
					<span style="font-size:11px"> <span style="color:#f00;">*</span> <?php echo $this->_tpl_vars['lang']['info']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **<?php echo $this->_tpl_vars['lang']['change_pass']; ?>
</span>
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['login']; ?>
<span style="color:#f00">*</span></label>
					<input type="text" name="login" id="login" value="<?php echo $this->_tpl_vars['user']->login; ?>
" class="form-control" onchange="javascript: panel_data_gen_obj.check_login();" />
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
**</label>
					<input type="password" name="pass" id="pass" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
 (<?php echo $this->_tpl_vars['lang']['repeat']; ?>
)**</label>
					<input type="password" name="pass_repeat" id="pass_repeat" value="" class="form-control" />
				</div>
				<div class="form-group">
					<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
*</label>
					<input type="text" name="mail" id="mail" value="<?php echo $this->_tpl_vars['user']->mail; ?>
" class="form-control" onchange="javascript: panel_data_gen_obj.check_mail();" />
				</div>
				<div class="form-group text-right">
					<br>
					<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
					<button type="submit" class="btn orangeButton" ><?php echo $this->_tpl_vars['lang']['save']; ?>
</button>
					<br><br><br><br>
				</div>
			</div>
		</div>
	</form>
	<div class="row_hidden">
		<input type="hidden" id="old_login" value="<?php echo $this->_tpl_vars['user']->login; ?>
" />
		<input type="hidden" id="old_mail" value="<?php echo $this->_tpl_vars['user']->mail; ?>
" />
		<input type="hidden" id="reg_login" value="1" />
		<input type="hidden" id="reg_mail" value="1" />
		<input type="hidden" id="bad_login" value="<?php echo $this->_tpl_vars['lang']['bad_login']; ?>
" />
		<input type="hidden" id="bad_mail" value="<?php echo $this->_tpl_vars['lang']['bad_mail']; ?>
" />
		<input type="hidden" id="bad_pass" value="<?php echo $this->_tpl_vars['lang']['bad_pass']; ?>
" />
		<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
		<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	</div>
</div>