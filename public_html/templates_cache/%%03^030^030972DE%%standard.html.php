<?php /* Smarty version 2.6.19, created on 2017-02-06 16:39:35
         compiled from panel/data_licence/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'panel/data_licence/standard.html', 45, false),array('modifier', 'string_format', 'panel/data_licence/standard.html', 60, false),)), $this); ?>
<script type="text/javascript" src="module/panel/data_licence/class.js"></script>
<?php if ($_GET['par4'] == 'ok'): ?><div class="row_info"><?php echo $this->_tpl_vars['lang']['new_ok']; ?>
</div>
<?php elseif ($_GET['par4'] == 'error'): ?><div class="row_error"><?php echo $this->_tpl_vars['lang']['new_error']; ?>
</div><?php endif; ?>
	
<div class="panel_main ramka">
	
	<h3><?php echo $this->_tpl_vars['lang']['nagl42']; ?>
 <a class="pomoc"><span class="glyphicon glyphicon-info-sign"></span><span><?php echo $this->_tpl_vars['lang']['nagl43']; ?>
</span></a></h3>
	
	<?php if ($this->_tpl_vars['end'] == 1): ?><p class="lead"><?php echo ((is_array($_tmp=$this->_tpl_vars['lang']['on_lic'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[date]", ($this->_tpl_vars['user_sub'])) : smarty_modifier_replace($_tmp, "[date]", ($this->_tpl_vars['user_sub']))); ?>
</p>
	<?php else: ?><div class="row_info2" ><?php echo $this->_tpl_vars['lang']['off_lic']; ?>
</div><?php endif; ?>
	<div class="row_info" ><?php echo ((is_array($_tmp=$this->_tpl_vars['lang']['info'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[cash]", ($this->_tpl_vars['cash'])) : smarty_modifier_replace($_tmp, "[cash]", ($this->_tpl_vars['cash']))); ?>
</div>
	<div class="row_info" ><?php echo $this->_tpl_vars['lang']['nagl44']; ?>
</div>


	<hr>
	<div class="row">
		<div class="col-sm-3">
			<label><?php echo $this->_tpl_vars['lang']['time']; ?>
</label>
		</div>
		<div class="col-sm-3">
			<select id="time" class="form-control">
								<option value="3">3 <?php echo $this->_tpl_vars['lang']['month_t']; ?>
 - <?php echo ((is_array($_tmp=34.96)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
zł</option>
												<option value="6">6 <?php echo $this->_tpl_vars['lang']['month_f']; ?>
 - <?php echo ((is_array($_tmp=56.91)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
zł</option>
								<option value="9">9 <?php echo $this->_tpl_vars['lang']['month_f']; ?>
 - <?php echo ((is_array($_tmp=74.8)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
zł</option>
								<option value="12" selected>12 <?php echo $this->_tpl_vars['lang']['month_f']; ?>
 - <?php echo ((is_array($_tmp=86.99)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
zł</option>
			</select>
			<?php echo $this->_tpl_vars['lang']['netto']; ?>

		</div>
		<input type="button" value="<?php echo $this->_tpl_vars['lang']['next']; ?>
" class="btn orangeButton" onclick="javascript: panel_data_lic_obj.submit(<?php echo $this->_tpl_vars['user_id']; ?>
);" />
	</div>
	<hr>
	<br>
			<?php if ($this->_tpl_vars['result'] == 'ok'): ?><div class="row_info"><?php echo $this->_tpl_vars['lang']['end_ok']; ?>
</div>
	<?php elseif ($this->_tpl_vars['result'] == 'exist'): ?><div class="row_info"><?php echo $this->_tpl_vars['lang']['end_exist']; ?>
</div>
	<?php elseif ($this->_tpl_vars['result'] == 'error'): ?><div class="row_info"><?php echo $this->_tpl_vars['lang']['end_error']; ?>
</div>
	<?php endif; ?>
		<h3>Faktura za wykupiony abonament zostanie wysłana na email podany podczas rejestracji w ciągu 48 h w dni robocze.</h3>
	<br><br><br>
</div>


<div class="row_hidden">
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
</div>
<div class="row_hidden" id="data_lic_tmp"></div>
