<?php /* Smarty version 2.6.19, created on 2017-02-10 09:21:37
         compiled from faktura-korygujaca/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'faktura-korygujaca/standard.html', 853, false),)), $this); ?>

<script type="text/javascript" src="module/faktura-korygujaca/class.js"></script>
<script type="text/javascript" src="jscript/valid_data.js"></script>
<script type="text/javascript" src="jscript/calendar.js"></script>
<div id="invoice">
		<?php if (! isset ( $_COOKIE['newApp2'] )): ?>
		<div class="alert_cookies" id="newApp2"><a title="Zamknij" onclick="javascript: setCookie('newApp2','y',30);document.getElementById('newApp2').style.display = 'none';"><span class="glyphicon glyphicon-remove"></span></a> Dostępny jest nowy lepszy, prostszy i łatwiejszy szablon faktury korygującej, aby go wypróbować kliknij w ten link:<br /><a href="https://afaktury.pl/wystaw-fakture-korygujaca,3,7/" title="Faktura korygująca online" class="correctLink">Nowy wzór faktury korygującej</a></div>
	<?php endif; ?>
<form id="faktura">
	<div class="row">
		<div class="col-md-7 col-md-offset-5">
			<ul class="invoiceMenu text-center">
			<?php if ($this->_tpl_vars['user_sub']): ?>
				<li><a onclick="javascript: invoice_obj.save_base();"><span class="icon"><i class="saveDisc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_save']; ?>
</span></a></li>
				<li><a onclick="javascript: invoice_obj.save_pdf();"><span class="icon"><i class="printer"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_pdf']; ?>
</span></a></li>
				<li><a href="invoice/" ><span class="icon"><i class="newDoc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_clean']; ?>
</span></a></li>
				<li><a href="panel/client/list/" title="<?php echo $this->_tpl_vars['lang']['list_client']; ?>
"><span class="icon"><i class="people"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_client']; ?>
</span></a></li>
				<li><a href="panel/product/list/" title="<?php echo $this->_tpl_vars['lang']['prod_head']; ?>
"><span class="icon"><i class="shoppingCart"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['prod_head']; ?>
</span></a></li>
				<li><a href="panel/invoice/search/" title="<?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
"><span class="icon"><i class="docList"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
</span></a></li>
			<?php else: ?>
								<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';"><span class="icon"><i class="printer"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_pdf']; ?>
</span></a></li>
				<li><a href="faktura-pro-forma/"  ><span class="icon"><i class="newDoc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_clean']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['list_client']; ?>
"><span class="icon"><i class="people"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_client']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['prod_head']; ?>
"><span class="icon"><i class="shoppingCart"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['prod_head']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
"><span class="icon"><i class="docList"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
</span></a></li>
			<?php endif; ?>
			</ul>
		</div>
	</div>
	<hr>
		<div class="row form-horizontal dane_osobiste">
		<div class="col-sm-6">
			<div class="form-group">
				<div class="col-xs-1 text-right">
					<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc1']; ?>
</span></a>
				</div>
				<div class="col-xs-11">
					<input type="text" name="lang_head" value="<?php echo $this->_tpl_vars['lang']['head']; ?>
" class="invoice_head opis"  />
				</div>
			</div>
						<div class="form-group">
				<div class="col-xs-2 col-xs-offset-1"><input type="text" name="lang_number" value="<?php echo $this->_tpl_vars['lang']['number']; ?>
" class="invoice_head opis"/></div>
				<div class="col-xs-9">
					<input type="text" name="number" value="<?php if ($this->_tpl_vars['inv']->number): ?><?php echo $this->_tpl_vars['inv']->number; ?>
<?php else: ?><?php echo $this->_tpl_vars['counter']; ?>
<?php endif; ?>" class="tresc" style="line-height:36px;height:36px;font-size:18px;" />
				</div>
			</div>
		</div>

				<div class="col-sm-6">
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_date_create" value="<?php echo $this->_tpl_vars['lang']['date_create']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<script type="text/javascript">calendar.create('date_create','<?php echo $this->_tpl_vars['inv']->date_add; ?>
');</script>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_date_sell" value="<?php echo $this->_tpl_vars['lang']['date_sell']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<script type="text/javascript">calendar.create('date_sell','<?php echo $this->_tpl_vars['inv']->date_sell; ?>
',1);</script>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_date_deadline" value="<?php echo $this->_tpl_vars['lang']['date_deadline']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<script type="text/javascript">calendar.create('date_deadline','<?php echo $this->_tpl_vars['inv']->date_deadline; ?>
',1);</script>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_place" value="<?php echo $this->_tpl_vars['lang']['place']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="place" value="<?php echo $this->_tpl_vars['user']->place; ?>
<?php echo $this->_tpl_vars['inv']->place; ?>
" class="tresc" />
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row form-horizontal dane_osobiste">
		<div class="col-sm-6">
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_kor_dotyczy" value="<?php echo $this->_tpl_vars['lang']['kor_dotyczy']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="kor_dotyczy" value="" class="tresc" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_kor_wystawionej" value="<?php echo $this->_tpl_vars['lang']['kor_wystawionej']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="kor_wystawionej" value="<?php echo $this->_tpl_vars['user']->place; ?>
<?php echo $this->_tpl_vars['inv']->place; ?>
" class="tresc" />
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_kor_data" value="<?php echo $this->_tpl_vars['lang']['kor_data']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<script type="text/javascript">calendar.create('kor_data','');</script>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-4">
					<input type="text" name="lang_kor_miejsce" value="<?php echo $this->_tpl_vars['lang']['kor_miejsce']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="kor_miejsce" value="<?php echo $this->_tpl_vars['user']->place; ?>
<?php echo $this->_tpl_vars['inv']->place; ?>
" class="tresc" />
				</div>
			</div>
		</div>
	</div>
	<hr>
		<div class="orginal_kopia">
		<span><input type="text" name="lang_original" id="original" value="<?php echo $this->_tpl_vars['lang']['original']; ?>
" /><br />
		<a href="#" onclick="javascript: return invoice_obj.strike('original');" title="<?php echo $this->_tpl_vars['lang']['strike']; ?>
" class="invoice_strike"><?php echo $this->_tpl_vars['lang']['strike']; ?>
</a></span>
		<span><input type="text" name="lang_copy" id="copy" value="<?php echo $this->_tpl_vars['lang']['copy']; ?>
"  /><br /><a href="#" onclick="javascript: return invoice_obj.strike('copy');" title="<?php echo $this->_tpl_vars['lang']['strike']; ?>
" class="invoice_strike"><?php echo $this->_tpl_vars['lang']['strike']; ?>
</a></span>
		<span><input type="text" name="lang_double" id="double" value="<?php echo $this->_tpl_vars['lang']['double']; ?>
" /><br /><a href="#" onclick="javascript: return invoice_obj.strike('double');" title="<?php echo $this->_tpl_vars['lang']['strike']; ?>
" class="invoice_strike"><?php echo $this->_tpl_vars['lang']['strike']; ?>
</a></span>
	</div>

<div class="row form-horizontal dane_osobiste">
	<div class="col-sm-6">
		<div class="form-group">
			<div class="col-sm-4">
			<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc2']; ?>
</span></a><input type="text" name="lang_user_name_main" value="<?php echo $this->_tpl_vars['lang']['user_name_main']; ?>
" class="invoice_normal "  />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_name" value="<?php echo $this->_tpl_vars['lang']['firma_name']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="user_name" id="user_name" class="col-sm-12 tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_address" value="<?php echo $this->_tpl_vars['lang']['user_address']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="user_address" id="user_address"  class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_nip" value="<?php echo $this->_tpl_vars['lang']['user_nip']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<input type="text" name="user_nip" id="user_nip" value="<?php echo $this->_tpl_vars['user']->nip; ?>
" maxlength="24" class="tresc" <?php if ($this->_tpl_vars['user']->nip): ?>onkeypress="javascript: return false"<?php endif; ?> />

			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_phone" value="<?php echo $this->_tpl_vars['lang']['user_phone']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="user_phone" id="user_phone"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_mail" value="<?php echo $this->_tpl_vars['lang']['user_mail']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="user_mail" id="user_mail"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_bank_name" value="<?php echo $this->_tpl_vars['lang']['user_bank_name']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
			<?php if (is_array ( $this->_tpl_vars['user']->account ) && count ( $this->_tpl_vars['user']->account )): ?>
				<select name="user_bank_name" id="user_bank_name" class="tresc" style="height:24px;" onchange="javascript: invoice_obj.change_account(false);"><?php $_from = $this->_tpl_vars['user']->account; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i'][1]; ?>
"><?php echo $this->_tpl_vars['i'][1]; ?>
</option><?php endforeach; endif; unset($_from); ?></select>
			<?php else: ?><input type="text" name="user_bank_name" id="user_bank_name" value="<?php echo $this->_tpl_vars['user']->bank_name; ?>
" class="tresc" /><?php endif; ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_account" value="<?php echo $this->_tpl_vars['lang']['user_account']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<?php if (is_array ( $this->_tpl_vars['user']->account ) && count ( $this->_tpl_vars['user']->account )): ?>
					<select name="user_account" id="user_account" class="tresc" style="height:24px;" onchange="javascript: invoice_obj.change_account(true);"><?php $_from = $this->_tpl_vars['user']->account; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i'][0]; ?>
"><?php echo $this->_tpl_vars['i'][0]; ?>
</option><?php endforeach; endif; unset($_from); ?></select>
				<?php else: ?>
					<input type="text" name="user_account" id="user_account" value="<?php if (! is_array ( $this->_tpl_vars['user']->account )): ?><?php echo $this->_tpl_vars['user']->account; ?>
<?php endif; ?>" maxlength="32" class="tresc" />
				<?php endif; ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_user_www" value="<?php echo $this->_tpl_vars['lang']['user_www']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<input type="text" name="user_www" id="user_www" value="<?php echo $this->_tpl_vars['user']->www; ?>
" maxlength="127" class="tresc" />
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<div class="col-sm-4">
				<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc2']; ?>
</span></a><input type="text" name="lang_client_name_main" value="<?php echo $this->_tpl_vars['lang']['client_name_main']; ?>
" class="invoice_normal" />
			</div>
			<div class="col-sm-8">
				<?php if ($this->_tpl_vars['user_sub']): ?>
					<a onclick="javascript: invoice_obj.panel_client();"><i class="manArrow"></i> <?php echo $this->_tpl_vars['lang']['menu_loadc']; ?>
</a>
				<?php else: ?>
					<a onclick="javascript: document.getElementById('sub_error').style.display='block';"><i class="manArrow"></i> <?php echo $this->_tpl_vars['lang']['menu_loadc']; ?>
</a>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['user_sub']): ?>
					<a onclick="javascript: return invoice_obj.save_client()" title="<?php echo $this->_tpl_vars['lang']['add_client']; ?>
" class="zapisz_kontrahenta"><i class="saveDiscSmall2"></i> <?php echo $this->_tpl_vars['lang']['add_client']; ?>
</a>
				<?php else: ?>
					<a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['add_client']; ?>
" class="zapisz_kontrahenta"><i class="saveDiscSmall2"></i> <?php echo $this->_tpl_vars['lang']['add_client']; ?>
</a>
				<?php endif; ?>
				<div id="save_client_ok_error" class="dane_error"> </div>
				<div id="save_client_bad_error" class="dane_error"> </div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_name" value="<?php echo $this->_tpl_vars['lang']['firma_name']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="client_name" id="client_name"  class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['client']->name; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_address" value="<?php echo $this->_tpl_vars['lang']['client_address']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="client_address" id="client_address"  class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['client']->address; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_nip" value="<?php echo $this->_tpl_vars['lang']['client_nip']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<input type="text" name="client_nip" id="client_nip" value="<?php echo $this->_tpl_vars['client']->nip; ?>
" maxlength="24" class="tresc" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_phone" value="<?php echo $this->_tpl_vars['lang']['client_phone']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="client_phone" id="client_phone"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['client']->phone; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_mail" value="<?php echo $this->_tpl_vars['lang']['client_mail']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<textarea name="client_mail" id="client_mail"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['client']->email; ?>
</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_bank_name" value="<?php echo $this->_tpl_vars['lang']['client_bank_name']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<input type="text" name="client_bank_name" id="client_bank_name" value="<?php echo $this->_tpl_vars['client']->bank_name; ?>
" class="tresc" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="lang_client_account" value="<?php echo $this->_tpl_vars['lang']['client_account']; ?>
" class="opis" />
			</div>
			<div class="col-sm-8">
				<input type="text" name="client_account" id="client_account" value="<?php echo $this->_tpl_vars['client']->account; ?>
" maxlength="36" class="tresc"  />
			</div>
		</div>
	</div>
</div>

<div class="row form-horizontal adres_dostawy">
    	<div class="col-sm-6 col-sm-offset-6">
		<div class="form-group">
			<div class="col-sm-4">
				<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc3']; ?>
</span></a>
				<input type="text" name="lang_branch" value="<?php echo $this->_tpl_vars['lang']['branch']; ?>
" class="invoice_normal" />
			</div>
			<div class="col-sm-8 text-right">
				<a onclick="javascript: return invoice_obj.show_branch();return key_mask(event,'')" title="<?php echo $this->_tpl_vars['lang']['show']; ?>
" id="branch_show" <?php if ($this->_tpl_vars['branch']->name): ?>style="display: none;"<?php endif; ?>><i class="downArrow"></i><?php echo $this->_tpl_vars['lang']['show']; ?>
</a>
				<a onclick="javascript: return invoice_obj.hide_branch();" title="<?php echo $this->_tpl_vars['lang']['hide']; ?>
" id="branch_hide" <?php if (! $this->_tpl_vars['branch']->name): ?>style="display: none;"<?php endif; ?>><i class="upArrow"></i><?php echo $this->_tpl_vars['lang']['hide']; ?>
</a>
			</div>
		</div>

		<div id="branch" <?php if (! $this->_tpl_vars['branch']->name): ?>style="display: none;"<?php endif; ?>>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_name" value="<?php echo $this->_tpl_vars['lang']['branch_name']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<textarea name="branch_name" id="branch_name"  class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['branch']->name; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_address" value="<?php echo $this->_tpl_vars['lang']['branch_address']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<textarea name="branch_address" id="branch_address" class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['branch']->address; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_phone" value="<?php echo $this->_tpl_vars['lang']['branch_phone']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<textarea name="branch_phone" id="branch_phone"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['branch']->phone; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_mail" value="<?php echo $this->_tpl_vars['lang']['branch_mail']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<textarea name="branch_mail" id="branch_mail"   class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"><?php echo $this->_tpl_vars['branch']->email; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_krs" value="<?php echo $this->_tpl_vars['lang']['branch_krs']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="branch_krs" id="branch_krs" value="<?php echo $this->_tpl_vars['branch']->krs; ?>
" maxlength="10" class="tresc" onkeypress="javascript: return valid_data.check_krs(event);" />
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_bank_name" value="<?php echo $this->_tpl_vars['lang']['branch_bank_name']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="branch_bank_name" id="branch_bank_name" value="<?php echo $this->_tpl_vars['branch']->bank_name; ?>
" class="tresc" />
				</div>
			</div>
			<div class="form-group">
								<div class="col-sm-4">
					<input type="text" name="lang_branch_account" value="<?php echo $this->_tpl_vars['lang']['branch_account']; ?>
" class="opis" />
				</div>
				<div class="col-sm-8">
					<input type="text" name="branch_account" id="branch_account" value="<?php echo $this->_tpl_vars['branch']->account; ?>
" maxlength="32" class="tresc" onkeypress="javascript: return valid_data.check_account(event,this.id);" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
			<?php if ($this->_tpl_vars['user_sub']): ?>
			<div id="faktura_okresowa_checkobox">
				<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc5']; ?>
</span></a><input type="checkbox" value="1" id="period_check" name="period" onclick="javascript: invoice_obj.period(this);" /> <label for="period_check"><?php echo $this->_tpl_vars['langs']['time_head']; ?>
</label>
			</div>
			<div id="period">
				<div>
					<?php echo $this->_tpl_vars['langs']['time_main']; ?>

					<input type="text" value="1" name="period_value" class="tresc" id="period_value" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" onkeyup="javascript: invoice_obj.count_all();" />
					<select name="period_time">
						<option value="day"><?php echo $this->_tpl_vars['langs']['time_day']; ?>
</option>
						<option value="week"><?php echo $this->_tpl_vars['langs']['time_week']; ?>
</option>
						<option value="month"><?php echo $this->_tpl_vars['langs']['time_month']; ?>
</option>
						<option value="year"><?php echo $this->_tpl_vars['langs']['time_year']; ?>
</option>
						</select><?php echo $this->_tpl_vars['langs']['time_start']; ?>

				</div>
				<script type="text/javascript">calendar.create('period_start','');</script>

			</div>
		<?php else: ?>
			<div id="faktura_okresowa_checkobox">
				<a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc5']; ?>
</span></a><input type="checkbox" value="1" id="period_check" name="period" onclick="javascript: document.getElementById('sub_error').style.display='block';" /> <label for="period_check"><?php echo $this->_tpl_vars['langs']['time_head']; ?>
</label>
			</div>
		<?php endif; ?>
		<div id="waluta"><label for="current"><?php echo $this->_tpl_vars['lang']['currency']; ?>
:</label> <select  name="current" id="current" onchange="javascript: invoice_obj.change_current(this.value);"><option value="PLN">PLN</option><option value="EUR">EUR</option><option value="USD">USD</option><option value="GBP">GBP</option><option value="AUD">AUD</option><option value="CZK">CZK</option><option value="DKK">DKK</option><option value="EEK">EEK</option><option value="HKD">HKD</option><option value="JPY">JPY</option><option value="CAD">CAD</option><option value="LTL">LTL</option><option value="LVL">LVL</option><option value="NOK">NOK</option><option value="ZAR">ZAR</option><option value="RUB">RUB</option><option value="CHF">CHF</option><option value="SEK">SEK</option><option value="UAH">UAH</option><option value="HUF">HUF</option><option value="XDR">XDR</option><option value="BGN">BGN</option><option value="RON">RON</option><option value="PHP">PHP</option><option value="THB">THB</option><option value="NZD">NZD</option><option value="SGD">SGD</option><option value="ISK">ISK</option><option value="HRK">HRK</option><option value="TRY">TRY</option><option value="MXN">MXN</option><option value="BRL">BRL</option><option value="MYR">MYR</option><option value="IDR">IDR</option><option value="KRW">KRW</option><option value="CNY">CNY</option><option value="ILS">ILS</option><option value="INR">INR</option><option value="CLP">CLP</option></select></div>
	</div>


	<div class="col-sm-6">
				<div class="kalkulator">
		<table>
			<tr>
				<th class="text-left"><a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc4']; ?>
</span></a><span><?php echo $this->_tpl_vars['lang']['calc_nagl1']; ?>
</span></th>
				<th><?php echo $this->_tpl_vars['lang']['calc_nagl2']; ?>
</th>
				<th><?php echo $this->_tpl_vars['lang']['calc_nagl3']; ?>
</th>
				<th><?php echo $this->_tpl_vars['lang']['calc_nagl4']; ?>
</th>
			</tr>
			<tr>
				<td class="tekst"><?php echo $this->_tpl_vars['lang']['calc_nagl5']; ?>
 <span class="bold"><?php echo $this->_tpl_vars['lang']['calc_nagl6']; ?>
</span><?php echo $this->_tpl_vars['lang']['calc_nagl7']; ?>
</td>
				<td><input type="text" id="n2b_n" value="0" onkeyup="javascript: invoice_obj.n2b();" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" style="width:60px;" /></td>
				<td><select id="n2b_v" onchange="javascript: invoice_obj.n2b();" ><option value="0">0 %</option><option value="3">3 %</option><option value="4">4 %</option><option value="5">5 %</option><option value="6">6 %</option><option value="7">7 %</option><option value="8">8 %</option><option value="18">18 %</option><option value="19">19 %</option><option value="20">20 %</option><option value="22">22 %</option><option value="23">23 %</option><option value="25">25 %</option></select></td>
				<td><input type="text" id="n2b_b" value="0" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);"  style="width:65px;" /></td>
			</tr>
			<tr>
				<td class="tekst"><?php echo $this->_tpl_vars['lang']['calc_nagl5']; ?>
 <span class="bold"><?php echo $this->_tpl_vars['lang']['calc_nagl8']; ?>
</span> <?php echo $this->_tpl_vars['lang']['calc_nagl9']; ?>
</td>
				<td><input type="text" id="b2n_n" value="0" onkeyup="javascript: invoice_obj.b2n();" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);"  style="width:60px;" /></td>
				<td><select id="b2n_v" onchange="javascript: invoice_obj.b2n();" ><option value="0">0 %</option><option value="3">3 %</option><option value="4">4 %</option><option value="5">5 %</option><option value="6">6 %</option><option value="7">7 %</option><option value="8">8 %</option><option value="18">18 %</option><option value="19">19 %</option><option value="20">20 %</option><option value="22">22 %</option><option value="23">23 %</option><option value="25">25 %</option></select></td>
				<td><input type="text" id="b2n_b" value="0" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);"  style="width:65px;" /></td>
			</tr>
			<tr><td colspan="4" class="tekst"><?php echo $this->_tpl_vars['lang']['calc_nagl0']; ?>
</td></tr>
		</table>
		</div>
	</div>
</div>






<div id="lista">
	<?php if ($this->_tpl_vars['user_sub']): ?>
		<a class="zaladuj_prod" onclick="javascript: invoice_obj.panel_prod();"><i class="cartArrow"></i> <?php echo $this->_tpl_vars['lang']['menu_loadp']; ?>
</a>
	<?php else: ?>
		<a class="zaladuj_prod" onclick="javascript: document.getElementById('sub_error').style.display='block';" ><i class="cartArrow"></i> <?php echo $this->_tpl_vars['lang']['menu_loadp']; ?>
</a>
	<?php endif; ?>
	<div id="save_product_ok_error" class="dane_error"> </div>
	<div id="save_product_bad_error" class="dane_error"> </div>
	<div class="table-responsive">
		<table class="table form-table lista">
			<thead>
				<tr>
					<th class="akcje"><a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc6']; ?>
</span></a></th>
					<th class="lp"><input type="text" name="lang_lp" value="<?php echo $this->_tpl_vars['lang']['lp']; ?>
" /></th>
					<th class="name"><input type="text" name="lang_name" value="<?php echo $this->_tpl_vars['lang']['name']; ?>
" /></th>
					<th class="pkwiu"><input type="text" name="lang_pkwiu" value="<?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
" /></th>
					<th class="ilosc"><input type="text" name="lang_amount" value="<?php echo $this->_tpl_vars['lang']['amount']; ?>
" /></th>
					<th class="jm"><input type="text" name="lang_unit" value="<?php echo $this->_tpl_vars['lang']['unit']; ?>
" /></th>
					<th class="kwota2"><input type="text" name="lang_netto" value="<?php echo $this->_tpl_vars['lang']['netto']; ?>
" /></th>
					<th class="kwota2"><input type="text" name="lang_rabat" value="<?php echo $this->_tpl_vars['lang']['rabat']; ?>
" /></th>
					<th class="kwota2"><input type="text" name="lang_prize_netto" value="<?php echo $this->_tpl_vars['lang']['prize_netto']; ?>
" /></th>
					<th class="vat"><input type="text" name="lang_vat" value="<?php echo $this->_tpl_vars['lang']['vat']; ?>
" /></th>
					<th class="kwota"><input type="text" name="lang_sum_netto" value="<?php echo $this->_tpl_vars['lang']['sum_netto']; ?>
" /></th>
					<th class="kwota"><input type="text" name="lang_sum_vat" value="<?php echo $this->_tpl_vars['lang']['sum_vat']; ?>
" /></th>
					<th class="kwota"><input type="text" name="lang_sum_brutto" value="<?php echo $this->_tpl_vars['lang']['sum_brutto']; ?>
" /></th>
				</tr>
			</thead>
			<tbody id="product_list">

			</tbody>
		</table>
	</div>

	<a title="<?php echo $this->_tpl_vars['lang']['add']; ?>
" onclick="javascript: return invoice_obj.add();"  class="added2"><i class="plus"></i> <?php echo $this->_tpl_vars['lang']['korekta1']; ?>
</a>
	<div style="font-weight:bold;font-size:14px;text-align:center;margin:10px"><?php echo $this->_tpl_vars['lang']['korekta2']; ?>
</div>

	<div class="table-responsive">
		<table class="table form-table lista">
			<thead>
				<tr>
					<th class="akcje"><a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc6']; ?>
</span></a></th>
					<th class="lp"><input type="text" name="lang_kor_lp" value="<?php echo $this->_tpl_vars['lang']['lp']; ?>
"  /></th>
					<th class="name"><input type="text" name="lang_kor_name" value="<?php echo $this->_tpl_vars['lang']['kor_name']; ?>
"  /></th>
					<th class="pkwiu"><input type="text" name="lang_kor_pkwiu" value="<?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
"  /></th>
					<th class="ilosc"><input type="text" name="lang_kor_amount" value="<?php echo $this->_tpl_vars['lang']['amount']; ?>
"  /></th>
					<th class="jm"><input type="text" name="lang_kor_unit" value="<?php echo $this->_tpl_vars['lang']['unit']; ?>
"  /></th>
					<th class="kwota2"><input type="text" name="lang_kor_netto" value="<?php echo $this->_tpl_vars['lang']['netto']; ?>
" /></th>
					<th class="kwota2" ><input type="text" name="lang_kor_rabat" value="<?php echo $this->_tpl_vars['lang']['rabat']; ?>
" /></th>
					<th class="kwota2"><input type="text" name="lang_kor_prize_netto" value="<?php echo $this->_tpl_vars['lang']['prize_netto']; ?>
"  /></th>
					<th class="vat"><input type="text" name="lang_kor_vat" value="<?php echo $this->_tpl_vars['lang']['vat']; ?>
"  /></th>
					<th class="kwota"><input type="text" name="lang_kor_sum_netto" value="<?php echo $this->_tpl_vars['lang']['kor_sum_netto']; ?>
" /></th>
					<th class="kwota"><input type="text" name="lang_kor_sum_vat" value="<?php echo $this->_tpl_vars['lang']['kor_sum_vat']; ?>
" /></th>
					<th class="kwota"><input type="text" name="lang_kor_sum_brutto" value="<?php echo $this->_tpl_vars['lang']['kor_sum_brutto']; ?>
"  /></th>
				</tr>
			</thead>
			<tbody id="product_kor">

			</tbody>
		</table>
	</div>
	<a title="<?php echo $this->_tpl_vars['lang']['add']; ?>
" onclick="javascript: return invoice_obj.add2();" class="added2"><i class="plus"></i> <?php echo $this->_tpl_vars['lang']['korekta3']; ?>
</a>


	<table>
		<tr id="row_position">
			<td class="akcje">
				<a title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" onclick="javascript: return invoice_obj.del([id]);"><i class="del"></i></a>
				<?php if ($this->_tpl_vars['user_sub']): ?>
					<a title="<?php echo $this->_tpl_vars['lang']['add_product']; ?>
" onclick="javascript: return invoice_obj.save_product([id]);"><i class="saveDiscSmall"></i></a>
				<?php else: ?>
					<a title="<?php echo $this->_tpl_vars['lang']['add_product']; ?>
" onclick="javascript: document.getElementById('sub_error').style.display='block';"><i class="saveDiscSmall"></i></a>
				<?php endif; ?>
			</td>
			  <td><input type="text" name="p_[id]_lp" id="p_[id]_lp" value="" class="invoice_data" onkeypress="javascript: return key_mask(event,'');" /><input type="hidden" name="p_[id]_type" id="p_[id]_type" value="" /></td>
			  <td><textarea name="p_[id]_name" id="p_[id]_name" class="invoice_data" onkeyup="javascript: invoice_obj.textarea_height(this);"></textarea></td>
			  <td><input type="text" name="p_[id]_pkwiu" id="p_[id]_pkwiu" value="" class="invoice_data" /></td>
			  <td><input type="text" name="p_[id]_amount" id="p_[id]_amount" value=""  onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" onkeyup="javascript: invoice_obj.count_all();" class="text-right" /></td>
			  <td><select name="p_[id]_unit" id="p_[id]_unit" ><option value="">---</option><?php $_from = $this->_tpl_vars['units']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['k']; ?>
</option><?php endforeach; endif; unset($_from); ?></select></td>
			  <td><input type="text" name="p_[id]_netto" id="p_[id]_netto" value="" class="text-right" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" onkeyup="javascript: invoice_obj.count_all();" /></td>
			  <td><input type="text" name="p_[id]_rabat" id="p_[id]_rabat" value="" class="text-right" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" onkeyup="javascript: invoice_obj.count_all();" /></td>
			  <td><input type="text" name="p_[id]_pnetto" id="p_[id]_pnetto" value="" class="text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
			  <td><select name="p_[id]_vat" id="p_[id]_vat"  onchange="javascript: invoice_obj.count_all();"><option value="0">0 %</option><option value="3">3 %</option><option value="4">4 %</option><option value="5">5 %</option><option value="6">6 %</option><option value="7">7 %</option><option value="8">8 %</option><option value="18">18 %</option><option value="19">19 %</option><option value="20">20 %</option><option value="22">22 %</option><option value="23">23 %</option><option value="NP">NP</option><option value="ZW">ZW</option><option value="bez VAT">bez VAT</option><option value="OO">OO</option><option value="-">-</option></select></td>
			  <td><input type="text" name="p_[id]_snetto"  id="p_[id]_snetto" value="" class="text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
			  <td><input type="text" name="p_[id]_svat" id="p_[id]_svat" value="" class="text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
			  <td><input type="text" name="p_[id]_sbrutto" id="p_[id]_sbrutto" value="" class="text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
		</tr>
	</table>
</div>

<div class="row form-horizontal">
	<div class="col-sm-6">
		<div class="form-group">
			<div class="col-sm-4">
				<input class="opis" name="lang_kor_powod" value="Przyczyna korekty podatku" type="text" />
			</div>
			<div class="col-sm-8">
				<textarea name="kor_powod" id="" class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"></textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input class="opis" name="lang_kor_sum" value="Kwota zmniejszenia/zwiekszęnia ceny bez podatku" type="text" />
			</div>
			<div class="col-sm-8">
				<textarea name="kor_sum" id="kor_sum" class="tresc"  onkeyup="javascript: invoice_obj.textarea_height(this);"></textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input class="opis" name="lang_kor_vat" value="Kwota zmniejszenia/zwiekszęnia podatku należnego" type="text" />
			</div>
			<div class="col-sm-8">
				<textarea name="kor_vat" id="kor_vat" class="tresc" onkeyup="javascript: invoice_obj.textarea_height(this);"></textarea>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-6">
				<div class="kwoty">
			<input type="text" name="lang_cash_sum" value="<?php echo $this->_tpl_vars['lang']['cash_sum']; ?>
" class="opis" />
			<input type="text" name="cash_sum" id="cash_sum" value="0.00 PLN" class="wartosc" onkeypress="return invoice_obj.insert_prize(event,this.id);" />
			<input type="text" name="lang_cash_word" value="<?php echo $this->_tpl_vars['lang']['cash_word']; ?>
" class="opis" />
			<input type="text" name="cash_word" id="cash_word" value="" class="wartosc" onkeypress="javascript: return key_mask(event,'');" />
		</div>
	</div>
	<div class="col-md-6">
				<div class="text-right">
				<table class="table form-table lista" id="sumVatTable">
					<thead>
					<tr>
						<th class="akcje"><a class="pomoc"><i class="help"></i><span><?php echo $this->_tpl_vars['lang']['pomoc7']; ?>
</span></a></th>
						<th><input type="text" name="lang_all_rate" value="<?php echo $this->_tpl_vars['lang']['all_rate']; ?>
" class="stawka" /></th>
						<th><input type="text" name="lang_all_netto" value="<?php echo $this->_tpl_vars['lang']['all_netto']; ?>
" class="netto" /></th>
						<th><input type="text" name="lang_all_vat" value="<?php echo $this->_tpl_vars['lang']['all_vat']; ?>
" class="vat" /></th>
						<th><input type="text" name="lang_all_brutto" value="<?php echo $this->_tpl_vars['lang']['all_brutto']; ?>
" class="brutto" /></th>
					</tr>
					</thead>
					<tfoot>
					<tr>
						<td class="akcje"></td>
						<td><input type="text" name="lang_all_sum" value="<?php echo $this->_tpl_vars['lang']['all_sum']; ?>
" class="stawka text-right" /></td>
						<td><input type="text" id="all_netto" value="0.00 PLN" class="netto text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
						<td><input type="text" id="all_vat" value="0.00 PLN" class="vat text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
						<td><input type="text" id="all_brutto" value="0.00 PLN" class="brutto text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
					</tr>
					</tfoot>
					<tbody id="vat_list"></tbody>
				</table>
				<table>
				<tr id="row_vat">
					<td class="akcje"></td>
					<td><input type="text" id="v_[id]_rate" value="" class="stawka text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
					<td><input type="text" id="v_[id]_netto" value="" class="netto text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
					<td><input type="text" id="v_[id]_vat" value="" class="vat text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
					<td><input type="text" id="v_[id]_brutto" value="" class="brutto text-right" onkeypress="javascript: return key_mask(event,'');" /></td>
				</tr>
				</table>
		</div>
				<div class="zaplata">
			<div class="form-group">
				<input type="text" name="lang_cash_type" value="<?php echo $this->_tpl_vars['lang']['cash_type']; ?>
" class="opis" />
				<select name="cash_type" id="cash_type" class="wartosc" style="width:152px;"><?php $_from = $this->_tpl_vars['lang']['cash_option']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>
			</div>
			<div class="form-group">
				<input type="text" name="lang_cash_pay" value="<?php echo $this->_tpl_vars['lang']['cash_pay']; ?>
" class="opis" />
				<input type="text" name="cash_pay" id="cash_pay" value="0.00 PLN" class="wartosc" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" onkeyup="javascript: invoice_obj.count_all();" style="width:146px;" />
			</div>

			<div class="form-group">
				<input type="text" name="lang_cash_date" value="<?php echo $this->_tpl_vars['lang']['cash_date']; ?>
" class="opis" style="padding-left:1px;padding-right:1px;" />
				<input type="radio" class="styled" name="cash_date" id="cash_date_l" value="list" checked="checked" />
				<select name="cash_date_list" id="cash_date_list" style="width:152px;" ><?php $_from = $this->_tpl_vars['lang']['cash_deadline']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['i']; ?>
" <?php if ($this->_tpl_vars['i'] == '7 dni'): ?>selected="selected" <?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select><br />

				<div style="height:40px;clear:both;padding-left:29%;margin:4px 0px">
					<input type="radio" name="cash_date" id="cash_date_d" value="date"  />
					<div style="display:inline-block;"><script type="text/javascript">calendar.create('cash_date_date','<?php if ($this->_tpl_vars['inv']->sell_date): ?><?php echo $this->_tpl_vars['inv']->sell_time; ?>
<?php endif; ?>');</script></div>
				</div>
			</div>
		</div>
	</div>
</div>

		<div class="row podpis_pieczatka">
		<div class="col-sm-3 col-sm-offset-7">
				<?php if ($this->_tpl_vars['extra']->sign): ?><img src="<?php echo $this->_tpl_vars['extra']->sign; ?>
" alt="" title="" /><?php endif; ?>
		</div>
	</div>
	<div class="row podpis">
		<div class="col-sm-3 col-sm-offset-2">
			<input type="text" name="lang_client_sign" value="<?php echo $this->_tpl_vars['lang']['client_sign']; ?>
" class="opis" />
		</div>
		<div class="col-sm-3 col-sm-offset-2">
			<input type="text" name="lang_user_sign" value="<?php echo $this->_tpl_vars['lang']['user_sign']; ?>
" class="opis" />
		</div>
	</div>
		<div class="row">
		<div class="col-md-12">
			<input type="text" name="lang_coment" value="<?php echo $this->_tpl_vars['lang']['coment']; ?>
" class="text-center opis" onkeyup="javascript: invoice_obj.textarea_height(this);" />
		</div>
		<div class="col-md-12">
			<textarea name="coment" class="form-control" ><?php echo $this->_tpl_vars['inv']->coment; ?>
</textarea>
		</div>
	</div>
		<div class="hidden">
		<input type="hidden" name="lang" value="<?php echo $this->_tpl_vars['cf_lang']; ?>
" />
		<input type="hidden" name="original_v" value="0" id="original_v" />
		<input type="hidden" name="branch_v" value="0" id="branch_v" />
		<input type="hidden" name="client_id" value="<?php echo $this->_tpl_vars['inv']->client; ?>
" id="client_id" />
	</div>
</form>
<div id="sub_error" class="invoice_client" style="left:50%;display:none;top:50%;margin-top:-65px;margin-left:-403px;position:fixed;line-height:60px;height:124px;font-weight:bold;text-align:center;font-size:22px;">
	<a onclick="javascript: document.getElementById('sub_error').style.display='none';" title="<?php echo $this->_tpl_vars['lang']['clients_close']; ?>
" class="close" ><span class="glyphicon glyphicon-remove"></span></a>
	<?php echo $this->_tpl_vars['lang']['error1']; ?>
<br />
</div>

<div id="save_base_error_all" class="invoice_client" style="left:50%;display:none;top:50%;margin-top:-65px;margin-left:-403px;position:fixed;line-height:124px;height:124px;font-weight:bold;text-align:center;font-size:22px;">
	<a onclick="javascript: document.getElementById('save_base_error_all').style.display='none';" title="<?php echo $this->_tpl_vars['lang']['clients_close']; ?>
" class="close" ><span class="glyphicon glyphicon-remove"></span></a>
	<div id="save_base_error"> </div>
</div>
<div class="hidden">
	<div class="invoice_client" id="client_panel">
				<a onclick="javascript: return invoice_obj.close_client();" title="<?php echo $this->_tpl_vars['lang']['clients_close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
		<h3><?php echo $this->_tpl_vars['lang']['clients_head']; ?>
</h3>
			<div class="col-sm-4">
				<label><?php echo $this->_tpl_vars['lang']['clients_name']; ?>
:</label>
				<input type="text" id="clients_[id]_name" value="[name]" class="form-control" />
			</div>
			<div class="col-sm-4">
				<label><?php echo $this->_tpl_vars['lang']['clients_address']; ?>
:</label>
				<input type="text" id="clients_[id]_address" value="[address]" class="form-control" />
			</div>
			<div>
				<label><?php echo $this->_tpl_vars['lang']['clients_nip']; ?>
:</label>
				<div class="form-inline">
					<div class="form-group">
						<input type="text" id="clients_[id]_nip" value="[nip]" maxlength="13" class="form-control" />
						<input type="button" value="<?php echo $this->_tpl_vars['lang']['clients_search']; ?>
"  onclick="javascript: invoice_obj.panel_client(1);" class="btn orangeButton"/>
					</div>
				</div>
			</div>

				<div id="zakres_faktur" style="clear:both;padding-top:10px" class="text-right col-sm-12">
			<div class="btn-group alphabet">
				<a onclick="javascript: return invoice_obj.panel_client([search],1,this.title);" title="" class="btn btn-default btn-sm" id="client_letter"><?php echo $this->_tpl_vars['lang']['clients_all']; ?>
</a>[letters]
			</div>
		</div>
				<div class="lista_kontrahentow" style="clear:both;">
			[clients]
		</div>
				<div style="clear:both;padding-top:10px" class="text-left col-sm-12">
			<div class="btn-group alphabet">
					[pages]
			</div>
		</div>
	</div>
	<a onclick="javascript: return invoice_obj.load_client(this.title);" title="" class="invoice_client" id="client_object"></a>
	<a onclick="javascript: return invoice_obj.panel_client([search],this.title,[letter]);" title="" class="" id="client_page">&nbsp;</a>
	<div class="invoice_prod" id="prod_panel">
				<a onclick="javascript: return invoice_obj.close_prod();" title="<?php echo $this->_tpl_vars['lang']['prod_close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
		<h3><?php echo $this->_tpl_vars['lang']['prod_head']; ?>
</h3>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_name']; ?>
:</label>
			<input type="text" id="prods_[id]_name" value="[name]" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_producer']; ?>
:</label>
			<input type="text" id="prods_[id]_producer" value="[producer]" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_cat']; ?>
:</label>
			<select id="prods_[id]_cat" class="form-control"><option value="">---</option></select>
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_number']; ?>
:</label>
			<input type="text" id="prods_[id]_number" value="[number]" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_pkwiu']; ?>
:</label>
			<input type="text" id="prods_[id]_pkwiu" value="[pkwiu]" class="form-control" />
		</div>
		<div class="col-sm-4">
			<label><?php echo $this->_tpl_vars['lang']['prod_prize']; ?>
:</label>
			<div class="row">
				<div class="col-sm-4">
					<input type="text" id="prods_[id]_prize_min" value="[prize_min]" class="form-control" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" placeholder="od" /> 
				</div>
				<div class="col-sm-4">
					<input type="text" id="prods_[id]_prize_max" value="[prize_max]" class="form-control" onkeypress="javascript: return invoice_obj.insert_prize(event,this.id);" placeholder="do" />
				</div>
			</div>
		</div>
		<div id="add_[id]_info" class="add_info" ></div>
		<div class="col-sm-12" style="padding-top:5px;clear:both;"><input type="button" value="<?php echo $this->_tpl_vars['lang']['prod_search']; ?>
" class="btn orangeButton" onclick="javascript: invoice_obj.panel_prod(1);" /></div>
				<div id="zakres_faktur" class="text-right col-sm-12">
		<div class="alphabet btn-group">
			<a onclick="javascript: return invoice_obj.panel_prod([search],1,this.title);" title="" class="btn btn-xs btn-default" id="prod_letter"><?php echo $this->_tpl_vars['lang']['prod_all']; ?>
</a>[letters]
		</div>
		</div>
				<div class="lista_produktow">
		<span class="header">
		<span class="column1">Nazwa</span>
		<span class="column2">Cena netto</span>
		<span class="column3">Cena brutto</span>
		</span>
			[prods]
		</div>
				<div  style="clear:both;overflow:auto;padding:8px 0;" class="col-sm-12 btn-group">
			[pages]
		</div>
	</div>
	<a onclick="javascript: return invoice_obj.load_prod(this.title);" title="" id="prod_object"></a>
	<a onclick="javascript: return invoice_obj.panel_prod([search],this.title,[letter]);" title="" class="" id="prod_page">&nbsp;</a>
</div>
<div class="invoice_client" id="what_print">
	<a onclick="javascript: this.parentNode.style.display='none';" class="close"><span class="glyphicon glyphicon-remove"></span></a>
	<h3><?php echo $this->_tpl_vars['lang']['menu_pdf']; ?>
</h3>
	<div class="what_print_line"><input type="radio" name="what_p" value="1" id="print1" checked="checked" /> <label for="print1"><?php echo $this->_tpl_vars['lang']['save_menu1']; ?>
</label></div>
	<div class="what_print_line"><input type="radio" name="what_p" value="2" id="print2" /> <label for="print2"><?php echo $this->_tpl_vars['lang']['save_menu2']; ?>
</label></div>
	<div class="what_print_line"><input type="radio" name="what_p" value="3" id="print3" /> <label for="print3"><?php echo $this->_tpl_vars['lang']['save_menu3']; ?>
</label></div>
	<div class="what_print_line"><input type="radio" name="what_p" value="4" id="print4" /> <label for="print4"><?php echo $this->_tpl_vars['lang']['save_menu4']; ?>
</label></div>
	<div class="text-center row">
	<input type="button" value="<?php echo $this->_tpl_vars['lang']['save_menu5']; ?>
" onclick="javascript: return invoice_obj.save_pdf_end();" class="btn orangeButton" />
	<br><br>
	</div>
</div>

<div class="hidden">
	<input type="hidden" id="count" value="0" />
	<input type="hidden" id="count_n" value="0" />
	<input type="hidden" id="count_p" value="0" />
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
	<input type="hidden" id="same_nr" value="<?php echo $this->_tpl_vars['lang']['same_nr']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	<input type="hidden" id="addc_ok" value="<?php echo $this->_tpl_vars['lang']['addc_ok']; ?>
" />
	<input type="hidden" id="addc_error" value="<?php echo $this->_tpl_vars['lang']['addc_error']; ?>
" />
	<input type="hidden" id="addp_ok" value="<?php echo $this->_tpl_vars['lang']['addp_ok']; ?>
" />
	<input type="hidden" id="addp_error" value="<?php echo $this->_tpl_vars['lang']['addp_error']; ?>
" />
	<input type="hidden" id="number_coin" value="<?php echo $this->_tpl_vars['lang']['number_coin']; ?>
" />
	<input type="hidden" id="number_tou" value="<?php echo $this->_tpl_vars['lang']['number_tou']; ?>
" />
	<input type="hidden" id="number_mln" value="<?php echo $this->_tpl_vars['lang']['number_mln']; ?>
" />
	<input type="hidden" id="number_mld" value="<?php echo $this->_tpl_vars['lang']['number_mld']; ?>
" />
	<input type="hidden" id="number_j" value="<?php echo $this->_tpl_vars['lang']['number_j']; ?>
" />
	<input type="hidden" id="number_n" value="<?php echo $this->_tpl_vars['lang']['number_n']; ?>
" />
	<input type="hidden" id="number_d" value="<?php echo $this->_tpl_vars['lang']['number_d']; ?>
" />
	<input type="hidden" id="number_s" value="<?php echo $this->_tpl_vars['lang']['number_s']; ?>
" />
</div>
	<div class="row">
		<div class="col-md-7 col-md-offset-5">
			<ul class="invoiceMenu text-center">
			<?php if ($this->_tpl_vars['user_sub']): ?>
				<li><a onclick="javascript: invoice_obj.save_base();"><span class="icon"><i class="saveDisc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_save']; ?>
</span></a></li>
				<li><a onclick="javascript: invoice_obj.save_pdf();"><span class="icon"><i class="printer"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_pdf']; ?>
</span></a></li>
				<li><a href="invoice/" ><span class="icon"><i class="newDoc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_clean']; ?>
</span></a></li>
				<li><a href="panel/client/list/" title="<?php echo $this->_tpl_vars['lang']['list_client']; ?>
"><span class="icon"><i class="people"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_client']; ?>
</span></a></li>
				<li><a href="panel/product/list/" title="<?php echo $this->_tpl_vars['lang']['prod_head']; ?>
"><span class="icon"><i class="shoppingCart"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['prod_head']; ?>
</span></a></li>
				<li><a href="panel/invoice/search/" title="<?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
"><span class="icon"><i class="docList"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
</span></a></li>
			<?php else: ?>
								<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';"><span class="icon"><i class="printer"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_pdf']; ?>
</span></a></li>
				<li><a href="faktura-pro-forma/"  ><span class="icon"><i class="newDoc"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['menu_clean']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['list_client']; ?>
"><span class="icon"><i class="people"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_client']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['prod_head']; ?>
"><span class="icon"><i class="shoppingCart"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['prod_head']; ?>
</span></a></li>
				<li><a onclick="javascript: document.getElementById('sub_error').style.display='block';" title="<?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
"><span class="icon"><i class="docList"></i></span><span class="buttonLabel"><?php echo $this->_tpl_vars['lang']['list_invoice']; ?>
</span></a></li>
			<?php endif; ?>
			</ul>
		</div>
	</div>
</div>


<?php $_from = $this->_tpl_vars['inv']->product; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<script type="text/javascript">invoice_obj.add('<?php echo ((is_array($_tmp=$this->_tpl_vars['i']->name)) ? $this->_run_mod_handler('replace', true, $_tmp, "\n", "\\n") : smarty_modifier_replace($_tmp, "\n", "\\n")); ?>
', '<?php echo $this->_tpl_vars['i']->pkwiu; ?>
', '<?php echo $this->_tpl_vars['i']->unit; ?>
', '<?php echo $this->_tpl_vars['i']->vat; ?>
', '<?php echo $this->_tpl_vars['i']->netto; ?>
', '<?php echo $this->_tpl_vars['i']->amount; ?>
', '<?php echo $this->_tpl_vars['i']->rabat; ?>
');</script>
<?php endforeach; endif; unset($_from); ?>
<?php if (count ( $this->_tpl_vars['inv']->product ) == 0): ?><script type="text/javascript">invoice_obj.add();</script><?php endif; ?>
<script type="text/javascript">invoice_obj.add2();</script>
<script type="text/javascript">invoice_obj.load_invoice('<?php echo $this->_tpl_vars['inv']->original; ?>
','<?php echo $this->_tpl_vars['inv']->sell_type; ?>
','<?php echo $this->_tpl_vars['inv']->sell_date; ?>
','<?php echo $this->_tpl_vars['inv']->sell_time; ?>
','<?php echo $this->_tpl_vars['inv']->sum_pay; ?>
','<?php echo $this->_tpl_vars['inv']->cur; ?>
');</script>
<script type="text/javascript">invoice_obj.resize_textareas();</script>