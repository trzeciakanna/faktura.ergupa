<?php /* Smarty version 2.6.19, created on 2015-02-28 13:59:19
         compiled from uploader/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'uploader/standard.html', 8, false),)), $this); ?>
<?php if (str_replace ( 'admin' , "" , $_SERVER['PHP_SELF'] ) == $_SERVER['PHP_SELF']): ?><script type="text/javascript" src="module/uploader/class.js"></script><script type="text/javascript">var path="";</script>
<?php else: ?><script type="text/javascript" src="../module/uploader/class.js"></script><script type="text/javascript">var path="../";</script><?php endif; ?>
<div class="row_auto">
	<div>	
		<input type="file" name="file<?php echo $this->_tpl_vars['count']; ?>
"  size="30" id="uploader_upload<?php echo $this->_tpl_vars['count']; ?>
" onchange="javascript: uploader_obj.upload(<?php echo $this->_tpl_vars['count']; ?>
,'<?php echo $this->_tpl_vars['uploader_what']; ?>
');" />
		<input type="hidden" name="<?php echo $this->_tpl_vars['uploader_name']; ?>
" id="up_file<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['uploader_file']; ?>
" />
		<div class="uploader_progress" id="uploader_progress<?php echo $this->_tpl_vars['count']; ?>
"><img src="images/layout/progress.gif" width="220px" height="19px" title="" alt="" /></div>
		<div class="row_info"><?php echo ((is_array($_tmp=$this->_tpl_vars['langs']['max_weight'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[weight]", ($this->_tpl_vars['max_weight'])) : smarty_modifier_replace($_tmp, "[weight]", ($this->_tpl_vars['max_weight']))); ?>
</div>
		<?php if ($this->_tpl_vars['uploader_what'] == 'img'): ?><div class="row_info"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['langs']['max_size'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[width]", ($this->_tpl_vars['max_size'][0])) : smarty_modifier_replace($_tmp, "[width]", ($this->_tpl_vars['max_size'][0]))))) ? $this->_run_mod_handler('replace', true, $_tmp, "[height]", ($this->_tpl_vars['max_size'][1])) : smarty_modifier_replace($_tmp, "[height]", ($this->_tpl_vars['max_size'][1]))); ?>
</div><?php endif; ?>
	</div>
	<div>
		<div class="">
		<?php if ($this->_tpl_vars['uploader_what'] == 'file'): ?>
			<?php if ($this->_tpl_vars['uploader_file'] != ""): ?><a href="<?php echo $this->_tpl_vars['uploader_file']; ?>
" title="" class="uploader_download" id="uploader_download<?php echo $this->_tpl_vars['count']; ?>
"><?php echo $this->_tpl_vars['langs']['download']; ?>
</a>
			<?php else: ?><a href="" title="" class="uploader_download_none" id="uploader_download<?php echo $this->_tpl_vars['count']; ?>
"><?php echo $this->_tpl_vars['langs']['download']; ?>
</a><?php endif; ?>
		<?php elseif ($this->_tpl_vars['uploader_what'] == 'img'): ?>
			<?php if ($this->_tpl_vars['uploader_file'] != ""): ?><img src="<?php echo $this->_tpl_vars['uploader_file']; ?>
" title="" alt="" class="uploader_download" id="uploader_download<?php echo $this->_tpl_vars['count']; ?>
" />
			<?php else: ?><img src="" title="" alt="" class="uploader_download_none" id="uploader_download<?php echo $this->_tpl_vars['count']; ?>
" /><?php endif; ?>
		<?php elseif ($this->_tpl_vars['uploader_what'] == 'flash'): ?>
			<?php if ($this->_tpl_vars['uploader_file'] != ""): ?>
			<?php else: ?><?php endif; ?>
		<?php endif; ?>
		</div>
		<div class="">
			<div class="row_auto" onclick="javascript: uploader_obj.more(<?php echo $this->_tpl_vars['count']; ?>
,'<?php echo $this->_tpl_vars['uploader_what']; ?>
');"><?php echo $this->_tpl_vars['langs']['more']; ?>
</div>
			<div class="row_auto">
			<?php if ($this->_tpl_vars['uploader_file'] != ""): ?><input type="button" value="<?php echo $this->_tpl_vars['langs']['del']; ?>
" id="uploader_del<?php echo $this->_tpl_vars['count']; ?>
" class="uploader_delete" onclick="javascript: uploader_obj.del(<?php echo $this->_tpl_vars['count']; ?>
);" style="width:auto;" />
			<?php else: ?><input type="button" value="<?php echo $this->_tpl_vars['langs']['del']; ?>
" id="uploader_del<?php echo $this->_tpl_vars['count']; ?>
" style="width:auto;" class="uploader_delete_none" onclick="javascript: uploader_obj.del(<?php echo $this->_tpl_vars['count']; ?>
,'<?php echo $this->_tpl_vars['uploader_what']; ?>
');" />
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="row_hidden">
	<input type="hidden" id="bad_ext<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['langs']['bad_ext']; ?>
" />
	<input type="hidden" id="bad_size<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['langs']['bad_size']; ?>
" />
	<input type="hidden" id="bad_weight<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['langs']['bad_weight']; ?>
" />
	<input type="hidden" id="error<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['langs']['error']; ?>
" />
	<input type="hidden" id="type<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['uploader_type']; ?>
" />
	<input type="hidden" id="user_id<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
	<input type="hidden" id="close<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo $this->_tpl_vars['langs']['close']; ?>
" />
	<div id="uploader_iframe<?php echo $this->_tpl_vars['count']; ?>
" class="row_hidden"></div>
</div>