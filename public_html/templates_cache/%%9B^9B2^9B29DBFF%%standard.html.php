<?php /* Smarty version 2.6.19, created on 2017-02-06 17:32:29
         compiled from main/standard.html */ ?>
	<div class=carousel>
    	<ul class=panes>
			<li class="slide1">
				<div class="row">
					<div class="col-sm-5">
						<img src="images/layout/zachowane-faktury.jpg" alt="archiwum zachowanych faktur" class="img-responsive" />
					</div>
					<div class="slideContent col-sm-5 col-sm-offset-1  text-left">
						<span class="slogan">Łatwy program do fakturowania </span><br>
						<strong><b>Zobacz jak łatwo i szybko wystawisz faktury, dodasz kontrahenta i produkty. Nasz prosty i czytelny program do wystawiania faktur online działa już 7 lat! <br>
						Podstawowe funkcje programu to:<br>
						baza kontrahentów, baza produktów, archiwum zapisanych faktur, własne logo na fakturze, kategorie produktów,
						Szablony i wzory faktur w PDF, faktury okresowe czyli cykliczne, osobny adres dostawy towaru, nadawanie rabatów produktom lub usługom w wystawianym dokumencie,<br>
						Wybór płatności za fakturę, wysyłka efaktur mailem, kontrola płatności za faktury zapłacone, proste raporty, wybór waluty </b></strong>
						<br> 
						<br>
						<a class="button" href="http://faktura.egrupa.pl/invoice/">Wystawiaj efaktury</a>
					</div>
				</div>
			</li>
			<li class="slide2">
				<div class="row">
					<div class="col-sm-5">
						<img src="images/layout/lista-kontranetow.jpg" alt="lista zachowanych kontrahentów" class="img-responsive" />
					</div>
					<div class="slideContent col-sm-5 col-sm-offset-1  text-left">
						<span class="slogan">Twoje faktury i Kontrahenci tam gdzie Ty</span><br>
						<strong><b>Archiwum faktur online zawsze pod ręką, baza Twoich kontrahentów oraz produktów dostępna z dowolnego miejsca na świecie. <br>
						Zarządzaj swoimi klientami w prosty sposób, kiedy i gdzie tylko chcesz.</b></strong><br> 
						<a class="button" href="http://faktura.egrupa.pl/invoice/">Wystaw fakturę</a>
					</div>
				</div>
			</li>
			<li class="slide3">
				<div class="row">
					<div class="col-sm-5">
						<img src="images/layout/wysylka-efaktur.jpg" alt="wysyłka efaktur mailem" class="img-responsive" />
					</div>
					<div class="slideContent col-sm-5 col-sm-offset-1  text-left">
						<span class="slogan">Zyskaj na czasie i oszczędzaj środowisko</span><br>
						<strong><b>U nas wystawisz fakturę w 30 sekund ! Dzięki przesłanej efakturze szybciej otrzymasz zapłatę za nią oraz nie marnujesz papieru.<br>
						Efaktury to oszczędność dla Twojej firmy i dbanie o środowisko.</b></strong> <br>
						<a class="button" href="http://faktura.egrupa.pl/invoice/">Wystaw efakturę</a>
					</div>
				</div>
			</li>
			<li class="slide4">
				<div class="row">
					<div class="col-sm-5">
						<img src="images/layout/wzor-faktury-online.jpg" alt="wzór faktury online" class="img-responsive" />
					</div>
					<div class="slideContent col-sm-5 col-sm-offset-1  text-left">
						<span class="slogan">Wystawiaj faktury online za darmo bez rejestracji!</span><br> 
						<strong><b>Sprawdź jak łatwo wypełnisz wzór faktury i go wydrukujesz w PDF<br>
						Prosty wzór tworzenia faktury pozwala na łatwe wprowadzenie oraz edytowanie danych, które automatycznie się przeliczają. </b></strong><br>
						<a class="button" href="http://faktura.egrupa.pl/invoice/">Wystaw fakturę</a>
					</div>
				</div>
			</li>
			<li class="slide3">
				<div class="row">
					<div class="col-sm-5">
						<img src="images/layout/szablony-faktur.jpg" alt="gotowe szablony faktur w pdf" class="img-responsive" />
					</div>
					<div class="slideContent col-sm-5 col-sm-offset-1 text-left">
						<span class="slogan">Dopasuj wygląd Twojej faktury</span><br> 
						<strong><b>Wstaw swoje logo na fakturę, wprowadź podpis, dodaj pieczątkę i ustaw własny nagłówek na fakturze.<br>
						Wybierz nowoczesne wzory faktur w PDF, dopasuj ich kolor wg własnych upodobań.</b></strong><br>
						<a class="button" href="http://faktura.egrupa.pl/invoice/">Wystaw fakturę online</a>
					</div>
				</div>
			</li>
		</ul>
	</div>
 
 
<div class="row">
	<div class="col-sm-6">
		<div class="ramka nowosci mainPageCol">
			<h4><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</h4>
			<div id="rodzic2"></div>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="ramka centrum_pomocy mainPageCol">
			<h4><?php echo $this->_tpl_vars['lang']['pomoc_nagl']; ?>
</h4>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc1_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc1_tresc']; ?>
</p>
				<a href="pomoc/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc2_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc2_tresc']; ?>
</p>
				<a href="contact/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
			<div>
				<span class="title"><?php echo $this->_tpl_vars['lang']['pomoc3_nagl']; ?>
</span><p><?php echo $this->_tpl_vars['lang']['pomoc3_tresc']; ?>
</p>
				<a href="contact/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="ramka o_programie mainPageCol">
			<h4><?php echo $this->_tpl_vars['lang']['o_programie_nagl']; ?>
</h4>
			<span class="title"><?php echo $this->_tpl_vars['lang']['o_programie_nagl2']; ?>
</span>
			<p><?php echo $this->_tpl_vars['lang']['o_programie']; ?>
</p>
			
			<a href="o-programie/" class="more"><?php echo $this->_tpl_vars['lang']['wiecej']; ?>
</a>
		</div>
	</div>
</div>