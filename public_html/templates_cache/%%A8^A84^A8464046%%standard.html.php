<?php /* Smarty version 2.6.19, created on 2015-02-27 15:51:25
         compiled from panel/client_add/standard.html */ ?>
<div class="ramka">
	<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
 </h3>
	<script type="text/javascript" src="module/panel/client_add/class.js"></script>
	<script type="text/javascript" src="jscript/valid_data.js"></script>
	<form action="" onsubmit="javascript: return panel_client_add_obj.submit();" id="client_add_form" class="form-horizontal">
		<div class="col-sm-12">
			<div id="ok_error"  class="alert alert-success" ></div>
			<div id="error_error" class="alert alert-danger"></div>
			<div id="exist_error" class="alert alert-danger"></div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['name']; ?>
<em>*</eM>:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="name" name="name" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="mail" id="mail" value="" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['sign']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="sign" id="sign" value="" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="2" id="address" name="address" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="nip" id="nip" value="" maxlength="24" class="form-control" />
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['phone']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="phone" name="phone" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4  control-label"><?php echo $this->_tpl_vars['lang']['email']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="email" name="email" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4  control-label"><?php echo $this->_tpl_vars['lang']['krs']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="krs" id="krs" value="" maxlength="10" class="form-control" onkeypress="javascript: return valid_data.check_krs(event);" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4  control-label"><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="bank_name" id="bank_name" value="" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4  control-label"><?php echo $this->_tpl_vars['lang']['account']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="account" id="account" value="" maxlength="32" class="form-control" onkeypress="javascript: return valid_data.check_account(event);" /> 
				</div>
			</div>
		</div>
		<div class="row text-center">
		<div class="col-sm-12">
			<br>
			<button type="submit" class="btn orangeButton" ><?php echo $this->_tpl_vars['lang']['add']; ?>
</button>
			<br>
			<br>
	 
		</div>
	</div>
</form>
<div class="row_hidden">
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
	<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />     
	<input type="hidden" id="add_exist" value="<?php echo $this->_tpl_vars['lang']['add_exist']; ?>
" />
	<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
</div>
</div>