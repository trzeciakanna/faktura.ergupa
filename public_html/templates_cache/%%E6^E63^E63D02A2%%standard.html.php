<?php /* Smarty version 2.6.19, created on 2015-02-27 10:36:32
         compiled from panel/invoice/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'panel/invoice/standard.html', 153, false),array('modifier', 'replace', 'panel/invoice/standard.html', 175, false),)), $this); ?>
<script type="text/javascript" src="module/panel/invoice/class.js"></script>
<script type="text/javascript" src="jscript/valid_data.js"></script>

<?php if ($this->_tpl_vars['action'] == 'list'): ?>

	<div id="wyszukiwarka_faktur" class="ramka">
		<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>
	
				<div class="row">
			<div id="search">
				<div class="col-sm-3 form-group">
					<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
					<input type="text" id="name" value="<?php echo $this->_tpl_vars['search']['name']; ?>
" class="form-control" />
				</div>

				<div class="col-sm-3 form-group">
					<label><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
					<input type="text" id="address" value="<?php echo $this->_tpl_vars['search']['address']; ?>
" class="form-control"  />
				</div>
				<div class="col-sm-3 form-group">
					<label><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
					<input type="text" id="nip" value="<?php echo $this->_tpl_vars['search']['nip']; ?>
" maxlength="13" class="form-control"  onkeypress="javascript: return valid_data.check_nip(event);" />
				</div>
			</div>

			<?php if ($this->_tpl_vars['search']['address'] || $this->_tpl_vars['search']['nip']): ?><script type="text/javascript">panel_invoice_obj.show_search();</script><?php endif; ?>
			<div class="col-sm-3 form-group">
				<label class="col-sm-12">&nbsp; </label>
				<input type="button" value="<?php echo $this->_tpl_vars['lang']['search']; ?>
" class="btn orangeButton" id="wyszukaj_faktury" onclick="javascript: panel_invoice_obj.search();"  />
			</div>
		</div>
	</div>
	<div class="text-right">
		<div class="btn-group alphabet">
			<?php if (! $_GET['par5']): ?>
				<a href="panel/invoice/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
" class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
			<?php else: ?>
				<a href="panel/invoice/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
" class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
			<?php endif; ?>
		<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<?php if ($this->_tpl_vars['i'] == $_GET['par5']): ?><a href="panel/invoice/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
			<?php else: ?><a href="panel/invoice/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</div>
	</div>
	<table class="table table-striped table-hover table-bordered lista">
		<?php if ($this->_tpl_vars['other']): ?>
		<tr>
			<td colspan="2" style="text-align:left;"><a href="panel/invoice/list/client/0/" title="<?php echo $this->_tpl_vars['lang']['other']; ?>
" style="font-weight:bold;color:red;"><?php echo $this->_tpl_vars['lang']['other']; ?>
</a></td>
			<td><a href="panel/invoice/list/client/0/" title="<?php echo $this->_tpl_vars['lang']['invoice']; ?>
" class="invoice"><?php echo $this->_tpl_vars['lang']['invoice']; ?>
 [<?php echo $this->_tpl_vars['other']; ?>
]</a></td>
		</tr>
		<?php endif; ?>
		<tr>
		<th><?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl4']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</th>
		</tr>
		<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
		<tr <?php if (!(1 & ($this->_foreach['a']['iteration']-1)+1)): ?>class="parzysty <?php if (!(( ($this->_foreach['a']['iteration']-1)+1 ) % 10)): ?>dziesiaty<?php endif; ?>"<?php endif; ?>>
			<td><a href="panel/invoice/list/client/<?php if ($this->_tpl_vars['i']->id): ?><?php echo $this->_tpl_vars['i']->id; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']->sign; ?>
<?php endif; ?>/" title="<?php echo $this->_tpl_vars['i']->sign; ?>
"><?php echo $this->_tpl_vars['i']->sign; ?>
</a></td>
			<td><a href="panel/invoice/list/client/<?php if ($this->_tpl_vars['i']->id): ?><?php echo $this->_tpl_vars['i']->id; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']->sign; ?>
<?php endif; ?>/" title="<?php echo $this->_tpl_vars['i']->name; ?>
"><?php echo $this->_tpl_vars['i']->name; ?>
</a></td>
			<td><a href="panel/invoice/list/client/<?php if ($this->_tpl_vars['i']->id): ?><?php echo $this->_tpl_vars['i']->id; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']->sign; ?>
<?php endif; ?>/" title="<?php echo $this->_tpl_vars['lang']['invoice']; ?>
"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;      <?php echo $this->_tpl_vars['lang']['invoice']; ?>
 [<?php echo $this->_tpl_vars['i']->count; ?>
]</a></td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/invoice/list/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

		<div class="row_hidden">
		<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	</div>

<?php elseif ($this->_tpl_vars['action'] == 'client'): ?>

	<?php if ($this->_tpl_vars['client_data']->name): ?>
	<div id="dane_kontrahenta" class="ramka">
		<h3><?php if ($this->_tpl_vars['client']): ?><?php echo $this->_tpl_vars['lang']['header']; ?>
 "<?php echo $this->_tpl_vars['client']; ?>
"<?php else: ?><?php echo $this->_tpl_vars['lang']['other']; ?>
<?php endif; ?></h3>
				<?php if ($this->_tpl_vars['client_data']->name != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_name']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->name; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->address != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_address']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->address; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->nip != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_nip']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->nip; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->phone != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_phone']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->phone; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->email != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_email']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->email; ?>
</strong></div>
		</div>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['client_data']->krs != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_krs']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->krs; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->bank_name != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_bank_name']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->bank_name; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->account != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_account']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->account; ?>
</strong></div>
		</div>
		<?php endif; ?>	
	</div>	
	<?php endif; ?>
		<div class="row">
		<div class="col-sm-6">
			<h4><?php echo $this->_tpl_vars['lang']['base']; ?>
</h4>
			<table class="table table-striped table-hover table-bordered lista">
					<?php if (sizeof ( $this->_tpl_vars['base'] )): ?>
			<tr>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</th>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>	
			<th><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</th>
			<th><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</th>
			<th><?php echo $this->_tpl_vars['lang']['nagl9']; ?>
</th>	
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl12']; ?>
</th>	
			</tr>
			<?php endif; ?>
			<?php $_from = $this->_tpl_vars['base']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
			<tr  <?php if (!(( ($this->_foreach['a']['iteration']-1)+1 ) % 10)): ?>class="dziesiaty"<?php endif; ?>>
				<td class="text-center"><a href="#" onclick="javascript: return panel_invoice_obj.save_pdf(<?php echo $this->_tpl_vars['i']->id; ?>
,<?php echo $this->_tpl_vars['i']->type; ?>
);" title="<?php echo $this->_tpl_vars['lang']['download']; ?>
"><span class="glyphicon glyphicon-file"></span></a></td>
				<td  class="text-center"><a href="#" onclick="javascript: return panel_invoice_obj.send2(<?php echo $this->_tpl_vars['i']->id; ?>
,<?php echo $this->_tpl_vars['i']->type; ?>
);" title="<?php echo $this->_tpl_vars['lang']['semd']; ?>
"><span class="glyphicon glyphicon-envelope"></span></a></td>
				<td><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
 <?php echo $this->_tpl_vars['i']->number; ?>
"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Nr: <?php echo $this->_tpl_vars['i']->number; ?>
</a></td>
				<td><?php echo $this->_tpl_vars['i']->date_create; ?>
</td>
				<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']->sum)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 <?php echo $this->_tpl_vars['i']->cur; ?>
</td>
				<td class="text-center"><a href="panel/invoice/list/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['nagl12']; ?>
"><span class="glyphicon glyphicon-remove"></span></a></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			</table>
		</div>
		<div class="col-sm-6">
			<h4><?php echo $this->_tpl_vars['lang']['pdf']; ?>
</h4>
			<table  class="table table-striped table-hover table-bordered lista">
			<?php if (sizeof ( $this->_tpl_vars['file'] )): ?>
			<tr>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</th>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
			<th><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</th>
			<th><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</th>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl12']; ?>
</th>		
			</tr>
			<?php endif; ?>
			<?php $_from = $this->_tpl_vars['file']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
			<tr <?php if (!(( ($this->_foreach['a']['iteration']-1)+1 ) % 10)): ?>class="dziesiaty"<?php endif; ?>>
				<td class="text-center"><a href="#" onclick="javascript: return panel_invoice_obj.download('<?php echo $this->_tpl_vars['i']->path; ?>
');" title="<?php echo $this->_tpl_vars['lang']['download']; ?>
"><span class="glyphicon glyphicon-file"></span></a></td>
				<td class="text-center"><a href="#" onclick="javascript: return panel_invoice_obj.send('<?php echo $this->_tpl_vars['i']->path; ?>
');" title="<?php echo $this->_tpl_vars['lang']['semd']; ?>
" ><span class="glyphicon glyphicon-envelope"></span></a></td>
				<td><a href="#" onclick="javascript: return panel_invoice_obj.download('<?php echo $this->_tpl_vars['i']->path; ?>
');" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['i']->name)) ? $this->_run_mod_handler('replace', true, $_tmp, "invoice-", "") : smarty_modifier_replace($_tmp, "invoice-", "")); ?>
">Nr: <?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['i']->name)) ? $this->_run_mod_handler('replace', true, $_tmp, "invoice-", "") : smarty_modifier_replace($_tmp, "invoice-", "")))) ? $this->_run_mod_handler('replace', true, $_tmp, ".pdf", "") : smarty_modifier_replace($_tmp, ".pdf", "")); ?>
</a></td>
				<td><?php echo $this->_tpl_vars['i']->date; ?>
</td>				
				<td class="text-center"><a href="panel/invoice/list/del/<?php echo ((is_array($_tmp=$this->_tpl_vars['i']->path)) ? $this->_run_mod_handler('replace', true, $_tmp, "/", '__') : smarty_modifier_replace($_tmp, "/", '__')); ?>
/" title="<?php echo $this->_tpl_vars['lang']['nagl12']; ?>
"><span class="glyphicon glyphicon-remove"></span></a></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			</table>
		</div>
	</div>
	<div style="clear:both"></div>

		<div class="invoice_list_mail" id="panel_mail" style="position:fixed">
		<a onclick="panel_invoice_obj.close();" title="<?php echo $this->_tpl_vars['lang']['clients_close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
		<h3><?php echo $this->_tpl_vars['lang']['select']; ?>
</h3>
		<form action="" id="send_mail" onsubmit="javascript: return panel_invoice_obj.submit();">
			<?php $_from = $this->_tpl_vars['mail']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<div class="col-sm-12">
			<input type="checkbox" name="mail[]" value="<?php echo $this->_tpl_vars['i']; ?>
" id="mail<?php echo $this->_tpl_vars['i']; ?>
" /> <label for="mail<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</label>
			</div>
			<?php endforeach; endif; unset($_from); ?>
			<div class="col-sm-12"><br></div>
			<div class="col-sm-12">
				<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
				<input type="text" name="email" class="form-control" />
			</div>
			<div class="col-sm-12">
				<br>
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" class="btn orangeButton" />
				<br><br>
			</div>
			<div class="row_hidden">
				<input type="hidden" name="path" id="path" />
				<input type="hidden" name="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
			</div>
		</form>
	</div>
		<div class="invoice_list_mail" id="panel_mail2" style="position:fixed">
		<a onclick="javascript: this.parentNode.style.display='none';" title="<?php echo $this->_tpl_vars['lang']['clients_close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
		<h3><?php echo $this->_tpl_vars['lang']['select']; ?>
</h3>
		<form action="" id="send_mail2" onsubmit="javascript: return panel_invoice_obj.submit2();">
			<div class="col-sm-6">
				<span style="font-weight:bold">Wybierz bądź wprowadź adres e-mail</span>

				<?php $_from = $this->_tpl_vars['mail']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
				<div class="row_auto" style="text-align:left" id="mail_item"><input type="checkbox" name="mail[]" value="<?php echo $this->_tpl_vars['i']; ?>
" id="mail<?php echo $this->_tpl_vars['i']; ?>
" /> <label for="mail<?php echo $this->_tpl_vars['i']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</label></div>
				<?php endforeach; endif; unset($_from); ?>
				<div class="row_auto" style="text-align:left;padding-left:42px;width:auto;">
					<div class="insert_left" style="width:auto;text-align:left;"><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</div>
					<input type="text" name="email" class="form-control" />
				</div>
			</div>
			<div class="col-sm-6">
				<span style="font-weight:bold;text-align:left;">Wybierz rodzaj dokumentu</span>
				<div class="what_print_line_half"><input type="radio" name="what_p" value="1" id="print1b" checked="checked" /> <label for="print1b" ><?php echo $this->_tpl_vars['lang']['save_menu1']; ?>
</label></div>  
				<div class="what_print_line_half"><input type="radio" name="what_p" value="2" id="print2b" /> <label for="print2b" ><?php echo $this->_tpl_vars['lang']['save_menu2']; ?>
</label></div>
								<div class="what_print_line_half"><input type="radio" name="what_p" value="4" id="print4b" /> <label for="print4b" ><?php echo $this->_tpl_vars['lang']['save_menu4']; ?>
</label></div>
			</div>
			<div class="col-sm-6 col-sm-offset-6">
				<br>
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" class="btn orangeButton" />
				<br>
				<br>
			</div>
			<div class="row_hidden">
				<input type="hidden" name="path" id="path2" />
				<input type="hidden" name="fak_type" id="fak_type" value="1"/>
				<input type="hidden" name="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
			</div>
		</form>
	</div>
		<div class="invoice_client" id="what_print">
		<a onclick="javascript: this.parentNode.style.display='none';" class="close"><span class="glyphicon glyphicon-remove"></span></a> 
		<h3><?php echo $this->_tpl_vars['lang']['save_menu0']; ?>
</h3>
		<div class="what_print_line"><input type="radio" name="what_p" value="1" id="print1" checked="checked" /> <label for="print1"><?php echo $this->_tpl_vars['lang']['save_menu1']; ?>
</label></div>  
		<div class="what_print_line"><input type="radio" name="what_p" value="2" id="print2" /> <label for="print2"><?php echo $this->_tpl_vars['lang']['save_menu2']; ?>
</label></div>
				<div class="what_print_line"><input type="radio" name="what_p" value="4" id="print4" /> <label for="print4"><?php echo $this->_tpl_vars['lang']['save_menu4']; ?>
</label></div>
		<div class="text-center">
			<br>
			<input type="button" value="<?php echo $this->_tpl_vars['lang']['save_menu5']; ?>
" onclick="javascript: return panel_invoice_obj.save_pdf_end();" class="btn orangeButton" />
			<br>
			<br>
		</div>
	</div>
		<div class="row_hidden">
		<input type="hidden" id="send_ok" value="<?php echo $this->_tpl_vars['lang']['send_ok']; ?>
" />
		<input type="hidden" id="send_error" value="<?php echo $this->_tpl_vars['lang']['send_error']; ?>
" />
	</div>
<?php endif; ?>