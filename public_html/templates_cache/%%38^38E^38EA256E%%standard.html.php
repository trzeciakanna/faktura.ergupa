<?php /* Smarty version 2.6.19, created on 2015-02-28 15:41:08
         compiled from panel/invoice_period/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'panel/invoice_period/standard.html', 203, false),)), $this); ?>
<div class="ramka" id="wyszukiwarka_faktur">
	<script type="text/javascript" src="module/panel/invoice_period/class.js"></script>
	<script type="text/javascript" src="jscript/valid_data.js"></script>
	<script type="text/javascript" src="jscript/calendar.js"></script>
		<?php if ($this->_tpl_vars['client_data']->id): ?>
	<div id="dane_kontrahenta">
		<span class="naglowek" style="font-size:16px;"><?php echo $this->_tpl_vars['lang']['c_head']; ?>
: <span style="color:#111"><?php echo $this->_tpl_vars['client_data']->name; ?>
 | <a href="client/list/edit/<?php echo $this->_tpl_vars['client_data']->id; ?>
/" style="font-size:12px"><?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</a></span>
		<span id="prev_next" style="float:right">
		<?php if ($this->_tpl_vars['prev_c']): ?><a href="panel/invoice/period/<?php echo $this->_tpl_vars['prev_c']; ?>
/1/dc/" title="" class="prev"><?php echo $this->_tpl_vars['lang']['prev']; ?>
</a><?php endif; ?>
		<?php if ($this->_tpl_vars['next_c']): ?><a href="panel/invoice/period/<?php echo $this->_tpl_vars['next_c']; ?>
/1/dc/" title="" class="next"><?php echo $this->_tpl_vars['lang']['next']; ?>
</a><?php endif; ?>
		</span>
		</span>
		<?php if ($this->_tpl_vars['client_data']->name != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_name']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->name; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->address != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_address']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->address; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->nip != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_nip']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->nip; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->phone != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_phone']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->phone; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->email != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_email']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->email; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->krs != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_krs']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->krs; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->bank_name != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_bank_name']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->bank_name; ?>
</strong></div>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['client_data']->account != ''): ?>
		<div class="row">
			<div class="col-sm-2 text-right"><?php echo $this->_tpl_vars['lang']['c_account']; ?>
:</div>
			<div class="col-sm-9"><strong><?php echo $this->_tpl_vars['client_data']->account; ?>
</strong></div>
		</div>
		<?php endif; ?>
	</div>	
	<?php endif; ?>

	
		<div class="row">
		<div class="col-sm-9">
			<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>
			<h4><?php echo $this->_tpl_vars['lang']['nagl4']; ?>
 			
			<?php if ($this->_tpl_vars['client_data']->id): ?> | <a style="display:block;clear:left;" href="panel/invoice/list/client/<?php echo $this->_tpl_vars['client_data']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['page']; ?>
"><?php echo $this->_tpl_vars['lang']['page']; ?>
</a><?php endif; ?></h4>
				<div class="col-sm-4 form-group">
					<label><?php echo $this->_tpl_vars['lang']['number']; ?>
:</label>
					<input type="text" id="number" value="<?php echo $this->_tpl_vars['search']['number']; ?>
" class="form-control" />
				</div>
				<div class="col-sm-4 form-group">
					<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
					<input type="text" id="name" value="<?php echo $this->_tpl_vars['search']['name']; ?>
" class="form-control" />
				</div>
				<div class="col-sm-4 form-group">
					<label><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
					<input type="text" id="nip" value="<?php echo $this->_tpl_vars['search']['nip']; ?>
" class="form-control" onkeypress="javascript: return valid_data.check_nip(event);" />
				</div>
			<div class="invoice_period_search" id="search" style="display:none">
				<div class="col-sm-4">
					<label><?php echo $this->_tpl_vars['lang']['date_add']; ?>
:</label>
					<div>
						<div style="float:none;"><span style="float:left;width:30px;"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</span><script type="text/javascript">calendar.create('dadd_min','<?php echo $this->_tpl_vars['search']['damin']; ?>
');</script></div>
						<div style="float:none;"><span style="float:left;width:30px;"><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</span><script type="text/javascript">calendar.create('dadd_max','<?php echo $this->_tpl_vars['search']['damax']; ?>
');</script></div>
					</div>
				</div>	
				<div class="col-sm-4">
					<label><?php echo $this->_tpl_vars['lang']['date_sell']; ?>
:</label>
					<div>
						<div style="float:none;"><span style="float:left;width:30px;"><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</span><script type="text/javascript">calendar.create('dsell_min','<?php echo $this->_tpl_vars['search']['dsmin']; ?>
');</script></div>
						<div style="float:none;"><span style="float:left;width:30px;"><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</span><script type="text/javascript">calendar.create('dsell_max','<?php echo $this->_tpl_vars['search']['dsmax']; ?>
');</script></div>
					</div>
				</div>
				<div class="col-sm-4">
					<label><?php echo $this->_tpl_vars['lang']['sum']; ?>
:</label>
					<div class="row">
						<div class="col-sm-6">
							<input type="text" id="sum_min" value="<?php echo $this->_tpl_vars['search']['smin']; ?>
" class="form-control" onkeypress="javascript: return panel_invoice_period_obj.insert_prize(event,this.id);" placeholder="od"/>
						</div>
						<div class="col-sm-6">
							<input type="text" id="sum_max" value="<?php echo $this->_tpl_vars['search']['smax']; ?>
" class="form-control" onkeypress="javascript: return panel_invoice_period_obj.insert_prize(event,this.id);"  placeholder="do" />
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<a class="zwin_rozwin" id="search_show" style="clear:both;" onclick="javascript: panel_invoice_period_obj.show_search();"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
 (<?php echo $this->_tpl_vars['lang']['showed']; ?>
) <span class="glyphicon glyphicon-chevron-down"></span></a>
				<a class="zwin_rozwin" style="display:none;clear:both;" id="search_hidden" onclick="javascript: panel_invoice_period_obj.hidden_search();"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
 (<?php echo $this->_tpl_vars['lang']['hidden']; ?>
) <span class="glyphicon glyphicon-chevron-up"></span></a>
			</div>
			<div class="col-sm-6 text-right">
				<input type="button" id="wyszukaj_faktury" value="<?php echo $this->_tpl_vars['lang']['search']; ?>
" onclick="javascript: panel_invoice_period_obj.search();" class="btn orangeButton" />
			</div>
		</div>
		<div class="col-sm-3 text-left">
			<h4><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</h4>
						<div class="row">
				<div class="col-sm-6 form-group">
					<label for="month">Miesiąc:</label>
					<select id="month" class="form-control">
					<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if ($this->_tpl_vars['search']['dcm'] == $this->_tpl_vars['k']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?>
					</select>
				</div>
				<div class="col-sm-6 form-group">
					<label for="year">Rok:</label>
					<select id="year" class="form-control">
					<?php unset($this->_sections['a']);
$this->_sections['a']['name'] = 'a';
$this->_sections['a']['loop'] = is_array($_loop=$this->_tpl_vars['ys']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['a']['max'] = (int)$this->_tpl_vars['ys']-2010;
$this->_sections['a']['step'] = ((int)-1) == 0 ? 1 : (int)-1;
$this->_sections['a']['show'] = true;
if ($this->_sections['a']['max'] < 0)
    $this->_sections['a']['max'] = $this->_sections['a']['loop'];
$this->_sections['a']['start'] = $this->_sections['a']['step'] > 0 ? 0 : $this->_sections['a']['loop']-1;
if ($this->_sections['a']['show']) {
    $this->_sections['a']['total'] = min(ceil(($this->_sections['a']['step'] > 0 ? $this->_sections['a']['loop'] - $this->_sections['a']['start'] : $this->_sections['a']['start']+1)/abs($this->_sections['a']['step'])), $this->_sections['a']['max']);
    if ($this->_sections['a']['total'] == 0)
        $this->_sections['a']['show'] = false;
} else
    $this->_sections['a']['total'] = 0;
if ($this->_sections['a']['show']):

            for ($this->_sections['a']['index'] = $this->_sections['a']['start'], $this->_sections['a']['iteration'] = 1;
                 $this->_sections['a']['iteration'] <= $this->_sections['a']['total'];
                 $this->_sections['a']['index'] += $this->_sections['a']['step'], $this->_sections['a']['iteration']++):
$this->_sections['a']['rownum'] = $this->_sections['a']['iteration'];
$this->_sections['a']['index_prev'] = $this->_sections['a']['index'] - $this->_sections['a']['step'];
$this->_sections['a']['index_next'] = $this->_sections['a']['index'] + $this->_sections['a']['step'];
$this->_sections['a']['first']      = ($this->_sections['a']['iteration'] == 1);
$this->_sections['a']['last']       = ($this->_sections['a']['iteration'] == $this->_sections['a']['total']);
?><option value="<?php echo $this->_sections['a']['index']; ?>
" <?php if ($this->_tpl_vars['search']['dcy'] == $this->_sections['a']['index']): ?>selected="selected"<?php endif; ?>><?php echo $this->_sections['a']['index']; ?>
</option><?php endfor; endif; ?>
					</select>
				</div>
			</div>
			<div class="row">
				<label class="col-sm-3"><?php echo $this->_tpl_vars['lang']['type']; ?>
:</label>
				<div class="col-sm-9">
					<label for="type_c" class="do_radio"><input type="radio" name="type" id="type_c" <?php if ($this->_tpl_vars['search']['type'] == 'c'): ?>checked="checked"<?php endif; ?> /> <?php echo $this->_tpl_vars['lang']['dcreate']; ?>
</label><br />
					<input type="radio" name="type" id="type_a" <?php if ($this->_tpl_vars['search']['type'] == 'a'): ?>checked="checked"<?php endif; ?> /> <label for="type_a" class="do_radio"><?php echo $this->_tpl_vars['lang']['dadd']; ?>
</label><br />
					<input type="radio" name="type" id="type_s" <?php if ($this->_tpl_vars['search']['type'] == 's'): ?>checked="checked"<?php endif; ?>  /> <label for="type_s" class="do_radio"><?php echo $this->_tpl_vars['lang']['dsell']; ?>
</label><br />
				</div>
			</div>
			<div class="text-right">
				<input type="button" value="<?php echo $this->_tpl_vars['lang']['show']; ?>
" onclick="javascript: panel_invoice_period_obj.show();" class="btn orangeButton" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="btn-group col-sm-7">
		<?php if (! $_GET['par8']): ?>
			<a href="panel/invoice/period/<?php echo $this->_tpl_vars['par4']; ?>
/<?php echo $this->_tpl_vars['client']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['all_l']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['lang']['all_l']; ?>
</a>
		<?php else: ?>
			<a href="panel/invoice/period/<?php echo $this->_tpl_vars['par4']; ?>
/<?php echo $this->_tpl_vars['client']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['all_l']; ?>
" class="btn btn-default btn-xs"><?php echo $this->_tpl_vars['lang']['all_l']; ?>
</a>
		<?php endif; ?>
	<?php $_from = $this->_tpl_vars['months']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
		<?php if ($this->_tpl_vars['k'] == $this->_tpl_vars['search']['dcm'] && $_GET['par8']): ?><a href="#" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
		<?php else: ?><a href="#" title="<?php echo $this->_tpl_vars['i']; ?>
" onclick="javascript: return panel_invoice_period_obj.show_month('<?php echo $this->_tpl_vars['k']; ?>
');" class="btn btn-default btn-xs"><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	</div>
		<?php if (! $this->_tpl_vars['client_data']->id): ?>
	<div class="col-sm-5 text-right">
		<div class="btn-group alphabet">
			<?php if (! $_GET['par7']): ?>
				<a href="panel/invoice/period/<?php echo $this->_tpl_vars['client']; ?>
/1/dc/-/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['lang']['all_l']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['lang']['all_l']; ?>
</a>
			<?php else: ?>
				<a href="panel/invoice/period/<?php echo $this->_tpl_vars['client']; ?>
/1/dc/-/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['lang']['all_l']; ?>
" class="btn btn-default btn-xs"><?php echo $this->_tpl_vars['lang']['all_l']; ?>
</a>
			<?php endif; ?>
		<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<?php if ($this->_tpl_vars['i'] == $_GET['par7']): ?><a href="panel/invoice/period/<?php echo $this->_tpl_vars['client']; ?>
/1/dc/<?php echo $this->_tpl_vars['i']; ?>
/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-xs disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
			<?php else: ?><a href="panel/invoice/period/<?php echo $this->_tpl_vars['client']; ?>
/1/dc/<?php echo $this->_tpl_vars['i']; ?>
/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-default btn-xs" ><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		</div>
	</div>	
	<?php endif; ?>
</div>

<table class="table table-striped table-hover table-bordered lista">
	<tr>
				<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl15']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl11']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl12']; ?>
</th>
		<th><a href="panel/invoice/period/<?php echo $this->_tpl_vars['par4']; ?>
/<?php echo $this->_tpl_vars['client']; ?>
/1/<?php if ($this->_tpl_vars['sort'] == 'dc'): ?>ac<?php else: ?>dc<?php endif; ?>/<?php echo $this->_tpl_vars['par7']; ?>
/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['lang']['dcreate']; ?>
"><?php echo $this->_tpl_vars['lang']['dcreate']; ?>
 <?php if ($this->_tpl_vars['sort'] == 'dc'): ?>&uArr;<?php else: ?>&dArr;<?php endif; ?></a></th>
		<th><a href="panel/invoice/period/<?php echo $this->_tpl_vars['par4']; ?>
/<?php echo $this->_tpl_vars['client']; ?>
/1/<?php if ($this->_tpl_vars['sort'] == 'da'): ?>aa<?php else: ?>da<?php endif; ?>/<?php echo $this->_tpl_vars['par7']; ?>
/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['lang']['dadd']; ?>
"><?php echo $this->_tpl_vars['lang']['dadd']; ?>
 <?php if ($this->_tpl_vars['sort'] == 'da'): ?>&uArr;<?php else: ?>&dArr;<?php endif; ?></a></th>
		<th><a href="panel/invoice/period/<?php echo $this->_tpl_vars['par4']; ?>
/<?php echo $this->_tpl_vars['client']; ?>
/1/<?php if ($this->_tpl_vars['sort'] == 'ds'): ?>as<?php else: ?>ds<?php endif; ?>/<?php echo $this->_tpl_vars['par7']; ?>
/<?php echo $this->_tpl_vars['par8']; ?>
" title="<?php echo $this->_tpl_vars['lang']['dsell']; ?>
"><?php echo $this->_tpl_vars['lang']['dsell']; ?>
 <?php if ($this->_tpl_vars['sort'] == 'ds'): ?>&uArr;<?php else: ?>&dArr;<?php endif; ?></a></th>
		<th class="text-center"><?php echo $this->_tpl_vars['lang']['nagl14']; ?>
</th>
	</tr>
	<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
	<tr <?php if (!(1 & ($this->_foreach['a']['iteration']-1)+1)): ?>class="parzysty <?php if (!(( ($this->_foreach['a']['iteration']-1)+1 ) % 10)): ?>dziesiaty<?php endif; ?>"<?php endif; ?>  >
				<td class="text-center"><a href="panel/invoice/list/is_ok/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['is_ok']; ?>
"><span class="glyphicon glyphicon-ok"></span></a></td>
		<td><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
 <?php echo $this->_tpl_vars['i']->number; ?>
"><span class="glyphicon glyphicon-eye-open"></span> <?php echo $this->_tpl_vars['i']->number; ?>
</a></td>
		<td class="text-right"><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" ><?php echo ((is_array($_tmp=$this->_tpl_vars['i']->sum)) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł</a></td>
		<td class="text-center"><a href="panel/invoice/list/client/<?php if ($this->_tpl_vars['i']->client): ?><?php echo $this->_tpl_vars['i']->client; ?>
<?php else: ?><?php echo $this->_tpl_vars['i']->client_url; ?>
<?php endif; ?>/" title="<?php echo $this->_tpl_vars['i']->client_name; ?>
"><?php if ($this->_tpl_vars['i']->client_name): ?><?php echo $this->_tpl_vars['i']->client_name; ?>
<?php else: ?><?php echo $this->_tpl_vars['lang']['other']; ?>
<?php endif; ?> - <?php echo $this->_tpl_vars['i']->nip; ?>
</a></td>
		<td class="text-center"><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->date_create; ?>
"><?php echo $this->_tpl_vars['i']->date_create; ?>
</a></td>
		<td class="text-center"><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->date_add; ?>
"><?php echo $this->_tpl_vars['i']->date_add; ?>
</a></td>
		<td class="text-center"><a href="invoice/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->date_sell; ?>
"><?php echo $this->_tpl_vars['i']->date_sell; ?>
</a></td>
		<td class="text-center"><a href="panel/invoice/list/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
" onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/invoice/period/".($this->_tpl_vars['par4'])."/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="invoice_client" id="what_print" style="display:none;">
	<a onclick="javascript: this.parentNode.style.display='none';" ><span class="glyphicon glyphicon-remove close"></span></a> 
	<h3><?php echo $this->_tpl_vars['lang']['save_menu0']; ?>
</h3>
	<div class="what_print_line"><input type="radio" name="what_p" value="1" id="print1" checked="checked" /><label for="print1"><?php echo $this->_tpl_vars['lang']['save_menu1']; ?>
</label></div>  
	<div class="what_print_line"><input type="radio" name="what_p" value="2" id="print2" /><label for="print2"><?php echo $this->_tpl_vars['lang']['save_menu2']; ?>
</label></div>
	<div class="what_print_line"><input type="radio" name="what_p" value="3" id="print3" /><label for="print3"><?php echo $this->_tpl_vars['lang']['save_menu3']; ?>
</label></div>
	<div class="what_print_line"><input type="radio" name="what_p" value="4" id="print4" /><label for="print4"><?php echo $this->_tpl_vars['lang']['save_menu4']; ?>
</label></div>
	<div class="text-center">
		<br>
		<input type="button" class="btn orangeButton" value="<?php echo $this->_tpl_vars['lang']['save_menu5']; ?>
" onclick="javascript: return panel_invoice_period_obj.save_pdf_end();" />
		<br>
		<br>
	</div>
</div>
<div class="row_hidden">
	<input type="hidden" id="client" value="<?php echo $this->_tpl_vars['client']; ?>
" />
	<input type="hidden" id="letter" value="<?php echo $this->_tpl_vars['par7']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="send_ok" value="<?php echo $this->_tpl_vars['lang']['send_ok']; ?>
" />
	<input type="hidden" id="send_error" value="<?php echo $this->_tpl_vars['lang']['send_error']; ?>
" />
	<input type="hidden" id="send_null" value="<?php echo $this->_tpl_vars['lang']['send_null']; ?>
" />
	<input type="hidden" id="delete" value="Czy na pewno chcesz usunąć tę fakturę?" />
</div>