<?php /* Smarty version 2.6.19, created on 2015-02-27 11:25:26
         compiled from pomoc/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'pomoc/standard.html', 24, false),)), $this); ?>
<div id="strona_glowna">
		<ul id="pomoc_menu">
		<?php unset($this->_sections['a']);
$this->_sections['a']['name'] = 'a';
$this->_sections['a']['loop'] = is_array($_loop=$this->_tpl_vars['pomoc']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['a']['show'] = true;
$this->_sections['a']['max'] = $this->_sections['a']['loop'];
$this->_sections['a']['step'] = 1;
$this->_sections['a']['start'] = $this->_sections['a']['step'] > 0 ? 0 : $this->_sections['a']['loop']-1;
if ($this->_sections['a']['show']) {
    $this->_sections['a']['total'] = $this->_sections['a']['loop'];
    if ($this->_sections['a']['total'] == 0)
        $this->_sections['a']['show'] = false;
} else
    $this->_sections['a']['total'] = 0;
if ($this->_sections['a']['show']):

            for ($this->_sections['a']['index'] = $this->_sections['a']['start'], $this->_sections['a']['iteration'] = 1;
                 $this->_sections['a']['iteration'] <= $this->_sections['a']['total'];
                 $this->_sections['a']['index'] += $this->_sections['a']['step'], $this->_sections['a']['iteration']++):
$this->_sections['a']['rownum'] = $this->_sections['a']['iteration'];
$this->_sections['a']['index_prev'] = $this->_sections['a']['index'] - $this->_sections['a']['step'];
$this->_sections['a']['index_next'] = $this->_sections['a']['index'] + $this->_sections['a']['step'];
$this->_sections['a']['first']      = ($this->_sections['a']['iteration'] == 1);
$this->_sections['a']['last']       = ($this->_sections['a']['iteration'] == $this->_sections['a']['total']);
?>
			<?php if ($this->_sections['a']['index'] == $this->_tpl_vars['page']): ?>
			<li class="active">
			<?php else: ?>
			<li>
			<?php endif; ?>
				<a class="pytanie" href="/pomoc/<?php echo $this->_sections['a']['index']; ?>
/"><?php echo $this->_sections['a']['index']+1; ?>
.
				<?php if ($this->_tpl_vars['pomoc'][$this->_sections['a']['index']][0] != ''): ?>
				<?php echo $this->_tpl_vars['pomoc'][$this->_sections['a']['index']][0]; ?>

				<?php else: ?>
				<?php echo $this->_tpl_vars['pomoc'][$this->_sections['a']['index']][1]; ?>

				<?php endif; ?>
				</a>
			</li>
		<?php endfor; endif; ?>
		</ul>
		<div class="pomoc_tresc"  > 
		<div class="help_prev_next">
		<?php if ($this->_tpl_vars['page'] != 0): ?>
		<a href="/pomoc/<?php echo $this->_tpl_vars['page']-1; ?>
/" class="help_prev"><span class="glyphicon glyphicon-chevron-left"></span> Poprzedni</a>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['page'] != ( count($this->_tpl_vars['pomoc']) ) -1): ?>
		<a href="/pomoc/<?php echo $this->_tpl_vars['page']+1; ?>
/"  class="help_next">Następna <span class="glyphicon glyphicon-chevron-right"></span></a>
		<?php endif; ?>
		</div>
			<h1><?php echo $this->_tpl_vars['pomoc'][$this->_tpl_vars['page']][1]; ?>
</h1>
			<?php echo $this->_tpl_vars['pomoc'][$this->_tpl_vars['page']][2]; ?>

		</div>
<div style="clear:both;"></div>
</div>