<?php /* Smarty version 2.6.19, created on 2015-02-27 10:33:55
         compiled from ../module/pager/standard.html */ ?>
<?php if ($this->_tpl_vars['pages'] > 1): ?>
<?php echo '<nav><ul class="pagination">'; ?><?php if ($this->_tpl_vars['page'] != 1): ?><?php echo '<li><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo '1/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['first']; ?><?php echo '">&laquo;</a></li><li><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo ''; ?><?php echo $this->_tpl_vars['page']-1; ?><?php echo '/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['prev']; ?><?php echo '">&lsaquo;</a></li>'; ?><?php endif; ?><?php echo ''; ?><?php unset($this->_sections['a']);
$this->_sections['a']['name'] = 'a';
$this->_sections['a']['loop'] = is_array($_loop=$this->_tpl_vars['page']+2) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['a']['show'] = true;
$this->_sections['a']['max'] = $this->_sections['a']['loop'];
$this->_sections['a']['step'] = 1;
$this->_sections['a']['start'] = $this->_sections['a']['step'] > 0 ? 0 : $this->_sections['a']['loop']-1;
if ($this->_sections['a']['show']) {
    $this->_sections['a']['total'] = $this->_sections['a']['loop'];
    if ($this->_sections['a']['total'] == 0)
        $this->_sections['a']['show'] = false;
} else
    $this->_sections['a']['total'] = 0;
if ($this->_sections['a']['show']):

            for ($this->_sections['a']['index'] = $this->_sections['a']['start'], $this->_sections['a']['iteration'] = 1;
                 $this->_sections['a']['iteration'] <= $this->_sections['a']['total'];
                 $this->_sections['a']['index'] += $this->_sections['a']['step'], $this->_sections['a']['iteration']++):
$this->_sections['a']['rownum'] = $this->_sections['a']['iteration'];
$this->_sections['a']['index_prev'] = $this->_sections['a']['index'] - $this->_sections['a']['step'];
$this->_sections['a']['index_next'] = $this->_sections['a']['index'] + $this->_sections['a']['step'];
$this->_sections['a']['first']      = ($this->_sections['a']['iteration'] == 1);
$this->_sections['a']['last']       = ($this->_sections['a']['iteration'] == $this->_sections['a']['total']);
?><?php echo ''; ?><?php if ($this->_sections['a']['index_next'] >= $this->_tpl_vars['page']-2 && $this->_sections['a']['index_next'] <= $this->_tpl_vars['pages']): ?><?php echo ''; ?><?php if ($this->_sections['a']['index_next'] == $this->_tpl_vars['page']): ?><?php echo '<li class="active"><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo ''; ?><?php echo $this->_sections['a']['index_next']; ?><?php echo '/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['actual']; ?><?php echo '">'; ?><?php echo $this->_sections['a']['index_next']; ?><?php echo '</a></li>'; ?><?php else: ?><?php echo '<li><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo ''; ?><?php echo $this->_sections['a']['index_next']; ?><?php echo '/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['number']; ?><?php echo ''; ?><?php echo $this->_sections['a']['index_next']; ?><?php echo '">'; ?><?php echo $this->_sections['a']['index_next']; ?><?php echo '</a></li>'; ?><?php endif; ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php endfor; endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['page'] != $this->_tpl_vars['pages']): ?><?php echo '<li><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo ''; ?><?php echo $this->_tpl_vars['page']+1; ?><?php echo '/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['next']; ?><?php echo '">&rsaquo;</a></li><li><a href="'; ?><?php echo $this->_tpl_vars['prefix']; ?><?php echo ''; ?><?php echo $this->_tpl_vars['pages']; ?><?php echo '/'; ?><?php echo $this->_tpl_vars['surfix']; ?><?php echo '" title="'; ?><?php echo $this->_tpl_vars['pager']['last']; ?><?php echo '">&raquo;</a></li>'; ?><?php endif; ?><?php echo '</ul></nav>'; ?>

<?php endif; ?>