<?php /* Smarty version 2.6.19, created on 2015-02-27 10:38:12
         compiled from reg/standard.html */ ?>
<div id="invoice" class="container-fluid ramka">
	<form action="" onsubmit="javascript: return reg_obj.submit();">
		<div class="col-sm-6">
			<script type="text/javascript" src="module/reg/class.js"></script>
			<h3>Zarejestruj się</h3>

			<div class="form-group col-sm-12">
				<label><?php echo $this->_tpl_vars['lang']['login']; ?>
:</label>
				<input type="text" name="login" id="reg_login"  onchange="javascript: reg_obj.check_login();" class="form-control" />
			</div>
			<div class="col-sm-12">
				<div id="reg_success_error" class="alert alert-success"></div>
				<div id="reg_error_error" class="alert alert-danger"></div>
				<div id="reg_login_error" class="alert alert-danger"></div>
			</div>

			<div class="form-group col-sm-6">
				<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
:</label>
				<input type="password" name="pass" id="reg_pass" class="form-control" />
			</div>
			<div class="form-group col-sm-6">
				<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
 (<?php echo $this->_tpl_vars['lang']['repeat']; ?>
):</label>
				<input type="password" name="pass_repeat" id="reg_pass_repeat" class="form-control"  />
			</div>
			<div class="col-sm-12">
				<div id="reg_pass_error" class="alert alert-danger"></div>
			</div>
			<div class="form-group col-sm-6">
				<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
				<input type="text" name="mail" id="reg_mail"  onchange="javascript: reg_obj.check_mail();" class="form-control" />
			</div>
			<div class="form-group col-sm-6">
				<label><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
				<input type="text" name="nip" id="reg_nip" class="form-control" />
			</div>
			<div class="col-sm-12">
				<div id="reg_mail_error" class="alert alert-danger"></div>
				<div id="sub_errorN" class="alert alert-danger" ><?php echo $this->_tpl_vars['lang']['err_nip']; ?>
</div>
				<div id="reg_nip_error" class="alert alert-danger"></div>
			</div>
			
			<div class="col-sm-12 checkbox">
				<label for="reg_accept"><input type="checkbox" name="accept" id="reg_accept"  />  <?php echo $this->_tpl_vars['lang']['accept']; ?>
</label>
				<div id="reg_accept_error" class="alert alert-danger"></div>
			</div>
						<div class="row_hidden">
				<input type="hidden" id="regv_login" value="0" />
				<input type="hidden" id="regv_mail" value="0" />  
				<input type="hidden" id="regv_nip" value="1" />
				<input type="hidden" id="bad_login" value="<?php echo $this->_tpl_vars['lang']['bad_login']; ?>
" />
				<input type="hidden" id="bad_mail" value="<?php echo $this->_tpl_vars['lang']['bad_mail']; ?>
" />
				<input type="hidden" id="bad_pass" value="<?php echo $this->_tpl_vars['lang']['bad_pass']; ?>
" />  
				<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
				<input type="hidden" id="bad_accept" value="<?php echo $this->_tpl_vars['lang']['bad_accept']; ?>
" />
				<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
				<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
			</div>
			<div id="sub_error" class="invoice_client" style="left:50%;top:50%;display:none;margin-top:-65px;margin-left:-403px;padding:10px;position:fixed;line-height:42px;font-weight:bold;text-align:center;font-size:22px;">
					<a href="/"  title="<?php echo $this->_tpl_vars['lang']['close']; ?>
" class="close"><span class="glyphicon glyphicon-remove"></span></a>
			<?php echo $this->_tpl_vars['lang']['add_ok']; ?>


			</div>
		</div>
		<div class="col-sm-6">
			<h3>Przeczytaj zanim założysz nowe konto!</h3>
			<p><img src="images/layout/ssl-icon.png" style="float:left" width="60"/> Jako login możesz użyć swojego nazwiska, nazwy firmy, adresu email lub innego unikalnego słowa.</p>
			<p>Jedna firma może założyć tylko jedno konto na jeden NIP !!! Wszelkie konta zdublowane są blokowane przez nasz system.</p>
			<p>Rejestracja zapewnia miesięczny, bezpłatny dostęp do pełnej wersji programu.<br> Po upływie 30 dni można dalej korzystać bezpłatnie z wersji podstawowej.</p>
			<p>Zalecamy aby hasło składało się z minimum 8 znaków.<br> Uwaga! System rozróżnia małe i wielkie litery, oznacza to, że hasło "xyz123" nie jest tym samym hasłem co "XYZ123". </p>
			<p>Wszelkie dane wpisywane przez użytkowników w naszym serwisie przetwarzane są przez Internet w formie zaszyfrowanej, przy użyciu protokołu SSL ze 256-bitowym kluczem szyfrującym.</p>
<br><br>
				<button type="submit"  class="btn orangeButton btn-lg" ><?php echo $this->_tpl_vars['lang']['send']; ?>
</button> <br><br>
		</div>
		<div class="col-sm-6 col-sm-offset-6"><h3>Potrzebujesz więcej opcji, utwórz konto w naszym nowym serwisie: <a href="http://afaktury.pl/">afaktury.pl</a></h3><br><br><br><br></div>
	</form>
</div>