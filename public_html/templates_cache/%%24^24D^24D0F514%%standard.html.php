<?php /* Smarty version 2.6.19, created on 2015-02-27 10:34:23
         compiled from panel/product_add/standard.html */ ?>
<div class="ramka">
	<script type="text/javascript" src="module/panel/product_add/class.js"></script>
	<form action="" onsubmit="javascript: return panel_product_add_obj.submit();" id="product_add_form" class="form-horizontal">
		<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>
		<div id="add_error" class="alert alert-success"></div>
		<div id="error_error" class="alert alert-danger"></div>

		<div class="row">
			<div class="col-sm-6">
				<div>
					<label><?php echo $this->_tpl_vars['lang']['name']; ?>
<span style="color:#f00">*</span>:</label>	
					<textarea  name="name" id="name" class="form-control" rows="1" /></textarea>
					<div id="name_error" class="alert alert-danger"></div>
				</div>
				<div>	
					<label><?php echo $this->_tpl_vars['lang']['producer']; ?>
:</label>
					<input type="text" name="producer" id="producer" value="" class="form-control" />
				</div>
				<div>
					<label><?php echo $this->_tpl_vars['lang']['cat']; ?>
<span style="color:#f00">*</span></label><a href="panel/product/cat/" style="float:right"> <?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</a>
					<select name="cat" id="cat" class="form-control">
						<option value="">---</option>
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "panel/product_add/standard_list.html", 'smarty_include_vars' => array('tree' => $this->_tpl_vars['tree'],'select' => $this->_tpl_vars['cat'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					</select>
					<div id="cat_error" class="alert alert-danger"></div>
				</div>
				<div>
					<label><?php echo $this->_tpl_vars['lang']['number']; ?>
:</label>
					<input type="text" name="number" id="number" value="" class="form-control" />
				</div>
			</div>
			<div class="col-sm-6">
				<div>
					<label><?php echo $this->_tpl_vars['lang']['pkwiu']; ?>
:</label>
					<input type="text" name="pkwiu" id="pkwiu" value="" class="form-control" />
				</div>
				<div>
					<label><?php echo $this->_tpl_vars['lang']['unit']; ?>
:</label>
					<select name="unit" id="unit" class="form-control">
						<option value="">---</option>
							<?php $_from = $this->_tpl_vars['units']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
 [<?php echo $this->_tpl_vars['k']; ?>
]</option><?php endforeach; endif; unset($_from); ?>
					</select>
				</div>

				<div>
					<label><?php echo $this->_tpl_vars['lang']['vat']; ?>
<span style="color:#f00">*</span>:</label>
					<select name="vat" id="vat" class="form-control" onchange="javascript: panel_product_add_obj.change_netto();">
						<option value="">---</option>
						<option value="0">0 %</option>
						<option value="3">3 %</option>
						<option value="4">4 %</option>
						<option value="5">5 %</option>
						<option value="6">6 %</option>
						<option value="7">7 %</option>
						<option value="8">8 %</option>
						<option value="18">18 %</option>
						<option value="19">19 %</option> 
						<option value="20">20 %</option>
						<option value="22">22 %</option> 
						<option value="23">23 %</option> 
						<option value="NP">NP</option> 
						<option value="ZW">ZW</option>  
						<option value="-">-</option>
					</select>
					<div id="vat_error" class="alert alert-danger"></div>
				</div>	
				<div>
					<label><?php echo $this->_tpl_vars['lang']['netto']; ?>
<span style="color:#f00">*</span>:</label>
					<input type="text" name="netto" id="netto" value="" class="form-control" onkeypress="javascript: return panel_product_add_obj.insert_prize(event,'netto');" onkeyup="javascript: panel_product_add_obj.change_netto();" />
					<div id="cena_error" class="alert alert-danger"></div>
				</div>
				<div>
					<label><?php echo $this->_tpl_vars['lang']['brutto']; ?>
<span style="color:#f00">*</span>:</label>
					<input type="text" name="brutto" id="brutto" value="" class="form-control" onkeypress="javascript: return panel_product_add_obj.insert_prize(event,'brutto');" onkeyup="javascript: panel_product_add_obj.change_brutto();" />
				</div><br>
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['add']; ?>
" class="btn orangeButton" />&nbsp;    <span style="color:#f00">*</span> <?php echo $this->_tpl_vars['lang']['info']; ?>

				<br><br><br>
			</div>
		</div>
	</form>

		<div class="row_hidden">
		<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
		<input type="hidden" id="empty_cat" value="<?php echo $this->_tpl_vars['lang']['empty_cat']; ?>
" />
		<input type="hidden" id="empty_name" value="<?php echo $this->_tpl_vars['lang']['empty_name']; ?>
" />
		<input type="hidden" id="empty_vat" value="<?php echo $this->_tpl_vars['lang']['empty_vat']; ?>
" />
		<input type="hidden" id="empty_prize" value="<?php echo $this->_tpl_vars['lang']['empty_prize']; ?>
" />
		<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
		<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
	</div>
</div>