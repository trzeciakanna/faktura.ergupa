<?php /* Smarty version 2.6.19, created on 2015-04-23 15:46:43
         compiled from panel/raport_sell/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date', 'panel/raport_sell/standard.html', 10, false),array('modifier', 'string_format', 'panel/raport_sell/standard.html', 50, false),)), $this); ?>

<div class="row">
	<div class="col-sm-12">
	<h1 class="col-sm-12"><?php echo $this->_tpl_vars['lang']['head']; ?>
</h1>
	<div class="col-sm-4">
    <table class="table table-striped table-hover table-bordered lista">
	<tr><th><?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</th></tr>
	<tr><td><a href="panel/raport/sell/year/" style="font-weight:bold;padding:5px;display:block;"><?php echo $this->_tpl_vars['lang']['year']; ?>
</a></td></tr>
	<?php $_from = $this->_tpl_vars['dane2']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<?php if ($this->_tpl_vars['i'] != 0000 && $this->_tpl_vars['i'] <= ( ((is_array($_tmp='Y')) ? $this->_run_mod_handler('date', true, $_tmp) : date($_tmp)) )): ?>
	<tr><td<?php if ($_GET['par4'] == $this->_tpl_vars['i']): ?> style=background:#539ccc;" <?php endif; ?> ><a href="panel/raport/sell/<?php echo $this->_tpl_vars['i']; ?>
/" style="font-weight:bold;padding:5px;display:block;<?php if ($_GET['par4'] == $this->_tpl_vars['i']): ?> color:#fff; <?php endif; ?>" class=""><?php echo $this->_tpl_vars['lang']['month']; ?>
 <?php echo $this->_tpl_vars['i']; ?>
</a></td></tr>
	<?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>

	
	</table>
	</div>
<?php if ($this->_tpl_vars['action'] == 'list'): ?>
	<?php elseif ($this->_tpl_vars['action'] == 'year'): ?>
		<div class="col-sm-6 col-sm-offset-2">
				<table class="table table-striped table-hover table-bordered lista" style="margin-bottom:20px;">
		<tr><th colspan="5"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</th></tr>
		<tr>
		<th><?php echo $this->_tpl_vars['lang']['nagl6']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl9']; ?>
</th>
		<th><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
		</tr>
		

		<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
		<tr><th><?php echo $this->_tpl_vars['i']['year']; ?>
</th>  
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['netto'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['brutto'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['vat'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł </td> 
		<td><?php echo $this->_tpl_vars['i']['ile']; ?>
 </td></tr>
		

		<?php endforeach; endif; unset($_from); ?>
		</table>
	</div>
<?php elseif ($this->_tpl_vars['action'] == 'month'): ?>
	<div class="col-sm-6 col-sm-offset-2">
		
	<table class="table table-striped table-hover table-bordered lista">
	<tr><th colspan="5"><?php echo $this->_tpl_vars['lang']['nagl5']; ?>
</th></tr>
	<tr>
	<th><?php echo $this->_tpl_vars['lang']['nagl11']; ?>
</th>
	<th class="text-right"><?php echo $this->_tpl_vars['lang']['nagl7']; ?>
</th>
	<th class="text-right"><?php echo $this->_tpl_vars['lang']['nagl8']; ?>
</th>
	<th class="text-right"><?php echo $this->_tpl_vars['lang']['nagl9']; ?>
</th>
	<th><?php echo $this->_tpl_vars['lang']['nagl10']; ?>
</th>
	</tr>
	<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?>
	<tr>
		<th><?php echo $this->_tpl_vars['lang'][$this->_tpl_vars['k']]; ?>
</th>
		<td class="text-right"> <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['netto'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł</td>
		<td class="text-right"> <?php echo ((is_array($_tmp=$this->_tpl_vars['i']['brutto'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł</td>
		<td class="text-right"><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['vat'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 zł </td>
		<td><?php echo $this->_tpl_vars['i']['ile']; ?>
 </td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>
	</div>
<?php endif; ?>
	</div>
</div>
</div>