<?php /* Smarty version 2.6.19, created on 2016-07-26 12:46:17
         compiled from panel/szablon-faktury/standard.html */ ?>
<script type="text/javascript" src="module/panel/szablon-faktury/class.js"></script> 
<div id="wyszukiwarka_faktur" class="ramka">
	<h3><?php echo $this->_tpl_vars['lang']['head']; ?>
</h3>

	<div style="padding:0 1.5%">
	<input id="gallery_count" value="0" type="hidden">
	<input id="gallery_max" value="0" type="hidden">
	<input id="gallery_width_0" value="500" type="hidden">
	<input id="gallery_height_0" value="710" type="hidden">	
	<input type="hidden" id="gallery_width" value="64" />
	<input type="hidden" id="gallery_height" value="0" />
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "gallery_tpl/standard.html", 'smarty_include_vars' => array('k' => 0,'max' => $this->_tpl_vars['max'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
	
<div > 
<form id="szablon">
	<div class="btn-group">
		<a href="panel/data/szablon-faktury/1/" title="" class="btn <?php if ($_GET['par4'] == 1): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay1']; ?>
</a>
		<a href="panel/data/szablon-faktury/2/" title="" class="btn <?php if ($_GET['par4'] == 2): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay2']; ?>
</a>
		<a href="panel/data/szablon-faktury/3/" title="" class="btn <?php if ($_GET['par4'] == 3): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay3']; ?>
</a>
		<a href="panel/data/szablon-faktury/4/" title="" class="btn <?php if ($_GET['par4'] == 4): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay4']; ?>
</a>
		<a href="panel/data/szablon-faktury/5/" title="" class="btn <?php if ($_GET['par4'] == 5): ?>active<?php endif; ?> btn-default"><?php echo $this->_tpl_vars['lang']['lay5']; ?>
</a>
	</div>
	<br>
	<span id="save_ok_error"  style="color:green;font-weight:bold;display:none;background:transparent;"></span><br>
	<span id="save_bad_error"  style="color:red;font-weight:bold;display:none;background:transparent;"></span><br>
	
<div class="row">
	<div class="col-sm-4" style="border-right:1px solid #dfdfdf;">
		<div class="row">
			<div class="col-sm-8">
				<h4>Aktualny szablon</h4>
			</div>
			<div class="col-sm-4"><button type="submit" class="btn blueButton" onclick="javascript: return panel_data_template_obj.save_example();"><?php echo $this->_tpl_vars['lang']['show_example']; ?>
</button> </div>
		</div>
		
		<a><img src="" onclick="javascript: gallery_s.open(this,0);"  id="template_big_img" class="img-responsive"></a>
		<br/>
	</div>
	
	<div class="col-sm-8" >
		<h4>Wybierz szablon</h4>
		<div class="row text-center">
			<div class="col-sm-3">
				<a onclick="javascript: return panel_data_template_obj.change_template(1,'');"><img src="images/layout/szablony/min/szablon1.png" id="l1"><br><strong>Szablon główny</strong></a>
			</div>
			<div class="col-sm-3">
				<a onclick="javascript: return panel_data_template_obj.change_template(2,'');"><img src="images/layout/szablony/min/szablon2s.png" id="l2"><br><strong>Szablon 2</strong></a>
			</div>
			<div class="col-sm-3">
				<a onclick="javascript: return panel_data_template_obj.change_template(3,'');" ><img src="images/layout/szablony/min/szablon3s.png" id="l3"><br><strong>Szablon 3</strong></a>
			</div>	
		</div>
		<div class="row" style="padding-top:30px;">
			<div class="kolory_szablonu col-sm-3" style="text-align:left;">
			<h4>Wybierz kolor</h4>
			<a class="<?php if ($this->_tpl_vars['data']->color == 4): ?> kolor_active<?php else: ?>kolor<?php endif; ?>" id="kolorystyka4" onclick="javascript: return panel_data_template_obj.change_color(4,'');"> </a>
			<a class="<?php if ($this->_tpl_vars['data']->color == 1): ?>kolor_active<?php else: ?>kolor<?php endif; ?>" id="kolorystyka1" onclick="javascript: return panel_data_template_obj.change_color(1,'');"></a>
			<a class="<?php if ($this->_tpl_vars['data']->color == 2): ?> kolor_active<?php else: ?>kolor<?php endif; ?>" id="kolorystyka2" onclick="javascript: return panel_data_template_obj.change_color(2,'');"></a>
			<a class="<?php if ($this->_tpl_vars['data']->color == 3): ?> kolor_active<?php else: ?>kolor<?php endif; ?>" id="kolorystyka3" onclick="javascript: return panel_data_template_obj.change_color(3,'');"> </a>
			<a class="<?php if ($this->_tpl_vars['data']->color == 5): ?> kolor_active<?php else: ?>kolor<?php endif; ?>" id="kolorystyka5" onclick="javascript: return panel_data_template_obj.change_color(5,'');"> </a>
			</div>	
			<div class="col-sm-5">
				<h4>Nagłowki</h4>
				<div class="checkbox">
				<label style="font-size:11px;"><input value="1" type="checkbox" name="pokaz_naglowki" id="pokaz_naglowki" <?php if ($this->_tpl_vars['data']->nagl == 1): ?>checked="checked" <?php endif; ?>/> <strong>Pokaż nagłówki</strong> (nazwa firmy, adres, telefon itd.)</label>
				</div>
			</div>
			<div class="col-sm-2">
				<br>
				<button type="submit" class="btn orangeButton" onclick="javascript: return panel_data_template_obj.save_data();" ><?php echo $this->_tpl_vars['lang']['save']; ?>
</button>
			</div>
		</div>	
	</div>


</div>

<input type="hidden" id="user_id" name="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
<input type="hidden" id="szablon_id" name="szablon_id" value="<?php echo $this->_tpl_vars['szablon']['id']; ?>
" />
<input type="hidden" id="kolorystyka" name="kolorystyka" value="<?php echo $this->_tpl_vars['data']->color; ?>
" >
<input type="hidden" id="kolor_ramki" name="kolor_ramki" value="<?php echo $this->_tpl_vars['data']->colors['kolor_ramki']; ?>
" >
<input type="hidden" id="kolor_tekstu" name="kolor_tekstu" value="<?php echo $this->_tpl_vars['data']->colors['kolor_tekstu']; ?>
" />
<input type="hidden" id="kolor_tekstu_nagl" name="kolor_tekstu_nagl" value="<?php echo $this->_tpl_vars['data']->colors['kolor_tekstu_nagl']; ?>
" />
<input type="hidden" id="kolor_tla_nagl"  name="kolor_tla_nagl" value="<?php echo $this->_tpl_vars['data']->colors['kolor_tla_nagl']; ?>
" />
<input type="hidden" id="kolor_dodatkowy" name="kolor_dodatkowy" value="<?php echo $this->_tpl_vars['data']->colors['kolor_dodatkowy']; ?>
" />
<input type="hidden" id="jaka_faktura" name="jaka_faktura" value="<?php echo $this->_tpl_vars['szablon']['jaka_faktura']; ?>
" />

</form>
</div>
</div>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>
<div style="clear:both;width:100%;height:20px;" ></div>


<script type="text/javascript">
panel_data_template_obj.change_template(<?php echo $this->_tpl_vars['data']->tpl; ?>
, <?php echo $this->_tpl_vars['data']->color; ?>
);
panel_data_template_obj.change_color(<?php echo $this->_tpl_vars['data']->color; ?>
, <?php echo $this->_tpl_vars['data']->tpl; ?>
);

</script>

