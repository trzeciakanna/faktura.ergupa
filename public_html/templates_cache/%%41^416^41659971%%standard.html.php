<?php /* Smarty version 2.6.19, created on 2017-02-02 19:11:43
         compiled from o-programie/standard.html */ ?>
<div id="invoice">
Nasz program do wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, rachunek lub fakturę zaliczkową.
Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych, dzięki czemu jest elastyczny i dopasowany do potrzeb każdego przedsiębiorcy.<br>
Program daje możliwość zapisania stworzonej faktury w formacie PDF, wydrukowanie tej faktury zaraz po jej utworzeniu lub wysyłki efaktury bezpośrednio do kontrahenta.
Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, prosta kontrola płatności za wystawione faktury i wiele wiele innych przydatnych opcji.
Opcje te ułatwiają prowadzenie i  zarządzenie firmą.<br>
<br>
Każdy korzystający z programu faktura.egrupa.pl może wpisać i zapamiętać przez system swoje dane, które mają się wyświetlać na fakturach łącznie z numerami kont, logo, stopką. Dzięki temu, wystawiając dokument zawsze dane sprzedawcy będą wpisane.
Bardzo pomocna jest możliwość wystawienia i wysłania efaktury niemalże z każdego miejsca i urządzenia. Program do wystawiania faktur nie jest ograniczony do jednego komputera co daje nam możliwość korzystania z programu do wystawiania faktur w każdym miejscu, w którym się znajdujemy. Szybki podgląd do wystawionych faktur i dokumentów, dzięki liście faktur, ułatwia kontrolowanie naszą sprzedażą towarów i usług.<br>
<br>
Możliwość dodania i zapisania kontrahentów oraz produktów a następnie zaciąganie ich z bazy to opcja bardzo przydatna, zwłaszcza dla tych przedsiębiorców, którzy często wystawiają faktury stałym klientom i sprzedają określone towary lub usługi. Korzystając z tego ułatwienia, fakturę wystawia się bardzo szybko, pozwala na uniknięcie błędów.
Natomiast jeżeli ów błąd wedrze się na dokument, dzięki możliwości edytowaniu faktury, możemy go zniwelować bez potrzeby wypisywania od nowa.<br>
<br>
Poszukiwaną opcją, która ułatwia i przyspiesza wystawianie faktur co miesiąc dla tych samych klientów, z tym samym tematem sprzedaży, są faktury cykliczne. Faktury cykliczne ustawia się nadając im parametry, z jaką częstotliwością mają się generować i dla kogo. Wówczas w określonym dniu faktura taka się wygeneruje. Wystarczy ją tylko zatwierdzić i wysłać jako np. efakturę do kontrahenta.<br>
<br>
Każdy dokument, faktury vat, fakty zaliczkowe, faktury proformy i wszystkie pozostałe można bezpośrednio z programu wysłać do klienta jako edokument. Ta opcja zdecydowanie przyspiesza transakcje i rozliczenia. Dzięki niej zaoszczędzamy również czas i pieniądze, które są potrzebne na wysyłkę tradycyjną.
Obowiązujące, zmienione prawo polskie, akceptuje tą formę przesyłu i wystawiane efaktur. Dzięki przychylności ustawom są uznawane przez podmioty gospodarcze i urząd skarbowy.
</div>