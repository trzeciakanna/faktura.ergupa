<?php /* Smarty version 2.6.19, created on 2015-02-27 12:56:38
         compiled from panel/client_list/standard.html */ ?>
<script type="text/javascript" src="module/panel/client_list/class.js"></script>
<script type="text/javascript" src="jscript/valid_data.js"></script>
<?php if ($this->_tpl_vars['action'] == 'list'): ?>
<div id="wyszukiwarka_faktur" class="ramka">
	<div class="row">
		<div class="col-sm-9"><h3><?php echo $this->_tpl_vars['lang']['nagl3']; ?>
</h3></div>
		<div class="col-sm-3 text-right"><a href="panel/client/add/" class="btn blueButton" >Dodaj nowego kontrahenta</a></div>

		
			</div>	
	<div id="search" class="row">
		<div class="col-sm-3 form-group">
			<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
			<input type="text" id="name" value="<?php echo $this->_tpl_vars['search']['name']; ?>
"class="form-control" />
		</div>
		<div class="col-sm-3">
			<label><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
			<input type="text" id="address" value="<?php echo $this->_tpl_vars['search']['address']; ?>
"class="form-control" />
		</div>
		<div class="col-sm-3">
			<label><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
			<input type="text" id="nip" value="<?php echo $this->_tpl_vars['search']['nip']; ?>
" maxlength="13"class="form-control"  />
		</div>
		<div class="col-sm-3 form-group">
			<label class="col-sm-12">&nbsp;</label>
			<input type="button" value="<?php echo $this->_tpl_vars['lang']['search']; ?>
"  class="btn orangeButton" id="wyszukaj_faktury" onclick="javascript: panel_client_list_obj.search();" />
		</div>
	</div>

	<?php if ($this->_tpl_vars['search']['address'] || $this->_tpl_vars['search']['nip']): ?><script type="text/javascript">panel_client_list_obj.show_search();</script><?php endif; ?>
</div>

<div class="text-right"> 
	<div class="btn-group alphabet">
		<?php if (! $_GET['par5']): ?>
		<a href="panel/client/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
" class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
		<?php else: ?>
		<a href="panel/client/list/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
" class="btn btn-default btn-sm"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
		<?php endif; ?>
	<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
		<?php if ($this->_tpl_vars['i'] == $_GET['par5']): ?><a href="panel/client/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-primary btn-sm disabled"><?php echo $this->_tpl_vars['i']; ?>
</a>
		<?php else: ?><a href="panel/client/list/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="btn btn-default btn-sm" ><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	</div>
</div>
<?php if ($this->_tpl_vars['dane']): ?>
	<table class="table table-striped table-hover table-bordered lista">
	<tr>
	<th><?php echo $this->_tpl_vars['lang']['table_nagl1']; ?>
</th>
	<th><?php echo $this->_tpl_vars['lang']['table_nagl2']; ?>
</th>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['invoice']; ?>
</th>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['branch']; ?>
</th>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['edit']; ?>
</th>
	<th class="text-center"><?php echo $this->_tpl_vars['lang']['del']; ?>
</th>
	<tr>
	<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<tr>
		<td><a href="panel/invoice/search/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['page']; ?>
 <?php echo $this->_tpl_vars['i']->sign; ?>
"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp; <?php echo $this->_tpl_vars['i']->sign; ?>
</a></td>
		<td><a href="panel/invoice/search/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->name; ?>
"><?php echo $this->_tpl_vars['i']->name; ?>
</a></td>
		<td class="text-center"><a href="panel/invoice/list/client/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['invoice']; ?>
"><span class="glyphicon glyphicon-folder-open"></span></a></td>
		<td class="text-center"><a href="panel/client/list/branch/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['branch']; ?>
"><span class="glyphicon glyphicon-user"></span></a></td>
		<td class="text-center"><a href="panel/client/list/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
" class="edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
		<td class="text-center"><a href="panel/client/list/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
"  onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>
<?php else: ?>
	<div class="noResults">
	</div>
<?php endif; ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "panel/client/list/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div class="row_hidden">
	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>
<div class="ramka">
	<h3><?php echo $this->_tpl_vars['lang']['nagl4']; ?>
</h3>
	<form action="" onsubmit="javascript: return panel_client_list_obj.submit(<?php echo $this->_tpl_vars['user']->id; ?>
);" id="client_list_form" class="form-horizontal">
		<div id="ok_error" class="alert alert-success"></div>
		<div id="error_error" class="alert alert-danger"></div>
		<div id="exist_error" class="alert alert-danger"></div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="name" name="name"class="form-control"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="mail" id="mail" value="<?php echo $this->_tpl_vars['user']->mail; ?>
" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['sign']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="sign" id="sign" value="<?php echo $this->_tpl_vars['user']->sign; ?>
" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="2" id="address" name="address"class="form-control"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="nip" id="nip" value="<?php echo $this->_tpl_vars['user']->nip; ?>
" maxlength="24"class="form-control" />
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['phone']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="phone" name="phone"class="form-control"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['email']; ?>
:</label>
				<div class="col-sm-8">
					<textarea cols="" rows="1" id="email" name="email"class="form-control"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['krs']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="krs" id="krs" value="<?php echo $this->_tpl_vars['user']->krs; ?>
" maxlength="10"class="form-control" onkeypress="javascript: return valid_data.check_krs(event);" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="bank_name" id="bank_name" value="<?php echo $this->_tpl_vars['user']->bank_name; ?>
"class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['account']; ?>
:</label>
				<div class="col-sm-8">
					<input type="text" name="account" id="account" value="<?php echo $this->_tpl_vars['user']->account; ?>
" maxlength="32"class="form-control" onkeypress="javascript: return valid_data.check_account(event);" />
				</div>
			</div>
		</div>
				<div class="row text-center">
			<div class="col-sm-12">
				<br>
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['user']->id; ?>
" />
				<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="btn btn-warning" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn btn-primary" />
				<br><br>
			</div>
		</div>
	</form>
</div>
<div class="row_hidden">
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>

<?php elseif ($this->_tpl_vars['action'] == 'branch'): ?>
<div class="ramka">
	<br>
	<div class="row">
		<div class="col-sm-4"><h3><?php echo $this->_tpl_vars['lang']['branch']; ?>
</h3></div><div class="col-sm-2 text-right"><a href="panel/client/list/badd/<?php echo $_GET['par5']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['addb']; ?>
" class="btn orangeButton"><?php echo $this->_tpl_vars['lang']['addb']; ?>
</a></div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<table class="table table-striped table-hover table-bordered lista">
			<tr>
			<th>Nazwa skrócona</th>
			<th><?php echo $this->_tpl_vars['lang']['table_nagl3']; ?>
</th>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['edit']; ?>
</th>
			<th class="text-center"><?php echo $this->_tpl_vars['lang']['del']; ?>
</th>
			</tr>
			<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
			<tr>
				<td><a href="panel/invoice/search/<?php echo $_GET['par5']; ?>
/" title="<?php echo $this->_tpl_vars['i']->sign; ?>
"><?php echo $this->_tpl_vars['i']->sign; ?>
</a></td>
				<td><a href="panel/invoice/search/<?php echo $_GET['par5']; ?>
/" title="<?php echo $this->_tpl_vars['i']->name; ?>
"><?php echo $this->_tpl_vars['i']->name; ?>
</a></td>
				<td class="text-center"><a href="panel/client/list/bedit/<?php echo $_GET['par5']; ?>
/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
"><span class="glyphicon glyphicon-pencil"></span></a></td>
				<td class="text-center"><a href="panel/client/list/bdel/<?php echo $_GET['par5']; ?>
/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" onclick="javascript: return confirm_del();"><span class="glyphicon glyphicon-remove"></span></a></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			</table>
		</div>
	</div>
	<div class="row_hidden">
		<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['deleteb']; ?>
" />
	</div>
	<br><br><br><br>
</div>	

<?php elseif ($this->_tpl_vars['action'] == 'badd'): ?>

	<div class="ramka">
		<h3><?php echo $this->_tpl_vars['lang']['addb']; ?>
</h3>
				<form action="" onsubmit="javascript: return panel_client_list_obj.addb();" id="client_list_form" class="form-horizontal">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['sign']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="sign" id="sign" value="<?php echo $this->_tpl_vars['user']->sign; ?>
" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="name" name="name" class="form-control"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"> <?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="2" id="address" name="address" class="form-control"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['phone']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="phone" name="phone" class="form-control"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['email']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="email" name="email" class="form-control"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['krs']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="krs" id="krs" value="<?php echo $this->_tpl_vars['user']->krs; ?>
" maxlength="10" class="form-control" onkeypress="javascript: return valid_data.check_krs(event);" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="bank_name" id="bank_name" value="<?php echo $this->_tpl_vars['user']->bank_name; ?>
" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['account']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="account" id="account" value="<?php echo $this->_tpl_vars['user']->account; ?>
" maxlength="32" class="form-control" onkeypress="javascript: return valid_data.check_account(event);" />
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-sm-12">
					<br>
					<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="btn btn-warning" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn btn-primary" />
					<br><br>
				</div>		
			</div>
		</form>
				<div class="row_hidden">
			<input type="hidden" id="client_id" value="<?php echo $_GET['par5']; ?>
" />
			<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
			<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
			<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
			<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['addb_ok']; ?>
" />
			<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['addb_error']; ?>
" />
		</div>
	</div>
	
<?php elseif ($this->_tpl_vars['action'] == 'bedit'): ?>

	<div class="ramka">
		<h3><?php echo $this->_tpl_vars['lang']['branch_data']; ?>
 </h3>
				<form action="" onsubmit="javascript: return panel_client_list_obj.saveb(<?php echo $_GET['par6']; ?>
);" id="client_list_form" class="form-horizontal">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['sign']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="sign" id="sign" value="<?php echo $this->_tpl_vars['user']->sign; ?>
" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="name" name="name" class="form-control"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="2" id="address" name="address" class="form-control"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['phone']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="phone" name="phone" class="form-control"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['email']; ?>
:</label>
					<div class="col-sm-8">
						<textarea cols="" rows="1" id="email" name="email" class="form-control"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['krs']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="krs" id="krs" value="<?php echo $this->_tpl_vars['user']->krs; ?>
" maxlength="10" class="form-control" onkeypress="javascript: return valid_data.check_krs(event);" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="bank_name" id="bank_name" value="<?php echo $this->_tpl_vars['user']->bank_name; ?>
" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label"><?php echo $this->_tpl_vars['lang']['account']; ?>
:</label>
					<div class="col-sm-8">
						<input type="text" name="account" id="account" value="<?php echo $this->_tpl_vars['user']->account; ?>
" maxlength="32" class="form-control" onkeypress="javascript: return valid_data.check_account(event);" />
					</div>
				</div>
			</div>	
						<div class="row text-center">
				<div class="col-sm-12">
					<br>
					<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="btn btn-warning" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="btn btn-primary" />
					<br><br>
				</div>		
			</div>
		</form>
	</div>
		<div class="row_hidden">
		<input type="hidden" id="client_id" value="<?php echo $_GET['par5']; ?>
" />
		<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
		<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
		<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
		<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['saveb_ok']; ?>
" />
		<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['saveb_error']; ?>
" />
	</div>
<?php endif; ?>