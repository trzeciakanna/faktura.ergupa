<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',6600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
$tmp=explode("/",$_SERVER['SCRIPT_FILENAME']);
$tmp2=explode("/",$_SERVER['REQUEST_URI']);
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("PAGE_URL",str_replace(".php","",$tmp[sizeof($tmp)-1]));
define("MENU_URL",str_replace(".php","",$tmp2[1]));
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();

define("USER_ID",1);

/* kod właściwy */
require(ROOT_DIR."_api/PHPExcel.php");
$exc=head();
$exc=clients($exc,USER_ID);
$exc=products($exc,USER_ID);
$exc=categories($exc,USER_ID);
$exc=invnoices($exc,USER_ID);

foot($exc);





function head()
{
	//utworzenie obiektu pliku
	$exc=new PHPExcel();
	$exc->getProperties()->setCreator("Faktura Egrupa.pl");
	$exc->getProperties()->setTitle("Export");
	$exc->removeSheetByIndex(0);
	//ustawienie styli
	$style_unp=new PHPExcel_Style();
	$style_unp->getProtection()->_locked="unprotected";
	$style_unp->getProtection()->_hidden="unprotected";
	$style_pro=new PHPExcel_Style();
	$style_pro->getProtection()->_locked="inherit";
	$style_pro->getProtection()->_hidden="protected";
	$exc->addCellXf($style_unp);
	$exc->addCellXf($style_pro);
	//zwrócenie wyniku
	return $exc;
}

function foot($exc)
{
	$path="_api/exports/ex_.xls";
	$write=PHPExcel_IOFactory::createWriter($exc, 'Excel5');
	/*$path="_api/exports/ex_.xlsx";
	$write=PHPExcel_IOFactory::createWriter($exc, 'Excel2007');*/
	$write->save(ROOT_DIR.$path);
}

function set_protection($sheet)
{
	//ochrona arkusza
	$prot=new PHPExcel_Worksheet_Protection();
	$prot->setPassword("pass",false);
	$prot->setSheet(true);
	$sheet->setProtection($prot);
	//zwrócenie wyniku
	return $sheet;
}

function block_cr($sheet, $c, $r)
{
	$r+=50;
	$c+=66;
	//odblokowanie pól i zablokowanie nagłówków
	for($a=66;$a<$c;$a++)
	{
		for($b=2;$b<$r;$b++)
		{ $sheet->getCell(chr($a).$b)->setXfIndex(1); }
		$sheet->getCell(chr($a)."1")->setXfIndex(2);
	}
	//zablokowanie kolumny ID
	for($b=2;$b<$r;$b++)
	{ $sheet->getCell("A".$b)->setXfIndex(2); }
	//zwrócenie wyniku
	return $sheet;
}

function clients($exc, $id)
{
	global $DB;
	//utworzenie obiektu arkuszu
	$sheet=new PHPExcel_Worksheet($exc, "Kontrahenci");
	$sheet->setTitle("Kontrahenci");
	//dodanie arkusza do dokumentu
	$exc->addSheet($sheet);
	//ochrona
	$sheet=set_protection($sheet);
	//dodanie nagłówków
	$sheet->setCellValue("A1","ID");
	$sheet->setCellValue("B1","Adres e-mail do wysyłki");
	$sheet->setCellValue("C1","Sygnatura");
	$sheet->setCellValue("D1","Nazwa");
	$sheet->setCellValue("E1","Adres");
	$sheet->setCellValue("F1","NIP");
	$sheet->setCellValue("G1","Telefon");
	$sheet->setCellValue("H1","Adres e-mail");
	$sheet->setCellValue("I1","Nazwa banku");
	$sheet->setCellValue("J1","Numer konta");
	$sheet->setCellValue("K1","KRS");
	//dodanie danych
	$a=2;
	require(ROOT_DIR."module/panel/client_add/class.php");
	$client=new clients();
	list($dane,$page,$last)=$client->read_list($id,1,999999,"");
	foreach($dane as $i)
	{
		$sheet->setCellValue("A".$a,$i->id);
		$sheet->setCellValue("B".$a,$i->mail);
		$sheet->setCellValue("C".$a,$i->sign);
		$sheet->setCellValue("D".$a,$i->name);
		$sheet->setCellValue("E".$a,$i->address);
		$sheet->setCellValue("F".$a,$i->nip);
		$sheet->setCellValue("G".$a,$i->phone);
		$sheet->setCellValue("H".$a,$i->email);
		$sheet->setCellValue("I".$a,$i->bank_name);
		$sheet->setCellValue("J".$a,$i->account);
		$sheet->setCellValue("K".$a,$i->krs);
		$a++;
	}
	//blokowanie
	$sheet=block_cr($sheet,10,$a);
	//ustawienie parametrów kolumn
	for($a=66;$a<76;$a++)
	{ $sheet->getColumnDimension(chr($a))->setAutoSize(true); }
	return $exc;
}

function products($exc, $id)
{
	global $DB;
	//utworzenie obiektu arkuszu
	$sheet=new PHPExcel_Worksheet($exc, "Produkty");
	$sheet->setTitle("Produkty");
	//dodanie arkusza do dokumentu
	$exc->addSheet($sheet);
	//ochrona
	$sheet=set_protection($sheet);
	//dodanie nagłówków
	$sheet->setCellValue("A1","ID");
	$sheet->setCellValue("B1","Numer");
	$sheet->setCellValue("C1","Nazwa");
	$sheet->setCellValue("D1","Producent");
	$sheet->setCellValue("E1","PKWiU");
	$sheet->setCellValue("F1","Jednostka");
	$sheet->setCellValue("G1","Stawka VAT (%)");
	$sheet->setCellValue("H1","Netto (zł)");
	$sheet->setCellValue("I1","Brutto (zł)");
	//dodanie danych
	$a=2;
	require(ROOT_DIR."module/panel/product_add/class.php");
	$products=new products();
	$dane=$products->read_export($id);
	foreach($dane as $i)
	{
		$sheet->setCellValue("A".$a,$i->id);
		$sheet->setCellValue("B".$a,$i->number);
		$sheet->setCellValue("C".$a,$i->name);
		$sheet->setCellValue("D".$a,$i->producer);
		$sheet->setCellValue("E".$a,$i->pkwiu);
		$sheet->setCellValue("F".$a,$i->unit);
		$sheet->setCellValue("G".$a,$i->vat);
		$sheet->setCellValue("H".$a,$i->netto);
		$sheet->setCellValue("I".$a,$i->brutto);
		$a++;
	}
	//blokowanie
	$sheet=block_cr($sheet,8,$a);
	//ustawienie parametrów kolumn
	for($a=66;$a<76;$a++)
	{ $sheet->getColumnDimension(chr($a))->setAutoSize(true); }
	return $exc;
}

function categories($exc, $id)
{
	global $DB;
	//utworzenie obiektu arkuszu
	$sheet=new PHPExcel_Worksheet($exc, "Kategorie");
	$sheet->setTitle("Kategorie");
	//dodanie arkusza do dokumentu
	$exc->addSheet($sheet);
	//ochrona
	$sheet=set_protection($sheet);
	//dodanie danych
	$a=2; $lvl=1;
	require_once(ROOT_DIR."module/panel/product_add/class.php");
	$products=new products();
	list($cat,$s)=$products->cat_tree($id);
	list($a,$lvl,$sheet)=categories_lvl($sheet, $cat, $a, $lvl);
	//dodanie nagłówków
	$sheet->setCellValue("A1","ID");
	for($x=1;$x<=$lvl;$x++)
	{ $sheet->setCellValue(chr($x+65)."1","Nazwa (lvl. ".$x.")"); }
	//blokowanie
	$sheet=block_cr($sheet,$lvl,$a);
	//ustawienie parametrów kolumn
	for($a=66;$a<67+$lvl;$a++)
	{ $sheet->getColumnDimension(chr($a))->setAutoSize(true); }
	return $exc;
}

function categories_lvl($sheet, $cat, $a, $lvl)
{
	$l=$lvl;
	foreach($cat as $i)
	{
		$sheet->setCellValue("A".$a,$i->id);
		$sheet->setCellValue(chr($lvl+65).$a,$i->name);
		$a++;
		if(count($i->child))
		{
			list($a, $z, $sheet)=categories_lvl($sheet,$i->child,$a,$lvl+1);
			$l=$l>$z?$l:$z;
		}
	}
	return array($a, $l, $sheet);
}

function invnoices($exc, $id)
{
	global $DB;
	//utworzenie obiektu arkuszu
	$sheet=new PHPExcel_Worksheet($exc, "Faktury");
	$sheet->setTitle("Faktury");
	//dodanie arkusza do dokumentu
	$exc->addSheet($sheet);
	//ochrona
	$sheet=set_protection($sheet);
	//dodanie nagłówków
	$sheet->setCellValue("A1","ID");
	$sheet->setCellValue("B1","Dane faktury");
	//dodanie danych
	require(ROOT_DIR."module/panel/invoice/class.php");
	$inv=new invoice();
	$a=2;
	//odczyt lity faktur
	$list=$inv->read_list_base($id);
	foreach($list as $i)
	{
		list($pdf,$count)=$inv->get_to_pdf($i->id,$id);
		$sheet->setCellValue("A".$a,$i->id);
		//dane podstawowe
		$sheet->setCellValue("B".$a,"Nr faktury:");
		$sheet->setCellValue("C".$a,$pdf['number']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		$sheet->setCellValue("B".$a,"Miejsce wystawienia:");
		$sheet->setCellValue("C".$a,$pdf['place']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		$sheet->setCellValue("B".$a,"Data wystawienia:");
		$sheet->setCellValue("C".$a,$pdf['date_create']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		$sheet->setCellValue("B".$a,"Data sprzedaży:");
		$sheet->setCellValue("C".$a,$pdf['date_sell']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		//dane sprzedawca, nabywca, adres
		$br=$pdf['branch_name']?true:false;
		$a++;
		$sheet->setCellValue("C".$a,"Dane sprzedawcy");
		$sheet->setCellValue("D".$a,"Dane nabywcy");
		if($br) { $sheet->setCellValue("E".$a,"Dane dostawy"); }
		$sheet->getCell("C".$a)->setXfIndex(2);
		$sheet->getCell("D".$a)->setXfIndex(2);
		$sheet->getCell("E".$a)->setXfIndex(2);
		$a++;
		$sheet->setCellValue("B".$a,"Nazwa/Firma:");
		$sheet->setCellValue("C".$a,$pdf['user_name']);
		$sheet->setCellValue("D".$a,$pdf['client_name']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_name']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"Adres:");
		$sheet->setCellValue("C".$a,$pdf['user_address']);
		$sheet->setCellValue("D".$a,$pdf['client_address']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_address']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"NIP:");
		$sheet->setCellValue("C".$a,$pdf['user_nip']);
		$sheet->setCellValue("D".$a,$pdf['client_nip']);
		if($br) { $sheet->setCellValue("E".$a,"---"); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex(2);
		$a++;
		$sheet->setCellValue("B".$a,"Telefon:");
		$sheet->setCellValue("C".$a,$pdf['user_phone']);
		$sheet->setCellValue("D".$a,$pdf['client_phone']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_phone']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"E-mail:");
		$sheet->setCellValue("C".$a,$pdf['user_mail']);
		$sheet->setCellValue("D".$a,$pdf['client_mail']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_mail']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"KRS:");
		$sheet->setCellValue("C".$a,$pdf['user_krs']);
		$sheet->setCellValue("D".$a,$pdf['client_krs']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_krs']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"Nazwa banku:");
		$sheet->setCellValue("C".$a,$pdf['user_bank_name']);
		$sheet->setCellValue("D".$a,$pdf['client_bank_name']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_bank_name']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		$sheet->setCellValue("B".$a,"Konto:");
		$sheet->setCellValue("C".$a,$pdf['user_account']);
		$sheet->setCellValue("D".$a,$pdf['client_account']);
		if($br) { $sheet->setCellValue("E".$a,$pdf['branch_account']); }
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getCell("D".$a)->setXfIndex(1);
		$sheet->getCell("E".$a)->setXfIndex($br?1:2);
		$a++;
		//lista produktów
		$a++;
		$sheet->setCellValue("B".$a,"Nazwa:");
		$sheet->setCellValue("C".$a,"PKWiU:");
		$sheet->setCellValue("D".$a,"Ilość:");
		$sheet->setCellValue("E".$a,"Jednostka:");
		$sheet->setCellValue("F".$a,"Cena netto:");
		$sheet->setCellValue("G".$a,"Rabat (%):");
		$sheet->setCellValue("H".$a,"Cena netto:");
		$sheet->setCellValue("I".$a,"Stawka VAT (%):");
		$sheet->setCellValue("J".$a,"Kwota netto:");
		$sheet->setCellValue("K".$a,"Kwota VAT:");
		$sheet->setCellValue("L".$a,"Kwota brutto:");
		for($b=66;$b<77;$b++) { $sheet->getCell(chr($b).$a)->setXfIndex(2); }
		$a++;
		$ps=$a;
		for($c=1;$c<=$count;$c++)
		{
			$sheet->setCellValue("A".$a,$c.".");
			$sheet->setCellValue("B".$a,$pdf['p_'.$c.'_name']);
			$sheet->setCellValue("C".$a,$pdf['p_'.$c.'_pkwiu']);
			$sheet->setCellValue("D".$a,$pdf['p_'.$c.'_amount']);
			$sheet->setCellValue("E".$a,$pdf['p_'.$c.'_unit']);
			$sheet->setCellValue("F".$a,$pdf['p_'.$c.'_netto']);
			$sheet->setCellValue("G".$a,$pdf['p_'.$c.'_rabat']);
			$sheet->setCellValue("H".$a,"=F".$a."*(100-G".$a.")/100");
			$sheet->setCellValue("I".$a,$pdf['p_'.$c.'_vat']);
			$sheet->setCellValue("J".$a,"=H".$a."*D".$a);
			$sheet->setCellValue("K".$a,"=J".$a."*I".$a."/100");
			$sheet->setCellValue("L".$a,"=J".$a."+K".$a);
			$sheet->getCell("A".$a)->setXfIndex(2);
			for($b=66;$b<77;$b++) { $sheet->getCell(chr($b).$a)->setXfIndex(1); }
			$sheet->getCell("H".$a)->setXfIndex(2);
			$sheet->getCell("J".$a)->setXfIndex(2);
			$sheet->getCell("K".$a)->setXfIndex(2);
			$sheet->getCell("L".$a)->setXfIndex(2);
			
			$sheet->setSelectedCells("H".$a);
			$sheet->getStyle("H".$a)->getNumberFormat()->setFormatCode('#,##0.00');
			$sheet->getStyle("J".$a)->getNumberFormat()->setFormatCode('#,##0.00');
			$sheet->getStyle("K".$a)->getNumberFormat()->setFormatCode('#,##0.00');
			$sheet->getStyle("L".$a)->getNumberFormat()->setFormatCode('#,##0.00');
			$a++;
		}
		$pe=$a-1;
		//sposób i termin zapłaty oraz należności
		$a++;
		$sheet->setCellValue("B".$a,"Sposób zapłaty:");
		$sheet->setCellValue("C".$a,$pdf['cash_type']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		$sheet->setCellValue("B".$a,"Termin zapłaty:");
		$sheet->setCellValue("C".$a,$pdf['cash_date']);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$a++;
		$sheet->setCellValue("B".$a,"Zapłacono:");
		$sheet->setCellValue("C".$a,$pdf['cash_pay']?$pdf['cash_pay']:0);
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(1);
		$sheet->getStyle("C".$a)->getNumberFormat()->setFormatCode('#,##0.00');
		$a++;
		$sheet->setCellValue("B".$a,"Do zapłaty:");
		$sheet->setCellValue("C".$a,"=SUM(L".$ps.":L".$pe.")-C".($a-1));
		$sheet->getCell("B".$a)->setXfIndex(2);
		$sheet->getCell("C".$a)->setXfIndex(2);
		$sheet->getStyle("C".$a)->getNumberFormat()->setFormatCode('#,##0.00');
		$a++;
		
		
		$a+=2;
		//dodanie lini oddzielającej
		//for($b=65;$b<86;$b++) { $sheet->setCellValue(chr($b).$a,"x"); }
		//$a++;
	}

	
	
	//ustawienie parametrów kolumn
	for($a=66;$a<77;$a++)
	{ $sheet->getColumnDimension(chr($a))->setAutoSize(true); }
	
	return $exc;
}





?>