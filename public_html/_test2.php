<?php
/* ustawienia serwera */
set_time_limit(360);
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','256M');
/* inicjalizacja */
session_start();
ob_start();
/* wys�anie nag��wk�w */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie g��wnych sta�ych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
define("USER_ID",9557);
//define("USER_ID",9);
/* do��czenie blibliotek g��wnych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
/* utworzenie g��wnych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
//utworzenie faktury
$_GET['what']=1;
//$_GET['one']=1;
//$_POST['fak']=array(17479,47332,50703,48678);
$_POST['fak'] = array(87684,87882,87883,89443,89680,90247,90403,90436,90840,91352,91354,92002,92853,92856,92859,92865,92878,92881,92971,93182,93713,94044,95470,97078,97079,97418,97420,97424,97426,97427,97593,97594,98279,98280,98608,98612,98620,98693,98695,98707,98709,98710,98713,99012,99013,99471,99630,99632,100865,100866,101284,101285,101286,101287,101510,101872,101873,104686,105521,105523,105524,106054,108377,109057,109091,109338,109343,109345,110318,110320,110647,111445,111446,111448,111449,111880,111885,111892,111895,113032,113033,113035,113341,113432,113750,113878,113880,114040,117010,117015,117017,117768,118047,118070,118146,118150,118201,119051,120432,121031,121032,121358,121360,121606,121622,122693,122695,122965,123982,124038,124039,125492,125927,126782,126783,126785,127098,127099,127100,127145,127147,127987,128006,128011,128015,129469,129470,130020,130157,130158,130561,131438,132821,134636,134642,134740,140004,140704,141047,141052,141083,143994,145140,145168,145170,147741,147744,147746,147747,149587,152032,152034,152036,152057,152542,153931,153932,156501,158020,158021,158022,159832,159834);
//$_POST['fak'] = array(87684,87882,87883);

if(!is_array($_POST['fak']) OR !count($_POST['fak'])) { echo "errSel"; exit(); }

require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/data_extra/class.php");
require(ROOT_DIR."module/panel/szablon-faktury/class.php");

$inv=new invoice();
if($_GET['one'])
{
	require_once(ROOT_DIR.'module/invoice/main_pdf.php');
	$pdf=new mainPDF(USER_ID);
	
	foreach($_POST['fak'] as $id)
	{
		list($tmp,$count)=$inv->get_to_pdf($id,USER_ID);
		$data=new user_color(USER_ID,$tmp['type']);
		switch($tmp['type'])
		{
			case 1: $dirf="invoice"; $class="class_pdfI"; break;
			case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
			case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
			case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
			case 5: $dirf="rachunek"; $class="class_pdfR"; break;
			default: $dirf="invoice"; $class="class_pdfI"; break;
		}
		require_once(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
		$data=new user_color(USER_ID,$tmp['type']);
		foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
		$pdf=cast_class($pdf,$class);
		$pdf->gen(USER_ID,$count,$tmp,$_GET['what']);
	}
	$name="cache/dokumenty_".date("Ymd")."_".substr(SHA1(microtime()),0,4).".pdf";
	$pdf->Output(ROOT_DIR.$name,"F");
}
else
{
	$files=array();
	foreach($_POST['fak'] as $id)
	{
		list($tmp,$count)=$inv->get_to_pdf($id,USER_ID);
		$data=new user_color(USER_ID,$tmp['type']);
		switch($tmp['type'])
		{
			case 1: $dirf="invoice"; $class="class_pdfI"; break;
		    case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
		    case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
		    case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
		    case 5: $dirf="rachunek"; $class="class_pdfR"; break;
		    default: $dirf="invoice"; $class="class_pdfI"; break;
		}
		require_once(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
		
		$data=new user_color(USER_ID,$tmp['type']);
		foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
		$pdf=new $class(USER_ID,$count,$tmp,$_GET['what']);
		$t=explode("private_html/",urldecode($pdf->savePdf()));
		$files[]=$t[1];echo $t[0]." - ".$t[1]."<br/>";
	}
	
	$zip = new ZipArchive();
	$name="cache/dokumenty_".date("Ymd")."_".substr(SHA1(microtime()),0,4).".zip";
	if($zip->open(ROOT_DIR.$name, ZIPARCHIVE::CREATE)!==TRUE) { echo "errZip"; exit(); }
	foreach($files as $i=>$f)
	{
		$t=explode("/",$f);
		$zip->addFile(ROOT_DIR.$f,($i+1).".".array_pop($t));
	}
	$zip->close();
}
echo "ok|".$name;
?>