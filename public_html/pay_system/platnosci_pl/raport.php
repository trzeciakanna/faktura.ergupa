<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."pay_system/platnosci_pl/class.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$pay=new platnosci_pl($_POST['session_id']);
if($pay->pos_id==$_POST['pos_id'])
{
	if($pay->valid_sign($_POST['ts'],$_POST['sig']))
	{
		if($pay->check_status())
		{
			require(ROOT_DIR."includes/users.php");
			$month=$pay->get_month();
			$user=new users();
			$user=$user->read_one($pay->get_user_id());
			if($user->sub_end>date("Y-m-d H:i:s"))
			{ $date=date("Y-m-d H:i:s",strtotime($user->sub_end." +".$month." months")); }
			else
			{ $date=date("Y-m-d H:i:s",strtotime("+".$month." months")); }
			$user->new_lic($user->id, $date, $_POST['session_id']);
		}
		echo "ok";
	}
	else { $pay->error("invalid sign"); }
}
else { $pay->error("invalid pos_id"); }
?>