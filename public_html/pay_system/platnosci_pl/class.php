<?php
/**
 * Obsługa systemu platnosci.pl
 * @author kordan
 *
 */
class platnosci_pl
{
	public $pos_id;
	public $pos_auth_key;
	public $ts;
	public $session_id;
	private $key1;
	private $key2;
	/**
	 * Utworzenie obiektu płatności
	 * @param string $session identyfikator transakcji
	 */
	public function __construct($session=null)
	{
		$this->pos_id="18983";
		$this->pos_auth_key="vSZatAG";
		$this->ts=time();
		if($session) { $this->session_id=$session; }
		else { $this->session_id=SHA1(microtime()); }
		$this->key1="fdf6aa08ab8117819d4f2a04a9e0f40f";
		$this->key2="ac580e13d98ec9e1dbd17866eaaa9dc1";
	}
	/**
	 * Generowanie podpisu formularza
	 * @param string $cash kwota do zapłaty
	 * @param string $desc opis transakcji
	 * @param string $fname imie użytkownika
	 * @param string $lname nazwisko użytkownika
	 * @param string $email emial użytkownika
	 * @return string podpis formularza
	 */
	public function gen_sign($cash="",$desc="",$fname="",$lname="",$email="")
	{ return MD5($this->pos_id.$this->session_id.$cash.$desc.$fname.$lname.$email.$this->ts.$this->key1); }
	/**
	 * Sprawdzenie poprawności podpisu transakcji
	 * @param string $ts znacznik czasowy
	 * @param string $sign otrzymany podpis
	 * @return boolean poprawność podpisu
	 */
	public function valid_sign($ts, $sign)
	{ return MD5($this->pos_id.$this->session_id.$ts.$this->key2)==$sign?true:false; }
	/**
	 * Sprawdzenie statusu transakcji i jego aktualizacja oraz dodanie raportu
	 * @return boolean zakończenie transakcji
	 */
	public function check_status()
	{
		global $DB;
		//require(ROOT_DIR."includes/xml.php");
		$sign=$this->gen_sign();
		/* inicjalizacja socket'ów */
		$ch = curl_init();  		
		curl_setopt($ch, CURLOPT_URL, 'https://www.platnosci.pl/paygw/ISO/Payment/get/xml');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);				
		curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, "pos_id=".$this->pos_id."&session_id=".$this->session_id."&ts=".$this->ts."&sig=".$sign);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $odpowiedz = curl_exec($ch);
	    curl_close($ch);
		/* przetworzenie  odpowiedzi serwera */
		$tmp=explode("<?xml version",$odpowiedz,2);
		$odpowiedz="<?xml version".$tmp[1];
		$xml=XML_unserialize($odpowiedz);
		$DB->Execute("UPDATE `pay_sys_platnosci_pl` SET `status`='".$xml['response']['trans']['status']."' WHERE `session_id`='".$this->session_id."'");
		$tmp=array();
		$tmp['session']=$this->session_id;
		$tmp['status']=$xml['response']['trans']['status'];
		$tmp['date']=date("Y-m-d H:i:s");
		$tmp['type']="platnosci.pl";
		$DB->AutoExecute("pay_sys_raport",$tmp,"INSERT");
		return $xml['response']['trans']['status']==99?true:false;
	}
	/**
	 * 
	 * @param int $user_id identyfikator użytkownika
	 * @param int $month ilość miesięcy
	 * @param int $cash kwota
	 */
	public function add($user_id, $month, $cash)
	{
		global $DB;
		$tmp=array();
		$tmp['user_id']=$user_id;
		$tmp['month']=$month;
		$tmp['cash']=$cash;
		$tmp['date']=date("Y-m-d H:i:s");
		$tmp['session_id']=$this->session_id;
		$DB->AutoExecute("pay_sys_platnosci_pl",$tmp,"INSERT");
	}
	/**
	 * Pobranie ilości miesięcy na które przedłużono abonament
	 * @return int ilość miesięcy
	 */
	public function get_month()
	{
		global $DB;
		$r=$DB->Execute("SELECT `month` FROM `pay_sys_platnosci_pl` WHERE `session_id`='".$this->session_id."' LIMIT 0,1");
		return $r->fields['month'];
	}
	/**
	 * Pobranie identyfikatora użytkownika
	 * @return int identyfikator użytkownika
	 */
	public function get_user_id()
	{
		global $DB;
		$r=$DB->Execute("SELECT `user_id` FROM `pay_sys_platnosci_pl` WHERE `session_id`='".$this->session_id."' LIMIT 0,1");
		return $r->fields['user_id'];
	}
	/**
	 * Dodanie błędu
	 * @param string $error kod błędu
	 */
	public function error($error)
	{
		global $DB;
		$tmp=array();
		$tmp['error']=$error;
		$tmp['date']=date("Y-m-d H:i:s");
		$tmp['session']=$this->session_id;
		$tmp['type']="platnosci.pl";
		$DB->AutoExecute("pay_sys_error",$tmp,"INSERT");
	}
	
}
?>