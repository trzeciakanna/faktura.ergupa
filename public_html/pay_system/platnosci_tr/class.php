<?php

/** 
 * @author Karol
 * 
 */
class payTr
{
    public $ts;
    public $session_id;
    //static private $id = "14736";
    //static private $key = "hf2KSc85FSxMQ04e";
    //static private $id = "14751";
    //static private $key = "DoUfo18l6L7NaRCq"; 
    static private $id = "27631";
    static private $key = "6TVwNXnV3CtedFPI";
                           
    public static function add($user_id, $month, $cash)
    {
        $user = new users($user_id);
        $user = $user->read_one($user_id);
        global $DB;
        //if(!in_array($month, array(1, 3, 6, 12))) { $month = 3; }
        $t = array();
        $t['session_id'] = self::getHash();
        $t['date'] = date("Y-m-d H:i:s");
        $t['month'] = $month;
        $t['cash'] = round($cash, 2);
        $t['user_id'] = $user_id;
        
        $form = array();
        $form['id'] = self::$id;
        $form['crc'] = $t['session_id'];
        $form['opis'] = "Abonament faktura.egrupa.pl - " . $month . " mies.";
        $form['kwota'] = $t['cash'];
        $form['wyn_url'] = "http://faktura.egrupa.pl/pay_system/platnosci_tr/raport.php";
        $form['pow_url'] = "http://faktura.egrupa.pl/panel/data/licence/ok/";
        $form['pow_url_blad'] = "http://faktura.egrupa.pl/panel/data/licence/error/";
        $form['email'] = $user->mail;
        $form['nazwisko'] = $user->login;
        $form['jezyk'] = "pl";
        $form['md5sum'] = self::genSig($form);
                
        $h = '';
        foreach($form as $i => $v)
        { $h .= '<input type="hidden" name="' . $i . '" value="' . $v . '" />'; }  
        $t['cash'] = $cash * 100;
        $DB->AutoExecute("pay_sys_platnosci_pl", $t, "INSERT");
        return $h;
    }
    
    private static function genSig($form)
    {
        $t = $form['id'];
        $t .= $form['kwota'];
        $t .= $form['crc'];
        $t .= self::$key;
        return MD5($t);
    }
    
    public static function getReport($data)
    {
        global $func;
        if(!in_array($func->get_ip(), ["195.149.229.109","148.251.96.163","178.32.201.77","46.248.167.59","46.29.19.106"])) { return "errIP"; }
        if($data['md5sum'] != MD5(self::$id.$data['tr_id'].$data['tr_amount'].$data['tr_crc'].self::$key)) { return "errSig"; }
        
        if($data['tr_status'] == 'TRUE' AND $data['tr_amount'] == $data['tr_paid']) { $stat = 99; }
        else if($data['tr_status'] == 'TRUE' AND $data['tr_amount'] < $data['tr_paid']) { $stat = 98; } //za dużo
        else if($data['tr_status'] == 'TRUE' AND $data['tr_amount'] < $data['tr_paid']) { $stat = 97; } //za mało
        else { $stat = 0; }
        
        global $DB;
        $d = date("Y-m-d H:i:s");
        $t = array();
        $t['status'] = $stat;
        
        if($stat == 99 || $stat == 98)
        {
            $r = $DB->Execute("SELECT `user_id`, `month`, `status` FROM `pay_sys_platnosci_pl` WHERE `session_id` = '".$data['tr_crc']."' LIMIT 1");
            if($r->fields['status'] != 99)
            {
                $u = $DB->Execute("SELECT `sub_end` FROM `users` WHERE `id` = '".$r->fields['user_id']."' LIMIT 1");
                if($u->fields['sub_end'] > $d)
                { $dt = date("Y-m-d H:i:s", strtotime($u->fields['sub_end']." +".$r->fields['month']." months")); }
                else
                { $dt = date("Y-m-d H:i:s", strtotime($d." +".$r->fields['month']." months")); }
                $DB->Execute("UPDATE `users` SET `sub_end` = '".$dt."', `sub_send` = '0', `sub_hash` = '".$data['tr_crc']."' WHERE `id` = '".$r->fields['user_id']."'");
            }
            $t['vat'] = 'yes';
        }
        else
        {
            $tmp = array();
            $tmp['session'] = $data['tr_crc'];
            $tmp['error'] = $data['tr_status'].";".$data['tr_error'].";".$data['tr_paid'];
            $tmp['date'] = $d;
            $tmp['type'] = "transferuj.pl";
            $DB->AutoExecute("pay_sys_error", $tmp, "INSERT");
        }
        $r = $DB->AutoExecute("pay_sys_platnosci_pl", $t, "UPDATE", "`session_id` = '".$data['tr_crc']."'");
        $tmp = array();
        $tmp['session'] = $data['tr_crc'];
        $tmp['status'] = $stat;
        $tmp['date'] = $d;
        $tmp['type'] = "transferuj.pl";
        $DB->AutoExecute("pay_sys_raport", $tmp, "INSERT");
        
        echo $r ? "TRUE" : "errDb";
    }
    
    
    
    private static function getHash()
    {
        global $DB;
        do
        {
            $h = SHA1(microtime());
            $r = $DB->Execute("SELECT 1 AS `r` FROM `pay_sys_platnosci_pl` WHERE `session_id` = '".$h."' LIMIT 1");
        }
        while($r->fields['r']);
        return $h;
    }
}

?>