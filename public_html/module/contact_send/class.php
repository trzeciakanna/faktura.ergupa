<?php
/**
 * Obsługa formularza kontaktowego
 * @author kordan
 *
 */
class contact_send
{
	/**
	 * Odczyt dostępnych kontaktów
	 * @return array kontakty
	 */
	public function type()
	{
		global $DB;
		global $func;
		$dane=array();
		$r=$DB->Execute("SELECT `id`,`name` FROM `contact_send` ORDER BY `orders` ASC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				$tmp['id']=$r->fields['id'];
				$tmp['name']=$func->show_with_html($r->fields['name']);
				$dane[]=$tmp;
				$r->MoveNext();
			}
		}
		return $dane;
	}
	/**
	 * Pobranie danych kontaktu
	 * @param int $id identyfikator kontaktu
	 * @return array dane kontaktu
	 */
	public function get_mail($id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `mail`,`name` FROM `contact_send` WHERE `id`='".$id."' LIMIT 0,1");
		return array($r->fields['mail'],$r->fields['name']);
	}
	/**
	 * Wysłanie wiadomości
	 * @param array $dane dane do wiadomości
	 * @return string stan wykonania
	 */
	public function send($dane)
	{
		require(ROOT_DIR."includes/mailer.php");
		$mailer=new mailer();
		//mail do admina
		list($mail,$name)=$this->get_mail($dane['account']);
		$user=array();
		$user[]=array("mail"=>$mail,"name"=>$name);
		$result=$mailer->send($user,"contact_send_admin",$dane,null);
		if($result)
		{
      if($dane['mail'])
      {
  			$user=array();
  			$user[]=array("mail"=>$dane['mail'],"name"=>$dane['name']);
  			$result=$mailer->send($user,"contact_send_user",$dane,null);
        //print_r($mailer); 
  			if($result) { return "ok"; }
  			else { return "error"; }
      }
      else { return "ok"; }
		}
		else { return "error"; }
	}
	
	
}
?>