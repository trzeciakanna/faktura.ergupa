<?php
$lang=array();
$lang['head']="Formularz kontaktowy";
$lang['name']="Twoje Imię";
$lang['phone']="Telefon";
$lang['mail']="Adres e-mail";
$lang['text']="Treść";
$lang['send']="Wyślij zapytanie";
$lang['info']="Pola są wymagane";
$lang['info_m']="Któreś z podanych pól jest wymagane";
$lang['type']="Wybierz dział";
$lang['']="";
$lang['']="";

$lang['send_ok']="Wiadomość została wysłana";
$lang['send_error']="Wystąpił błąd podczas wysyłania wiadomości, spróbuj ponownie za chwilę (Wiadomość można wysłać tylko raz na 10 min)";
$lang['send_errorc']="Błędny kod";
$lang['empty_name']="Podaj swoje imię i nazwisko";
$lang['empty_pm']="Podaj numer telefonu lub adres e-mail";
$lang['empty_text']="Wpisz treść swojej wiadomości";
$lang['empty_captcha']="Przepisz kod obrazkowy";
$lang['']="";
$lang['']="";
?>