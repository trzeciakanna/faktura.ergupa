<?php
global $smarty;
require(ROOT_DIR."module/contact_send/class.php");
require(ROOT_DIR."module/contact_send/".LANG.".php");
global $lang;
global $func;
$contact=new contact_send();
$dane=$contact->type();
if(count($dane))
{
	$conf['contact_send']=$func->read_config("module/contact_send/config.xml");
	$smarty->assign("lang",$lang);
	$smarty->assign("dane",$dane);
	$smarty->assign("captcha",$conf['contact_send']['captcha']);
	
	$smarty->display("contact_send/".LAYOUT.".html");
}
?>