<?php
$lang=array();
$lang['head']="Contact Form";
$lang['name']="Your First Name";
$lang['phone']="Telephone";
$lang['mail']="E-mail Address";
$lang['text']="Contents";
$lang['send']="Submit";
$lang['info']="Required Fields";
$lang['info_m']="One of the fields is required";
$lang['type']="Select a Section";
$lang['']="";
$lang['']="";

$lang['send_ok']="The message was submitted";
$lang['send_error']="There was an error while submitting the message; try again later. You may submit 1 message every 10 minutes.";
$lang['send_errorc']="Wrong Code";
$lang['empty_name']="Type your full name";
$lang['empty_pm']="Type your telephone or e-mail address";
$lang['empty_text']="Type the message";
$lang['empty_captcha']="Copy the code from the picture";
$lang['']="";
$lang['']="";
?>