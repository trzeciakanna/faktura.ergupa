<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/contact_send/class.php");
require(ROOT_DIR."module/captcha/class.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$func=new functions();
$DB=$conn->connect();
$contact=new contact_send(); 
$captcha=new captcha();
/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
if($captcha->check($_POST['captcha_code'],$_POST['captcha_key']))
{
  if((time()-(int)$_SESSION['send_time']>600))
  {
    $x=$contact->send($_POST);  
    if($x == "ok") { $_SESSION['send_time']=time(); }
    echo $x;
  }
  else { echo "error"; }
}
else { echo "captcha"; }
?>