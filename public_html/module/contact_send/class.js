contact_send_add = {
	/**
	 * Wysłanie wiadomości
	 */
	submit_null : function(next)
	{
		if(!next) { $('komunikat').innerHTML=""; }
		if($('captcha_valid'))
		{
			if($('captcha_valid').value!=1) { setTimeout("contact_send_add.submit(1);",100); }
			else { this.submit_end(); }
		}
		else { this.submit_end(); }
	},
	/**
	 * Kończenie wysłania wiadomości
	 */
	submit : function()
	{
		var msg=""; 
		$('zapytanie_send_error').style.display="none";
		$('zapytanie_send_ok').style.display="none";
		//if($('name').value.length<1) { msg+=$('empty_name').value+"\n"; info($('empty_name').value,'name'); } else { info('','name'); }
		if($('phone').value.length<1 && $('mail').value.length<1) { msg+=$('empty_pm').value+"\n"; info($('empty_pm').value,'phone'); } else { info('','phone'); }
		if($('text').value.length<1) { msg+=$('empty_text').value+"\n"; info($('empty_text').value,'text'); } else { info('','text'); }
		//if($('captcha_valid')) { if($('captcha_valid').value==0) { msg+=$('empty_captcha').value+"\n"; } }
		if(msg) { return; }
		var req = mint.Request();
		req.retryNum=0;
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				//info($('send_ok').value);
				$('zapytanie_send_ok').innerHTML=$('send_ok').value;
				$('zapytanie_send_ok').style.display="block";
				$('contact_send_form').reset();
				//if($('captcha_valid')) { captcha.refresh(); }
			}
			else if(this.responseText=="captcha")
      {
        $('zapytanie_send_error').innerHTML=$('send_errorc').value;
				$('zapytanie_send_error').style.display="block";  
      }
      else
			{
				$('zapytanie_send_error').innerHTML=$('send_error').value;
				$('zapytanie_send_error').style.display="block";
				//info($('send_error').value);
			}
		};
		req.SendForm("contact_send_form","module/contact_send/ajax_add.php");
	}
};