reg_obj = {
	/**
	 * Sprawdzenie unikalności wprowadzonego loginu
	 */
	check_login : function() 
	{
		if(!$("reg_login").value)
		{
			$('reg_login').className="reg_text_error";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('reg_login').parentNode.className="form-group col-sm-12 has-success";
				$('regv_login').value=1;
				info('','reg_login');
			}
			else
			{
				$('reg_login').parentNode.className="form-group col-sm-12 has-error";
				$('regv_login').value=0;
				info($('bad_login').value,'reg_login');
			}
		};
		req.AddParam("login",$("reg_login").value);
		req.Send("module/reg/ajax_check.php");
	},
	/**
	 * Sprawdzenie unikalności wprowadzonego adresu e-mail
	 */
	check_mail : function() 
	{
		if(!$("reg_mail").value)
		{
			$('reg_mail').parentNode.className="form-group col-sm-6 has-error";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('reg_mail').parentNode.className="form-group col-sm-6 has-success";
				$('regv_mail').value=1;
				info('','reg_mail');
			}
			else
			{
				$('reg_mail').parentNode.className="form-group col-sm-6 has-error";
				$('regv_mail').value=0;
				info($('bad_mail').value,'reg_mail');
			}
		};
		req.AddParam("mail",$("reg_mail").value);
		req.Send("module/reg/ajax_check.php");
	},	
	/**
	 * Sprawdzenie poprawności formularza i dodanie użytkownika
	 */
	submit : function() 
	{
		var msg="";
		if($('reg_pass').value!=$('reg_pass_repeat').value || !$('reg_pass').value) { msg+=$('bad_pass').value+"\n"; info($('bad_pass').value,'reg_pass'); } else { info('','reg_pass'); }
		if($('regv_login').value!=1 || !$('reg_login').value) { msg+=$('bad_login').value+"\n"; info($('bad_login').value,'reg_login'); } else { info('','reg_login'); }
		if($('regv_mail').value!=1 || !$('reg_mail').value) { msg+=$('bad_mail').value+"\n"; info($('bad_mail').value,'reg_mail'); } else { info('','reg_mail'); }
		if($('regv_nip').value!=1 || !$('reg_nip').value) { msg+=$('bad_nip').value+"\n"; info($('bad_nip').value,'reg_nip'); } else { info('','reg_nip'); }
		if(!$('reg_accept').checked) { msg+=$('bad_accept').value+"\n"; info($('bad_accept').value,'reg_accept'); } else { info('','reg_accept'); }
    if(msg)
		{
			//alert(msg);
			return false;
		}
		else
		{
			var req = mint.Request();
			req.retryNum=0;
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ $('sub_error').style.display="block"; 
				}
				else if(this.responseText=="errorNip") { $('sub_errorN').style.display="block";  }
				else
				{ alert($('add_error').value); }
				//location.href=location.href;
			};
			req.AddParam("login",$("reg_login").value);
			req.AddParam("mail",$("reg_mail").value);
			req.AddParam("pass",$("reg_pass").value);
			req.AddParam("nip",$("reg_nip").value);
			req.Send("module/reg/ajax_add.php");
		}
		return false;
	}	
};