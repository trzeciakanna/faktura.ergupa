<?php
$lang=array();
$lang['head']="Rejestracja";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['repeat']="powtórz";
$lang['mail']="Adres e-mail";
$lang['send']="Utwórz nowe konto";
$lang['accept']="Akceptuje <a href='https://faktura.egrupa.pl/polityka/' title='Polityka prywatności' target='_blank'>politykę i <a href='https://faktura.egrupa.pl/regulamin/' title='Regulamin Serwisu' target='_blank'>Regulamin</a>"; 
$lang['nip']="NIP";
$lang['bad_login']="Podany login jest już zajęty";
$lang['bad_mail']="Podany adres e-mail jest już zajęty";
$lang['bad_pass']="Podane hasła są różne";
$lang['add_ok']="Rejestracja przebiegła pomyślnie. 
Możesz się zalogować używając danych podanych w trakcie rejestracji.
Na Twój adres email zostały wysłane informacje potwierdzające rejestrację.";
$lang['add_error']="Wystąpił błąd podczas dodawania konta, prosimy spróbować ponownie za kilka minut.";
$lang['']="";

$lang['bad_nip']="Brak NIP'u";
$lang['bad_accept']="Musisz zaakceptować regulamin";
$lang['err_nip']="Ten numer NIP został już wykorzystany. Zabrania się tworzyć dublujących się kont. Jeżeli jesteś właścicielem tego numeru NIP skontaktuj się z administratorem w celu wyjaśnienia.";

?>