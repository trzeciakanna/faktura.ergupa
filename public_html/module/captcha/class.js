captcha = {
	
	refresh : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			var tmp=this.responseText.split("|");
			$('captcha_key').value=tmp[0];
			$('captcha_img').src=tmp[1];
			$('captcha_order').innerHTML=tmp[2];
			$('captcha_code').value="";
			$('captcha_code').focus();
		};
		req.Send("module/captcha/refresh.php");
		return false;
	},
	
	check : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				//info($('captcha_ok').value);
				info('','captcha_code');
				$('captcha_valid').value=1;
				$('captcha_code').className="captcha_code_ok";
			}
			else
			{
				info($('captcha_error').value,'captcha_code');
				$('captcha_valid').value=0;
				$('captcha_code').className="captcha_code_error";
				captcha.refresh();
			}
		};
		req.AddParam("code",$('captcha_key').value);
		req.AddParam("chars",$('captcha_code').value);
		req.Send("module/captcha/check_code.php");
		return false;
	}
	
		
};