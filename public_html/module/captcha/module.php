<?php
global $smarty;
if(!class_exists("captcha"))
{ require(ROOT_DIR."module/captcha/class.php"); }
require(ROOT_DIR."module/captcha/".LANG.".php");
//global $lang;
global $conf;
global $func;
//global $codes;
$captcha=new captcha();
list($c,$f)=$captcha->gen_key();

$smarty->assign("langs",$langs);
$smarty->assign("code",$c);     
$smarty->assign("form",$f);
$smarty->display("captcha/".LAYOUT.".html");
?>