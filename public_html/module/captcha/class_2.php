<?php
class captcha
{
	private $sign;
	
	public function __construct()
	{
		$this->sign=array();
		for($a=48;$a<58;$a++) { $this->sign[]=chr($a); }
		for($a=65;$a<91;$a++) { $this->sign[]=chr($a); }
	}
	
	public function gen_key($n=6)
	{
		$key=array();
		//przesunięcie i ilość znaków
		$key[0]=$this->sign[array_rand($this->sign)];
		$key[1]=$this->sign[array_rand($this->sign)];
		$p=ord($key[0])%6;
		$e=(ord($key[1])+$p)%10;
		//określenie długości
		$l=8+2*$n+$e;
		//wypełnienie znakami
		for($a=2;$a<$l;$a++) { $key[$a]=$this->sign[array_rand($this->sign)]; }
		return implode("",$key);
	}
	
	private function get_char($code)
	{
		$signs=array();
		$key=str_split($code);
		//określenie przesunięcia i ilości znaków
		$p=ord($key[0])%6;
		$e=(ord($key[1])+$p)%10;
		$l=count($key);
		$n=($l-8-$e)/2;
		//odczyt kodu
		$code=array();
		for($a=0;$a<$p;$a++) { $code[$a]=$key[$a+2]; }
		for($a=$p;$a<6;$a++) { $code[$a]=$key[$l+$a-6]; }
		//odczyt znaków
		for($a=0;$a<$n;$a++)
		{
			$c=(ord($code[$a%6])+ord($key[$a+$p+2])+ord($key[$a+$p+2+$n+$e]))%36;
			$signs[$a]=$this->sign[$c];
		}
		return $signs;
	}
	
	public function type($code)
	{
		$key=str_split($code);
		$sum=0;
		for($a=0,$max=count($key);$a<$max;$a++)
		{ $sum+=ord($key[$a]); }
		return $sum%16;
	}
	
	public function gen_img($code, $width=400, $height=200)
	{
		$file=SHA1($code.$width.$height).".gif";
		if(!file_exists(ROOT_DIR."images/captcha/".$file));
		{
			$signs=$this->get_char($code);
			//utworzenie warstwy i naniesienie tła
			$images=imagecreatetruecolor($width, $height);
			$color_t=imagecolorallocate($images, 0xFF, 0xFF, 0xFF);
			imagecolortransparent($images,$color_t);
			imagefilledrectangle($images,0,0,$width,$height,$color_t);
			//baza czcionek
			$fonts=array();
			$fonts[]=ROOT_DIR."files/fonts/zerothre.ttf";
			$fonts[]=ROOT_DIR."files/fonts/oloron.ttf";
			$fonts[]=ROOT_DIR."files/fonts/stalker.ttf";
			//baza kolorów
			$colors=array();
			$colors[]=imagecolorallocate($images, 255, 0, 64); //czerwony
			$colors[]=imagecolorallocate($images, 64, 0, 255);  //niebieski
			$colors[]=imagecolorallocate($images, 0, 0, 0); //czarny
			$colors[]=imagecolorallocate($images, 255, 128, 255); //różowy
			$colors[]=imagecolorallocate($images, 0, 192, 64);  //zielony
			$colors[]=imagecolorallocate($images, 255, 128, 64); //pomarańczowy
			$colors[]=imagecolorallocate($images, 192, 128, 255); //fioletowy
			$colors[]=imagecolorallocate($images, 128, 255, 255); //błękit
			//obliczenie wymiarów
			$space=round($width/(count($signs)+1));
			$y=round($height/2);
			//naniesienie znaków
			for($a=0,$max=count($signs);$a<$max;$a++)
			{ imagettftext($images,$space*0.7,-23+rand(0,46),$a*$space+$space/2,$y+rand(0,round($y/2)),$colors[rand(0,2)],$fonts[rand(0,2)],$signs[$a]); }
			//zaciemnienie
			for($a=0;$a<200;$a++)
			{ imageellipse($images,rand(0,$width),rand(0,$height),rand(2,6),rand(2,6),$colors[rand(3,7)]); }
			//zapisanie grafiki
			//imagepng($images,ROOT_DIR."images/captcha/".$file,9, PNG_ALL_FILTERS);
			imagegif($images,ROOT_DIR."images/captcha/".$file);
		}
		return "images/captcha/".$file;
	}
	
	public function check($code, $chars)
	{
		$chars=str_split($chars);
		$signs=$this->get_char($code);
		$type=$this->type($code);
		$ls=count($signs);
		$lc=count($chars);
		switch($type)
		{
			case 0:
				if($ls==$lc)
				{
					for($a=0;$a<$ls;$a++) { if($chars[$a]!=$signs[$a]) { return false; } }
					return "ok";
				}
				break;
			case 1:
				if($ls==$lc)
				{
					for($a=0;$a<$ls;$a++) { if($chars[$a]!=$signs[$ls-$a-1]) { return false; } }
					return "ok";
				}
				break;
			case 2:
				if($lc==3 AND $chars[0]==$signs[0] AND $chars[1]==$signs[1] AND $chars[2]==$signs[2])
				{ return "ok"; }
				break;
			case 3:
				if($lc==3 AND $chars[0]==$signs[$ls-3] AND $chars[1]==$signs[$ls-2] AND $chars[2]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 4:
				if($lc==2 AND $chars[0]==$signs[0] AND $chars[1]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 5:
				if($lc==4 AND $chars[0]==$signs[0] AND $chars[1]==$signs[1] AND $chars[2]==$signs[$ls-2] AND $chars[3]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 6:
				if($lc==3 AND $chars[0]==$signs[2] AND $chars[1]==$signs[1] AND $chars[2]==$signs[0])
				{ return "ok"; }
				break;
			case 7:
				if($lc==3 AND $chars[2]==$signs[$ls-3] AND $chars[1]==$signs[$ls-2] AND $chars[0]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 8:
				if($lc==ceil($ls/2))
				{
					for($a=0;$a<$lc;$a++) { if($chars[$a]!=$signs[$a*2]) { return false; } }
					return "ok";
				}
				break;
			case 9:
				if($lc==floor($ls/2))
				{
					for($a=1;$a<$lc;$a++) { if($chars[$a]!=$signs[$a*2-1]) { return false; } }
					return "ok";
				}
				break;
			case 10:
				if($lc==4 AND $chars[2]==$signs[0] AND $chars[3]==$signs[1] AND $chars[0]==$signs[$ls-2] AND $chars[1]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 11:
				if($lc==1 AND $chars[0]==$signs[0])
				{ return "ok"; }
				break;
			case 12:
				if($lc==1 AND $chars[0]==$signs[$ls-1])
				{ return "ok"; }
				break;
			case 13:
				if($lc==5 AND implode("",$chars[0])=="admin")
				{ return "ok"; }
				break;
			case 14:
				if($lc==1 AND $chars[0]==" ")
				{ return "ok"; }
				break;
			case 15:
				if($lc==1 AND $chars[0]=="6")
				{ return "ok"; }
				break;
		}
		return "error";
	}
	
	
}
?>