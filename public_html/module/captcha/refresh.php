<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/captcha/class.php");
require(ROOT_DIR."module/captcha/".LANG.".php");
/* utworzenie głównych klas */
$func=new functions();
global $codes;
$captcha=new captcha();
$conf=array();
$conf['news']=$func->read_config("module/captcha/config.xml");
$c=$captcha->gen_key($conf['news']['sign']);
echo $c."|".$captcha->gen_img($c,$conf['news']['width'],$conf['news']['height'])."|".$codes[$captcha->type($c)];
?>