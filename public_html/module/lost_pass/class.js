lost_pass_obj = {
	/**
	 * Wysłanie maila
	 */
	submit : function(next)
	{
		if(!next) { $('komunikat').innerHTML=""; }
		if($('captcha_valid'))
		{
			if($('captcha_valid').value!=1) { /*setTimeout("lost_pass_obj.submit(1);",100);*/ }
			else { this.submit_end(); }
		}
		else { this.submit_end(); }
		return false;
	},
	/**
	 * Wysłanie maila
	 */
	submit_end : function() 
	{
		var msg="";
		if($('captcha_valid')) { if($('captcha_valid').value==0) { msg+=$('lost_pass_empty_captcha').value+"\n"; } }
		if(msg)
		{ info(msg, 'lost_pass_error'); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{
					info($('lost_pass_ok').value, 'lost_pass_ok');
					$('lost_pass_form').reset();
				}
				else if(this.responseText=="error")
				{ info($('lost_pass_bad').value, 'lost_pass_error'); }
				else
				{ info($('lost_pass_send').value, 'lost_pass_error'); }
			};
			req.AddParam("login",$("loginLost").value);
			req.AddParam("mail",$("mailLost").value);
			req.Send("module/lost_pass/ajax_check.php");
		}
		return false;
	}
};