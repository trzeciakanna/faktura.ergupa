<?php
$lang=array();
$lang['head']="Remind Me My Password";
$lang['login']="Login";
$lang['mail']="E-mail Address";

$lang['send']="Remind";
$lang['log_bad']="Incorrect Data.";
$lang['log_ok']="A new password has just been sent to your e-mail address";
$lang['log_send']="An error occurred while sending the password; please try agan later";
$lang['empty_captcha']="Copy the code from the picture";

?>