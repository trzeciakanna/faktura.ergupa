<?php
$lang=array();
$lang['head']="Przypomnienie hasła";
$lang['login']="Login";
$lang['mail']="Adres e-mail";

$lang['send']="Przypomnij";
$lang['log_bad']="Podane dane są błędne.";
$lang['log_ok']="Na Twój adres e-mail zostało wysłane nowe hasło";
$lang['log_send']="Wystąpił błąd podczas wysyłania hasła, spróbuj ponownie za chwilę";
$lang['empty_captcha']="Przepisz kod obrazkowy";

?>