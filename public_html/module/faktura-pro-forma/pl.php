<?php
$lang=array();
//nagłowek faktury
$lang['head']="Proforma";
$lang['number']="nr:";
$lang['original']="ORYGINAŁ";
$lang['copy']="KOPIA";
$lang['double']="DUPLIKAT";
//miejsce i data wystawienia/sprzedaży
$lang['place']="Miejsce wystawienia:";
$lang['date_create']="Data wystawienia:";
$lang['date_sell']="Data sprzedaży/wydania:";
$lang['date_deadline']="Data zakończenia usługi:";
//dane użytkownika
$lang['user_name_main']="Sprzedawca:";
$lang['user_name']="Sprzedawca:";
$lang['user_address']="Adres:";
$lang['user_nip']="NIP:";
$lang['user_phone']="Telefon:";
$lang['user_mail']="E-mail:";
$lang['user_krs']="KRS:";
$lang['user_bank_name']="Nazwa banku:";
$lang['user_account']="KONTO:";
$lang['user_www']="WWW:";
//dane kontrahenta
$lang['client_name_main']="Nabywca:";
$lang['client_title']="Kontrahent:";
$lang['client_name']="Nabywca:";
$lang['client_address']="Adres:";
$lang['client_nip']="NIP:";
$lang['client_phone']="Telefon:";
$lang['client_mail']="E-mail:";
$lang['client_krs']="KRS:";
$lang['client_bank_name']="Nazwa banku:";
$lang['client_account']="KONTO:";
//dane dostawy
$lang['branch']="Adres dostawy";
$lang['branch_name']="Oddział:";
$lang['branch_address']="Adres:";
$lang['branch_phone']="Telefon:";
$lang['branch_mail']="E-mail:";
$lang['branch_krs']="KRS:";
$lang['branch_bank_name']="Nazwa banku:";
$lang['branch_account']="KONTO:";
//nagłówki produktów
$lang['lp']="Lp.";
$lang['name']="Nazwa towaru/usługi";
$lang['pkwiu']="PKWiU";
$lang['amount']="Ilość";
$lang['unit']="Jm";
$lang['netto']="Cena netto";
$lang['rabat']="Rabat%";
$lang['prize_netto']="Cena netto";
$lang['vat']="VAT";
$lang['sum_netto']="Kwota netto";
$lang['sum_vat']="Kwota VAT";
$lang['sum_brutto']="Kwota brutto";
//nagłówki podsumowania
$lang['all_rate']="Stawka VAT";
$lang['all_netto']="Netto";
$lang['all_vat']="VAT";
$lang['all_brutto']="Brutto";
$lang['all_sum']="Razem:";
//nagłówki sumy
$lang['cash_word']="Kwota słownie:";
$lang['cash_pay']="Zapłacono:";
$lang['cash_sum']="Do zapłaty:";
$lang['cash_type']="Sposób zapłaty:";
$lang['cash_date']="Termin zapłaty:";
//opcje zapłaty
$lang['cash_option']=array();
$lang['cash_option'][]="przelew";
$lang['cash_option'][]="gotówka";
$lang['cash_option'][]="zapłacono";
$lang['cash_option'][]="za pobraniem";
$lang['cash_option'][]="barter";
$lang['cash_option'][]="karta płatnicza";
$lang['cash_option'][]="mieszany";
$lang['cash_option'][]="zgodnie z umową";
$lang['cash_option'][]="płatność on-line";
$lang['cash_option'][]="akredytywa";
$lang['cash_option'][]="rozliczono z przedpłatą";
$lang['cash_option'][]="terminal płatniczy";
$lang['cash_option'][]="kompensata";

//terminy zapłaty
$lang['cash_deadline']=array();
$lang['cash_deadline'][]="3 dni";
$lang['cash_deadline'][]="7 dni";
$lang['cash_deadline'][]="14 dni";
$lang['cash_deadline'][]="21 dni";
$lang['cash_deadline'][]="28 dni";
$lang['cash_deadline'][]="30 dni";
$lang['cash_deadline'][]="35 dni";
$lang['cash_deadline'][]="40 dni";
$lang['cash_deadline'][]="45 dni";
$lang['cash_deadline'][]="60 dni";
$lang['cash_deadline'][]="90 dni";
$lang['cash_deadline'][]="zapłacono";
$lang['cash_deadline'][]="zgodnie z umową";
$lang['cash_deadline'][]="przy odbiorze";
//podpisy
$lang['client_sign']="Osoba upoważniona do odbioru";
$lang['user_sign']="Osoba upoważniona do wystawienia";
//dodatkowe
$lang['coment']="Informacje dodatkowe/komentarz";
$lang['strike']="zaznacz";
$lang['show']="rozwiń";
$lang['hide']="zwiń";
$lang['n2b']="Przelicz netto na brutto";
$lang['b2n']="Przelicz brutto na netto";
//błędy i komunikaty
$lang['bad_nip']="Błędny numer NIP";
$lang['bad_krs']="Błędny numer KRS";
$lang['bad_regon']="Błędny numer REGON";
$lang['add']="Dodaj pozycję";
$lang['del']="Usuń pozycję";
$lang['delete']="Czy na pewno chcesz wyczyścić fakturę";
$lang['save_ok']="Faktura została zapisana poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania faktury, spróbuj ponownie za chwilę";

$lang['addc_ok']="Kontrahent został dodany poprawnie";
$lang['addc_error']="Wystąpił błąd podczas dodawania kontrahenta, spróbuj ponownie za chwilę";
$lang['addp_ok']="Produkt został dodany poprawnie";
$lang['addp_error']="Wystąpił błąd podczas dodawania produktu, spróbuj ponownie za chwilę";
$lang['same_nr']="Faktura o takim numerze już istniej, czy chcesz kontynuować?";
//zapis słowny kwot
$lang['number_coin']="zł,gr";
$lang['number_tou']="tysiąc,tysiące,tysięcy";
$lang['number_mln']="milion,miliony,milionów";
$lang['number_mld']="miliard,miliardy,miliardów";
$lang['number_j']="jeden,dwa,trzy,cztery,pięć,sześć,siedem,osiem,dziewięć,zero";
$lang['number_n']="dziesięć,jedenaście,dwanaście,trzynaście,czternaście,piętnaście,szesnaście,siedemnaście,osiemnaście,dziewiętnaście";
$lang['number_d']="dwadzieścia,trzydzieści,czterdzieści,pięćdziesiąt,sześćdziesiąt,siedemdziesiąt,osiemdziesiąt,dziewięćdziesiąt";
$lang['number_s']="sto,dwieście,trzysta,czterysta,pięćset,sześćset,siedemset,osiemset,dziewięćset";
//menu
$lang['menu_loadp']="Załaduj produkt/usługę z bazy";
$lang['menu_loadc']="Załaduj kontrahenta";
$lang['menu_save']="Zachowaj!";
$lang['menu_pdf']="Wydrukuj PDF";
$lang['menu_clean']="Nowa<br>faktura";
$lang['menu_duplicate']="Wystaw<br>podobną";
$lang['menu_mail']="Wyślij<br>e-mail";
//panel wyboru kontrahenta
$lang['clients_head']="Lista kontrahentów";
$lang['clients_name']="Imię i nazwisko/nazwa firmy";
$lang['clients_address']="Adres";
$lang['clients_nip']="NIP";
$lang['clients_search']="Szukaj";
$lang['clients_all']="wszystkie";
$lang['clients_close']="Zamknij";
//panel wyboru produktu
$lang['prod_head']="Lista produktów";
$lang['prod_name']="Nazwa";
$lang['prod_producer']="Producent";
$lang['prod_cat']="Kategoria";
$lang['prod_number']="Numer";
$lang['prod_pkwiu']="PKWiU";
$lang['prod_prize']="Cena";
$lang['prod_search']="Szukaj";
$lang['prod_all']="wszystkie";
$lang['prod_close']="Zamknij";
//powroty
$lang['list_invoice']="Lista faktur";
$lang['list_client']="Kontrahenci";
$lang['add_client']="Zapisz kontrahenta";
$lang['add_product']="Zapisz produkt";

$lang['firma_name']="Nazwa/Firma:";
$lang['currency']="Wybierz walutę";

//bledy

$lang['error1']="Aby skorzystać z tej opcji zaloguj się.";

//pomoc
$lang['pomoc1']="Czy wiesz, że możesz zmieniać nazwy wszystkich nagłówków, łącznie z tytułem:  np 'Faktura Vat' wystarczy kliknąć kursorem myszy i możesz zastąpić jej nazwę na dowolną np. Faktura Pro Forma.";
$lang['pomoc2']="Wprowadź poprawnie wszystkie dane w odpowiednie pola,
Jeżeli nazwa jest zbyt długa, użyj klawisza 'enter' tworzy on kolejny wiersz na wprowadzenie dodatkowej treści.
Jeśli jesteś zalogowany w każdej chwili możesz zapisać kontrahenta lub produkt do bazy.";

$lang['pomoc3']="W tym miejscu możesz podać lokalizacje dostawy sprzedanego towaru.
Możesz 'Adres dostawy' zedytować na 'adres usługi' jeżeli jest inny niż dane nabywcy.";
$lang['pomoc4']="W tym miejscu obliczysz wartość netto z brutto lub brutto z netto.
Jeżeli chcesz wyliczyć kwotę brutto wprowadź dane do pierwszego wiersza.
Jeżeli chcesz wyliczyć kotę netto wprowadź dane do drugiego wiersza.";
$lang['pomoc5']="Zaznacz 'faktura okresowa' w przypadku, gdy chcesz, aby co określony
czas generowała się dla danego klienta. Więcej o 'faktura okresowa' w dziale Funkcje Faktury";
$lang['pomoc6']="Towar/usługę możesz zapisać do listy produktów przypisując mu cenę oraz podatek VAT.
Możesz wybrać jednostkę sprzedawanej usługi/towaru, możesz przydzielić procentowego rabatu.
Sprzedawanych pozycji możesz dodać dowolną ilość jak również możesz usunąć wybraną/e";
$lang['pomoc7']="Suma wszystkich kwot: netto, brutto, vat.
Wybierz sposób i termin zapłaty. Termin zapłaty możesz wyznaczyć za pomocą kalendarza, uprzednio zaznaczając kropką kalendarz.
Jeżeli faktura jest częściowo płatna gotówką i częściowo przelewem uwzględnij to w polu 'zapłacono'";

$lang['menu1']='Faktura Vat';
$lang['menu2']='Pro Forma';
$lang['menu3']='Faktura Zaliczkowa';
$lang['menu4']='Faktura Korygująca';
$lang['menu5']='Rachunek';
$lang['menu6']='Wystaw fakturę';
$lang['navi1']='Jesteś w ';

$lang['calc_nagl1']='Kalkulator:';
$lang['calc_nagl2']='Wartość';
$lang['calc_nagl3']='VAT';
$lang['calc_nagl4']='Wynik';
$lang['calc_nagl5']='Przelicz';
$lang['calc_nagl6']='netto';
$lang['calc_nagl7']=' na brutto:';
$lang['calc_nagl8']='brutto';
$lang['calc_nagl9']='na netto:';
$lang['calc_nagl0']='* tego elementu nie będzie na fakturze';
$lang['korekta1']='Dodaj nową pozycję do faktury';
$lang['korekta2']='Korekta';
$lang['korekta3']='Dodaj nową pozycję do korekty';

$lang['save_menu1'] = 'Oryginał i Kopia';
$lang['save_menu2'] = 'Oryginał';
$lang['save_menu3'] = 'Kopia';
$lang['save_menu4'] = 'Duplikat';
$lang['save_menu5'] = 'Drukuj';

?>