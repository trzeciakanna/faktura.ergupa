<?php
require_once(ROOT_DIR.'module/invoice/main_pdf.php');

class class_pdfP extends mainPDF
{
	private $sum;

	public function __construct($id,$count,$data,$what=1)
	{
		parent::__construct($id);
		$this->gen($id,$count,$data,$what);
		$this->id=$id;
		$this->client=$data['client_id'];
		$this->name=$data['number'];
	}
	/**
	 * Generator
	 */
	public function gen($id,$count,$data,$what=1)
	{
		$print=array();
		if ($what==1) { $print=array(1,2); }
		elseif ($what==2) { $print=array(1); }
		elseif ($what==3) { $print=array(2); }
		elseif ($what==4) { $print=array(3); }
		foreach ($print as $i=>$v)
		{
			$this->startPageGroup();
			$this->AddPage();
			$data['original_v']=$v;
			// dodanie nagłówka
			$this->head($id,$data);
			// dodanie miejsca i daty wystawienia/sprzedaży
			$this->city_date($data);
			// dodanie danych użytkownika i kontrahenta
			$this->user_client($data);
			// adres dostawy
			$this->branch($data);
			// dodanie produktów
			$this->products($count,$data);
			// dodanie podsumowania
			$this->sum($data);
			// dane zapłaty
			$this->cash($data);
			// podpisy
			$this->sign($id,$data);
			// komentarze i stopka
			$this->foot($id,$data);
		}
	}
	/**
	 * Zapisanie dokumentu
	 */
	function savePdf()
	{
		$dir=$this->check_dir($this->id, $this->client);
		$name=$this->check_file($dir, $this->name);
		$this->Output($dir.$name,"F");
		return urlencode($dir.$name);
	}

	/**
	 * Komentarze i stopka
	 *
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function foot($id,$data)
	{
		$w=$this->width;
		$this->Ln(40);
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(90,90,90)));
		$this->SetFont('consola','',8);
		// komentarz
		if ($data['coment']!= '')
		{
			$this->MultiCell($w,16,$data['coment'],"T","L",0,1);
		}
		// dane dodatkowe

		if ($id)
		{
			if ($this->extra->info!='')
			{
				$this->MultiCell($w,16,$this->extra->info,"T","L",0,1);
			}
		}
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Podpisy
	 *
	 * @param int $id
	 *        	idnetyfikator uzytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sign($id,$data)
	{
		$w=$this->width;
		$m=$this->margin;
		$this->Ln(25);
		// wstawienie podpisu
		if ($id)
		{
			if ($this->extra->sign and file_exists(ROOT_DIR.$this->extra->sign))
			{
				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->sign);
				if ($hi>55)
				{
					$wi=round($wi*55/$hi);
					$hi=55;
				}
				$wi=$wi<110?$wi:110;
				$this->Image(ROOT_DIR.$this->extra->sign,$w*0.725+$m-$wi/2,$this->GetY(),$wi,0,'','','RTL',true);
				$this->Ln($hi);
			}
			else
			{
				$this->Ln(25);
			}
		}

		$this->SetFont('consola','',8);
		$y=$this->GetY();
		$this->Line($w*0.1+$m,$y,$w*0.45+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$this->Line($w*0.55+$m,$y,$w*0.90+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$this->SetDrawColor(255); // kolor rysowania (lini)
		$table="<table border=\"0\">";
		$table.="<tr>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="<td width=\"35%\" align=\"center\" colspan=\"3\">".$data['lang_client_sign']."</td>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="<td width=\"35%\" align=\"center\" colspan=\"3\">".$data['lang_user_sign']."</td>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"R");
	}
	/**
	 * Dane zapłaty
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function cash($data)
	{
		$this->Ln(15);
		$this->SetFont('consola','',10);
		$sum=0;
		foreach ($this->sum as $i)
		{
			$sum+=$i[2];
		}
		$table="<table border=\"0\" width=\"".($this->width*1)."\">";
		// zapłacono
		if (number_format($data['cash_pay'],2)>0)
		{
			$table.="<tr>";
			$table.="<td width=\"19%\" align=\"left\" ><b> ".$data['lang_cash_pay']."</b></td>";
			$table.="<td width=\"1%\" ></td>";
			$table.="<td width=\"80%\" align=\"left\" colspan=\"4\" ><b>".number_format($data['cash_pay'],2,",",".")." ".$data['current']."</b></td>";
			$table.="</tr>";
			$sum-=$data['cash_pay'];
		}
		// do zapłaty
		$this->SetFont('consola','',12);
		$table.="<tr>";
		$table.="<td width=\"1%\" style=\"background-color:#".$data['kolor_tla_nagl'].";color:#".$data['kolor_tekstu_nagl']."\"></td>";
		$table.="<td width=\"26%\" align=\"left\" style=\"background-color:#".$data['kolor_tla_nagl'].";color:#".$data['kolor_tekstu_nagl']."\"><u><b>".$data['lang_cash_sum']."</b></u></td>";
		$table.="<td width=\"1%\" style=\"background-color:#".$data['kolor_tla_nagl'].";color:#".$data['kolor_tekstu_nagl']."\"></td>";
		$table.="<td width=\"72%\" align=\"left\" colspan=\"4\" style=\"background-color:#".$data['kolor_tla_nagl'].";color:#".$data['kolor_tekstu_nagl']."\"><u><b>".number_format($sum,2,","," ")." ".$data['current']."</b></u></td>";
		$table.="</tr>";
		// dodanie tabeli
		$table.="</table>";
		// $this->Cell($this->width*0.3,0,"",0,0);
		$this->writeHTML($table,1,0,0,0,"R");
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu']));
		// słownie
		// $this->Ln(1);
		$this->SetFont('consola','',8);
		$this->SetY($this->GetY()-10);
		$table="<table border=\"0\">";
		$table.="<tr>";
		$table.="<td width=\"15%\" align=\"left\" >".$data['lang_cash_word']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"84%\" align=\"left\" >".$data['cash_word']."</td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"L");
		$this->Ln(2);
		// druga tabela
		$this->SetFont('consola','',8);
		$table="<table border=\"0\" >";
		// sposób i termin zapłaty
		$table.="<tr>";
		$table.="<td width=\"15%\" align=\"left\">".$data['lang_cash_type']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"30%\" align=\"left\">".$data['cash_type']."</td>";
		$table.="</tr>";
		$table.="<tr>";

		$table.="<td width=\"15%\" align=\"left\">".$data['lang_cash_date']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"30%\" align=\"left\">".($data['cash_date']=="list"?$data['cash_date_list']:date("Y-m-d",strtotime($data['cash_date_date'])))."</td>";
		$table.="</tr>";

		// dodanie tabeli
		$table.="</table>";
		// $this->Cell($this->width*0.3,0,"",0,0);
		$this->writeHTML($table,1,0,0,0,"L");
	}
	/**
	 * Wyświetlenie podliczenia
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sum($data)
	{
		$this->SetFillColor(255); // kolor wypelnienia
		$this->SetDrawColor(255); // kolor rysowania (lini)
		$this->Cell($this->procent(40),18,'',1,0,'C',1);

		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl'])); // kolor
		                                                                                               // wypelnienia
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl'])); // kolor
		                                                                                                  // tekstu
		$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki'])); // kolor
		                                                                                            // rysowania
		                                                                                            // (lini)
		$this->SetLineWidth(1);
        $this->SetFont('','B',7);
		$header=array($data['lang_all_rate'],$data['lang_all_netto'],$data['lang_all_vat'],$data['lang_all_brutto']);
		$w=array(12,16,16,16);
        $num_headers=count($header);
        for ($i=0;$i<$num_headers;++$i)
		{
            $this->Cell($this->procent($w[$i]),18,$header[$i],1,0,'C',1);
        }
		$this->Ln(18);
		// zmienne
		$netto=0;
		$vat=0;
		$brutto=0;
		// kwoty
		foreach ($this->sum as $k=>$i)
		{
			if ($i[2])
			{
				$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu']));
				$this->SetFillColor(255); // kolor wypelnienia
				$this->SetDrawColor(255); // kolor rysowania (lini)
				$this->Cell($this->procent(40),18,'',1,0,'C',1);
				$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki'])); // kolor
				                                                                                            // rysowania
				                                                                                            // (lini)
				$this->Cell($this->procent(12),18,$k.(is_numeric($k)?"%":""),1,0,'R',1);
				$this->Cell($this->procent(16),18,number_format($i[0],2,","," ")." ".$data['current'],1,0,'R',1);
				$this->Cell($this->procent(16),18,number_format($i[1],2,","," ")." ".$data['current'],1,0,'R',1);
				$this->Cell($this->procent(16),18,number_format($i[2],2,","," ")." ".$data['current'],1,0,'R',1);
				$this->Ln(18);
				$netto+=$i[0];
				$vat+=$i[1];
				$brutto+=$i[2];
			}
		}

		// podsumowanie
		$this->SetFillColor(255); // kolor wypelnienia
		$this->SetDrawColor(255); // kolor rysowania (lini)
		$this->Cell($this->procent(40),18,'',1,0,'C',1);
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl']));
		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl'])); // kolor
		                                                                                               // wypelnienia
		$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki'])); // kolor
		                                                                                            // rysowania
		                                                                                            // (lini)
		$this->Cell($this->procent(12),18,$data['lang_all_sum'],1,0,'R',1);
		$this->Cell($this->procent(16),18,number_format($netto,2,","," ")." ".$data['current'],1,0,'R',1);
		$this->Cell($this->procent(16),18,number_format($vat,2,","," ")." ".$data['current'],1,0,'R',1);
		$this->Cell($this->procent(16),18,number_format($brutto,2,","," ")." ".$data['current'],1,0,'R',1);
		$this->Ln(18);
	}
	/**
	 * Dodanie produktów
	 *
	 * @param int $count
	 *        	identyfikator ostatniego produktu
	 * @param array $data
	 *        	dane do wstawienia
	 */
	function procent($procent)
	{
		$result=($this->width/100)*$procent;
		return $result;
	}
	private function products($count,$data)
	{
		// sprawdzamy czy jest jakis rabat.
		for ($a=1;$a<=$count;$a++)
		{
			if ($data['p_'.$a.'_name'])
			{
				if ($data['p_'.$a.'_rabat']!=0)
				{
					$jest_rabat=1;
				}
			}
		}

		$this->SetLineWidth(1);
        $this->SetFont('','B',7);

		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
		// ustawienie parametrów
		$this->Ln(7);
		$this->SetFont('consola','',7);
		// utworzenie zmiennych
		$vat=array(0,3,5,6,7,8,18,19,22,23,25);
		$sum=array();

		foreach ($vat as $i)
		{
			$sum[$i]=array(0,0,0);
		}
		// tabela
		$table='<table  style="padding:3px 2px;border:1px solid #'.$data['kolor_ramki'].'">';
		// nagłówki powyzej są lepsze
		$table.='<tr>';

		$table.='<td width="3%" align="center" valign="middle" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'" ><b>'.$data['lang_lp'].'</b></td>';
		if ($jest_rabat==1)
		{
			$table.='<td width="23%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_name'].'</b></td>';
			$table.='<td width="8%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_pkwiu'].'</b></td>';
			$table.='<td width="5%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_amount'].'</b></td>';
			$table.='<td width="5%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_unit'].'</b></td>';
			$table.='<td width="8%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_netto'].'</b></td>';
			$table.='<td width="6%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_rabat'].'</b></td>';
			$table.='<td width="8%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_prize_netto'].'</b></td>';
		}
		else
		{
			$table.='<td width="27%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_name'].'</b></td>';
			$table.='<td width="10%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_pkwiu'].'</b></td>';
			$table.='<td width="9%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_amount'].'</b></td>';
			$table.='<td width="7%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_unit'].'</b></td>';
			$table.='<td width="10%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_netto'].'</b></td>';
		}

		$table.='<td width="4%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_vat'].'</b></td>';
		$table.='<td width="10%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_sum_netto'].'</b></td>';
		$table.='<td width="10%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_sum_vat'].'</b></td>';
		$table.='<td width="10%" align="center" style="color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';border:1px solid #'.$data['kolor_ramki'].'"><b>'.$data['lang_sum_brutto'].'</b></td>';
		$table.='</tr>';
		// produkty
		for ($a=1;$a<=$count;$a++)
		{
			if ($data['p_'.$a.'_name'])
			{
				$cnp=str_replace(",",".",$data['p_'.$a.'_netto']);
				$cnp=$cnp==round($cnp,2)?number_format($cnp,2,".",""):($cnp==round($cnp,3)?number_format($cnp,3,".",""):$cnp);
				$amo=str_replace(",",".",$data['p_'.$a.'_amount']);

				$t=explode(".",$amo);
				$t=strlen(rtrim($t[1],"0"));
				if ($t>4)
				{
					$t=4;
				}
				$amo=number_format($amo,$t,".","");
				// $amo=$amo==round($amo,2)?number_format($amo,2,".",""):($amo==round($amo,3)?number_format($amo,3,".",""):$amo);
				$cnr=$data['p_'.$a.'_pnetto'];
				$cnr=$cnr==round($cnr,2)?number_format($cnr,2,".",""):($cnr==round($cnr,3)?number_format($cnr,3,".",""):$cnr);
				$table.='<tr>';

				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_lp'].'</td>';
				$table.='<td height="11px" align="left" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.nl2br($data['p_'.$a.'_name']).'</td>';
				$table.='<td height="11px" align="left" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_pkwiu'].'</td>';
				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$amo.'</td>';
				$table.='<td height="11px" align="center" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_unit'].'</td>';
				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$cnp." </td>";
				if ($jest_rabat==1)
				{
					$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_rabat'].'% </td>';
					$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$cnr.'</td>';
				}
				$table.='<td height="11px" align="center" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_vat'].(is_numeric($data['p_'.$a.'_vat'])?"%":"").'</td>';
				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_snetto'].'</td>';
				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_svat'].'</td>';
				$table.='<td height="11px" align="right" style="border:1px solid #'.$data['kolor_ramki'].';color:#'.$data['kolor_tekstu'].';">'.$data['p_'.$a.'_sbrutto'].'</td>';
				$table.='</tr>';
				// podliczenie
				$v=$data['p_'.$a.'_vat'];
				$sum[$v][0]+=$data['p_'.$a.'_snetto'];
				$sum[$v][1]+=$data['p_'.$a.'_svat'];
				$sum[$v][2]+=$data['p_'.$a.'_sbrutto'];
			}
		}
		// dodanie tabeli
		$table.="</table>";
		$this->SetY($this->GetY()-8);
		$this->writeHTML($table,1,0,0,0,"C");

		$this->sum=$sum;
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie danych użytkownika i kontrahenta
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function user_client($data)
	{
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(255)));
		// odczyt parametrów
		$w=$this->width;
		$m=$this->margin;
		$this->SetFont('consola','B',14);
		// linia górna
		$this->Cell($w/2,1,$data['lang_user_name_main'],"B",0,"L");
		$this->Cell($w/2,1,$data['lang_client_name_main'],"B",1,"L");
		// dane
		$this->SetFont('consola','',9);
		$table='<table border="0">';
		$tab=array("name","address","nip","phone","mail","bank_name","account");
		foreach ($tab as $k=>$i)
		{
			if ($data['user_'.$i] or $data['client_'.$i])
			{
				$table.='<tr>';
				if ($data['user_'.$i])
				{
					if ($data['pokaz_naglowki'])
					{
						$table.='<td width="12%" align="right">'.($k==100?"":"").$data['lang_user_'.$i].($k==100?"":"").'</td>';
						$table.='<td width="1%" align="left"></td>';
						$table.='<td width="37%" align="left">'.nl2br($data['user_'.$i]).'</td>';
					}
					else
					{
						$table.='<td width="1%" align="left"></td>';
						$table.='<td width="49%" align="left">'.nl2br($data['user_'.$i]).'</td>';
					}
				}
				else
				{
					$table.='<td width="50%" colspan="3"></td>';
				}
				if ($data['client_'.$i])
				{
					if ($data['pokaz_naglowki'])
					{
						$table.='<td width="12%" align="right">'.($k==100?"<b>":"").$data['lang_client_'.$i].($k==100?"</b>":"").'</td>';
						$table.='<td width="1%" align="left"></td>';
						$table.='<td width="37%" align="left">'.nl2br($data['client_'.$i]).'</td>';
					}
					else
					{
						$table.='<td width="1%" align="left"></td>';
						$table.='<td width="49%" align="left">'.nl2br($data['client_'.$i]).'</td>';
					}
				}
				else
				{
					$table.='<td width="50%" colspan="3"></td>';
				}
				$table.='</tr>';
			}
		}
		$table.='</table>';

		$y1=$this->GetY();
		$this->writeHTML($table,1,0,0,0,"C");
		$y2=$this->GetY();
		// linie boczne i środkowa
		$this->line($m,$y1,$m,$y2);
		$this->line($m+$w/2,$y1,$m+$w/2,$y2);
		$this->line($m+$w,$y1,$m+$w,$y2);
		// linia dolna
		$this->Cell($w,1,"","T",1,"L");
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie adresu dostawy
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function branch($data)
	{
		if (!$data['branch_v'])
		{return false; }
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(255)));
		// odczyt parametrów
		$w=$this->width;
		$m=$this->margin;
		$this->SetFont('consola','B',9);
		// linia górna
		$this->Cell($w/2,1,"","",0,"L");
		$this->Cell($w/2,1,$data['lang_branch'],"B",1,"L");
		$this->SetFont('consola','',9);
		// dane
		$table='<table border="0px">';
		// $table.="<tr><td width=\"50%\" align=\"left\"></td><td colspan=\"3\"
		// align=\"left\"><b>".$data['lang_branch']."</b></td></tr>";
		$tab=array("name","address","phone","mail","krs","bank_name","account");
		foreach ($tab as $k=>$i)
		{
			if ($data['branch_'.$i])
			{
				$table.="<tr>";
				$table.="<td width=\"50%\" align=\"left\"></td>";
				if ($data['pokaz_naglowki'])
				{
					$table.="<td width=\"12%\" align=\"right\">".$data['lang_branch_'.$i]."</td>";
					$table.="<td width=\"1%\" align=\"left\"></td>";
					$table.="<td width=\"37%\" align=\"left\">".nl2br($data['branch_'.$i])."</td>";
				}
				else
				{
					$table.="<td width=\"1%\" align=\"left\"></td>";
					$table.="<td width=\"49%\" align=\"left\">".nl2br($data['branch_'.$i])."</td>";
				}
				$table.="</tr>";
			}
		}
		$table.="</table>";
		$y1=$this->GetY();
		$this->writeHTML($table,1,0,0,0,"C");
		$y2=$this->GetY();
		// linie boczne i środkowa
		// $this->line($m,$y1,$m,$y2);
		$this->line($m+$w/2,$y1,$m+$w/2,$y2);
		$this->line($m+$w,$y1,$m+$w,$y2);
		// linia dolna
		$this->Cell($w/2,1,"","",0,"L");
		$this->Cell($w/2,1,"","T",1,"L");
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie miejsca i daty wystawienia oraz daty sprzedaży
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function city_date($data)
	{
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu']));
		$w=$this->width;
		$this->Ln(18);
		$this->SetFont('consola','',10);
		// miejsce wystawienia
		$this->Cell($w*0.25,16,$data['lang_place'],0,0,"L");
		$this->Cell($w*0.71,16,$data['place'],0,1,"L");
		// data wystawienia
		if($data['date_create']!='--------' AND $data['date_create']!='-0001-11-30' AND $data['date_create']!='0000-00-00' AND $data['date_create']!='' AND $data['date_create']!='1970-01-01') {
		$this->Cell($w*0.25,16,$data['lang_date_create'],0,0,"L");
		$this->Cell($w*0.71,16,date("Y-m-d",strtotime($data['date_create'])),0,1,"L");
		}
		// data sprzedaży
		if($data['date_sell']!='--------' AND $data['date_sell']!='-0001-11-30' AND $data['date_sell']!='0000-00-00' AND $data['date_sell']!='' AND $data['date_sell']!='1970-01-01') {
		$this->Cell($w*0.25,16,$data['lang_date_sell'],0,0,"L");
		$this->Cell($w*0.71,16,date("Y-m-d",strtotime($data['date_sell'])),0,1,"L");
		}
	}
	/**
	 * Utworzenie nagłówka
	 *
	 * @param int $id
	 *        	indtyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function head($id,$data)
	{
		$w=$this->width;
		$m=$this->margin;
		$this->SetFont('consola','',10);
		// dane dodatkowe
		if ($id)
		{
			if ($this->extra->logo and file_exists(ROOT_DIR.$this->extra->logo))
			{

				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->logo);
				if ($hi>65)
				{
					$wi=round($wi*65/$hi);
					$hi=65;
				}
				$this->Image(ROOT_DIR.$this->extra->logo,$this->margin,$this->GetY(),($wi<150?$wi:150),0,'','','RTL',true);

				$this->Cell($this->pixelsToUnits($wi)+0.5,$this->pixelsToUnits($hi),'',0,0,"L",0);
				$this->Cell($w-$this->pixelsToUnits($wi)+0.5,$this->pixelsToUnits($hi),$this->extra->head,0,1,"L",0);
			}
			else
			{
				$this->MultiCell($w,16,$this->extra->head,"","L",0,1);
			}
		}
		// nagłówek
		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl']));
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl']));
		$this->SetFont('consola','B',16);
		$this->Cell($w*0.8,20,$data['lang_head'].' '.$data['lang_number'].' '.$data['number'],0,0,'L',1);

		// numer

		// oryginał/kopia
		if ($data['original_v']==3)
		{
			if ($data['original_v']==1)
			{
				$this->Cell($w*0.2,16,$data['lang_original'],0,0,"R",1);
			}
			if ($data['original_v']==2)
			{
				$this->Cell($w*0.2,16,$data['lang_copy'],0,1,"R",1);
			}
			if ($data['original_v']==3)
			{
				$this->Cell($w*0.2,16,$data['lang_double'],0,1,"R",1);
			}
		}
		else
		{
			if ($data['original_v']!=1&&$data['original_v']!=0)
			{
				$this->Cell($w*0.2,16,$data['lang_copy'],0,1,"R",1);
			}
			if ($data['original_v']!=2&&$data['original_v']!=0)
			{
				$this->Cell($w*0.2,16,$data['lang_original'],0,1,"R",1);
			}
		}
	}
	/**
	 * Sprawdzenie katalogu do zapisu i ewentualne stworzenie go
	 *
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param int $client
	 *        	identyfikator klienta
	 * @return string ścieżka do katalogu
	 */
	private function check_dir($id,$client=0)
	{
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/".$client."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		return $dir;
	}
	/**
	 * Sprawdzenie nazwy plikui ewentualna zmiana
	 *
	 * @param string $dir
	 *        	katalog zapisu
	 * @param string $name
	 *        	domyślna nazwa pliku
	 * @return string nazwa pliku
	 */
	private function check_file($dir,$name)
	{
		global $func;
		$name=$func->create_url($name);
		$name=$name?"faktura-".$name:"faktura";
		if (file_exists($dir.$name.".pdf"))
		{
			$count=0;
			while (file_exists($dir.$name."-".$count.".pdf"))
			{
				$count++;
			}
			$name=$name."-".$count;
		}
		return $name.".pdf";
	}
}
?>