<?php
require_once(ROOT_DIR.'module/invoice/main_pdf.php');

class class_pdfP extends mainPDF
{
	private $sum;

	public function __construct($id,$count,$data,$what=1)
	{
		parent::__construct($id);
		$this->gen($id,$count,$data,$what);
		$this->id=$id;
		$this->client=$data['client_id'];
		$this->name=$data['number'];
	}
	/**
	 * Generator
	 */
	public function gen($id,$count,$data,$what=1)
	{
$css= '<style>
table#invoice_number_all{background-color:#'.$data['kolor_tla_nagl'].';color:#'.$data['kolor_tekstu_nagl'].';height:500px;}
.invoice_number{font-weight:bold;font-size:16pt;text-align:center;}
.orginal_copy{font-size:16px;	font-weight:bold;text-align:center;}
.invoice_place_date{padding:6px 1px;color:#'.$data['kolor_tekstu'].';font-size:7px;}
.invoice_place_date td{}
.invoice_place_date .nagl{font-size:7px;text-align:right;width:80px;}
.user_nagl{color:#'.$data['kolor_tekstu_nagl'].';background-color:#'.$data['kolor_tla_nagl'].';width:100%;}
table#branch{font-size:7px;}
table#branch .nagl{text-align:right;font-size:6px;}
table#branch tr{font-weight:normal;color:#'.$data['kolor_tekstu'].';text-align:left;}
table#user_client{font-size:7px;}
table#user_client .nagl{font-size:6px;}
table#user_client tr{color:#'.$data['kolor_tekstu'].';}
table#products{padding:3px 2px;border:1px solid #'.$data['kolor_ramki'].';font-size:7pt;}
table#products th {background-color:#'.$data['kolor_tla_nagl'].';color:#'.$data['kolor_tekstu_nagl'].';text-align:center;font-weight:bold;border:1px solid #'.$data['kolor_ramki'].';}
table#products td {margin-top:3px;color:#444444;background-color:transparent;border:1px solid #'.$data['kolor_ramki'].';}
table#products tr.parzysty td{background-color:#'.$data['kolor_dodatkowy'].';}
table#sum_vat{padding:3px 2px;border:1px solid #'.$data['kolor_ramki'].';font-size:7pt;}
table#sum_vat th {background-color:#'.$data['kolor_tla_nagl'].';color:#'.$data['kolor_tekstu_nagl'].';text-align:center;font-weight:bold;border:1px solid #'.$data['kolor_ramki'].';}
table#sum_vat td {margin-top:3px;color:#444444;background-color:transparent;border:1px solid #'.$data['kolor_ramki'].';text-align:right;}
table#sum_vat td.podsumowanie{background-color:#'.$data['kolor_tla_nagl'].';color:#'.$data['kolor_tekstu_nagl'].';font-weight:bold;}
table#to_pay_all td{font-weight:bold;color:#'.$data['kolor_tekstu'].'}
table#to_pay_all th{color:#'.$data['kolor_tekstu'].';text-align:right;}
tr#to_pay{background-color:#'.$data['kolor_tla_nagl'].';color:#'.$data['kolor_tekstu_nagl'].'}
tr#to_pay td{color:#'.$data['kolor_tekstu_nagl'].'}
tr#to_pay th{color:#'.$data['kolor_tekstu_nagl'].'}
table#how_pay td{color:#'.$data['kolor_tekstu'].';}
</style>';
		$print=array();
		if ($what==1) { $print=array(1,2); }
		elseif ($what==2) { $print=array(1); }
		elseif ($what==3) { $print=array(2); }
		elseif ($what==4) { $print=array(3); }
		foreach ($print as $i=>$v)
		{
			$this->startPageGroup();
			$this->AddPage();
			$data['original_v']=$v;
			// dodanie nagłówka
			$this->head($id,$data,$css);
			// dodanie miejsca i daty wystawienia/sprzedaży
			//$this->city_date($data);
			// dodanie danych użytkownika i kontrahenta
			$this->user_client($data, $css);
			// adres dostawy
			//$this->branch($data,$css);
			// dodanie produktów
			$this->products($count,$data,$css);
			// dodanie podsumowania
			$this->sum($data,$css);
			// dane zapłaty
			$this->cash($data,$css);
			// podpisy
			$this->sign($id,$data,$css);
			// komentarze i stopka
			$this->foot($id,$data);
		}
	}
	/**
	 * Zapisanie dokumentu
	 */
	function savePdf()
	{
		$dir=$this->check_dir($this->id, $this->client);
		$name=$this->check_file($dir, $this->name);
		$this->Output($dir.$name,"F");
		return urlencode($dir.$name);
	}
	/**
	 * Utworzenie nagłówka
	 *
	 * @param int $id
	 *        	indtyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function head($id,$data,$css)
	{
		$w=$this->width;
		$m=$this->margin;
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu'])); // kolor tekstu
		// dane dodatkowe
		if ($id)
		{
			if ($this->extra->logo and file_exists(ROOT_DIR.$this->extra->logo))
			{
				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->logo);
				if ($hi>65)
				{
					$wi=round($wi*65/$hi);
					$hi=65;
				}
				$this->Image(ROOT_DIR.$this->extra->logo,$this->margin,$this->GetY(),($wi<150?$wi:150),0,'','','RTL',true);

				$this->Cell(($wi<150?$wi:150),$hi*0.75,'',0,0,"L",0);
				$this->Cell($w-($wi<150?$wi:150),$hi*0.75,$this->extra->head,0,1,"L",0);
			}
			else
			{
				$this->MultiCell($w,16,$this->extra->head,"","L",0,1);
			}
		}
	}
	private function user_client($data,$css)
	{
		$table=$css;
		$table.='<table id="user_client">';
		// $table .='<tr><th
		// colspan="3">'.$data['lang_user_name_main'].'</th></tr>';
		/* <th>'.$data['lang_client_name_main'].'</th></tr>'; */
		$licz_dane=0;
		$tab=array("name","address","nip","phone","mail","www","bank_name","account");
		
		foreach ($tab as $k=>$i)
		{

			
			if ($data['user_'.$i])
			{
				$table.='<tr>';
				$licz_dane++;
				$table.='<td width="11%"></td>';
				if ($data['pokaz_naglowki'])
				{
					$table.='<td width="27%" align="right" class="nagl">'.trim(($k==100?"":"").$data['lang_user_'.$i].($k==100?"":"")).'</td>';
					$table.='<td width="62%" align="left">'.trim(nl2br($data['user_'.$i])).'</td>';
				} else {

					$table.='<td width="89%" align="left">'.trim(nl2br($data['user_'.$i])).'</td>';
				}
			
				$table.='</tr>';
			}

			
		}
		for($a=0;$licz_dane<7;$licz_dane++){
			$table.='<tr><td></td><td></td><td></td></tr>';
		}
		$table.='</table>';
		$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki']));
		$y=$this->getY(); // nie zmieniac miejsca
		$x=$this->getX(); // nie zmieniac miejsca
		$this->ln(0);
		$this->writeHTMLCell($this->procent(33),'','',$y,$table,1,0,false,true,'L',FALSE); // nie
		                                                                                            // zmieniac
		                                                                                            // miejsca

		$this->SetX($x+20);
		$this->SetY($y+$this->getLastH());

		$this->StartTransform();
		$this->Rotate(90);
		$lol=$this->getLastH();

		$this->SetFontSize(9);
		$html=$data['lang_user_name_main'];
		//$this->writeHTMLCell($this->getLastH(),20, '', '', $html, 0, 0, false, true, 'J', true);
		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl']));  // kolor wypelnienia
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl'])); // kolor tekstu


		$aaa=$this->getLastH();
		$this->Cell($this->getLastH(),20,$html,1,0,0,1);
		$this->StopTransform();

		$this->SetXY($x+$this->procent(33),$y);
		$this->Cell($this->procent(2),$aaa,'',0,0,'',0);

		$html=$css;
		$html.='
		<table id="invoice_number_all">
		<tr><td class="invoice_number">'.$data['lang_head'].' '.$data['lang_number'].' '.$data['number'].'</td></tr>
		<tr><td class="orginal_copy">';

		// oryginał/kopia

		if ($data['original_v']==3)
		{
			/*if ($data['original_v']==1)
			{
				$html.=$data['lang_original'];
			}
			if ($data['original_v']==2)
			{
				$html.=$data['lang_copy'];
			}*/
			if ($data['original_v']==3)
			{
				$html.=$data['lang_double'];
			}
		}
		/*else
		{
			if ($data['original_v']!=1&&$data['original_v']!=0)
			{
				$html.=$data['lang_copy'];
			}
			if ($data['original_v']!=2&&$data['original_v']!=0)
			{
				$html.=$data['lang_original'];
			}
		}*/
		$html.='</td></tr></table>';
		$this->writeHTMLCell($this->procent(33),$aaa,'','',$html,1,0,1,true,'J',true);


		$html=$css;
		$html.='
		<table class="invoice_place_date" style="height:'.$aaa.'">';
		$html.='<tr>
			<td class="nagl">'.$data['lang_place'].'</td>
			<td>'.$data['place'].'</td>
		</tr>';
		if($data['date_create']!='--------' AND $data['date_create']!='-0001-11-30' AND $data['date_create']!='0000-00-00' AND $data['date_create']!='')
		{
			$html.='<tr>
			<td class="nagl">'.$data['lang_date_create'].'</td>
			<td>'.date("Y-m-d",strtotime($data['date_create'])).'</td>
			</tr>';
		}
		if($data['date_sell']!='--------' AND $data['date_sell']!='-0001-11-30' AND $data['date_sell']!='0000-00-00' AND $data['date_sell']!='')
		{
			$html.='<tr>
			<td class="nagl">'.$data['lang_date_sell'].'</td>
			<td>'.date("Y-m-d",strtotime($data['date_sell'])).'</td>
			</tr>';
		}
		$html.='</table>';
		$this->Cell($this->procent(2),$aaa,'',0,0,'',0);
		$this->writeHTMLCell($this->procent(30),$aaa,'','',$html,1,1,0,true,'J',true);
		$this->ln(8);
		$table=$css;

		$table.='<table id="user_client">';
		// $table .='<tr><th
		// colspan="3">'.$data['lang_user_name_main'].'</th></tr>';
		/* <th>'.$data['lang_client_name_main'].'</th></tr>'; */

		$tab=array("name","address","nip","phone","mail","bank_name","account");
		$licz_dane =0;
		foreach ($tab as $k=>$i)
		{

			if ($data['client_'.$i])
			{
			$licz_dane++;
				$table.='<tr>';
				$table.='<td width="7%"></td>';
				if ($data['pokaz_naglowki'])
				{
				$table.='<td width="18%" align="right" class="nagl">'.($k==100?"<b>":"").$data['lang_client_'.$i].($k==100?"</b>":"").'</td>';
				$table.='<td width="70%" align="left">'.trim(nl2br($data['client_'.$i])).'</td>';
				} else {
		
					$table.='<td width="88%" align="left">'.trim(nl2br($data['client_'.$i])).'</td>';
				
				}

				$table.='</tr>';
			}
		}
		if ($data['branch_v']) {
			for($a=0;$licz_dane<8;$licz_dane++){
				$table.='<tr><td></td></tr>';
			}
		} else {
			for($a=0;$licz_dane<5;$licz_dane++){
				$table.='<tr><td></td></tr>';
			}
		}
		$table.='</table>';
		$y=$this->getY(); // nie zmieniac miejsca
		$x=$this->getX(); // nie zmieniac miejsca
		$this->writeHTMLCell($this->procent(49),'','','',$table,1,0,false,true,'J',true);

		$this->SetX($x+20);
		$this->SetY($y+$this->getLastH());

		$this->StartTransform();
		$this->Rotate(90);
		$lol=$this->getLastH();

		$html=$data['lang_client_name_main'];
		// $this->writeHTMLCell($this->getLastH(),20, '', '', $html, 0, 0,
		// false, true, 'J', true);
		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl']));  // kolor wypelnienia
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl'])); // kolor tekstu
		$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki']));
		$aaa=$this->getLastH();
		$this->Cell($this->getLastH(),20,$html,1,0,0,1);
		$this->StopTransform();

		$this->SetXY($x+$this->procent(49),$y);
		// $this->Cell($this->procent(2), $aaa, '', 0, 1, '', 0);

		// adres dostawy
		if ($data['branch_v'])
		{
			// odczyt parametrów
			$w=$this->width;
			$m=$this->margin;
			// dane
			$table=$css;
			$table.='<table id="branch">';
			// $table .= '<tr><th></th><th colpsan="3">'. $data['lang_branch']
			// .'</th></tr>';
			$licz_dane =0;
			$tab=array("name","address","phone","mail","krs","bank_name","account");
			foreach ($tab as $k=>$i)
			{
				if ($data['branch_'.$i])
				{
					$licz_dane++;
					$table.="<tr>";
					$table.="<td width=\"6%\"></td>";
					if ($data['pokaz_naglowki'])
					{
					$table.="<td class=\"nagl\" width=\"16%\">".($k==0?"":"").$data['lang_branch_'.$i].($k==0?"":"")."</td>";
					$table.="<td width=\"70%\" align=\"left\">".nl2br($data['branch_'.$i])."</td>";
					} else {
						$table.="<td width=\"86%\" align=\"left\">".nl2br($data['branch_'.$i])."</td>";
					
					}
					$table.="</tr>";
				}
			}
			for($a=0;$licz_dane<8;$licz_dane++){
				$table.='<tr><td></td></tr>';
			}
			$table.="</table>";
			$this->Cell($this->procent(2),'','',0,0,'',0);
			$y=$this->GetY(); // nie zmieniac miejsca
			$x=$this->GetX(); // nie zmieniac miejsca

			$this->writeHTMLCell($this->procent(49),$aaa,'','',$table,1,0,false,true,'J',true);

			$this->SetXY($x,$y+$aaa);

			$this->StartTransform();
			$this->Rotate(90);
			$lol=$this->getLastH();

		$this->SetFillColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tla_nagl']));  // kolor wypelnienia
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu_nagl'])); // kolor tekstu
		$this->SetDrawColorArray($this->convertHTMLColorToDec('#'.$data['kolor_ramki']));
			$aaa=$this->getLastH();
			$html=$data['lang_branch'];

			$this->Cell($this->getLastH(),20,$html,1,0,0,1);
			$this->StopTransform();
			$this->SetXY($x+$this->procent(49),$y);
		}
	}

	/**
	 * Dodanie produktów
	 *
	 * @param int $count
	 *        	identyfikator ostatniego produktu
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function products($count,$data,$css)
	{
		$this->ln(98);
		// sprawdzamy czy jest jakis rabat.
		for ($a=1;$a<=$count;$a++)
		{
			if ($data['p_'.$a.'_name'])
			{
				if ($data['p_'.$a.'_rabat']!=0)
				{
					$jest_rabat=1;
				}
			}
		}

		// utworzenie zmiennych
		$vat=array(0,3,5,6,7,8,18,19,22,23,25);
		$sum=array();

		foreach ($vat as $i)
		{
			$sum[$i]=array(0,0,0);
		}
		// tabela
		$table=$css;
		$table.='<table id="products">';

		$table.='<tr>';

		$table.='<th width="3%"  valign="middle"  ><b>'.$data['lang_lp'].'</b></th>';
		if ($jest_rabat==1)
		{
			$table.='<th width="23%">'.$data['lang_name'].'</th>';
			$table.='<th width="8%">'.$data['lang_pkwiu'].'</th>';
			$table.='<th width="5%">'.$data['lang_amount'].'</th>';
			$table.='<th width="5%">'.$data['lang_unit'].'</th>';
			$table.='<th width="8%">'.$data['lang_netto'].'</th>';
			$table.='<th width="6%">'.$data['lang_rabat'].'</th>';
			$table.='<th width="8%">'.$data['lang_prize_netto'].'</th>';
		}
		else
		{
			$table.='<th width="27%">'.$data['lang_name'].'</th>';
			$table.='<th width="10%">'.$data['lang_pkwiu'].'</th>';
			$table.='<th width="9%">'.$data['lang_amount'].'</th>';
			$table.='<th width="7%">'.$data['lang_unit'].'</th>';
			$table.='<th width="10%">'.$data['lang_netto'].'</th>';
		}

		$table.='<th width="4%">'.$data['lang_vat'].'</th>';
		$table.='<th width="10%">'.$data['lang_sum_netto'].'</th>';
		$table.='<th width="10%">'.$data['lang_sum_vat'].'</th>';
		$table.='<th width="10%">'.$data['lang_sum_brutto'].'</th>';
		$table.='</tr>';

		// produkty
		$parzysty=0;
		for ($a=1;$a<=$count;$a++)
		{
			if ($data['p_'.$a.'_name'])
			{
				// $cnp=$data['p_'.$a.'_netto'];
				$cnp=str_replace(",",".",$data['p_'.$a.'_netto']);
				$cnp=$cnp==round($cnp,2)?number_format($cnp,2,".",""):($cnp==round($cnp,3)?number_format($cnp,3,".",""):$cnp);
				// $amo=$data['p_'.$a.'_amount'];
				$amo=str_replace(",",".",$data['p_'.$a.'_amount']);
				$t=explode(".",$amo);
				$t=strlen(rtrim($t[1],"0"));
				if ($t>4)
				{
					$t=4;
				}
				$amo=number_format($amo,$t,".","");
				// $amo=$amo==round($amo,2)?number_format($amo,2,".",""):($amo==round($amo,3)?number_format($amo,3,".",""):$amo);
				$cnr=$data['p_'.$a.'_pnetto'];
				$cnr=$cnr==round($cnr,2)?number_format($cnr,2,".",""):($cnr==round($cnr,3)?number_format($cnr,3,".",""):$cnr);
				/*
				 * if($jest_rabat == 1){ $table.='<tr><td colspan="12"
				 * style="background-color:transparent;height:5px;"></td></tr>';
				 * } else { $table.='<tr><td colspan="10"
				 * style="background-color:transparent;height:5px;"></td></tr>';
				 * }
				 */
				$parzysty++;
				if ($parzysty%2==0)
				{
					$table.='<tr class="parzysty" >';
				}
				else
				{
					$table.='<tr>';
				}
				$table.='<td align="right">'.$data['p_'.$a.'_lp'].'</td>';
				$table.='<td align="left">'.nl2br($data['p_'.$a.'_name']).'</td>';
				$table.='<td align="left">'.$data['p_'.$a.'_pkwiu'].'</td>';
				$table.='<td align="right">'.$amo.'</td>';
				$table.='<td align="center">'.$data['p_'.$a.'_unit'].'</td>';
				$table.='<td align="right">'.$cnp." </td>";
				if ($jest_rabat==1)
				{
					$table.='<td align="right">'.$data['p_'.$a.'_rabat'].'% </td>';
					$table.='<td align="right">'.$cnr.'</td>';
				}
				$table.='<td align="center">'.$data['p_'.$a.'_vat'].(is_numeric($data['p_'.$a.'_vat'])?"%":"").'</td>';
				$table.='<td align="right">'.$data['p_'.$a.'_snetto'].'</td>';
				$table.='<td align="right">'.$data['p_'.$a.'_svat'].'</td>';
				$table.='<td align="right">'.$data['p_'.$a.'_sbrutto'].'</td>';
				$table.='</tr>';

				// podliczenie
				$v=$data['p_'.$a.'_vat'];
				$sum[$v][0]+=$data['p_'.$a.'_snetto'];
				$sum[$v][1]+=$data['p_'.$a.'_svat'];
				$sum[$v][2]+=$data['p_'.$a.'_sbrutto'];
				if ($a%50==0 and $a>0)
				{
					$table.="</table>";
					$this->SetY($this->GetY()-8);
					$this->writeHTML($table,1,0,0,0,"C");
					$table="<table border=\"1\">";
				}
			}
		}
		// dodanie tabeli
		$table.="</table>";
		$this->SetY($this->GetY()-8);
		$this->writeHTML($table,1,0,0,0,"C");

		$this->sum=$sum;
	}
	/**
	 * Wyświetlenie podliczenia
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sum($data,$css)
	{
		$table=$css;
		$table.='<table id="sum_vat">';
		$table.='<tr>';
		$table.='<th>'.$data['lang_all_rate'].'</th>';
		$table.='<th>'.$data['lang_all_netto'].'</th>';
		$table.='<th>'.$data['lang_all_vat'].'</th>';
		$table.='<th>'.$data['lang_all_brutto'].'</th>';
		$table.='</tr>';

		// zmienne
		$netto=0;
		$vat=0;
		$brutto=0;
		// kwoty
		foreach ($this->sum as $k=>$i)
		{
			if ($i[2])
			{
				$table.='<tr>';
				$table.='<td>'.$k.(is_numeric($k)?"%":"").'</td>';
				$table.='<td>'.number_format($i[0],2,","," ")." ".$data['current'].'</td>';
				$table.='<td>'.number_format($i[1],2,","," ")." ".$data['current'].'</td>';
				$table.='<td>'.number_format($i[2],2,","," ")." ".$data['current'].'</td>';
				$table.='</tr>';
				$netto+=$i[0];
				$vat+=$i[1];
				$brutto+=$i[2];
			}
		}

		// podsumowanie
		$table.='<tr>';
		$table.='<td class="podsumowanie">'.$data['lang_all_sum'].'</td>';
		$table.='<td class="podsumowanie">'.number_format($netto,2,","," ")." ".$data['current'].'</td>';
		$table.='<td class="podsumowanie">'.number_format($vat,2,","," ")." ".$data['current'].'</td>';
		$table.='<td class="podsumowanie">'.number_format($brutto,2,","," ")." ".$data['current'].'</td>';
		$table.='</tr>';
		$table.='</table>';
		$this->SetAlpha(0);
		$this->Cell($this->procent(40),18,'',0,0,'C',1);
		$this->SetAlpha(1);
		$this->writeHTML($table,1,0,0,0,"C");
	}

	/**
	 * Dane zapłaty
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function cash($data, $css)
	{
		$this->Ln(2);
		$this->SetFont('consola','',11);
		$sum=0;
		foreach ($this->sum as $i)
		{
			$sum+=$i[2];
		}
		$table=$css;
		$table.="<table border=\"0\" width=\"".($this->width*1)."\" id=\"to_pay_all\">";
		// zapłacono
		if (number_format($data['cash_pay'],2)>0)
		{
			$table.="<tr>";
			$table.="<th width=\"19%\"  > ".$data['lang_cash_pay']."</th>";
			$table.="<td width=\"1%\" ></td>";
			$table.="<td width=\"80%\" align=\"left\" colspan=\"4\" >".number_format($data['cash_pay'],2,",",".")." ".$data['current']."</td>";
			$table.="</tr>";
			$sum-=$data['cash_pay'];
		}
		// do zapłaty

		$table.="<tr id=\"to_pay\">";
		$table.="<th width=\"19%\"  >".$data['lang_cash_sum']."</th>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"80%\" align=\"left\" colspan=\"4\">".number_format($sum,2,","," ")." ".$data['current']."</td>";
		$table.="</tr>";
		// dodanie tabeli
		//$table.="</table>";
		// $this->Cell($this->width*0.3,0,"",0,0);
		//$this->writeHTML($table,1,0,0,0,"R");
		// słownie
		// $this->Ln(1);

		//$this->SetY($this->GetY()-10);
		//$table="<table border=\"0\">"; //jak nie bedzie problenów wywalić
		$table.="<tr>";
		$table.="<th width=\"19%\" >".$data['lang_cash_word']."</th>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"80%\" align=\"left\" >".$data['cash_word']."</td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"L");
		$this->Ln(2);

		$table=$css;
		// druga tabela
		$this->SetFont('consola','',8);
		$table.="<table id=\"how_pay\" border=\"0\" >";
		// sposób i termin zapłaty
		$table.="<tr>";
		$table.="<td width=\"19%\" align=\"right\" >".$data['lang_cash_type']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"80%\" align=\"left\" >".$data['cash_type']."</td>";
		$table.="</tr>";

    if($data['cash_date']=="list" AND ((int)$data['cash_date_list']))
    {
    $table.="<tr>";
		
		$table.="<td width=\"19%\"  align=\"right\" >".$data['lang_cash_date']."</td>";
		$table.="<td width=\"1%\" ></td>";
		$table.="<td width=\"80%\" align=\"left\" >".$data['cash_date_list']."</td>";
	   $table.="</tr>";
	      $table.="<tr>";
		 $table.="<td width=\"20%\"></td>";
		$table.="<td width=\"80%\" align=\"left\" >".date("Y-m-d",strtotime($data['date_create'])+86400*((int)$data['cash_date_list']))."</td>";
		$table.="</tr>";
    }
    else
    {
		$table.="<tr>";
	
		$table.="<td width=\"19%\" align=\"right\" >".$data['lang_cash_date']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"84%\" align=\"left\" >".($data['cash_date']=="list"?$data['cash_date_list']:date("Y-m-d",strtotime($data['cash_date_date'])))."</td>";
		$table.="</tr>";
		}
		// dodanie tabeli
		$table.="</table>";
		// $this->Cell($this->width*0.3,0,"",0,0);
		$this->writeHTML($table,1,0,0,0,"L");
	}
	/**
	 * Podpisy
	 *
	 * @param int $id
	 *        	idnetyfikator uzytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sign($id,$data)
	{
		$w=$this->width;
		$m=$this->margin;
		$this->Ln(25);
		// wstawienie podpisu
		if ($id)
		{
			if ($this->extra->sign and file_exists(ROOT_DIR.$this->extra->sign))
			{
				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->sign);
				if ($hi>55)
				{
					$wi=round($wi*55/$hi);
					$hi=55;
				}
				$wi=$wi<110?$wi:110;
				$this->Image(ROOT_DIR.$this->extra->sign,$w*0.725+$m-$wi/2,$this->GetY(),$wi,0,'','','RTL',true);
				$this->Ln($hi);
			}
			else
			{
				$this->Ln(25);
			}
		}

		$this->SetFont('consola','',8);
		$y=$this->GetY();
		$this->Line($w*0.1+$m,$y,$w*0.45+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$this->Line($w*0.55+$m,$y,$w*0.90+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$this->SetDrawColor(255); // kolor rysowania (lini)
		$table="<table border=\"0\">";
		$table.="<tr>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="<td width=\"35%\" align=\"center\" colspan=\"3\">".$data['lang_client_sign']."</td>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="<td width=\"35%\" align=\"center\" colspan=\"3\">".$data['lang_user_sign']."</td>";
		$table.="<td width=\"10%\" align=\"right\"></td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"R");
	}

	/**
	 * Komentarze i stopka
	 *
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function foot($id,$data)
	{

		$w=$this->width;
		$kolor_lini = $this->convertHTMLColorToDec('#'.$data['kolor_tekstu']);
		$this->SetLineStyle(array("color"=>$kolor_lini,'dash'=>0));
		$this->Ln(40);
		// ustalenie kolorów linii

		$this->SetFont('consola','',8);
		$this->SetTextColorArray($this->convertHTMLColorToDec('#'.$data['kolor_tekstu'])); // kolor tekstu
		// komentarz
		if ($data['coment']!='')
		{
			$this->MultiCell($w,16,$data['coment'],"T","L",0,1);
		}
		// dane dodatkowe

		if ($id)
		{
			if ($this->extra->info!='')
			{
				$this->MultiCell($w,16,$this->extra->info,"T","L",0,1);
			}
		}

	}
	/**
	 * Prawdopodobnei do usuniecia
	 */
	function procent($procent)
	{
		$result=($this->width/100)*$procent;
		return $result;
	}
	/**
	 * Sprawdzenie katalogu do zapisu i ewentualne stworzenie go
	 *
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param int $client
	 *        	identyfikator klienta
	 * @return string ścieżka do katalogu
	 */
	private function check_dir($id,$client=0)
	{
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/".$client."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		return $dir;
	}
	/**
	 * Sprawdzenie nazwy plikui ewentualna zmiana
	 *
	 * @param string $dir
	 *        	katalog zapisu
	 * @param string $name
	 *        	domyślna nazwa pliku
	 * @return string nazwa pliku
	 */
	private function check_file($dir,$name)
	{
		global $func;
		$name=$func->create_url($name);
		$name=$name?"faktura-".$name:"faktura";
		if (file_exists($dir.$name.".pdf"))
		{
			$count=0;
			while (file_exists($dir.$name."-".$count.".pdf"))
			{
				$count++;
			}
			$name=$name."-".$count;
		}
		return $name.".pdf";
	}
}
?>