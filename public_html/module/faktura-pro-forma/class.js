invoice_obj = {
	current : "PLN",

  change_account : function(nr)
  {
    var s=0;
    if(nr) { var sr='user_account'; var dt='user_bank_name'; }
    else { var sr='user_bank_name'; var dt='user_account'; }
    var o=$(sr).getElementsByTagName('option');
    for(var a=0;a<o.length;a++)
    {
      if(o[a].selected) { s=a; break; }
    }
    o=$(dt).getElementsByTagName('option');
    o[s].selected=true;
  },
	/**
	 * Dodanie pozycji
	 * @param name nazwa produktu/usługi
	 * @param pkwiu numer pkwiu
	 * @param unit jednostka
	 * @param vat stawka VAT
	 * @param netto cena netto
	 * @param amount ilość
	 * @param rabat wysokość rabatu
	 */
	add : function(name, pkwiu, unit, vat, netto, amount, rabat)
	{
		$('count').value++;
		$('count_n').value++;
		var id=$('count').value;
		var row=$('row_position').cloneNode(false);
		//przekopiowanie dzieci
		var t=$('row_position').childNodes;
		for(var a=0;a<t.length;a++)
		{
			var x=t[a].cloneNode(true);
			if(x.innerHTML) { x.innerHTML=x.innerHTML.replace(/\[id\]/g,id); }
			row.appendChild(x);
		}
		row.id="posiotion_"+id;
		$('product_list').appendChild(row);
		//alert(row.innerHTML);
		//podmiana zmiennych
		if(!name) { name=""; }
		if(!pkwiu) { pkwiu=""; }
		if(!unit) { unit=""; }
		if(!vat) { vat=0; }
		if(!netto) { netto=0; }
		if(!amount) { amount=1; }
		if(!rabat) { rabat=0; }
		$('p_'+id+'_lp').value=$('count_n').value+".";
		$('p_'+id+'_name').value=name;
		$('p_'+id+'_pkwiu').value=pkwiu;
		$('p_'+id+'_amount').value=amount;
		$('p_'+id+'_unit').value=unit;
		$('p_'+id+'_vat').value=vat;
		$('p_'+id+'_rabat').value=round(rabat,2);
		$('p_'+id+'_netto').value=round(netto,4);
		this.count_all();
		return false;
	},
	/**
	 * Dodanie pozycji podliczenia VAT
	 * @param rate stawka VAT
	 * @param netto kwota netto
	 * @param vat kwota VAT
	 */
	add_vat : function(rate, netto, vat)
	{
		var row=$('row_vat').cloneNode(true);
		row.innerHTML=row.innerHTML.replace(/\[id\]/g,rate);
		row.id="rate_"+rate;
		$('vat_list').appendChild(row);
		$('sumVatTable').style.display = 'table';
		//podmiana zmiennych
		if(!vat) { vat=0; }
		if(!netto) { netto=0; }
		$('v_'+rate+'_rate').value=rate+" %";
		$('v_'+rate+'_netto').value=round(netto,2);
		$('v_'+rate+'_vat').value=round(vat,2);
		$('v_'+rate+'_brutto').value=round(netto*1+vat*1,2) ;
	},
	/**
	 * Przeliczenie pozycji
	 * @param id identyfikator pozycji
	 */
	count : function(id)
	{
		if($('p_'+id+'_lp'))
		{
			var netto=$('p_'+id+'_netto').value.replace(",",".");
			$('p_'+id+'_netto').value=netto;
			var vat=Number($('p_'+id+'_vat').value.replace(",","."));
			vat=(isNaN(vat)?0:vat);
			var rabat=$('p_'+id+'_rabat').value.replace(",",".");
			$('p_'+id+'_rabat').value=rabat;
			var amount=$('p_'+id+'_amount').value.replace(",",".");
			$('p_'+id+'_amount').value=amount;
			//po rabacie
			var pnetto=netto*(100-rabat)/100;
			//kwoty
			var snetto=pnetto*amount;
			var svat=pnetto*amount*vat/100;
			var sbrutto=snetto+svat;
			//przepisanie
			$('p_'+id+'_pnetto').value=round(pnetto,2);
			$('p_'+id+'_snetto').value=round(snetto,2);
			$('p_'+id+'_svat').value=round(svat,2);
			//$('p_'+id+'_sbrutto').value=round(sbrutto,2);
			$('p_'+id+'_sbrutto').value=round(parseFloat(round(snetto,2))+parseFloat(round(svat,2)),2);
		}
	},
	/**
	 * Przeliczenie kwot VAT
	 */
	count_vat : function()
	{
		var vat_arr=new Array(26);
		var a, v, sn=0, sv=0;
		for(a=0;a<26;a++) { vat_arr[a]=new Array(0,0); }
		for(var a=1;a<=$('count').value;a++)
		{
			if($('p_'+a+'_lp'))
			{
				v=Number($('p_'+a+'_vat').value);
				v=(isNaN(v)?0:v);
				vat_arr[v][0]+=parseFloat($('p_'+a+'_snetto').value);
				vat_arr[v][1]+=parseFloat($('p_'+a+'_svat').value);
			}
		}
		$('vat_list').innerHTML="";
		for(a=0;a<26;a++)
		{
			if(vat_arr[a][0]!=0)
			{
				this.add_vat(a, vat_arr[a][0], vat_arr[a][1]);
				sn+=parseFloat(vat_arr[a][0]);
				sv+=parseFloat(vat_arr[a][1]);
			}
		}
		var to_pay=round(sn+sv,2);
		$('all_netto').value=round(sn,2);
		$('all_vat').value=round(sv,2);
		$('all_brutto').value=to_pay;
		$('cash_pay').value=$('cash_pay').value.replace(',','.');
		to_pay=round(to_pay-parseFloat("0"+$('cash_pay').value.replace(this.current,'')),2);
		$('cash_sum').value=to_pay+" "+this.current;
		$('cash_word').value=this.number_to_word(to_pay);
	},
	/**
	 * Zamiana liczby na słowa
	 * @param number słowa
	 * @return liczna słownie
	 */
	number_to_word : function(number)
	{
		try{
		var liczby_waluta=$('number_coin').value.split(",");
		var liczby_tysiac=$('number_tou').value.split(",");
		var liczby_milion=$('number_mln').value.split(",");
		var liczby_miliard=$('number_mld').value.split(",");
		var liczby_j=$('number_j').value.split(",");
		var liczby_n=$('number_n').value.split(",");
		var liczby_d=$('number_d').value.split(",");
		var liczby_s=$('number_s').value.split(",");
		var tmp;
		var tmpl;
		var text="";
		var grosze="";
		var liczba=number.toString().split(".");
		//obliczenie groszy
		if(liczba[1].length==0) { grosze="00/100"; }
		else if(liczba[1].length==1) { grosze=liczba[1]+"0/100"; }
		else if(liczba[1].length==2) { grosze=liczba[1]+"/100"; }
		//obliczenie całości
		var trojki=number_format(liczba[0],0,"."," ").split(" ");
		for(var a=trojki.length-1; a>=0; a--)
		{
			tmp="";
			tmpl=trojki[a];
			if(trojki[a]>=100) { tmp+=" "+liczby_s[Math.floor(trojki[a]/100)-1]; trojki[a]=trojki[a]%100; }
			if(trojki[a]>=20)
			{
				tmp+=" "+liczby_d[Math.floor(trojki[a]/10)-2]; trojki[a]=trojki[a]%10;
				if(trojki[a]!=0) { tmp+=" "+liczby_j[trojki[a]-1]; }
			}
			else if(trojki[a]>=10) { tmp+=" "+liczby_n[trojki[a]-10]; }
			else if(trojki[a]!=0) { tmp+=" "+liczby_j[trojki[a]-1]; }

			if((trojki.length-a)==2)
			{
				if(tmpl==1) { tmp+=" "+liczby_tysiac[0]; }
				else if(tmpl%10>1 && tmpl%10<5 && (tmpl<11 || tmpl>20)) { tmp+=" "+liczby_tysiac[1]; }
				else if(tmpl!=0) { tmp+=" "+liczby_tysiac[2]; }
			}
			else if((trojki.length-a)==3)
			{
				if(tmpl==1) { tmp+=" "+liczby_milion[0]; }
				else if(tmpl%10>1 && tmpl%10<5 && (tmpl<11 || tmpl>20)) { tmp+=" "+liczby_milion[1]; }
				else if(tmpl!=0) { tmp+=" "+liczby_milion[2]; }
			}
			else if((trojki.length-a)==4)
			{
				if(tmpl==1) { tmp+=" "+liczby_miliard[0]; }
				else if(tmpl%10>1 && tmpl%10<5 && (tmpl<11 || tmpl>20)) { tmp+=" "+liczby_miliard[1]; }
				else if(tmpl!=0) { tmp+=" "+liczby_miliard[2]; }
			}
			text=tmp+text;
		}
		if(number=="0.00") { text=liczby_j[9]; }
		text=text+" "+this.current+" "+grosze;
		//podzielenie wyniku na wiersze
		tmp=text.split(" ");
		text="";
		var suma=0;
		for(var a=0;a<tmp.length;a++)
		{
			if((suma+tmp[a].length)>75) { text=text+"\n"+tmp[a]+" "; suma=tmp[a].length; }
			else if(tmp[a].length>1) { text=text+tmp[a]+" "; suma+=tmp[a].length; }
		}
		}catch(err){alert(err);}
		return text;
	},
	/**
	 * Przeliczenie całej faktury
	 */
	count_all : function()
	{
		//podliczenie produktów
		for(var a=1;a<=$('count').value;a++)
		{ this.count(a); }
		//podliczenie stawek vat
		this.count_vat();
	},
	/**
	 * Usunięcie pozycji
	 * @param id identyfikator pozycji
	 */
	del : function(id)
	{
		$('product_list').removeChild($('posiotion_'+id));
		//przenumerowanie
		var c=1;
		for(var a=1;a<=$('count').value;a++)
		{
			if($('p_'+a+'_lp'))
			{
				$('p_'+a+'_lp').value=c+".";
				c++;
			}
		}
		$('count_n').value--;
		this.count_all();
		return false;
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"-0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przekreślenie/normalizacja napisu
	 * @param input identyfikator pola
	 */
	strike : function(input)
	{
		if(input=="original") { $('original_v').value='1'; $('original').style.textDecoration="none"; $('copy').style.textDecoration="line-through"; $('double').style.textDecoration="line-through"; }
		else if(input=="copy") { $('original_v').value='2'; $('copy').style.textDecoration="none"; $('original').style.textDecoration="line-through"; $('double').style.textDecoration="line-through"; }
		else if(input=="double") { $('original_v').value='3'; $('double').style.textDecoration="none"; $('copy').style.textDecoration="line-through"; $('original').style.textDecoration="line-through"; }
		return false;
	},
	/**
	 * Pokazanie adresu dostawy
	 */
	show_branch : function()
	{
		$('branch_show').style.display="none";
		$('branch_hide').style.display="inline-block";
		$('branch').style.display="block";
		$('branch_v').value=1;
		return false;
	},
	/**
	 * Ukrycie adresu dostawy
	 */
	hide_branch : function()
	{
		$('branch_show').style.display="inline-block";
		$('branch_hide').style.display="none";
		$('branch').style.display="none";
		$('branch_v').value=0;
		return false;
	},
	/**
	 * Określenie wysokości textarea
	 * @param obj obiekt textarea
	 */
	textarea_height : function(obj)
	{
		textareaHeight = 30;
		/*this.string_break(obj);
		var enter=obj.value.length-obj.value.replace(/\n/g,"").length+1;
		obj.style.height=(enter*17+2)+"px"; */
		//alert(obj.style.height+' '+obj.scrollHeight);

	/*if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
 {

    } else {
			obj.style.height=(obj.scrollHeight<26?26:obj.scrollHeight)+'px';
	}*/
		tmp = obj.scrollHeight/textareaHeight;
		if(tmp < 1){
			tmp = tmp +1;
		}
		obj.style.height=(parseInt(tmp)*textareaHeight)+'px';
     // obj.style.height=(obj.scrollHeight<textareaHeight?30:obj.scrollHeight)+'px';
	},
	
	
	/**
	 * Załamanie zbyt długich ciągów znaków
	 * @param obj obiekt formularza
	 */
	string_break : function(obj)
	{
	  //return false;
		switch(obj.name)
		{
			case "user_name":
			case "client_name":
			case "user_address":
			case "client_address":
			case "user_phone":
			case "client_phone":
			case "user_mail":
			case "client_mail":
				var max=40;
				break;
			case "coment":
				var max=190;
				break;
			default:
				var max=33;
				break;
		}
		var x, tmp, str, val="";
		var rows=obj.value.split("\n");
		for(var a=0; a<rows.length; a++)
		{
			if(rows[a].length>max)
			{
				str="";
				tmp=rows[a].split(" ");
				rows[a]="";
				var c=0;
				for(var b=0; b<tmp.length; b++)
				{
					if((str.length+tmp[b].length)<=max) { str+=(b==0?"":" ")+tmp[b]; }
					else if(tmp[b].length>max)
					{
						rows[a]+=str+(b==0?"":" ")+tmp[b].substr(0,max-str.length-1)+"-\n";
						tmp[b]=tmp[b].substring(max-str.length-1,tmp[b].length);
						str="";
						b--;
					}
					else { str+="\n"+tmp[b]; }
					c++;
					if(c==10) {break;}
				}
				rows[a]+=str;
			}
			val+=(a==0?"":"\n")+rows[a];
		}
		obj.value=val;
	},
	/**
	 * Wyczyszczenie faktury
	 */
	clean : function()
	{
		if(confirm_del())
		{
			$('product_list').innerHTML="";
			$('vat_list').innerHTML="";
			$('faktura').reset();
			$('count').value=0;
			$('count_n').value=0;
			this.add();
		}
	},
	/**
	 * Otwarcie panelu wyboru klienta
	 * @param search czy jest to wyszukiwanie
	 * @param page numer strony
	 * @param letter pierwsza litera wyszukiwania
	 */
	panel_client : function(search, page, letter)
	{
		if(!search) { search=0; }
		if(!page) { page=1; }
		if(!letter) { letter=""; }

		var req = mint.Request();
		req.OnSuccess = function()
		{
			var tmp=this.responseText.split("..|-|.|-|..");
			var letters=tmp[0].split(",");
			var pages=parseInt(tmp[1]);
			var clients=tmp[2].split("|-|.|-|");
			var a;
			//pokazanie panelu
			var size=window_size();
			var panel=$('client_panel').cloneNode(true);
			panel.id="show_panel";
			//dodanie liter
			var boxl=document.createElement("div");
			for(a=0;a<letters.length;a++)
			{
				var let=$('client_letter').cloneNode(true);
				let.id="";
				let.className=letter==letters[a]?"btn btn-default btn-xs active":"btn btn-default btn-xs";
				let.title=letters[a];
				let.innerHTML=""+letters[a]+"";
				boxl.appendChild(let);
			}
			//dodanie kontrahentów
			var boxc=document.createElement("div");
			for(a=0;a<clients.length;a++)
			{
				if(tmp[1])
				{
					var let=$('client_object').cloneNode(true);
					var tmp=clients[a].split(",",3);
					let.id="";
					let.title=tmp[0];
					let.innerHTML=tmp[1]+" - "+tmp[2];
					boxc.appendChild(let);
				}
			}
			//dodanie stronicowania
			var boxp=document.createElement("div");
			if(pages>1)
			{
				if(page!=1)
				{
					var let=$('client_page').cloneNode(true);
					let.id="";
					let.className="first";
					let.title=1;
					boxp.appendChild(let);
					var let=$('client_page').cloneNode(true);
					let.id="";
					let.className="prev";
					let.title=page-1;
					boxp.appendChild(let);
				}
				for(a=page-2>0?page-2:1,max=page+3;a<max,a<=pages;a++)
				{
					var let=$('client_page').cloneNode(true);
					let.id="";
					let.className=a==page?"btn btn-default btn-xs active":"btn btn-default btn-xs";
					let.title=a;
					let.innerHTML=a;
					boxp.appendChild(let);
				}
				if(page!=pages)
				{
					var let=$('client_page').cloneNode(true);
					let.id="";
					let.className="next";
					let.title=page+1;
					boxp.appendChild(let);
					var let=$('client_page').cloneNode(true);
					let.id="";
					let.className="last";
					let.title=pages;
					boxp.appendChild(let);
				}
			}
			//podmiana zmiennych
			panel.innerHTML=panel.innerHTML.replace(/\[id\]/g,"p");
			panel.innerHTML=panel.innerHTML.replace(/\[letters\]/g,boxl.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[clients\]/g,boxc.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[pages\]/g,boxp.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[search\]/g,search?"1":"0");
			panel.innerHTML=panel.innerHTML.replace(/\[letter\]/g,"'"+letter+"'");
			if(search)
			{
				panel.innerHTML=panel.innerHTML.replace(/\[name\]/g,$("clients_p_name").value);
				panel.innerHTML=panel.innerHTML.replace(/\[address\]/g,$("clients_p_address").value);
				panel.innerHTML=panel.innerHTML.replace(/\[nip\]/g,$("clients_p_nip").value);
			}
			else
			{
				panel.innerHTML=panel.innerHTML.replace(/\[name\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[address\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[nip\]/g,"");
			}
			if($('show_panel')) { document.body.removeChild($('show_panel')); }
			document.body.appendChild(panel);
			//$('show_panel').style.top=((size[1]-$('show_panel').clientHeight)/2)+"px";
			//$('show_panel').style.left=((size[0]-$('show_panel').clientWidth)/2)+"px";
			$('show_panel').style.marginTop="-"+(($('show_panel').clientHeight-6)/2)+"px";
		};
		req.AddParam("page",page);
		req.AddParam("letter",letter);
		req.AddParam("user_id",$("user_id").value);
		if(search)
		{
			req.AddParam("name",$("clients_p_name").value);
			req.AddParam("address",$("clients_p_address").value);
			req.AddParam("nip",$("clients_p_nip").value);
		}
		req.Send("module/faktura-pro-forma/client_panel.php");
		return false;
	},
	/**
	 * Załadowanie klienta
	 * @param id identyfikator klienta
	 */
	load_client : function(id)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			var tmp=this.responseText.split("|-|.|-|");
			$('client_name').value=tmp[0]?tmp[0]:"";
			$('client_address').value=tmp[1]?tmp[1]:"";
			$('client_nip').value=tmp[2]?tmp[2]:"";
			$('client_phone').value=tmp[3]?tmp[3]:"";
			$('client_mail').value=tmp[4]?tmp[4]:"";
			$('client_bank_name').value=tmp[6]?tmp[6]:"";
			$('client_account').value=tmp[7]?tmp[7]:"";
			$('client_id').value=tmp[8]?tmp[8]:"";
			if(tmp[9])
			{
				$('branch_name').value=tmp[9]?tmp[9]:"";
				$('branch_address').value=tmp[10]?tmp[10]:"";
				$('branch_phone').value=tmp[11]?tmp[11]:"";
				$('branch_mail').value=tmp[12]?tmp[12]:"";
				$('branch_bank_name').value=tmp[14]?tmp[14]:"";
				$('branch_account').value=tmp[15]?tmp[15]:"";
				invoice_obj.show_branch();
			}
			invoice_obj.resize_textareas();
			document.body.removeChild($('show_panel'));
		};
		req.AddParam("id",id);
		req.AddParam("user_id",$("user_id").value);
		req.Send("module/faktura-pro-forma/client_load.php");
		return false;
	},
	/**
	 * Zamknięcie okna wyboru klienta
	 */
	close_client : function()
	{
		document.body.removeChild($('show_panel'));
		return false;
	},
	/**
	 * Otwarcie panelu wyboru produktu
	 * @param search czy jest to wyszukiwanie
	 * @param page numer strony
	 * @param letter pierwsza litera wyszukiwania
	 */
	panel_prod : function(search, page, letter)
	{
		if(!search) { search=0; }
		if(!page) { page=1; }
		if(!letter) { letter=""; }

		var req = mint.Request();
		req.OnSuccess = function()
		{
			var tmp=this.responseText.split("..|-|.|-|..");
			var letters=tmp[0].split(",");
			var pages=parseInt(tmp[1]);
			var cats=tmp[2].split("|-|.|-|");
			var prods=tmp[3].split("|-|.|-|");
			var a;
			//pokazanie panelu
			var size=window_size();
			var panel=$('prod_panel').cloneNode(true);
			panel.id="show_prod";
			//dodanie liter
			var boxl=document.createElement("div");
			for(a=0;a<letters.length;a++)
			{
				var let=$('prod_letter').cloneNode(true);
				let.id="";
				let.className=letter==letters[a]?"btn btn-default btn-xs active":"btn btn-default btn-xs";
				let.title=letters[a];
				let.innerHTML=""+letters[a]+"";
				boxl.appendChild(let);
			}
			//dodanie produktów
			var boxc=document.createElement("div");
			for(a=0;a<prods.length;a++)
			{
				if(tmp[3])
				{
					var let=$('prod_object').cloneNode(true);
					var tmp=prods[a].split(",");
					var prod_name = tmp[3];
					if(typeof tmp[4] != 'undefined' && tmp[4] != ''){
						prod_name = prod_name+','+tmp[4]; 
					}
					if(typeof tmp[5] != 'undefined' && tmp[5] != ''){
						prod_name = prod_name+','+tmp[5];
					}
					let.id="";
					let.title=tmp[0];
					let.innerHTML="<span class='column1'>"+prod_name+"</span><span class='column2'>"+tmp[1]+" "+invoice_obj.current+"</span><span class='column3'> "+tmp[2]+""+invoice_obj.current+"</span> ";
					boxc.appendChild(let);
				}
			}
			//dodanie stronicowania
			var boxp=document.createElement("div");
			if(pages>1)
			{
				if(page!=1)
				{
					var let=$('prod_page').cloneNode(true);
					let.id="";
					let.className="first";
					let.title=1;
					boxp.appendChild(let);
					var let=$('prod_page').cloneNode(true);
					let.id="";
					let.className="prev";
					let.title=page-1;
					boxp.appendChild(let);
				}
				for(a=page-2>0?page-2:1,max=page+3;a<max,a<=pages;a++)
				{
					var let=$('prod_page').cloneNode(true);
					let.id="";
					let.className=a==page?"btn btn-default btn-xs active":"btn btn-default btn-xs";
					let.title=a;
					let.innerHTML=a;
					boxp.appendChild(let);
				}
				if(page!=pages)
				{
					var let=$('prod_page').cloneNode(true);
					let.id="";
					let.className="next";
					let.title=page+1;
					boxp.appendChild(let);
					var let=$('prod_page').cloneNode(true);
					let.id="";
					let.className="last";
					let.title=pages;
					boxp.appendChild(let);
				}
			}
			//podmiana zmiennych
			panel.innerHTML=panel.innerHTML.replace(/\[id\]/g,"p");
			panel.innerHTML=panel.innerHTML.replace(/\[letters\]/g,boxl.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[prods\]/g,boxc.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[pages\]/g,boxp.innerHTML);
			panel.innerHTML=panel.innerHTML.replace(/\[search\]/g,search?"1":"0");
			panel.innerHTML=panel.innerHTML.replace(/\[letter\]/g,"'"+letter+"'");
			if(search)
			{
				panel.innerHTML=panel.innerHTML.replace(/\[name\]/g,$("prods_p_name").value);
				panel.innerHTML=panel.innerHTML.replace(/\[producer\]/g,$("prods_p_producer").value);
				panel.innerHTML=panel.innerHTML.replace(/\[cat\]/g,$("prods_p_cat").value);
				panel.innerHTML=panel.innerHTML.replace(/\[number\]/g,$("prods_p_number").value);
				panel.innerHTML=panel.innerHTML.replace(/\[pkwiu\]/g,$("prods_p_pkwiu").value);
				panel.innerHTML=panel.innerHTML.replace(/\[prize_min\]/g,$("prods_p_prize_min").value);
				panel.innerHTML=panel.innerHTML.replace(/\[prize_max\]/g,$("prods_p_prize_max").value);
			}
			else
			{
				panel.innerHTML=panel.innerHTML.replace(/\[name\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[producer\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[cat\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[number\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[pkwiu\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[prize_min\]/g,"");
				panel.innerHTML=panel.innerHTML.replace(/\[prize_max\]/g,"");
			}
			if($('show_prod')) { document.body.removeChild($('show_prod')); }
			document.body.appendChild(panel);
			//$('show_prod').style.top=((size[1]-$('show_prod').clientHeight)/2)+"px";
			//$('show_prod').style.left=((size[0]-$('show_prod').clientWidth)/2)+"px";
			$('show_prod').style.marginTop="-"+(($('show_prod').clientHeight-6)/2)+"px";
			//dodanie kategorii
			for(a=0;a<cats.length-1;a++)
			{
				var tmp=cats[a].split(",",3);
				var let=new Option(tmp[2],tmp[0]);
				let.style.paddingLeft=(tmp[1]*10-8)+"px";
				$('prods_p_cat').appendChild(let);
			}
		};
		req.AddParam("page",page);
		req.AddParam("letter",letter);
		req.AddParam("user_id",$("user_id").value);
		if(search)
		{
			req.AddParam("name",$("prods_p_name").value);
			req.AddParam("producer",$("prods_p_producer").value);
			req.AddParam("cat",$("prods_p_cat").value);
			req.AddParam("number",$("prods_p_number").value);
			req.AddParam("pkwiu",$("prods_p_pkwiu").value);
			req.AddParam("prize_min",$("prods_p_prize_min").value);
			req.AddParam("prize_max",$("prods_p_prize_max").value);
		}
		req.Send("module/faktura-pro-forma/prod_panel.php");
		return false;
	},
	/**
	 * Załadowanie produktu
	 * @param id identyfikator produktu
	 */
	load_prod : function(id)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			var tmp=this.responseText.split("|-|.|-|");
			if($('count').value==1 && $('p_1_name').value=="")
			{
				invoice_obj.del(1);
				$('count').value=0;
			}
			invoice_obj.add(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], 1, 0);
			invoice_obj.resize_textareas();
			$('add_p_info').innerHTML="produkt/usługa <span class='bold'>"+tmp[0]+"</span> została dodana do faktury";
			//document.body.removeChild($('show_prod'));
		};
		req.AddParam("id",id);
		req.AddParam("user_id",$("user_id").value);
		req.Send("module/faktura-pro-forma/prod_load.php");
		return false;
	},
	/**
	 * Zamknięcie okna wyboru produktu
	 */
	close_prod : function()
	{
		document.body.removeChild($('show_prod'));
		return false;
	},
	/**
	 * Zmiana rozmiaru wszystkich textarea
	 */
	resize_textareas : function()
	{
		var tmp=document.getElementsByTagName("textarea");
		for(var a=0;a<tmp.length;a++)
		{ this.textarea_height(tmp[a]); }
	},
	/**
	 * Zapis do pdf
	 */
	save_pdf : function()
	{
		$('what_print').style.display='block';
    return false;
	},
	save_pdf_end : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			//alert(this.responseText);
			location.href="http://"+location.host+"/download.php?file="+this.responseText;
		};
		var what=1;
		what=$('print2').checked?2:what;
		what=$('print3').checked?3:what;
		what=$('print4').checked?4:what;
		req.SendForm("faktura","module/faktura-pro-forma/save_pdf.php?user_id="+$("user_id").value+"&count="+$("count").value+"&what="+what);
		$('what_print').style.display='none';
		return false;
	},
	/**
	 * Zapis do bazy
	 */
	save_base : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				if(confirm($('same_nr').value))
				{ invoice_obj.save_base_step2(); }
			}
			else
			{ invoice_obj.save_base_step2(); }
		};
		req.SendForm("faktura","module/faktura-pro-forma/check_nr.php?user_id="+$("user_id").value);
		return false;
	},
	/**
	 * Zapis do bazy
	 */
	save_base_step2 : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			//alert(this.responseText);
			if(this.responseText=="ok")
			{ info($('save_ok').value, 'save_base');

			$('save_base_error_all').style.display = 'block';

			}
			else
			{ info($('save_error').value, 'save_base');

			$('save_base_error_all').style.display = 'block';

			}
		};
		req.SendForm("faktura","module/faktura-pro-forma/save_base.php?user_id="+$("user_id").value+"&count="+$("count").value);
		return false;
	},
	/**
	 * Załadowanie faktury
	 * @param original czy przekreślony napis oryginał
	 * @param type rozdzaj zapłaty
	 * @param date rodzaj terminu zapłaty
	 * @param termin zapłaty
	 */
	load_invoice : function(original ,type, date, time, cash_pay, cur)
	{
		if(original==0) { $('double').style.textDecoration="line-through"; }
		else if(original==1) { this.strike('original'); }
		else if(original==2) { this.strike('copy'); }
		else if(original==3) { this.strike('double'); }
		else { this.strike('copy'); }
		this.current=cur?cur:"PLN";
		$('current').value=this.current;
    if(cash_pay) { $('cash_pay').value=cash_pay+' '+this.current; }
		if(type) { $('cash_type').value=type; }
		else { $('cash_type').options[0].select=true; }
		if(date=="date")
		{ $('cash_date_d').checked=true; }
		else
		{
			$('cash_date_l').checked=true;
			if(time){ $('cash_date_list').value=time; }
			else { $('cash_date_list').options[0].select=true; }
		}
		this.count_all();
	},
	/**
	 * Zapisanie wprowadzonego kontrahenta
	 */
	save_client : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			$('save_client_ok_error').style.display='none';
			$('save_client_bad_error').style.display='none';

			var tmp=this.responseText.split("|");
			if(tmp[0]=="ok")
			{
				$('client_id').value=tmp[1];
				info($('addc_ok').value, 'save_client_ok');
			}
			else
			{ info($('addc_error').value, 'save_client_bad'); }
		};
		req.AddParam("sign",$('client_name').value);
		req.AddParam("name",$('client_name').value);
		req.AddParam("address",$('client_address').value);
		req.AddParam("nip",$('client_nip').value);
		req.AddParam("phone",$('client_phone').value);
		req.AddParam("email",$('client_mail').value);
		req.AddParam("bank_name",$('client_bank_name').value);
		req.AddParam("account",$('client_account').value);
		req.AddParam("user_id",$("user_id").value);
		req.Send("module/faktura-pro-forma/add_client.php");
		return false;
	},
	/**
	 * Zapisanie wprowadzonego produktu
	 * @param id identyfikator produktu
	 */
	save_product : function(id)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
			$('save_product_ok_error').style.display='none';
			$('save_product_bad_error').style.display='none';
			info($('addp_ok').value, 'save_product_ok');

			}
			else
			{
			$('save_product_ok_error').style.display='none';
			$('save_product_bad_error').style.display='none';
			info($('addp_error').value, 'save_product_bad');

			}
		};
		req.AddParam("name",$('p_'+id+'_name').value);
		req.AddParam("cat",1);
		req.AddParam("number","");
		req.AddParam("pkwiu",$('p_'+id+'_pkwiu').value);
		req.AddParam("unit",$('p_'+id+'_unit').value);
		req.AddParam("vat",$('p_'+id+'_vat').value);
		req.AddParam("netto",$('p_'+id+'_netto').value);
		req.AddParam("brutto",$('p_'+id+'_netto').value*(1+$('p_'+id+'_vat').value*0.01));
		req.AddParam("user_id",$("user_id").value);
		req.Send("module/faktura-pro-forma/add_product.php");
		return false;
	},
	/**
	 * Przeliczenie netto na brutto
	 */
	n2b : function()
	{
		$('n2b_b').value=round($('n2b_n').value.replace(",",".")*(100+parseInt($('n2b_v').value))/100,2);
	},
	/**
	 * Przeliczenie brutto na netto
	 */
	b2n : function()
	{
		$('b2n_b').value=round($('b2n_n').value.replace(",",".")*100/(100+parseInt($('b2n_v').value)),2);
	},
	/**
	 * Zmiana waluty
	 * @param curr waluta
	 */
	change_current : function(curr)
	{
		this.current=curr;
		this.count_all();
	},

	period : function(obj)
	{
		if(obj.checked) { $('period').style.display="block"; }
		else { $('period').style.display="none"; }
	}
};