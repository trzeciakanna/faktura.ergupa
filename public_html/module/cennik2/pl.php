<?php
$lang=array();
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik on-line";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstrzych problemów";
$lang['pomoc2_tresc']="	Jeśłi potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "System wystawiania faktur on line pozwala w bardzo szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, fakturę zaliczkową lub korygującą<br> Posiada bardzo szerokie możliwości edycyjne wszystkich nagłówków np. sprzedawca, faktura, data sprzedaży itp...<br> Możliwość zapisania i wydruku stworzonej faktury w pliku PDF lub też wysłanie faktury zaraz po jej utworzeniu na maila do naszego kontrahenta.<br> Automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje pełen dostęp do wszystkich możliwości, takich jak: lista kontrahentów, baza produktów, archiwum faktur, faktury cykliczne, logo na fakturze i wiele innych przydatnych funkcji.";

$lang['cennik'] = "<h1>Bezpłatny 30-dniowy okres testowy</h1>
<p>
Po <a href='http://faktura.egrupa.pl/main/rejestracja/#r'>zarejestrowaniu</a> się przez okres 30 dni możesz bezpłatnie korzystać ze wszystkich dostępnych <a href='funkcje/' title='funkcje faktury'>funkcji</a>. Warunkiem tylko jest logowanie się na swoje konto.
W tym czasie możesz sprawdzić, wypróbować wszystkie <a href='funkcje/' title='funkcje faktury'>funkcje faktury</a> , które udostępniamy stałym klientom.
Po upływie 30-to dniowego okresu tu naszego programu, możesz przedłużyć ważność swojego konta na czas, który sam sobie wybierzesz.
Jeżeli nie przedłużysz ważności konta, możesz nadal korzystać z programu i wystawiać faktury ale bez dostępu do pełnej funkcjonalności.
</p>



<h1 style='text-align: left;'>Cennik abonamentu:</h1>

<ul id='cennik'>
<li>3 miesiące - <span> 28,50 zł</span></li>
<li>4 miesiące -  <span>37 zł</span></li>
<li>5 miesięcy -  <span>45 zł</span></li>
<li>6 miesięcy -  <span>52,50 zł</span></li>
<li>7 miesięcy -  <span>59,50 zł</span></li>
<li>8 miesięcy -  <span>66 zł</span></li>
<li>9 miesięcy -  <span>72 zł</span></li>
<li>10 miesięcy -  <span>77,50 zł</span></li>
<li>11 miesięcy -  <span>82,50 zł</span></li>
<li>12 miesiący -  <span>87 zł </span></li>
</ul>
<p>
Wykupienie abonamentu daje możliwość korzystanie ze wszystkich <a href='funkcje/' title='funkcje faktury'>funkcji faktury</a>. 
Po wygaśnięciu abonamentu zapisane faktury oraz wszelkie dane nie usuwają się. 
Masz do nich dostęp zaraz po przedłużeniu abonamentu. 
Niezależnie od okresu na jaki wykupisz abonament, masz zawsze pełen dostęp do wszystkich dodatkowych opcji faktury !

</p>";
?>