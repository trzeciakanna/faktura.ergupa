<?php
$lang=array();
$lang['pomoc_nagl']="Help Center:";
$lang['pomoc1_nagl']="Online Guide";
$lang['pomoc2_nagl']="Talk to a consultant";
$lang['pomoc3_nagl']="Contact Form - Ask Us a Question";
$lang['pomoc1_tresc']="Contains a detailed description of the functions of the application and the solutions to the most common problems";
$lang['pomoc2_tresc']="If you need help, call us!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="You can also ask a question by filling in our Contact Form";
$lang['wiecej']= "more";
$lang['do_pomocy'] = "Go to Help";
$lang['o_programie_nagl'] = "What is the online invoicing application?";
$lang['o_programie'] = "The online invoicing application allows you to create your own VAT invoice, proforma invoice or other, in a quick, simple and intuitive way, without having to register.<br> It has a lot of editing options for all the heading fields.<br> It is possible to save the created invoice in PDF format, or to print the invoice right after it is created.<br> Additionally, the service offers automatic calculation of net and gross values, with different VAT rates.<br>
Upon registration every member gains access to all options, such as: making lists of customers, adding products, automatic saving of your data, putting your logo onto your invoices, an archive of stored invoices and many more useful options...";

$lang['cennik'] = "<h1>Free 30-day trial period</h1>
<p>
After <a href='http://afaktura.pl/main/rejestracja/#r'>signing up</a> you can use all the available <a href='funkcje/' title='funkcje faktury'>options</a> for 30 days free. The only requirement is logging on to your account.
During the 30-day period you can try out and use all the <a href='funkcje/' title='funkcje faktury'>invoice options</a> that we offer to our regular customers.
After the 30-day free trial period you can extend the validity of your account for however long you wish.
If you do not extend your account you can still use the application and issue invoices, however, no full functionality will be available.
</p>



<h1 style='text-align: left;'>Pricelist for account extension:</h1>

<ul id='cennik'>
<li>1 month - <span>10 zł</li>
<li>2 months -  <span>19,50 zł</span></li>
<li>3 months - <span> 28,50 zł</span></li>
<li>4 months -  <span>37 zł</span></li>
<li>5 months -  <span>45 zł</span></li>
<li>6 months -  <span>52,50 zł</span></li>
<li>7 months -  <span>59,50 zł</span></li>
<li>8 months -  <span>66 zł</span></li>
<li>9 months -  <span>72 zł</span></li>
<li>10 months -  <span>77,50 zł</span></li>
<li>11 months -  <span>82,50 zł</span></li>
<li>12 months -  <span>87 zł </span></li>
</ul>
<p>
Buying the extension gives you the possibility to use all the <a href='funkcje/' title='funkcje faktury'>invoice options</a>. 
After the extension expires, none of the saved invoices are deleted. 
You still have access to them, right after you extend the validity of the account. 
Regardless of the length of your extension, you always have full access to all the extra options of the invoice!

</p>";
?>