<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/panel/product_add/class.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$products=new products();
//wywołanie funkcji odczytującej
if($_GET['name'] || $_GET['producer'] || $_GET['number'] || $_GET['pkwiu'] || $_GET['prize_min'] || $_GET['prize_max'] || $_GET['cat'])
{
	require(ROOT_DIR."includes/search.php");
	$search=new search();
	list($dane,$page,$last)=$products->read_list_search($_GET['user_id'],$_GET['page'],10,$_GET['name'],$_GET['producer'],$_GET['number'],$_GET['pkwiu'],$_GET['prize_min'],$_GET['prize_max'],$_GET['cat']);
}
else
{ list($dane,$page,$last)=$products->read_list($_GET['user_id'],$_GET['page'],10,$_GET['cat'],$_GET['letter'],true); }
list($cat, $sum)=$products->cat_tree($_GET['user_id'],0,0);
echo implode(",",$products->letter_list($_GET['user_id'],$_GET['cat']));
echo "..|-|.|-|..";
echo $last;
echo "..|-|.|-|..";
show_cat($cat);
echo "..|-|.|-|..";
for($a=0,$max=count($dane);$a<$max;$a++)
{ echo ($a==0?"":"|-|.|-|"); echo $dane[$a]->id.",".$dane[$a]->netto.",".$dane[$a]->brutto.",".strip_tags($dane[$a]->name); }

function show_cat($cat)
{
	foreach($cat as $i)
	{
		echo $i->id.",".$i->level.",".$i->name."|-|.|-|";
		show_cat($i->child);
	}
}
?>