<?php
$lang=array();
$lang['head']="Sign-up";
$lang['login']="Login";
$lang['pass']="Password";
$lang['repeat']="re-enter";
$lang['mail']="Email Address"; 
$lang['nip']="NIP";
$lang['send']="Sign Up";
$lang['bad_login']="The login is already taken";
$lang['bad_mail']="The email is already taken";
$lang['bad_pass']="The passwords are different";
$lang['add_ok']="The account has been added, check your email inbox.";
$lang['add_error']="An error occurred while adding the acount, please try again later.";
$lang['']="";
$lang['bad_nip']="Brak NIP'u";
$lang['bad_accept']="Musisz zaakceptować regulamin";
$lang['err_nip']="Ten numer NIP został już wykorzystany. Zabrania się tworzyć dublujących się kont. Jeżeli jesteś właścicielem tego numeru NIP skontaktuj się z administratorem w celu wyjaśnienia.";

?>