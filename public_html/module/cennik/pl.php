<?php
$lang=array();
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik on-line";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstszych problemów";
$lang['pomoc2_tresc']="	Jeśli potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000 </span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "System wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę VAT, proformę, bądź inną<br> Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych.<br> Możliwość zapisania stworzonej faktury w formacie PDF lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, i wiele wiele innych przydatnych opcji...";

$lang['cennik'] = "<h1>Bezpłatny 30-dniowy okres testowy</h1>
<p>
Po <a href='https://faktura.egrupa.pl/reg/'>zarejestrowaniu</a> się przez okres 30 dni możesz bezpłatnie korzystać ze wszystkich dostępnych <a href='funkcje/' title='funkcje faktury'>funkcji</a>.
W tym czasie możesz w pełni sprawdzić oraz wypróbować wszystkie funkcje faktury, które udostępniamy stałym klientom.
Po upływie 30-to dniowego okresu, program automatycznie przejdzie w wersję bezpłatną z ograniczoną funkcjonalnością. W każdej chwili możesz przedłużyć ważność swojego konta na czas, który sam sobie wybierzesz i przywrócić wszystkie funkcje oraz dane, które zapisałeś.
Jeżeli nie przedłużysz ważności konta, możesz nadal korzystać z programu i wystawiać faktury ale bez dostępu do pełnej funkcjonalności.
</p>



<h1 style='text-align: left;'>Cennik abonamentu:</h1>

<ul id='cennik'>
<li>3 miesiące - <span> 34,96 zł</span></li>
<li>6 miesięcy -  <span>56,91 zł</span></li>
<li>9 miesięcy -  <span>74.80 zł</span></li>
<li>12 miesiący -  <span>86.99 zł </span></li>
</ul>
<p style='text-align: left; font-weight: bold'>Podane kwoty są cenami NETTO</p>
<p>
Wykupienie abonamentu daje możliwość korzystanie ze wszystkich <a href='funkcje/' title='funkcje faktury'>funkcji faktury</a>. 
Po wygaśnięciu abonamentu zapisane faktury oraz wszelkie dane NIE usuwają się, przechowujemy je dla Ciebie przez 6 lat! 
Masz do nich pełen dostęp zaraz po przedłużeniu abonamentu. 
Niezależnie od okresu na jaki wykupisz abonament, masz zawsze pełen dostęp do wszystkich dodatkowych opcji faktury !

</p>";
?>