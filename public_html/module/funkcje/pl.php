<?php
$lang=array();
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik on-line";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstrzych problemów";
$lang['pomoc2_tresc']="	Jeśłi potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "Program do wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, badz inną<br> Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych.<br> Możliwość zapisania stworzonej faktury w formacie PDF lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, i wiele wiele innych przydatnych opcji...";
$lang['funkcje'] = '<h1 style="text-align: left;">Funkcje Faktury On-line:</h1>
<ul >

    <li><b><u>FUNKCJE FAKTURY DARMOWEJ</u></b></li><br>
    
    <li> Faktura darmowa pozwala na wystawianie faktur w każdej chwili (max 3 szt dziennie) bez potrzeby rejestracji.</li>
    <li> Darmowe faktury można zapisać w pdf na komputerze</li>
    <li> automatyczne przeliczanie wartości netto oraz brutto</li>
    <li> przedstawienie kwoty w formie liczbowej oraz słownej</li>
    <li> możliwość edycji wszystkich pól i nazw nagłówków</li>
    <li> możliwośc dodania niezliczonej ilości nowych pozycji</li>
    <li> możliwośc przypisania rabatu</li>
    <li> podstawowy wzór faktury w wydruku</li>
    <br>
    <li><b><u>FUNKCJE FAKTURY W ABONAMENCIE</u></b></li><br>
    
    
    <li> abonament pozwala na zapisanie faktury w pdf oraz na zabezpieczonych serwerach on-line, do których jest dostęp w każdej chwili z każdego miejsca na świecie po zalogowaniu się</li>
    <li> zachowaną fakturę na serwerze można w każdej chwili edytować, zmieniać wybrane dane, podglądać, drukować lub wysłać kontrahentowi na maila</li>
    <li> istnieje możliwość zapisania i edytowania danych swojej Firmy, co ułatwia i przyspieszy wystawianie faktury, ponieważ po zalogowaniu się dane te automatycznie pojawią się w formularzu wystawiania faktury</li>
    <li> możliwość zapisania i edytowania danych kontrahenta, co ułatwia wystawianie faktur stałym klientom, jednym kliknięciem wybieramy kontrahenta i jego dane wstawiaja się automatycznie do faktury</li>
    <li> archiwum listy faktur z zaawansowaną wyszukiwarką</li>
    <li> możliwość zapisania i edytowania produktu oraz jego ceny , pozwala to w szybki sposób dodać wartość sprzedaży lub usługi do faktury</li>
    <li> możliwość utworzenia kategorii sprzedawanych produktów lub usług, przez co przyspieszy się ich wyszukiwanie wraz z lepszą ich organizacją</li>
    <li> ulepszony wzór faktury w wydruku</li>
    <li> wysyłanie faktury na adres e-mail kontrahenta</li>
    <li> własna numeracja faktur, możliwośc dopisania nazwy w nr faktury, możliwość ustwienia od jakiego numeru system ma rozpocząć numerację</li>
    <li> zapis faktur do własnej bazy online</li>
    <li> lista faktur z podziałem na lata/miesiace/kontrahentów</li>
    <li> tworzenie faktur okresowych (cyklicznych)</li>
    <li> kontrolowanie płatnosci za wystawione faktury</li>
    <li> automatyczne przeliczanie wartości netto oraz brutto</li>
    <li> przedstawienie kwoty w formie liczbowej oraz słownej</li>
    <li> wybór sposobu zapłaty za fakturę</li>
    <li> opcja wysokości wpłaconej kwoty</li>
    <li> wybór terminu zapłaty (przypisane opcje, bądź dowolna data)</li>
    <li> wybór waluty</li>
    <li> możliwośc przypisania rabatu do wystawianych produktów, usług</li>
    <li> automatyczna numeracja kolejnych faktur</li>
    <li> wybór różnych szablonów faktur do wydruku (wkrótce)</li>
    <li> wystawianie faktury zaliczkowej</li>
    <li> wystawianie faktury pro forma</li>
    <li> rachunek uproszczony</li>
    <li> faktura korygująca</li>
    <li> export/import danych do pliku exel</li>
    <li> możliwość edycji wszystkich pól i nazw nagłówków</li>
    <li> wprowadzanie długiego opisu usługi/produktu</li>
    <li> możliwość dodania niezliczonej ilości nowych pozycji</li>
    <li> dodawanie i zarządzanie kontrahentami</li>
    <li> ustanawianie oddziałów dla kontrahenta</li>
    <li> wysyłanie wiadomosci do kontrahentów</li>
    <li> dodawanie i zarządzanie produktami</li>
    <li> podział produktów na kategorie</li>
    <li> statystyki faktur</li>
    <li> własny nagłówek w fakturze</li>
    <li> możliwość stworzenia podpisu graficznego faktury (zdjęcie pieczatki z podpisem)</li>
    <li> własne logo na fakturze</li>
    <li> możliwośc ustawienia stopki oraz uwag do faktury</li>
    <li> możliwośc przypisania kilku kont bankowych</li>
    <li> automatyczne wstawianie własnych danych do faktury</li>
    <li> pobieranie danych z listy kontrahentów w trakcie tworzenia faktury</li>
    <li> pobieranie produktów bezpośrednio do faktury w trakcie jej tworzenia</li>
    <li> zapis produktów przy tworzeniu faktury</li>
    <li> zapis kontrahentów przy tworzeniu faktury</li>
    <li> wystawianie nieograniczonej ilości faktur</li>
    <li> faktura w wersji angielskiej</li>
    <li> faktura w wersji niemieckiej (wkrótce)</li>
    <li> faktura w wersji rosyjskiej (wkrótce)</li>
    <li> raporty</li>
    <li> bezpłatna aktualizacja o nowe funkcje i usprawnienia w/g próśb użytkowników</li>
    <li> Pomoc telefoniczna</li>

</ul>';

?>