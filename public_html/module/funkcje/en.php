<?php
$lang=array();
$lang['pomoc_nagl']="Help Center:";
$lang['pomoc1_nagl']="Online Guide";
$lang['pomoc2_nagl']="Talk to a Consultant";
$lang['pomoc3_nagl']="Contact Form - Ask Us a Question";
$lang['pomoc1_tresc']="Contains a detailed description of the application functions and the solutions to the most common problems";
$lang['pomoc2_tresc']="	If you need help - call!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="	You can also ask us a question by filling in our Contact Form";
$lang['wiecej']= "more";
$lang['do_pomocy'] = "Go to Help";
$lang['o_programie_nagl'] = "What is the online invoicing application?";
$lang['o_programie'] = "The online invoicing application allows you to create your own VAT invoice, proforma invoice or other, in a quick, simple and intuitive way, without having to register.<br> It has a lot of editing options for all the heading fields.<br> It is possible to save the created invoice in PDF format, or to print the invoice right after it is created.<br> Additionally, the service offers automatic calculation of net and gross values, with different VAT rates.<br>
Upon registration every member gains access to all options, such as: making lists of customers, adding products, automatic saving of your data, putting your logo onto your invoices, an archive of stored invoices and many more useful options...";
$lang['funkcje'] = '<h1 style="text-align: left;">The Online Invoice Functions:</h1>
<ul >

    <li><b><u>FREE INVOICE FUNCTIONS</u></b></li><br>
    
    <li> print the VAT invoice</li>
    <li> automatically calculate Net and Gross values</li>
    <li> see the amounts in numbers and words</li>
    <li> edit all the fields and heading names</li>
    <li> add a limitless number of new items</li>
    <li> issue a limitless number of invoices</li><br>
    <br>
    <li><b><u>SUBSCRIPTION INVOICE FUNCTIONS</u></b></li><br>
    
    
    <li> print the VAT invoice</li>
    <li> save the invoice to a .PDF file</li>
    <li> send the invoice to the customers e-mail address</li>
    <li> edit your invoices</li>
    <li> save the invoices to your own database</li>
    <li> a list of invoices sorted by years/months/customers</li>
    <li> make a periodical (cyclical) invoice</li>
    <li> monitor your payments for the issued invoices</li>
    <li> automatically calculate Net and Gross values</li>
    <li> see the amounts in numbers and words</li>
    <li> select the payment method</li>
    <li> an option of the value of paid amount</li>
    <li> select the payment deadline (default options or users choice)</li>
    <li> select your currency</li>
    <li> automatic numbering of consecutive invoices</li>
    <li> invoice templates</li>
    <li> Pre-payment invoices</li>
    <li> Pro Forma invoices</li>
    <li> simplified bills</li>
    <li> Correction invoices</li>
    <li> export/import data to an Excel file</li>
    <li> edit all the fields and heading names</li>
    <li> enter a long name of a product/service</li>
    <li> add a limitless number of new items</li>
    <li> add and edit your customers</li>
    <li> appoint branches for customers</li>
    <li> send messages to your customers</li>
    <li> add and edit your products</li>
    <li> divide your products into categories</li>
    <li> an archive of issued invoices</li>
    <li> your own personal heading on the invoice</li>
    <li> make your own description base</li>
    <li> your own logo on the invoice</li>
    <li> automatically insert your own data to the invoice</li>
    <li> insert customers data from your list while making the invoice</li>
    <li> insert products directly into the invoice while making it</li>
    <li> save new products while making the invoice</li>
    <li> save new customers while making the invoice</li>
    <li> issue a limitless number of invoices</li>
    <li> invoice in English (soon)</li>
    <li> invoice in German (soon)</li>
    <li> invoice in Russian (soon)</li>
    <li> reports (soon)</li>
    <li> free update and upgrades</li>
    <li> telephone assistance</li>

</ul>';

?>