<?php
require_once(ROOT_DIR.'module/invoice/main_pdf.php');

class class_pdfR extends mainPDF
{
	private $sum;
	
	public function __construct($id,$count,$data,$what=1)
	{
		parent::__construct($id);
		$this->gen($id,$count,$data,$what);
		$this->id=$id;
		$this->client=$data['client_id'];
		$this->name=$data['number'];
	}
	/**
	 * Generator
	 */
	public function gen($id,$count,$data,$what=1)
	{
		$print=array();
		if ($what==1) { $print=array(1,2); }
		elseif ($what==2) { $print=array(1); }
		elseif ($what==3) { $print=array(2); }
		elseif ($what==4) { $print=array(3); }
		foreach ($print as $i=>$v)
		{
			$this->startPageGroup();
			$this->AddPage();
			$data['original_v']=$v;
			// dodanie nagłówka
			$this->head($id,$data);
			// dodanie miejsca i daty wystawienia/sprzedaży
			$this->city_date($data);
			// dodanie danych użytkownika i kontrahenta
			$this->user_client($data);
			// adres dostawy
			$this->branch($data);
			// dodanie produktów
			$this->products($count,$data);
			// dodanie podsumowania
			$this->sum($data);
			// dane zapłaty
			$this->cash($data);
			// podpisy
			$this->sign($id,$data);
			// komentarze i stopka
			$this->foot($id,$data);
		}
	}
	/**
	 * Zapisanie dokumentu
	 */
	function savePdf()
	{
		$dir=$this->check_dir($this->id, $this->client);
		$name=$this->check_file($dir, $this->name);
		$this->Output($dir.$name,"F");
		return urlencode($dir.$name);
	}
	/**
	 * Komentarze i stopka
	 * 
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function foot($id,$data)
	{
		$w=$this->width;
		$this->Ln(40);
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(90,90,90)));
		$this->SetFont('consola','',8);
		// komentarz
		$this->MultiCell($w,16,$data['coment'],"T","L",0,1);
		// dane dodatkowe
		if ($id)
		{
			$this->MultiCell($w,16,$this->extra->info,"T","L",0,1);
		}
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Podpisy
	 * 
	 * @param int $id
	 *        	idnetyfikator uzytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sign($id,$data)
	{
		$w=$this->width;
		$m=$this->margin;
		$this->Ln(25);
		// wstawienie podpisu
		if ($id)
		{
			if ($this->extra->sign and file_exists(ROOT_DIR.$this->extra->sign))
			{
				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->sign);
				if ($hi>55)
				{
					$wi=round($wi*55/$hi);
					$hi=55;
				}
				$wi=$wi<110?$wi:110;
				$this->Image(ROOT_DIR.$this->extra->sign,$w*0.725+$m-$wi/2,$this->GetY(),$wi,0,'','','RTL',true);
				$this->Ln($hi);
			}
			else
			{
				$this->Ln(25);
			}
		}
		
		$this->SetFont('consola','',8);
		$y=$this->GetY();
		$this->Line($w*0.1+$m,$y,$w*0.45+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$this->Line($w*0.55+$m,$y,$w*0.90+$m,$y,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,2"));
		$table="<table style=\"border: none\">";
		$table.="<tr style=\"border: none\">";
		$table.="<td width=\"10%;border: none\" align=\"right\"></td>";
		$table.="<td width=\"35%;border: none\" align=\"center\" colspan=\"3\">".$data['lang_client_sign']."</td>";
		$table.="<td width=\"10%;border: none\" align=\"right\"></td>";
		$table.="<td width=\"35%;border: none\" align=\"center\" colspan=\"3\">".$data['lang_user_sign']."</td>";
		$table.="<td width=\"10%;border: none\" align=\"right\"></td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"R");
	}
	/**
	 * Dane zapłaty
	 * 
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function cash($data)
	{
		$this->Ln(2);
		$this->SetFont('consola','',10);
		$sum=0;
		foreach ($this->sum as $i)
		{
			$sum+=$i[2];
		}
		$table="<table border=\"0\" width=\"".($this->width*1)."\">";
		// zapłacono
		if (number_format($data['cash_pay'],2)>0)
		{
			$table.="<tr>";
			$table.="<td width=\"19%\" align=\"right\"><b><u>".$data['lang_cash_pay']."</u></b></td>";
			$table.="<td width=\"1%\"></td>";
			$table.="<td width=\"80%\" align=\"left\" colspan=\"4\"><b><u>".number_format($data['cash_pay'],2,",",".")." ".$data['current']."</u></b></td>";
			$table.="</tr>";
			$sum-=$data['cash_pay'];
		}
		// do zapłaty
		$this->SetFont('consola','',12);
		$table.="<tr>";
		$table.="<td width=\"19%\" align=\"right\"><b><u>".$data['lang_cash_sum']."</u></b></td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"80%\" align=\"left\" colspan=\"4\"><b><u>".number_format($sum,2,",",".")." ".$data['current']."</u></b></td>";
		$table.="</tr>";
		// dodanie tabeli
		$table.="</table>";
		// $this->Cell($this->width*0.3,0,"",0,0);
		$this->writeHTML($table,1,0,0,0,"R");
		// słownie
		// $this->Ln(1);
		$this->SetFont('consola','',8);
		$this->SetY($this->GetY()-10);
		$table="<table border=\"0\">";
		$table.="<tr>";
		$table.="<td width=\"15%\" align=\"right\">".$data['lang_cash_word']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"84%\" align=\"left\" colspan=\"3\">".$data['cash_word']."</td>";
		$table.="</tr>";
		$table.="</table>";
		$this->writeHTML($table,1,0,0,0,"L");
		$this->Ln(2);
		// druga tabela
		$this->SetFont('consola','',8);
		$table="<table border=\"0\" width=\"".($this->width*0.7)."\">";
		// sposób i termin zapłaty
		$table.="<tr>";
		$table.="<td width=\"19%\" align=\"right\">".$data['lang_cash_type']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"30%\" align=\"left\">".$data['cash_type']."</td>";
		$table.="<td width=\"19%\" align=\"right\">".$data['lang_cash_date']."</td>";
		$table.="<td width=\"1%\"></td>";
		$table.="<td width=\"30%\" align=\"left\">".($data['cash_date']=="list"?$data['cash_date_list']:date("Y-m-d",strtotime($data['cash_date_date'])))."</td>";
		$table.="</tr>";
		// dodanie tabeli
		$table.="</table>";
		$this->Cell($this->width*0.3,0,"",0,0);
		$this->writeHTML($table,1,0,0,0,"R");
	}
	/**
	 * Wyświetlenie podliczenia
	 * 
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function sum($data)
	{
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(90,90,90)));
		// ustawienie parametrów
		$this->Ln(2);
		$this->SetFont('consola','',8);
		// zmienne
		$netto=0;
		$vat=0;
		$brutto=0;
		// tabela
		$table="<table border=\"1\" width=\"".($this->width*0.7)."\">";
		// nagłówki
		$table.="<tr height=\"18px\">";
		$table.="<td height=\"18px\" width=\"16%\" align=\"center\"><b>".$data['lang_all_rate']."</b></td>";
		$table.="<td height=\"18px\" width=\"28%\" align=\"center\"><b>".$data['lang_all_netto']."</b></td>";
		$table.="<td height=\"18px\" width=\"28%\" align=\"center\"><b>".$data['lang_all_vat']."</b></td>";
		$table.="<td height=\"18px\" width=\"28%\" align=\"center\"><b>".$data['lang_all_brutto']."</b></td>";
		$table.="</tr>";
		// kwoty
		foreach ($this->sum as $k=>$i)
		{
			if ($i[2])
			{
				$table.="<tr>";
				$table.="<td height=\"11px\" width=\"16%\" align=\"right\"><b>".$k.(is_numeric($k)?"%":"")."</b> </td>";
				$table.="<td height=\"11px\" width=\"28%\" align=\"right\">".number_format($i[0],2,",",".")." ".$data['current']."</td>";
				$table.="<td height=\"11px\" width=\"28%\" align=\"right\">".number_format($i[1],2,",",".")." ".$data['current']."</td>";
				$table.="<td height=\"11px\" width=\"28%\" align=\"right\">".number_format($i[2],2,",",".")." ".$data['current']."</td>";
				$table.="</tr>";
				$netto+=$i[0];
				$vat+=$i[1];
				$brutto+=$i[2];
			}
		}
		// podsumowanie
		$table.="<tr>";
		$table.="<td height=\"11px\" width=\"16%\" align=\"right\"><b>".$data['lang_all_sum']."</b></td>";
		$table.="<td height=\"11px\" width=\"28%\" align=\"right\"><b>".number_format($netto,2,",",".")." ".$data['current']."</b></td>";
		$table.="<td height=\"11px\" width=\"28%\" align=\"right\"><b>".number_format($vat,2,",",".")." ".$data['current']."</b></td>";
		$table.="<td height=\"11px\" width=\"28%\" align=\"right\"><b>".number_format($brutto,2,",",".")." ".$data['current']."</b></td>";
		$table.="</tr>";
		// dodanie tabeli
		$table.="</table>";
		$this->Cell($this->width*0.3,5,"",0,0);
		$this->writeHTML($table,1,0,0,0,"R");
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Dodanie produktów
	 * 
	 * @param int $count
	 *        	identyfikator ostatniego produktu
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function products($count,$data)
	{
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(90,90,90)));
		// ustawienie parametrów
		$this->Ln(7);
		$this->SetFont('consola','',7);
		// utworzenie zmiennych
		$vat=array(0,3,5,6,7,8,18,19,22,23);
		$sum=array();
		foreach ($vat as $i)
		{
			$sum[$i]=array(0,0,0);
		}
		// tabela
		$table="<table border=\"1\">";
		// nagłówki
		$table.="<tr height=\"18px\">";
		$table.="<td height=\"18px\" width=\"3%\" align=\"center\"><b>".$data['lang_lp']."</b></td>";
		$table.="<td height=\"18px\" width=\"23%\" align=\"center\"><b>".$data['lang_name']."</b></td>";
		$table.="<td height=\"18px\" width=\"8%\" align=\"center\"><b>".$data['lang_pkwiu']."</b></td>";
		$table.="<td height=\"18px\" width=\"5%\" align=\"center\"><b>".$data['lang_amount']."</b></td>";
		$table.="<td height=\"18px\" width=\"5%\" align=\"center\"><b>".$data['lang_unit']."</b></td>";
		$table.="<td height=\"18px\" width=\"8%\" align=\"center\"><b>".$data['lang_netto']."</b></td>";
		$table.="<td height=\"18px\" width=\"6%\" align=\"center\"><b>".$data['lang_rabat']."</b></td>";
		$table.="<td height=\"18px\" width=\"8%\" align=\"center\"><b>".$data['lang_prize_netto']."</b></td>";
		$table.="<td height=\"18px\" width=\"4%\" align=\"center\"><b>".$data['lang_vat']."</b></td>";
		$table.="<td height=\"18px\" width=\"10%\" align=\"center\"><b>".$data['lang_sum_netto']."</b></td>";
		$table.="<td height=\"18px\" width=\"10%\" align=\"center\"><b>".$data['lang_sum_vat']."</b></td>";
		$table.="<td height=\"18px\" width=\"10%\" align=\"center\"><b>".$data['lang_sum_brutto']."</b></td>";
		$table.="</tr>";
		// produkty
		for ($a=1;$a<=$count;$a++)
		{
			if ($data['p_'.$a.'_name'])
			{
				$cnp=str_replace(",",".",$data['p_'.$a.'_netto']);
				$cnp=$cnp==round($cnp,2)?number_format($cnp,2,".",""):($cnp==round($cnp,3)?number_format($cnp,3,".",""):$cnp);
				$amo=str_replace(",",".",$data['p_'.$a.'_amount']);
				$amo=$amo==round($amo,2)?number_format($amo,2,".",""):($amo==round($amo,3)?number_format($amo,3,".",""):$amo);
				$cnr=$data['p_'.$a.'_pnetto'];
				$cnr=$cnr==round($cnr,2)?number_format($cnr,2,".",""):($cnr==round($cnr,3)?number_format($cnr,3,".",""):$cnr);
				
				$table.="<tr>";
				$table.="<td height=\"11px\" width=\"3%\" align=\"right\">".$data['p_'.$a.'_lp']."</td>";
				$table.="<td height=\"11px\" width=\"23%\" align=\"left\">".nl2br($data['p_'.$a.'_name'])."</td>";
				$table.="<td height=\"11px\" width=\"8%\" align=\"left\">".$data['p_'.$a.'_pkwiu']."</td>";
				$table.="<td height=\"11px\" width=\"5%\" align=\"right\">".$amo."</td>";
				$table.="<td height=\"11px\" width=\"5%\" align=\"center\">".$data['p_'.$a.'_unit']."</td>";
				$table.="<td height=\"11px\" width=\"8%\" align=\"right\">".$cnp."</td>";
				$table.="<td height=\"11px\" width=\"6%\" align=\"right\">".$data['p_'.$a.'_rabat']."%</td>";
				$table.="<td height=\"11px\" width=\"8%\" align=\"right\">".$cnr."</td>";
				$table.="<td height=\"11px\" width=\"4%\" align=\"center\">".$data['p_'.$a.'_vat'].(is_numeric($data['p_'.$a.'_vat'])?"%":"")."</td>";
				$table.="<td height=\"11px\" width=\"10%\" align=\"right\">".$data['p_'.$a.'_snetto']."</td>";
				$table.="<td height=\"11px\" width=\"10%\" align=\"right\">".$data['p_'.$a.'_svat']."</td>";
				$table.="<td height=\"11px\" width=\"10%\" align=\"right\">".$data['p_'.$a.'_sbrutto']."</td>";
				$table.="</tr>";
				// podliczenie
				$v=$data['p_'.$a.'_vat'];
				$sum[$v][0]+=$data['p_'.$a.'_snetto'];
				$sum[$v][1]+=$data['p_'.$a.'_svat'];
				$sum[$v][2]+=$data['p_'.$a.'_sbrutto'];
				if ($a%50==0 and $a>0)
				{
					$table.="</table>";
					$this->SetY($this->GetY()-8);
					$this->writeHTML($table,1,0,0,0,"C");
					$table="<table border=\"1\">";
				}
			}
		}
		// dodanie tabeli
		$table.="</table>";
		$this->SetY($this->GetY()-8);
		$this->writeHTML($table,1,0,0,0,"C");
		$this->sum=$sum;
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie danych użytkownika i kontrahenta
	 * 
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function user_client($data)
	{
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(90,90,90)));
		// odczyt parametrów
		$w=$this->width;
		$m=$this->margin;
		$this->SetFont('consola','B',9);
		// linia górna
		$this->Cell($w/2,1,$data['lang_user_name_main'],"B",0,"L");
		$this->Cell($w/2,1,$data['lang_client_name_main'],"B",1,"L");
		// dane
		$this->SetFont('consola','',9);
		$table="<table style=\"border: none\" style='background-color: rgb(255, 0, 0);'>";
		$tab=array("name","address","nip","phone","mail","www","bank_name","account");
		foreach ($tab as $k=>$i)
		{
			if ($data['user_'.$i] or $data['client_'.$i])
			{
				$table.="<tr style=\"border: none\">";
				if ($data['user_'.$i])
				{
					$table.="<td width=\"12%;border: none\" align=\"right\" >".($k==100?"<b>":"").$data['lang_user_'.$i].($k==100?"</b>":"")."</td>";
					$table.="<td width=\"1%;border: none\" align=\"left\"></td>";
					$table.="<td width=\"37%;border: none\" align=\"left\">".nl2br($data['user_'.$i])."</td>";
				}
				else
				{
					$table.="<td width=\"50%;border: none\" volspan=\"3\"></td>";
				}
				if ($data['client_'.$i])
				{
					$table.="<td width=\"12%;border: none\" align=\"right\" >".($k==100?"<b>":"").$data['lang_client_'.$i].($k==100?"</b>":"")."</td>";
					$table.="<td width=\"1%;border: none\" align=\"left\"></td>";
					$table.="<td width=\"37%;border: none\" align=\"left\"><b>".nl2br($data['client_'.$i])."</b></td>";
				}
				else
				{
					$table.="<td width=\"50%;border: none\" volspan=\"3\"></td>";
				}
				$table.="</tr>";
			}
		}
		$table.="</table>";
		$y1=$this->GetY();
		$this->writeHTML($table,1,0,0,0,"C");
		$y2=$this->GetY();
		// linie boczne i środkowa
		$this->line($m,$y1,$m,$y2);
		$this->line($m+$w/2,$y1,$m+$w/2,$y2);
		$this->line($m+$w,$y1,$m+$w,$y2);
		// linia dolna
		$this->Cell($w,1,"","T",1,"L");
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie adresu dostawy
	 *
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function branch($data)
	{
		if (!$data['branch_v'])
		{return false;}
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(255,0,0)));
		// odczyt parametrów
		$w=$this->width;
		$m=$this->margin;
		$this->SetFont('consola','B',9);
		// linia górna
		$this->Cell($w/2,1,"","",0,"L");
		$this->Cell($w/2,1,$data['lang_branch'],"B",1,"L");
		$this->SetFont('consola','',9);
		// dane
		$table="<table style=\"border: none\">";
		// $table.="<tr><td width=\"50%\" align=\"left\"></td><td colspan=\"3\"
		// align=\"left\"><b>".$data['lang_branch']."</b></td></tr>";
		$tab=array("name","address","phone","mail","krs","bank_name","account");
		foreach ($tab as $k=>$i)
		{
			if ($data['branch_'.$i])
			{
				$table.="<tr style=\"border: none\">";
				$table.="<td width=\"50%;border: none\" align=\"left\"></td>";
				$table.="<td width=\"12%;border: none\" align=\"right\">".($k==0?"<b>":"").$data['lang_branch_'.$i].($k==0?"</b>":"")."</td>";
				$table.="<td width=\"1%;border: none\" align=\"left\"></td>";
				$table.="<td width=\"37%;border: none\" align=\"left\"><b>".nl2br($data['branch_'.$i])."</b></td>";
				$table.="</tr>";
			}
		}
		$table.="</table>";
		$y1=$this->GetY();
		$this->writeHTML($table,1,0,0,0,"C");
		$y2=$this->GetY();
		// linie boczne i środkowa
		// $this->line($m,$y1,$m,$y2);
		$this->line($m+$w/2,$y1,$m+$w/2,$y2);
		$this->line($m+$w,$y1,$m+$w,$y2);
		// linia dolna
		$this->Cell($w/2,1,"","",0,"L");
		$this->Cell($w/2,1,"","T",1,"L");
		// ustalenie kolorów linii
		$this->SetLineStyle(array("color"=>array(0,0,0)));
	}
	/**
	 * Utworzenie miejsca i daty wystawienia oraz daty sprzedaży
	 * 
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function city_date($data)
	{
		$w=$this->width;
		$this->Ln(18);
		$this->SetFont('consola','',10);
		// miejsce wystawienia
		$this->Cell($w*0.30,16,$data['lang_place'],0,0,"R");
		$this->Cell($w*0.25,16,$data['place'],0,0,"L");
		// data wystawienia
		$this->Cell($w*0.30,16,$data['lang_date_create'],0,0,"R");
		$this->Cell($w*0.25,16,date("Y-m-d",strtotime($data['date_create'])),0,1,"L");
		// puste
		$this->Cell($w*0.50,16,"",0,0,"R");
		// data sprzedaży
		$this->Cell($w*0.30,16,$data['lang_date_sell'],0,0,"R");
		$this->Cell($w*0.25,16,date("Y-m-d",strtotime($data['date_sell'])),0,1,"L");
	}
	/**
	 * Utworzenie nagłówka
	 * 
	 * @param int $id
	 *        	indtyfikator użytkownika
	 * @param array $data
	 *        	dane do wstawienia
	 */
	private function head($id,$data)
	{
		$w=$this->width;
		$m=$this->margin;
		// dane dodatkowe
		if ($id)
		{
			if ($this->extra->logo and file_exists(ROOT_DIR.$this->extra->logo))
			{
				$this->MultiCell($w,16,$this->extra->head,"","L",0,1);
				list($wi,$hi)=getimagesize(ROOT_DIR.$this->extra->logo);
				if ($hi>65)
				{
					$wi=round($wi*65/$hi);
					$hi=65;
				}
				$this->Image(ROOT_DIR.$this->extra->logo,$this->margin,$this->GetY(),($wi<150?$wi:150),0,'','','RTL',true);
			}
			else
			{
				$this->MultiCell($w,16,$this->extra->head,"","L",0,1);
			}
		}
		// nagłówek
		$this->SetFont('consola','B',20);
		$this->Cell($w,30,$data['lang_head'],0,1,"C");
		// numer
		$this->SetFont('consola','',14);
		$this->Cell($w/2,16,$data['lang_number'],0,0,"R");
		$this->Cell($w/2,16,$data['number'],0,1,"L");
		// oryginał/kopia
		$this->SetFont('consola','',10);
		$y=$this->GetY();
		$this->SetLineStyle(array("color"=>array(0,0,0)));
		if ($data['original_v']==3)
		{
			$this->Cell($w*0.30,16,$data['lang_original'],0,0,"C");
			$this->Cell($w*0.05,16,"/",0,0,'C');
			$this->Cell($w*0.30,16,$data['lang_copy'],0,0,"C");
			$this->Cell($w*0.05,16,"/",0,0,'C');
			$this->Cell($w*0.30,16,$data['lang_double'],0,1,"C");
			if ($data['original_v']!=1)
			{
				$this->Line($w*0.05+$m,$y+8,$w*0.25+$m,$y+8);
			}
			if ($data['original_v']!=2)
			{
				$this->Line($w*0.40+$m,$y+8,$w*0.60+$m,$y+8);
			}
			if ($data['original_v']!=3)
			{
				$this->Line($w*0.75+$m,$y+8,$w*0.95+$m,$y+8);
			}
		}
		else
		{
			$this->Cell($w*0.48,16,$data['lang_original'],0,0,"R");
			$this->Cell($w*0.04,16,"/",0,0,'C');
			$this->Cell($w*0.48,16,$data['lang_copy'],0,0,"L");
			if ($data['original_v']!=1&&$data['original_v']!=0)
			{
				$this->Line($w*0.39+$m,$y+8,$w*0.49+$m,$y+8,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,0"));
			}
			if ($data['original_v']!=2&&$data['original_v']!=0)
			{
				$this->Line($w*0.51+$m,$y+8,$w*0.61+$m,$y+8,array("width"=>1,"cap"=>"round","join"=>"round","dash"=>"3,0"));
			}
		}
	}
	/**
	 * Sprawdzenie katalogu do zapisu i ewentualne stworzenie go
	 * 
	 * @param int $id
	 *        	identyfikator użytkownika
	 * @param int $client
	 *        	identyfikator klienta
	 * @return string ścieżka do katalogu
	 */
	private function check_dir($id,$client=0)
	{
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/".$client."/";
		if (!is_dir($dir))
		{
			mkdir($dir,0777,true);
		}
		if (!is_writable($dir))
		{
			chmod($dir,0777);
		}
		return $dir;
	}
	/**
	 * Sprawdzenie nazwy plikui ewentualna zmiana
	 * 
	 * @param string $dir
	 *        	katalog zapisu
	 * @param string $name
	 *        	domyślna nazwa pliku
	 * @return string nazwa pliku
	 */
	private function check_file($dir,$name)
	{
		global $func;
		$name=$func->create_url($name);
		$name=$name?"rachunek-".$name:"rachunek";
		if (file_exists($dir.$name.".pdf"))
		{
			$count=0;
			while (file_exists($dir.$name."-".$count.".pdf"))
			{
				$count++;
			}
			$name=$name."-".$count;
		}
		return $name.".pdf";
	}
}
?>