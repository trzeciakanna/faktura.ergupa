<?php
global $smarty;
require(ROOT_DIR."includes/units.php");
require(ROOT_DIR."module/rachunek/".LANG.".php");
require(ROOT_DIR."module/rachunek/".LANG."_const.php");
global $lang;
global $langs;
global $units;
$sub=(USER_SUB>date("Y-m-d H:i:s") OR USER_RANG=="admin")?true:false;
$smarty->assign("units",$units);
$smarty->assign("user_id",USER_ID);
$smarty->assign("user_sub",$sub);


global $navi;

$smarty->assign("navi",$navi->navi);
//odczyt danych użytkownika
if($sub AND !$_GET['par2'])
{
	require(ROOT_DIR."module/panel/data_extra/class.php");
  require(ROOT_DIR."module/panel/data_number/class.php");
	require(ROOT_DIR."module/panel/invoice/class.php");
	$inv=new invoice();
	$user=new users_invoice(USER_ID);
	$nr=new user_number(USER_ID,5);
	$smarty->assign("counter",$nr->get_nr());
	$smarty->assign("user",$user);
	$smarty->assign("extra",new user_extra(USER_ID));
}
//odczyt danych zapisanej faktury
elseif($sub AND (int)$_GET['par2'])
{
  require(ROOT_DIR."module/panel/data_extra/class.php");
	require(ROOT_DIR."module/panel/invoice/class.php");
	$inv=new invoice();
	$data=$inv->read_one(USER_ID,$_GET['par2']);
	if($data)
	{
		if(file_exists(ROOT_DIR."files/invoice_lang/".$_GET['par2'].".php"))
		{ require(ROOT_DIR."files/invoice_lang/".$_GET['par2'].".php"); }
		$user=new invoice_data($_GET['par2'],"user");
		$client=new invoice_data($_GET['par2'],"client");
		$smarty->assign("inv",$data);
		$smarty->assign("user",$user);
		$smarty->assign("client",$client);
    $smarty->assign("extra",new user_extra(USER_ID));
	}
}
$smarty->assign("lang",$lang);
$smarty->assign("langs",$langs);


$smarty->display("rachunek/".LAYOUT.".html");
?>