<?php
global $smarty;
require(ROOT_DIR."module/probe/".LANG.".php");
require(ROOT_DIR."admin/module/probe/class.php");
global $lang;

$probe=new probe();
$smarty->assign("dane",$probe->read_rand());
$dane = $probe->read_one_with_vote();
$smarty->assign("dane_glosy",$dane['dane']);
$smarty->assign("liczba_glosow",$dane['glosy']);
$smarty->assign("lang",$lang);
$smarty->display("probe/".LAYOUT.".html");
?>
