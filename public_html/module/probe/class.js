probe_obj = {
		add : function(probe)
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="error")
				{ info($('probe_error').value); }
				else
				{
					var tmp=this.responseText.split("|||");
					var odp=tmp[0].split("||");
					var max=tmp[1];
					$('probe_answer').innerHTML="";
					for(var a=0; a<odp.length; a++)
					{
						tmp=odp[a].split("|");
						$('probe_answer').innerHTML+='<div class="row_auto"><strong>'+tmp[0]+': '+tmp[1]+'</strong></div>';
					}

				}
			};
			var tmp=document.getElementsByName("probe_"+probe);
			for(var a=0;a<tmp.length;a++)
			{ if(tmp[a].checked) { req.AddParam("answer",tmp[a].value); } }
			req.AddParam("probe",probe);
			req.Send("module/probe/ajax_add.php");
			return false;
		}
};