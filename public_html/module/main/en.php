<?php
$lang=array();
$lang['pomoc_nagl']="Help Center:";
$lang['pomoc1_nagl']="Online Guide";
$lang['pomoc2_nagl']="Talk to a Consultant";
$lang['pomoc3_nagl']="Contact Form - Ask Us a Question";
$lang['pomoc1_tresc']="Contains a detailed description of the application functions and the solutions to the most common problems";
$lang['pomoc2_tresc']="	If you need help - call!<br><span class='bold textOrange'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="	You can also ask us a question by filling in our Contact Form";
$lang['wiecej']= "more";
$lang['do_pomocy'] = "Go to Help";
$lang['o_programie_nagl'] = "What is the online invoicing application?";
$lang['o_programie'] = "The online invoicing application allows you to create your own VAT invoice, proforma invoice or other, in a quick, simple and intuitive way, without having to register.<br> It has a lot of editing options for all the heading fields.<br> It is possible to save the created invoice in PDF format, or to print the invoice right after it is created.<br> Additionally, the service offers automatic calculation of net and gross values, with different VAT rates.<br>
Upon registration every member gains access to all options, such as: making lists of customers, adding products, automatic saving of your data, putting your logo onto your invoices, an archive of stored invoices and many more useful options...";
$lang['zadaj_pytanie'] =  'Ask a question about the invoicing application';
$lang['zalety'] = '	<li>customers</li>
   <li>products</li>
  <li>invoice list</li>
   <li>own logo</li>
   <li>product categories</li>
   <li>templates and examples</li>
   <li>cyclical invoices</li>
   <li>delivery address</li>
   <li>awarding discounts</li>
  <li>method of payment</li>
   <li>send by e-mail</li>
  <li>paid invoices</li>
   <li>reports</li>
   <li>currency selection</li>
   <li>data import/export</li>';
   $lang['nagl1'] = 'Try out free without registration!';
   $lang['nagl2'] = 'Check out our program and issue invoices for free!';
   $lang['nagl3'] = 'To use all the functions';
   $lang['nagl4'] = 'Register';
   $lang['nagl5'] = '<span class="greyText textStandard">News</span>';

?>
