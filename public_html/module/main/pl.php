<?php
$lang=array();
$lang['pomoc_nagl']="Pomoc <span class='textStandard lightGreyText'>faktura.egrupa.pl</span>";
$lang['pomoc1_nagl']="Funkcje programu fakturowania online";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Problem z fakturami - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstszych problemów";
$lang['pomoc2_tresc']="	Jeśli potrzebujesz pomocy zadzwoń!<br><span class='bold textOrange'>+48 504 667 000</span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "czytaj więcej...";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "O programie do faktur";
$lang['o_programie_nagl2'] = "Co to jest program do faktur online?";
$lang['o_programie'] = "Program do wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki i łatwy sposób utworzyć własną <strong>fakturę vat</strong>, <strong>proformę</strong>, <strong>fakturę zaliczkową</strong>, <strong>korygującą</strong> lub <strong>rachunek</strong>.<br> Dzięki gotowym wzorom online do wypełnienia, każdy dokument możemy wystawić w kilka sekund co oszczędza nas czas i przyspiesza płatności. Dodatkowo program fakturowy posiada szerokie możliwości edycyjne wszystkich pól.<br> Możliwość zapisania wystawionej faktury w formacie PDF i wysłania go do kontrahenta na maila lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy otrzymuje pełny dostęp do funkcji programu, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, wybór numeracji faktur, wybór szablonów faktur w PDF, możliwość dodania kilku kont bankowych, pieczątkę graficzną i wiele wiele innych przydatnych funkcji...";
$lang['zadaj_pytanie'] =  'Zadaj pytanie odnośnie programu do wystawiania faktur';
$lang['zalety'] = '	<li>kontrahenci</li>
   <li>produkty</li>
  <li>lista faktur</li>
   <li>własne logo</li>
   <li>kategorie produktów</li>
   <li>Szablony i wzory</li>
   <li>faktury okresowe</li>
   <li>adres dostawy</li>
   <li>nadawanie rabatów</li>
  <li>wybór płatności</li>
   <li>wysyłka mailem</li>
  <li>faktury zapłacone</li>
   <li>raporty</li>
   <li>wybór waluty</li>
   <li>import/export danych</li>';
   $lang['nagl1'] = 'Wystaw fakturę bez rejestracji!';
   $lang['nagl2'] = 'Sprawdź nasz program i <h3 style="display:inline;color:#6C6C6C;font-size:14px;border:0">wystawiaj faktury za darmo</h3>!';
   $lang['nagl3'] = 'Aby sprawdzić wszystkie funkcje';
   $lang['nagl4'] = 'Zarejestruj się';
   $lang['nagl5'] = '<span class="greyText textStandard">Aktualności</span> z faktura.egrupa';

?>
