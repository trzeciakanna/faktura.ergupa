<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/panel/client_add/class.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$client=new clients();
//wywołanie funkcji odczytującej
if($_GET['name'] || $_GET['address'] || $_GET['nip'])
{
	require(ROOT_DIR."includes/search.php");
	$search=new search();
	list($dane,$page,$last)=$client->read_list_search($_GET['user_id'],$_GET['page'],10,$_GET['name'],$_GET['address'],$_GET['nip']);
}
else
{ list($dane,$page,$last)=$client->read_list($_GET['user_id'],$_GET['page'],10,$_GET['letter']); }
echo implode(",",$client->letter_list($_GET['user_id']));
echo "..|-|.|-|..";
echo $last;
echo "..|-|.|-|..";
for($a=0,$max=count($dane);$a<$max;$a++)
{
	//odczyt oddziałów
	$tmp=$client->branch_list($dane[$a]->id);
	echo ($a==0?"":"|-|.|-|"); echo $dane[$a]->id.",".$dane[$a]->sign.",".strip_tags($dane[$a]->name);
	for($b=0,$maxb=count($tmp);$b<$maxb;$b++)
	{ echo "|-|.|-|".$dane[$a]->id.".".$tmp[$b]->id.",+ ".$tmp[$b]->sign.",".strip_tags($tmp[$b]->name." (".$dane[$a]->sign.")"); }
}
?>