<?php
$lang=array();
$lang['text']='<h2>30 days for free!</h2>
This promotion allows to get familiar with our Online Invoicing Application.<br>
Completely for free you can test, check and try out all the option we have created, so that your invoicing is no longer a mundane job.<br>
We have created for our Users the following option of running a company efficiently: selling and buying goods, monitoring the payments for issued invoices, sending the invoices to Customers or Accountancy, simple edition, chronological and automated numbering and many others.<br>
To try out all the functions, you only need to register. Nothing more.<br>
<br>
Use it! It is free! It is all from us to You...<br>
<br>
<br>
<h2>The longer, the cheaper !</h2>

After a 30-day free testing trial and learning how to use our Online Invoicing Application, you can extend your Subscription.
You can buy your Subscription for 1 to 12 months.<br>
We have followed the current trends in saving money.<br>
Our offer is very attractive: the longer your Subscription is, the LESS you will pay!<br>
Every month you gain 2,5 %<br>
<br>
Remember<br>
Our invoices are limitless, with free upgrades and the longer you are with us, the cheaper it gets!<br>';


?>