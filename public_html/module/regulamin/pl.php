<?php
$lang=array();
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik on-line";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstszych problemów";
$lang['pomoc2_tresc']="	Jeśli potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000 </span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "Program do wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, bądź inną<br> Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych.<br> Możliwość zapisania stworzonej faktury w formacie PDF lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, i wiele wiele innych przydatnych opcji...";
$lang['regulamin'] = ' <h1 style="text-align: left;">REGULAMIN SERWISU FAKTURA.EGRUPA.PL</h1>

<p style="text-align: left; ">
<br>
   

<b>I POSTANOWIENIA OGÓLNE</b><br>
<br>
1.1.	Niniejszy regulamin serwisu faktura.egrupa.pl (dalej zwany „Regulaminem”), określa zasady korzystania przez Klientów (dalej zwanymi „Użytkownikami”) z tegoż serwisu, a także warunki świadczenia usług za pomocą tego serwisu.<br>

1.2.	Przez Użytkowników rozumie się korzystającą z Serwisu faktura.egrupa.pl osobę fizyczną, osobę prawną lub jednostkę organizacyjną, która nie posiada osobowości prawnej, utworzoną zgodnie z obowiązującymi przepisami.<br>

1.3.	Operatorem serwisu internetowego faktura.egrupa.pl jest :<br>
<br>
<br>

Avano Sp. z o.o.<br>
Cienista 16/4<br>
02-439 Warszawa<br>
KRS: 0000375198<br>
NIP: 7010277497<br>

zwana dalej Operatorem. <br>
<br>
1.4.	Aktualne dane Operatora oraz siedziba, a także dane kontaktowe umieszczone są na stronie: 
https://faktura.egrupa.pl/contact/<br>

1.5.	Dostęp do ogólnodostępnej części serwisu  posiadają wszyscy korzystający z sieci Internet zarówno poprzez komputer, jak i telefon komórkowy. Użytkownik powinien posiadać dostęp do komputera oraz Internetu a także posiadać zainstalowany  program Adobe Reader. Korzystanie ze strony internetowej www.faktura.egrupa.pl jest możliwe pod warunkiem korzystania przez Użytkownika z przeglądarki internetowej, jednak Serwis nie gwarantuje poprawnego działania przy korzystaniu z przeglądarki Internet Explorer. Do tego celu zalecamy przeglądarkę Firefoks.<br>

1.6.	Operator jest Administratorem Danych Osobowych Użytkowników serwisu faktura.egrupa.pl,  zgodnie z ustawą z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (Dz. U. z 2002 r., Nr 101, poz. 926 ze zmianami).<br>

1.7.	Użytkownik podaje swoje dane osobowe dobrowolnie w celu realizacji świadczenia.<br>

1.8.	Operator  przetwarza dane osobowe Użytkownika w celu realizacji usług (wystawianie faktur za usługę, pomoc techniczna). <br>

1.9.	Operator może udostępnić dane osobowe Użytkowników osobom trzecim (takim jak na przykład operatorzy płatności, księgowa, etc.), jeżeli jest to niezbędne do realizacji świadczonych przez Operatora usług.<br>

1.10.	Operator nie ma wglądu w żadne dane użytkownika, prócz danych firmowych o ile takowe użytkownik wprowadził do serwisu, tj. Nazwa Firmy, Login, Data rejestracji, Adres prowadzonej działalności, Nip, Telefon, Operator nie udostępnia żadnych danych firmowych osobom ani podmiotom trzecim. <br> 

1.11.	Operator na adres email przesyła jedynie informacje ściśle związane z serwisem. Nie przesyła żadnych informacji handlowych, reklamowych oraz marketingowych.<br>

1.12.	Użytkownikowi w każdej chwili przysługuje prawo uzupełnienia, uaktualnienia, zaniechania przetwarzania,  a nawet usunięcia danych osobowych. Pozostałe prawa i obowiązki Użytkownika w zakresie danych osobowych określa Ustawa z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (Dz. U. z 2002 r., Nr 101, poz. 926 ze zmianami).<br>
<br>
<br>
<b>II ZAKRES ŚWIADCZONYCH USŁUG</b>
<br>
2.1.	Operator na rzecz Użytkowników udostępnia poprzez serwis faktua.egrupa.pl aplikację internetową, dzięki której Użytkownik serwisu ma możliwość wystawiania faktur VAT, pro forma oraz innych dokumentów.  Aplikacja ta posiada wiele możliwości edycji pól nagłówkowych, a także wiele opcji dodatkowych, które szerzej opisane są na stronie:<br>
<br>
https://faktura.egrupa.pl/funkcje/ <br>
<br>

2.2.	<b>Operator oferuje dwie grupy produktów:</b><br>
<br>
2.3.	<u>Faktura darmowa.</u><br>
2.4.	Faktura darmowa pozwala Użytkownikowi na wystawianie faktur w każdej chwili i dowolnym czasie z ograniczeniem do 3 szt/24h.<br>
2.5.	Darmowe faktury mogą być zapisywane w formacie pdf na prywatnym komputerze.<br>
2.6.	Faktury darmowe nie mogą być zapisywane na serwerach.<br>
2.7.	Brak możliwości tworzenia listy faktur.<br>
2.8.	Brak możliwości zapisywania własnych danych, kontrahentów, produktów oraz kont.<br>
2.9.	Jeden prosty szablon darmowej faktury.<br>
<br>
2.10.	<u>Faktura w abonamencie.</u><br>
2.11.	Faktura w abonamencie daje możliwość  zapisywania faktury w formacie pdf oraz na zabezpieczonych serwerach on-line.<br>
2.12.	Zachowaną fakturę na serwerze można w każdej chwili edytować, a także zmieniać wybrane dane.<br>
2.13.	Brak limitów wystawianych faktur<br>
2.14.	Możliwość zapisania i edytowania danych firmy, a także kontrahenta.<br>
2.15.	Możliwość zapisania i edytowania produktu oraz jego ceny.<br>
2.16.	Możliwość utworzenia kategorii sprzedawanych produktów oraz usług.<br>
2.17.	Możliwość wysłania wiadomości do wszystkich jednocześnie lub wybranych kontrahentów.<br>
2.18.	Możliwość wystawiania faktury w języku polskim, angielskim oraz rosyjskim.<br>
2.19.	Możliwość zapisania kilku kont bankowych.<br>
2.20.	Możliwość eksportu oraz importu danych.<br>
2.21.	Wybór szablonu faktury.<br>
2.22.	Szczegółowe informacje na temat różnic pomiędzy w/w grupami produktów można odnaleźć na stronie:
https://faktura.egrupa.pl/funkcje/<br>

2.23.	Operator zastrzega możliwość wycofania oraz zmiany niektórych świadczonych usług.<br>
2.24.	Korzystanie przez Użytkownika z wszystkich funkcji serwisu faktura.egrupa.pl jest płatne zgodnie z cennikiem zamieszczonym na stronie internetowej Operatora:
https://faktura.egrupa.pl/cennik/<br>
<br>
<b>III Korzystanie z serwisu faktura.egrupa.pl</b><br>
<br>
3.1.  Dostęp do serwisu faktura.egrupa.pl obywa się jedynie  w formie on-line. <br>
3.2.  Jeden użytkownik może założyć tylko jedno konto na daną firmę w serwisie, każde kolejne konto założone na tą samą firmę, zostanie zbanowane (brak dostępu do konta) przez operatora.<br> 
3.2.  Aby korzystać z serwisu nie trzeba się rejestrować.<br>
3.3. Jednakże korzystanie ze wszystkich możliwości serwisu Operatora możliwe jest tylko i wyłącznie po akceptacji Regulaminu oraz rejestracji w serwisie.<br>
3.4. Podczas rejestracji Użytkownik zobowiązany jest do podania unikalnego, zaproponowanego przez siebie identyfikatora oraz hasła.<br>
3.5. Operator nie ma dostępu do haseł, danych oraz zapisywanych faktur, a także klientów.<br>
3.6. Operator posiada dostęp do loginu, ważności abonamentu, a także danych firmy, takich jak nazwa firmy, adres, NIP, email, numer telefonu.<br> 
3.7.  Od dnia zarejestrowania się, Użytkownik ma możliwość skorzystania z bezpłatnego 30 dniowego okresu próbnego.<br>
3.8. Operator nie zwraca należności za zapłacony abonament- wyjątkiem jest podwójna zapłata.<br>
<br>
<br>
<b>IV REKLAMACJE ORAZ ODSTĄPIENIE OD UMOWY</b><br>
<br>
4.1. Zgodnie z Ustawą o ochronie niektórych praw konsumentów oraz o odpowiedzialności za szkodę wyrządzoną przez produkt niebezpieczny z 2 marca 2000 (Dz. U. z 2000 r., Nr 22, poz. 271 ze zmianami), Użytkownik ma prawo do odstąpienia od umowy poprzez złożenie pisemnego oświadczenia, bez podawania przyczyn, w terminie 14 dni .<br>
4.2.   Umowa odstąpienia od umowy  pomiędzy Operatorem a Użytkownikiem może być:<br>
4.2.1. na wskutek pisemnego wypowiedzenia,<br>
4.2.2. porozumienia stron,<br>
4.2.3. śmierci Użytkownika.<br>
4.2.4. nie uiszczenia przez Użytkownika należności przysługujących Operatorowi w zamian za udostępnienie świadczonych usług.<br>
4.3.  Operator zastrzega możliwość natychmiastowego rozwiązania umowy z Użytkownikiem bez zachowania okresu wypowiedzenia w przypadku działania sprzecznego z niniejszym regulaminem, działania na szkodę Operatora, podejmowania czynności niezgodnych z prawem, a także w innych uzasadnionych przypadkach.<br>
<br>
<br>
<b>V ZAKRES ODPOWIEDZIALNOŚCI OPERATORA</b><br>
<br>
5.1. Operator zobowiązany jest do podjęcia wszystkich czynności, które zapewniają prawidłowe funkcjonowanie serwisu.<br>
5.2. Operator nie ponosi odpowiedzialności za szkody wynikające z użycia, niemożliwości użycia lub nieprawidłowego działania serwisu, chyba że szkody te wynikają z umyślnego działania Operatora.<br>
5.3. Operator nie ponosi odpowiedzialności za jakiekolwiek niezgodne z prawem czynności wykonywane przez Użytkownika.<br>
5.4. Odpowiedzialność za poprawne wystawienie faktury swojemu kontrahentowi oraz sprawdzenie prawidłowości danych na wystawianej fakturze leży wyłącznie po stronie Użytkownika.<br>
<br>
<b>VI POSTANOWIENIA KOŃCOWE</b><br>
<br>
6.1.	W sprawach nieuregulowanych niniejszym Regulaminem, zastosowanie mają przepisy Kodeksu Cywilnego, a ponadto także przepisy ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (Dz. U. z 2002 r., Nr 144, poz. 1204 ze zmianami), ustawy z dnia 2 marca 2000 r. o ochronie niektórych praw konsumentów oraz o odpowiedzialności za szkodę wyrządzoną przez produkt niebezpieczny (Dz. U. z 2000 r., Nr 22, poz. 271 ze zmianami), ustawy z dnia 27 lipca 2002 r. o szczególnych warunkach sprzedaży konsumenckiej oraz o zmianie Kodeksu cywilnego (Dz. U. z 2002 r., Nr 141, poz. 1176 ze zmianami), ustawie z dnia 29 sierpnia 1997 r. o ochronie danych osobowych (Dz. U. z 2002 r., Nr 133, poz. 926 ze zmianami).<br>

6.2.	 Za sąd właściwy do rozstrzygania sporów powstałych pomiędzy Operatorem, a Użytkownikiem, jest sąd właściwy dla miejsca wykonania umowy pomiędzy stronami.<br>

6.3.	Operator zastrzega sobie prawo do zmiany niniejszego Regulaminu. O każdorazowych zmianach w Regulaminie, Użytkownicy zarejestrowani na stronie www.faktura.egrupa.pl zostaną o tym fakcie poinformowani.<br>





</ul>';

?>