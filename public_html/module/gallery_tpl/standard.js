gallery_s = {
	timer : null,
	time : 500,
	step : 25,
		
	close : function()
	{
		$('gallery').style.display="none";
		return false;
	},
	
	change : function(obj,id)
	{
		var max_w=parseInt($('gallery_width').value)+parseInt($('gallery_width_'+id).value);
		var max_h=parseInt($('gallery_height').value)+parseInt($('gallery_height_'+id).value);
	
		//załadowanie grafiki
		$('gallery_tmp').style.opacity="0";
		$('gallery_tmp').src="";
		$('gallery_tmp').src=$('gallery_img').src;
		$('gallery_img').src="";
		$('gallery_tmp').style.width=$('gallery_img').style.width;
		$('gallery_tmp').style.height=$('gallery_img').style.height;
		$('gallery_tmp').style.opacity="100";
		$('gallery_img').style.opacity="0";
		$('gallery_img').src=obj.src;
		$('gallery_img').alt=obj.alt;
		$('gallery_img').title=obj.title;
		$('gallery_img').style.width=$('gallery_width_'+id).value+"px";
		$('gallery_img').style.height=$('gallery_height_'+id).value+"px";
		//przejście
		mint.fx.Style('gallery_tmp','opacity',100,0,this.step,this.time);
		mint.fx.Style('gallery_img','opacity',0,100,this.step,this.time);
		//nagłówki
		$('gallery_name').innerHTML=obj.alt;
		$('gallery_counter').innerHTML=(id+1)+"/"+$('gallery_max').value;
		$('gallery_count').value=id;
		//przemieszczenie + powiększenie
		size=window_size();
		mint.fx.Move('gallery',(size[0]-max_w)/2,(size[1]-max_h)/2,this.step,this.time);
		mint.fx.Size('gallery',max_w,max_h,this.step,this.time);
		mint.fx.Size('gallery_img_div',$('gallery_width_'+id).value,$('gallery_height_'+id).value,this.step,this.time);
		//mint.fx.Size('gallery_img',$('gallery_width_'+id).value,$('gallery_height_'+id).value,this.step,this.time);
		//mint.fx.Size('gallery_tmp',$('gallery_width_'+id).value,$('gallery_height_'+id).value,this.step,this.time);
		//mint.fx.Size('gallery_list',$('gallery_width_'+id).value,null,this.step,this.time);
	},
	
	open : function(obj,id)
	{
		var max_w=parseInt($('gallery_width').value)+parseInt($('gallery_width_'+id).value);
		var max_h=parseInt($('gallery_height').value)+parseInt($('gallery_height_'+id).value);
		//załadowanie grafiki
		$('gallery_img').src=obj.src;
		$('gallery_img').alt=obj.alt;
		$('gallery_img').title=obj.title;
		$('gallery_img').style.width=$('gallery_width_'+id).value+"px";
		$('gallery_img').style.height=$('gallery_height_'+id).value+"px";
		//nagłówki
		$('gallery_name').innerHTML=obj.alt;
		$('gallery_counter').innerHTML=(id+1)+"/"+$('gallery_max').value;
		$('gallery_count').value=id;
		//ustawienia początkowe
		size=window_size();
		$('gallery').style.left=(size[0]/2)+"px";
		$('gallery').style.top=(size[1]/2)+"px";
		//$('gallery').style.width="800px";
		//$('gallery').style.height="600px";
		
		$('gallery').style.width=$('gallery_width').value+"px";
		$('gallery').style.height=$('gallery_height').value+"px";
		
		$('gallery').style.display="block";
		//$('gallery_list').style.width="0px";
		$('gallery_img_div').style.width="0px";
		$('gallery_img_div').style.height="0px";
		//przemieszczenie + powiększenie
		mint.fx.Move('gallery',(size[0]-max_w)/2,(size[1]-max_h)/2,this.step,this.time);
		mint.fx.Size('gallery',max_w,max_h,this.step,this.time);
		mint.fx.Size('gallery_img_div',$('gallery_width_'+id).value,$('gallery_height_'+id).value,this.step,this.time);
		//mint.fx.Size('gallery_img',$('gallery_width_'+id).value,$('gallery_height_'+id).value,this.step,this.time);
		//mint.fx.Size('gallery_list',$('gallery_width_'+id).value,null,this.step,this.time);
	},
	
	prev : function()
	{
		var ac=parseInt($('gallery_count').value);
		var max=parseInt($('gallery_max').value);
		if(ac==0) { ac=max-1; } else { ac--; }
		this.change($('gallery_img_'+ac),ac);
	},
	
	next : function()
	{
		var ac=parseInt($('gallery_count').value);
		var max=parseInt($('gallery_max').value);
		if(ac+1==max) { ac=0; } else { ac++; }
		this.change($('gallery_img_'+ac),ac);
	},
	
	left : function()
	{
		//$('gallery_list').scrollLeft-=2;
		this.timer=setTimeout("gallery_s.left()",30);
	},
	
	right : function()
	{
		//$('gallery_list').scrollLeft+=2;
		this.timer=setTimeout("gallery_s.right()",30);
	},
	
	stop : function()
	{
		clearTimeout(this.timer);
	}

};