<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/uploader/class.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$uploader=new uploader();
//odczyt pliku
$file=fopen(ROOT_DIR."module/uploader/standard_items.html","rt");
$txt="";
while(!feof($file))
{ $txt.=fgets($file,1024); }
fclose($file);
$txt=str_replace("\n","",$txt);
//odczyt boxu
$tmp=preg_match("/{\*begin box\*}(.*){\*end box\*}/",$txt,$match);
$box=$match[1];
//odczyt elementu
if($_GET['what']=="img")
{ $tmp=preg_match("/{\*begin img\*}(.*){\*end img\*}/",$txt,$match); }
elseif($_GET['what']=="file")
{ $tmp=preg_match("/{\*begin file\*}(.*){\*end file\*}/",$txt,$match); }
elseif($_GET['what']=="flash")
{ $tmp=preg_match("/{\*begin flash\*}(.*){\*end flash\*}/",$txt,$match); }
$item=$match[1];
$items="";
//odczyt plików i stworzenie listy
$files=$uploader->files($_GET['type'],$_GET['user_id']);
foreach($files as $f)
{ $items.="\n".str_replace(array("[value]","[name]","[size]","[date]","[width]","[height]"),array($_GET['path'].$f->path,$f->name,$f->size,$f->date,$f->width,$f->height),$item); }
$box=str_replace("[items]",$items,$box);
echo str_replace(array("[count]","[type]"),array($_GET['count'],$_GET['what']),$box);
?>