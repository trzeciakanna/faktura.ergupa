<?php
$langs=array();
$langs['del']="Usuń plik";
$langs['more']="Dostępne pliki";
$langs['download']="Pobierz plik";
$langs['close']="Zamknij";
$langs['max_size']="Maksymalny rozmiar grafiki to [width]px * [height]px.";
$langs['max_weight']="Maksymalna waga pliku to [weight]kB.";
$langs['bad_ext']="Załadowałeś plik o złym rozszeżeniu.";
$langs['bad_size']="Załadowałeś plik o zbyt dużym rozmiarze. Maksymalny rozmiar to [width]px * [height]px.";
$langs['bad_weight']="Załadowałeś plik o zbyt dużej wadze. Maksymalna waga to [weight]kB.";
$langs['error']="Wystąpił problem z wysyłaniem pliku, proszę odświerzyć stronę (CTRL+F5) i spróbować ponownie.";
?>