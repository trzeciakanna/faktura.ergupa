<?php
/**
 * Klasa obsługująca upload plików
 * @author kordan
 *
 */
class uploader
{
	/**
	 * Wybranie odpowiedzniego katalogu
	 * @param string $what co jest uploadowane
	 * @param int $user_id identyfikator użytkownika
	 * @return string ścieżka do katalogu
	 */
	private function select_dir($what, $user_id=null)
	{
		switch($what)
		{
			case "invoice_logo":
			case "invoice_sign":
				$dir=ROOT_DIR."files/invoice_logo/".$user_id."/";
				break;
			case "news":
				$dir=ROOT_DIR."images/news/";
				break;
			
			
			default :
				$dir=ROOT_DRI."files/other/";
				break;
		}
		if(!is_dir($dir)) { mkdir($dir,0777,true); }
		if(!is_writable($dir)) { chmod($dir,0777); }
		return $dir;
	}
	/**
	 * Wybranie odpowiedniej wagi pliku
	 * @param string $what co jest uploadowane
	 * @return int maksymalna waga pliku
	 */
	public function select_weight($what)
	{
		switch($what)
		{
			case "invoice_logo":
			case "invoice_sign":
			case "news":	
				$weight=1024;
				break;
			default :
				$weight=128;
				break;
		}
		return $weight;
	}
	/**
	 * Wybranie odpowiedzniego rozmiaru pliku
	 * @param string $what co jest uploadowane
	 * @return array maksymalny rozmiar grafiki
	 */
	public function select_max_size($what)
	{
		$size=array();
		switch($what)
		{
			case "invoice_logo":  
				$size['width']=650;
				$size['height']=600;
				break;
			case "news":	
				$size['width']=1024;
				$size['height']=768;
				break;
			case "invoice_sign":	
				$size['width']=225;
				$size['height']=110;
				break;
			default :
				$size['width']=50;
				$size['height']=50;
				break;
		}
		$size[0]=$size['width'];
		$size[1]=$size['height'];
		return $size;
		
	}
	/**
	 * Upload pliku
	 * @param array $file dane uploadowanego pliku
	 * @param string $what co jest uploadowane
	 * @param string $type rodzaj uploadowanego pliku
	 * @param int $user_id identyfikator użytkownika
	 * @return string status uploadu
	 */
	public function upload($file, $what, $type, $user_id)
	{
		global $func;
		$dir=$this->select_dir($what, $user_id);
		$weight=$this->select_weight($what);
		$msg="ok";
		//sprawdzenie wagi
		if($file['size']/1024<=$weight)
		{
			//sprawdzenie rozmiaru jeżeli grafikia
			if($type=="img")
			{
				$size=$this->select_max_size($what);
				list($w,$h)=getimagesize($file['tmp_name']);
				if($w>$size['width'] || $h>$size['height'])
				{ return "error|size|".$size['width']."|".$size['height']; }
			}
			//utworzenie nazwy pliku
			$tmp=explode(".",$file['name']);
			$ext=strtolower($tmp[sizeof($tmp)-1]);
			$tmp[sizeof($tmp)-1]="";
			$tmp=$func->create_url(implode("-",$tmp));
			if(file_exists($dir.$tmp.".".$ext))
			{
				$count=0;
				while(file_exists($dir.$tmp."-".$count.".".$ext)) { $count++; }
				$tmp=$tmp."-".$count;
			}
			if(!move_uploaded_file($file['tmp_name'],$dir.$tmp.".".$ext))
			{ $msg="error|upload|"; }
			else
			{
				chmod($dir.$tmp.".".$ext,0777);
				$msg="ok|".str_replace(ROOT_DIR,"",$dir).$tmp.".".$ext;
			}
		}
		else { $msg="error|weight|".$weight; }
		return $msg;
	}
	/**
	 * Odczyt plików z katalogu
	 * @param string $what co jest uploadowane
	 * @param int $user_id identyfikator użytkownika
	 * @return array tablica z obiektami plików
	 */
	public function files($what, $user_id)
	{
		$dir=$this->select_dir($what, $user_id);
		$tmp=array();
		$kat=opendir($dir);
		while($plik=readdir($kat))
		{
			$file=new one_file($dir.$plik);
			if($file->name) { $tmp[]=$file; }
		}
		return $tmp;
	}
}
/**
 * Klasa obsługująca parametry pliku
 * @author kordan
 *
 */
class one_file
{
	public $name;
	public $path;
	public $weight;
	public $date;
	public $width;
	public $height;
	/**
	 * Odczytanie danych pliku
	 * @param string $path ścieżka do pliku
	 */
	public function __construct($path)
	{
		if(is_file($path))
		{
			$this->path=str_replace(ROOT_DIR,"",$path);
			$tmp=explode("/",$path);
			$this->name=$tmp[sizeof($tmp)-1];
			$this->weight=filesize($path)/1024;
			$this->date=date("Y-m-d H:i:s",fileatime($path));
			$tmp=explode(".",$this->name);
			$tmp=$tmp[sizeof($tmp)-1];
			if($tmp=="jpg" || $tmp=="jpeg" || $tmp=="gif" || $tmp=="png")
			{ @list($this->width, $this->height)=getimagesize($path); }
		}
	}
}
?>