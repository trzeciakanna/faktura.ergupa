uploader_obj = {	
	/**
	 * Uploadowanie pliku
	 * @param id numer id uploadera
	 * @param type typ pliku
	 */
	upload : function(id, type)
	{
		var src=$('uploader_upload'+id).value;
		var tmp=src.toLowerCase().split(".");
		tmp=tmp[tmp.length-1];
		if(!(type=="img" && (tmp=="jpg" || tmp=="jpeg" || tmp=="png" || tmp=="gif")) && !(type=="file") && !(type=="flash" && (tmp=="swf")))
		{ alert($('bad_ext'+id).value); }
		else
		{
			//znalezienie formularza
			var form=$('uploader_upload'+id);
			while(form.nodeName.toLowerCase()!="form" && form.nodeName.toLowerCase()!="body")
			{ form=form.parentNode; }
			//sprawdzenie czy znalazło formularz
			if(form.nodeName.toLowerCase()=="body")
			{
				alert($('error'+id).value);
				return false;
			}
			//zapamiętanie danych formularza
			var form_action=form.action;
			var form_method=form.method;
			var form_target=form.target;
			var form_enctype=form.enctype;
			var form_onsubmit=form.onsubmit;
			//wysłanie formularza
			$('uploader_iframe'+id).innerHTML='<iframe name="iframe_upload'+id+'" id="iframe_upload'+id+'" src="#"></iframe>';
			form.action=path+"module/uploader/upload.php?count="+id+"&type="+$('type'+id).value+"&what="+type+"&user_id="+$('user_id'+id).value;
			form.method="post";
			form.target="iframe_upload"+id;
			form.enctype="multipart/form-data";
			form.onsubmit=null;
			form.submit();
			$('uploader_progress'+id).style.display="block";
			$('uploader_upload'+id).style.display="none";
			//przywrócenie danych formularza
			form.action=form_action;
			form.method=form_method;
			form.target=form_target;
			form.enctype=form_enctype;
			form.onsubmit=form_onsubmit;
		}
	},
	/**
	 * Zamkończenie uploadu
	 * @param id numer id uploadera
	 * @param type typ pliku
	 * @param msg rezultat uploadu
	 */
	close : function(id, type, msg)
	{
		$('uploader_progress'+id).style.display="none";
		$('uploader_upload'+id).style.display="block";
		$('uploader_upload'+id).value="";
		var tmp=msg.split("|");
		if(tmp[0]=="ok")
		{
			if(type=="img")
			{ $('uploader_download'+id).src=path+tmp[1]; }
			else if(type=="file")
			{ $('uploader_download'+id).href=path+tmp[1]; }
			else if(type=="flash")
			{  }
			$('up_file'+id).value=path+tmp[1];
			$('uploader_download'+id).style.display="block";
			$('uploader_del'+id).style.display="block";
		}
		else if(tmp[0]=="error")
		{
			if(tmp[1]=="size")
			{ alert($('bad_size'+id).value.replace("[width]",tmp[2]).replace("[height]",tmp[3])); }
			else if(tmp[1]=="weight")
			{ alert($('bad_weight'+id).value.replace("[weight]",tmp[2])); }
			else { alert($('error'+id).value); }
		}
		else { alert($('error'+id).value); }
	},
	/**
	 * Usunięcie załadowaniego pliku
	 * @param id numer id uploadera
	 * @param type typ pliku
	 */
	del : function(id, type)
	{
		$('up_file'+id).value="";
		if(type=="img")
		{ $('uploader_download'+id).src=""; }
		else if(type=="file")
		{ $('uploader_download'+id).href=""; }
		else if(type=="flash")
		{  }
		$('uploader_download'+id).style.display="none";
		$('uploader_del'+id).style.display="none";
	},
	/**
	 * Załadowanie wgranego już pliku
	 * @param id numer id uploadera
	 * @param value adres pliku
	 * @param type typ pliku
	 */
	load : function(id, value, type)
	{
		$('up_file'+id).value=value;
		if(type=="img")
		{ $('uploader_download'+id).src=value; }
		else if(type=="file")
		{ $('uploader_download'+id).href=value; }
		else if(type=="flash")
		{  }
		$('uploader_download'+id).style.display="block";
		$('uploader_del'+id).style.display="block";
		this.more_close(id);
	},
	/**
	 * Pokazanie okna plików
	 * @param id numer id uploadera
	 * @param type typ plików
	 */
	more : function(id, type)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			var layer=document.createElement("div");
			layer.innerHTML=this.responseText;
			layer.id="uploader_more_"+id;
			layer.className="uploader_contener";
			document.body.appendChild(layer);
			size=window_size();
			var width=size[0]*0.9;
			var height=size[1]*0.9;
			$("uploader_more_"+id).style.left=(size[0]/2)+"px";
			$("uploader_more_"+id).style.top=(size[1]/2)+"px";
			mint.fx.Move("uploader_more_"+id,size[0]*0.1,size[1]*0.1,100,2000);
			mint.fx.Size("uploader_more_"+id,size[0]*0.8,size[1]*0.8,100,2000);
			this.responseText;
			
		};
		req.AddParam('count',id);
		req.AddParam('type',$('type'+id).value);
		req.AddParam('what',type);
		req.AddParam('user_id',$('user_id'+id).value);
		req.AddParam('path',path);
		req.Send(path+"module/uploader/ajax_more.php");
	},
	/**
	 * Zamknięcie okna plików
	 * @param id numer id uploadera
	 */
	more_close : function(id)
	{
		document.body.removeChild($('uploader_more_'+id));
	}
};