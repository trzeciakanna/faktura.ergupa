<?php
$langs=array();
$langs['del']="Remove File";
$langs['more']="Available Files";
$langs['download']="Download File";
$langs['close']="Close";
$langs['max_size']="Maximum Graphics size is [width]px * [height]px.";
$langs['max_weight']="Maximum file size is [weight]kB.";
$langs['bad_ext']="You uploaded a file with a wrong extension.";
$langs['error']="An error occurred while sending the file, please refresh the page (CTRL+F5) and try again.";
?>