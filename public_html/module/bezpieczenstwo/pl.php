<?php
$lang=array();
$lang['nagl']="Bezpieczeństwo w afaktury.pl";
$lang['text']="<br>
<br>
<p style='text-align: left;'>
<b>Baza danych została zgłoszona do GIODO, wszystkie formuły bezpieczeństwa wymagane przez tą instytucję zostały zachowane z najwyższą starannością.<br>
<br>
Nie udostępniamy żadnych danych osobowych innym podmiotom ani osobom trzecim. <br>
<br>
Państwa dane oraz dane kontrahentów są bezpieczne, przetwarzanie danych osobowych odbywa się za pomocą połączenia szyfrowanego SSL na zabezpieczonych serwerach. <br>
<br>

<u>Dodatkowo zapewniamy:</u><br>
    
      - Bezpieczeństwo danych Certyfikat SSL najwyższej walidacji EV, takim jak w bankach<br>

      - Szyfrowanie danych przy użyciu protokołu SSL z 256-bitowym kluczem szyfrującym<br>
    
      - Zabezpieczenia serwerów poprzez Firewall<br>

      - Kopie bezpieczeństwa (backup wszystkich danych)<br>

      - Bezpieczny dostęp przez 24h na dobę z każdego miejsca na świecie w zasięgu internetu (99.9% dostępności)<br>
      
      - Spokojny sen :) W przypadku awarii lub zawirusowania Państwa komputera wszystkie dane są nadal bezpieczne na naszych serwerach i nie ma obawy o ich utratę</b>
    
</p>
<br>
<br>";


?>