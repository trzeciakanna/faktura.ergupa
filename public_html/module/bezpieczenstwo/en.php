<?php
$lang=array();
$lang['text']="<br>
<br>
<p style='text-align: left;'>
<b>The database has been registered with GIODO, all security procedures required by this institution have been observed with utmost accuracy.<br>
<br>
We do not process or render any personal information accessible to any third parties or entities. <br>
<br>
Your data and the customers' data are safe; the processing of personal data is carried out with the use of an SSL coded connection on secure servers.<br>
<br>

<u>We also provide:</u><br>
    
      - Security of the data - SSL Certificate<br>

      - Data encoding using an SSL protocol with a 256-bit encoding key, similar to that used in internet banking<br>
    
      - Server security through a Firewall<br>

      - Backup copies of all data<br>

      - Safe access from any place in the world with internet connection, 24/7<br>
      
      - Peaceful sleep :) In case of a malfunction or a virus infection of your computer, all data are still safe on our servers and there is no need to worry about their loss</b>
    
</p>
<br>
<br>";


?>