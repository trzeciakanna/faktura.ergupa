<?php
$lang=array();
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik online";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstszych problemów";
$lang['pomoc2_tresc']="	Jeśli potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000</span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "System wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, bądź inną<br> Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych.<br> Możliwość zapisania stworzonej faktury w formacie PDF lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, i wiele wiele innych przydatnych opcji...";
$pomoc = array();
$pomoc[] = array('','W jakiej przeglądarce działa program ','Do korzystania z naszej aplikacji zalecamy przeglądarki Firefox, przy korzystaniu z przeglądarki internet Explorer nie gwarantujemy działania prawidłowości wszystkich funkcji. ');
$pomoc[] = array('','Jak zarejestrować się w serwisie ?', '<img src="images/pomoc/pomoc1.jpg"><br>
Aby zarejestrować się w naszym serwisie, kliknij w przycisk zarejestruj się widoczny w prawym górnym rogu. System poprosi o podanie ( ustalenie ) loginu oraz hasła i zatwierdzenie przyciskiem REJESTRUJ. Następnie należy kliknąć w prawym górnym rogu ZALOGUJ SIĘ wpisać ustalony login i hasło'); 
$pomoc[] = array('','Czy informację przypominającą o zakończeniu abonamentu dostanę na adres mailowy?', 'Tak, informację o zakończeniu okresu abonamentowego dostaniecie Państwo bezpośrednio na podany adres mailowy, wraz z propozycja przedłużenia abonamentu na dogodny dla Państwa okres czasu. '); 
$pomoc[] = array('','Czy moje faktury są bezpiecznie przechowywane? ', 'Tak, wszystkie faktury są zabezpieczone na naszych serwerach on-line. W każdej chwili można je edytować, zmieniać dane - są one tylko do Twojego wglądu, nikt inny nie może ich przeglądać. '); 
$pomoc[] = array('','Czy za nową wersję programu oraz aktualizację trzeba dodatkowo zapłacić?', 'Wszelkie aktualizacje naszego programu mają na celu poprawienie jakości i funkcjonalności. Wprowadzamy je bez dodatkowych kosztów dla Państwa. O nowościach w naszym programie, można przeczytać w zakładce: Aktualności.'); 
$pomoc[] = array('','W jaki sposób mogę przejrzeć do archiwum moich faktur?', 'Każdy użytkownik , z aktywnym abonamentem ma możliwość przeglądania wszystkich swoich, zapisanych faktur. Aby je przeglądać należy wejść w TWOJE KONTO, następnie wybrać FAKTURY i LISTA FAKTUR. Wszystkie faktury są przechowywane na naszych, zabezpieczonych serwerach.   '); 
$pomoc[] = array('','Czy po zakończonym czasie abonamentowym będę miał wgląd do wystawionych wcześniej faktur?', 'LISTA FAKTUR jest dostępna po wygaśnięciu abonamentu. Można w każdej chwili wejść do archiwum zachowanych faktur po zalogowaniu się na swoje konto.  '); 
$pomoc[] = array('','Czy można wypróbować program bez rejestracji?', '<img src="images/pomoc/pomoc2.jpg"><br>Tak, można wystawić fakturę całkowicie za darmo, bez rejestracji. Należy kliknąć WYSTAW FAKTURĘ - w menu głównym serwisu. Bez logowania nie ma możliwości korzystania z dodatkowych opcji przewidzianych dla zarejestrowanych użytkowników'); 
$pomoc[] = array('','Czy istnieje wersja testowa?? Ile za nią muszę zapłacić? ', 'Wersja testowa jest dostępna przez 30 dni dla użytkownika który zarejestrował i zalogował się. W tym czasie zalogowany użytkownik może bezpłatnie korzystać ze wszystkich opcji faktury. Po 30 dniach testu, aby nadal korzystać do dodatkowych funkcji należy wykupić abonament.  '); 
$pomoc[] = array('','Jak mogę się z Wami skontaktować ?', 'Pytanie możesz zadać o każdej porze dnia i nocy poprzez formularz: "Zadaj pytanie" oraz poprzez dział "Kontakt".<br> <br>Wysyłasz pytanie do nas, a my staramy się na nie jak najszybciej odpowiedzieć.<br><br> Jesteśmy też otwarci na wszelkie sugestie dotyczące działania serwisu oraz jego opcji.<br><br><b>Kolejną formą kontaktu jest telefon do nas:)</b><br><br>Jesteśmy dostępni w dni powszednie od 9.00 do 17.00. pod nr. <b>tel. 601-686-414</b><br><br>W Bardzo pilnych sprawach pełnimy dyżur codziennie w godz 9.00 - 23.00 pod nr <b>tel - 601 686 414.</b> '); 
$pomoc[] = array('','Czy mogę się dowiedzieć czegoś na temat rodzajów faktur? Gdzie mam tych informacji szukać?', '<img src="images/pomoc/pomoc3.jpg"><br>Tak, wszelkie nowości ze świata faktur znajdziesz w zakładce : Aktualności. '); 
$pomoc[] = array('','Czy istnieje możliwość wystawienia faktury bez logowania?', 'Tak, możesz wystawić fakturę całkowicie za darmo, bez rejestracji. Jest to możliwe po kliknięciu przycisku : wystaw fakturę - na stronie głównej serwisu. '); 
$pomoc[] = array('','Czy mogę wypróbować program bez rejestracji?', 'Tak, oczywiście, nasz serwis umożliwia wystawianie Rachunku, oraz różnych rodzajów faktur takich jak: faktura vat, faktura Pro Forma, faktura Zaliczkowa.
Możesz wybrać rodzaj faktury w zakładce: Wystaw fakturę i tam wybrać spośród mniejszych zakładek rodzaj rachunku/faktury. '); 

$pomoc[] = array('','Jak wystawić fakturę? ','Wystawianie faktury jest dostępne dla użytkowników zalogowanych jak i nie zalogowanych. Aby wystawić fakturę należy kliknąć w menu górnym WYSTAW FAKTURĘ i wpisać niezbędne dane. ');
$pomoc[] = array('','Gdzie mogę wybrać czas na jaki chcę wykupić/przedłużyć abonament?','Ta usługa jest dostępna po zarejestrowaniu sie do serwisu.
Okres abonamentu możesz wybrać w zakładce : Twoje konto i wybrać z listy  interesujący Cię czas, następnie przedłużyć go klikając w przycisk : Przedłuż - zostaniesz przeniesiony do strony płatności i zapłać za wybrany przez Ciebie okres. ');
$pomoc[] = array('','Jak szybko wpisać nazwę towaru z mojej listy do faktury?','Ta usługa jest dostępna po zarejestrowaniu sie do serwisu.
Aby dodać produkt/usługę z bazy na fakturę należy kliknąć napis: Załaduj produkt/usługę z bazy - znajdujący się nad okienkami do wpisania towaru. Po kliknięciu wyświetli się lista oraz wyszukiwarka towarów/produktów/usług dodanych wcześniej przez Ciebie do bazy.');
$pomoc[] = array('','Jak wystawić datę do zapłaty?','Możesz wybrać termin zapłaty z listy lub wybrać dowolną datę z kalendarza. Aby zaznaczyć wybrany przez Ciebie termin zapłaty kółeczko przy dacie musi być odznaczone na czerwono, kliknij na nie.
Z listy wybierz sposób zapłaty, masz do wyboru: przelew, gotówka, zapłacono, za pobraniem. ');
$pomoc[] = array('','Jak szybko wpisać nazwę towaru z mojej listy do faktury?','Ta usługa jest dostępna po zarejestrowaniu sie do serwisu.
Aby dodać produkt/usługę z bazy na fakturę należy kliknąć napis: Załaduj produkt/usługe z bazy - znajdujący się nad okienkami do wpisania towaru. Po kliknięciu wyświetli się lista oraz wyszukiwarka towarów/produktów/usług dodanych wcześniej przez Ciebie do bazy.');

$pomoc[] = array('','Jak mam obliczyć vat ? ','Każdy klient ma do dyspozycji kalkulator Vat, dzięki niemu możesz łatwo przeliczyć wartość netto na brutto lub odwrotnie, możesz wybrać interesująca Cię stawkę vat.
Kalkulator znajdziesz w zakładce: "Wystaw fakturę"
Kalkulator nie będzie widoczny na fakturze. ');
$pomoc[] = array('','Czy mogę wstawić własne logo na fakturę?','
Tak, jest to możliwe po zarejestrowaniu się.
Aby dodać logo należy wejść w zakładkę: Twoje konto, następnie Twoje dane, tam wpiszesz dane podstawowe do faktury oraz dane dodatkowe - czyli logo na fakturę. ');
$pomoc[] = array('','Jak mam dodać produkt do listy produktów ?','Ta usługa jest dostępna po zarejestrowaniu się do serwisu.
Aby dodać Produkt należy wejść w zakładkę: Twoje konto, następnie: Produkty, Dodawanie produktu i wpisać nazwę wraz z danymi. Musisz wybrać kategorię produktu z listy lub dodać nową kategorię.
Możesz sprawdzić listę dodanych produktów, edytować, usunąć je w zakładce Produkty-lista produktów. ');
$pomoc[] = array('','Jak dodać kontrahenta do listy kontrahentów?','Ta usługa jest dostępna po zarejestrowaniu się do serwisu.
Aby dodać kontrahenta należy wejść w zakładkę Twoje konto, Kontrahenci i wpisać nazwę oraz dane w dostępne okienka. Możesz łatwo sprawdzić listę kontrahentów, usunąć je lub wysłać wiadomość do kontrahenta poprzez zakładkę Kontrahenci-wysyłanie wiadomości.
Możesz też dodać kontrahenta przy wystawianiu faktury, w tym celu po wpisaniu danych nabywcy należy kliknąć napis : Zapisz kontrahenta - widoczny nad okienkami do wpisania danych.');
$pomoc[] = array('','Jak szybko wpisać nabywcę z mojej listy kontrahentów na fakturę? ','Ta usługa jest dostępna po zarejestrowaniu się do serwisu.
Aby szybko dodać kontrahenta należy w zakładce: Wystaw fakturę kliknąć napis: Załaduj kontrahenta - wyświetli się lista dodanych przez Ciebie wcześniej kontrahentów, z których wybierasz tego, który Cię interesuje.');
$pomoc[] = array('','Czy można wpisać adres dostawy na fakturę?','Tak, w tym celu należy kliknąć napis : rozwiń - znajdujący się pod Adresem Dostawy na fakturze. ');

$pomoc[] = array('','Jak mam wysłać fakturę na adres e-mail do kontrahenta?','Musisz przypisać adres e-mail do wysyłki danemu kontrahentowi przy dodawaniu kontrahenta. Po wystawieniu faktury i zapisaniu jej na liście faktur kliknij w kopertę w tabeli obok danej faktury. Faktura zostanie wysłana. ');
$pomoc[] = array('','Czy mogę zaznaczyć/odznaczyć fakturę, która została już zapłacona?','Tak, jest to możliwe. Na liście Twoich faktur obok daty sprzedaży jest okienko, które możesz odhaczyć po wpłynięciu należności, nie zapomnij kliknąć \'zapisz\' u góry lub na dole tabeli. ');
$pomoc[] = array('','Jak wysłać wiadomość do klienta/kontrahenta?','Możesz wysłać wiadomość do swoich kontrahentów, do jednego, kilku lub do wszystkich jednocześnie. W zakładce Twoje Konto-Kontrahenci-Wysyłanie wiadomości. Wpisz temat i treść wiadomości w okienko, następnie wyszukaj  kontrahenta wg listy lub odznacz wszystkich i wyślij wiadomość. Zapisze się ona w archiwum wiadomości, do których możesz mieć w każdym momencie podgląd, oraz możesz ponownie wysłać wiadomość. ');

$pomoc[] = array('','Czy mój podpis musi widnieć na fakturze? ','Według przepisów rozporządzających o fakturach wśród elementów jakie powinna zawierać faktura, nie wymieniony jest podpis sprzedawcy lub nabywcy. Wyjątek stanowią faktury VAT RR, czyli faktury wystawiane przez nabywców produktów rolnych i usług rolniczych od rolników ryczałtowych. ');


$pomoc[] = array('','Co to jest faktura korygująca? ','Jeśli błędnie wystawimy fakturę to nie ma powodów do niepokoju, taki dokument zawsze można poprawić wystawiając fakturę korygująca. Na fakturze korygującej należy opisać zdarzenie oraz błąd jaki popełniliśmy w poprzedniej fakturze. Na takiej korekcie faktury powinno byc napisane jakiej faktury i z jakiego dnia dotyczy, w przypadku zwrotów jaka jest ilość, rodzaj i cena zwracanego towaru. Taką fakturę wystawia się też w przypadku zwrotu towaru, reklamacji lub błędów w zamówieniu.');

$pomoc[] = array('','W jakim celu wystawia się fakturę Pro Forma?','Fakturę Pro Forma najczęściej wystawia się przed zapłatą. Forma tej faktury sprawia, że nie może ona być zaksięgowana i rozliczona. Taka faktura stanowi zestawienie towarów i usług z podaniem ceny łącznej jaką należy za nie zapłacić. Dopiero po wpłynięciu zapłaty do sprzedającego, wystawiana jest Faktura Vat, która może zostać zaksięgowana i rozliczona. ');
$pomoc[] = array('','Czym jest Faktura Wzorcowa? ','Jeśli wystawiamy fakturę, a nie mamy pewności co do jej poprawności warto wystawić Fakturę Wzorcową. Fakturę taką zawsze można skorygować i wystawić poprawną, właściwą Fakturę Vat. ');

$pomoc[] = array('',' Jak długo mam obowiązek przechowywać fakturę?','Faktury w formie elektronicznej powinny być przechowywane przez ich wystawcę oraz odbiorcę w formacie, w którym zostały przesłane, w sposób gwarantujący autentyczność ich pochodzenia i integralność ich treści, jak również ich czytelność przez cały okres ich przechowywania. E-faktury przechowuje się tak długo jak papierowe, to jest do czasu upływu terminu przedawnienia zobowiązania podatkowego. ');
$pomoc[] = array('','Co to jest Faktura Okresowa?','Faktura Okresowa jest to faktura wystawiana co określony czas. Takie faktury przeznaczone są do generowania opłat wnoszonych przez klientów co pewien określony czas, na przykład stałe opłaty miesięczne, opłaty czynszowe, itp. Odznacz Fakturę okresową (pod kalkulatorem vat) i wybierz czas/datę co jaki faktura ma być wystawiana. ');
$pomoc[] = array('',' W jaki sposób zmienić nazwę mojej faktury?','Nasz program umożliwia zmianę nazwy nagłówków, łącznie z tytułem. Wystarczy najechać kursorem na wybrany napis, kliknąć na niego i wpisać dowolny tytuł/nazwę.');

$pomoc[] = array('','Ile mam czasu na wystawienie faktury dla klienta?','Fakturę, czyli dowód transakcji, wystawia się nie później niż siódmego dnia od daty wydania towaru lub wykonania usługi. Wyjątek stanowi Faktura Zaliczkowa, którą wystawia się nie później niż siódmego dnia od dnia otrzymania części lub całości należności.');


$pomoc[] = array('','Gdzie mogę wpisać dane swojej firmy lub je edytować ?','Po pierwszym zalogowaniu się do naszego serwisu na wskazany adres e-mailowy przyjdzie link, który przekierowuje do miejsca, gdzie można uzupełnić swoje dane/dane firmy. W przypadku, gdy usuniesz tego maila nim uzupełnisz dane lub, gdy chcesz je edytować, po zalogowaniu się przejdź w menu w Twoje Konto. W tym miejscu możesz uzupełnić/edytować dane. Będą one widoczne na fakturze. ');
$pomoc[] = array('','Jak zapisać/zachować wystawioną fakturę? ','Po wystawieniu możesz zapisać fakturę na zabezpieczonych serwerach klikając Zachowaj (w menu górnym formularza). Opcja ta jest dostępna po zalogowaniu się. W wersji darmowej (również i po zalogowaniu się) fakturę można zapisać na swoim komputerze w wersji pdf - klikając Wydruk PDF. ');
$pomoc[] = array('','Czy mogę edytować/zmieniać nazwy nagłówków w formularzu wystawiania faktur?','Tak, istnieje możliwość zmiany nagłówków, łącznie z tytułem, np. Faktura Vat - wystarczy kliknąć kursorem na dowolny napis i zastąpić go indywidualną nazwą. ');
$pomoc[] = array('','Czy mogę wystawić inny rodzaj dokumentu niż faktura Vat?','Tak, możesz wystawić inny rodzaj dokumentu, Fakturę Pro-Forma, Fakturę Zaliczkową, Fakturę Korygującą, bądź Rachunek. Dodatkowe druki dostępne są po zalogowaniu się, wybiera się je z miejsca Wystaw Fakturę ');
$pomoc[] = array('','Co zrobić gdy zapomniałem hasła po wylogowaniu ?','Twoje hasło jest w powitalnym mailu, który otrzymałeś po zarejestrowaniu się, jeśli jednak nie masz już tego maila kliknij: Zaloguj się a następnie Zapomniałem hasła, wypełnij formularz przypomnienia. ');
$pomoc[] = array('','Czy mogę i gdzie przeglądać wystawione/zachowane faktury?','Tak, ta opcja jest dostępna po zarejestrowaniu się. W tym celu wejdź w zakładkę Twoje konto, Faktury i Lista faktur. Faktury możesz przeglądać z całego okresu fakturowania lub według wybranego miesiąca. Możesz posegregować sobie faktury wg. numeru faktury, daty sprzedaży, daty utworzenia lub daty wystawienia oraz statusu zapłacone. ');
$pomoc[] = array('','Jak wyszukać fakturę, gdy nie znam wszystkich danych ?','
W Liście Faktur jest wyszukiwarka. Możesz wyszukać fakturę po: numerze faktury, nazwie nabywcy lub NIP-ie nabywcy. Dostępna jest  również wyszukiwarka zaawansowana - po rozwinięciu jej możesz wyszukać według daty wystawienia, sprzedaży lub kwoty faktury.
Wyszukiwarka znajduje się w zakładce Twoje Konto-Faktury-Lista faktur. ');
$pomoc[] = array('','Jak mogę skomentować artykuł w dziale Aktualności?','Możesz skomentować dany artykuł wpisując treść komentarza w okienku pod każdym newsem. Pamiętaj o podpisie i przepisaniu kodu z obrazka wg wskazówek. Kliknij Dodaj. ');
$pomoc[] = array('','Czy pomyłkowo wpisane dane mogę szybko usunąć?','Tak, jeśli pomyliłeś się wypisując fakturę możesz szybko wyczyścić formularz klikając Nowa Faktura na górze dokumentu - uwaga, wówczas wszystkie wpisane dane na fakturze zostaną usunięte! ');
$pomoc[] = array('','
Czy moje dane są bezpiecznie przechowywane?','Oczywiście, nie przetwarzamy oraz nie udostępniamy żadnych danych osobowych innym podmiotom ani osobom trzecim.
Państwa dane oraz dane kontrahentów są bezpieczne, przetwarzanie danych osobowych odbywa się za pomocą połączenia szyfrowanego na bezpiecznych serwerach. ');
$pomoc[] = array('','Co oznaczają znaki zapytania obok niektórych nazw?','Po najechaniu kursorem myszki na pytajnik ukaże się okienko z podpowiedzią do danego nagłówka. Jest to dodatkowa funkcja, która ułatwi Państwu korzystanie z naszego serwisu. ');


?>