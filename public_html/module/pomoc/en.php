<?php
$lang=array();
$lang['pomoc_nagl']="Help Center:";
$lang['pomoc1_nagl']="Online Guide";
$lang['pomoc2_nagl']="Talk to a consultant";
$lang['pomoc3_nagl']="Contact Form - Ask Us a Question";
$lang['pomoc1_tresc']="Contains a detailed description of the application functions and the solutions to the most common problems";
$lang['pomoc2_tresc']="If you need help, call us!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="You can also ask a question by filling in our Contact Form";
$lang['wiecej']= "more";
$lang['do_pomocy'] = "Go to Help";
$lang['o_programie_nagl'] = "What is the online invoicing application?";
$lang['o_programie'] = "The online invoicing application allows you to create your own VAT invoice, proforma invoice or other, in a quick, simple and intuitive way, without having to register.<br> It has a lot of editing options for all the heading fields.<br> It is possible to save the created invoice in PDF format, or to print the invoice right after it is created.<br> Additionally, the service offers automatic calculation of net and gross values, with different VAT rates.<br>
Upon registration every member gains access to all options, such as: making lists of customers, adding products, automatic saving of your data, putting your logo onto your invoices, an archive of stored invoices and many more useful options...";

$pomoc = array();
$pomoc[] = array('','What browser is the best for using the application ','We recommend Firefox browsers. With IE we do not guarantee that all the features will work correctly. ');
$pomoc[] = array('','How to sign up in the service ?', '<img src="images/pomoc/pomoc1.jpg"><br>
In order to sign up in our service, click the SIGNUP button in the top right corner. The system will then ask you to provide a login and password and to confirm with the SIGNUP button. Now all you have to do is enter your login and password.'); 
$pomoc[] = array('','Will I receive a reminder about the end of my Subscription to my email address?', 'Yes. The information about the forthcoming expiry of your Subscription will be sent directly to your email address together with an offer of its extension. '); 
$pomoc[] = array('','Are my invoices safely stored? ', 'Yes, all the invoices are secure on our online servers. You can access them at any time for editing and/or changing - they are for your eyes only. Nobody else can view them. '); 
$pomoc[] = array('','Do I pay extra for the new version or upgrade of the app?', 'No. All upgrades of our app are meant to improve the functionality of the service. We introduce those without any further cost for you. You can read the current news in the News bookmark.'); 
$pomoc[] = array('','How can I view the archive of my invoices? ', 'Every user with an active Subscription has a possibility to view all of his saved invoices. To view these, you must go to your PROFILE EDIT, then select INVOICES and INVOICE LIST. All the invoices are stored on our secure online servers.   '); 
$pomoc[] = array('','After my Subscription expires, Will I have access to the invoices I issued earlier?', 'Yes. INVOICE LIST is available after your Subscription expires. You can enter the archive of the stored invoices upon signing up to your account.  '); 
$pomoc[] = array('','Can I try out the app without signing up? ', '<img src="images/pomoc/pomoc2.jpg"><br>Yes, you can issue an invoice completely free of charge without signing up. All you need to do is click ISSUE AN INVOICE - in the main menu. Without logging in, it is not possible to use the extra features meant for the registered users.'); 
$pomoc[] = array('','Is there a trial version? How much is it? ', 'The trial version is available for 30 days for a User who signe up and logged in. During this period, the User may use all the features of our app. After the 30 days, it is advisable to buy the Subscription in order to continue using all the features of the service.  '); 
$pomoc[] = array('','Where can I ask a question about the afaktura.pl service?', 'You can ask a question 24/7 through the main page: "Ask Us a Question" and/or through the "Contact" section. You send us a question and we will try to answer as well as we can. We are also open to all suggestions concerning the service. '); 
$pomoc[] = array('','Can I learn something about the kinds of invoices? Where do I look for this information?', '<img src="images/pomoc/pomoc3.jpg"><br>Yes, you can get the latest news from the world of invoices in the NEWS section'); 
$pomoc[] = array('','Is it possible to issue an invoice without logging in?', 'Yes, you can issue an invoice completely free of charge, without signing up. You can do that by clicking the ISSUE AN INVOICE button on the main page of the service. '); 
$pomoc[] = array('','Can I try the app out without signing up?', 'Yes, of course. Our service let you make out a bill and/or various kind of invoices, such as: VAT, Pro Forma, Pre-Payment invoices.
You can select the type of the invoice in the ISSUE AN INVOICE section and there you can select the type of the invoice/bill. '); 

$pomoc[] = array('','How do I issue an invoice? ','Issuing invoices is available to both logged and unlogged Users. To issue an invoice, click in the main menu the ISSUE button and enter the required data. ');
$pomoc[] = array('','Where can I choose how long I want my Subscription for?','This feature is available after sign-up.
You can choose the length of your Subscription in YOUR ACCOUNT section. You can also extend your Subscription by clicking EXTEND - you will be redirected to the payment page where you can pay for the selected period. ');
$pomoc[] = array('','How do I quickly put a name of a Product from my list onto the invoice?','This feature is available after sign-up.
In order to add a Product/Service from the database onto the invoice, click LOAD PRODUCT/SERVICE FROM THE DATABASE which is located above the windows for entering the Product. On clicking, a list will be displayed with a search engine for goods/products/services added to the base by you earlier.');
$pomoc[] = array('','How do I enter the payment deadline?','You can choose the deadline from the list or select any date from the calendar. In order to mark the selected date, the circle by the date must be marked red - click on it.
From the list you may select the following: wire transfer, cash, paid, cash on delivery ');
$pomoc[] = array('','How do I calculate the VAT? ','Every User can use a VAT calculator. With it, you can easily calculate between Net and Gross values or select the VAT Rate of your interest.
The calculator can be found in the ISSUE AN INVOICE section.
The calculator will not be seen on the invoice. ');
$pomoc[] = array('','Can I put my own logo on the invoice?','
Yes, it is possible upon signing up.
To add a logo, go to YOUR ACCOUNT -> YOUR DATA. There, you can enter the basic data for the invoice as well as extra data - the logo. ');
$pomoc[] = array('','How do I add a Product to the Product List ?','This feature is available after sign-up.
To add a Product, go to YOUR ACCOUNT -> PRODUCTS -> ADD PRODUCT and enter the name and data.
You must select the category of the Product from the list or add a new category.
You may check the list of added Products, edit or remove them in the PRODUCT LIST section. ');
$pomoc[] = array('','How do I add a Customer to the Customer List?','This feature is available after sign-up.
To add a Customer, go to YOUR ACCOUNT -> CUSTOMERS and enter all the data in the available widows. You can easily check the Customer List while actually issuing the invoice, or send a message to a Cusstomer through CUSTOMERS -> SEND A MESSAGE.
You can also add a Customer while issuing an invoice. To do so, when all the Buyers data are entered, click the SAVE CUSTOMER button above the data fields.');
$pomoc[] = array('','How do I quickly put the Buyer from my list onto the invoice? ','This feature is available after sign-up.
To quickly add a Customer, click LOAD CUSTOMER in the ISSUE AN INVOICE section - a list will be displayed of the Customers you added earlier and you can choose the one you want.');
$pomoc[] = array('','Can I put the deliverz address on the invoice?','Yes. You must click the PULL DOWN button which can be found below the delivery address on the invoice. ');

$pomoc[] = array('','How do I send the invoice to my Customers email address?','You must ascribe an email address to every Customer while adding him. After issuing the invoice and saving it on the Invoice List, click on the envelope symbol in the chart next to the invoice. The invoice will be sent. ');
$pomoc[] = array('','Can I mark/unmark an invoice that has already been paid?','Yes. This is possible. On the list of your invoices there is a window which you can unmark after the payment was made. Do not forget to click SAVE at the top or the bottom of the chart. ');
$pomoc[] = array('','How do I send a message to my Customer/Partner?','You can send messages to your Customers, one at a time or all at the same time.  Go to YOUR ACCOUNT -> CUSTOMERS -> SEND A MESSAGE. Enter the subject and the contents of the message and find a Customer according to the list or unmark all and send the message. It will be automatically saved in the message archive which you can view at any time and/or resend any message that is there.');

$pomoc[] = array('','Does my signature have to be on the invoice? ','According to the current regulations, there is no need for the signature of the Seller or the Buyer. An exception is the VAT RR Invoice - a type of invoice issued by agricultural product buyers and agricultural services from lump farmers.');

$pomoc[] = array('','What is a Correction Invoice? ','If you make out the invoice wrongly, there is no need to worry. Such a document can easily be corrected by issuing a Correction Invoice. It is important to describe the event and the kind of mistake you made in the original invoice. It should also say what original invoice it concerns. In case of returning goods, you must also state the quantity, kind and price of the returned goods. A correction invoice is also issued in case of returning, complaint or wrong order.');

$pomoc[] = array('','Why is the Pro Forma Invoice issued?','The Pro Forma is commonly issued before the payment is made. The format of this invoice makes it impossible to book. This kind of invoice is a list of goods and services with the total price to be paid. Only upon receiving the payment can a proper VAT Invoice be issued, which can be booked and accounted. ');
$pomoc[] = array('','What is the Example Invoice? ','If you make out an invoice but are not sure if it is correct, it is worth trying to issue an example invoice. This kind of invoice can always be corrected and reissued in the form of a proper VAT Invoice. ');

$pomoc[] = array('','How long do I need to keep the invoices?','The invoices in the electronic form ought to be kept by the issuer in the format in which they were sent, in the manner that guarantees their authenticity and integrity as well as their intelligibility throughout the period of keeping. E-invoices should be kept as long as their paper counterparts, i.e. until the expiration of the tax obligation. ');
$pomoc[] = array('','What is a periodical invoice?','A periodical invoice is an invoice issued regularly. Such invoices are meant for generating payments made by Customers regularly, e.g. regular monthly payments, rent payments, etc. Mark the Periodical Invoice (below the VAT calculator) and select time/date of regular issuing of the invoice. ');
$pomoc[] = array('','How can I change the name of my invoice?','Our system makes it possible to change headers including titles. All you need to do is click the selected name and type any name you want.');

$pomoc[] = array('','How much time do I have to issue an invoice for my Customer?','The invoice, or proof of transaction, must be issued no later than 7 days from the reception of goods or execution of services. An exception is the Pre-Payment Invoice which must be issued no later than 7 days from the reception of part or all of the payment.');

$pomoc[] = array('','Where can I enter or edit the data of my company?','Upon the first logging in to our service, you will receive an email which will direct you to the profile editing. In case you delete this email before editing your data or if you wish to edit them, go to YOUR ACCOUNT after logging in. Here you can complete/edit/modify your data. These will be seen on the invoice. ');
$pomoc[] = array('','How do I save the issued invoice? ','After issuing you may save the invoice on secure servers by clicking SAVE (in the upper menu of the form). This feature is available after logging in. Both in trial and full versions it is possible to save the invoice on your local hard disk in .PDF format by clicking PRINT PDF.');
$pomoc[] = array('','Can I edit/change the names of the headers in the issuing form?','Yes, it is possible to change the headers, including the title, e.g. VAT Invoice - all you need to do is click on any given name and replace it with your own. ');
$pomoc[] = array('','Can I issue a different document than a VAT Invoice?','Yes, you can issue a different document: a Pro Forma, a Pre-Payment, a Correction Invoice or a Bill. Additional forms are available after logging in. You can find them in the ISSUE AN INVOICE section ');
$pomoc[] = array('','What should I do if I forgot my password after logging out?','Your password is included in the welcome email which you received upon signing up. But if you do not have this email any more, click LOG IN and then FORGOT THE PASSWORD and fill out the reminder form. ');
$pomoc[] = array('','Can I browse through all my issued/saved invoices? If so, where?','Yes, This feature is available after sign-up. Go to YOUR ACCOUNT -> INVOICES -> INVOICE LIST. You can view all your invoices in total or you can sort them by month, invoice number, date of sale, date of creating, date of issue or payment status. ');
$pomoc[] = array('','How do I search for an invoice if I do not have all the required data?','
In the Invoice List there is a search engine. You can search by: invoice number, buyers name or the Tax ID number of the Buyer. There is also an advanced search available - after scrolling this, you can search by date of issue, date of sale or invoice amount.
The search engine can be found in YOUR ACCOUNT -> INVOICES -> INVOICE LIST. ');
$pomoc[] = array('','How can I comment on an article in the NEWS section?','You can comment on an article by entering your comment in a window below every news piece. Remember to sign your comment and copy the picture code, according to the instructions.Click ADD. ');
$pomoc[] = array('','Can I quickly remove the data which I entered by mistake?','Yes, if you made a mistake while making out an invoice, you can quickly clear the form by clicking NEW INVOICE at the top of the document. CAUTION!!! All the data will then dissappear from the invoice. ');
$pomoc[] = array('','Are my data safely stored?','Of course. We do not process or make accessible ANY personal data to any third parties.
Your and your Customers data are 100% safe. All the data processing is performed with the use of an encoded connection on our secure servers. ');
$pomoc[] = array('','What do the question marks mean beside some names?','On putting the cursor over the question mark, a window will pop up with a hint for a given header. It is an additional feature which will make it more convenient to use our service. ');

?>