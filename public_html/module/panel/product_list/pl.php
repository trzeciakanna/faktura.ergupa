<?php
$lang=array();
$lang['head']="Lista produktów";
$lang['head_cat']="Lista podkategorii";
$lang['head_view']="Dane produktu";
$lang['head_edit']="Edycja produktu";

$lang['searcher']="Wyszukiwarka";
$lang['show']="rozwiń";
$lang['hidden']="zwiń";
$lang['all']="wszystkie";
$lang['view_cat']="Widok kategorii";
$lang['view_list']="Widok listy";
$lang['add']="Dodaj produkt do tej kategorii";
$lang['back']="wstecz";
$lang['']="";

$lang['cancel']="Anuluj";
$lang['edit']="Edytuj";
$lang['save']="Zapisz";
$lang['del']="Usuń";
$lang['view']="Zobacz";
$lang['search']="Szukaj";
$lang['enter']="Wejdź";

$lang['cat']="Wybierz kategorie";
$lang['number']="Numer";
$lang['name']="Nazwa";
$lang['producer']="Producent";
$lang['pkwiu']="PKWiU";
$lang['unit']="Jednostka";
$lang['vat']="Stawka VAT";
$lang['netto']="Cena NETTO";
$lang['brutto']="Cena BRUTTO";
$lang['prize']="Cena";

$lang['info']="Pola są wymagane";
$lang['empty_cat']="Wybierz kategorię";
$lang['empty_name']="Podaj nazwę produktu";
$lang['empty_vat']="Wybierz stawkę VAT";
$lang['empty_prize']="Podaj cenę produktu";

$lang['save_ok']="Produkt został zapisany poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania produktu. Spróbuj ponownie za chwilę.";
$lang['del_ok']="Produkt został usunięty poprawnie";
$lang['del_error']="Wystąpił błąd podczas usuwania produktu. Spróbuj ponownie za chwilę.";
$lang['delete']="Czy na pewno chcesz usunąć ten produkt";


$lang['nagl1'] ='Faktura VAT';
$lang['nagl2'] ='Jesteś w';
$lang['nagl3'] ='Wyszukiwarka faktur';
$lang['nagl4'] ='Dodaj produkt';
$lang['nagl5'] ='Wyszukiwanie zaawansowane';
$lang['nagl6'] ='Nazwa';
$lang['nagl7'] ='Kategoria';
$lang['nagl8'] ='Cena produktu/usługi';
$lang['nagl9'] ='Zobacz';
$lang['nagl10'] ='Edytuj';
$lang['nagl11'] ='Usuń';
$lang['nagl12'] ='Nazwa produktu';
$lang['nagl13'] ='Usuń';
?>