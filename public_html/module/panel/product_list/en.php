<?php
$lang=array();
$lang['head']="Product List";
$lang['head_cat']="Subcategory List";
$lang['head_view']="Product Data";
$lang['head_edit']="Edit Product";

$lang['searcher']="Search Engine";
$lang['show']="pull down";
$lang['hidden']="pull up";
$lang['all']="all";
$lang['view_cat']="Category View";
$lang['view_list']="List View";
$lang['add']="Add Product to this Category";
$lang['back']="back";
$lang['']="";

$lang['cancel']="Cancel";
$lang['edit']="Edit";
$lang['save']="Save";
$lang['del']="Remove";
$lang['view']="View";
$lang['search']="Search";
$lang['enter']="Enter";

$lang['cat']="Select Category";
$lang['number']="Number";
$lang['name']="Name";
$lang['producer']="Manufacturer";
$lang['pkwiu']="Polish Goods Classification#";
$lang['unit']="Unit";
$lang['vat']="Vat Rate";
$lang['netto']="Net Price";
$lang['brutto']="Gross Price";
$lang['prize']="Price";

$lang['info']="Fields are required";
$lang['empty_cat']="Select Category";
$lang['empty_name']="Enter Product Name";
$lang['empty_vat']="Select VAT Rate";
$lang['empty_prize']="Enter Product Price";

$lang['save_ok']="Product successfully saved";
$lang['save_error']="An error occurred while saving the Product. Please try again later.";
$lang['del_ok']="Product successfully removed";
$lang['del_error']="An error occurred while removing the Product. Please try again later.";
$lang['delete']="Do you really want to remove this Product?";


$lang['nagl1'] ='VAT Invoice';
$lang['nagl2'] ='You are in';
$lang['nagl3'] ='Invoice Search';
$lang['nagl4'] ='Add a Product';
$lang['nagl5'] ='Advanced Search';
$lang['nagl6'] ='Name';
$lang['nagl7'] ='Category';
$lang['nagl8'] ='Product/Service Price';
$lang['nagl9'] ='View';
$lang['nagl10'] ='Edit';
$lang['nagl11'] ='Remove';
$lang['nagl12'] ='Product Name';
$lang['nagl13'] ='Remove';
?>