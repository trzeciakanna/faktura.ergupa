<?php
global $smarty;
require(ROOT_DIR."includes/units.php");
require(ROOT_DIR."module/panel/product_add/class.php");
require(ROOT_DIR."module/panel/product_list/".LANG.".php");
global $lang;
global $units;
global $navi;

$smarty->assign("navi",$navi->navi);
$products=new products();
$smarty->assign("lang",$lang);
//odczyt danych użytkownika
$smarty->assign("user_id",USER_ID);

switch($_GET['par4'])
{
	case "del":
		$products->del(USER_ID, (int)$_GET['par5']);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit();
		break;
	case "view":
		$dane=$products->read_one(USER_ID, (int)$_GET['par5']);
		$smarty->assign("dane",$dane);
		$smarty->assign("cat",$products->cat_one(USER_ID,$dane->cat));
		$smarty->assign("action","view");
		break;
	case "edit":
		$smarty->assign("dane",$products->read_one(USER_ID, (int)$_GET['par5']));
		list($prod, $sum)=$products->cat_tree(USER_ID,0,0);
		$smarty->assign("tree",$prod);
		$smarty->assign("units",$units);
		$smarty->assign("action","edit");
		break;
	case "cat":
		$smarty->assign("cats",$products->cat_list(USER_ID,(int)$_GET['par5'],$products->counter(USER_ID)));
		if($_GET['par5'])
		{
			list($dane,$page,$last)=$products->read_list(USER_ID,$_GET['par6'],20,$_GET['par5'],$_GET['par7'],false);
			$smarty->assign("dane",$dane);
			$smarty->assign("page",$page);
			$smarty->assign("last",$last);
			$smarty->assign("sign",$_GET['par7']?$_GET['par7']."/":"");
			$smarty->assign("cat",$products->cat_one(USER_ID,(int)$_GET['par5']));
			$smarty->assign("cat_n",(int)$_GET['par5']);
			$smarty->assign("letter",$products->letter_list(USER_ID,$_GET['par5']));
		}
		$smarty->assign("action","cat");
		break;
	default:
		if(strlen($_GET['par5'])>=4)
		{
			require(ROOT_DIR."includes/search.php");
			$search=new search();
			$_GET['par5']=str_replace("_"," ",urldecode($_GET['par5']));
			$tmp=explode(",",$_GET['par5']);
			$searchs=array();
			$searchs['name']=$tmp[0];
			$searchs['producer']=$tmp[1];
			$searchs['number']=$tmp[2];
			$searchs['pkwiu']=$tmp[3];
			$searchs['prize_min']=$tmp[4];
			$searchs['prize_max']=$tmp[5];
			$searchs['cat']=$tmp[6];
			$smarty->assign("search",$searchs);
			list($dane,$page,$last)=$products->read_list_search(USER_ID,$_GET['par4'],20,$tmp[0],$tmp[1],$tmp[2],$tmp[3],$tmp[4],$tmp[5],$tmp[6]);
		}
		else { list($dane,$page,$last)=$products->read_list(USER_ID,$_GET['par4'],20,0,$_GET['par5']); }
		$smarty->assign("dane",$dane);
		$smarty->assign("page",$page);
		$smarty->assign("last",$last);
		$smarty->assign("sign",$_GET['par5']?$_GET['par5']."/":"");
		$smarty->assign("letter",$products->letter_list(USER_ID));
		list($prod, $sum)=$products->cat_tree(USER_ID,0,0);
		$smarty->assign("tree",$prod);
		$smarty->assign("action","list");
		break;
}

$smarty->display("panel/product_list/".LAYOUT.".html");
?>