panel_product_list_obj = {	
	/**
	 * Zapisanie produktu
	 */
	submit : function(id) 
	{
		var msg="";
		if(!$('cat').value) { msg+=$('empty_cat').value+"\n"; info($('empty_cat').value,'cat'); } else { info('','cat'); }
		if(!$('name').value) { msg+=$('empty_name').value+"\n"; info($('empty_name').value,'name'); } else { info('','name'); }
		if(!$('vat').value) { msg+=$('empty_vat').value+"\n"; info($('empty_vat').value,'vat'); } else { info('','vat'); }
		if(!$('netto').value) { msg+=$('empty_prize').value+"\n";  info($('empty_prize').value,'cena');} else { info('','cena'); }
		if(msg)
		{  }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('save_ok').value); }
				else
				{ info($('save_error').value); }
			};
			req.SendForm("product_list_form","module/panel/product_list/ajax_save.php?user_id="+$("user_id").value+"&id="+id,"post");
		}
		return false;
	},
	/**
	 * Pokazanie panelu wyszukiwarki
	 */
	show_search : function()
	{
		$('search_show').style.display="none";
		$('search_hidden').style.display="block";
		$('search').style.display="block";
	},
	/**
	 * Ukrycie panelu wyszukiwarki
	 */
	hidden_search : function()
	{
		$('search_show').style.display="block";
		$('search_hidden').style.display="none";
		$('search').style.display="none";
	},
	/**
	 * Przejście na stronę wyników wyszukiwania
	 */
	search : function()
	{
		var par=$('name').value+","+$('producer').value+","+$('number').value+","+$('pkwiu').value+","+$('prize_min').value+","+$('prize_max').value+","+$('cat').value;
		location.href="panel/product/list/1/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przeliczenie ceny netto -> brutto
	 */
	change_netto : function()
	{
		var vat=parseInt($('vat').value);
		if(isNaN(vat)) { info($('empty_vat').value); }
		else	
		{
			$('netto').value=$('netto').value.replace(",",".");
			$('brutto').value=round($('netto').value*(10000+vat*100)/10000,4);
		}
	},
	/**
	 * Przeliczenie ceny brutto -> netto
	 */
	change_brutto : function()
	{
		var vat=parseInt($('vat').value);
		if(isNaN(vat)) { info($('empty_vat').value); }
		else	
		{
			$('brutto').value=$('brutto').value.replace(",",".");
			$('netto').value=round($('brutto').value*10000/(10000+vat*100),4);
		}
	}
};