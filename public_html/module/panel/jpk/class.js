jpk_obj = {
  generate : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"error");
			info('',"generate");
		  var m = this.responseText.split('|');
			if (m[0] == "ok") {
			  location.href="http://"+location.host+"/download.php?file="+m[1];
				if (m[2] == 1) {
          info($('okValid').value,"generate");
				} else {
					var msg = $('okInvalid').value;
					var errors = JSON.parse(m[3]);
					for ( var i in errors) {
						msg += '<br/>' + errors[i];
					}
		      msg += '<br/>' + $('jpkError').value;
          info(msg, "error");
				}	
			} else {
				info($(m).value, "error");
			}
		};
		req.SendForm("jpkForm","module/panel/jpk/generate.php","post");
		return false;
	},
  
  showErrors : function(errors) {
		setTimeout(function() {
			var editor = ace.edit("jpkEditor");
			var cells = editor.renderer.$gutterLayer.$cells;
			errors = JSON.parse(errors);
			var errorHTML = '';
      for ( var i in errors) {
        errorHTML += '<div>' + errors[i] + '</div>';
				var error = errors[i].split(';');
				var line = error[0].split(':')[1].trim() - 1;
				if (line < 0) {
					continue;
				}
				editor.session.highlightLines(line, line, null, null);
			}
      $('jpkErrorList').innerHTML = errorHTML;
		}, 100);
	},
  
  saveFile : function(id) {
		var req = mint.Request();
		req.OnSuccess = function()
		{
      $('jpkXML').value = '';
			info('',"error");
			info('',"generate");
		  var m = this.responseText.split('|');
			if (m[0] == "ok") {
			  //location.href="http://"+location.host+"/download.php?file="+m[1];
				if (m[2] == 1) {
          info($('okValid').value,"generate");
				} else {
					var msg = $('okInvalid').value;
					var errors = JSON.parse(m[3]);
					for ( var i in errors) {
						msg += '<br/>' + errors[i];
					}
		      msg += '<br/>' + $('jpkError').value;
          info(msg, "error");
				}	
			} else {
				info($(m).value, "error");
			}
		};
    $('jpkXML').value = ace.edit("jpkEditor").getValue();
		req.SendForm("jpkForm","module/panel/jpk/save.php?id=" + id,"post");
		return false;
	},
  
  showErrorsPop : function(obj) {
		errors = JSON.parse(obj.dataset.errors);
		var errorHTML = '';
    for ( var i in errors) {
      errorHTML += '<div>' + errors[i] + '</div>';
		}
    $('errorsList').innerHTML = errorHTML;
    var size=window_size();
		$('panel_error').style.display="block";
		$('panel_error').style.top=((size[1]-$('panel_error').clientHeight)/2)+"px";    
    return false;
	},
  
  close : function()
	{
		$('panel_error').style.display="none";
		$('panel_mail').style.display="none";
    return false; 
	},

  send : function(id)
  {
    $('idJpk').value = id;
    var size=window_size();
		$('panel_mail').style.display="block";
		$('panel_mail').style.top=((size[1]-$('panel_mail').clientHeight)/2)+"px";    
    return false;
  },
  
  sendEnd : function()
  {
    var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"error");
			info('',"ok");
		  switch(this.responseText)
      {
        case "errId": info($('errId').value, "error");
          break;
        case "errMail": info($('errMail').value, "error");
          break;
        case "ok": info($('okSend').value, "ok");
          break;
        default:
          info($(this.responseText).value, "error");
          break;
      }
			
      jpk_obj.close();
		};
		req.SendForm("jpkSend","module/panel/jpk/send.php","post"); 
    return false;
  },
};