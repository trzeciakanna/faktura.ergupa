<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
require(ROOT_DIR."includes/users.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();

$users=new users();
if($_SESSION['user_id']) { $my=$users->read_one($_SESSION['user_id'],false); }
elseif($_COOKIE['user_id']) { $my=$users->read_one_by_hash($_COOKIE['user_id']); }
else { exit(); }
define("USER_ID",$my->id);

require(ROOT_DIR."module/panel/jpk/class.php");
$jpk = new jpk();
echo $jpk->saveXML($_GET['id'], $_POST);
exit();

//echo ($result?"ok":"error");
?>