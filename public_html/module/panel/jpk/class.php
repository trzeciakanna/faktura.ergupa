<?php
class jpk
{
  public function getId($id)
  {
    global $DB;
    $r=$DB->Execute("SELECT * FROM `jpk` WHERE `id`='".$id."' AND `user_id`='".USER_ID."' LIMIT 0,1");
    if($r->fields['id'] != $id) {
      return null;
    }
    $doc = new jpkDoc($r->fields);
    return $doc;
  }

  public function getList(&$page = 1, &$count = 25)
  {
    $page=(int)$page > 0?(int)$page:1;
    $count=(int)$count > 0?(int)$count:25;
    global $DB;
    $r=$DB->PageExecute("SELECT `id`, `date_begin`, `date_end`, `date_create`, `destination`, `kind`, `errors`, `is_valid`, `is_send`, `send_info` FROM `jpk` WHERE `user_id`='".USER_ID."' ORDER BY `date_end` DESC, `date_create` DESC", $count, $page);  
    $t = [];
    while(!$r->EOF)
    {
      $t[] = new jpkDoc($r->fields);
      $r->MoveNext();
    }
    return array($t,$page,$r->_lastPageNo);
  }

  public function saveXML($id, $data)
	{
    global $DB;
		$r = $DB->Execute("SELECT `id`, `kind` FROM `jpk` WHERE `id` = '" . $id . "' AND `user_id` = '" . USER_ID . "' LIMIT 1");
		if($r->fields['id'] != $id)
		{
			return "errId";
		}
		switch($r->fields['kind'])
		{
			case 1:
			default:
				$schema = ROOT_DIR . "jpkschema/jpk_fa_v1.xsd";
				break;
		}
		// print_r($data['xml']);
		$data['xml'] = html_entity_decode($data['xml']);
		$data['xml'] = str_replace([
			"<br />",
			"<br/>",
			"<br>",
			"&lt;br /&gt;"
		], [
			" ",
			" ",
			" ",
			" "
		], $data['xml']);
		// print_r($data['xml']);
		libxml_use_internal_errors(true);
		libxml_clear_errors();
		$doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput = true;
		$doc->loadXML($data['xml']);
		@$isValid = (int)$doc->schemaValidate($schema);
		
		$t = [
			"xml_code" => $doc->saveXML(),
			"is_valid" => $isValid,
			"errors" => $this->parseErrors(libxml_get_errors())
		];
		// print_r($t['jpkData']);
		$r = $DB->AutoExecute("jpk", $t, "UPDATE", "`id` = '" . $id . "'");
		return $r ? ("ok||" . $isValid . "|" . $t['errors']) : "errDb";
	}
  
  public function sendJPK($id, $data)
  {
    if(!$_POST['mail']) {
      return "errMail";
    }
    if(!$id)
		{
			return "errId";
		}
    
    global $DB;
		$r = $DB->Execute("SELECT * FROM `jpk` WHERE `id` = '" . $id . "' AND `user_id` = '" . USER_ID . "' LIMIT 1");
		if($r->fields['id'] != $id)
		{
			return "errId";
		} 
    $jpkDoc =  new jpkDoc($r->fields);
		$path = ROOT_DIR . "files/jpk/";
    $fileName = str_replace([
			" ",
			"-",
			":"
		], [
			"T",
			"",
			""
		], $r->fields['date_create']) . "S" . rand(100, 999).".xml";
		file_put_contents($path . $fileName, $r->fields['xml_code']);
    $file=[["path"=>$path.$fileName,"name"=> $fileName]];

    $user =[["mail"=>$data['mail'],"name"=>$data['mail']]];
    $u=new users_invoice(USER_ID);
    $x=array();
    $data['subject'] = str_replace(["[jpk]", "[nazwa_firmy]"], ["JPK VAT v1", $u->name], $data['subject']); 
    $data['text'] = str_replace(["[jpk]", "[nazwa_firmy]", "[dataOd]", "[dataDo]"], ["JPK VAT v1", $u->name, $r->fields['date_begin'], $r->fields['date_end']], $data['text']);
    
    $x['name']=$u->name;    
    $x['subject']=$data['subject'];  
    $x['text']=$data['text'];
    $mailer=new mailer($u->mail);
    $result=$mailer->send($user,"empty",$x,$file);
    if($result)
    {
      $jpkDoc->send_info[] = ["mail"  => $data['mail'], "date" => date("Y-m-d H:i:s")];
      $send_info = json_encode($jpkDoc->send_info);
      $DB->Execute("UPDATE `jpk` SET `is_send` ='1', `send_info` = '". $send_info ."' WHERE `id` = '" . $id . "' AND `user_id` = '" . USER_ID . "'");
      return "ok";
    }
    return "error";
  }

  public function addJPK($data, $kind = 1)
  {
    switch($kind) {
      case 1:
      default:
        $result = $this->generateFileJPK_FAv1($data);
    }
    if($result[0] == "e")
    {
      return $result;
    }
    $t = [
      "date_begin" => $data['dateBegin'], 
      "date_end" => $data['dateEnd'],
      "date_create" => date("Y-m-d H:i:s"),
      "kind" => $kind,
      "destination" => $data['destination'],
      "is_valid" => $result['isValid'],   
      "xml_code" => $result['xml'],
      "errors" => $this->parseErrors($result['errors']),
      "user_id" => USER_ID
    ];
    global $DB;
    $r = $DB->AutoExecute("jpk", $t, "INSERT");
		if($r)
		{
			$id = $DB->Insert_ID();
			return "ok|" . $id;
		}
		return "errDb";
  }   
  
  public function download($id)
	{
		if(!$id)
		{
			return "errId";
		}
    global $DB;
		$r = $DB->Execute("SELECT * FROM `jpk` WHERE `id` = '" . $id . "' AND `user_id` = '" . USER_ID . "' LIMIT 1");
		if($r->fields['id'] != $id)
		{
			return "errId";
		}
		$path = "files/jpk/" . str_replace([
			" ",
			"-",
			":"
		], [
			"T",
			"",
			""
		], $r->fields['date_create']) . "S" . rand(100, 999) . ".xml";
		file_put_contents(ROOT_DIR . $path, $r->fields['xml_code']);
		return "ok|" . $path . "|" . $r->fields['is_valid'] . "|" . $r->fields['errors'] . "|" . $id;
	}
  
                                 

  public function parseErrors($errors)
	{
		$parsed = [];
		foreach($errors as $error)
		{
			$e = [];
			$e[] = "Linia: " . $error->line;
			$e[] = "Pole: " . $this->parseGetElementName($error->message);
			switch($error->code)
			{
				case 1824:
					$e[] = "Niepoprawny format wartości";
					break;
				case 1830:
					$e[] = "Niepoprawna ilość znaków";
					break;
				case 1831:
					$e[] = "Za mało znaków";
					break;
				case 1832:
					$e[] = "Za dużo znaków";
					break;
				case 1839:
					$e[] = "Wartość nie pasuje do wyrażenia regularnego";
					break;
				case 1840:
					$e[] = "Wartość z poza zakresu dopuszczalnych";
					break;
				default:
					$e[] = "Błąd nr " . $error->code;
					break;
			}
			$parsed[] = join("; ", $e);
		}
		return json_encode($parsed);
	}

	public function parseGetElementName($error)
	{
		// $pattern = "/Element '{http:\/\/jpk.mf.gov.pl\/wzor\/2017\/11\/13\/1113\/}NrKontrahenta': [facet 'minLength'] The value has a length of '0'; this underruns the allowed minimum length of '1'.\n/";
		$pattern = "/Element '(.*)':(.*)/";
		$matches = [];
		preg_match_all($pattern, $error, $matches);
		$element = $matches[1][0];
		$replace = [
			"{http://jpk.mf.gov.pl/wzor/2017/11/13/1113/}" => "tns:",
			"{http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/}" => "etd:",
			"{http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2013/05/23/eD/KodyCECHKRAJOW/}" => "kck:",
			"{http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}" => "tns:",
			"{http://jpk.mf.gov.pl/wzor/2016/10/26/10261/}" => "tns:"
		];
		$element = str_replace(array_keys($replace), array_values($replace), $element);
		return $element;
	}


  public function generateFileJPK_FAv1($data)
  {
    if(!$data['dateBegin'] OR !$data['dateEnd'])
    {
      return "errDate";
    }    
    global $DB;
    $userData = new users_invoice(USER_ID);
    
    libxml_use_internal_errors(true);
		libxml_clear_errors();
		$document = new DOMDocument("1.0", "UTF-8");
		$document->preserveWhiteSpace = false;
		$document->formatOutput = true;
		
    $data['dateBegin'] = date("Y-m-d", strtotime($data['dateBegin'])); 
    $data['dateEnd'] = date("Y-m-d", strtotime($data['dateEnd']));
    
		$JPK = $document->createElement("tns:JPK");
		$JPK->setAttribute("xmlns:etd", "http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/");
		$JPK->setAttribute("xmlns:kck", "http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2013/05/23/eD/KodyCECHKRAJOW/");
		$JPK->setAttribute("xmlns:tns", "http://jpk.mf.gov.pl/wzor/2016/03/09/03095/");
		$JPK->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		$JPK->setAttribute("xsi:schemaLocation", "http://jpk.mf.gov.pl/wzor/2016/03/09/03095/Schemat_JPK_FA(1)_v1-0.xsd");
		// naglowek
		$naglowek = $document->createElement("tns:Naglowek");
		$kodFormularza = $document->createElement("tns:KodFormularza", "JPK_FA");
		$kodFormularza->setAttribute("kodSystemowy", "JPK_FA (1)");
		$kodFormularza->setAttribute("wersjaSchemy", "1-0");
		$wariantFormularza = $document->createElement("tns:WariantFormularza", "1");
		$celZlozenia = $document->createElement("tns:CelZlozenia", $data['destination'] == 0 ? 1 : 2);
		$dataWytworzenia = $document->createElement("tns:DataWytworzeniaJPK", date("Y-m-d") . "T" . date("H:i:s"));
		$dataOd = $document->createElement("tns:DataOd", $data['dateBegin']);
		$dataDo = $document->createElement("tns:DataDo", $data['dateEnd']);
		$domyslnyKodWaluty = $document->createElement("tns:DomyslnyKodWaluty", $data['currency']);
		$kodUrzedu = $document->createElement("tns:KodUrzedu", $data['KodUrzedu']);
		$naglowek->appendChild($kodFormularza);
		$naglowek->appendChild($wariantFormularza);
		$naglowek->appendChild($celZlozenia);
		$naglowek->appendChild($dataWytworzenia);
		$naglowek->appendChild($dataOd);
		$naglowek->appendChild($dataDo);
		$naglowek->appendChild($domyslnyKodWaluty);
		$naglowek->appendChild($kodUrzedu);
		$JPK->appendChild($naglowek);
    
    $podmiot1 = $document->createElement("tns:Podmiot1");
		$identyfikatorPodmiotu = $document->createElement("tns:IdentyfikatorPodmiotu");
		$NIP = $document->createElement("etd:NIP", functions::trimNip($userData->nip));
		$pelnaNazwa = $document->createElement("etd:PelnaNazwa");
		$pelnaNazwa->appendChild($document->createTextNode($userData->name));
		
		$adresPodmiotu = $document->createElement("tns:AdresPodmiotu");
		$kodKraju = $document->createElement("etd:KodKraju", $data['KodKraju']);
		$wojewodztwo = $document->createElement("etd:Wojewodztwo", $data['Wojewodztwo']);
		$powiat = $document->createElement("etd:Powiat", $data['Powiat']);
		$gmina = $document->createElement("etd:Gmina", $data['Gmina']);
		$ulica = $document->createElement("etd:Ulica", $data['Ulica']);
		$nrDomu = $document->createElement("etd:NrDomu", $data['NrDomu']);
		$nrLokalu = $document->createElement("etd:NrLokalu", $data['NrLokalu']);
		$miejscowosc = $document->createElement("etd:Miejscowosc", $data['Miejscowosc']);
		$kodPocztowy = $document->createElement("etd:KodPocztowy", $data['KodPocztowy']);
		$poczta = $document->createElement("etd:Poczta", $data['Poczta']);
		
		$identyfikatorPodmiotu->appendChild($NIP);
		$identyfikatorPodmiotu->appendChild($pelnaNazwa);
		$adresPodmiotu->appendChild($kodKraju);
		$adresPodmiotu->appendChild($wojewodztwo);
		$adresPodmiotu->appendChild($powiat);
		$adresPodmiotu->appendChild($gmina);
		$adresPodmiotu->appendChild($ulica);
		$adresPodmiotu->appendChild($nrDomu);
		if($data['NrLokalu']) { $adresPodmiotu->appendChild($nrLokalu); }
		$adresPodmiotu->appendChild($miejscowosc);
		$adresPodmiotu->appendChild($kodPocztowy);
		$adresPodmiotu->appendChild($poczta);
		$podmiot1->appendChild($identyfikatorPodmiotu);
		$podmiot1->appendChild($adresPodmiotu);
		$JPK->appendChild($podmiot1);
    
    
    $faktury = []; 
    $wiersze = [];
    $sumNettoWiersze = 0;
    $sumBruttoFaktura = 0;
        
    //VAT
    $r = $DB->Execute("SELECT * FROM `invoice` WHERE `user_id`='".USER_ID."' AND `type`='1' AND `date_add` BETWEEN '" . $data['dateBegin'] . "' AND  '". $data['dateEnd'] ."' AND `cur` = '".$data['currency']."' ORDER BY `date_add` ASC LIMIT 10");
    while(!$r->EOF)
    {
      $this->invoiceJPK($r, $document, $faktury, $wiersze, $sumBruttoFaktura, $sumNettoWiersze, 1);
      $r->MoveNext();
    }
    //zaliczkowe
    $r = $DB->Execute("SELECT * FROM `invoice` WHERE `user_id`='".USER_ID."' AND `type`='3' AND `date_add` BETWEEN '" . $data['dateBegin'] . "' AND  '". $data['dateEnd'] ."' AND `cur` = '".$data['currency']."' ORDER BY `date_add` ASC LIMIT 10");
    while(!$r->EOF)
    {
      $this->invoiceJPK($r, $document, $faktury, $wiersze, $sumBruttoFaktura, $sumNettoWiersze, 0);
      $r->MoveNext();
    }
    //korekta
    //brak poprawnego zapisu
  
    if(!empty($faktury))
    {
      foreach($faktury as $faktura)
      {
        $JPK->appendChild($faktura);
      }
      $fakturaCtrl = $document->createElement("tns:FakturaCtrl");
			$liczbaFaktur = $document->createElement("tns:LiczbaFaktur", count($faktura));
			$wartoscFaktur = $document->createElement("tns:WartoscFaktur", number_format($sumBruttoFaktura, 2, ".", ""));
			$fakturaCtrl->appendChild($liczbaFaktur);
			$fakturaCtrl->appendChild($wartoscFaktur);
			$JPK->appendChild($fakturaCtrl);
    }

		if(!empty($wiersze))
		{
      $stawkiPodatku = $document->createElement("tns:StawkiPodatku");
  		$stawki = [
  			"tns:Stawka1" => 0.23,
  			"tns:Stawka2" => 0.08,
  			"tns:Stawka3" => 0.05,
  			"tns:Stawka4" => 0.00,
  			"tns:Stawka5" => 0.00
  		];
  		foreach($stawki as $key => $value)
  		{
  			$stawka = $document->createElement($key, $value);
  			$stawkiPodatku->appendChild($stawka);
  		}
  		$JPK->appendChild($stawkiPodatku);
    
      foreach($wiersze as $wiersz)
  		{
  			$JPK->appendChild($wiersz);
  		}
			$fakturaWierszCtrl = $document->createElement("tns:FakturaWierszCtrl");
			$liczbaWierszyFaktur = $document->createElement("tns:LiczbaWierszyFaktur", count($wiersze));
			$wartoscWierszyFaktur = $document->createElement("tns:WartoscWierszyFaktur", number_format($sumNettoWiersze, 2, ".", ""));
			$fakturaWierszCtrl->appendChild($liczbaWierszyFaktur);
			$fakturaWierszCtrl->appendChild($wartoscWierszyFaktur);
			$JPK->appendChild($fakturaWierszCtrl);
		}
  
    $document->appendChild($JPK);
		$xml = $document->saveXML();
    
    $doc = new DOMDocument();
		$doc->preserveWhiteSpace = false;
		$doc->formatOutput = true;
		$doc->loadXML($xml);
		$isValid = $doc->schemaValidate(ROOT_DIR . "jpkschema/jpk_fa_v1.xsd");
    $errors = libxml_get_errors();
    
    return [
      "xml" => $xml,
      "errors" => $errors,
      "isValid" => $isValid
    ];
  }
  
  private function invoiceJPK($r, &$document, &$faktury, &$wiersze, &$sumBruttoFaktura, &$sumNettoWiersze, $isVat)
  {
    global $DB;
  	$sumNetto = 0;
  	$vatSum = 0;
  
    $sell = $DB->Execute("SELECT * FROM `invoice_data` WHERE `invoice_id` = '" . $r->fields['id'] . "' AND `type` = 'user' LIMIT 1");
    $buy = $DB->Execute("SELECT * FROM `invoice_data` WHERE `invoice_id` = '" . $r->fields['id'] . "' AND `type` = 'client' LIMIT 1");
    $products = $DB->Execute("SELECT * FROM `invoice_product` WHERE `invoice_id` = '" . $r->fields['id'] . "'");
    
    $faktura = $document->createElement("tns:Faktura");
    $faktura->setAttribute("typ", "G");
    $dRows = [];
    $dRows2 = [];
    $dRows["tns:P_1"] = $r->fields['date_add']; // data wystawienia
    $dRows["tns:P_2A"] = $r->fields['number']; // numer faktury
    $dRows["tns:P_3A"] = $buy->fields['name']; // nazwa nabywcy
    $dRows["tns:P_3B"] = $buy->fields['address']; // adres nabywcy
    $dRows["tns:P_3C"] = $sell->fields['name']; // nazwa sprzedawcy
    $dRows["tns:P_3D"] = $sell->fields['address']; // adres sprzedawcy
    $dRows["tns:P_4A"] = functions::getNipEU($sell->fields['nip']); // kod eu podatnika
    $dRows["tns:P_4B"] = functions::trimNipJPK($sell->fields['nip']); // numer podatnika - NIP?
    $dRows["tns:P_5A"] = functions::getNipEU($buy->fields['nip']); // kod eu nabwycy - podatnika
    $dRows["tns:P_5B"] = functions::trimNipJPK($buy->fields['nip']); // number nabywcy - podatnika EU
    $dRows["tns:P_6"] = $r->fields['date_sell'] != $r->fields['date_add'] ? $r->fields['date_sell'] : ""; // data sprzedaży/zapłacenia jeżeli rózna od wystawienia
    for($i = 16; $i < 22; $i ++)
    {
    	$dRows2["tns:P_" . $i] = "false";
    }
    $dRows2["tns:P_23"] = "false";
    $dRows2["tns:P_106E_2"] = "false";
    $dRows2["tns:P_106E_3"] = "false";

    $vatSums = [
			0 => 0,
			5 => 0,
			7 => 0,
			8 => 0,
			22 => 0,
			23 => 0
		];
		$nettoSums = [
			0 => 0,
			5 => 0,
			7 => 0,
			8 => 0,
			22 => 0,
			23 => 0,
			-1 => 0, // zw
			-2 => 0, // oo
			-3 => 0 // np
		];
  
    while(!$products->EOF)
		{
			$fakturaWiersz = $document->createElement("tns:FakturaWiersz");
			$fakturaWiersz->setAttribute("typ", "G");
			$pRows = [];
			$pRows["tns:P_2B"] = $r->fields['number']; // numer faktury
			$pRows["tns:P_7"] = $products->fields['name']; // nazwa
			$pRows["tns:P_8A"] = $products->fields['unit']; // jednostka
			$pRows["tns:P_8B"] = $products->fields['amount']; // ilość
      
      $nettoR = number_format($products->fields['netto']*(100-$products->fields['rabat'])/100,4,".","");
      $vatR = number_format($nettoR*$products->fields['vat']/100,4,".","");
      $nettoAll = number_format($nettoR * $products->fields['amount'], 2, ".", "");
      $vatAll = number_format($nettoAll*$products->fields['vat']/100,2,".","");
      
			$pRows["tns:P_9A"] = number_format($nettoR, 2, ".", ""); // netto jednostkowe po rabacie
			$pRows["tns:P_9B"] = number_format($nettoR + $vatR, 2, ".", ""); // brutto jednostkowe po rabacie
			$pRows["tns:P_11"] = $nettoAll; // netto całość
			$pRows["tns:P_11A"] = number_format($nettoAll + $vatAll, 2, ".", ""); // brutto całość
			
			$sumNettoWiersze += $nettoAll;
			if($products->fields['vat'] == "ZW")
			{
				$nettoSums[-1] += $nettoAll;
				$dRows2["tns:P_19"] = "true";
				$pRows["tns:P_12"] = "zw"; // stawka VAT
			}
			else if($products->fields['vat'] == "OO")
			{
				$nettoSums[-2] += $nettoAll;
				$dRows2["tns:P_18"] = "true";
			}
			else if($products->fields['vat'] == "NP")
			{
				$nettoSums[-3] += $nettoAll;
			}
			else
			{
				$vatValue = (int)$products->fields['vat'];
				$nettoSums[$vatValue] += $nettoAll;
				$vatSums[$vatValue] += $vatAll;
				$pRows["tns:P_12"] = $products->fields['vat']; // stawka VAT
			}
			
			foreach($pRows as $key => $value)
			{
				if(strlen($value))
				{
					$pRow = $document->createElement($key, $value);
					$fakturaWiersz->appendChild($pRow);
				}
			}
			$wiersze[] = $fakturaWiersz;
			$products->MoveNext();
		}
  
    $cRows = [];
		$cRows["tns:P_13_1"] = $nettoSums[22] + $nettoSums[23]; // netto 22% i 23%
		$cRows["tns:P_14_1"] = $vatSums[22] + $vatSums[23]; // podatek 22% i 23%
		$cRows["tns:P_13_2"] = $nettoSums[7] + $nettoSums[8]; // netto 7% i 8%
		$cRows["tns:P_14_2"] = $vatSums[7] + $vatSums[8]; // podatek 7% i 8%
		$cRows["tns:P_13_3"] = $nettoSums[5]; // netto 5%
		$cRows["tns:P_14_3"] = $vatSums[5]; // podatek 5%
		$cRows["tns:P_13_4"] = $nettoSums[5]; // netto OO
		$cRows["tns:P_13_4"] = $nettoSums[-2]; // netto OO
		$cRows["tns:P_13_5"] = 0; // podatek NP
		$cRows["tns:P_13_5"] = $nettoSums[-3]; // netto NP
		$cRows["tns:P_13_6"] = 0; // podatek 0%
		$cRows["tns:P_13_7"] = $nettoSums[-1]; // netto ZW
		$sumNetto = array_sum($nettoSums);
		$sumVat = array_sum($vatSums);
		$cRows["tns:P_15"] = number_format($sumNetto + $sumVat, 2, ".", ""); // suma brutto
  
    $sRows = [];
		foreach($dRows as $key => $value)
		{
			if(strlen($value))
			{
				$dRow = $document->createElement($key);
				$dRow->appendChild($document->createTextNode($value));
				$faktura->appendChild($dRow);
			}
		}
		foreach($cRows as $key => $value)
		{
			if($value)
			{
				$cRow = $document->createElement($key);
				$cRow->appendChild($document->createTextNode($value));
				$faktura->appendChild($cRow);
			}
		}
		foreach($dRows2 as $key => $value)
		{
			if(strlen($value))
			{
				$dRow = $document->createElement($key);
				$dRow->appendChild($document->createTextNode($value));
				$faktura->appendChild($dRow);
			}
		}
		$dRow = $document->createElement("tns:RodzajFaktury", $isVat ? "VAT" : "ZAL");
		$faktura->appendChild($dRow);
    if(!$isVat)
		{
			$dRow = $document->createElement("tns:ZALZaplata", number_format($sumNetto + $sumVat, 2, ".", ""));
			$faktura->appendChild($dRow);
			$dRow = $document->createElement("tns:ZALPodatek", number_format($sumVat, 2, ".", ""));
			$faktura->appendChild($dRow);
		}
  
    $faktury[] = $faktura;
    $sumBruttoFaktura += $sumNetto + $sumVat;
  }	
}

class jpkDoc
{
  public $id;
  public $date_begin;
  public $date_end;
  public $date_create;
  public $kind;
  public $destination;
  public $is_valid;
  public $xml_code;
  public $errors;
  public $is_send;  
  public $send_info;
  
  public function __construct($data)
  {
    $t = ["id", "date_begin", "date_end", "date_create", "kind", "destination", "is_valid", "xml_code", "errors", "is_send"];
    foreach($t as $i)
    {
      $this->$i = $data[$i];
    }
    $this->send_info = $data['send_info'] ? json_decode($data['send_info'], true) : [];
  }
  
}