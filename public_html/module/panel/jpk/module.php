<?php
global $smarty;
require(ROOT_DIR."module/panel/jpk/class.php");
require(ROOT_DIR."module/panel/jpk/".LANG.".php");
global $navi;

$smarty->assign("navi",$navi->navi);
global $lang;
$smarty->assign("lang",$lang);
//odczyt danych użytkownika
$smarty->assign("user_id",USER_ID);

if($_GET['par3'] == "add")
{
    $smarty->assign("dateBegin", date("Y-m-01")); 
    $smarty->assign("dateEnd", date("Y-m-t"));
    $smarty->assign("action", "add");
}
else if($_GET['par3'] == "edit")
{
    $jpk = new jpk();
    $data = $jpk->getId($_GET['par4']);
    $smarty->assign("data", $data);
    $smarty->assign("action", "edit");
}
else
{
    $jpk = new jpk();
    list($data, $page, $last) = $jpk->getList($_GET['par4'], $_GET['par5']);
    $smarty->assign("data", $data);
  	$smarty->assign("page", $page);
  	$smarty->assign("last", $last);
  	$smarty->assign("count", $_GET['par5']);
    
    $smarty->assign("action", "list");
}


$smarty->display("panel/jpk/".LAYOUT.".html");
?>