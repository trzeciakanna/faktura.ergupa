<?php
/**
 * Obsługa raportów
 * @author kordan
 *
 */
class raport
{	
	public function stat($id)
  {
    global $DB;
    $dane=array();
    $r=$DB->Execute("SELECT COUNT(*) AS ile FROM invoice WHERE user_id='".$id."'");
    $dane['invoice']=$r->fields['ile']; 
    $r=$DB->Execute("SELECT COUNT(*) AS ile FROM clients WHERE user_id='".$id."'");
    $dane['client']=$r->fields['ile'];
    $r=$DB->Execute("SELECT COUNT(*) AS ile FROM product WHERE user_id='".$id."'");
    $dane['product']=$r->fields['ile'];
    return $dane;
  }
  
  public function years($id)
  {
    global $DB;
    $dane=array();
    $r=$DB->Execute("SELECT DISTINCT(DATE_FORMAT(date_sell,'%Y')) AS year FROM invoice WHERE user_id='".$id."' ORDER BY year DESC");
    if($r->fields['year'])
    {
      while(!$r->EOF)
      {
        $dane[]=$r->fields['year'];
        $r->MoveNext();
      }
    }
    return $dane;
  }
  
  public function sum($id,$year)
  {
    global $DB;
    $dane=array();
    $ids=array();
    for($a=0;$a<13;$a++) { $dane[$a]=array("netto"=>0, "brutto"=>0, "vat"=>0, "ile"=>0); $ids[$a]=array(); }
    $r=$DB->Execute("SELECT id, DATE_FORMAT(date_create,'%m') AS month FROM invoice WHERE user_id='".$id."' AND DATE_FORMAT(date_create,'%Y')='".$year."'");
    if($r->fields['id'])
    {
      while(!$r->EOF)
      {
        $m=(int)$r->fields['month']-1;
        $ids[$m][]=$r->fields['id'];
        $r->MoveNext();
      }
    }
    for($a=0;$a<12;$a++)
    {
      if(count($ids[$a])>0)
      {
        //$r=$DB->Execute("SELECT SUM(netto*amount) AS sn, SUM(netto*amount*vat/100) as sv, SUM(netto*amount*(1+vat/100)) as sb  FROM invoice_product WHERE invoice_id in (".implode(",",$ids[$a]).")");  
        $r=$DB->Execute("SELECT SUM(netto*amount) AS sn, SUM(netto*amount*CAST(CAST(vat AS CHAR) AS SIGNED)/100) as sv  FROM invoice_product WHERE invoice_id in (".implode(",",$ids[$a]).")");  
        //$dane[$a]=array("netto"=>$r->fields['sn'], "brutto"=>$r->fields['sb'], "vat"=>$r->fields['sv'], "ile"=>count($ids[$a]));
        $dane[$a]=array("netto"=>$r->fields['sn'], "brutto"=>$r->fields['sn']+$r->fields['sv'], "vat"=>$r->fields['sv'], "ile"=>count($ids[$a]));
        $dane[12]['netto']+=$dane[$a]['netto'];
        $dane[12]['brutto']+=$dane[$a]['brutto'];
        $dane[12]['vat']+=$dane[$a]['vat'];
        $dane[12]['ile']+=$dane[$a]['ile'];
      }
    }
    return $dane;
  }

  public function sumy($id)
  {
    global $DB;
    $dane=array();
    $ids=array();$sn=0;$sb=0;$sv=0;$si=0;
    $r=$DB->Execute("SELECT id, DATE_FORMAT(date_create,'%Y') AS year FROM invoice WHERE user_id='".$id."' ORDER BY year ASC");
    if($r->fields['id'])
    {
      while(!$r->EOF)
      {
        $m=$r->fields['year'];
        $ids[$m][]=$r->fields['id'];
        $r->MoveNext();
      }
    }
    
    foreach($ids as $k=>$id)
    {
      if(count($id)>0)
      {
        //$r=$DB->Execute("SELECT SUM(netto*amount) AS sn, SUM(netto*amount*vat/100) as sv, SUM(netto*amount*(1+vat/100)) as sb  FROM invoice_product WHERE invoice_id in (".implode(",",$id).")");  
        $r=$DB->Execute("SELECT SUM(netto*amount) AS sn, SUM(netto*amount*CAST(CAST(vat AS CHAR) AS SIGNED)/100) as sv  FROM invoice_product WHERE invoice_id in (".implode(",",$id).")");  
        //$dane[]=array("year"=>$k,"netto"=>$r->fields['sn'], "brutto"=>$r->fields['sb'], "vat"=>$r->fields['sv'], "ile"=>count($id));
        $dane[]=array("year"=>$k,"netto"=>$r->fields['sn'], "brutto"=>$r->fields['sn']+$r->fields['sv'], "vat"=>$r->fields['sv'], "ile"=>count($id));
        $sn+=$r->fields['sn'];
        $sb+=$r->fields['sn']+$r->fields['sv'];
        $sv+=$r->fields['sv'];
        $si+=count($id);
      }
    }
    $dane[]=array("year"=>"Razem:","netto"=>$sn, "brutto"=>$sb, "vat"=>$sv, "ile"=>$si);
    return $dane;
  }  
  	
}
?>