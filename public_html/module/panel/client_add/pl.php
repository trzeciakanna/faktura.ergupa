<?php
$lang=array();
$lang['head']="Dodaj kontrahenta";
$lang['mail']="E-mail do wysyłki faktur";
$lang['sign']="Nazwa skrócona";
$lang['name']="Imię i nazwisko/ <br>nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['phone']="Telefon";
$lang['email']="Adres e-mail";
$lang['bank_name']="Nazwa banku";
$lang['krs']="Numer KRS";
$lang['account']="Numer konta";
$lang['cancel']="Anuluj";
$lang['add']="Dodaj nowego kontrahenta";    
$lang['list']="Lista kontrahentów";
$lang['add_ok']="Kontrahent został dodany poprawnie";
$lang['add_exist']="Kontrahent o podanym numerze NIP już istnieje";
$lang['add_error']="Wystąpił błąd podczas dodawania kontrahenta, spróbuj ponownie za chwilę";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_krs']="Podany numer KRS jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_regon']="Podany numer REGON jest nieprawidłowy, wprowadź poprawny numer.";

$lang['nagl1'] = 'Faktura VAT';
$lang['nagl2'] = 'Jesteś w';
?>