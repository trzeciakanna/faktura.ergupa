panel_client_add_obj = {	
	/**
	 * Dodanie kontrahenta
	 */
	submit : function() 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"ok");
			info('',"error");
			info('',"exist");
			if(this.responseText=="ok")
			{ info($('add_ok').value,"ok"); $('client_add_form').reset(); }
			else if(this.responseText=="exist")
			{ info($('add_exist').value,"exist"); }
			else
			{ info($('add_error').value,"error"); }
		};
		req.SendForm("client_add_form","module/panel/client_add/ajax_add.php?id="+$("user_id").value,"post");
		return false;
	}
};