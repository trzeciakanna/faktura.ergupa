<?php
/**
 * Obsługa kontrahentów
 * @author kordan
 *
 */
class clients
{	
	/**
	 * Dodanie kontrahenta
	 * @param int $user_id identyfikator użytkownika
	 * @param array $post dane do dodania
	 * @param boolean $i wstawianie z faktury
	 * @return string wynik wykonania
	 */
	public function add($user_id, $post, $i=false)
	{
		if(trim($post['name']))
		{
			global $DB;
			$r=$DB->Execute("SELECT `id` FROM `clients` WHERE ((`nip`='".$post['nip']."' AND `nip`!='') OR `name`='".$post['name']."') AND `user_id`='".$user_id."' LIMIT 0,1");
			if($r->fields['id']) { return "exist"; }
			global $search;
			$tmp=array();
			$tmp['user_id']=$user_id;
			$tmp['mail']=$post['mail'];
			$tmp['sign']=$post['sign'];
			$tmp['name']=$post['name'];
			$tmp['address']=$post['address'];
			$tmp['sign_s']=$search->parse($post['sign'],1);
			$tmp['name_s']=$search->parse($post['name'],1);
			$tmp['address_s']=$search->parse($post['address'],1);
			$tmp['nip']=$post['nip'];
      $tmp['nipc']=functions::trimNip($tmp['nip']);
			$tmp['phone']=$post['phone'];
			$tmp['email']=$post['email'];
			$tmp['bank_name']=$post['bank_name'];
			$tmp['krs']=$post['krs'];
			$tmp['account']=$post['account'];
			$DB->AutoExecute("clients",$tmp,"INSERT");
			return "ok".($i?"|".$DB->Insert_ID():"");
		}
		else { return "error"; }
	}
	/**
	 * Uaktualnienie danych kontrahenta
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kontrahenta
	 * @param array $post dane do zapisu
	 * @return string wynik wykonania
	 */
	public function save($user_id, $id, $post)
	{
		if(trim($post['name']))
		{
			global $DB;
			global $search;
			$tmp=array();
			$tmp['mail']=$post['mail'];
			$tmp['sign']=$post['sign'];
			$tmp['name']=$post['name'];
			$tmp['address']=$post['address'];
			$tmp['sign_s']=$search->parse($post['sign'],1);
			$tmp['name_s']=$search->parse($post['name'],1);
			$tmp['address_s']=$search->parse($post['address'],1);
			$tmp['nip']=$post['nip'];
      $tmp['nipc']=functions::trimNip($tmp['nip']);
			$tmp['phone']=$post['phone'];
			$tmp['email']=$post['email'];
			$tmp['bank_name']=$post['bank_name'];
			$tmp['krs']=$post['krs'];
			$tmp['account']=$post['account'];
			$DB->AutoExecute("clients",$tmp,"UPDATE","`user_id`='".$user_id."' AND `id`='".$id."'");
			return "ok";
		}
		else { return "error"; }
	}
	/**
	 * Usunięcie kontrahenta
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kontrahenta
	 */
	public function del($user_id, $id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `clients` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `branches` WHERE `client_id`='".$user_id."'");
		$DB->Execute("OPTIMIZE TABLE `branches`,`clients`");
	}
	/**
	 * Dane kontrahenta
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kontrahenta
	 * @return mixed obiekt kontrahenta
	 */
	public function read_one($user_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT * FROM `clients` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 0,1");
		return new client($r->fields['id'],$r->fields['mail'],$r->fields['sign'],$r->fields['name'],$r->fields['address'],$r->fields['nip'],$r->fields['phone'],$r->fields['email'],$r->fields['bank_name'],$r->fields['krs'],$r->fields['account']);
	}
	/**
	 * Lista kontrahentów
	 * @param int $user_id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $letter pierwsza litera sygnatury
	 * @return array tablica z danymi
	 */
	public function read_list($user_id, $page=1, $ile=20, $letter="")
	{
		global $DB;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		$war=$letter?"AND `sign` LIKE '".$letter."%'":"";
		$r=$DB->PageExecute("SELECT `id`,`name`,`sign`,`mail` FROM `clients` WHERE `user_id`='".$user_id."' ".$war." ORDER BY `name` ASC",$ile,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new client($r->fields['id'],$r->fields['mail'],$r->fields['sign']." ",$r->fields['name']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Lista kontrahentów - wyszukiwanie
	 * @param int $user_id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $name fraza nazwy
	 * @param string $address fraza adresu
	 * @param string $nip fraza numeru NIP
	 * @return array tablica z danymi 
	 */
	public function read_list_search($user_id, $page=1, $ile=20, $name=null, $address=null, $nip=null)
	{
		global $DB;
		global $search;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		//warunki
		if($name)
		{
			list($and, $not)=$search->phrase($name);
			$war_name=" AND (search_engine(name_s,'".$and."','".$not."') OR search_engine(sign_s,'".$and."','".$not."'))";
		}
		else { $war_name=""; }
		if($address)
		{
			list($and, $not)=$search->phrase($address);
			$war_address=" AND search_engine(address_s,'".$and."','".$not."')";
		}
		else { $war_address=""; }
		$war_nip=$nip?" AND `nipc` LIKE '".functions::trimNip($nip)."%'":"";
		$r=$DB->PageExecute("SELECT `id`,`name`,`sign`,`mail` FROM `clients` WHERE `user_id`='".$user_id."'".$war_name.$war_address.$war_nip." ORDER BY `name` ASC",$ile,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new client($r->fields['id'],$r->fields['mail'],$r->fields['sign']." ",$r->fields['name']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Litery od których zaczynają się sygnatury kontrahentów
	 * @param int $user_id identyfikator użytkownika
	 * @return array tablica liter
	 */
	public function letter_list($user_id)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(sign),1,1) as `letter` FROM `clients` WHERE `user_id`='".$user_id."' AND `sign`!='' GROUP BY SUBSTRING(sign,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Dodanie wiadomości do wysłania
	 * @param int $user_id identyfikator użytkownika
	 * @param string $topic tytuł wiadomości
	 * @param string $text treść wiadomości
	 * @param array $clients lista klientów
	 * @return string wynik operacji
	 */
	public function send_msg($user_id, $topic, $text, $clients)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['user_id']=$user_id;
		$tmp['topic']=$topic;
		$tmp['text']=$text;
		$tmp['topic_s']=$search->parse($topic,1);
		$tmp['text_s']=$search->parse($text,1);
		$tmp['date']=date("Y-m-d H:i:s");
		$DB->AutoExecute("clients_msg",$tmp,"INSERT");
		$id=$DB->Insert_ID();
		foreach($clients as $key=>$val) { $clients[$key]=array($val); } 
		$DB->Execute("INSERT INTO `clients_send` (msg_id, client_id) VALUES ('".$id."',?)",$clients);
		return "ok";
	}
	/**
	 * Lista wysłanych wiadomości
	 * @param int $user_id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $letter pierwsza litera tematu
	 * @return array tablica z danymi
	 */
	public function send_list($user_id, $page=1, $ile=20, $letter="")
	{
		global $DB;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		$war=$letter?"AND `topic` LIKE '".$letter."%'":"";
		$r=$DB->PageExecute("SELECT `id`,`topic`,`date` FROM `clients_msg` WHERE `user_id`='".$user_id."' ".$war." ORDER BY `date` DESC",$ile,$page);
		if($r->fields['topic'])
		{
			while(!$r->EOF)
			{
				$dane[]=new client_msg($r->fields['id'],$r->fields['topic'],$r->fields['date']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Litery od których zaczynają się tytuły wiadomości
	 * @param int $user_id identyfikator użytkownika
	 * @return array tablica liter
	 */
	public function send_letter($user_id)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(topic),1,1) as `letter` FROM `clients_msg` GROUP BY SUBSTRING(topic,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Odczyt wiadomości
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator wiadomości
	 * @return array tablica z obiektem wiadomości i kontrahentami
	 */
	public function send_one($user_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `topic`,`text`,`date` FROM `clients_msg` WHERE `id`='".(int)$id."' AND `user_id`='".$user_id."' LIMIT 0,1");
		$msg=new client_msg($id, $r->fields['topic'], $r->fields['date'], $r->fields['text']);
		$client=array();
		$r=$DB->Execute("SELECT clients.id, clients.name FROM clients INNER JOIN clients_send ON clients.id=clients_send.client_id WHERE clients_send.msg_id='".(int)$id."' ORDER BY clients.name ASC");
		while(!$r->EOF)
		{
			$client[]=$r->fields['id'];
			$r->MoveNext();
		}
		return array($msg, $client);
	}
	
	
	
	/**
	 * Dodanie oddziału
	 * @param int $id identyfikator kontrahenta
	 * @param array $post dane do dodania
	 * @param boolean $i wstawianie z faktury
	 * @return string wynik wykonania
	 */
	public function branch_add($id, $post, $i=false)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['client_id']=$id;
		$tmp['sign']=$post['sign'];
		$tmp['name']=$post['name'];
		$tmp['address']=$post['address'];
		$tmp['sign_s']=$search->parse($post['sign'],1);
		$tmp['name_s']=$search->parse($post['name'],1);
		$tmp['address_s']=$search->parse($post['address'],1);
		$tmp['nip']=$post['nip'];
		$tmp['phone']=$post['phone'];
		$tmp['email']=$post['email'];
		$tmp['bank_name']=$post['bank_name'];
		$tmp['krs']=$post['krs'];
		$tmp['account']=$post['account'];
		$DB->AutoExecute("branches",$tmp,"INSERT");
		return "ok".($i?"|".$DB->Insert_ID():"");
	}
	/**
	 * Uaktualnienie danych oddziału
	 * @param int $c_id identyfikator kontrahenta
	 * @param int $id identyfikator oddziału
	 * @param array $post dane do zapisu
	 * @return string wynik wykonania
	 */
	public function branch_save($c_id, $id, $post)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['sign']=$post['sign'];
		$tmp['name']=$post['name'];
		$tmp['address']=$post['address'];
		$tmp['sign_s']=$search->parse($post['sign'],1);
		$tmp['name_s']=$search->parse($post['name'],1);
		$tmp['address_s']=$search->parse($post['address'],1);
		$tmp['phone']=$post['phone'];
		$tmp['email']=$post['email'];
		$tmp['bank_name']=$post['bank_name'];
		$tmp['krs']=$post['krs'];
		$tmp['account']=$post['account'];
		$DB->AutoExecute("branches",$tmp,"UPDATE","`client_id`='".$c_id."' AND `id`='".$id."'");
		return "ok";
	}
	/**
	 * Usunięcie oddziału
	 * @param int $c_id identyfikator kontrahenta
	 * @param int $id identyfikator oddziału
	 */
	public function branch_del($c_id, $id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `branches` WHERE `client_id`='".$c_id."' AND `id`='".$id."' LIMIT 1");
	}
	/**
	 * Dane oddziału
	 * @param int $c_id identyfikator kontrahenta
	 * @param int $id identyfikator oddziału
	 * @return mixed obiekt kontrahenta
	 */
	public function branch_one($c_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT * FROM `branches` WHERE `client_id`='".$c_id."' AND `id`='".$id."' LIMIT 0,1");
		return new branch($r->fields['id'],$r->fields['sign'],$r->fields['name'],$r->fields['address'],$r->fields['phone'],$r->fields['email'],$r->fields['bank_name'],$r->fields['krs'],$r->fields['account']);
	}
	/**
	 * Lista oddziałów
	 * @param int $c_id identyfikator kontrahenta
	 * @return array tablica z danymi
	 */
	public function branch_list($c_id)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT `id`,`name`,`sign` FROM `branches` WHERE `client_id`='".$c_id."' ORDER BY `name` ASC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new branch($r->fields['id'],$r->fields['sign'],$r->fields['name']);
				$r->MoveNext();
			}
		}
		return $dane;
	}
}
/**
 * Obiekt kontrahenta
 * @author kordan
 *
 */
class client
{
	public $id;
	public $mail;
	public $sign;
	public $name;
	public $address;
	public $nip;
	public $phone;
	public $email;
	public $bank_name;
	public $krs;
	public $account;
	/**
	 * Konstruktor kontrahenta
	 * @param int $id identyfikator kontrahenta
	 * @param string $sign nazwa skrócona kontrahenta
	 * @param string $name nazwa kontrahenta
	 * @param string $address adres kontrahenta
	 * @param string $nip numer NIP kontrahenta
	 * @param string $phone numery telefonów kontrahenta
	 * @param string $email adresy email kontrahenta
	 * @param string $bank_name nazwa banku
	 * @param string $krs numer KRS kontrahenta
	 * @param string $account numer konta kontrahenta
	 */
	public function __construct($id,$mail,$sign,$name,$address=null,$nip=null,$phone=null,$email=null,$bank_name=null,$krs=null,$account=null)
	{
		global $func;
		$this->id=$id;
		$this->mail=$func->show_with_html($mail);
		$this->sign=$func->show_with_html($sign);
		$this->name=$func->show_with_html($name);
		$this->address=$func->show_with_html($address);
		$this->nip=$nip;
		$this->phone=$func->show_with_html($phone);
		$this->email=$func->show_with_html($email);
		$this->bank_name=$bank_name;
		$this->krs=$krs;
		$this->account=$account;
	}	
}
/**
 * Wiadomość dla kontrahentów
 * @author kordan
 *
 */
class client_msg
{
	public $id;
	public $topic;
	public $text;
	public $date;
	/**
	 * Konstruktor wiadomości
	 * @param int $id identyfikator wiadomości
	 * @param string $topic temat wiadomości
	 * @param datetime $date data wysłania
	 * @param string $text treść wiadomości
	 */
	public function __construct($id,$topic,$date,$text=null)
	{
		global $func;
		$this->id=$id;
		$this->topic=$func->show_with_html($topic);
		$this->date=$date;
		$this->text=$func->show_with_html($text);
	}
}
/**
 * Obiekt oddziału
 * @author kordan
 *
 */
class branch
{
	public $id;
	public $sign;
	public $name;
	public $address;
	public $phone;
	public $email;
	public $bank_name;
	public $krs;
	public $account;
	/**
	 * Konstruktor oddziału
	 * @param int $id identyfikator kontrahenta
	 * @param string $sign nazwa skrócona kontrahenta
	 * @param string $name nazwa kontrahenta
	 * @param string $address adres kontrahenta
	 * @param string $phone numery telefonów kontrahenta
	 * @param string $email adresy email kontrahenta
	 * @param string $bank_name nazwa banku
	 * @param string $krs numer KRS kontrahenta
	 * @param string $account numer konta kontrahenta
	 */
	public function __construct($id,$sign,$name,$address=null,$phone=null,$email=null,$bank_name=null,$krs=null,$account=null)
	{
		global $func;
		$this->id=$id;
		$this->sign=$func->show_with_html($sign);
		$this->name=$func->show_with_html($name);
		$this->address=$func->show_with_html($address);
		$this->phone=$func->show_with_html($phone);
		$this->email=$func->show_with_html($email);
		$this->bank_name=$bank_name;
		$this->krs=$krs;
		$this->account=$account;
	}	
}
?>