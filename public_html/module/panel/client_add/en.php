<?php
$lang=array();
$lang['head']="Add a Customer";
$lang['mail']="Email Address for sending";
$lang['sign']="Name (short)";
$lang['name']="Full Name/Company Name";
$lang['address']="Address";
$lang['nip']="";
$lang['phone']="Telephone";
$lang['email']="Email Address";
$lang['bank_name']="Bank Name";
$lang['krs']="Companies House #";
$lang['account']="Account No.";
$lang['cancel']="Cancel";
$lang['add']="Add";    
$lang['list']="Customer List";
$lang['add_ok']="Customer has been added correctly";
$lang['add_exist']="Customer with this tax id number already exists";
$lang['add_error']="An error occurred while adding a customer. Please try again later";
$lang['bad_nip']="The given Tax ID Number is incorrect, please input the correct number.";
$lang['bad_krs']="The given Companies House Number is incorrect, please input the correct number.";
$lang['bad_regon']="The given REGON Number is incorrect, please input the correct number.";

$lang['nagl1'] = 'VAT Invoice';
$lang['nagl2'] = 'You are in';
?>