<?php
$lang=array();
$lang['head']="Logo / Nagłówek / Stopka";
$lang['logo']="Logo na fakturę";
$lang['header']="Nagłówek faktury";
$lang['foot']="Stopka faktury";
$lang['info']="Informacja do faktury"; 
$lang['sign']="Podpis";
$lang['cancel']="Anuluj";
$lang['save']="Zapisz";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania, spróbuj ponownie za chwilę";


$lang['ehead']="Dane dodatkowe";
$lang['elogo']="Logo na fakturę";
$lang['eheader']="Nagłówek faktury";
$lang['efoot']="Stopka faktury";
$lang['einfo']="Informacja do faktury";
$lang['nagl1']="Faktura VAT";
$lang['nagl2']="Jesteś w ";
$lang['nagl3']="Logo pokaże się na fakturze w górnym lewym rogu";
$lang['nagl4']="Podpis na fakturze w formie zdjęcia jpg. Zrób zdjęcie swojej pieczątki i podpisu a będzie ono zawsze widoczne na wystawianej fakturze";
$lang['nagl5']="Nagłówek czyli np. nazwa firmy, stosuje się w przypadku gdy nie używasz logo graficznego, pokaże się w lewym rogu.";
$lang['nagl6']="Możesz wprowadzić informacje do stopki faktury";
$lang['nagl7']="Dodatkowe informacje do faktury, np. o godz pracy twojej firmy itp...";
$lang['']="";
?>