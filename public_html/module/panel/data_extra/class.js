panel_data_extra_obj = {	
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function(id) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value); }
			else
			{ info($('save_error').value); }
		};
		req.SendForm("data_extra_save","module/panel/data_extra/ajax_save.php?user_id="+id,"post");
		return false;
	}
};