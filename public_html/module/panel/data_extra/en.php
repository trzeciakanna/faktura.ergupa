<?php
$lang=array();
$lang['head']="Extra Data";
$lang['logo']="Logo for the Invoice";
$lang['header']="Invoice Header";
$lang['foot']="Invoice Footer";
$lang['info']="Information for the Invoice"; 
$lang['sign']="Signature";
$lang['cancel']="Cancel";
$lang['save']="Save";
$lang['save_ok']="Data successfully saved";
$lang['save_error']="An error occurred while saving, please try again later";


$lang['ehead']="Extra Data";
$lang['elogo']="Logo for the Invoice";
$lang['eheader']="Invoice Header";
$lang['efoot']="Invoice Footer";
$lang['einfo']="Information for the Invoice";
$lang['nagl1']="VAT Invoice";
$lang['nagl2']="You are in ";
$lang['nagl3']="The logo will be displayed on the invoice in the top left-hand corner";
$lang['nagl4']="The signature on the invoice in the .JPG picture format. Take a picture of your stamp and signature and it will always appear on your invoices";
$lang['nagl5']="Header - i.e. Company Name. Use it if you do not have a graphic logo. Will be displayed in the left corner.";
$lang['nagl6']="You may input the info for the footer of the invoice";
$lang['nagl7']="Extra information for the invoice, eg. your opening hours, etc.";
$lang['']="";
?>