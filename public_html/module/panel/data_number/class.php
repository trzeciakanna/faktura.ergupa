<?php
/**
 * Zarządzanie danymi dodatkowymi
 * @author kordan
 *
 */
class user_number
{
	public $nr_format;
	public $nr_last;
	public $nr_type;
	public $nr_date;
	public $nr_letter;
	/**
	 * Utworzenie obiektu danych dodatkowych
	 * @param int $id identyfikator użytkownika
	 */
	public function __construct($id=null, $type=1)
	{
		if($id)
		{
		  $type=$type?$type:1;
			global $DB;
			global $func;
			$r=$DB->Execute("SELECT * FROM `users_number` WHERE `user_id`='".$id."' AND `type`='".$type."' LIMIT 0,1");
			$this->nr_format=$func->show_with_html($r->fields['nr_format']);
			$this->nr_last=$func->show_with_html($r->fields['nr_last']);
			$this->nr_type=$func->show_with_html($r->fields['nr_type']);
			$this->nr_date=$func->show_with_html($r->fields['nr_date']);
			$this->nr_letter=$func->show_with_html($r->fields['nr_letter']);
			if(!$this->nr_format) { $this->nr_format="x/m/Y"; }
			if(!$this->nr_type) { $this->nr_type=3; }
			if(!$r->fields['user_id']) { $DB->Execute("INSERT INTO `users_number` SET `user_id`='".$id."', `type`='".$type."', `nr_format`='".$this->nr_format."'"); }
		}
	}

	public function save_nr($id, $type, $nr_format, $nr_type, $day, $month, $year, $nr_letter, $nr_last)
	{
		global $DB;
		$tmp=array();
		$nr_format=str_replace("n","x",$nr_format);
		$nr_format=str_replace("A","X",$nr_format);
		$nr_format=str_replace("d",$day?$day:'d',$nr_format);
		$nr_format=str_replace("m",$month?$month:'m',$nr_format);
		$nr_format=str_replace("y",$year?$year:'Y',$nr_format);
		$tmp['nr_format']=$nr_format;
		$tmp['nr_type']=$nr_type;
		$tmp['nr_letter']=$nr_letter;     
    $tmp['nr_date']=date("Y-m-d");
		if($nr_last) { $tmp['nr_last']=$nr_last; }
		if($type) { $DB->AutoExecute("users_number",$tmp,"UPDATE","`user_id`='".$id."' AND `type`='".$type."'"); }
		else
		{
      for($a=1;$a<7;$a++)
      {
        $r=$DB->Execute("SELECT id FROM users_number WHERE `user_id`='".$id."' AND `type`='".$a."' LIMIT 0,1");
        if($r->fields['id'])
        { $DB->AutoExecute("users_number",$tmp,"UPDATE","`user_id`='".$id."' AND `type`='".$a."'"); }
        else
        { 
          $tmp['user_id']=$id;
          $tmp['type']=$a;
          $DB->AutoExecute("users_number",$tmp,"INSERT");  
        }
      }
    }
    return "ok";
	}
	
	public function get_nr()
	{
    //określenie numeru
    $nr=$this->nr_last+1;
    if($this->nr_type==1 AND date("Y-m-d")>date("Y-m-d",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==2 AND date("W")>date("W",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==3 AND date("Y-m")>date("Y-m",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==4 AND date("Y")>date("Y",strtotime($this->nr_date))) { $nr=1; }
    $fr=date($this->nr_format);
    return str_replace(array("x","X"),array($nr,$this->nr_letter),$fr);
  }
	
	public function up_nr($id, $type)
	{
    global $DB;
    //określenie numeru
    $nr=$this->nr_last+1;
    if($this->nr_type==1 AND date("Y-m-d")>date("Y-m-d",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==2 AND date("W")>date("W",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==3 AND date("Y-m")>date("Y-m",strtotime($this->nr_date))) { $nr=1; }
    elseif($this->nr_type==4 AND date("Y")>date("Y",strtotime($this->nr_date))) { $nr=1; }
    //zaktualizacja
    $tmp=array();
    $tmp['nr_last']=$nr;
    $tmp['nr_date']=date("Y-m-d");
    $DB->AutoExecute("users_number",$tmp,"UPDATE","`user_id`='".$id."' AND `type`='".$type."'");    
  }
  
}
?>