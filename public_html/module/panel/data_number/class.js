panel_data_number_obj = {	
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function(id,type) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value, 'save_ok'); }
			else
			{ info($('save_error').value, 'save_bad'); }
		};
		req.SendForm("data_number_save","module/panel/data_number/ajax_save.php?user_id="+id+"&type="+type,"post");
		return false;
	},
	
	init : function()
	{
    var obj=$('wyszukiwarka_faktur').getElementsByTagName("input");
    for(a=0;a<obj.length;a++)
    {
      if(obj[a].type=="text") { addEvent(obj[a],"keyup",panel_data_number_obj.example); }
      else { addEvent(obj[a],"click",panel_data_number_obj.example); }  
    }
  },
  
  example : function()
  {
    $('save_ok_error').style.display="none";
	$('save_bad_error').style.display="none";
    var date=new Date();
    var year=date.getFullYear();
    if($('year1').checked) { year=year.toString().substring(2,4); }
    var month=date.getMonth()+1;
    if($('month2').checked && month<10) { month="0"+month; }
    var day=date.getDate();
    if($('day2').checked && day<10) { day="0"+day; }
    
    var ex;
    if($('format1').checked) { ex=$('format1').value; $('letter').disabled=true;}
    else if($('format2').checked) { ex=$('format2').value; $('letter').disabled=true;}
    else if($('format3').checked) { ex=$('format3').value; $('letter').disabled=true;}
    else if($('format4').checked) { ex=$('format4').value; $('letter').disabled=true;}
    else if($('format5').checked) { ex=$('format5').value; $('letter').disabled=true;}
    else if($('format6').checked) { ex=$('format6').value; $('letter').disabled=true;}
    else if($('format7').checked) { ex=$('format7').value; $('letter').disabled=false;}
    else if($('format8').checked) { ex=$('format8').value; $('letter').disabled=false;}
  
    ex=ex.replace('d',day).replace('m',month).replace('y',year).replace("A",$('letter').value).replace('n',1);
    $('example').innerHTML=ex;
  },
  
  letter : function(event)
  {
    var r=key_mask(event,'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM',1);
    if(r=='del') { return true; }
    else if($('letter').value.length>11) { return false; }
    else { return r; }
  }
	
};