<?php
$lang=array();
$lang['head']="Category Tree";
$lang['name']="Category Name";
$lang['list']="Category List";
$lang['']="";
$lang['']="";
$lang['']="";

$lang['show']="pull down";
$lang['hidden']="pull up";
$lang['all']="all";

$lang['cancel']="Cancel";
$lang['add']="Add";
$lang['edit']="Edit";
$lang['save']="Save";
$lang['del']="Remove";

$lang['del_confirm']="Do you really want to remove this Category together with its Subcategories?";
$lang['add_ok']="Category successfully added.";
$lang['add_error']="An error occurred while adding the Category. Please try again later.";
$lang['save_ok']="Category successfully saved";
$lang['save_error']="An error occurred while saving the Category. Please try again later.";

$lang['nagl1'] = 'VAT Invoice';
$lang['nagl2'] = 'You are in';
$lang['nagl3'] = 'Add new Category';
$lang['nagl4'] = '';
$lang['nagl5'] = '';
$lang['nagl6'] = '';
$lang['nagl7'] = '';
$lang['nagl8'] = '';
?>