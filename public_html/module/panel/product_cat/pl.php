<?php
$lang=array();
$lang['head']="Drzewo kategorii";
$lang['name']="Nazwa kategorii";
$lang['list']="Lista kategorii";
$lang['']="";
$lang['']="";
$lang['']="";

$lang['show']="rozwiń";
$lang['hidden']="zwiń";
$lang['all']="wszystko";

$lang['cancel']="Anuluj";
$lang['add']="Dodaj";
$lang['edit']="Edytuj";
$lang['save']="Zapisz";
$lang['del']="Usuń";

$lang['del_confirm']="Czy na pewno chcesz usunąć tą kategorię razem z podkategoriami?";
$lang['add_ok']="Kategoria dodana pomyślnie";
$lang['add_error']="Wystąpił błąd podczas dodawania kategorii. Spróbuj ponownie za chwilę.";
$lang['save_ok']="Kategoria zapisana pomyślnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania kategorii. Spróbuj ponownie za chwilę.";

$lang['nagl1'] = 'Faktura VAT';
$lang['nagl2'] = 'Jesteś w';
$lang['nagl3'] = 'Dodaj nową kategorie';
$lang['nagl4'] = '';
$lang['nagl5'] = '';
$lang['nagl6'] = '';
$lang['nagl7'] = '';
$lang['nagl8'] = '';
?>