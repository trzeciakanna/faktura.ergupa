panel_product_cat_obj = {	
	
		
	cancel_add : function() 
	{
		var tmp=parseInt($('cat_id_add').value);
		if(tmp!=0) { $('name_'+tmp).style.position=""; }
		$('cat_id_add').value=0;
		$('cat_add').style.display="none";
		return false;
	},
	
	cancel_edit : function() 
	{
		var tmp=parseInt($('cat_id_edit').value);
		if(tmp!=0) { $('name_'+tmp).style.position=""; }
		$('cat_id_edit').value=0;
		$('cat_edit').style.display="none";
		return false;
	},
	
	show_add : function(id) 
	{
		var tmp=parseInt($('cat_id_add').value);
		if(tmp!=0) { $('name_'+tmp).style.position=""; }
		//$('name_'+id).style.position="relative"; edycja kamil 2010-09-22
		$('cat_id_add').value=id;
		$('name_'+id).appendChild($('cat_add'));
		$('cat_add').style.display="block";
		return false;
	},
	
	show_edit : function(id, value) 
	{
		var tmp=parseInt($('cat_id_edit').value);
		if(tmp!=0) { $('name_'+tmp).style.position=""; }
		//$('name_'+id).style.position="relative"; edycja kamil 2010-09-22
		$('cat_id_edit').value=id;
		$('cat_edit_name').value=value;
		$('name_'+id).appendChild($('cat_edit'));
		$('cat_edit').style.display="block";
		return false;
	},
	
	add : function() 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ alert($('add_ok').value); location.href=location.href; }
			else
			{ info($('add_error').value); }
		};
		req.AddParam("user_id",$("user_id").value);
		req.AddParam("parent",$("cat_id_add").value);
		req.AddParam("name",$("cat_add_name").value);
		req.Send("module/panel/product_cat/ajax_add.php");
		return false;
	},
	
	save : function() 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ alert($('save_ok').value); location.href=location.href; }
			else
			{ info($('save_error').value); }
		};
		req.AddParam("user_id",$("user_id").value);
		req.AddParam("id",$("cat_id_edit").value);
		req.AddParam("name",$("cat_edit_name").value);
		req.Send("module/panel/product_cat/ajax_save.php");
		return false;
	},
	
	del : function(id) 
	{
		if(confirm($('delete').value))
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ alert($('save_ok').value); location.href=location.href; }
				else
				{ info($('save_error').value); }
			};
			req.AddParam("user_id",$("user_id").value);
			req.AddParam("id",id);
			req.Send("module/panel/product_cat/ajax_del.php");
		}
		return false;
	},
	
	tree : function(level, id)
	{
		//sprawdzenie co robic
		if($('tree_'+level+'_'+id).style.display=="none")
		{
			//pokazanie bloku
			$('tree_'+level+'_'+id).style.display="block";
			$('button_'+level+'_'+id).className="minus";
		}
		else
		{
			//ukrycie bloku
			$('tree_'+level+'_'+id).style.display="none";
			$('button_'+level+'_'+id).className="plus";	
		}
		return false;
	},
	
	tree_show : function(level, id)
	{
		var a, tmp;
		var nodes=$('tree_'+level+'_'+id).childNodes;
		for(a=0;a<nodes.length;a++)
		{
			if(nodes[a].id)
			{
				nodes[a].style.display="block";
				tmp=nodes[a].id.split("_");
				$('button_'+tmp[1]+'_'+tmp[2]).className="minus";
				this.tree_show(tmp[1],tmp[2]);
			}
		}
		$('cat_show_all').style.display="none";
		$('cat_hidden_all').style.display="block";
		return false;
	},
	
	tree_hidden : function(level, id)
	{
		var a, tmp;
		var nodes=$('tree_'+level+'_'+id).childNodes;
		for(a=0;a<nodes.length;a++)
		{
			if(nodes[a].id)
			{
				nodes[a].style.display="none";
				tmp=nodes[a].id.split("_");
				$('button_'+tmp[1]+'_'+tmp[2]).className="plus";
				this.tree_hidden(tmp[1],tmp[2]);
			}
		}
		$('cat_show_all').style.display="block";
		$('cat_hidden_all').style.display="none";
		return false;
	}
	
	
};