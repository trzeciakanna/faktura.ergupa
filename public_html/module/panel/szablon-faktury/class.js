function hasClass(ele,cls) {
	return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}
function addClass(ele,cls) {
	if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}
function removeClass(ele,cls) {
	if (hasClass(ele,cls)) {
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg,' ');
	}
}
panel_data_template_obj = {

	/**
	 * Zmiana wybranego kwadracika, zaakutalizowanie input�w z kolorami.
	 */	
	change_color : function (color_id,template_id){
		if(template_id == ''){
			template_id = $('szablon_id').value;
		}
		$('template_big_img').src ='images/layout/szablony/mid/szablon'+template_id+'_'+color_id+'.jpg';
		old_color = 'kolorystyka'+$('kolorystyka').value;
		$('kolorystyka').value = color_id;
		$(old_color).className = 'kolor';
		$('kolorystyka'+color_id).className = 'kolor_active';
		
		if(color_id == 1){ //szara
			$('kolor_tla_nagl').value = 'e0e0e0';
			$('kolor_ramki').value = '000000';
			$('kolor_tekstu').value = '000000';
			$('kolor_tekstu_nagl').value = '000000';
			$('kolor_dodatkowy').value = 'ffffff';
		} else if(color_id == 2) { //zielona
			$('kolor_tla_nagl').value = '52B848';
			$('kolor_ramki').value = 'c9c9c9';
			$('kolor_tekstu').value = '444444';
			$('kolor_tekstu_nagl').value = 'ffffff';
			$('kolor_dodatkowy').value = 'd2ecc9';
		} else if(color_id == 3) { //niebieska 
			$('kolor_tla_nagl').value = '0A40AA';
			$('kolor_ramki').value = '0a40aa'; 
			$('kolor_tekstu').value = '444444';
			$('kolor_tekstu_nagl').value = 'ffffff';
			$('kolor_dodatkowy').value = '84aaf3';
		}	else if(color_id == 4) { //bia�a (drukarka ig�owa) 
			$('kolor_tla_nagl').value = 'ffffff';
			$('kolor_ramki').value = '000000';
			$('kolor_tekstu').value = '000000';
			$('kolor_tekstu_nagl').value = '000000';
			$('kolor_dodatkowy').value = 'ffffff';
		}	
		else if(color_id == 5) { //czerwona
			$('kolor_tla_nagl').value = 'ff0000';
			$('kolor_ramki').value = '555555';
			$('kolor_tekstu').value = '000000';
			$('kolor_tekstu_nagl').value = 'ffffff';
			$('kolor_dodatkowy').value = 'f68181';
		}
	},
	/**
	 * Zmiana wybranego szablonu, zaktualizowanie inputa.
	 */	
	change_template : function (template_id,color_id){
		if(color_id == ''){
			color_id = $('kolorystyka').value;
		} 
		$('szablon_id').value = template_id;
		$('template_big_img').src ='images/layout/szablony/mid/szablon'+template_id+'_'+color_id+'.jpg';
		if(template_id==1)
		{
      $('l1').style.border="1px solid red"; 
      $('l2').style.border="1px solid transparent";
      $('l3').style.border="1px solid transparent";
    }
			if(template_id==2)
		{
      $('l2').style.border="1px solid red"; 
      $('l1').style.border="1px solid transparent";
      $('l3').style.border="1px solid transparent";
    }
			if(template_id==3)
		{
      $('l3').style.border="1px solid red"; 
      $('l2').style.border="1px solid transparent";
      $('l1').style.border="1px solid transparent";
    }

	},
	/**
	 * Zapisz przyk�adowego szablonu
	 */
	save_example : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			location.href="http://"+location.host+"/download.php?file="+this.responseText;
		};
		req.SendForm("szablon","module/panel/szablon-faktury/save_example.php");
		return false;
	},
  /**
	 * Zapisz danych
	 */
	save_data : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
      if(this.responseText=="ok")
			{ info($('save_ok').value, 'save_ok'); }
			else
			{ info($('save_error').value, 'save_bad'); }
		};
		req.SendForm("szablon","module/panel/szablon-faktury/save.php");
		return false;
	}
}