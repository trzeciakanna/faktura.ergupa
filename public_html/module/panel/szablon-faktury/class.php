<?php
/**
 * Zarządzanie danymi dodatkowymi
 * @author kordan
 *
 */
class user_color
{
	public $color;
	public $tpl;
	public $nagl;
	public $colors;
	/**
	 * Utworzenie obiektu danych dodatkowych
	 * @param int $id identyfikator użytkownika
	 */
	public function __construct($id=null, $type=1)
	{
		if($id)
		{
		  $type=$type?$type:1;
			global $DB;
			$r=$DB->Execute("SELECT * FROM `users_colors` WHERE `user_id`='".$id."' AND `type`='".$type."' LIMIT 0,1");
      $this->color=$r->fields['color'];
      $this->tpl=$r->fields['tpl'];  
      $this->nagl=$r->fields['nagl'];
      if(!$this->color) { $this->color=1; }  
      if(!$this->tpl) { $this->tpl=1; } 
			$this->colors=array();
			switch($this->color)
			{
        case 2:
          $this->colors['kolor_tla_nagl']="52B848";
          $this->colors['kolor_ramki']="c9c9c9";
          $this->colors['kolor_tekstu']="444444";
          $this->colors['kolor_tekstu_nagl']="ffffff";   
          $this->colors['kolor_dodatkowy']="d2ecc9";
          $this->colors['pokaz_naglowki']=$this->nagl;
          break;
        case 3:
          $this->colors['kolor_tla_nagl']="0A40AA";
          $this->colors['kolor_ramki']="0a40aa";
          $this->colors['kolor_tekstu']="444444";
          $this->colors['kolor_tekstu_nagl']="ffffff";   
          $this->colors['kolor_dodatkowy']="84aaf3";
          $this->colors['pokaz_naglowki']=$this->nagl;
          break;
		case 4: //biały 
          $this->colors['kolor_tla_nagl']="ffffff";
          $this->colors['kolor_ramki']="000000";
          $this->colors['kolor_tekstu']="000000";
          $this->colors['kolor_tekstu_nagl']="000000";   
          $this->colors['kolor_dodatkowy']="ffffff";
          $this->colors['pokaz_naglowki']=$this->nagl;
          break;
		case 5: //czerwony
          $this->colors['kolor_tla_nagl']="ff0000";
          $this->colors['kolor_ramki']="555555";
          $this->colors['kolor_tekstu']="000000";
          $this->colors['kolor_tekstu_nagl']="ffffff";   
          $this->colors['kolor_dodatkowy']="f68181";
          $this->colors['pokaz_naglowki']=$this->nagl;
          break;  
        default:
          $this->colors['kolor_tla_nagl']="e0e0e0";
          $this->colors['kolor_ramki']="000000";
          $this->colors['kolor_tekstu']="000000";
          $this->colors['kolor_tekstu_nagl']="000000";   
          $this->colors['kolor_dodatkowy']="ffffff";
          $this->colors['pokaz_naglowki']=$this->nagl;
          break;
      }
			if(!$r->fields['user_id']) { $DB->Execute("INSERT INTO `users_colors` SET `user_id`='".$id."', `type`='".$type."'"); }
		}
	}

	public function save($id, $type=1, $color, $tpl, $nagl)
	{
		global $DB;
		$tmp=array();
		$tmp['color']=$color;
		$tmp['tpl']=$tpl;
		$tmp['nagl']=$nagl;
		$DB->AutoExecute("users_colors",$tmp,"UPDATE","`user_id`='".$id."' AND `type`='".$type."'");
    return "ok";
	}  
}
?>