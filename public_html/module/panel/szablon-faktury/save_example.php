<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',6600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/panel/data_extra/class.php");
$func=new functions();
$invoice_type = 1;
$id_szablonu = $_POST['szablon_id'];
switch ($_POST['jaka_faktura']) {
    case 1:
        require(ROOT_DIR.'module/invoice/szablony/class_pdf'.$id_szablonu.'.php');
		$dane['lang_head']             = 'Faktura';
        break;
    case 2:
        require(ROOT_DIR.'module/faktura-pro-forma/szablony/class_pdf'.$id_szablonu.'.php');
		$dane['lang_head']             = 'Faktura Pro Forma';
        break;
    case 3:
        require(ROOT_DIR.'module/faktura-zaliczkowa/szablony/class_pdf'.$id_szablonu.'.php');
		$dane['lang_head']             = 'Faktura Zaliczkowa';
        break;
	case 4:
        require(ROOT_DIR.'module/faktura-korygujaca/szablony/class_pdf'.$id_szablonu.'.php');
		$dane['lang_head']             = 'Faktura Korygująca';
        break;
	case 5:
        require(ROOT_DIR.'module/rachunek/szablony/class_pdf'.$id_szablonu.'.php');
		$dane['lang_head']             = 'Rachunek';
        break;
}


 	/*15
	12
	2011
	
	15
	12
	2011
	
	0
	0
	0
	0
	0
	0
	15
	12
	2011
	
	3 %
	4.00
	0.12
	4.12
	18 %
	5.00
	0.90
	5.90
	23 %
	6.00
	1.38
	7.38
	
	
	
	
	15.00
	2.40
	17.40
	15
	12
	2011 */
	

$dane['branch_account']        = '';	
$dane['calendar_date_kor_data']= '2012-1-5';	
$dane['lang_kor_data']= 'Data wystawienie faktury kor.';	
$dane['lang_kor_miejsce']= 'Miejsce wystawienia faktury kor.';	
$dane['kor_miejsce']= 'Grodzisk Mazowiecki';	
$dane['coment']= 'Komentarz do faktury';	



$dane['lang_kor_dotyczy']= 'Korekta dotyczy faktury nr';
$dane['kor_dotyczy']= '1/2011';
	
$dane['lang_kor_wystawionej']= 'wystawionej';	
$dane['kor_wystawionej']= 'Grodzisk Mazowiecki';	

$dane['branch_address']        = 'ul. Wielicka 782, 54-789 Kraków';
$dane['branch_bank_name']      = '';
$dane['branch_krs']            = '';	
$dane['branch_mail']           = 'krakow@poczta.zxc';
$dane['branch_name']           = 'oddział w Krakowie';
$dane['branch_phone']          = '(78) 789-456-65';
$dane['branch_v']              = '1';
$dane['cash_date']             = 'list';
$dane['cash_date_date']        = '2011-12-15';
$dane['cash_date_list']        = '7 dni';
$dane['cash_pay']              = '0.00 PLN';
$dane['cash_sum']              = '17.40 PLN';
$dane['cash_type']             = 'przelew';
$dane['cash_word']             = 'siedemnaście PLN 40/100';
$dane['client_account']  	   = 'PL78 9456 1231 2312 3132 1214 5645';
$dane['client_address']        = 'Ul. Królewska 18a 83-330 Żukowo';
$dane['client_bank_name']      = 'Bank Szwajcarski';
$dane['client_id']             = '1';
$dane['client_mail']           = 'adresmail@poczta.pl';
$dane['client_name']           = 'Arkadiusz Czarny';
$dane['client_nip']            = '78945612313';
$dane['client_phone']          = '(12) 454-561-65';
$dane['coment']                = '';
$dane['current']               = 'PLN';
$dane['date_create']           = '2011-12-15';
$dane['date_sell']             = '2011-12-15';
$dane['lang']                  = '';
$dane['lang_all_brutto']       = 'Brutto';
$dane['lang_all_netto']        = 'Netto';
$dane['lang_all_rate']         = 'Stawka VAT';
$dane['lang_all_sum']          = 'Razem:';
$dane['lang_all_vat']          = 'VAT';
$dane['lang_amount']           = 'Ilość';
$dane['lang_branch']           = 'Adres dostawy';
$dane['lang_branch_account']   = 'KONTO:';
$dane['lang_branch_address']   = 'Adres:';
$dane['lang_branch_bank_name'] = 'Nazwa banku:';
$dane['lang_branch_krs']       = 'KRS:';
$dane['lang_branch_mail']      = 'E-mail:';
$dane['lang_branch_name']      = 'Oddział:';
$dane['lang_branch_phone']     = 'Telefon:';
$dane['lang_cash_date']        = 'Termin zapłaty:';
$dane['lang_cash_pay']         = 'Zapłacono:';
$dane['lang_cash_sum']         = 'Do zapłaty:';
$dane['lang_cash_type']        = 'Sposób zapłaty:';
$dane['lang_cash_word']        = 'Kwota słownie:';
$dane['lang_client_account']   = 'KONTO:';
$dane['lang_client_address']   = 'Adres:';
$dane['lang_client_bank_name'] = 'Nazwa banku:';
$dane['lang_client_mail']      = 'E-mail:';
$dane['lang_client_name']      = 'Nazwa/Firma:';
$dane['lang_client_name_main'] = 'Nabywca:';
$dane['lang_client_nip']       = 'NIP:';
$dane['lang_client_phone']     = 'Telefon:';
$dane['lang_client_sign']      = 'Osoba upoważniona do odbioru';
$dane['lang_coment']           = 'Informacje dodatkowe/komentarz';
$dane['lang_copy']             = 'KOPIA';
$dane['lang_date_create']      = 'Data wystawienia:';
$dane['lang_date_sell']        = 'Data sprzedaży:';
$dane['lang_double']           = 'DUPLIKAT';

$dane['lang_lp']               = 'Lp.';
$dane['lang_name']             = 'Nazwa towaru/usługi';
$dane['lang_netto']            = 'Cena netto';
$dane['lang_number']           = 'nr:';
$dane['lang_original']         = 'ORYGINAŁ';
$dane['lang_pkwiu']            = 'PKWiU';
$dane['lang_place']            = 'Miejsce wystawienia:';
$dane['lang_prize_netto']      = 'Cena netto';
$dane['lang_rabat']            = 'Rabat%';
$dane['lang_sum_brutto']       = 'Kwota brutto';
$dane['lang_sum_netto']        = 'Kwota netto';
$dane['lang_sum_vat']          = 'Kwota VAT';
$dane['lang_unit']             = 'Jm';
$dane['lang_user_account']     = 'KONTO:';
$dane['lang_user_address']     = 'Adres:';
$dane['lang_user_bank_name']   = 'Nazwa banku:';
$dane['lang_user_mail']        = 'E-mail:';
$dane['lang_user_name']        = 'Nazwa/Firma:';
$dane['lang_user_name_main']   = 'Sprzedawca:';
$dane['lang_user_nip']         = 'NIP:';
$dane['lang_user_phone']       = 'Telefon:';
$dane['lang_user_sign']        = 'Osoba upoważniona do wystawienia';
$dane['lang_vat']              = 'VAT';
$dane['number']                = '1/12/2011';
$dane['original_v']            = '0';
$dane['p_1_amount']            = '88';
$dane['p_1_lp']            = '1.';
$dane['p_1_name']            = 'Morbi condimentum eleifend';
$dane['p_1_netto']            = '3.0420';
$dane['p_1_pkwiu']            = '';
$dane['p_1_pnetto']            = '3.04';
$dane['p_1_rabat']            = '0.00';
$dane['p_1_sbrutto']            = '267.70';
$dane['p_1_snetto']            = '267.70';
$dane['p_1_svat']            = '0.00';
$dane['p_1_unit']            = 'doba';
$dane['p_1_vat']            = '0';
$dane['p_2_amount']            = '7';
$dane['p_2_lp']            = '2.';
$dane['p_2_name']            = 'Nulla massa purus fermentum sed';
$dane['p_2_netto']            = '6.0400';
$dane['p_2_pkwiu']            = '';
$dane['p_2_pnetto']            = '6.04';
$dane['p_2_rabat']            = '0.00';
$dane['p_2_sbrutto']            = '43.55';
$dane['p_2_snetto']            = '42.28';
$dane['p_2_svat']            = '1.27';
$dane['p_2_unit']            = 'godz.';
$dane['p_2_vat']            = '3';
$dane['p_3_amount']            = '33';
$dane['p_3_lp']            = '3.';
$dane['p_3_name']            = 'Fusce eleifend magna nis';
$dane['p_3_netto']            = '0.300';
$dane['p_3_pkwiu']            = '';
$dane['p_3_pnetto']            = '0.30';
$dane['p_3_rabat']            = '0.00';
$dane['p_3_sbrutto']            = '10.30';
$dane['p_3_snetto']            = '9.90';
$dane['p_3_svat']            = '0.40';
$dane['p_3_unit']            = 'km';
$dane['p_3_vat']            = '4';
$dane['p_4_amount']            = '3';
$dane['p_4_lp']            = '4.';
$dane['p_4_name']            = 'Nam sed.';
$dane['p_4_netto']            = '122.4000';
$dane['p_4_pkwiu']            = '';
$dane['p_4_pnetto']            = '.28';
$dane['p_4_rabat']            = '5';
$dane['p_4_sbrutto']            = '436.05';
$dane['p_4_snetto']            = '348.84';
$dane['p_4_svat']            = '87.21';
$dane['p_4_unit']            = 'kWh';
$dane['p_4_vat']            = '25';
$dane['p_5_amount']            = '65';
$dane['p_5_lp']            = '5.';
$dane['p_5_name']            = 'Integer vehicula odio';
$dane['p_5_netto']            = '43.1000';
$dane['p_5_pkwiu']            = '';
$dane['p_5_pnetto']            = '43.10';
$dane['p_5_rabat']            = '0.00';
$dane['p_5_sbrutto']            = '3305.77';
$dane['p_5_snetto']            = '2801.50';
$dane['p_5_svat']            = '504.27';
$dane['p_5_unit']            = 'km';
$dane['p_5_vat']            = '18';
$dane['p_6_amount']            = '9';
$dane['p_6_lp']            = '6.';
$dane['p_6_name']            = 'sollicitudin auctor';
$dane['p_6_netto']            = '23.0000';
$dane['p_6_pkwiu']            = '';
$dane['p_6_pnetto']            = '23.00';
$dane['p_6_rabat']            = '0.00';
$dane['p_6_sbrutto']            = '254.61';
$dane['p_6_snetto']            = '207.00';
$dane['p_6_svat']            = '47.61';
$dane['p_6_unit']            = 'opak';
$dane['p_6_vat']            = '23';

$dane['lang_kor_lp']               = 'Lp.';
$dane['lang_kor_name']               = 'Zestawienie korekt';
$dane['lang_kor_pkwiu']               = 'PKWiU';
$dane['lang_kor_amount']               = 'Ilość';
$dane['lang_kor_unit']               = 'Jm';
$dane['lang_kor_netto']               = 'Cena netto';
$dane['lang_kor_rabat']               = 'Rabat%';
$dane['lang_kor_prize_netto']               = 'Cena netto';
$dane['lang_kor_vat']               = 'VAT';
$dane['lang_kor_sum_netto']               = 'Korekta netto';
$dane['lang_kor_sum_vat']               = 'Korekta VAT';
$dane['lang_kor_sum_brutto']               = 'Korekta brutto';

$dane['lang_kor_powod']               = 'Przyczyna korekty podatku';
$dane['kor_powod']               = 'Bo ktoś sie pomylił';
$dane['lang_kor_sum']               = 'Kwota zmniejszenia/zwiekszęnia ceny bez podatku';
$dane['kor_sum']               = '2';
$dane['lang_kor_vat']               = 'Kwota zmniejszenia/zwiekszęnia podatku należnego';
$dane['kor_vat']               = '4';
$dane['p_[id]_amount']         = '';
$dane['p_[id]_lp']	           = '';
$dane['p_[id]_name']	       = '';
$dane['p_[id]_netto']	       = '';
$dane['p_[id]_pkwiu']	       = '';
$dane['p_[id]_pnetto']	       = '';
$dane['p_[id]_rabat']	       = '';
$dane['p_[id]_sbrutto']	       = '';
$dane['p_[id]_snetto']         = '';	
$dane['p_[id]_svat']	       = '';
$dane['p_[id]_unit']           = '---';
$dane['p_[id]_vat']            = '0';
$dane['period_start']          = '2011-12-15';
$dane['period_time']	       = 'day';
$dane['period_value']	       = '1';
$dane['place']	               = 'Żukowo';
$dane['user_account']	       = 'PL34 4234 2344 3242 3424 2342 3442';
$dane['user_address']          = 'Derdowskiego 22 89-632 Brusy';
$dane['user_bank_name']        = 'ING Bank';
$dane['user_mail']             = '';
$dane['user_name']             = 'E-GRUPA COM';
$dane['user_nip']              = '555-130-11-11';
$dane['user_phone']            = '601 255665456565';
	
$dane['kolor_dodatkowy']       = $_POST['kolor_dodatkowy'];
$dane['kolor_ramki']           = $_POST['kolor_ramki'];
$dane['kolor_tekstu']          = $_POST['kolor_tekstu'];
$dane['kolor_tekstu_nagl']     = $_POST['kolor_tekstu_nagl'];
$dane['kolor_tla_nagl']        = $_POST['kolor_tla_nagl'];
$dane['pokaz_naglowki']        = $_POST['pokaz_naglowki'];
if(isset($_POST['pokaz_naglowki'])){
	$dane['pokaz_naglowki'] = 1;
} else {
	$dane['pokaz_naglowki'] = 0;
}

switch ($_POST['jaka_faktura'])
{
	case 1: $pdf=new class_pdfI('',6,$dane,2); break;
	case 2: $pdf=new class_pdfP('',6,$dane,2); break;
	case 3: $pdf=new class_pdfZ('',6,$dane,2); break;
	case 4: $pdf=new class_pdfK('',6,$dane,2); break;
	case 5: $pdf=new class_pdfR('',6,$dane,2); break;
	default: $pdf=new class_pdfI('',6,$dane,2); break; 
}

//$t=explode("public_html/",urldecode($pdf->savePdf()));

echo $pdf->savePdf();

?>