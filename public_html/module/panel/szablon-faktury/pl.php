<?php
$lang=array();
$lang['head']="Wybór szablonu faktury";
$lang['save']="Zapisz zmiany";
$lang['show_example']="Podgląd PDF";
$lang['nagl1'] ='Faktura VAT';
$lang['nagl2'] ='Jesteś w';
$lang['lay1']="Faktura VAT";
$lang['lay2']="Faktura Pro Forma";
$lang['lay3']="Faktura zaliczkowa";
$lang['lay4']="Faktura korygująca";
$lang['lay5']="Rachunek";
$lang['lay6']="nie wiem co";
$lang['lay']="Wszystkie faktury";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania danych, spróbuj za chwilę ponownie";

?>