panel_data_gen_obj = {

	/**
	 * Sprawdzenie unikalności wprowadzonego loginu
	 */
	check_login : function() 
	{
		if(!$("login").value)
		{
			$('login').className="data_gen_text_error";
			return false;
		}
		else if($("login").value==$("old_login").value)
		{
			$('reg_login').value=1;
			$('login').className="data_gen_text_ok";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('login').className="data_gen_text_ok";
				$('reg_login').value=1;
			}
			else
			{
				$('login').className="data_gen_text_error";
				$('reg_login').value=0;
				info($('bad_login').value, 'bad_login');
			}
		};
		req.AddParam("login",$("login").value);
		req.Send("module/panel/data_general/ajax_check.php");
	},
	/**
	 * Sprawdzenie unikalności wprowadzonego adresu e-mail
	 */
	check_mail : function() 
	{
		if(!$("mail").value)
		{
			$('mail').className="data_gen_text_error";
			return false;
		}
		else if($("mail").value==$("old_mail").value)
		{
			$('reg_mail').value=1;
			$('mail').className="data_gen_text_ok";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('mail').className="data_gen_text_ok";
				$('reg_mail').value=1;
			}
			else
			{
				$('mail').className="data_gen_text_error";
				$('reg_mail').value=0;
				info($('bad_mail').value, 'bad_mail');
			}
		};
		req.AddParam("mail",$("mail").value);
		req.Send("module/panel/data_general/ajax_check.php");
	},	
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function() 
	{
			$('dane_bledy_error').style.display='none';
			$('save_ok_error').style.display='none';
			$('save_error_error').style.display='none';

		var msg="";
		if($('pass').value!=$('pass_repeat').value) { msg+=$('bad_pass').value+'<br>'; }
		if($('reg_login').value!=1 || !$('login').value) { msg+=$('bad_login').value+'<br>'; }
		if($('reg_mail').value!=1 || !$('mail').value) { msg+=$('bad_mail').value+'<br>'; }
		if(msg)
		{
			
			info(msg, 'dane_bledy');
				$('dane_bledy_error').style.display='block';
			return false;
		}
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('save_ok').value , 'save_ok' ); $('save_ok_error').style.display='block';}
				else
				{ info($('save_error').value , 'save_error'); }
			};
			req.SendForm("data_gen_save","module/panel/data_log/ajax_save.php","post");
		}
		return false;
	},
};