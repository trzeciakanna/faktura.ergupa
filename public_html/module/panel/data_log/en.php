<?php
$lang=array();
$lang['head']="Logging Data";
$lang['login']="Login";
$lang['pass']="Password";
$lang['repeat']="retype";
$lang['mail']="Email Address";

$lang['cancel']="Cancel";
$lang['save']="Save";   
$lang['del']="Delete";
$lang['add']="Add";
$lang['bad_login']="The given log-in is already occupied";
$lang['bad_mail']="The given email address is already occupied";
$lang['bad_pass']="The two passwords are different";
$lang['save_ok']="Data stored successfully";
$lang['save_error']="An error occurred while saving. Please try again later";
$lang['info']="These fields are required";
$lang['change_pass']="Submit only if you want to change your password";

$lang['']="";
?>