<?php
$lang=array();
$lang['head']="Dane do serwisu";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['repeat']="powtórz";
$lang['mail']="Adres e-mail";
$lang['cancel']="Anuluj";
$lang['save']="Zapisz";   
$lang['add']="Dodaj";
$lang['del']="Usuń";
$lang['bad_login']="Podany login jest już zajęty";
$lang['bad_mail']="Podany adres e-mail jest już zajęty";
$lang['bad_pass']="Podane hasła są różne";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania, spróbuj ponownie za chwilę";
$lang['info']="Podane pola są wymagane";
$lang['change_pass']="Wypełnij tylko, jeżeli chcesz zmienić hasło";
$lang['nagl1']= 'Faktura VAT';
$lang['nagl2']= 'Jesteś w';
$lang['']="";
?>