panel_data_gen_obj = {

		
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function() 
	{
			$('dane_bledy_error').style.display='none';
			$('save_ok_error').style.display='none';
			$('save_error_error').style.display='none';

		/*var msg="";
		//if($('pass').value!=$('pass_repeat').value) { msg+=$('bad_pass').value+'<br>'; }
		//if($('reg_login').value!=1 || !$('login').value) { msg+=$('bad_login').value+'<br>'; }
		//if($('reg_mail').value!=1 || !$('mail').value) { msg+=$('bad_mail').value+'<br>'; }
		if(msg)
		{
			
			info(msg, 'dane_bledy');
				$('dane_bledy_error').style.display='block';
			return false;
		}
		else
		{*/
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('save_ok').value , 'save_ok' ); $('save_ok_error').style.display='block';}
				else
				{ info($('save_error').value , 'save_error'); }
			};
			req.SendForm("data_gen_save","module/panel/data_general/ajax_save.php","post");
		//}
		return false;
	},
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	esubmit : function(id) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value, 'save_ok'); }
			else
			{ info($('save_error').value, 'save_error'); }
		};
		req.SendForm("data_extra_save","module/panel/data_general/ajax_esave.php?user_id="+id,"post");
		return false;
	},
	
	addAc : function(uid)
	{
    var req = mint.Request();
		req.OnSuccess = function()
		{ location.href=location.href; };
		req.AddParam('data',$('account').value);  
		req.AddParam('bn',$('bank_name').value);
		req.AddParam('uid',uid);
		req.Send("module/panel/data_general/ajax_addAc.php");
  },
  
  saveAc : function(uid,k)
	{
    var req = mint.Request();
		req.OnSuccess = function()
		{ location.href=location.href; };
		req.AddParam('data',$('account'+k).value); 
		req.AddParam('bn',$('bank_name'+k).value);
		req.AddParam('id',k);   
		req.AddParam('uid',uid);
		req.Send("module/panel/data_general/ajax_saveAc.php");
  },
  
  delAc : function(uid,k)
	{
    var req = mint.Request();
		req.OnSuccess = function()
		{ location.href=location.href; };
		req.AddParam('id',k);   
		req.AddParam('uid',uid);
		req.Send("module/panel/data_general/ajax_delAc.php");
  },
	
};