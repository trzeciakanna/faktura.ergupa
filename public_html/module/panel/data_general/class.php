<?php
/**
 * Zarządzanie danymi dodatkowymi
 * @author kordan
 *
 */
class user_extra
{
	public $logo;
	public $head;
	public $foot;
	public $info;
	/**
	 * Utworzenie obiektu danych dodatkowych
	 * @param int $id identyfikator użytkownika
	 */
	public function __construct($id=null)
	{
		if($id)
		{
			global $DB;
			global $func;
			$r=$DB->Execute("SELECT * FROM `users_extra` WHERE `user_id`='".$id."' LIMIT 0,1");
			$this->logo=$func->show_with_html($r->fields['logo']);
			$this->head=$func->show_with_html($r->fields['head']);
			$this->foot=$func->show_with_html($r->fields['foot']);
			$this->info=$func->show_with_html($r->fields['info']);
			if(!$r->fields['user_id']) { $DB->Execute("INSERT INTO `users_extra` SET `user_id`='".$id."'"); }
		}
	}
	/**
	 * Zapisanie danych dodatkowych użytkownika
	 * @param int $id identyfikator użytkownika
	 * @param string $logo ściezka pliku logotypu
	 * @param string $head nagłówek
	 * @param string $foot stopka
	 * @param string $info informacje dodatkowe
	 * @return string stan wykonania
	 */
	public function save($id, $logo, $head, $foot, $info)
	{
		global $DB;
		$tmp=array();
		$tmp['logo']=$logo;
		$tmp['head']=$head;
		$tmp['foot']=$foot;
		$tmp['info']=$info;
		$DB->AutoExecute("users_extra",$tmp,"UPDATE","`user_id`='".$id."'");
		return "ok";
	}
}
?>