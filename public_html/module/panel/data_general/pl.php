<?php
$lang=array();
$lang['head']="Dane do faktury";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['repeat']="powtórz";
$lang['mail']="Adres e-mail";
$lang['invoice']="Dane do faktury";
$lang['name']="Imię i nazwisko/nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['phone']="Telefon";
$lang['email']="Adres e-mail";
$lang['bank_name']="Nazwa banku";
$lang['krs']="Numer KRS";
$lang['account']="Numer konta";
$lang['place']="Miejsce wystawienia faktury"; 
$lang['www']="WWW";
$lang['cancel']="Anuluj";
$lang['save']="Zapisz";   
$lang['add']="Dodaj nowe konto";
$lang['del']="Usuń";
$lang['bad_login']="Podany login jest już zajęty";
$lang['bad_mail']="Podany adres e-mail jest już zajęty";
$lang['bad_pass']="Podane hasła są różne";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania, spróbuj ponownie za chwilę";
$lang['info']="Podane pola są wymagane";
$lang['change_pass']="Wypełnij tylko, jeżeli chcesz zmienić hasło";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_krs']="Podany numer KRS jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_regon']="Podany numer REGON jest nieprawidłowy, wprowadź poprawny numer.";

$lang['ehead']="Dane dodatkowe";
$lang['elogo']="Logo na fakturę";
$lang['eheader']="Nagłówek faktury";
$lang['efoot']="Stopka faktury";
$lang['einfo']="Informacja do faktury";
$lang['nagl1']="Faktura VAT";
$lang['nagl2']="Jesteś w";
$lang['nagl3']="Dane podstawowe";
$lang['nagl4']="Dane dodatkowe";
$lang['saveAc']="Zmiany zostały dokonane";
$lang['addAc']="Nowe konto zostało dodane";
$lang['account_change'] = "Numery kont bankowych";
$lang['add_account'] = "Dodaj nowe konto bankowe";

?>