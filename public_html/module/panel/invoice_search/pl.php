<?php
$lang=array();
$lang['head']="Wyszukiwarka zapisanych faktur (bez PDF)";
$lang['number']="Numer faktury";
$lang['name']="Nazwa nabywcy";
$lang['nip']="Numer NIP nabywcy";
$lang['date_add']="Data wystawienia";
$lang['date_sell']="Data sprzedaży";
$lang['sum']="Kwota faktury";
$lang['search']="Szukaj";
$lang['view']="Zobacz";
$lang['other']="Pozostali";
$lang['dcreate']="Data utworzenia";
$lang['dadd']="Data wystawienia";
$lang['dsell']="Data sprzedaży";
$lang['month']="Przeglądanie miesięczne";
$lang['show']="Pokaż";
$lang['type2']="Rodzaj";
$lang['all']="Pokaż wszystkie";
$lang['all_l']="wszystkie";
$lang['page']="Lista wszystkich faktur kontrahenta";
$lang['showed']="rozwiń";
$lang['hidden']="zwiń";
$lang['searcher']="Wyszukiwanie zaawansowane";
$lang['sendm']="Wyślij";
$lang['payed']="Zapłacono";
$lang['save_payed']="Zapisz";


$lang['m_select']="Wybierz adres/adresy e-mail na które wysłać fakturę";
$lang['m_mail']="Podaj nowy adres";
$lang['m_close']="Zamknij";
$lang['m_send']="Wyślij";

$lang['type']=array();
$lang['type'][0]="Typ";
$lang['type'][1]="VAT";
$lang['type'][2]="Pro Forma";
$lang['type'][3]="Zaliczka";
$lang['type'][4]="Korekta";
$lang['type'][5]="Rachunek";

$lang['c_head']="Dane kontrahenta";
$lang['c_name']="Imię i nazwisko/nazwa firmy";
$lang['c_address']="Adres";
$lang['c_nip']="NIP";
$lang['c_phone']="Telefon";
$lang['c_email']="Adres e-mail";
$lang['c_krs']="Numer KRS";
$lang['c_bank_name']="Nazwa banku";
$lang['c_account']="Numer konta";

$lang['prev']="Poprzedni kontrahent";
$lang['next']="Następny kontrahent";

$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania danych, spróbuj ponownie za chwilę";

$lang['send_ok']="Faktura została wysłana na wybrany adres email";
$lang['send_error']="Wystąpił błąd podczas wysyłania wiadomości, spróbuj ponownie za chwilę";
$lang['send_null']="Brak adresu e-mail do wysyłki, uzupełnij dane kontrahenta";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";


$lang['nagl0']= 'Lista zapisanych faktur ';
$lang['nagl1']= 'Faktura VAT';
$lang['nagl2']= 'Jesteś w';
$lang['nagl3']= 'Od:';
$lang['nagl4']= 'Do:';
$lang['nagl5']= 'Przeglądanie miesięczne';
$lang['nagl6']= 'Brak zapisanych faktur';
$lang['nagl7']= 'PDF';
$lang['nagl8']= 'Numer faktury';
$lang['nagl9']= 'Kwota faktury';
$lang['nagl10']= 'Wpłacono';
$lang['nagl11']= 'Nazwa / Firma';
$lang['nagl12']= 'Usuń';
$lang['nagl13']= 'Wyślij';
$lang['nagl14']= 'Wysyłka dokumentu na adres e-mail';
$lang['nagl15']= 'Wybierz bądź wprowadź adres e-mail';
$lang['nagl16']= 'Wybierz rodzaj dokumentu';
$lang['nagl17']= 'PDF';
$lang['nagl18']= 'Zobacz';


$lang['save_menu0'] = 'Wydrukuj PDF';
$lang['save_menu1'] = 'Dwa egzemplarze';
$lang['save_menu2'] = 'Jeden egzemplarz';
$lang['save_menu3'] = 'Kopia';
$lang['save_menu4'] = 'Duplikat';
$lang['save_menu5'] = 'Drukuj';

$lang['send_loading'] = 'Trwa wysyłanie wiadomości'; 
$lang['printm']="Multidruk"; 
$lang['errSel']="Nie wybrano dokumentów";
$lang['errZip']="Błąd systemu. Spróbuj ponownie";
$lang['print_loading']="Trwa zapisywanie dokumentów"; 
$lang['print_one']="W jednym pliku PDF";
$lang['print_more']="W oddzielnych plikach PDF";
?>