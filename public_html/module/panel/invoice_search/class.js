panel_invoice_search_obj = {
	/**
	 * Pokazanie panelu wyszukiwarki
	 */
	show_search : function()
	{
		$('search_show').style.display="none";
		$('search_hidden').style.display="block";
		$('search').style.display="block";
	},
	/**
	 * Ukrycie panelu wyszukiwarki
	 */
	hidden_search : function()
	{
		$('search_show').style.display="block";
		$('search_hidden').style.display="none";
		$('search').style.display="none";
	},
	/**
	 * Przekierowanie na stronę wyszukiwania
	 */
	search : function()
	{
		var par=$('number').value.replace('/','-sl-').replace('/','-sl-').replace('/','-sl-')+","+$('name').value+","+$('nip').value+",";
		if($('search').style.display!='block')
		{
		  var d=new Date();
      par+=d.getFullYear()+'-1-1,'+d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+',';
      par+='2010-1-1,'+d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate()+',';
    }
    else
    {
      par+=$('calendar_date_dadd_min').value+","+$('calendar_date_dadd_max').value+",";
  		par+=$('calendar_date_dsell_min').value+","+$('calendar_date_dsell_max').value+",";
		}
		par+=$('sum_min').value+","+$('sum_max').value;
		location.href="panel/invoice/search/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przekierowanie na stronę listy
	 */
	show : function()
	{
		var par=$('year').value+'-'+$('month').value+'-'+($('type_c').checked?"c":($('type_a').checked?"a":"s"));
		location.href="panel/invoice/search/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Przekierowanie na stronę listy
	 */
	show_month : function(month)
	{
		var t=new Date();
		var par=$('year').value+'-'+month+'-'+($('type_c').checked?"c":($('type_a').checked?"a":"s"));
		location.href="panel/invoice/search/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
		return false;
	},
	
	sendm : function(id, cid, uid, type)
	{
    var req = mint.Request();
		req.OnSuccess = function()
		{
		  try{
      var tmp=this.responseText.split("|");
      if(this.responseText=="") { tmp.length=0; }
      $('mail_list').innerHTML="";
      for(a=0;a<tmp.length;a++)
      {
        var r=$('mail_item').cloneNode(true);
        r.innerHTML=r.innerHTML.replace(/\[m\]/g,tmp[a]).replace(/\[i\]/g,a);
        $('mail_list').appendChild(r);
      }
      var size=window_size();
  		$('panel_mail').style.display="block";
  		$('panel_mail').style.top=((size[1]-$('panel_mail').clientHeight)/2)+"px";
  		//$('panel_mail').style.left=((size[0]-$('panel_mail').clientWidth)/2)+"px";
      
      }catch(e) { alert(e); }
    }
    $('iid').value=id; 
	$('fak_type').value=type; // Typ faktury (rachunek, prof form itp 
    //$('uid').value=uid;
    //$('cid').value=cid;
		req.AddParam("id",id);
		req.AddParam("cid",cid);
		req.AddParam("uid",uid);
		req.Send("module/panel/invoice_search/mail_list.php");
		return false;
  },
	/**
	 * Wysłanie maila z fakturą
	 * @param id identyfikator faktury
	 * @param cui identygikator kontrahenta
	 * @param uid identyfikator użytkownika
	 */
	sendm_2 : function()
	{
		var req = mint.Request();
		req.retryNum=0;
		req.OnSuccess = function()
		{
			$('send_loading').style.display = 'none'; // zamkniecie informacji o ładowaniu
			if(this.responseText=="ok")
			{ info($('send_ok').value ,'send'); 
			$('send_error_all').style.display = 'block';
			}
			else if(this.responseText=="null")
			{ info($('send_null').value ,'send');
			$('send_error_all').style.display = 'block';
			}
			else
			{ info($('send_error').value ,'send'); 
				$('send_error_all').style.display = 'block';
			}
		};
		//req.AddParam("id",$('iid').value);
		//req.AddParam("cid",$('cid').value);
		//req.AddParam("uid",$('uid').value);
		//req.Send("module/panel/invoice_period/send_mail.php"); 
		req.SendForm("send_mail","module/panel/invoice/send_mail.php","post");
		panel_invoice_search_obj.close(); //zamkniecie formularz wysyłania 
		$('send_loading').style.display = 'block';
		return false;
	},
	/**
	 * Zamknięcie panelu wybieranie adresów email
	 */
	close : function()
	{
		$('panel_mail').style.display="none";
	},
	/**
	 * Zapisanie zapłacenia faktur
	 * @param uid identyfikator użytkownika
	 */
	save_payed : function(uid)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ location.reload(true); }
			else
			{ info($('save_error').value ,'save'); }
		};
		req.SendForm("invoice_payed","module/panel/invoice_search/save_payed.php?uid="+uid,"post");
		return false;
	},
	/**
	 * Zapis do pdf
	 */
	idf : 0,
	type : 0,
	save_pdf : function(id, type)
	{
		$('what_print').style.display='block';
		this.idf=id;      
		this.type=type;
    return false;
	},
	save_pdf_end : function()
	{
		var what=1;
		what=$('print2').checked?2:what; 
		//what=$('print3').checked?3:what;
		what=$('print4').checked?4:what;
		$('what_print').style.display='none';
		location.href='panel/invoice/list/file/'+this.idf+'/'+what+'/'+this.type+'/';
		return false;
	},
	
	
	saveM_pdf : function(id, type)
	{
		$('what_printM').style.display='block';
    return false;
	},
	saveM_pdf_end : function(user)
	{
    $('print_loading').style.display = 'block';
		var what=1;
		what=$('printM2').checked?2:what; 
		//what=$('printM3').checked?3:what;
		what=$('printM4').checked?4:what;
		$('what_printM').style.display='none';
		var one=1;  
		one=$('printM').checked?0:what; 
		
		var req = mint.Request();
		req.timeout=360000;
		req.OnSuccess = function()
		{
		  $('print_loading').style.display = 'none';
		  var t=this.responseText.split('|');
		  if(t[0]=="ok")
		  { location.href='download.php?file='+t[1]; }
		  else if(t[0]=="errSel") { info($('error_sel').value); } 
		  else if(t[0]=="errZip") { info($('error_zip').value); }
		};
		req.SendForm("invoice_payed","module/panel/invoice/mprint.php?what="+what+"&user="+user+"&one="+one,"post");
		return false;
	},
	
	selAllM : function(v)
	{
    var o=document.getElementsByName("fak[]");
    for(var a=0;a<o.length;a++) { o[a].checked=v; }
  }
};