<?php
$lang=array();
$lang['head']="Saved Invoice Browser (no PDF)";
$lang['number']="Invoice Number";
$lang['name']="Buyer's Name";
$lang['nip']="Buyer's Tax ID#";
$lang['date_add']="Date of Issue";
$lang['date_sell']="Date of Sale";
$lang['sum']="Invoice Amount";
$lang['search']="Search";
$lang['view']="View";
$lang['other']="Remaining";
$lang['dcreate']="Date of creating";
$lang['dadd']="Date of Issue";
$lang['dsell']="Date of Sale";
$lang['month']="Monthly browsing";
$lang['show']="Display";
$lang['type2']="Type";
$lang['all']="Show All";
$lang['all_l']="all";
$lang['page']="List of all the Customer's invoices";
$lang['showed']="pull down";
$lang['hidden']="pull up";
$lang['searcher']="Advanced Search Engine";
$lang['sendm']="Send";
$lang['payed']="Paid";
$lang['save_payed']="Save";


$lang['m_select']="Select email address(es) to which the Invoice will be sent";
$lang['m_mail']="Enter new address";
$lang['m_close']="Close";
$lang['m_send']="Send";

$lang['type']=array();
$lang['type'][0]="Type";
$lang['type'][1]="VAT Invoice";
$lang['type'][2]="Pro Forma Invoice";
$lang['type'][3]="Pre-payment Invoice";
$lang['type'][4]="Correction Invoice";
$lang['type'][5]="Bill";

$lang['c_head']="Customer Data";
$lang['c_name']="Full name/Company Name";
$lang['c_address']="Address";
$lang['c_nip']="Tax ID#";
$lang['c_phone']="Telephone";
$lang['c_email']="Email Address";
$lang['c_krs']="Companies House #";
$lang['c_bank_name']="Bank Name";
$lang['c_account']="Account No.";

$lang['prev']="Previous Customer";
$lang['next']="Next Customer";

$lang['save_ok']="Data successfully saved";
$lang['save_error']="An error occurred while saving data. Please try again later";

$lang['send_ok']="The invoice has been sent to the email address ascribed to this Customer";
$lang['send_error']="An error occurred while sending the message. Please try again later";
$lang['send_null']="No email address. Please complete the Customer's data";
$lang['bad_nip']="The Tax ID# is incorrect. Please provide the correct number";


$lang['nagl0']= 'Invoice List ';
$lang['nagl1']= 'VAT Invoice';
$lang['nagl2']= 'You are in';
$lang['nagl3']= 'From:';
$lang['nagl4']= 'To:';
$lang['nagl5']= 'Monthly browsing';
$lang['nagl6']= 'No saved Invoices';
$lang['nagl7']= 'Print PDF';
$lang['nagl8']= 'Invoice Number';
$lang['nagl9']= 'Invoice Amount';
$lang['nagl10']= 'Paid';
$lang['nagl11']= 'Name / Company';
$lang['nagl12']= 'Remove';
$lang['nagl13']= 'Send';
$lang['nagl14']= 'Send document to this Email Address';
$lang['nagl15']= 'Select or enter Email Address';
$lang['nagl16']= 'Select type of document';
$lang['nagl17']= 'PDF';
$lang['nagl18']= 'View';


$lang['save_menu0'] = 'Print PDF';
$lang['save_menu1'] = 'Original and Copy';
$lang['save_menu2'] = 'Original';
$lang['save_menu3'] = 'Copy';
$lang['save_menu4'] = 'Duplicate';
$lang['save_menu5'] = 'Print';

$lang['send_loading'] = 'Trwa wysyłanie wiadomości';
?>