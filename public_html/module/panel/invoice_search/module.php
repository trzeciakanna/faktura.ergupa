<?php
global $smarty;
require(ROOT_DIR."includes/months.php");
require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/invoice_search/".LANG.".php");
global $navi;
$smarty->assign("navi",$navi->navi);
global $lang;
global $months;

$smarty->assign("lang",$lang);
$smarty->assign("months",$months);
//odczyt danych użytkownika
$inv=new invoice();
$smarty->assign("user_id",USER_ID);
$searchs=array();

$_GET['par4']=(int)$_GET['par4'];
$_GET['par6']=$_GET['par6']?$_GET['par6']:"dn";
//print_r($_GET);
if(strlen($_GET['par8'])>8 AND !(strpos($_GET['par8'],",")===false))
{
	require(ROOT_DIR."includes/search.php");
	$search=new search();
	$_GET['par8']=str_replace("_"," ",urldecode($_GET['par8']));
	$tmp=explode(",",$_GET['par8']);
	for($a=3;$a<=6;$a++) { $tmp[$a]=date("Y-m-d",strtotime($tmp[$a])); }
	$searchs['number']=str_replace("-sl-","/",$tmp[0]);
	$searchs['name']=$tmp[1];
	$searchs['nip']=$tmp[2];
	$searchs['damin']=$tmp[3];
	$searchs['damax']=$tmp[4];
	$searchs['dsmin']=$tmp[5];
	$searchs['dsmax']=$tmp[6];
	$searchs['smin']=$tmp[7];
	$searchs['smax']=$tmp[8];
	$searchs['dcy']=date("Y");
	$searchs['dcm']=date("m");
	$searchs['type']="s";
	list($dane,$page,$last)=$inv->read_search(USER_ID,$_GET['par4'],$_GET['par5'],100,$_GET['par6'],$_GET['par7'],$tmp[0],$tmp[1],$tmp[2],$tmp[3],$tmp[4],$tmp[5],$tmp[6],$tmp[7],$tmp[8]);
}
elseif(strlen($_GET['par8'])==9)
{
	$tmp=explode("-",$_GET['par8']);
	$searchs['dcy']=$tmp[0];
	$searchs['dcm']=$tmp[1];
	$searchs['type']=$tmp[2];
	$searchs['damin']=date("Y-m")."-1";
	$searchs['dsmin']=date("Y-m")."-1";
	$as=null; $ae=null; $ss=null; $se=null; $c=null;
	if($tmp[2]=="a")
	{
		$as=$tmp[0]."-".$tmp[1]."-1";
		$ae=$tmp[0]."-".$tmp[1]."-".date("t",strtotime($tmp[0]."-".$tmp[1]."-1"));
	}
	elseif($tmp[2]=="s")
	{
		$ss=$tmp[0]."-".$tmp[1]."-1";
		$se=$tmp[0]."-".$tmp[1]."-".date("t",strtotime($tmp[0]."-".$tmp[1]."-1"));
	}
	else { $c=$tmp[0]."-".$tmp[1]; }
	list($dane,$page,$last)=$inv->read_search(USER_ID,$_GET['par4'],$_GET['par5'],100,$_GET['par6'],$_GET['par7'],null,null,null,$as,$ae,$ss,$se,null,null,$c);
}
else
{
	$searchs['dcy']=date("Y");
	$searchs['dcm']=date("m"); 
	$searchs['damin']=date("Y")."-1-1";
	$searchs['dsmin']=(date("Y")-1)."-1-1"; 
	//$searchs['damin']=date("Y-m")."-1";
	//$searchs['dsmin']=date("Y-m")."-1";
	$searchs['type']="a";
	//$ss=date("Y-m")."-1";
	//$se=date("Y-m-t");
	$ss=date("Y")."-01-01";
	$se=date("Y")."-12-31";
	list($dane,$page,$last)=$inv->read_search(USER_ID,$_GET['par4'],$_GET['par5'],100,$_GET['par6'],$_GET['par7'],null,null,null,$ss,$se,null,null);
}
$smarty->assign("client",(int)$_GET['par4']);
$smarty->assign("search",$searchs);
$smarty->assign("dane",$dane);
$smarty->assign("page",$page);
$smarty->assign("last",$last);
$smarty->assign("sort",$_GET['par6']);
$smarty->assign("sign","dn/".($_GET['par7']?$_GET['par7']."/":"-/").($_GET['par8']?$_GET['par8']."/":""));
$smarty->assign("par8",$_GET['par8']?$_GET['par8']."/":"");
$smarty->assign("par4",$_GET['par4']?$_GET['par4']."":"0");
$smarty->assign("par7",$_GET['par7']?$_GET['par7']:"-");
$smarty->assign("ys",date("Y")+1);
$smarty->assign("letter",$inv->letter_list(USER_ID,true));
//dane kontrahenta
if($_GET['par4'])
{
	require(ROOT_DIR."module/panel/client_add/class.php");
	$client=new clients();
	$cl=$client->read_one(USER_ID,$_GET['par4']);
	$smarty->assign("client_data",$cl);
	$smarty->assign("prev_c",$inv->prev_client(USER_ID,$cl->sign));
	$smarty->assign("next_c",$inv->next_client(USER_ID,$cl->sign));
}

$smarty->display("panel/invoice_search/".LAYOUT.".html");
?>