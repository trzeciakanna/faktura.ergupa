<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
require(ROOT_DIR."module/panel/client_add/class.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
//odczyt id klienta faktury i mail
$mail=array();
if((int)$_GET['cid'])
{
  $client=new clients();
  $c=$client->read_one((int)$_GET['uid'],(int)$_GET['cid']);
  if($func->is_mail($c->mail)) { $mail[]=$c->mail; }
  $tmp=preg_split("/[,\s]+/",$c->email);
	foreach($tmp as $m)
	{ if($func->is_mail($m)) { $mail[]=$m; } }
}
$r=$DB->Execute("SELECT email FROM invoice_data WHERE invoice_id='".(int)$_GET['id']."' AND type='client' LIMIT 0,1");
if($r->fields['email'])
{
  $tmp=preg_split("/[,\s]+/",$r->fields['email']);
	foreach($tmp as $m)
	{ if($func->is_mail($m)) { $mail[]=$m; } }
}
$mail=array_unique($mail);
echo implode("|",$mail);
?>