<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
require(ROOT_DIR."pay_system/platnosci_pl/class.php");
require(ROOT_DIR."pay_system/platnosci_pl/pl.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$user_invoice=new users_invoice($_GET['user_id']);
$pay=new platnosci_pl();
/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
//obliczenie kwoty
$_GET['time']=$_GET['time']<3?3:$_GET['time'];
$cash=10*$_GET['time']*(1-0.025*($_GET['time']-1))*100*1.23;
$pay->add($_GET['user_id'],$_GET['time'],$cash);
//odczyt formularza
$file=fopen(ROOT_DIR."pay_system/platnosci_pl/form.html","rt");
$txt="";
while(!feof($file))
{ $txt.=fgets($file,1024); }
fclose($file);
$name=explode(" ",$user_invoice->name);
echo str_replace(array("[first_name]","[last_name]","[email]","[pos_id]","[pos_auth_key]","[session_id]","[cash]","[desc]","[ip]"),array($name[0],$name[1],$user_invoice->mail,$pay->pos_id,$pay->pos_auth_key,$pay->session_id,$cash,str_replace("[month]",$_GET['time'],$lang['desc']),$func->get_ip()),$txt);
?>