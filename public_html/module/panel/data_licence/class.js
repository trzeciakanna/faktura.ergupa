panel_data_lic_obj = {	
	/**
	 * Utworzenie formularza płatności i wysłanie go
	 */
	submit : function() 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			$('data_lic_tmp').innerHTML=this.responseText;
			$('data_lic_form').submit();
		};
		req.AddParam("time",$("time").value);
		req.AddParam("user_id",$("user_id").value);
		req.Send("module/panel/data_licence/ajax_pay.php");
	}
};