<?php
$lang=array();
$lang['head']="Reports";
$lang['year']="Yearly";
$lang['month']="Monthly";
$lang['']="";
$lang['']="";

$lang[0]="January";
$lang[1]="February";
$lang[2]="March";
$lang[3]="April";
$lang[4]="May";
$lang[5]="June";
$lang[6]="July";
$lang[7]="August";
$lang[8]="September";
$lang[9]="October";
$lang[10]="November";
$lang[11]="December"; 
$lang[12]="Total";

$lang['nagl1']='VAT Invoice';
$lang['nagl2']='You are in';
$lang['nagl3']='Select a Report to view its results';
$lang['nagl4']='back';
$lang['nagl5']='The Report concerns Invoices issued for a total of:';
$lang['nagl6']='year';
$lang['nagl7']='net';
$lang['nagl8']='gross';
$lang['nagl9']='VAT';
$lang['nagl10']='how much';
$lang['nagl11']='Month';

?>