<?php
/* ustawienia serwera */
set_time_limit(360);
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
define("USER_ID",$_GET['user']);
//define("USER_ID",9);
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
//utworzenie faktury
//$_GET['what']=1;
//$_GET['one']=1;
//$_POST['fak']=array(17479,47332,50703,48678);
if(!is_array($_POST['fak']) OR !count($_POST['fak'])) { echo "errSel"; exit(); }

require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/data_extra/class.php");
require(ROOT_DIR."module/panel/szablon-faktury/class.php");

$inv=new invoice();
if($_GET['one'])
{
	require_once(ROOT_DIR.'module/invoice/main_pdf.php');
	$pdf=new mainPDF(USER_ID);
	
	foreach($_POST['fak'] as $id)
	{
		list($tmp,$count)=$inv->get_to_pdf($id,USER_ID);
		$data=new user_color(USER_ID,$tmp['type']);
		switch($tmp['type'])
		{
			case 1: $dirf="invoice"; $class="class_pdfI"; break;
			case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
			case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
			case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
			case 5: $dirf="rachunek"; $class="class_pdfR"; break;
			default: $dirf="invoice"; $class="class_pdfI"; break;
		}
		require_once(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
		$data=new user_color(USER_ID,$tmp['type']);
		foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
		$pdf=cast_class($pdf,$class);
		$pdf->gen(USER_ID,$count,$tmp,$_GET['what']);
	}
	$name="cache/dokumenty_".date("Ymd")."_".substr(SHA1(microtime()),0,4).".pdf";
	$pdf->Output(ROOT_DIR.$name,"F");
}
else
{
	$files=array();
	foreach($_POST['fak'] as $id)
	{
		list($tmp,$count)=$inv->get_to_pdf($id,USER_ID);
		$data=new user_color(USER_ID,$tmp['type']);
		switch($tmp['type'])
		{
			case 1: $dirf="invoice"; $class="class_pdfI"; break;
		    case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
		    case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
		    case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
		    case 5: $dirf="rachunek"; $class="class_pdfR"; break;
		    default: $dirf="invoice"; $class="class_pdfI"; break;
		}
		require_once(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
		
		$data=new user_color(USER_ID,$tmp['type']);
		foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
		$pdf=new $class(USER_ID,$count,$tmp,$_GET['what']);
		$t=explode("private_html/",urldecode($pdf->savePdf()));
		$files[]=$t[1];
	}
	
	$zip = new ZipArchive();
	$name="cache/dokumenty_".date("Ymd")."_".substr(SHA1(microtime()),0,4).".zip";
	if($zip->open(ROOT_DIR.$name, ZIPARCHIVE::CREATE)!==TRUE) { echo "errZip"; exit(); }
	foreach($files as $i=>$f)
	{
		$t=explode("/",$f);
		$zip->addFile(ROOT_DIR.$f,($i+1).".".array_pop($t));
	}
	$zip->close();
}
echo "ok|".$name;
?>