<?php
class invoice
{
	public function get_to_pdf($id, $user_id)
	{
		global $func;
		$pdf=array();
		//przeparsowanie pliku językowego
		if(file_exists(ROOT_DIR."files/invoice_lang/".$id.".php")) { include ROOT_DIR."files/invoice_lang/".$id.".php"; }
		else { include ROOT_DIR."module/invoice/pl.php"; }
		foreach($lang as $key => $var)
		{ $pdf['lang_'.$key]=$var; }
		//dane sprzedawcy
		$tmp=new invoice_data($id,"user");
		$pdf['user_name']=$tmp->name;;
		$pdf['user_address']=$tmp->address;
		$pdf['user_nip']=$tmp->nip;
		$pdf['user_phone']=$tmp->phone;
		$pdf['user_mail']=$tmp->email;
		$pdf['user_krs']=$tmp->krs;     
		$pdf['user_www']=$tmp->www;
		$pdf['user_bank_name']=$tmp->bank_name;
		$pdf['user_account']=$tmp->account;
		$pdf['cash_pay']=$tmp->sum_pay;
		//dane nabywcy
		$tmp=new invoice_data($id,"client");
		$pdf['client_name']=$tmp->name;
		$pdf['client_address']=$tmp->address;
		$pdf['client_nip']=$tmp->nip;
		$pdf['client_phone']=$tmp->phone;
		$pdf['client_mail']=$tmp->email;
		$pdf['client_krs']=$tmp->krs;
		$pdf['client_bank_name']=$tmp->bank_name;
		$pdf['client_account']=$tmp->account;
		//dane dostawy
		$tmp=new invoice_data($id,"branch");
		if($tmp->name)
		{
			$pdf['branch_name']=$tmp->name;
			$pdf['branch_address']=$tmp->address;
			$pdf['branch_nip']=$tmp->nip;
			$pdf['branch_phone']=$tmp->phone;
			$pdf['branch_mail']=$tmp->email;
			$pdf['branch_krs']=$tmp->krs;
			$pdf['branch_bank_name']=$tmp->bank_name;
			$pdf['branch_account']=$tmp->account;
			$pdf['branch_v']=true;
		}
		//dane faktury
		$tmp=$this->read_one($user_id,$id);
		$pdf['number']=$tmp->number;
		$pdf['client_id']=$tmp->client;
		$pdf['original_v']=$tmp->original;
		$pdf['place']=$tmp->place;
		$pdf['date_create']=$tmp->date_add; 
		$pdf['date_deadline']=$tmp->date_deadline;
		$pdf['date_sell']=$tmp->date_sell;
		$pdf['cash_type']=$tmp->sell_type;
		$pdf['cash_date']=$tmp->sell_date;
		$pdf['current']=$tmp->cur;  
		$pdf['cash_pay']=$tmp->sum_pay;  
		$pdf['lang']=$tmp->lang;  
		if($tmp->sell_date=="date") { $pdf['cash_date_date']=$tmp->sell_time; }
		else { $pdf['cash_date_list']=$tmp->sell_time; }
		$pdf['coment']=$tmp->coment; 
		$pdf['type']=$tmp->type;
		//produkty
		$sum=0;
		foreach($tmp->product as $k => $i)
		{
			$k++;
			$pdf['p_'.$k.'_lp']=$k.".";
			$pdf['p_'.$k.'_name']=$i->name;
			$pdf['p_'.$k.'_pkwiu']=$i->pkwiu;
			$pdf['p_'.$k.'_amount']=$i->amount;
			$pdf['p_'.$k.'_unit']=$i->unit;
			$pdf['p_'.$k.'_netto']=$i->netto;
			$pdf['p_'.$k.'_rabat']=$i->rabat;
			$pdf['p_'.$k.'_pnetto']=number_format($i->netto*(100-$i->rabat)/100,4,".","");
			$pdf['p_'.$k.'_vat']=$i->vat;
			$pdf['p_'.$k.'_snetto']=number_format($pdf['p_'.$k.'_pnetto']*$i->amount,2,".","");
			$pdf['p_'.$k.'_svat']=number_format($pdf['p_'.$k.'_snetto']*$i->vat/100,2,".","");
			$pdf['p_'.$k.'_sbrutto']=number_format($pdf['p_'.$k.'_snetto']+$pdf['p_'.$k.'_svat'],2,".","");
			$sum+=$pdf['p_'.$k.'_sbrutto'];
		}
		//słownie
		$pdf['cash_sum']=$sum;
		$_GET['langword']=$tmp->lang;
		$pdf['cash_word']=$func->number_to_words($sum-$tmp->sum_pay,$pdf['current']);
		$_GET['langword']='';
		//zwrócenie danych
		return array($pdf,count($tmp->product));
	}
	/**
	 * Zapisanie faktury
	 * @param int $id identyfikator użytkownika
	 * @param array $data tablica danych
	 * @param int $count identyfikator ostatniego produktu
	 * @param boolean $period faktura okresowa
	 */
	public function add($id, $data, $count, $period=false, $type=1)
	{
    //$fl=fopen(ROOT_DIR."files/".date("His").".txt","wt");
		//fputs($fl,print_r($data,true)."\n"); 
		//fclose($fl);
    if(!class_exists("user_number")) { require(ROOT_DIR."module/panel/data_number/class.php"); }
		global $DB;//$DB->debug=true;
		//zaktualizacja numeracji
		$r=$DB->Execute("SELECT `id` FROM `invoice` WHERE `user_id`='".$id."' AND `number`='".$data['number']."' AND `type`='".$type."' LIMIT 0,1");
    if(!$r->fields['id'])
    {
  		$nr=new user_number($id,$type);
  		$nr->up_nr($id,$type);
		}
    //dodanie faktury
		$tmp=array();
		$tmp['user_id']=$id;
		$tmp['type']=$type;
		$tmp['client_id']=$data['client_id'];
		$tmp['number']=$data['number'];
		$tmp['cur']=$data['current'];
		$tmp['original']=(int)$data['original_v'];
		$tmp['place']=$data['place'];
		$tmp['date_add']=$data['date_create'];
		$tmp['date_sell']=$data['date_sell'];   
		$tmp['date_deadline']=$data['date_deadline'];
		$tmp['sell_type']=$data['cash_type'];
		$tmp['sell_time']=$data['cash_date']=="list"?$data['cash_date_list']:date("Y-m-d",strtotime($data['cash_date_date']));
		$tmp['coment']=$data['coment'];
		$tmp['sum']=str_replace("PLN","",$data['cash_sum']);
		$tmp['date_create']=date("Y-m-d H:i:s");
		$tmp['pay']=$data['pay'];
		$tmp['sum_pay']=str_replace("PLN","",$data['cash_pay']);
		$tmp['lang']=$data['lang'];
		if($period) { $tmp['is_ok']=0; }
		$DB->AutoExecute("invoice",$tmp,"INSERT");
		$idi=$DB->Insert_ID();
		//dodanie danych
		$this->add_data($idi, $data, "user");
		$this->add_data($idi, $data, "client");
		$this->add_data($idi, $data, "branch");
		//dodanie produktów
		$c=1;
		for($a=1;$a<=$count;$a++)
		{
			if($data["p_".$a."_name"])	
			{
				$this->add_product($idi, $data, $c, $a);
				$c++;
			}
		}
		//utworzenie pliku językowego
		$txt="<?php ";
		foreach($data as $k=>$v)
		{
			$t=str_replace("lang_","",$k);
			if($k!=$t)
			{
				if(is_array($v))
				{
					foreach($v as $i=>$l)
					{ $txt.='$lang[\''.$t.'\'][\''.$i.'\']="'.$l.'";'; }
				}
				else { $txt.='$lang[\''.$t.'\']="'.$v.'";'; }
			}
		}
		$txt.=" ?>";
		$file=fopen(ROOT_DIR."files/invoice_lang/".$idi.".php","wt");
		fputs($file,$txt,strlen($txt));
		fclose($file);
		//dodanie jeżeli okresowa
		if($data['period'] AND $idi)
		{
			$tmp=array();
			$tmp['user_id']=$id;
			$tmp['invoice_id']=$idi;
			$tmp['priod']=$data['period_time'];
			$tmp['value']=$data['period_value'];
			$tmp['date_start']=$data['period_start'];
			$tmp['date_next']=date("Y-m-d",strtotime($tmp['date_start']." +".$tmp['value']." ".$tmp['period'].($tmp['value']>1?"s":"")));
			$DB->AutoExecute("invoice_auto",$tmp,"INSERT");
		}
		return "ok";
	}
	/**
	 * Aktualizacja faktury
	 * @param int $id identyfikator użytkownika
	 * @param array $data tablica danych
	 * @param int $count identyfikator ostatniego produktu
	 * @param boolean $period faktura okresowa
	 */
	public function save($id, $uid, $data, $count)
	{
		global $DB;
		//dodanie faktury
		$tmp=array();
		$tmp['number']=$data['number'];
		$tmp['place']=$data['place'];
		$tmp['date_add']=$data['date_create'];
		$tmp['date_sell']=$data['date_sell'];   
		$tmp['date_deadline']=$data['date_deadline'];
		$tmp['sell_type']=$data['cash_type'];
		$tmp['sell_time']=$data['cash_date'];
		//$tmp['cur']=$data['current'];
		$tmp['sum']=str_replace("PLN","",$data['cash_sum']);
		$tmp['date_create']=date("Y-m-d H:i:s");
		$tmp['pay']=$data['pay'];
		$tmp['sum_pay']=str_replace("PLN","",$data['cash_pay']);
		$DB->AutoExecute("invoice",$tmp,"UPDATE","`id`='".$id."'");
		//dodanie danych
		$this->save_data($id, $data, "user");
		$this->save_data($id, $data, "client");
		$this->save_data($id, $data, "branch");
		//dodanie produktów
		$c=1;
		for($a=1;$a<=$count;$a++)
		{
			if($data["p_".$a."_name"])	
			{
				$this->save_product($id, $data, $c, $a);
				$c++;
			}
		}
		return "ok";
	}
	/**
	 * Usunięcie faktury
	 * @param int $id identyfikator faktury
	 * @param int $user_id identyfikator użytkownika
	 */
	public function del($id, $user_id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `invoice` WHERE `id`='".$id."' AND `user_id`='".$user_id."' LIMIT 0,1");
		if($r->fields['id'])
		{
			$DB->Execute("DELETE FROM `invoice` WHERE `id`='".$id."' LIMIT 1");
			$DB->Execute("DELETE FROM `invoice_auto` WHERE `invoice_id`='".$id."'");
			$DB->Execute("DELETE FROM `invoice_data` WHERE `invoice_id`='".$id."'");
			$DB->Execute("DELETE FROM `invoice_product` WHERE `invoice_id`='".$id."'");
		}
	}
	/**
	 * Usunięcie faktury - PDF
	 * @param string $file adres pliku
	 * @param int $user_id identyfikator użytkownika
	 */
	public function del_file($file, $user_id)
	{
		$tmp=explode("__",$file);
		if($user_id==$tmp[2]);
		{ @unlink(ROOT_DIR.str_replace("__","/",$file)); }
	}
	/**
	 * Dodanie danych uzytkownika/kontrahenta do zapisanej faktury
	 * @param int $id identyfikator faktury
	 * @param array $data tablica danych
	 * @param string $type typ danych [user,client]
	 */
	private function add_data($id, $data, $type)
	{
		global $DB;
		global $func;
		global $search;
		//if($data[$type."_name"] || $data[$type."_address"])
		//{
  		$tmp=array();
  		$tmp['invoice_id']=$id;
  		$tmp['type']=$type;
  		$tmp['name']=$data[$type."_name"];
  		$tmp['address']=$data[$type."_address"];
  		$tmp['nip']=$data[$type."_nip"];
      $tmp['nipc']=functions::trimNip($tmp['nip']);
  		$tmp['phone']=$data[$type."_phone"];
  		$tmp['email']=$data[$type."_mail"];
  		$tmp['krs']=$data[$type."_krs"];
  		$tmp['bank_name']=$data[$type."_bank_name"];
  		$tmp['account']=$data[$type."_account"];   
  		$tmp['www']=$data[$type."_www"];
  		$tmp['name_s']=$search->parse($data[$type."_name"],1);
  		$tmp['address_s']=$search->parse($data[$type."_address"],1);
  		$tmp['name_url']=$func->create_url($data[$type."_name"]);
  		$DB->AutoExecute("invoice_data",$tmp,"INSERT");
		//}
	}
	/**
	 * Aktualizacja danych uzytkownika/kontrahenta do zapisanej faktury
	 * @param int $id identyfikator faktury
	 * @param array $data tablica danych
	 * @param string $type typ danych [user,client]
	 */
	private function save_data($id, $data, $type)
	{
		global $DB;
		global $func;
		global $search;
		$tmp=array();
		$tmp['name']=$data[$type."_name"];
		$tmp['address']=$data[$type."_address"];
		$tmp['nip']=$data[$type."_nip"];
    $tmp['nipc']=functions::trimNip($tmp['nip']);
		$tmp['phone']=$data[$type."_phone"];
		$tmp['email']=$data[$type."_mail"];
		$tmp['krs']=$data[$type."_krs"];
		$tmp['bank_name']=$data[$type."_bank_name"];
		$tmp['account']=$data[$type."_account"];  
		$tmp['www']=$data[$type."_www"];
		$tmp['name_s']=$search->parse($data[$type."_name"],1);
		$tmp['address_s']=$search->parse($data[$type."_address"],1);
		$tmp['name_url']=$func->create_url($data[$type."_name"]);
		if($tmp['name']) { $DB->AutoExecute("invoice_data",$tmp,"UPDATE","`invoice_id`='".$id."' AND `type`='".$type."'"); }
	}
	/**
	 * Dodane produktu do zapisanej faktury
	 * @param int $id identyfikator faktury
	 * @param array $data tablica danych
	 * @param int $count numer produktu
	 * @param int $idp identyfikator produktu
	 */
	private function add_product($id, $data, $count, $idp)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['invoice_id']=$id;
		$tmp['count']=$count;
		$tmp['name']=$data["p_".$idp."_name"];
		$tmp['name_s']=$search->parse($data["p_".$idp."_name"],1);
		$tmp['pkwiu']=$data["p_".$idp."_pkwiu"];
		$tmp['unit']=$data["p_".$idp."_unit"];
		$tmp['vat']=$data["p_".$idp."_vat"];
		$tmp['netto']=str_replace(",",".",$data["p_".$idp."_netto"]);
		$tmp['rabat']=str_replace(",",".",$data["p_".$idp."_rabat"]);
		$tmp['amount']=str_replace(",",".",$data["p_".$idp."_amount"]);
		$DB->AutoExecute("invoice_product",$tmp,"INSERT");
	}
	/**
	 * Aktualizacja produktu do zapisanej faktury
	 * @param int $id identyfikator faktury
	 * @param array $data tablica danych
	 * @param int $count numer produktu
	 * @param int $idp identyfikator produktu
	 */
	private function save_product($id, $data, $count, $idp)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['count']=$count;
		$tmp['name']=$data["p_".$idp."_name"];
		$tmp['name_s']=$search->parse($data["p_".$idp."_name"],1);
		$tmp['pkwiu']=$data["p_".$idp."_pkwiu"];
		$tmp['unit']=$data["p_".$idp."_unit"];
		$tmp['vat']=$data["p_".$idp."_vat"];
		$tmp['netto']=str_replace(",",".",$data["p_".$idp."_netto"]);
		$tmp['rabat']=str_replace(",",".",$data["p_".$idp."_rabat"]);
		$tmp['amount']=str_replace(",",".",$data["p_".$idp."_amount"]);
		$DB->AutoExecute("invoice_product",$tmp,"UPDATE","`count`='".$data["p_".$idp."_id"]."' AND `invoice_id`='".$id."'");
	}
	
	public function pro2vat($user_id, $id)
	{
    global $DB;
    $r=$DB->Execute("SELECT * FROM `invoice` WHERE `id`='".$id."' AND `user_id`='".$user_id."' LIMIT 0,1");
    if($r->fields['id'])
    {
      require(ROOT_DIR."module/panel/data_number/class.php");
      $nr=new user_number(USER_ID,2);
      $t=$r->GetRowAssoc();
      $t['id']='';
      $t['type']=2; 
      $t['number']=$nr->get_nr();
      $t['date_add']=date("Y-m-d");
      $nr->up_nr(USER_ID,2);
      $DB->AutoExecute("invoice",$t,"INSERT");
      $inv=$DB->Insert_ID();
      //dane
      $r=$DB->Execute("SELECT * FROM `invoice_data` WHERE `invoice_id`='".$id."'");
      if($r->fields['id'])
      {
        while(!$r->EOF)
        {
          $t=$r->GetRowAssoc();
          $t['id']='';
          $t['invoice_id']=$inv;
          $DB->AutoExecute("invoice_data",$t,"INSERT"); 
          $r->MoveNext(); 
        }
      }
      //produkty
      $r=$DB->Execute("SELECT * FROM `invoice_product` WHERE `invoice_id`='".$id."'");
      if($r->fields['id'])
      {
        while(!$r->EOF)
        {
          $t=$r->GetRowAssoc();
          $t['id']='';
          $t['invoice_id']=$inv;
          $DB->AutoExecute("invoice_product",$t,"INSERT");
          $r->MoveNext();  
        }
      }
    }
  }
	/**
	 * Odczyt danych faktury
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator faktury
	 * @return mixed obiekt faktury
	 */
	public function read_one($user_id, $id)
	{
		global $DB;
		global $func;
		$r=$DB->Execute("SELECT * FROM `invoice` WHERE `id`='".$id."' AND `user_id`='".$user_id."' LIMIT 0,1");
		$inv=new invoice_main($r->fields['client_id'],$r->fields['number'],$r->fields['cur'],$r->fields['original'],$r->fields['place'],$r->fields['date_add'],$r->fields['date_sell'],$r->fields['date_deadline'],$r->fields['sell_type'],$r->fields['sell_time'],$r->fields['coment'],$r->fields['sum_pay'],$r->fields['type'],$r->fields['lang']);
		//odczyt produktów
		if($r->fields['id'])
		{
			$r=$DB->Execute("SELECT * FROM `invoice_product` WHERE `invoice_id`='".$id."' ORDER BY `count` ASC");
			if($r->fields['id'])
			{
				while(!$r->EOF)
				{
					$inv->product[]=new invoice_product($r->fields['name'],$r->fields['pkwiu'],$r->fields['unit'],$r->fields['vat'],$r->fields['netto'],$r->fields['amount'],$r->fields['rabat']);
					$r->MoveNext();
				}
			}
			return $inv;
		}
		return null;
	}
	/**
	 * Odczyt listy zapisanych faktur dla klienta
	 * @param int $id identyfikator użytkownika
	 * @param int $client identyfikator kontrahenta
	 * @return array tablica obiektów faktury
	 */
	public function read_list_base($id, $client=0)
	{
		global $DB;
		$tmp=array();
		$war=is_numeric($client)?"invoice.client_id='".$client."'":"invoice_data.name_url='".$client."' AND invoice_data.type='client'";
		$r=$DB->Execute("SELECT invoice.id,invoice.number,invoice.cur,invoice.payed,invoice.type,invoice.date_create,invoice.sum,invoice.sum_pay FROM invoice INNER JOIN invoice_data ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND ".$war." GROUP BY invoice.id ORDER BY invoice.date_create DESC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp[]=new invoice_list($r->fields['id'],$client,$r->fields['number'],$r->fields['cur'],$r->fields['payed'],$r->fields['type'],$r->fields['date_create'],null,null,null,null,$r->fields['sum'],$r->fields['sum_pay']);
				$r->MoveNext();
			}
		}
		return $tmp;
	} 
	/**
	 * Odczyt listy faktur PDF dla klienta
	 * @param int $id identyfikator użytkownika
	 * @param int $client identyfikator kontrahenta
	 * @return array tablica obiektów faktury
	 */
	public function read_list_pdf($id, $client=0)
	{
		if(is_numeric($client))
		{
			$dir=$this->check_dir($id, $client);
			$tmp=array();
			$kat=opendir($dir);
			while($plik=readdir($kat))
			{
				$file=new invoice_file($dir.$plik);
				if($file->name) { $tmp[]=$file; }
			}
			//posortowanie
			uasort($tmp,"sort_date");
			return $tmp;
		}
		else { return array(); }
	}
	/**
	 * Lista wszystkich faktur
	 * @param int $id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $sort rodzaj sortowania
	 * @param string $letter litera od której zaczyna się nazwa kontrahenta
	 * @param boolean $period odczyt faktur okresowych
	 * @return array tablica z danymi
	 */
	public function read_all($id, $client, $page=1, $ile=20, $sort="dn", $letter="", $period=false)
	{
		global $DB;
		global $func;
		$letter=$letter=="-"?"":$letter;
		$page=(int)$page?(int)$page:1;
		//sortowanie
		switch(substr($sort,1,1))
		{
			case "c": $s="invoice.date_create"; break;
			case "a": $s="invoice.date_add"; break;
			case "s": $s="invoice.date_sell"; break;
			case "p": $s="invoice.payed"; break;
			case "n": $s="STR_TO_DATE(invoice.number,'%d/%m/%Y')"; break;
		}
		$s.=" ".(substr($sort,0,1)=="d"?"DESC":"ASC");
		//$s=(substr($sort,1,1)=="c"?"`date_create`":(substr($sort,1,1)=="a"?"`date_add`":"`date_sell`"))." ".(substr($sort,0,1)=="d"?"DESC":"ASC");
		//kontrahenci
		$clients=array();$c=array();
		$r=$DB->Execute("SELECT `id`,`name`,`sign` FROM `clients` WHERE `user_id`='".$id."'".($client?" AND `id`='".$client."'":""));
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$clients[$r->fields['id']]=$func->show_with_html($r->fields['name']);
				if(substr(strtolower($r->fields['sign']),0,1)==$letter OR substr(strtolower($r->fields['name']),0,1)==$letter OR $letter=="") { $c[]=$r->fields['id']; }
				$r->MoveNext();
			}
		}
		//faktury
		$war=$letter?"AND ((invoice_data.name LIKE '".$letter."%' AND invoice_data.type='client') ".(count($c)?"OR invoice.client_id in (".implode(",",$c).")":"").")":"";
		$dane=array();
		$r=$DB->PageExecute("SELECT invoice.id,invoice.number,invoice.cur,invoice.payed,invoice.type,invoice.date_create,invoice.client_id,invoice.date_add,invoice.date_sell,invoice.date_deadline,invoice_data.name,invoice_data.nip,invoice.sum,invoice.sum_pay FROM invoice INNER JOIN invoice_data ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND invoice.is_ok='".($period?"0":"1")."' AND invoice_data.type='client' ".$war.($client?" AND invoice.client_id='".$client."'":"")." GROUP BY invoice.id ORDER BY ".$s,$ile,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_list($r->fields['id'],$r->fields['client_id'],$r->fields['number'],$r->fields['cur'],$r->fields['payed'],$r->fields['type'],$r->fields['date_create'],(($r->fields['client_id'] AND in_array($r->fields['client_id'],$c)!==false)?$clients[$r->fields['client_id']]:$r->fields['name']),$r->fields['date_add'],$r->fields['date_sell'],$r->fields['date_deadline'],$r->fields['sum'],$r->fields['sum_pay'],$r->fields['nip']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Lista wszystkich faktur - wyszukiwarka
	 * @param int $id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $sort rodzaj sortowania
	 * @param boolean $period odczyt faktur okresowych
	 * @return array tablica z danymi
	 */
	public function read_search($id, $client, $page=1, $ile=20, $sort="dc", $letter="", $number=null, $name=null, $nip=null, $damin=null, $damax=null, $dsmin=null, $dsmax=null, $smin=null, $smax=null, $date=null, $period=false)
	{
		global $DB;$DB->debug=false;
		global $func;
		global $search;
		$letter=$letter=="-"?"":$letter;
		$page=(int)$page?(int)$page:1;
		$number=str_replace("-sl-","/",$number);
		//warunki
		$war_number=$number?" AND invoice.number LIKE '%".$number."%'":"";
		if($name)
		{
			list($and, $not)=$search->phrase($name);
			$war_name=" AND search_engine(invoice_data.name_s,'".$and."','".$not."')";
		}
		else { $war_name=""; }
		$war_nip=$nip?" AND invoice_data.nipc LIKE '".functions::trimNip($nip)."%'":"";
    if(!$war_name)
    {
		$war_damin=$damin?" AND invoice.date_add >= '".$damin."'":"";
		$war_damax=$damax?" AND invoice.date_add <= '".$damax."'":"";
		$war_dsmin=$dsmin?" AND invoice.date_sell >= '".$dsmin."'":"";
		$war_dsmax=$dsmax?" AND invoice.date_sell <= '".$dsmax."'":"";
    }
		$war_smin=$smin?" AND invoice.sum >= '".$smin."'":"";
		$war_smax=$smax?" AND invoice.sum <= '".$smax."'":"";
		$war_date=$date?" AND DATE_FORMAT(invoice.date_create,'%Y-%m') = '".$date."'":"";
		$war=$war_number.$war_name.$war_nip.$war_damin.$war_damax.$war_dsmin.$war_dsmax.$war_smin.$war_smax.$war_date;
		//sortowanie
		switch(substr($sort,1,1))
		{
			case "c": $s="invoice.date_create"; break;
			case "a": $s="invoice.date_add"; break;
			case "s": $s="invoice.date_sell"; break;
			case "p": $s="invoice.payed"; break;
			case "n": $s="STR_TO_DATE(invoice.number,'%d/%m/%Y')"; break;
		}
		$s.=" ".(substr($sort,0,1)=="d"?"DESC":"ASC");
    //$DB->debug=true;
		//$s=(substr($sort,1,1)=="c"?"invoice.date_create":(substr($sort,1,1)=="a"?"invoice.date_add":"invoice.date_sell"))." ".(substr($sort,0,1)=="d"?"DESC":"ASC");
		//kontrahenci
		$clients=array();$c=array();//$DB->debug=true;
		$r=$DB->Execute("SELECT `id`,`name`,`sign` FROM `clients` WHERE `user_id`='".$id."'".($client?" AND `id`='".$client."'":""));
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$clients[$r->fields['id']]=$func->show_with_html($r->fields['name']);
				if(substr(strtolower($r->fields['sign']),0,1)==$letter OR substr(strtolower($r->fields['name']),0,1)==$letter OR $letter=="") { $c[]=$r->fields['id']; }
				$r->MoveNext();
			}
		}
		//faktury
		$warl=$letter?"AND ((invoice_data.name LIKE '".$letter."%' AND invoice_data.type='client') ".(count($c)?"OR invoice.client_id in (".implode(",",$c).")":"").")":"";
		$dane=array();
		$r=$DB->PageExecute("SELECT invoice.id,invoice.number,invoice.cur,invoice.payed,invoice.type,invoice.date_create,invoice.client_id,invoice.date_add,invoice.date_sell,invoice.date_deadline,invoice_data.name,invoice_data.nip,invoice.sum,invoice.sum_pay FROM invoice INNER JOIN invoice_data ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND invoice.is_ok='".($period?"0":"1")."' AND invoice_data.type='client' ".$war.$warl.($client?" AND invoice.client_id='".$client."'":"")." ORDER BY ".$s,$ile,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_list($r->fields['id'],$r->fields['client_id'],$r->fields['number'],$r->fields['cur'],$r->fields['payed'],$r->fields['type'],$r->fields['date_create'],(($r->fields['client_id'] AND in_array($r->fields['client_id'],$c)!==false)?$clients[$r->fields['client_id']]:$r->fields['name']),$r->fields['date_add'],$r->fields['date_sell'],$r->fields['date_deadline'],$r->fields['sum'],$r->fields['sum_pay'],$r->fields['nip']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Odczyt ilości faktur dla kontrahenta
	 * @param int $id identyfikator użytkownika
	 * @param int $client identyfikator kontrahenta
	 * @return int ilość faktur
	 */
	public function count_all($id, $client=0)
	{
		global $DB;
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `invoice` WHERE `user_id`='".$id."' AND `client_id`='".$client."'");
		return $r->fields['ilosc']+$this->count_pdf($id,$client);
	}
	/**
	 * Odczyt ilości faktur PDF dla kontrahenta
	 * @param int $id identyfikator użytkownika
	 * @param int $client identyfikator kontrahenta
	 * @return int ilość faktur
	 */
	public function count_pdf($id, $client=0)
	{
		$dir=$this->check_dir($id, $client);
		$count=0;
		$kat=opendir($dir);
		while($file=readdir($kat))
		{ $count+=is_file($dir.$file)?1:0; }
		closedir($kat);
		return $count;
	}
	/**
	 * Lista kontrahentów
	 * @param int $id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $letter pierwsza litera sygnatury
	 * @return array tablica z danymi
	 */
	public function client_list($id, $page=1, $ile=20, $letter="")
	{
		global $DB;
		$page=(int)$page?(int)$page:1;
		$dane=array();
		//kontrahenci
		$war=$letter?"AND clients.sign LIKE '".$letter."%'":"";
		$r=$DB->PageExecute("SELECT clients.id, clients.sign, clients.name, COUNT(invoice.id) AS `ilosc` FROM `invoice` INNER JOIN `clients` ON invoice.client_id=clients.id WHERE invoice.user_id='".$id."' AND clients.user_id='".$id."' ".$war." GROUP BY invoice.client_id ORDER BY clients.name ASC", $ile*$page, 1);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$c=$this->count_pdf($id,$r->fields['id']);
				$dane[]=new invoice_client($r->fields['id'],$r->fields['sign'],$r->fields['name'],($r->fields['ilosc']+$c));
				$r->MoveNext();
			}
		}
		//pozostali
		$war=$letter?"AND invoice_data.name_url LIKE '".$letter."%'":"";
		$r=$DB->PageExecute("SELECT invoice_data.name, invoice_data.name_url, COUNT(invoice.id) AS `ilosc` FROM `invoice` INNER JOIN `invoice_data` ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND invoice.client_id='0' AND invoice_data.type='client' ".$war." GROUP BY invoice_data.name_url ORDER BY invoice_data.name ASC", $ile*$page, 1);
		if($r->fields['name_url'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_client(0,$r->fields['name_url'],$r->fields['name'],$r->fields['ilosc']);
				$r->MoveNext();
			}
		}
		//posortowanie
		uasort($dane,"sort_name");
		$p=ceil(count($dane)/$ile);
		return array(array_slice($dane,($page-1)*$ile,$ile),$page,$p);
	}
	/**
	 * Lista kontrahentów - wyszukiwanie
	 * @param int $id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $name fraza nazwy
	 * @param string $address fraza adresu
	 * @param string $nip fraza numeru NIP
	 * @return array tablica z danymi 
	 */
	public function client_list_search($id, $page=1, $ile=20, $name=null, $address=null, $nip=null)
	{
		global $DB;
		global $search;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		//warunki
		if($name)
		{
			list($and, $not)=$search->phrase($name);
			$war_name=" AND (search_engine(clients.name_s,'".$and."','".$not."') OR search_engine(clients.sign_s,'".$and."','".$not."'))";
		}
		else { $war_name=""; }
		if($address)
		{
			list($and, $not)=$search->phrase($address);
			$war_address=" AND search_engine(clients.address_s,'".$and."','".$not."')";
		}
		else { $war_address=""; }
		$war_nip=$nip?" AND clients.nipc LIKE '".functions::trimNip($nip)."%'":"";
		//kontrahenci
		$r=$DB->PageExecute("SELECT clients.id, clients.sign, clients.name, COUNT(*) AS `ilosc` FROM `invoice` INNER JOIN `clients` ON invoice.client_id=clients.id WHERE clients.user_id='".$id."'".$war_name.$war_address.$war_nip." GROUP BY invoice.client_id ORDER BY clients.name ASC",$ile*$page,1);
		if($r->fields['name'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_client($r->fields['id'],$r->fields['sign'],$r->fields['name'],$r->fields['ilosc']);
				$r->MoveNext();
			}
		}
		//warunki
		if($name)
		{
			list($and, $not)=$search->phrase($name);
			$war_name=" AND search_engine(invoice_data.name_s,'".$and."','".$not."')";
		}
		else { $war_name=""; }
		$war=str_replace("clients.","invoice_data.",$war_name.$war_address.$war_nip);
		//pozostali
		$r=$DB->PageExecute("SELECT invoice_data.name, invoice_data.name_url, COUNT(invoice.id) AS `ilosc` FROM `invoice` INNER JOIN `invoice_data` ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND invoice.client_id='0' AND invoice_data.type='client' ".$war." GROUP BY invoice_data.name_url ORDER BY invoice_data.name ASC",$ile*$page,1);
		if($r->fields['name_url'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_client(0,$r->fields['name_url'],$r->fields['name'],$r->fields['ilosc']);
				$r->MoveNext();
			}
		}
		//posortowanie
		uasort($dane,"sort_name");
		$p=ceil(count($dane)/$ile);
		return array(array_slice($dane,($page-1)*$ile,$ile),$page,$p);
	}
	/**
	 * Litery od których zaczynają się sygnatury kontrahentów
	 * @param int $id identyfikator użytkownika
	 * @param boolean $all pokazywać nabywców
	 * @param boolean $period odczyt faktur okresowych
	 * @return array tablica liter
	 */
	public function letter_list($id, $all=false, $period=false)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(clients.sign),1,1) as `letter` FROM `invoice` INNER JOIN `clients` ON invoice.client_id=clients.id WHERE clients.user_id='".$id."' AND invoice.is_ok='".($period?"0":"1")."' GROUP BY SUBSTRING(clients.sign,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		if($all)
		{
			$r=$DB->Execute("SELECT SUBSTRING(LOWER(invoice_data.name),1,1) as `letter` FROM `invoice` INNER JOIN `invoice_data` ON invoice.id=invoice_data.invoice_id WHERE invoice.user_id='".$id."' AND invoice_data.type='client' AND invoice.is_ok='".($period?"0":"1")."' GROUP BY SUBSTRING(invoice_data.name,1,1)");
			if($r->fields['letter'])
			{
				while(!$r->EOF)
				{
					if(array_search($r->fields['letter'],$dane)===false) { $dane[]=$r->fields['letter']; }
					$r->MoveNext();
				}
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Poprzedni kontrahent
	 * @param int $id
	 * @param string $name
	 */
	public function prev_client($id, $name)
	{
		global $DB;
		$r=$DB->Execute("SELECT clients.id FROM `invoice` INNER JOIN `clients` ON invoice.client_id=clients.id WHERE invoice.user_id='".$id."' AND clients.user_id='".$id."' AND clients.sign<'".$name."'  ORDER BY clients.sign DESC LIMIT 0,1");
		return $r->fields['id'];
	}
	/**
	 * Nastepny kontrahent
	 * @param int $id
	 * @param string $name
	 * @return int id kontrahenta
	 */
	public function next_client($id, $name)
	{
		global $DB;
		$r=$DB->Execute("SELECT clients.id FROM `invoice` INNER JOIN `clients` ON invoice.client_id=clients.id WHERE invoice.user_id='".$id."' AND clients.user_id='".$id."' AND clients.sign>'".$name."'  ORDER BY clients.sign ASC LIMIT 0,1");
		return $r->fields['id'];
	}
	/**
	 * Sprawdzenie katalogu do zapisu i ewentualne stworzenie go
	 * @param int $id identyfikator użytkownika
	 * @param int $client identyfikator klienta
	 * @return string ścieżka do katalogu
	 */
	private function check_dir($id, $client=0)
	{
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/";
		if(!is_dir($dir)) { mkdir($dir,0777,true); }
		if(!is_writable($dir)) { chmod($dir,0777); }
		$dir=ROOT_DIR."files/invoice_pdf/".$id."/".$client."/";
		if(!is_dir($dir)) { mkdir($dir,0777,true); }
		if(!is_writable($dir)) { chmod($dir,0777); }
		return $dir;
	}
	/**
	 * Ilość faktur z danego miesiąca
	 * @param int $id identyfikator użytkownika
	 * @param int $type typ faktury
	 * @return int ilość faktur
	 */
	public function counter($id, $type=1)
	{
		global $DB;
		$r=$DB->Execute("SELECT COUNT(*) as `ilosc` FROM `invoice` WHERE `user_id`='".$id."' AND `type`='".$type."' AND DATE_FORMAT(`date_create`,'%Y-%m')='".date("Y-m")."'");
		return $r->fields['ilosc'];
	}
	/**
	 * Odczyt faktur za abonament
	 * @return array tablica danych faktur
	 */
	public function licence_invoice()
	{
		global $DB;
		$dane=array();
		//$r=$DB->Execute("SELECT p.month, i.id, p.id AS pid, u.login, p.vat FROM invoice i INNER JOIN pay_sys_platnosci_pl p ON i.pay=p.id INNER JOIN users u ON p.user_id=u.id WHERE p.vat!='end' AND p.vat!='no' ORDER BY p.vat ASC");
		$r=$DB->Execute("SELECT p.month, p.id AS pid, u.login, p.vat, p.user_id FROM pay_sys_platnosci_pl p INNER JOIN users u ON p.user_id=u.id WHERE p.vat!='end' AND p.vat!='no' ORDER BY u.login ASC");
		if($r->fields['pid'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				//$tmp['id']=$r->fields['id'];
				$tmp['pid']=$r->fields['pid'];
				$tmp['login']=$r->fields['login'];
				$tmp['month']=$r->fields['month'];
				$tmp['vat']=$r->fields['vat'];
				$tmp['user_id']=$r->fields['user_id'];
				$dane[]=$tmp;
				$r->MoveNext();
			}
		}
		return $dane;
	}
	/**
	 * Zatwierdzebnie faktury
	 * @param int $id identyfikator faktury
	 * @param int $user_id identyfikator użytkownika
	 */
	public function is_ok($id, $user_id)
	{
		global $DB;
		$DB->Execute("UPDATE `invoice` SET `is_ok`='1' WHERE `id`='".$id."' AND `user_id`='".$user_id."'");
	}
	/**
	 * Uaktualnienie zapłaconych faktur
	 * @param int $uid identyfikator użytkownika
	 * @param array $data tablica identyfikatorów faktur
	 * @return string status wykonania
	 */
	public function save_payed($uid, $data)
	{
		global $DB;
		$DB->Execute("UPDATE `invoice` SET `payed`='0' WHERE `user_id`='".$uid."' AND `id` in (".implode(",",$data['payed_all']).")");
		$DB->Execute("UPDATE `invoice` SET `payed`='1' WHERE `user_id`='".$uid."' AND `id` in (".implode(",",$data['payed']).")");
		return "ok";
	}
	
}
/**
 * Obiekt listy faktur
 * @author kordan
 *
 */
class invoice_list
{
	public $id;
	public $client;
	public $number;
	public $payed;
	public $type;
	public $date_create;
	public $client_name;
	public $date_add;
	public $date_sell;  
	public $date_deadline;
	public $client_url;
	public $sum;
	public $sum_pay;
	public $nip; 
	public $cur;
	/**
	 * Konstruktor
	 * @param int $id identyfikator faktury
	 * @param int $client identyfikator kontrahenta
	 * @param string $number numer faktury
	 * @param boolean $payed czy faktura jest zapłacona
	 * @param int $type typ faktury
	 * @param datetime $date_create data utworzenia
	 * @param string $client_name nazwa kontrahenta
	 * @param datetime $date_add data wystawienia
	 * @param datetime $date_sell data sprzedaży
	 * @param int $sum suma faktury
	 * @param int $sum_pay zapłacona suma faktury
	 * @param string $nip numer NIP
	 */
	public function __construct($id, $client, $number, $cur, $payed, $type, $date_create, $client_name="", $date_add=null, $date_sell=null, $date_deadline=null, $sum=0, $sum_pay=0, $nip="")
	{
		global $func;
		$this->id=$id;
		$this->client=$client;
		$this->number=$number;
		$this->type=$type;
		$this->payed=$payed;
		$this->date_create=$date_create;
		$this->client_name=$client_name;
		$this->date_add=$date_add;
		$this->date_sell=$date_sell;     
		$this->date_deadline=$date_deadline;
		$this->client_url=$func->create_url($client_name);
		$this->sum=$sum;
		$this->sum_pay=$sum_pay;
		$this->nip=$nip; 
		$this->cur=$cur;
	}
}
/**
 * Głowne dane faktury
 * @author kordan
 *
 */
class invoice_main
{
	public $client;
	public $number;
	public $original;
	public $place;
	public $date_add;
	public $date_sell;
	public $date_deadline;
	public $sell_type;
	public $sell_date;
	public $sell_time;
	public $coment;
	public $product;
	public $sum_pay;
	public $cur;   
	public $type;
	public $lang;
	/**
	 * Kontruktor
	 * @param int $client identyfikator kontrahenta
	 * @param string $number numer faktury
	 * @param boolean $original czy przekreślić napis oryginał
	 * @param string $place miejsce wystawienia
	 * @param datetime $date_add data wystawienia
	 * @param datetime $date_sell data sprzedaży
	 * @param string $sell_type sposób zapłaty
	 * @param string $sell_time termin zapłaty
	 * @param string $coment komentarz
	 * @param int $sum_pay zapłacona kwota faktury
	 */
	public function __construct($client, $number, $cur="PLN", $original=null, $place=null, $date_add=null, $date_sell=null, $date_deadline = null, $sell_type=null, $sell_time=null, $coment=null, $sum_pay=null, $type=null, $lang="pl")
	{
		global $func;
		$this->client=$client;
		$this->number=$number;
		$this->cur=$cur;
		$this->original=$original;
		$this->place=$place;
		$this->date_add=$date_add;
		$this->date_sell=$date_sell=='0000-00-00'?'':$date_sell;
		$this->sell_type=$sell_type;
		$this->sell_date=(strlen($sell_time)==10 AND substr($sell_time,0,1)==2)?"date":"list"; 
		$this->date_deadline=$date_deadline;
		$this->sell_time=$sell_time;
		$this->coment=$func->show_with_html($coment);
		$this->product=array();
		$this->sum_pay=$sum_pay;   
		$this->type=$type;   
		$this->lang=$lang;
	}
}
/**
 * Dane do faktury
 * @author kordan
 *
 */
class invoice_data
{
	public $name;
	public $address;
	public $nip;
	public $phone;
	public $email;
	public $bank_name;
	public $krs;
	public $account; 
	public $www;
	/**
	 * Konstruktor
	 * @param int $id identyfikator faktury
	 * @param stryin $type typ danych [user,client]
	 */
	public function __construct($id=0, $type='user')
	{
		if($id)
		{
			global $DB;
			global $func;
			$r=$DB->Execute("SELECT * FROM `invoice_data` WHERE `invoice_id`='".$id."' AND `type`='".$type."' LIMIT 0,1");
			$this->name=$func->show_with_html($r->fields['name']);
			$this->address=$func->show_with_html($r->fields['address']);
			$this->nip=$r->fields['nip'];
			$this->phone=$func->show_with_html($r->fields['phone']);
			$this->email=$func->show_with_html($r->fields['email']);
			$this->bank_name=$r->fields['bank_name'];
			$this->krs=$r->fields['krs'];
			$this->account=$r->fields['account']; 
			$this->www=$r->fields['www'];
		}                                        
	}
	/**
	 * Odczyt po url
	 * @param int $url nazwa usera
	 * @param stryin $type typ danych [user,client]
	 */
	public function read_url($url, $type)
	{
		global $DB;
		global $func;
		$r=$DB->Execute("SELECT * FROM `invoice_data` WHERE `name_url` LIKE '".$url."' AND `type`='".$type."' LIMIT 0,1");
		$this->name=$func->show_with_html($r->fields['name']);
		$this->address=$func->show_with_html($r->fields['address']);
		$this->nip=$r->fields['nip'];
		$this->phone=$func->show_with_html($r->fields['phone']);
		$this->email=$func->show_with_html($r->fields['email']);
		$this->bank_name=$r->fields['bank_name'];
		$this->krs=$r->fields['krs'];
		$this->account=$r->fields['account'];  
		$this->www=$r->fields['www'];
	}
}
/**
 * Produkt do faktury
 * @author kordan
 *
 */
class invoice_product
{
	public $name;
	public $pkwiu;
	public $unit;
	public $vat;
	public $netto;
	public $amount;
	public $rabat;
	/**
	 * Konstruktor
	 * @param string $name nazwa produktu
	 * @param string $pkwiu numer PKWiU
	 * @param string $unit jednostka
	 * @param decimal $vat stawka VAT
	 * @param decimal $netto cena netto
	 * @param decimal $amount ilość sztuk
	 * @param decimal $rabat wysokość rabatu
	 */
	public function __construct($name="", $pkwiu="", $unit="", $vat=0, $netto=0, $amount=1, $rabat=0)
	{
		//$this->name=str_replace("\r","",$name);
		global $func;
    $this->name=str_replace("\r","",$func->show_with_html($name));
		$this->pkwiu=$pkwiu;
		$this->unit=$unit;
		$this->vat=$vat;
		$this->netto=$netto;
		$this->amount=$amount;
		$this->rabat=$rabat;
	}
}
/**
 * Element listy kontrahentów
 * @author kordan
 *
 */
class invoice_client
{
	public $id;
	public $sign;
	public $name;
	public $count;
	/**
	 * Konstruktor kontrahenta
	 * @param int $id identyfikator kontrahenta
	 * @param string $sign nazwa skrócona kontrahenta
	 * @param string $name nazwa kontrahenta
	 * @param int $count ilość faktur
	 */
	public function __construct($id,$sign,$name,$count=0)
	{
		global $func;
		$this->id=$id;
		$this->sign=$func->show_with_html($sign);
		$this->name=$func->show_with_html($name);
		$this->count=$count;
	}	
}
/**
 * Klasa obsługująca parametry pliku
 * @author kordan
 *
 */
class invoice_file
{
	public $name;
	public $path;
	public $weight;
	public $date;
	/**
	 * Odczytanie danych pliku
	 * @param string $path ścieżka do pliku
	 */
	public function __construct($path)
	{
		if(is_file($path))
		{
			$this->path=str_replace(ROOT_DIR,"",$path);
			$tmp=explode("/",$path);
			$this->name=$tmp[sizeof($tmp)-1];
			$this->weight=filesize($path)/1024;
			$this->date=date("Y-m-d H:i:s",fileatime($path));
		}
	}
}
/**
 * Posortowanie obiektów po dacie w kolejności malejącej
 * @param mixed $a obiekt pierwszy
 * @param mixed $b obiekt drugi
 * @return int wynik porównania
 */
function sort_date($a,$b)
{ return $a->date<$b->date?1:($a->date==$b->date?0:-1); }
/**
 * Posortowanie obiektów po nazwie w kolejności malejącej
 * @param mixed $a obiekt pierwszy
 * @param mixed $b obiekt drugi
 * @return int wynik porównania
 */
function sort_name($a,$b)
{
	global $func;
	return strcmp($func->create_url($a->sign),$func->create_url($b->sgin));
}