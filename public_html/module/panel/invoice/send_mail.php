<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
require(ROOT_DIR."includes/users.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
//utworzenie faktury
if(is_numeric($_POST['path']))
{
	require(ROOT_DIR."module/panel/invoice/class.php");
	require(ROOT_DIR."module/panel/data_extra/class.php");
	switch($_POST['fak_type'])
	{
	    case 1: $dirf="invoice"; $class="class_pdfI"; break;
	    case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
	    case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
	    case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
	    case 5: $dirf="rachunek"; $class="class_pdfR"; break;
	    default: $dirf="invoice"; $class="class_pdfI"; break;
	}
	require(ROOT_DIR."module/panel/szablon-faktury/class.php");
	$data=new user_color($_POST['user_id'],$_POST['fak_type']);
	require(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
	$inv=new invoice();
	list($tmp,$count)=$inv->get_to_pdf($_POST['path'],$_POST['user_id']); 
	foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
	$pdf=new $class($_POST['user_id'],$count,$tmp,$_POST['what_p']?$_POST['what_p']:2);
	$_POST['path']=str_replace(ROOT_DIR,"",urldecode($pdf->savePdf()));
}
//lista adresów email					
$user=array();
if(is_array($_POST['mail']))
{
	foreach($_POST['mail'] as $i)
	{ $user[]=array("mail"=>$i,"name"=>$i); }
}
if($_POST['email'])
{ $user[]=array("mail"=>$_POST['email'],"name"=>$_POST['email']); }
//faktura
$file=array();
//$file[]=array("path"=>ROOT_DIR.$_POST['path'],"name"=>"invoice.pdf");
if($dirf=="invoice") { $dirf="faktura"; }
$tmp['number']=$tmp['number']?$tmp['number']:"faktura-egrupa-pl";   
$file[]=array("path"=>ROOT_DIR.$_POST['path'],"name"=>$dirf."-".str_replace("/","-",$tmp['number']).".pdf");

//dane dodatkowe
$u=new users_invoice($_POST['user_id']);
$x=array();
$x['name']=$u->name;    
$x['number']=$tmp['number'];
$mailer=new mailer($u->mail);
$result=$mailer->send($user,"send_invoice",$x,$file);
echo ($result?"ok":"error");
?>