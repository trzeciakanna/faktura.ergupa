<?php
$lang=array();
$lang['head']="Lista kontrahentów, którym wystawiono faktury";
$lang['header']="Lista faktur dla kontrahenta";

$lang['all']="wszystkie";
$lang['invoices']="Wszystkie faktury";
$lang['sign']="Nazwa skrócona";
$lang['name']="Imię i nazwisko / nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['searcher']="Wyszukiwarka";
$lang['search']="Wyszukaj";
$lang['show']="rozwiń";
$lang['hidden']="zwiń";

$lang['invoice']="Faktury";
$lang['other']="Pozostałe faktury PDF";
$lang['base']="Zapisane faktury";
$lang['pdf']="Faktury tylko w PDF";
$lang['select']="Wybierz adres/adresy e-mail na które wysłać fakturę";
$lang['mail']="Podaj nowy adres";

$lang['view']="Zobacz";
$lang['download']="Pobierz";
$lang['close']="Zamknij";
$lang['send']="Wyślij";
$lang['back']="wstecz";
$lang['page']="Lista faktur z wyszukiwarką";

$lang['c_head']="Dane kontrahenta";
$lang['c_name']="Imię i nazwisko /<Br> nazwa firmy";
$lang['c_address']="Adres";
$lang['c_nip']="NIP";
$lang['c_phone']="Telefon";
$lang['c_email']="Adres e-mail";
$lang['c_krs']="Numer KRS";
$lang['c_bank_name']="Nazwa banku";
$lang['c_account']="Numer konta";

$lang['nagl0']="Faktury wg Kontrahentów";
$lang['nagl1']=">Faktura VAT";
$lang['nagl2']="Jesteś w";
$lang['nagl3']="Skrócona nazwa";
$lang['nagl4']="Nazwa";
$lang['nagl5']="Ilość faktur";
$lang['nagl6']="PDF";
$lang['nagl7']="Numer faktury";
$lang['nagl8']="Data wystawienia";
$lang['nagl9']="Kwota faktury";
$lang['nagl10']="Wyślij";
$lang['nagl11']="Zobacz";
$lang['nagl12']="Usuń";
$lang['nagl13']="Usuń";
$lang['nagl14']="Usuń";
$lang['nagl15']="Usuń";
$lang['nagl16']="Usuń";
$lang['nagl17']="Usuń";
$lang['nagl18']="Usuń";

$lang['save_menu0'] = 'Wydrukuj PDF';
$lang['save_menu1'] = 'Dwa egzemplarze';
$lang['save_menu2'] = 'Jeden egzemplarz';
$lang['save_menu3'] = 'Kopia';
$lang['save_menu4'] = 'Duplikat';
$lang['save_menu5'] = 'Drukuj';

$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['send_ok']="Faktura została wysłana poprawnie";
$lang['send_error']="Wystąpił błąd podczas wysyłania faktury. Spróbuj ponownie za chwilę.";
$lang['']="";
$lang['']="";
?>