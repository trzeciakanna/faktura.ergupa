<?php
global $smarty;
require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/invoice/".LANG.".php");
global $navi;

$smarty->assign("navi",$navi->navi);
global $lang;
$smarty->assign("lang",$lang);
//odczyt danych użytkownika
$inv=new invoice();
$smarty->assign("user_id",USER_ID);

if($_GET['par4']=="file")
{
	require(ROOT_DIR."module/panel/data_extra/class.php");
	require(ROOT_DIR."module/panel/szablon-faktury/class.php");
	
	list($tmp,$count)=$inv->get_to_pdf($_GET['par5'],USER_ID);
	$data=new user_color(USER_ID,$_GET['par7']);
	foreach($data->colors as $k=>$v) { $tmp[$k]=$v; }
	
	switch($_GET['par7'])
	{
	    case 1: $dirf="invoice"; $class="class_pdfI"; break;
	    case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
	    case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
	    case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
	    case 5: $dirf="rachunek"; $class="class_pdfR"; break;
	    default: $dirf="invoice"; $class="class_pdfI"; break;
	}
	require(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
    
	$pdf=new $class(USER_ID,$count,$tmp,$_GET['par6']);
	//echo $pdf->file;
	header("Location: ".ROOT_URL."/download.php?file=".$pdf->savePdf());
	exit();
}
elseif($_GET['par4']=="pro2vat")
{
	$inv->pro2vat(USER_ID,$_GET['par5']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
elseif($_GET['par4']=="is_ok")
{
	$inv->is_ok($_GET['par5'],USER_ID);
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
elseif($_GET['par4']=="del")
{
	if(is_numeric($_GET['par5']))
	{ $inv->del($_GET['par5'],USER_ID); }
	else
	{ $inv->del_file($_GET['par5'],USER_ID); }
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
elseif($_GET['par4']=="client")
{
	require(ROOT_DIR."module/panel/client_add/class.php");
	$client=new clients();
	$file=$inv->read_list_pdf(USER_ID,$_GET['par5']);
	$base=$inv->read_list_base(USER_ID,$_GET['par5']);
	if((int)$_GET['par5'])
	{
		global $func;
		$c=$client->read_one(USER_ID,(int)$_GET['par5']);
		$smarty->assign("client",$c->name);
		//odcztyanie mail 
		$mail=array();
		if($func->is_mail($c->mail)) { $mail[]=$c->mail; }
		$tmp=preg_split("/[,\s]+/",$c->email);
		foreach($tmp as $m)
		{ if($func->is_mail($m)) { $mail[]=$m; } }
		$smarty->assign("mail",$mail);
		$smarty->assign("client_data",$c);
	}
	else
	{
		global $func;
		$c=new invoice_data();
		$c->read_url($_GET['par5'],'client');
		$smarty->assign("client",$c->name);
		//odcztyanie mail
		$tmp=preg_split("/[,\s]+/",$c->email);
		$mail=array();
		foreach($tmp as $m)
		{ if($func->is_mail($m)) { $mail[]=$m; } }
		$smarty->assign("mail",$mail);
		$smarty->assign("client_data",$c);
	}
	$smarty->assign("file",$file);
	$smarty->assign("base",$base);
	$smarty->assign("action","client");
}
else
{
	if(strlen($_GET['par5'])>=2)
	{
		require(ROOT_DIR."includes/search.php");
		$search=new search();
		$_GET['par5']=str_replace("_"," ",urldecode($_GET['par5']));
		$tmp=explode(",",$_GET['par5']);
		$searchs=array();
		$searchs['name']=$tmp[0];
		$searchs['address']=$tmp[1];
		$searchs['nip']=$tmp[2];
		$smarty->assign("search",$searchs);
		list($dane,$page,$last)=$inv->client_list_search(USER_ID,$_GET['par4'],20,$tmp[0],$tmp[1],$tmp[2]);
	}
	else { list($dane,$page,$last)=$inv->client_list(USER_ID,$_GET['par4'],20,$_GET['par5']); }
	$smarty->assign("dane",$dane);
	$smarty->assign("page",$page);
	$smarty->assign("last",$last);
	$smarty->assign("sign",$_GET['par5']?$_GET['par5']."/":"");
	$smarty->assign("letter",$inv->letter_list(USER_ID));
	$smarty->assign("other",$inv->count_pdf(USER_ID,0));
	$smarty->assign("action","list");
}


$smarty->display("panel/invoice/".LAYOUT.".html");
?>