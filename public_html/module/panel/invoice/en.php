<?php
$lang=array();
$lang['head']="List of Customers to whom Invoices were issued";
$lang['header']="List of Invoices for a Customer";

$lang['all']="all";
$lang['invoices']="All Invoices";
$lang['sign']="Name (short)";
$lang['name']="Full name/Company Name";
$lang['address']="Address";
$lang['nip']="Tax ID#";
$lang['searcher']="Search engine";
$lang['search']="Search";
$lang['show']="pull down";
$lang['hidden']="pull up";

$lang['invoice']="Invoices";
$lang['other']="Remaining PDF Invoices";
$lang['base']="Saved Invoices";
$lang['pdf']="Invoices only in PDF";
$lang['select']="Select email address(es) to which the Invoice will be sent";
$lang['mail']="Enter new address";

$lang['view']="View";
$lang['download']="Download";
$lang['close']="Close";
$lang['send']="Send";
$lang['back']="back";
$lang['page']="List of Invoices with a browser";

$lang['c_head']="Customer Data";
$lang['c_name']="Full name/Company Name";
$lang['c_address']="Address";
$lang['c_nip']="Tax ID#";
$lang['c_phone']="Telephone";
$lang['c_email']="Email Address";
$lang['c_krs']="Companies House #";
$lang['c_bank_name']="Bank Name";
$lang['c_account']="Account No.";

$lang['nagl0']="Invoices according to Customers";
$lang['nagl1']=">VAT Invoice";
$lang['nagl2']="You are in";
$lang['nagl3']="Short Name";
$lang['nagl4']="Name";
$lang['nagl5']="Quantity of Invoices";
$lang['nagl6']="PDF";
$lang['nagl7']="Invoice Number";
$lang['nagl8']="Date of Issue";
$lang['nagl9']="Invoice Amount";
$lang['nagl10']="Send";
$lang['nagl11']="View";
$lang['nagl12']="Remove";
$lang['nagl13']="Remove";
$lang['nagl14']="Remove";
$lang['nagl15']="Remove";
$lang['nagl16']="Remove";
$lang['nagl17']="Remove";
$lang['nagl18']="Remove";

$lang['save_menu0'] = 'Print PDF';
$lang['save_menu1'] = 'Original and Copy';
$lang['save_menu2'] = 'Original';
$lang['save_menu3'] = 'Copy';
$lang['save_menu4'] = 'Duplicate';
$lang['save_menu5'] = 'Print';

$lang['bad_nip']="The Tax ID Number is incorrect. Please provide a correct number.";
$lang['send_ok']="Invoice successfully sent";
$lang['send_error']="An error occurred while sending the invoice. Please try again later.";
$lang['']="";
?>