panel_invoice_obj = {
	/**
	 * Pokazanie panelu wyszukiwarki
	 */
	show_search : function()
	{
		$('search_show').style.display="none";
		$('search_hidden').style.display="block";
		$('search').style.display="block";
	},
	/**
	 * Ukrycie panelu wyszukiwarki
	 */
	hidden_search : function()
	{
		$('search_show').style.display="block";
		$('search_hidden').style.display="none";
		$('search').style.display="none";
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przejście na stronę wyników wyszukiwania
	 */
	search : function()
	{
		var par=$('name').value+","+$('address').value+","+$('nip').value;
		location.href="panel/invoice/list/1/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Pobranie faktury
	 * @param path ścieżka do pliku
	 */
	download : function(path)
	{
		location.href="download.php?file="+path;
		return false;
	},
	/**
	 * Otwarcie panelu wybierania adresów emial 
	 * @param path ścieżka do pliku
	 * TYLKO PDF
	 */
	send : function(path)
	{
		var size=window_size();

		$('panel_mail').style.display="block";
		$('panel_mail').style.top=((size[1]-$('panel_mail').clientHeight)/2)+"px";
		//$('panel_mail').style.left=((size[0]-$('panel_mail').clientWidth)/2)+"px";
		$('path').value=path;
		return false;
	},
	/**
	 * Otwarcie panelu wybierania adresów emial 
	 * @param path ścieżka do pliku
	 * NORMLANE
	 */
	send2 : function(path, type)
	{
		var size=window_size();
		$('panel_mail2').style.display="block";
		$('panel_mail2').style.top=((size[1]-$('panel_mail2').clientHeight)/2)+"px";
		//$('panel_mail2').style.left=((size[0]-$('panel_mail2').clientWidth)/2)+"px";
		$('path2').value=path;
		return false;
	},
	/**
	 * Zamknięcie panelu wybieranie adresów email
	 */
	close : function()
	{
		$('panel_mail').style.display="none";
	},
	/**
	 * Wysłanie mail'a z fakturą
	 */
	submit : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('send_ok').value); }
			else
			{ info($('send_error').value); }
			panel_invoice_obj.close();
		};
		req.SendForm("send_mail","module/panel/invoice/send_mail.php","post");
		return false;
	},
	submit2 : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('send_ok').value); }
			else
			{ info($('send_error').value); }
			$('panel_mail2').style.display='none'
		};
		req.SendForm("send_mail2","module/panel/invoice/send_mail.php","post");
		return false;
	},
	/**
	 * Zapis do pdf
	 */
	idf : 0,
	type : 0,
	save_pdf : function(id, type)
	{
		$('what_print').style.display='block';
		this.idf=id;      
		this.type=type;
    return false;
	},
	save_pdf_end : function()
	{
		var what=1;
		what=$('print2').checked?2:what; 
		//what=$('print3').checked?3:what;
		what=$('print4').checked?4:what;
		$('what_print').style.display='none';
		location.href='panel/invoice/list/file/'+this.idf+'/'+what+'/'+this.type+'/';
		return false;
	}
	
};