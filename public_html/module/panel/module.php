<?php
//sprawdzenie czy zalogowany
if(USER_RANG=="ban" OR USER_RANG=="") { header("Location: ".ROOT_URL); exit(); }


switch($_GET['par2'])
{
	case "data":
		switch($_GET['par3'])
		{
			case "general":
				require(ROOT_DIR."module/panel/data_general/module.php");
				break;
			case "extra":
				require(ROOT_DIR."module/panel/data_extra/module.php");
				break;
			case "log":
				require(ROOT_DIR."module/panel/data_log/module.php");
				break;	
			case "licence":
				require(ROOT_DIR."module/panel/data_licence/module.php");
				break;
			case "number":
				require(ROOT_DIR."module/panel/data_number/module.php");
				break;
			case "szablon-faktury":
				require(ROOT_DIR."module/panel/szablon-faktury/module.php");
				break;	
			default:
				break;
		}
		break;
	case "szablon":
		switch($_GET['par3'])
		{
			case "szablon-faktury":
				require(ROOT_DIR."module/panel/szablon-faktury/module.php");
				break;
			default:
				break;
		}
		break;	
	case "client":
		switch($_GET['par3'])
		{
			case "add":
				require(ROOT_DIR."module/panel/client_add/module.php");
				break;
			case "list":
				require(ROOT_DIR."module/panel/client_list/module.php");
				break;
			case "send":
				require(ROOT_DIR."module/panel/client_send/module.php");
				break;
			default:
				break;
		}
		break;
	case "product":
		switch($_GET['par3'])
		{
			case "add":
				require(ROOT_DIR."module/panel/product_add/module.php");
				break;
			case "list":
				require(ROOT_DIR."module/panel/product_list/module.php");
				break;
			case "cat":
				require(ROOT_DIR."module/panel/product_cat/module.php");
				break;
			default:
				break;
		}
		break;
	case "invoice":
		switch($_GET['par3'])
		{
			case "add":
				header("Location: ".ROOT_URL."invoice/"); exit();
				break;
			case "list":
				require(ROOT_DIR."module/panel/invoice/module.php");
				break;
			case "search":
				require(ROOT_DIR."module/panel/invoice_search/module.php");
				break;
			case "period":
				require(ROOT_DIR."module/panel/invoice_period/module.php");
				break;
			case "org":
				require(ROOT_DIR."module/panel/invoice_org/module.php");
				break;
			default:
				break;
		}
		break;
	case "raport":
		switch($_GET['par3'])
		{
			case "sell":
				require(ROOT_DIR."module/panel/raport_stat/module.php");
				require(ROOT_DIR."module/panel/raport_sell/module.php");
	
				break;
			case "stat":
			require(ROOT_DIR."module/panel/menu/module.php");
				
				break;
			default:
				break;
		}
		break;
  case "jpk":
		require(ROOT_DIR."module/panel/jpk/module.php");
		break;
	default:
		require(ROOT_DIR."module/panel/main/module.php");
		
		break;
}
?>