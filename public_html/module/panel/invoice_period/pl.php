<?php
$lang=array();
$lang['head']="Faktury cykliczne";
$lang['number']="Numer faktury";
$lang['name']="Nazwa nabywcy";
$lang['nip']="Numer NIP nabywcy";
$lang['date_add']="Data wystawienia";
$lang['date_sell']="Data sprzedaży";
$lang['sum']="Kwota faktury";
$lang['search']="Szukaj";
$lang['view']="Zobacz";
$lang['other']="Pozostali";
$lang['dcreate']="Data utworzenia";
$lang['dadd']="Data wystawienia";
$lang['dsell']="Data sprzedaży";
$lang['month']="Przeglądanie miesięczne";
$lang['show']="Pokaż";
$lang['type']="Rodzaj";
$lang['all']="Pokaż wszystkie";
$lang['all_l']="wszystkie";
$lang['page']="Lista wszystkich faktur kontrahenta";
$lang['showed']="rozwiń";
$lang['hidden']="zwiń";
$lang['searcher']="Wyszukiwarka";
$lang['is_ok']="Zatwierdź";
$lang['sendm']="Wyślij";

$lang['c_head']="Dane kontrahenta";
$lang['c_name']="Imię i nazwisko/nazwa firmy";
$lang['c_address']="Adres";
$lang['c_nip']="NIP";
$lang['c_phone']="Telefon";
$lang['c_email']="Adres e-mail";
$lang['c_krs']="Numer KRS";
$lang['c_bank_name']="Nazwa banku";
$lang['c_account']="Numer konta";

$lang['prev']="Poprzedni kontrahent";
$lang['next']="Następny kontrahent";

$lang['send_ok']="Wiadomość została wysłana";
$lang['send_error']="Wystąpił błąd podczas wysyłania wiadomości, spróbuj ponownie za chwilę";
$lang['send_null']="Brak adresu e-mail do wysyłki, uzupełnij dane kontrahenta";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";

$lang['nagl1'] = 'Faktura VAT';
$lang['nagl2'] = 'Jesteś w';
$lang['nagl3'] = 'edytuj';
$lang['nagl4'] = 'Wyszukiwarka faktur';
$lang['nagl5'] = 'Wyszukiwanie zaawansowane';
$lang['nagl6'] = 'Od:';
$lang['nagl7'] = 'Do:';
$lang['nagl8'] = 'Przeglądanie miesięczne';
$lang['nagl9'] = 'PDF';
$lang['nagl10'] = 'Numer faktury';
$lang['nagl11'] = 'Kwota faktury';
$lang['nagl12'] = 'Nazwa / Firma';
$lang['nagl13'] = 'Zobacz';
$lang['nagl14'] = 'Usuń';
$lang['nagl15'] = 'Zatwierdź';
$lang['nagl16'] = 'Wyślij';

$lang['save_menu0'] = 'Wydrukuj PDF';
$lang['save_menu1'] = 'Oryginał i Kopia';
$lang['save_menu2'] = 'Oryginał';
$lang['save_menu3'] = 'Kopia';
$lang['save_menu4'] = 'Duplikat';
$lang['save_menu5'] = 'Drukuj';
?>