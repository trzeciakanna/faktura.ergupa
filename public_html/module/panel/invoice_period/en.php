<?php
$lang=array();
$lang['head']="Cyclical Invoices";
$lang['number']="Invoice Number";
$lang['name']="Buyer's Name";
$lang['nip']="Buyer's Tax ID#";
$lang['date_add']="Date of Issue";
$lang['date_sell']="Date of Sale";
$lang['sum']="Invoice Amount";
$lang['search']="Search";
$lang['view']="View";
$lang['other']="Remaining";
$lang['dcreate']="Date of creating";
$lang['dadd']="Date of Issue";
$lang['dsell']="Date of Sale";
$lang['month']="Monthly browsing";
$lang['show']="Display";
$lang['type']="Type";
$lang['all']="Show all";
$lang['all_l']="All";
$lang['page']="List of all the Customer's Invoices";
$lang['showed']="pull down";
$lang['hidden']="pull up";
$lang['searcher']="Search engine";
$lang['is_ok']="Confirm";
$lang['sendm']="Send";

$lang['c_head']="Customer's Data";
$lang['c_name']="Full Name/Company Name";
$lang['c_address']="Address";
$lang['c_nip']="Tax ID#";
$lang['c_phone']="Telephone";
$lang['c_email']="Email Address";
$lang['c_krs']="Companies House#";
$lang['c_bank_name']="Bank Name";
$lang['c_account']="Account No.";

$lang['prev']="Previous Customer";
$lang['next']="Next Customer";

$lang['send_ok']="Message successfully sent";
$lang['send_error']="An error occurred while sending the message. Please try again later.";
$lang['send_null']="No email, please complete the Customer's Data";
$lang['bad_nip']="The Tax ID# is incorrect. Please provide the correct number.";

$lang['nagl1'] = 'VAT Invoice';
$lang['nagl2'] = 'You are in';
$lang['nagl3'] = 'edit';
$lang['nagl4'] = 'Invoice Search';
$lang['nagl5'] = 'Advanced Search';
$lang['nagl6'] = 'From:';
$lang['nagl7'] = 'To:';
$lang['nagl8'] = 'Monthly browsing';
$lang['nagl9'] = 'PDF';
$lang['nagl10'] = 'Invoice Number';
$lang['nagl11'] = 'Invoice Amount';
$lang['nagl12'] = 'Name / Company';
$lang['nagl13'] = 'View';
$lang['nagl14'] = 'Remove';
$lang['nagl15'] = 'Confirm';
$lang['nagl16'] = 'Send';

$lang['save_menu0'] = 'Print PDF';
$lang['save_menu1'] = 'Original and Copy';
$lang['save_menu2'] = 'Original';
$lang['save_menu3'] = 'Copy';
$lang['save_menu4'] = 'Duplicate';
$lang['save_menu5'] = 'Print';
?>