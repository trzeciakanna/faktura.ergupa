<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("LANG","pl");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/mailer.php");
require(ROOT_DIR."includes/users.php");
/* utworzenie głównych klas */
global $lang;
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
//utworzenie faktury
require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/data_extra/class.php");
require(ROOT_DIR."module/invoice/class_pdf.php");
$inv=new invoice();
list($tmp,$count)=$inv->get_to_pdf($_GET['id'],$_GET['uid']);
$pdf=new class_pdf($_POST['uid'],$count,$tmp,2);
$path=str_replace(ROOT_DIR,"",urldecode($pdf->file));

$r=$DB->Execute("SELECT `mail` FROM `clients` WHERE `id`='".$_GET['cid']."' LIMIT 0,1");
if(!$r->fields['mail']) { echo "null"; exit(); }

//lista adresów email					
$user=array();
$user[]=array("mail"=>$r->fields['mail'],"name"=>$r->fields['mail']);

//faktura
$file=array();
$file[]=array("path"=>ROOT_DIR.$path,"name"=>"invoice.pdf");
//dane dodatkowe
$u=new users_invoice($_GET['uid']);
$x=array();
$x['name']=$u->name;
$mailer=new mailer($u->mail);
$result=$mailer->send($user,"send_invoice",$x,$file);
echo ($result?"ok":"error");
?>