panel_invoice_period_obj = {
	/**
	 * Pokazanie panelu wyszukiwarki
	 */
	show_search : function()
	{
		$('search_show').style.display="none";
		$('search_hidden').style.display="block";
		$('search').style.display="block";
	},
	/**
	 * Ukrycie panelu wyszukiwarki
	 */
	hidden_search : function()
	{
		$('search_show').style.display="block";
		$('search_hidden').style.display="none";
		$('search').style.display="none";
	},
	/**
	 * Przekierowanie na stronę wyszukiwania
	 */
	search : function()
	{
		var par=$('number').value+","+$('name').value+","+$('nip').value+",";
		par+=$('calendar_date_dadd_min').value+","+$('calendar_date_dadd_max').value+",";
		par+=$('calendar_date_dsell_min').value+","+$('calendar_date_dsell_max').value+",";
		par+=$('sum_min').value+","+$('sum_max').value;
		location.href="panel/invoice/period/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przekierowanie na stronę listy
	 */
	show : function()
	{
		var par=$('year').value+'-'+$('month').value+'-'+($('type_c').checked?"c":($('type_a').checked?"a":"s"));
		location.href="panel/invoice/period/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
	},
	/**
	 * Przekierowanie na stronę listy
	 */
	show_month : function(month)
	{
		var t=new Date();
		var par=t.getFullYear()+'-'+month+'-'+($('type_c').checked?"c":($('type_a').checked?"a":"s"));
		location.href="panel/invoice/period/"+$('client').value+"/1/dc/"+$('letter').value+"/"+delete_pl(par.replace(/ /g,"_"))+"/";
		return false;
	},
	/**
	 * Wysłanie maila z fakturą
	 * @param id identyfikator faktury
	 * @param cui identygikator kontrahenta
	 * @param uid identyfikator użytkownika
	 */
	sendm : function(id, cid, uid)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('send_ok').value); }
			else if(this.responseText=="null")
			{ info($('send_null').value); }
			else
			{ info($('send_error').value); }
		};
		req.AddParam("id",id);
		req.AddParam("cid",cid);
		req.AddParam("uid",uid);
		req.Send("module/panel/invoice_period/send_mail.php");
		return false;
	},
	/**
	 * Zapis do pdf
	 */
	idf : 0,
	save_pdf : function(id)
	{
		$('what_print').style.display='block';
		this.idf=id;
    return false;
	},
	save_pdf_end : function()
	{
		var what=1;
		what=$('print2').checked?2:what;
		what=$('print3').checked?3:what;
		what=$('print4').checked?4:what;
		$('what_print').style.display='none';
		location.href='panel/invoice/list/file/'+this.idf+'/'+what+'/';
		return false;
	}
	
};