<?php
$lang=array();
$lang['head']="Wyślij wiadomość";
$lang['topic']="Temat";
$lang['text']="Treść";
$lang['client']="Lista kontrahentów";
$lang['search']="Wyszukaj";
$lang['all']="zaznacz/odznacz wszystkich";
$lang['count']="Wybrałeś [count] użytkowników";

$lang['last']="Archiwum wiadomości";
$lang['view']="Zobacz";
$lang['resend']="Wyślij jeszcze raz";
$lang['back']="wróć do archiwum";
$lang['all_last']="wszystkie";
$lang['sended']="wysłano:";

$lang['err_users']="Wybierz kontrahentów z listy";
$lang['err_topic']="Podaj temat wiadomości";
$lang['err_text']="Podaj treść wiadomości";

$lang['cancel']="Wyczyść";
$lang['send']="Wyślij";
$lang['send_ok']="Wiadomości zostały dodane do wysłania poprawnie.";
$lang['send_error']="Wystąpił błąd, proszę spróbować ponownie za chwilę.";

$lang['nagl1'] = 'Faktura VAT';
$lang['nagl2'] = 'Jesteś w';
$lang['nagl3'] = 'Wpisz treść wiadomości';

$lang['table_nagl1'] ='Tytuł wiadomości';
$lang['table_nagl2'] ='Data wysłania';
$lang['table_nagl3'] ='Zobacz';
$lang['table_nagl4'] ='Wyślij ponownie';
?>