<?php
$lang=array();
$lang['head']="Send Message";
$lang['topic']="Subject";
$lang['text']="Content";
$lang['client']="Customer List";
$lang['search']="Search";
$lang['all']="select/deselect all";
$lang['count']="You have selected [count] users";

$lang['last']="Message Archive";
$lang['view']="View";
$lang['resend']="Resend";
$lang['back']="back";
$lang['all_last']="all";

$lang['err_users']="Select Customers from the list";
$lang['err_topic']="Input Subject";
$lang['err_text']="Input Content";

$lang['cancel']="Clear";
$lang['send']="Send";
$lang['send_ok']="Messages successfully stored for sending.";
$lang['send_error']="An error occurred, please try again later.";

$lang['nagl1'] = 'VAT Invoice';
$lang['nagl2'] = 'You are in';
$lang['nagl3'] = 'Input Message Content';

$lang['table_nagl1'] ='Message Title';
$lang['table_nagl2'] ='Sending Date';
$lang['table_nagl3'] ='View';
$lang['table_nagl4'] ='Resend';
?>