<?php
global $smarty;
require(ROOT_DIR."module/panel/client_add/class.php");
require(ROOT_DIR."module/panel/client_send/".LANG.".php");
global $navi;

$smarty->assign("navi",$navi->navi);
global $lang;
$smarty->assign("lang",$lang);
$client=new clients();
//odczyt danych użytkownika

switch($_GET['par4'])
{
	case "last":
		list($dane,$page,$last)=$client->send_list(USER_ID,$_GET['par5'],20,$_GET['par6']);
		$smarty->assign("dane",$dane);
		$smarty->assign("page",$page);
		$smarty->assign("last",$last);
		$smarty->assign("sign",$_GET['par6']?$_GET['par6']."/":"");
		$smarty->assign("letter",$client->send_letter(USER_ID));
		$smarty->assign("action","last");
		break;
	case "view":
		list($msg, $user)=$client->send_one(USER_ID, $_GET['par5']);
		$smarty->assign("msg",$msg);
		$smarty->assign("user",$user);
		$smarty->assign("action","view");
		break;
	case "resend":
		$smarty->assign("user_id",USER_ID);
		list($msg, $user)=$client->send_one(USER_ID, $_GET['par5']);
		$smarty->assign("msg",$msg);
		$smarty->assign("user",$user);
		list($dane,$page,$last)=$client->read_list(USER_ID,1,99999,"");
		$smarty->assign("dane",$dane);
		$smarty->assign("action","send");
		break;
	default:
		$smarty->assign("user_id",USER_ID);
		list($dane,$page,$last)=$client->read_list(USER_ID,1,99999,"");
		$smarty->assign("dane",$dane);
		$smarty->assign("user",array());
		$smarty->assign("action","send");
		break;
}
$smarty->display("panel/client_send/".LAYOUT.".html");
?>