<?php
global $smarty;
require(ROOT_DIR."module/panel/invoice_org/class.php");
require(ROOT_DIR."module/panel/invoice_org/".LANG.".php");
global $lang;
global $navi;

$smarty->assign("navi",$navi->navi);
$smarty->assign("lang",$lang);
//odczyt danych użytkownika
$smarty->assign("user_id",USER_ID);
$inv=new invoices_org();

if($_GET['par4']=="del")
{
	$inv->del(USER_ID,(int)$_GET['par5']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
list($data,$page,$last)=$inv->read_all(USER_ID,$_GET['par4'],20);
$smarty->assign("data",$data);
$smarty->assign("page",$page);
$smarty->assign("last",$last);
$smarty->display("panel/invoice_org/".LAYOUT.".html");
?>