panel_invoice_org_obj = {
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	
	submit : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value,'save_ok'); }
			else
			{ info($('save_error').value,'save_error'); }
			panel_invoice_obj.close();
		};
		req.SendForm("invoice_org_from","module/panel/invoice_org/save.php?user_id="+$('user_id').value,"post");
		return false;
	}
	
};