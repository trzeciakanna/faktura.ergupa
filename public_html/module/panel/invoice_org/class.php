<?php

class invoices_org
{
	
	public function read_all($user_id, $page=1, $ile=20)
	{
		global $DB;
		$page=(int)$page?(int)$page:1;
		$dane=array();
		$r=$DB->PageExecute("SELECT * FROM `invoice_auto` WHERE `user_id`='".$user_id."' ORDER BY `date_next` DESC",$ile,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new invoice_org($r->fields['id'],$r->fields['user_id'],$r->fields['invoice_id'],$r->fields['date_start'],$r->fields['date_next'],$r->fields['period'],$r->fields['value']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	
	public function save($user_id, $data)
	{
		global $DB;
		$max=count($data)/4;
		for($a=0;$a<$max;$a++)
		{
			$tmp=array();
			$tmp['date_start']=$data['period_start'.$a];
			$tmp['value']=$data['period_value'.$a];
			$tmp['period']=$data['period_time'.$a];
			$DB->AutoExecute("invoice_auto",$tmp,"UPDATE","`id`='".$data['period_id'.$a]."'");
		}
		return "ok";
	}
	
	public function del($user_id, $id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `invoice_auto` WHERE `id`='".$id."' AND `user_id`='".$user_id."' LIMIT 1");
	}
	
	
}



class invoice_org
{
	public $id;
	public $user_id;
	public $invoice_id;
	public $date_start;
	public $date_next;
	public $period;
	public $value;
	
	public function __construct($id, $user_id=null, $invoice_id=null, $date_start=null, $date_next=null, $period=null, $value=null)
	{
		$this->id=$id;
		$this->user_id=$user_id;
		$this->invoice_id=$invoice_id;
		$this->date_start=$date_start;
		$this->date_next=$date_next;
		$this->period=$period;
		$this->value=$value;
	}
	
	
}

?>