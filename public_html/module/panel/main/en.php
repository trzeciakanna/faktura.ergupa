<?php
$lang=array();

$lang['product_main']="Products";
$lang['product_add']="Add a product";
$lang['product_list']="Product list";
$lang['product_cat']="Categories";
$lang['product_add_info']="Here you can add a product to your product base to streamline invoice issuing";
$lang['product_list_info']="List of all Products/Services. ";
$lang['product_cat_info']="Here you can add or edit the Product/Service categories to streamline their searching.";

$lang['client_main']="Customers";
$lang['client_add']="Add a Customer";
$lang['client_add_info']="here you can add the data of your customers to streamline invoice issuing. You can also add a Customer while actually issuing an invoice. ";
$lang['client_list']="Customer management";
$lang['client_list_info']="Here you can preview and manage (edit, remove) the Customer data.";
$lang['client_send']="Send a Message";
$lang['client_send_info']="Here you can send messages to your Customers, one at a time or all at the same time.";

$lang['invoice_main']="Invoices";

$lang['invoice_period']="Cyclical Invoices";
$lang['invoice_period_info']="Here you can check, correct and approve those invoices which were set to be issued automatically";
$lang['invoice_list2']="Invoice list";
$lang['invoice_list2_info']="List of all your issued invoices.";
$lang['invoice_org']="Example Invoices";
$lang['invoice_org_info']="List and editing of example cyclical invoices.";
$lang['invoice_add']="Add an Invoice";
$lang['invoice_add_info']="lick and you will be directed to the issuing page.";
$lang['invoice_list']="Invoices by Customer";
$lang['invoice_list_info']="Here you can view or edit invoices sorted by Customer.";

$lang['data_main']="Basic data";
$lang['data_general']="Invoice data";
$lang['data_general_info']="";
$lang['data_extra']="Logo, header, footer"; 
$lang['data_extra_info']=""; 
$lang['data_log']="Login / Pasword";
$lang['data_log_info']="";
$lang['data_number']="Invoice numbering";
$lang['data_number_info']="Here you can select the numbering method.";
$lang['data_licence']="Subscription";
$lang['data_licence_info']="here you can buy or extend your Subscription.";
?>