<?php
$lang=array();

$lang['product_main']="Produkty";
$lang['product_add']="Dodawanie produktu";
$lang['product_list']="Lista produktów";
$lang['product_cat']="Kategorie";
$lang['product_add_info']="Tutaj dodasz produkt do swojej bazy produktów, aby usprawnić wystawianie faktur.";
$lang['product_list_info']="Tutaj zamieszczona jest lista wszystkich produktów oraz usług.";
$lang['product_cat_info']="Tutaj dodasz bądź edytujesz wszystkie kategorie produktów lub usług, aby sprawdzić ich wyszukanie należy skorzystać z naszego samouczka.";

$lang['client_main']="Kontrahenci";
$lang['client_add']="Dodawanie kontrahentów";
$lang['client_add_info']="Tutaj dodasz dane swoich kontrahentów, aby usprawnić wystawianie faktur. ";
$lang['client_list']="Lista kontrahentów";
$lang['client_list_info']="W tym miejscu jest podgląd oraz zarządzanie ( edytowanie, usuwanie ) danymi kontrahentów.";
$lang['client_send']="Wysyłanie wiadomości";
$lang['client_send_info']="Tutaj możesz wysłać wiadomość do swoich kontrahentów, do jednego, kilku lub do wszystkich jednocześnie. ";

$lang['invoice_main']="Faktury";

$lang['invoice_period']="Faktury cykliczne";
$lang['invoice_period_info']="Tutaj sprawdzisz i zatwierdzisz faktury, które ustawiłeś aby generowały się automatycznie.";
$lang['invoice_list2']="Lista faktur";
$lang['invoice_list2_info']="Lista wszystkich wystawionych faktur. ";
$lang['invoice_org']="Faktury wzorcowe";
$lang['invoice_org_info']="Lista wzorców faktur cyklicznych oraz ich wszelka edycja.";
$lang['invoice_add']="Wystawianie faktur";
$lang['invoice_add_info']="Po kliknięciu przycisku zostaniesz przeniesiony do strony wystawiania faktur.";
$lang['invoice_list']="Faktury wg kontrahentów";
$lang['invoice_list_info']="Tutaj przejrzysz lub zedytujesz wszystkie faktury ułożone wg kontrahentów.<br><br>";

$lang['data_main']="Dane podstawowe";
$lang['data_general']="Dane do faktury ";
$lang['data_general_info']="Tutaj wprowadzisz swoje dane do faktury oraz dodasz konta bankowe.";
$lang['data_extra']="Logo, nagłówek, stopka"; 
$lang['data_extra_info']="Tutaj wstawisz swoje logo lub nagłówek oraz dopiszesz informacje dodatkowe"; 
$lang['data_log']="Logowanie / Hasło";
$lang['data_log_info']="Tutaj zmienisz swoje wszystkie dane odnośnie logowania, hasła, etc.";
$lang['data_number']="Numeracja faktur";
$lang['data_number_info']="W tym miejscu możesz wybrać kilka sposobów numeracji faktur.";
$lang['data_licence']="Przedłuż Abonament";
$lang['data_licence_info']="Tutaj uzyskasz informacje, wykupisz bądź przedłużysz swój abonament.";




?>