<?php
$lang=array();
$lang['head']="Customer List";
$lang['all']="all";
$lang['back']="back";
$lang['data']="Customer data";
$lang['mail']="Email Address for sending";
$lang['sign']="Name (short)";
$lang['name']="Full Name/Company Name";
$lang['address']="Address";
$lang['nip']="Tax ID#";
$lang['phone']="Telephone";
$lang['email']="Email Address";
$lang['bank_name']="Bank Name";
$lang['krs']="Companies House #";
$lang['account']="Account No.";
$lang['addb_ok']="Branch added successfully";
$lang['addb_error']="An error occurred while adding a branch. Please try again later";
$lang['saveb_ok']="Branch data saved successfully";
$lang['saveb_error']="Wystąpił błąd podczas zapisywania danych oddziału, spróbuj ponownie za chwilę";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="An error occurred while saving. Please try again later";
$lang['bad_nip']="The given Tax ID Number is incorrect, please input the correct number.";
$lang['bad_krs']="The given Companies House Number is incorrect, please input the correct number.";
$lang['bad_regon']="The given REGON Number is incorrect, please input the correct number.";
$lang['delete']="Do you really want to remove this Customer?";
$lang['deleteb']="Do you really want to remove this Branch?";
$lang['edit']="Edit";
$lang['del']="Remove";
$lang['save']="Save";
$lang['cancel']="Cancel";
$lang['add']="Add";
$lang['addb']="Add a Branch";
$lang['invoice']="Invoices";
$lang['branch']="Branches";
$lang['branch_data']="Branch Data";
$lang['page']="Customer's Page";

$lang['searcher']="Browser";
$lang['search']="Search";
$lang['show']="pull down";
$lang['hidden']="pull up";

$lang['nagl1'] = 'VAT Invoice';
$lang['nagl2'] = 'You are in';
$lang['nagl3'] = 'Customer Browser';
$lang['nagl4'] = 'Edit Customer ';

$lang['table_nagl1'] = 'Name (short):';
$lang['table_nagl2'] = 'Full Name/Company Name';
$lang['table_nagl2'] = 'Name';
$lang['']="";
?>