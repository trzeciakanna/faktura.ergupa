<?php
global $smarty;
require(ROOT_DIR."module/panel/client_add/class.php");
require(ROOT_DIR."module/panel/client_list/".LANG.".php");
require(ROOT_DIR."module/pager/".LANG.".php");
global $navi;

$smarty->assign("navi",$navi->navi);
global $lang;
global $pager;
$smarty->assign("lang",$lang);
$smarty->assign("pager",$pager);
//odczyt danych użytkownika
$smarty->assign("user_id",USER_ID);
$client=new clients();

switch($_GET['par4'])
{
	case "del":
		$client->del(USER_ID,(int)$_GET['par5']);
		header("Location: ".$_SERVER['HTTP_REFERER']);
	case "bdel":
		$client->branch_del((int)$_GET['par5'],(int)$_GET['par6']);
		header("Location: ".$_SERVER['HTTP_REFERER']);
	case "edit":
		$smarty->assign("user",$client->read_one(USER_ID,(int)$_GET['par5']));
		$smarty->assign("action","edit");
		break;
	case "bedit":
		$smarty->assign("user",$client->branch_one((int)$_GET['par5'],(int)$_GET['par6']));
		$smarty->assign("action","bedit");
		break;
	case "badd":
		$smarty->assign("user",$client->read_one((int)$_GET['par5'],(int)$_GET['par6']));
		$smarty->assign("action","badd");
		break;
	case "branch":
		$smarty->assign("dane",$client->branch_list((int)$_GET['par5']));
		$smarty->assign("action","branch");
		break;
	default:
		if(strlen($_GET['par5'])>=2)
		{
			require(ROOT_DIR."includes/search.php");
			$search=new search();
			$_GET['par5']=str_replace("_"," ",urldecode($_GET['par5']));
			$tmp=explode(",",$_GET['par5']);
			$searchs=array();
			$searchs['name']=$tmp[0];
			$searchs['address']=$tmp[1];
			$searchs['nip']=$tmp[2];
			$smarty->assign("search",$searchs);
			list($dane,$page,$last)=$client->read_list_search(USER_ID,$_GET['par4'],20,$tmp[0],$tmp[1],$tmp[2]);
		}
		else { list($dane,$page,$last)=$client->read_list(USER_ID,$_GET['par4'],20,$_GET['par5']); }
		$smarty->assign("dane",$dane);
		$smarty->assign("page",$page);
		$smarty->assign("last",$last);
		$smarty->assign("sign",$_GET['par5']?$_GET['par5']."/":"");
		$smarty->assign("letter",$client->letter_list(USER_ID));
		$smarty->assign("action","list");
		break;
}
$smarty->display("panel/client_list/".LAYOUT.".html");
?>