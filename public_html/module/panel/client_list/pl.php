<?php
$lang=array();
$lang['head']="Lista kontrahentów";
$lang['all']="wszystkie";
$lang['back']="wstecz";
$lang['data']="Dane kontrahenta";
$lang['mail']="Adres e-mail do wysyłki";
$lang['sign']="Nazwa skrócona";
$lang['name']="Imię i nazwisko/nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['phone']="Telefon";
$lang['email']="Adres e-mail";
$lang['bank_name']="Nazwa banku";
$lang['krs']="Numer KRS";
$lang['account']="Numer konta";
$lang['addb_ok']="Oddział został dodany poprawnie";
$lang['addb_error']="Wystąpił błąd podczas dodawania oddziału, spróbuj ponownie za chwilę";
$lang['saveb_ok']="Dane oddziału zostały zapisane poprawnie";
$lang['saveb_error']="Wystąpił błąd podczas zapisywania danych oddziału, spróbuj ponownie za chwilę";
$lang['save_ok']="Dane zostały zapisane poprawnie";
$lang['save_error']="Wystąpił błąd podczas zapisywania, spróbuj ponownie za chwilę";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_krs']="Podany numer KRS jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_regon']="Podany numer REGON jest nieprawidłowy, wprowadź poprawny numer.";
$lang['delete']="Czy na pewno chcesz usunąć tego kontrahenta?";
$lang['deleteb']="Czy na pewno chcesz usunąć ten oddział?";
$lang['edit']="Edytuj";
$lang['del']="Usuń";
$lang['save']="Zapisz";
$lang['cancel']="Anuluj";
$lang['add']="Dodaj";
$lang['addb']="Dodaj oddział";
$lang['invoice']="Faktury";
$lang['branch']="Oddziały";
$lang['branch_data']="Dane oddziału";
$lang['page']="Strona kontrahenta";

$lang['searcher']="Wyszukiwarka";
$lang['search']="Wyszukaj kontrahenta";
$lang['show']="rozwiń";
$lang['hidden']="zwiń";

$lang['nagl1'] = 'Faktura VAT';
$lang['nagl2'] = 'Jesteś w';
$lang['nagl3'] = 'Wyszukiwarka kontrahentów';
$lang['nagl4'] = 'Edycja kontrahenta ';

$lang['table_nagl1'] = 'Nazwa skrócona:';
$lang['table_nagl2'] = 'Imię nazwisko(firma)';
$lang['table_nagl2'] = 'Nazwa';
$lang['']="";
?>