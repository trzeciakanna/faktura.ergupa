panel_client_list_obj = {	
	/**
	 * Zapisanie danych kontrahenta
	 */
	submit : function(id) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"ok");
			info('',"error");
			if(this.responseText=="ok")
			{ info($('save_ok').value,"ok"); }
			else
			{ info($('save_error').value,"error"); }
		};
		req.SendForm("client_list_form","module/panel/client_list/ajax_save.php?user_id="+$("user_id").value+"&id="+id,"post");
		return false;
	},
	/**
	 * Dodanie oddziału
	 */
	addb : function(id) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"ok");
			info('',"error");
			if(this.responseText=="ok")
			{ info($('add_ok').value,"ok"); }
			else
			{ info($('add_error').value,"error"); }
		};
		req.SendForm("client_list_form","module/panel/client_list/ajax_addb.php?id="+$("client_id").value,"post");
		return false;
	},
	/**
	 * Zapisanie danych oddziału
	 */
	saveb : function(id) 
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			info('',"ok");
			info('',"error");
			if(this.responseText=="ok")
			{ info($('save_ok').value,"ok"); }
			else
			{ info($('save_error').value,"error"); }
		};
		req.SendForm("client_list_form","module/panel/client_list/ajax_saveb.php?c_id="+$("client_id").value+"&id="+id,"post");
		return false;
	},
	/**
	 * Pokazanie panelu wyszukiwarki
	 */
	show_search : function()
	{
		$('search_show').style.display="none";
		$('search_hidden').style.display="block";
		$('search').style.display="block";
	},
	/**
	 * Ukrycie panelu wyszukiwarki
	 */
	hidden_search : function()
	{
		$('search_show').style.display="block";
		$('search_hidden').style.display="none";
		$('search').style.display="none";
	},
	/**
	 * Przejście na stronę wyników wyszukiwania
	 */
	search : function()
	{
		var par=$('name').value+","+$('address').value+","+$('nip').value;
		location.href="panel/client/list/1/"+delete_pl(par.replace(/ /g,"_"))+"/";
	}
	
	
};