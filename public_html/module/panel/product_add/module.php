<?php
global $smarty;
require(ROOT_DIR."includes/units.php");
require(ROOT_DIR."module/panel/product_add/class.php");
require(ROOT_DIR."module/panel/product_add/".LANG.".php");
global $lang;
global $units;
global $navi;

$smarty->assign("navi",$navi->navi);
$products=new products();
$smarty->assign("lang",$lang);
$smarty->assign("units",$units);
//odczyt danych użytkownika
$smarty->assign("user_id",USER_ID);
$smarty->assign("cat",(int)$_GET['par4']);

list($prod, $sum)=$products->cat_tree(USER_ID,0,0);
$smarty->assign("tree",$prod);

$smarty->display("panel/product_add/".LAYOUT.".html");
?>