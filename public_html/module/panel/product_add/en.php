<?php
$lang=array();
$lang['head']="Add a Product";

$lang['cat']="Select Category";
$lang['number']="Number";
$lang['name']="Name";
$lang['producer']="Manufacturer";
$lang['pkwiu']="Polish Goods Classification #";
$lang['unit']="Unit";
$lang['vat']="VAT Rate";
$lang['netto']="Net Price";
$lang['brutto']="Gross Price";

$lang['info']="Fields are required";
$lang['empty_cat']="Select Category";
$lang['empty_name']="Enter Product Name";
$lang['empty_vat']="Select VAT Rate";
$lang['empty_prize']="Enter Product Price";

$lang['cancel']="Cancel";
$lang['add']="Add";

$lang['add_ok']="Product successfully added";
$lang['add_error']="An error occurred while adding the Product. Please try again later.";


$lang['nagl1'] ='VAT Invoice';
$lang['nagl2'] ='You are in';
$lang['nagl3'] ='Enter Product Name';
$lang['nagl4'] ='or';
$lang['nagl5'] ='add new Category';
$lang['nagl6'] ='You may give the Product a Number';
$lang['nagl7'] ='Enter if VAT is other than 22%.';
$lang['nagl8'] ='Select default unit.';
$lang['nagl9'] ='Select VAT';
$lang['nagl10'] ='Enter Net Price';
$lang['nagl11'] ='Enter Gross Price';
?>