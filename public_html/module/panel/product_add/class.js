panel_product_add_obj = {	
	/**
	 * Dodanie produktu
	 */
	submit : function() 
	{
		var msg="";
		if(!$('cat').value) { msg+=$('empty_cat').value+"\n"; info($('empty_cat').value,'cat'); } else { info('','cat'); }
		if(!$('name').value) { msg+=$('empty_name').value+"\n"; info($('empty_name').value,'name'); } else { info('','name'); }
		if(!$('vat').value) { msg+=$('empty_vat').value+"\n"; info($('empty_vat').value,'vat'); } else { info('','vat'); }
		if(!$('netto').value) { msg+=$('empty_prize').value+"\n";  info($('empty_prize').value,'cena');} else { info('','cena'); }
		if(msg)
		{  }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{
					var c=$('cat').value;
					info($('add_ok').value,"add");
					info('',"error");
					$('product_add_form').reset();
					$('cat').value=c;
				}
				else
				{
					info($('add_error').value,"error");
					info('',"add");
				}
			};
			req.SendForm("product_add_form","module/panel/product_add/ajax_add.php?id="+$("user_id").value,"post");
		}
		return false;
	},
	/**
	 * Kontrola wpisywanej wartości
	 * @param event obiekt zdarzenia
	 * @param box pole wpisu
	 * @return boolean czy nak jest dozwolony
	 */
	insert_prize : function(event,box)
	{
		var del=key_mask(event,"0123456789.,",1);
		if(del)
		{
			if($(box).value.indexOf(".")==-1 && $(box).value.indexOf(",")==-1)
			{ return true; }
			else if(del!="." && del!=",")
			{ return true; }
			else { return false; }
		}
		else { return false; }
	},
	/**
	 * Przeliczenie ceny netto -> brutto
	 */
	change_netto : function()
	{
		var vat=parseInt($('vat').value);
		if(isNaN(vat)) { info($('empty_vat').value); }
		else	
		{
			$('netto').value=$('netto').value.replace(",",".");
			$('brutto').value=round($('netto').value*(10000+vat*100)/10000,4);
		}
	},
	/**
	 * Przeliczenie ceny brutto -> netto
	 */
	change_brutto : function()
	{
		var vat=parseInt($('vat').value);
		if(isNaN(vat)) { info($('empty_vat').value); }
		else	
		{
			$('brutto').value=$('brutto').value.replace(",",".");
			$('netto').value=round($('brutto').value*10000/(10000+vat*100),4);
		}
	}
};