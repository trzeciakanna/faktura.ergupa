<?php
$lang=array();
$lang['head']="Dodaj produkt";

$lang['cat']="Wybierz kategorie";
$lang['number']="Numer";
$lang['name']="Nazwa";
$lang['producer']="Producent";
$lang['pkwiu']="PKWiU";
$lang['unit']="Jednostka";
$lang['vat']="Stawka VAT";
$lang['netto']="Cena NETTO";
$lang['brutto']="Cena BRUTTO";

$lang['info']="Pola są wymagane";
$lang['empty_cat']="Wybierz kategorię";
$lang['empty_name']="Podaj nazwę produktu";
$lang['empty_vat']="Wybierz stawkę VAT";
$lang['empty_prize']="Podaj cenę produktu";

$lang['cancel']="Anuluj";
$lang['add']="Dodaj nowy produkt";

$lang['add_ok']="Produkt dodany poprawnie";
$lang['add_error']="Wystąpił błąd podczas dodawania produktu. Spróbuj ponownie za chwilę.";


$lang['nagl1'] ='Faktura VAT';
$lang['nagl2'] ='Jesteś w';
$lang['nagl3'] ='Wprowadź nazwę produktu';
$lang['nagl4'] ='lub';
$lang['nagl5'] ='dodaj nową kategorie';
$lang['nagl6'] ='Możesz nadać numer produktowi';
$lang['nagl7'] ='Wprowadź jeżeli podatek Vat jest inny niż 22%.';
$lang['nagl8'] ='Wybierz domyślną jednostkę.';
$lang['nagl9'] ='Wybierz podatek VAT';
$lang['nagl10'] ='Wprowadź cenne NETTO';
$lang['nagl11'] ='Wprowadź cenę BRUTTO';
?>