<?php 

class products
{
	/**
	 * Dodanie produktu
	 * @param int $user_id identyfikator użytkownika
	 * @param array $post dane do dodania
	 * @return string wynik wykonania
	 */
	public function add($user_id, $post)
	{
		if(trim($post['name']))
		{
			global $DB;
			global $search;
			$tmp=array();
			$tmp['user_id']=$user_id;
			$tmp['name']=$post['name'];
			$tmp['name_s']=$search->parse($post['name'],1);
			$tmp['producer']=$post['producer'];
			$tmp['producer_s']=$search->parse($post['producer'],1);
			$tmp['cat']=$post['cat'];
			$tmp['number']=$post['number'];
			$tmp['pkwiu']=$post['pkwiu'];
			$tmp['unit']=$post['unit'];
			$tmp['vat']=$post['vat'];
			$tmp['netto']=$post['netto'];
			$tmp['brutto']=$post['brutto'];
			$DB->AutoExecute("product",$tmp,"INSERT");
			return "ok";
		}
		else { return "error"; }
	}
	/**
	 * Uaktualnienie produktu
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator produktu
	 * @param array $post dane do zapisu
	 * @return string wynik wykonania
	 */
	public function save($user_id, $id, $post)
	{
		if(trim($post['name']))
		{
			global $DB;
			global $search;
			$tmp=array();
			$tmp['name']=$post['name'];
			$tmp['name_s']=$search->parse($post['name'],1);
			$tmp['producer']=$post['producer'];
			$tmp['producer_s']=$search->parse($post['producer'],1);
			$tmp['cat']=$post['cat'];
			$tmp['number']=$post['number'];
			$tmp['pkwiu']=$post['pkwiu'];
			$tmp['unit']=$post['unit'];
			$tmp['vat']=$post['vat'];
			$tmp['netto']=$post['netto'];
			$tmp['brutto']=$post['brutto'];
			$DB->AutoExecute("product",$tmp,"UPDATE","`user_id`='".$user_id."' AND `id`='".$id."'");
			return "ok";
		}
		else { return "error"; }
	}
	/**
	 * Usunięcie produktu
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator produktu
	 */
	public function del($user_id, $id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `product` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 1");
	}
	/**
	 * Odczyt pojedynczego produktu
	 * @param $user_id identyfikator użytkownika
	 * @param $id identyfikator produktu
	 * @return mixed obiekt produktu
	 */
	public function read_one($user_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT * FROM `product` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 0,1");
		return new product($r->fields['id'],$r->fields['cat'],$r->fields['name'],$r->fields['producer'],$r->fields['netto'],$r->fields['brutto'],$r->fields['number'],$r->fields['pkwiu'],$r->fields['unit'],$r->fields['vat']);
	}
	/**
	 * Lista produktów
	 * @param int $user_id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param int $cat identyfikator kategorii
	 * @param string $letter pierwsza litera nazyw
	 * @param boolean $tree czy przeszukiwać drzewo kategorii
	 * @return array tablica z danymi
	 */
	public function read_list($user_id, $page=1, $ile=20, $cat=0, $letter="", $tree=true)
	{
		global $DB;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		$war=$letter?"AND product.name LIKE '".$letter."%' ":"";
		$war.=$cat?($tree?"AND product.cat in (".implode(",",$this->cat_tree_list($user_id, $cat, array())).")":"AND product.cat='".$cat."'"):"";
		$r=$DB->PageExecute("SELECT product.id, product.name, product.producer, product_cat.name AS cat, product.netto, product.brutto FROM product INNER JOIN product_cat ON product.cat=product_cat.id WHERE product.user_id='".$user_id."' ".$war." ORDER BY product.name ASC",$ile,$page);
		if($r->fields['name'])
		{
			while(!$r->EOF)
			{
				$dane[]=new product($r->fields['id'],0,$r->fields['name'],$r->fields['producer'],$r->fields['netto'],$r->fields['brutto'],null,null,null,null,$r->fields['cat']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Wyszukiwarka produktów
	 * @param int $user_id identyfikator użytkownika
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $name nazwa produktu
	 * @param string $producer nazwa producenta
	 * @param string $number numer produktu
	 * @param string $pkwiu numer PKWiU produktu
	 * @param decimal $prize_min cena minimalna
	 * @param decimal $prize_max cena maksymalna
	 * @param int $cat kategoria produktu
	 * @return array tablica z danymi
	 */
	public function read_list_search($user_id, $page=1, $ile=20, $name="", $producer="", $number="", $pkwiu="", $prize_min=0, $prize_max=0, $cat=0)
	{
		global $DB;
		global $search;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		//warunki
		if($name)
		{
			list($and, $not)=$search->phrase($name);
			$war=" AND search_engine(name_s,'".$and."','".$not."')";
		}
		else { $war=""; }
		if($producer)
		{
			list($and, $not)=$search->phrase($producer);
			$war.=" AND search_engine(producer_s,'".$and."','".$not."')";
		}
		$war.=$number?" AND product.number LIKE '%".$number."%'":"";
		$war.=$pkwiu?" AND pkwiu LIKE '%".$pkwiu."%'":"";
		$war.=$prize_min?" AND (product.netto>='".$prize_min."' OR product.brutto>='".$prize_max."')":"";
		$war.=$prize_max?" AND (product.netto<='".$prize_max."' OR product.brutto<='".$prize_max."')":"";
		$war.=$cat?"AND product.cat in (".implode(",",$this->cat_tree_list($user_id, $cat, array())).")":"";
		$r=$DB->PageExecute("SELECT product.id, product.name, product.producer, product_cat.name AS cat, product.netto, product.brutto FROM product INNER JOIN product_cat ON product.cat=product_cat.id WHERE product.user_id='".$user_id."' ".$war." ORDER BY product.name ASC",$ile,$page);
		if($r->fields['name'])
		{
			while(!$r->EOF)
			{
				$dane[]=new product($r->fields['id'],0,$r->fields['name'],$r->fields['producer'],$r->fields['netto'],$r->fields['brutto'],null,null,null,null,$r->fields['cat']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Odczyt danych do exportu
	 * @param int $user_id identyfikator użytkownika
	 * @return array tablica z danymi
	 */
	public function read_export($user_id)
	{
		global $DB;
		$data=array();
		$r=$DB->Execute("SELECT * FROM `product` WHERE `user_id`='".$user_id."' ORDER BY `name` ASC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$data[]=new product($r->fields['id'],$r->fields['cat_id'],$r->fields['name'],$r->fields['producer'],$r->fields['netto'],$r->fields['brutto'],$r->fields['number'],$r->fields['pkwiu'],$r->fields['unit'],$r->fields['vat'],$r->fields['cat']);
				$r->MoveNext();
			}
		}
		return $data;
	}
	
	/**
	 * Litery od których zaczynają się nazwy produktów
	 * @param int $user_id identyfikator użytkownika
	 * @param int $cat identyfikator kategorii
	 * @return array tablica liter
	 */
	public function letter_list($user_id, $cat=0)
	{
		global $DB;
		$dane=array();
		$war=$cat?"AND `cat`='".$cat."'":"";
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(name),1,1) as `letter` FROM `product` WHERE `user_id`='".$user_id."' ".$war." GROUP BY SUBSTRING(name,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Dodanie kategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $parent identyfikator rodzica
	 * @param string $name nazwa kategorii
	 */
	public function cat_add($user_id, $parent, $name)
	{
		global $DB;
		if($name)
		{
			$tmp=array();
			$tmp['user_id']=$user_id;
			$tmp['parent']=$parent;
			$tmp['name']=$name;
			$DB->AutoExecute("product_cat",$tmp,"INSERT");
		}
		return "ok";
	}
	/**
	 * Zapisanie kategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kategorii
	 * @param string $name nazwa kategorii
	 */
	public function cat_save($user_id, $id, $name)
	{
		global $DB;
		if($name)
		{
			$tmp=array();
			$tmp['name']=$name;
			$DB->AutoExecute("product_cat",$tmp,"UPDATE","`user_id`='".$user_id."' AND `id`='".$id."'");
		}
		return "ok";
	}
	/**
	 * Usunięcie kategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kategorii
	 */
	public function cat_del($user_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `product_cat` WHERE `parent`='".$id."' AND `user_id`='".$user_id."'");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$this->cat_del($user_id, $r->fields['id']);
				$r->MoveNext();
			}
		}
		$DB->Execute("DELETE FROM `product` WHERE `user_id`='".$user_id."' AND `cat`='".$id."'");
		$DB->Execute("DELETE FROM `product_cat` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 1");
		return "ok";
	}
	/**
	 * Odczyt pojedynczej kategorii
	 * @param $user_id identyfikator użytkownika
	 * @param $id identyfikatro kategorii
	 * @return mixed obiekt kategorii
	 */
	public function cat_one($user_id, $id)
	{
		global $DB;
		$r=$DB->Execute("SELECT * FROM `product_cat` WHERE `user_id`='".$user_id."' AND `id`='".$id."' LIMIT 0,1");
		return new cat($r->fields['id'],$r->fields['parent'],$r->fields['name']);
	}
	/**
	 * Odczyt drzewa kategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $parent identyfikator rodzica
	 * @param int $level poziom zagnieżdżenia
	 * @param array $counter tablica z ilością produktów
	 * @return array tablica obiektów kategorii
	 */
	public function cat_tree($user_id, $parent=0, $level=1, $counter=array())
	{
		global $DB;
		$tmp=array();
		$sum=0;
		$r=$DB->Execute("SELECT `id`,`name` FROM `product_cat` WHERE `user_id`='".$user_id."' AND `parent`='".$parent."' ORDER BY `name` ASC");
		if($r->fields['name'])
		{
			while(!$r->EOF)
			{
				list($child,$s)=$this->cat_tree($user_id,$r->fields['id'],$level+1,$counter);
				$tmp[]=new cat($r->fields['id'],$parent,$r->fields['name'],$level+1,$child,$counter[$r->fields['id']]+$s);
				$sum+=$counter[$r->fields['id']]+$s;
				$r->MoveNext();
			}
		}
		return array($tmp,$sum);
	}
	/**
	 * Odczyt listy kategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $parent identyfikator rodzica
	 * @param array $counter tablica z ilością produktów
	 * @return array tablica obiektów kategorii
	 */
	public function cat_list($user_id, $parent=0, $counter=array())
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `id`,`name` FROM `product_cat` WHERE `user_id`='".$user_id."' AND `parent`='".$parent."' ORDER BY `name` ASC");
		if($r->fields['name'])
		{
			while(!$r->EOF)
			{
				$ile=0;
				$t=$this->cat_tree_list($user_id, $r->fields['id'], array());
				for($a=0,$max=count($t);$a<$max;$a++)
				{ $ile+=$counter[$t[$a]]; }
				$tmp[]=new cat($r->fields['id'],$parent,$r->fields['name'],0,array(),$ile);
				$r->MoveNext();
			}
		}
		return $tmp;
	}
	/**
	 * Utworzenie listy podkategorii
	 * @param int $user_id identyfikator użytkownika
	 * @param int $id identyfikator kategorii
	 * @param array $tmp tablica danych
	 * @return array tablica z kategoriami
	 */
	public function cat_tree_list($user_id, $id=0, $tmp=array())
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `product_cat` WHERE `user_id`='".$user_id."' AND `parent`='".$id."'");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp=$this->cat_tree_list($user_id,$r->fields['id'],$tmp);
				$r->MoveNext();
			}
		}
		$tmp[]=$id;
		return $tmp;
	}
	/**
	 * Ilość produktów w kategoriach
	 * @param int $user_id id użytkownika
	 * @return array tablica z ilością produktów przypisana do kategorii
	 */
	public function counter($user_id)
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc`,`cat` FROM `product` WHERE `user_id`='".$user_id."' GROUP BY `cat`");
		if($r->fields['cat'])
		{
			while(!$r->EOF)
			{
				$tmp[$r->fields['cat']]=$r->fields['ilosc'];
				$r->MoveNext();
			}
		}
		return $tmp;
	}
}

class product
{
	public $id;
	public $cat;
	public $number;
	public $name;
	public $producer;
	public $pkwiu;
	public $unit;
	public $vat;
	public $netto;
	public $brutto;
	public $cat_n;
	/**
	 * 
	 * @param int $id identyfikator produktu
	 * @param int $cat identyfikator kategorii
	 * @param string $name nazwa
	 * @param string $producer nazwa producenta
	 * @param decimal $netto cena netto
	 * @param decimal $brutto cena brutto
	 * @param string $number numer identyfikacyjny
	 * @param string $pkwiu numer PKWiU
	 * @param string $unit jednostka
	 * @param int $vat stawka VAT
	 * @param string $cat_n nazwa kategorii
	 */
	public function __construct($id, $cat, $name, $producer=null, $netto=null, $brutto=null, $number=null, $pkwiu=null, $unit=null, $vat=null, $cat_n=null)
	{
		global $func;
		$this->id=$id;
		$this->cat=$cat;
		$this->name=$func->show_with_html($name);
		$this->producer=$func->show_with_html($producer);
		$this->number=$number;
		$this->pkwiu=$pkwiu;
		$this->unit=$unit;
		$this->vat=$vat;
		$this->netto=$netto;
		$this->brutto=$brutto;
		$this->cat_n=$cat_n;
	}
}

class cat
{
	public $id;
	public $parent;
	public $name;
	public $level;
	public $child;
	public $counter;
	/**
	 * 
	 * @param int $id identyfikator kategorii
	 * @param int $parent identyfikator rodzica
	 * @param string $name nazwa kategorii
	 * @param int $level poziom zagnieżdżenia
	 * @param array $child dzieci kategorii
	 * @param int $counter ilość produktów w kategorii
	 */
	public function __construct($id, $parent, $name, $level=1, $child=array(), $counter=0)
	{
		global $func;
		$this->id=$id;
		$this->parent=$parent;
		$this->name=$func->show_with_html($name);
		$this->level=$level;
		$this->child=$child;
		$this->counter=(int)$counter;
	}
}
?>