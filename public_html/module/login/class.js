login_obj = {
	/**
	 * Sprawdzenie logowania
	 */
	submit : function() 
	{
		
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				var ref=$('log_referer').value;
				if(ref.replace("/panel/","")!=ref || ref.replace("/admin/","")!=ref)
				{ location.href=ref; }
				else { location.href="/panel/"; }
			}
			else if(this.responseText=="error")
			{ info($('log_bad').value, 'login');
				$('pass').style.borderColor = 'red';
				$('login').style.borderColor = 'red';}
			else
			{ info($('log_error').value); }
		};
		req.AddParam("login",$("login").value);
		req.AddParam("pass",$("pass").value);
		req.AddParam("remember",$("remember").value);
		req.Send("module/login/ajax_check.php");
		return false;
	},
	
	change_style : function(val,styl)
	{
    $('domyslny_styl').href=val;
    setCookie('style',val,90);
	if(styl == 1){
		$('change_style_1').style.display= "none";
		$('change_style_2').style.display= "block";
	} else{
		$('change_style_2').style.display= "none";
		$('change_style_1').style.display= "block";
	}
	//window.location.reload();
  }
};