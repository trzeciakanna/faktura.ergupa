<?php
$lang=array();
$lang['head']="Logging In";
$lang['login']="Login";
$lang['pass']="Password";
$lang['remember']="Remember Me";
$lang['reg']="Sing Up";
$lang['new_pass']="Forgot the password";
$lang['send']="Log In";
$lang['log_bad']="Incorrect data.";
$lang['log_error']="An error occurred while adding the account; please try again in a few minutes.";
$lang['hello']="Hello";
$lang['hello2']="Hello Stranger!";
$lang['logout']="Log Out";
$lang['last_login']="Last Log-In:";
$lang['abonament']="Subscription Valid Until:";
$lang['zaloguj1'] = ' Log In ';
$lang['zaloguj2'] = 'or';
$lang['zaloguj3'] = 'Sign Up';
$lang['zaloguj4'] = 'Sign Up';
$lang['przdluzAbonament'] = 'Extend your Subscription ';
?>
