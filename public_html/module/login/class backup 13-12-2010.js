login_obj = {
	/**
	 * Sprawdzenie logowania
	 */
	submit : function() 
	{
		
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				var ref=$('log_referer').value;
				if(ref.replace("/panel/","")!=ref || ref.replace("/admin/","")!=ref)
				{ location.href=ref; }
				else { location.href="/panel/"; }
			}
			else if(this.responseText=="error")
			{ info($('log_bad').value); }
			else
			{ info($('log_error').value); }
		};
		req.AddParam("login",$("login").value);
		req.AddParam("pass",$("pass").value);
		req.AddParam("remember",$("remember").value);
		req.Send("module/login/ajax_check.php");
		return false;
	}
};