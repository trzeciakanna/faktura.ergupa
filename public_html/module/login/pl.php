<?php
$lang=array();
$lang['head']="Logowanie";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['remember']="Zapamiętaj mnie";
$lang['reg']="Rejestracja";
$lang['new_pass']="Zapomniałem hasła";
$lang['send']="Loguj";
$lang['log_bad']="Podane dane są błędne.";
$lang['log_error']="Wystąpił błąd podczas dodawania konta, prosimy spróbować ponownie za kilka minut.";
$lang['hello']=" Twoje<br>konto";
$lang['hello2']="Witaj nieznajomy!";
$lang['logout']="Wyloguj";
$lang['last_login']="Ostatnie logowanie:";
$lang['abonament']="Abonament do:";
$lang['zaloguj1'] = ' Zaloguj';
$lang['zaloguj2'] = 'lub';
$lang['zaloguj3'] = 'Utwórz konto';
$lang['zaloguj4'] = 'Utwórz<br>konto';
$lang['przdluzAbonament'] = ' - Przedłuż ważność';
?>
