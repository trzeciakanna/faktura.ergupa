<?php

class navi
{
	public $title;
	public $desc;
	public $keys;
	public $navi;
	
	private $meta;
	private $lvl;
	
	public function __construct($lang)
	{
		//wyczyszczenie
		$this->navi=array();
		$this->meta=array();
		$this->lvl=100;
		//dodanie pierwszych
		if(!empty($_GET['par1']) AND $_GET['par1']!="main")
		{
			$this->navi[]=new navi_item($lang['main'],"main/");
			//$this->meta[]=$lang['title'];
		}
		//dodanie reszty
		switch($_GET['par1'])
		{
			case "news":
				$this->news($lang);
				break;
			case "invoice":
				$this->navi[]=new navi_item($lang['invoice'],"invoice/");
				$this->meta[]=$lang['invoice'];
				break;
			case "faktura-zaliczkowa":
				$this->navi[]=new navi_item($lang['faktura-zaliczkowa'],"faktura-zaliczkowa/");
				$this->meta[]=$lang['faktura-zaliczkowa'];
				break;	
			case "pro-forma-invoice":
				$this->navi[]=new navi_item($lang['faktura-pro-forma'],"faktura-pro-forma/");
				$this->meta[]=$lang['faktura-pro-forma'];
				break;	
			case "faktura-korygujaca":
				$this->navi[]=new navi_item($lang['faktura-korygujaca'],"faktura-korygujaca/");
				$this->meta[]=$lang['faktura-korygujaca'];
				break;	
			case "rachunek":
				$this->navi[]=new navi_item($lang['rachunek'],"rachunek/");
				$this->meta[]=$lang['rachunek'];
				break;		
			case "contact":
				$this->navi[]=new navi_item($lang['contact'],"contact/");
				$this->meta[]=$lang['contact'];
				break;
			case "cennik":
				$this->navi[]=new navi_item($lang['cennik'],"cennik/");
				$this->meta[]=$lang['cennik'];  
				break;	
			case "pomoc":
				require(ROOT_DIR."module/pomoc/".LANG.".php");
				if(empty($_GET['par2'])){
					$page = 0;
				} else {
					$page = $_GET['par2'];
				}
				$this->meta[]= $pomoc[$page][1];	
				break;	
			case "funkcje":
				$this->navi[]=new navi_item($lang['funkcje'],"funkcje/");
				$this->meta[]=$lang['funkcje'];	
				break;		
			case "reg":
				$this->navi[]=new navi_item($lang['reg'],"reg/");
				$this->meta[]=$lang['reg'];
				break;
			case "panel":
				$this->panel($lang);
				break;
			default:
				$this->lvl=1;
				break;
		}
		//wygenerowanie danych
		$this->meta=array_reverse($this->meta);
		for($a=0,$max=min(count($this->meta),$this->lvl);$a<$max;$a++)
		{
			$this->title.=($a!=0?" - ":"").$this->meta[$a];
			$this->desc.=($a!=0?" - ":"").$this->meta[$a];
			$this->keys.=($a!=0?", ":"").$this->meta[$a];
		}
	}
	
	private function news($lang)
	{
		$this->navi[]=new navi_item($lang['news'],"news/");
		$this->meta[]=$lang['news'];
		if($_GET['par3'])
		{
			global $DB;
			global $func;
			$r=$DB->Execute("SELECT `title` FROM `news` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
			$this->navi[]=new navi_item($func->show_with_html($r->fields['title']),"news/".$_GET['par2']."/".$_GET['par3']."/");
			$this->meta[]=$func->show_with_html($r->fields['title']);
		}
		if($_GET['par4']=="contact")
		{
			$this->navi[]=new navi_item($lang['add_coment'],"news/".$_GET['par2']."/".$_GET['par3']."/contact/");
			$this->meta[]=$lang['add_coment'];
		}
	}
	
	private function panel($lang)
	{
		$this->navi[]=new navi_item($lang['panel'],"panel/");
		$this->meta[]=$lang['panel'];
		switch($_GET['par2'])
		{
			case "main":
				$this->navi[]=new navi_item($lang['panelp']['main'],"panel/main/");
				$this->meta[]=$lang['panelp']['main'];
				break;
			case "data":
				$this->navi[]=new navi_item($lang['panelp']['data'],"panel/data/");
				$this->meta[]=$lang['panelp']['data'];
				if($_GET['par3']=="general")
				{
					$this->navi[]=new navi_item($lang['panelp']['data_general'],"panel/data/general/");
					$this->meta[]=$lang['panelp']['data_general'];
				}
				elseif($_GET['par3']=="licence")
				{
					$this->navi[]=new navi_item($lang['panelp']['data_licence'],"panel/data/licence/");
					$this->meta[]=$lang['panelp']['data_licence'];
				}
				elseif($_GET['par3']=="szablon-faktury")
				{
					$this->navi[]=new navi_item($lang['panelp']['szablon_faktury'],"panel/data/szablon-faktury");
					$this->meta[]=$lang['panelp']['szablon_faktury'];
				}
				break;
			case "client":
				$this->navi[]=new navi_item($lang['panelp']['client'],"panel/client/");
				$this->meta[]=$lang['panelp']['client'];
				$this->panel_client($lang);
				break;
			case "invoice":
				$this->navi[]=new navi_item($lang['panelp']['invoice'],"panel/invoice/");
				$this->meta[]=$lang['panelp']['invoice'];
				$this->panel_invoice($lang);
				break;
			case "product":
				$this->navi[]=new navi_item($lang['panelp']['product'],"panel/product/");
				$this->meta[]=$lang['panelp']['product'];
				$this->panel_product($lang);
				break;
			default:
				break;
		}
	}
	
	private function panel_product($lang)
	{
		switch($_GET['par3'])
		{
			case "add":
				$this->navi[]=new navi_item($lang['panelp']['product_add'],"panel/product/add/");
				$this->meta[]=$lang['panelp']['product_add'];
				break;
			case "list":
				$this->navi[]=new navi_item($lang['panelp']['product_list'],"panel/product/list/");
				$this->meta[]=$lang['panelp']['product_list'];
				if($_GET['par4']=="view")
				{
					global $DB;
					global $func;
					$r=$DB->Execute("SELECT `name` FROM `product` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
					$this->navi[]=new navi_item($lang['panelp']['product_view'].": ".$func->show_with_html($r->fields['name']),"panel/product/list/view/".$_GET['par5']."/");
					$this->meta[]=$lang['panelp']['product_view'].": ".$func->show_with_html($r->fields['name']);
				}
				elseif($_GET['par4']=="edit")
				{
					global $DB;
					global $func;
					$r=$DB->Execute("SELECT `name` FROM `product` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
					$this->navi[]=new navi_item($lang['panelp']['product_edit'].": ".$func->show_with_html($r->fields['name']),"panel/product/list/edit/".$_GET['par5']."/");
					$this->meta[]=$lang['panelp']['product_edit'].": ".$func->show_with_html($r->fields['name']);
				}
				break;
			case "cat":
				$this->navi[]=new navi_item($lang['panelp']['product_cat'],"panel/product/cat/");
				$this->meta[]=$lang['panelp']['product_cat'];
				break;
			default:
				break;
		}
	}
	
	private function panel_client($lang)
	{
		switch($_GET['par3'])
		{
			case "add":
				$this->navi[]=new navi_item($lang['panelp']['client_add'],"panel/client/add/");
				$this->meta[]=$lang['panelp']['client_add'];
				break;
			case "list":
				$this->navi[]=new navi_item($lang['panelp']['client_list'],"panel/client/list/");
				$this->meta[]=$lang['panelp']['client_list'];
				switch($_GET['par4'])
				{
					case "branch":
					case "badd":
					case "bedit":
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `sign` FROM `clients` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['client_branch'].": ".$func->show_with_html($r->fields['sign']),"panel/client/list/branch/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['client_branch'].": ".$func->show_with_html($r->fields['sign']);
						if($_GET['par4']=="badd")
						{
							$this->navi[]=new navi_item($lang['panelp']['client_branch_add'],"panel/client/list/badd/".$_GET['par5']."/");
							$this->meta[]=$lang['panelp']['client_branch_add'];
						}
						elseif($_GET['par4']=="bedit")
						{
							$r=$DB->Execute("SELECT `sign` FROM `branches` WHERE `id`='".(int)$_GET['par6']."' LIMIT 0,1");
							$this->navi[]=new navi_item($lang['panelp']['client_branch_edit'].": ".$func->show_with_html($r->fields['sign']),"panel/client/list/bedit/".$_GET['par5']."/".$_GET['par6']."/");
							$this->meta[]=$lang['panelp']['client_branch_edit'].": ".$func->show_with_html($r->fields['sign']);
						}
						break;
					case "edit":
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `sign` FROM `clients` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['client_edit'].": ".$func->show_with_html($r->fields['sign']),"panel/client/list/edit/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['client_edit'].": ".$func->show_with_html($r->fields['sign']);
					default:
						break;
				}
				break;
			case "send":
				$this->navi[]=new navi_item($lang['panelp']['client_send'],"panel/client/send/");
				$this->meta[]=$lang['panelp']['client_send'];
				switch($_GET['par4'])
				{
					case "last":
						$this->navi[]=new navi_item($lang['panelp']['client_send_last'],"panel/client/send/last/");
						$this->meta[]=$lang['panelp']['client_send_last'];
						break;
					case "view":
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `topic` FROM `clients_msg` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['client_send_view'].": ".$func->show_with_html($r->fields['topic']),"panel/client/send/view/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['client_send_view'].": ".$func->show_with_html($r->fields['topic']);
						break;
					case "resend":
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `topic` FROM `clients_msg` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['client_send_resend'].": ".$func->show_with_html($r->fields['topic']),"panel/client/send/resend/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['client_send_resend'].": ".$func->show_with_html($r->fields['topic']);
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private function panel_invoice($lang)
	{
		switch($_GET['par3'])
		{
			case "list":
				$this->navi[]=new navi_item($lang['panelp']['invoice_list'],"panel/invoice/list/");
				$this->meta[]=$lang['panelp']['invoice_list'];
				if($_GET['par4']=="client")
				{
					if(is_numeric($_GET['par5']) AND $_GET['par5'])
					{
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `sign` FROM `clients` WHERE `id`='".(int)$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['sign']),"panel/invoice/list/client/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['sign']);
					}
					elseif($_GET['par5'])
					{
						global $DB;
						global $func;
						$r=$DB->Execute("SELECT `name` FROM `invoice_data` WHERE `name_url`='".$_GET['par5']."' LIMIT 0,1");
						$this->navi[]=new navi_item($lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['name']),"panel/invoice/list/client/".$_GET['par5']."/");
						$this->meta[]=$lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['name']);
					}
					else
					{
						$this->navi[]=new navi_item($lang['panelp']['invoice_other'],"panel/invoice/list/client/0/");
						$this->meta[]=$lang['panelp']['invoice_other'];
					}
				}
				break;
			case "search":
				$this->navi[]=new navi_item($lang['panelp']['invoice_search'],"panel/invoice/search/");
				$this->meta[]=$lang['panelp']['invoice_search'];
				if(is_numeric($_GET['par4']) AND $_GET['par4'])
				{
					global $DB;
					global $func;
					$r=$DB->Execute("SELECT `sign` FROM `clients` WHERE `id`='".(int)$_GET['par4']."' LIMIT 0,1");
					$this->navi[]=new navi_item($lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['sign']),"panel/invoice/search/".$_GET['par4']."/1/dc/");
					$this->meta[]=$lang['panelp']['invoice_user'].": ".$func->show_with_html($r->fields['sign']);
				}
				break;
			default:
				break;
		}
	}
}

class navi_item
{
	public $name;
	public $url;

	public function __construct($name, $url)
	{
		$this->name=$name;
		$this->url=$url;
	}
}
?>