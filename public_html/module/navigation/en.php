<?php
$nlang=array();
//$nlang['main']="Faktura VAT";
$nlang['title']="Online Invoicing Application";
$nlang['invoice']="VAT INVOICE - Create an invoice - An application for Online VAT invoicing";
$nlang['faktura-zaliczkowa']="PRE-PAYMENT INVOICE - Create an invoice - An application for Online PRE-PAYMENT invoicing";
$nlang['faktura-pro-forma']="PRO FORMA INVOICE - Create an invoice - An application for Online PRO FORMA invoicing";
$nlang['rachunek']="SIMPLIFIED BILL - Create an invoice - An application for Online SIMPLIFIED BILL invoicing";
$nlang['faktura-korygujaca']="CORRECTION INVOICE - Create an invoice - An application for Online CORRECTION invoicing";
$nlang['contact']="Contact - An application for Online invoicing - o-invoice.com";
$nlang['cennik']="Pricelist - An application for Online invoicing - o-invoice.com";
$nlang['pomoc']="Help - An application for Online invoicing - o-invoice.com";
$nlang['funkcje']="Functions of the o-invoice.com application - An application for Online invoicing - o-invoice.com";

$nlang['reg']="Registration";
$nlang['news']="News";
$nlang['add_coment']="Add a Comment";
$nlang['panel']="Control Panel";

$lang['panelp']=array();
$nlang['panelp']['main']="Main Page";
$nlang['panelp']['data']="Manage Data";
$nlang['panelp']['client']="Manage Customers";
$nlang['panelp']['invoice']="Manage Invoices";
$nlang['panelp']['product']="Manage Products";

$nlang['panelp']['data_general']="Basic Data";
$nlang['panelp']['data_licence']="Subscription";

$nlang['panelp']['product_add']="Add a Product";
$nlang['panelp']['product_list']="Product List";
$nlang['panelp']['product_cat']="Categories";
$nlang['panelp']['product_view']="Product View";
$nlang['panelp']['product_edit']="Edit Product";

$nlang['panelp']['client_add']="Add a Customer";
$nlang['panelp']['client_list']="Customer List";
$nlang['panelp']['client_edit']="Edit Customer";
$nlang['panelp']['client_send']="Sending a Message";
$nlang['panelp']['client_send_last']="Sent Messages";
$nlang['panelp']['client_send_resend']="Resend";
$nlang['panelp']['client_send_view']="View Messages";
$nlang['panelp']['client_branch']="Branches";
$nlang['panelp']['client_branch_add']="Add a Branch";
$nlang['panelp']['client_branch_edit']="Edit Branch";

$nlang['panelp']['invoice_list']="Invoice List";
$nlang['panelp']['invoice_search']="Invoice Browser";
$nlang['panelp']['invoice_user']="User Invoice List";
$nlang['panelp']['invoice_other']="Other PDF Invoices";
$nlang['panelp']['']="";
$nlang['panelp']['']="";
$nlang['panelp']['']="";
$nlang['panelp']['']="";
$nlang['']="";
$nlang['']="";
$nlang['']="";
?>