<?php
$nlang=array();
//$nlang['main']="Faktura VAT";
$nlang['title']="Aplikacja do wystawiania faktur online";
$nlang['invoice']="FAKTURA VAT - wystawianie faktury - Program do wystawiania Faktury VAT on-line";
$nlang['faktura-zaliczkowa']="FAKTURA ZALICZKOWA - Tworzenie faktury zaliczkowej - Program do wystawiania Faktury zaliczkowej on-line";
$nlang['faktura-pro-forma']="FAKTURA PRO FORMA - Tworzenie faktury proforma - Program do wystawiania Faktury pro forma on-line";
$nlang['rachunek']="RACHUNEK UPROSZCZONY - Wystawianie rachunku - Program do wystawiania Rachunku uproszczonego pro forma on-line";
$nlang['faktura-korygujaca']="FAKTURA KORYGUJĄCA - Tworzenie faktury - Program do wystawiania Faktury Korygującej on-line";
$nlang['contact']="Kontakt - Aplikacja do wystawiania faktur online - faktura.egrupa.pl";
$nlang['cennik']="Cennik - Aplikacja do wystawiania faktur online - faktura.egrupa.pl";
$nlang['pomoc']="Pomoc - Aplikacja do wystawiania faktur online - faktura.egrupa.pl";
$nlang['funkcje']="Funkcje programu faktura.egrupa.pl - Aplikacja do wystawiania faktur online - faktura.egrupa.pl";
$nlang['reg']="Rejestracja";
$nlang['news']="Wiadomości";
$nlang['add_coment']="Dodawanie komentarza";
$nlang['panel']="Panel zarządzania";

$lang['panelp']=array();
$nlang['panelp']['main']="Strona główna";
$nlang['panelp']['data']="Zarządzanie danymi";
$nlang['panelp']['client']="Zarządzanie kontrahentami";
$nlang['panelp']['invoice']="Zarządzanie fakturami";
$nlang['panelp']['product']="Zarządzanie produktami";

$nlang['panelp']['data_general']="Dane podstawowe";
$nlang['panelp']['data_licence']="Abonament";

$nlang['panelp']['product_add']="Dodawanie produktu";
$nlang['panelp']['product_list']="Lista produktów";
$nlang['panelp']['product_cat']="Kategorie";
$nlang['panelp']['product_view']="Widok produktu";
$nlang['panelp']['product_edit']="Edycja produktu";

$nlang['panelp']['client_add']="Dodawanie kontrahenta";
$nlang['panelp']['client_list']="Lista kontrahentów";
$nlang['panelp']['client_edit']="Edycja kontrahenta";
$nlang['panelp']['client_send']="Wysyłanie wiadomości";
$nlang['panelp']['client_send_last']="Wysłane wiadomości";
$nlang['panelp']['client_send_resend']="Ponowne wysyłanie";
$nlang['panelp']['client_send_view']="Podgląd wiadomości";
$nlang['panelp']['client_branch']="Oddziały";
$nlang['panelp']['client_branch_add']="Dodawanie oddziału";
$nlang['panelp']['client_branch_edit']="Edycja oddziału";

$nlang['panelp']['invoice_list']="Lista faktur";
$nlang['panelp']['invoice_search']="Wyszukiwarka faktur";
$nlang['panelp']['invoice_user']="Lista faktur użytkownika";
$nlang['panelp']['invoice_other']="Pozostałe faktury PDF";
$nlang['panelp']['szablon_faktury']="Szablon faktury";
$nlang['panelp']['']="";
$nlang['panelp']['']="";
$nlang['panelp']['']="";
$nlang['']="";
$nlang['']="";
$nlang['']="";
?>