<?php
set_time_limit(360);
if ($_COOKIE["lang"]=="en") { require_once (ROOT_DIR.'includes/tcpdf5/config/lang/eng.php'); }
else { require_once (ROOT_DIR.'includes/tcpdf5/config/lang/pol.php'); }
require_once (ROOT_DIR.'includes/tcpdf5/tcpdf.php');

class mainPDF extends TCPDF
{
	public $margin;
	public $width;
	public $id;
	public $client;
	public $name;
	public $extra;
	
	public function __construct($id)
	{
		parent::__construct("P","pt","A4",true,"UTF-8",false);
		global $l;
		//odczyt danych dodatkowych
		if($id) { $this->extra=new user_extra($id); }
		// rozmiar marginesu
		$this->margin=25;
		// metadane
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', 7));
		$this->SetCreator(PDF_CREATOR);
		$this->SetAuthor('E-GRUPA');
		$this->SetTitle('Faktura');
		$this->SetSubject('Faktura');
		$this->SetKeywords('Faktura, E-GRUPA');
		// dodanie czcionek
		$this->AddFont('consola','','consola');
		$this->AddFont('consola','','consolab.php');
		$this->SetFont('consola','',10);
		$this->SetHeaderFont(array('consola','',8));
		$this->SetFooterFont(array('consola','',8));
		// ustawienie marginesów
		$this->SetMargins($this->margin,$this->margin,$this->margin);
		// odczyt szerokości strony
		$this->width=$this->getPageWidth()-2*$this->margin;
		// ustawienie stopki i nagłówka
		if($id)
		{
			$l['foot']=$this->extra->foot;
			$tmp=explode("\n",$this->extra->foot);
			$w=count($tmp);
			for ($a=0,$max=$w;$a<$max;$a++)
			{ if (strlen($tmp[$a])>120) { $w++; } }
		}
		else
		{ $w=0; }
		$this->setFooterMargin(25+$w*12);
		$this->SetHeaderMargin(0);
		$this->setPrintFooter(true);
		$this->setPrintHeader(false);
		// autodzielenie strony
		$this->SetAutoPageBreak(TRUE,25+$w*12);
		// dodanie tablicy jezykowej
		$this->setLanguageArray($l);
	}
}

function cast_class($object, $newclass)
{
	if(!is_object($object))
	{
		trigger_error('cast_class expects parameter 1 to be object, '.gettype($object).' given', E_USER_WARNING);
		return false;
	}
	if(!class_exists($newclass))
	{
		trigger_error('Class '.$newclass.' not found', E_USER_ERROR);
		return false;
	}
	$serialized_parts=explode(':',serialize($object));
	$serialized_parts[1]=strlen($newclass);
	$serialized_parts[2]='"'. $newclass.'"';
	return unserialize(implode(':',$serialized_parts));
}
?>