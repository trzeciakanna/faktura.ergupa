<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',6600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/panel/data_number/class.php"); 
require(ROOT_DIR."includes/users.php");

/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
       
require(ROOT_DIR."module/panel/invoice/class.php");
require(ROOT_DIR."module/panel/data_extra/class.php");
require(ROOT_DIR."includes/mailer.php");

switch($_GET['fak_type'])
{
	case 1: $dirf="invoice"; $class="class_pdfI"; break;
	case 2: $dirf="faktura-pro-forma"; $class="class_pdfP"; break;
	case 3: $dirf="faktura-zaliczkowa"; $class="class_pdfZ"; break;
	case 4: $dirf="faktura-korygujaca"; $class="class_pdfK"; break;
	case 5: $dirf="rachunek"; $class="class_pdfR"; break;
	default: $dirf="invoice"; $class="class_pdfI"; break;
}
if($_GET['user_id'])
{
	require(ROOT_DIR."module/panel/szablon-faktury/class.php");
	$data=new user_color($_GET['user_id'],$_GET['fak_type']);
	foreach($data->colors as $k=>$v) { $_POST[$k]=$v; }
	require(ROOT_DIR.'module/'.$dirf.'/szablony/class_pdf'.$data->tpl.'.php');
}
else { require(ROOT_DIR."module/".$dirf."/class_pdf.php"); }

$pdf=new $class($_GET['user_id'],$_GET['count'],$_POST,$_GET['what']?$_GET['what']:2);
$path=str_replace(ROOT_DIR,"",urldecode($pdf->savePdf()));
$user=array();
$user[]=array("mail"=>$_GET['mail'],"name"=>$_GET['mail']);
$file=array();
//$file[]=array("path"=>ROOT_DIR.$path,"name"=>"faktura-egrupa-pl.pdf");
$_POST['number']=$_POST['number']?$_POST['number']:"faktura-egrupa-pl";
if($dirf=="invoice") { $dirf="faktura"; }
$file[]=array("path"=>ROOT_DIR.$path,"name"=>$dirf."-".str_replace("/","-",$_POST['number']).".pdf");
$x=array();
if($_GET['user_id'])
{
  $u=new users_invoice($_GET['user_id']);
  $x['name']=$u->name;
}
else { $x['name']=$_GET['mail']; }   
$x['number']=$_POST['number'];

$mailer=new mailer($u->mail);
$result=$mailer->send($user,"send_invoice",$x,$file);
echo ($result?"ok":"error");
?>