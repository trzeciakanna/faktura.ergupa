<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',6600);
ini_set('memory_limit','128M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."module/panel/data_extra/class.php");

if(!$_GET['user_id'])
{
  $_SESSION['datePDF']=max($_SESSION['datePDF'],$_COOKIE['datePDF']);
  if($_SESSION['datePDF']<date('Ymd'))
  {
    $_SESSION['datePDF']=date('Ymd');
    setcookie('datePDF',date('Ymd'),time()+300,'/');
    $_SESSION['countPDF']=1;
  }
  elseif(isset($_SESSION['countPDF']))
  { $_SESSION['countPDF']=$_SESSION['countPDF']+1; }
  else
  { $_SESSION['countPDF']=$_COOKIE['countPDF']+1; }
  setcookie('countPDF',$_SESSION['countPDF'],time()+300,'/');

  if($_SESSION['countPDF']>3)
  { echo "errLog"; exit(); }
}
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();

$t="";
if($_GET['user_id'])
{
  require(ROOT_DIR."module/panel/szablon-faktury/class.php");
  $data=new user_color($_GET['user_id'],1);
  foreach($data->colors as $k=>$v) { $_POST[$k]=$v; }
  require(ROOT_DIR.'module/invoice/szablony/class_pdf'.$data->tpl.'.php');
}
else
{
  //require(ROOT_DIR."module/invoice/class_pdf.php");
  $_POST['kolor_tla_nagl']="e0e0e0";
  $_POST['kolor_ramki']="000000";
  $_POST['kolor_tekstu']="000000";
  $_POST['kolor_tekstu_nagl']="000000";   
  $_POST['kolor_dodatkowy']="aaaaaa";
  $_POST['pokaz_naglowki']=1;
  require(ROOT_DIR.'module/invoice/szablony/class_pdf1.php');
}

/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
$pdf=new class_pdfI((int)$_GET['user_id'],$_GET['count'],$_POST,$_GET['what']);
echo $pdf->savePdf();
?>