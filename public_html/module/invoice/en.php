<?php
$lang=array();
//nagłowek faktury
$lang['head']="VAT Invoice";
$lang['number']="#:";
$lang['original']="ORIGINAL";
$lang['copy']="COPY";
$lang['double']="DUPLICATE";
//miejsce i data wystawienia/sprzedaży
$lang['place']="Place of Issue:";
$lang['date_create']="Date of Issue:";
$lang['date_sell']="Date of Sale:";    
$lang['date_deadline']="Date of service ends:";
//dane użytkownika   
$lang['user_name_main']="Seller:";
$lang['user_name']="Seller:";
$lang['user_address']="Address:";
$lang['user_nip']="Tax ID#:";
$lang['user_phone']="Telephone:";
$lang['user_mail']="E-mail:";
$lang['user_krs']="Companies House #:";
$lang['user_bank_name']="Bank Name:";
$lang['user_account']="ACCOUNT:";  
$lang['user_www']="WWW:";
//dane kontrahenta   
$lang['client_name_main']="Buyer:";       
$lang['client_title']="Customer:";
$lang['client_name']="Buyer:";
$lang['client_address']="Address:";
$lang['client_nip']="Tax ID#:";
$lang['client_phone']="Telephone:";
$lang['client_mail']="E-mail:";
$lang['client_krs']="Companies House #:";
$lang['client_bank_name']="Bank Name:";
$lang['client_account']="ACCOUNT:";
//dane dostawy
$lang['branch']="Delivery Address";
$lang['branch_name']="Branch:";
$lang['branch_address']="Address:";
$lang['branch_phone']="Telephone:";
$lang['branch_mail']="E-mail:";
$lang['branch_krs']="Companies House #:";
$lang['branch_bank_name']="Bank Name:";
$lang['branch_account']="ACCOUNT:";
//nagłówki produktów
$lang['lp']="#";
$lang['name']="Name of goods/service";
$lang['pkwiu']="Polish Goods Classification #";
$lang['amount']="Quantity";
$lang['unit']="Unit";
$lang['netto']="Net Price";
$lang['rabat']="Discount %";
$lang['prize_netto']="Net Price";
$lang['vat']="VAT";
$lang['sum_netto']="Net Amount";
$lang['sum_vat']="VAT Amount";
$lang['sum_brutto']="Gross Amount";
//nagłówki podsumowania
$lang['all_rate']="VAT Rate";
$lang['all_netto']="Net";
$lang['all_vat']="VAT";
$lang['all_brutto']="Gross";
$lang['all_sum']="Total:";
//nagłówki sumy
$lang['cash_word']="Amount in words:";
$lang['cash_pay']="Paid:";
$lang['cash_sum']="Remaining To Be Paid:";
$lang['cash_type']="Payment Method:";
$lang['cash_date']="Payment Deadline:";
//opcje zapłaty
$lang['cash_option']=array();
$lang['cash_option'][]="wire transfer";
$lang['cash_option'][]="cash";
$lang['cash_option'][]="paid";
$lang['cash_option'][]="cash on delivery";
$lang['cash_option'][]="barter";
$lang['cash_option'][]="payment card";
//terminy zapłaty
$lang['cash_deadline']=array();
$lang['cash_deadline'][]="3 days";
$lang['cash_deadline'][]="7 days";
$lang['cash_deadline'][]="14 days";
$lang['cash_deadline'][]="21 days";
$lang['cash_deadline'][]="28 days";
$lang['cash_deadline'][]="40 days";
$lang['cash_deadline'][]="paid";
$lang['cash_deadline'][]="cash on delivery";
//podpisy
$lang['client_sign']="Person authorized to receive";
$lang['user_sign']="Person authorized to issue";
//dodatkowe
$lang['coment']="Additional Information/Comment";
$lang['strike']="mark";
$lang['show']="pull down";
$lang['hide']="pull up";    
$lang['n2b']="Calculate Net to Gross";
$lang['b2n']="Calculate Gross to Net";
//błędy i komunikaty
$lang['bad_nip']="Wrong Tax ID #";
$lang['bad_krs']="Wrong Companies House #";
$lang['bad_regon']="Wrong REGON #";
$lang['add']="Add Item";
$lang['del']="Remove Item";
$lang['delete']="Do you really want to clear the invoice";
$lang['save_ok']="Invoice sucessfully saved";
$lang['save_error']="An error occurred while saving the invoice. Try again later";

$lang['addc_ok']="Customer successfully added";
$lang['addc_error']="An error occurred while adding the customer. Try again later";
$lang['addp_ok']="Product successfully added";
$lang['addp_error']="An error occurred while adding the product. Try again later"; 
$lang['same_nr']="The invoice with this number already exists, do you want to continue?";
//zapis słowny kwot
$lang['number_coin']="zł,gr";
$lang['number_tou']="thousand,thousand,thousand";
$lang['number_mln']="million,million,million";
$lang['number_mld']="billion,billion,billion";
$lang['number_j']="one,two,three,four,five,six,seven,eight,nine,zero";
$lang['number_n']="ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen";
$lang['number_d']="twenty,thirty,forty,fifty,sixty,seventy,eighty,ninety";
$lang['number_s']="one hundred,two hundred,three hundred,four hundred,five hundred,six hundred,seven hundred,eight hundred,nine hundred";
//menu
$lang['menu_loadp']="Load a Product/Service from Database";
$lang['menu_loadc']="Load a Customer";
$lang['menu_save']="Save!";
$lang['menu_pdf']="Print PDF";
$lang['menu_clean']="New Invoice";
//panel wyboru kontrahenta
$lang['clients_head']="Customer List";
$lang['clients_name']="Full Name/Company Name";
$lang['clients_address']="Address";
$lang['clients_nip']="Tax ID #";
$lang['clients_search']="Search";
$lang['clients_all']="all";
$lang['clients_close']="Close";
//panel wyboru produktu
$lang['prod_head']="Product List";
$lang['prod_name']="Name";
$lang['prod_producer']="Manufacturer";
$lang['prod_cat']="Category";
$lang['prod_number']="Number";
$lang['prod_pkwiu']="Polish Goods Classification #";
$lang['prod_prize']="Price";
$lang['prod_search']="Search";
$lang['prod_all']="all";
$lang['prod_close']="Close";
//powroty
$lang['list_invoice']="Invoice List";
$lang['list_client']="Customer List";
$lang['add_client']="Save Customer";
$lang['add_product']="Save Product";

$lang['firma_name']="Name/Company:";
$lang['currency']="Select Currency";

//bledy

$lang['error1']="Log in and buy the subscription to use this function. Use the 30-day free trial period.";
//pomoc
$lang['pomoc1']="Did you know you can change names of all the headings, including the title: e.g. 'VAT Invoice' can be chcnged into any given name, e.g. 'Pro Forma Invoice' with just one click.";
$lang['pomoc2']="Please type correctly all the data in the respective fields. If the name is too long, use the ENTER key - it will create another line to enter some extra contents. If you are logged in, you can save a Customer or Product in the Database at any moment.";          
$lang['pomoc3']="Here you can enter the location(s) for the delivery of the Product sold. You can edit 'Delivery Address' into 'Service Address', if it is different than the Buyer's data.";
$lang['pomoc4']="Here you can calculate between Net and Gross values.
If you wish to calculate the Gross value, please enter data in the first line. 
If you wish to calculate the Net value, please enter data in the second line.";
$lang['pomoc5']="Mark 'periodical invoice' in case you want an invoice to be generated regularly for a Customer. More about 'periodical invoices' in the 'Invoice Options' Section.";
$lang['pomoc6']="Products/Services may be added to the list and accompanied by the price and VAT rate.
You may select the unit of Product/Service sold, you may also assign a percentage of discount.
You may add and/or delete any sold items";     
$lang['pomoc7']="The total of all amounts: Net, Gross, VAT.
Select the payment method and date. You can select the date using the calendar, having marked the calendar with a dot.
If an invoice is partially paid in cash and partially by transfer, please note that in the 'paid' field.";

$lang['menu1']='VAT Invoice';
$lang['menu2']='Pro Forma Invoice';
$lang['menu3']='Pre-Payment Invoice';
$lang['menu4']='Correction Invoice';
$lang['menu5']='Bill';
$lang['menu6']='Issue an Invoice';

$lang['navi1']='You are in ';

$lang['calc_nagl1']='Calculator:';
$lang['calc_nagl2']='Value';
$lang['calc_nagl3']='VAT';
$lang['calc_nagl4']='Result';
$lang['calc_nagl5']='Calculate';
$lang['calc_nagl6']='Net';
$lang['calc_nagl7']=' into Gross:';
$lang['calc_nagl8']='Gross';
$lang['calc_nagl9']=' into Net:';
$lang['calc_nagl0']='* this element will not be included in the invoice';
$lang['korekta1']='Add new Item to the Invoice ';
$lang['korekta2']='Correction';
$lang['korekta3']='Add new Item to the Correction ';

$lang['save_menu1'] = 'Original and Copy';
$lang['save_menu2'] = 'Original';
$lang['save_menu3'] = 'Copy';
$lang['save_menu4'] = 'Duplicate';
$lang['save_menu5'] = 'Print';
?>