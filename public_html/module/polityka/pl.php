<?php
$lang=array();
$lang['text']='
 <h1>Szanujemy prawo użytkowników do prywatności.</h1><br>
 Nie gromadzimy danych osobowych (za wyjątkiem danych teleadresowych firm, celem wystawienia faktur za abonament) oraz nie udostępniamy adresów e-mail osób korzystających z naszej aplikacji.<br><br>
 Nie rozsyłamy żadnych wiadomości o charakterze reklamowym, adresy e-mail wykorzystujemy wyłącznie w celu kontaktu technicznego.<br><br>
 Dane każdego Użytkownika, wprowadzane do aplikacji, stanowią jego własność.<br><br>
 Użytkownik ma dostęp do swoich danych, poprzez rejestrację w serwisie za pomocą wymyślonego przez siebie loginu i hasła. Nie mamy wglądu do danych Użytkownika, poza danymi teleadresowymi rejestrującej się firmy.<br>
 <br>
 <br>
 <b>Cookies</b><br>
<br>
Podmiotem zamieszczającym informacje w formie plików cookies (tzw. ciasteczek) w urządzeniu końcowym Użytkownika (np. komputerze, laptopie, tablecie, smartfonie itp.) oraz uzyskującym do nich dostęp jest E-GRUPA z siedzibą w Grodzisku Mazowieckim pełne dane kontaktowe na stronie www.egrupa.pl.
<br>
Zamieszczać informacje w urządzeniu końcowym użytkownika i korzystać z nich mogą również podmioty współpracujące z E-GRUPA, np świadczący usługi analityczne, twórcy aplikacji.<br>
<br>
Pliki cookies (tzw. „ciasteczka”) stanowią dane informatyczne, w szczególności pliki tekstowe, które przechowywane są w urządzeniu końcowym Użytkownika Serwisu i przeznaczone są do korzystania ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony internetowej, z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny numer. <br>

Pliki cookies nie służą identyfikacji użytkownika i na ich podstawie nie jest ustalana tożsamość użytkownika.<br>
<br>
Pliki cookies wykorzystywane są w celu: <br>
a)	dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych; w szczególności pliki te pozwalają rozpoznać urządzenie Użytkownika Serwisu i odpowiednio wyświetlić stronę internetową, dostosowaną do jego indywidualnych potrzeb;<br>
b)	tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają ze stron internetowych, co umożliwia ulepszanie ich struktury i zawartości;<br>
c)	utrzymanie sesji Użytkownika Serwisu (po zalogowaniu), dzięki której Użytkownik nie musi na każdej podstronie Serwisu ponownie wpisywać loginu i hasła;<br>
<br>
<br>
W ramach Serwisu stosowane są dwa zasadnicze rodzaje plików cookies: „sesyjne”  (session cookies) oraz „stałe” (persistent cookies). Cookies „sesyjne” są plikami tymczasowymi, które przechowywane są w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej lub wyłączenia oprogramowania (przeglądarki internetowej). „Stałe” pliki cookies przechowywane są w urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu ich usunięcia przez Użytkownika. <br>
<br>
W ramach Serwisu stosowane są następujące rodzaje plików cookies:<br>
a)	„niezbędne” pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach Serwisu, np. uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania w ramach Serwisu;<br>
b)	pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania w ramach Serwisu;<br>
c)	„wydajnościowe” pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych Serwisu;<br>
d)	„funkcjonalne” pliki cookies, umożliwiające „zapamiętanie” wybranych przez Użytkownika ustawień i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.; <br>
e)	„reklamowe” pliki cookies, umożliwiające dostarczanie Użytkownikom treści reklamowych bardziej dostosowanych do ich zainteresowań.<br> 
<br>
<br>
Inne technologie<br>
<br>
W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka internetowa) domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym Użytkownika. Użytkownicy Serwisu mogą dokonać w każdym czasie zmiany ustawień dotyczących plików cookies. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu w urządzeniu Użytkownika Serwisu. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej). <br>
Operator Serwisu informuje, że ograniczenia stosowania plików cookies mogą wpłynąć na niektóre funkcjonalności dostępne na stronach internetowych Serwisu.<br>
Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane mogą być również przez współpracujących z operatorem Serwisu reklamodawców oraz partnerów.<br>
<br> 
Więcej informacji na temat plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej). Szczegóły dla osób korzystających z poszczególnych przeglądarek internetowych:<br>
<br>
Internet Explorer - support.microsoft.com/kb/196955/pl<br>
Mozilla Firefox - support.mozilla.org/pl/products/firefox/cookies<br>
Chrome -  support.google.com/chrome/bin/answer.py?hl=pl&amp;answer=95647<br>
Opera - help.opera.com/Linux/9.22/pl/cookies.html<br>
Itp.<br>
<br>
Informujemy, że ograniczenia bądź wyłączenia stosowania plików cookies i innych podobnych technologii mogą wpłynąć na niektóre funkcjonalności dostępne w naszych serwisach.<br>
<br>
<br>
Informacje dodatkowe<br>
<br>
Dodatkowe informacje na temat plików cookies i innych technologii można znaleźć m.in. pod adresem wszystkoociasteczkach.pl, youronlinechoices.com lub w sekcji Pomoc w menu przeglądarki internetowej. ';


?>