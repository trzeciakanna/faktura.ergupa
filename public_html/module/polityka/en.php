<?php
$lang=array();
$lang['text']='<h2>
 <h2>Szanujemy prawo użytkowników do prywatności.</h2><br>
 Nie gromadzimy danych osobowych (za wyjątkiem danych teleadresowych firm, celem wystawienia faktur za abonament) oraz nie udostępniamy adresów e-mail osób korzystajacych z naszej aplikacji.<br><br>
 Nie rozsyłamy żadnych wiadomości o charakterze reklamowym, adresy e-mail wykorzystujemy wyłącznie w celu kontaktu technicznego.<br><br>
 Dane każdego Użytkownika, wprowadzane do aplikacji, stanowią jego własność.<br><br>
 Użytkownik ma dostęp do swoich danych, poprzez rejestrację w serwisie za pomocą wymyślonego przez siebie loginu i hasła. Nie mamy wglądu do danych Użytkownika, poza danymi teleadresowymi rejestrującej się firmy.<br>';


?>