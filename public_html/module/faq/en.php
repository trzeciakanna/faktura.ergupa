<?php
$lang=array();
$lang['text']='<span style="font-weight: bold;">What is Online Invoice?</span><br>
It is a program that you do not need to install on your computer.<br>
You can use it from any computer in the world.<br>
At any moment you can issue an invoice, view or edit saved invoices and save them to a PDF file.<br>
So if you travel a lot, make out your invoices in different places and want an instant look at all your invoices, this program is for you.<br>
There is no risk of losing an invoice.<br>
There is no risk of infecting your computer during installation.<br>
There is no risk of losing your lists of invoices in case your computer breaks.<br>
Invoicing is very easy and quick.<br>
    <br>
<span style="font-weight: bold;">Free Invoice:</span><br>
The Free Invoice allows to issue invoices any time, in unlimited numbers.<br>
The free invoices can be then saved on your local computer in .PDF.<br>
The data seen on an invoice will disappear on refreshing the website; they will also disappear after not using the site for more than a couple of minutes! <br>
This is to prevent your private data from being used by third parties.<br>
<br>
<span style="font-weight: bold;">What does the subscription give me ?</span><br>
- It allows you to save your invoices in .PDF on the secure online servers. <br>
- Any saved invoice can be edited, viewed and modified from any computer in any place in the world.<br>
- The invoice archive with an advanced search engine <br>
- You can save and edit the data of your company, which will make it easier and quicker to issue an invoice, because after logging in these data <br>
will automatically appear in the issuing form.<br>
- You can save and edit the data of your customers, which will make it easier to issue invoices to regular customers.<br>
- You can save and edit the product and its price. This will allow you to add value of sales or services quickly.<br>
- You can create Categories of offered products or services, which will speed up their searching.<br>
- You can send messages to all or selected customers.<br>
<br>
<span style="font-weight: bold;">Pricelist:</span><br>

1 month -  10 zł<br>
2 months - 19,50 zł<br>
3 months - 28,50 zł<br>
4 months - 37 zł<br>
5 months - 45 zł<br>
6 months - 52,50 zł<br>
7 months - 59,50 zł<br>
8 months - 66 zł<br>
9 months - 72 zł<br>
10 months - 77,50 zł<br>
11 months - 82,50 zł<br>
12 months - 87 zł<br>
<br>
<span style="font-weight: bold;">Will the invoices issued during the free period be available after buying the subscription?</span><br>
All the invoices saved on the server during the free period will be available after<br>
buying the subscription, and later, when extending it.<br>
<br>
<span style="font-weight: bold;">How can I buy the subscription? </span><br>
To buy the subscription, you have to log in. <br>
Upon logging in, you will receive a welcome email with a direct link to data edit screen <br>
which needs to be filled in.<br>
The first month of using is free.<br>
After one month, if you want to keep using the service, you must buy the subscription for 1 to 12 months.<br>
In order to do this, please log in, select the subscription period, the bank where you have your account and pay for the service.<br>
When the money is booked, it will be possible to use all the options.<br>';


?>