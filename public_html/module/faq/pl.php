<?php
$lang=array();
$lang['text']='<span style="font-weight: bold;">Co to jest Faktura VAT on-line?</span><br>
Jest to program, którego nie musisz instalować na swoim komputerze. <br>
Korzystać z niego możesz z każdego komputera w dowolnym miejscu na świecie.<br>
W każdej chwili możesz wystawić fakturę, podglądnąć lub z edytować już wystawione, zapisać do pdf.<br>
Więc jeżeli dużo podróżujesz, często wypisujesz faktury w różnych miejscach, chcesz mieć w każdej chwili podgląd do wszystkich faktur to ten program jest właśnie dla Ciebie.<br>
Nie ma ryzyka utraty faktur.<br>
Nie ma ryzyka zainfekowania wirusem komputera przy instalacji.<br>
Nie ma ryzyka, że jeżeli popsuje Ci się komputer utracisz całą listę faktur.<br>
Wystawianie faktury jest bardzo proste i szybkie.<br>
    <br>
<span style="font-weight: bold;">Faktura darmowa:</span><br>
Faktura darmowa pozwala na wystawianie faktur w każdej chwili i w nieograniczonej ilości.<br>
Darmowe faktury można zapisać w pdf na prywatnym komputerze.<br>
Dane widoczne na fakturze po odświeżeniu strony znikają jak również znikną po kilku minutach nieużywania programu ! <br>
Dzieję się tak, aby dane, które Państwo wpisują nie były  wykorzystywane przez osoby trzecie.<br>
<br>
<span style="font-weight: bold;">Co nam daje abonament ?</span><br>
- Abonament pozwala na zapisanie faktury w pdf oraz na zabezpieczonych serwerach on-line.<br>
- Zachowaną fakturę na serwerze można w każdej chwili edytować, zmieniać wybrane dane, podglądać <br>
z każdego komputera i z każdego miejsca na świecie.<br>
- Archiwum faktur z zaawansowaną wyszukiwarką <br>
- Możliwość zapisania i edytowania danych swojej firmy, co ułatwi i przyspieszy wystawianie faktury, ponieważ po zalogowaniu się <br>
automatycznie dane te pojawią się w formularzu wystawiania faktury.<br>
- Możliwość zapisania i edytowania  danych kontrahenta, co ułatwi wystawianie faktur stałym klientom.<br>
- Możliwość zapisania i edytowania produktu oraz jego ceny , pozwoli to w szybki sposób dodać wartość sprzedaży lub usługi<br>
- Możliwość utworzenia kategorii sprzedawanych produktów lub usług, przez co przyspieszy się ich wyszukiwanie.<br>
- Możliwość wysłania wiadomości do wszystkich jednocześnie lub wybranych kontrahentów.<br>
<br>
<span style="font-weight: bold;">Cennik abonamentu netto:</span><br>


3 miesiące - 28,50 zł<br>
4 miesiące - 37 zł<br>
5 miesięcy - 45 zł<br>
6 miesięcy - 52,50 zł<br>
7 miesięcy - 59,50 zł<br>
8 miesięcy - 66 zł<br>
9 miesięcy - 72 zł<br>
10 miesięcy - 77,50 zł<br>
11 miesięcy - 82,50 zł<br>
12 miesięcy - 87 zł<br>
<br>
<span style="font-weight: bold;">Czy faktury wystawione w okresie bezpłatnego korzystania z programu będą dostępne po wykupieniu abonamentu?</span><br>
Wszystkie faktury zachowane na serwerze w trakcie korzystania z okresu darmowego będą dostępne po <br>
wykupieniu abonamentu, jak również w późniejszym czasie przy przedłużaniu go.<br>
<br>
<span style="font-weight: bold;">Jak można wykupić abonament ? </span><br>
Aby wykupić abonament należy się zalogować. <br>
Po zalogowaniu na podaną skrzynkę pocztową przyjdzie powitanie oraz link bezpośrednio kierujący do edycji danych, <br>
które należy uzupełnić.<br>
Pierwszy miesiąc użytkowania jest darmowy.<br>
Po upływie miesiąca, aby korzystać należy wykupić abonament na okres od 1 do 12 miesięcy.<br>
W tym też celu należy być zalogowanym, wybrać okres abonamentu, następnie bank, w którym masz konto i opłacić usługę.<br>
Gdy pieniążki zostaną zaksięgowane możliwość korzystania ze wszystkich opcji będzie uruchomiona. <br>';


?>