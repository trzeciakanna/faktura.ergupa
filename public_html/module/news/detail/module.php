<?php
global $smarty;
require(ROOT_DIR."module/news/detail/".LANG.".php");
global $lang;
global $conf;
global $news;
global $navi;
$smarty->assign("navi",$navi->navi);
$smarty->assign("lang",$lang);

$dane=$news->read_one((int)$_GET['par3']);
$smarty->assign("dane",$dane);

$smarty->assign("prev",$news->prev_one($dane->date_add));
$smarty->assign("next",$news->next_one($dane->date_add));
$smarty->assign("max",count($dane->gallery));


$smarty->display("news/detail/".LAYOUT.".html");
?>