<?php
/**
 * 
 * @author kordan
 *
 */
class newses
{
	/**
	 * Odczyt pojedynczego newsa
	 * @param int $id identyfikator newsa
	 * @param boolean $edit czy edycja
	 * @return mixed obiekt newsa
	 */
	public function read_one($id, $edit=false)
	{
		global $DB;
		if(!$edit)
    {
      $DB->Execute("UPDATE `news` SET `count`=count+1 WHERE `id`='".$id."'");
      setcookie("news".$id,"1",time()+14*86400,"/");
    }
		return new news($id,null,null,null,null,null,null,null,"all");
	}
	/**
	 * Odczyt listy newsów
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $sort rodzaj sortowania
	 * @retrun array tablica danych
	 */
	public function read_list($page=1, $ile=20, $sort="dd")
	{
		global $DB;
		$dane=array();
		$page=(int)$page?$page:1;
		//sortowanie
		$sort=(substr($sort,0,1)=="d"?"`date_add`":"`title`")." ".(substr($sort,1,1)=="d"?"DESC":"ASC");
		//odczyt
		$r=$DB->PageExecute("SELECT * FROM `news` WHERE `ac`='1' ORDER BY ".$sort,$ile,$page);
		{
			while(!$r->EOF)
			{
				$rs=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `news_coment` WHERE `news_id`='".$r->fields['id']."'");
				$dane[]=new news($r->fields['id'],$r->fields['title'],$r->fields['text'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['date_add'],$r->fields['date_mod'],$r->fields['count'],"one",false,(int)$rs->fields['ilosc']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Odczyt poprzedniego newsa
	 * @param datetime $date_add data dodania
	 * @return mixed obiekt news
	 */
	public function prev_one($date_add)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id`,`title` FROM `news` WHERE `ac`='1' AND `date_add`>'".$date_add."' ORDER BY `date_add` ASC LIMIT 0,1");
		return new news($r->fields['id'],$r->fields['title']);
	}
	/**
	 * Odczyt nstepnego newsa
	 * @param datetime $date_add data dodania
	 * @return mixed obiekt news
	 */
	public function next_one($date_add)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id`,`title` FROM `news` WHERE `ac`='1' AND `date_add`<'".$date_add."' ORDER BY `date_add` DESC LIMIT 0,1");
		return new news($r->fields['id'],$r->fields['title']);
	}
	/**
	 * Dodanie komentarza
	 * @param int $id identyfikator newsa
	 * @param array $dane dane komentarza
	 */
	public function add_coment($id, $dane)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['parent']=$dane['coment_id'];
		$tmp['news_id']=$dane['news_id'];
		$tmp['user_id']=$id;
		$tmp['user_name']=$dane['sign'];
		$tmp['text']=$dane['text'];
		$tmp['text_s']=$search->parse($dane[''],1);
		$tmp['date_add']=date("Y-m-d H:i:s");
		$DB->AutoExecute("news_coment",$tmp,"INSERT");
	}
	/**
	 * Odczyt komentarzy
	 * @param int $id identyfikator newsa
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @return mixed tablica z danymi
	 */
	public function read_coment($id, $page=1, $ile=20)
	{
		global $DB;
		$dane=array();
		$page=(int)$page?$page:1;
		//odczyt
		$r=$DB->PageExecute("SELECT * FROM `news_coment` WHERE `ac`='1' AND `news_id`='".$id."' AND `parent`='0' ORDER BY `date_add` DESC",$ile,$page);
		{
			while(!$r->EOF)
			{
				$dane[]=new coment($r->fields['id'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['text'],$r->fields['date_add'],true,1);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Usunięcie komentarza
	 * @param int $id identyfikator komentarza
	 */
	public function del_coment($id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `news_coment` WHERE `parent`='".$id."'");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$this->del_coment($r->fields['id']);
				$r->MoveNext();
			}
		}
		$DB->Execute("DELETE FROM `news_coment` WHERE `id`='".$id."' LIMIT 1");
	}
}


/**
 * 
 * @author kordan
 *
 */
class news
{
	public $id;
	public $user_id;
	public $user_name;
	public $url;
	public $title;
	public $text;
	public $date_add;
	public $dane_mod;
	public $count;
	public $gallery;
	public $coment;
	public $count_c;
	/**
	 * Utworzenie obiektu newsa
	 * @param int $id identyfikator newsa
	 * @param string $title tytuł
	 * @param string $text treść
	 * @param int $user_id identyfikator autora
	 * @param string $user_name nazwa autora
	 * @param datetime $date_add data utworzenia
	 * @param datetim $date_mod data modyfikacji
	 * @param int $count licznik odwiedzin
	 * @param array $gallery tablica galerii
	 * @param array $coment tablica komentarzy
	 * @param array $count_c ilość komentarzy
	 */
	public function __construct($id, $title="", $text="", $user_id=0, $user_name="", $date_add=null, $date_mod=null, $count=0, $gallery=false, $coment=false, $count_c=0)
	{
		global $func;
		if($title)
		{
			$this->id=$id;
			$this->title=$func->show_with_html($title);
			$this->text=$func->show_with_html($text,false,false,true);
			$this->url=$func->create_url($this->title);
			$this->user_id=$user_id;
			$this->user_name=$user_name;
			$this->date_add=$date_add;
			$this->date_mod=$date_mod;
			$this->count=$count;
			$this->count_c=$count_c;
			if($gallery)
			{ $this->gallery=$this->read_gallery($id,$gallery); }
			if($coment)
			{ $this->coment=$this->read_coment($id,$coment); }
		}
		else
		{
			global $DB;
			$r=$DB->Execute("SELECT * FROM `news` WHERE `id`='".$id."' AND `ac`='1' LIMIT 0,1");
			$this->id=$id;
			$this->title=$func->show_with_html($r->fields['title']);
			$this->text=$func->show_with_html($r->fields['text'],false,false,true);
			$this->url=$func->create_url($this->title);
			$this->user_id=$r->fields['user_id'];
			$this->user_name=$r->fields['user_name'];
			$this->date_add=$r->fields['date_add'];
			$this->date_mod=$r->fields['date_mod'];
			$this->count=$r->fields['count'];
			$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `news_coment` WHERE `news_id`='".$id."'");
			$this->count_c=$r->fields['ilosc'];
			if($gallery)
			{ $this->gallery=$this->read_gallery($id,$gallery); }
			if($coment)
			{ $this->coment=$this->read_coment($id,$coment); }
		}
	}
	/**
	 * Odczyt galerii
	 * @param int $id identyfikator newsa
	 * @param string $type rodzaj odczytu
	 * @return array tablica obiektów galerii
	 */
	public function read_gallery($id, $type="all")
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT * FROM `news_gallery` WHERE `news_id`='".$id."' ORDER BY `count` ASC".($type=="one"?" LIMIT 0,1":""));
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$t=new news_galery($r->fields['id'],$r->fields['name'],$r->fields['path']);
				if($t->id) { $dane[]=$t; }
				$r->MoveNext();
			}
		}
		return $dane;
	}
}
/**
 * 
 * @author kordan
 *
 */
class news_galery
{
	public $id;
	public $name;
	public $path;
	public $width;
	public $height;
	/**
	 * Utworzenie obiektu galerii
	 * @param int $id identyfikator grafiki
	 * @param string $name nazwa grafiki
	 * @param string $path ścieżka do grafiki
	 */
	public function __construct($id, $name, $path)
	{
		if(is_file($path))	
		{
			$this->id=$id;
			$this->name=$name;
			$this->path=$path;
			list($this->width, $this->height)=getimagesize($path);
		}
	}
}
/**
 * 
 * @author kordan
 *
 */
class coment
{
	public $id;
	public $user_id;
	public $user_name;
	public $text;
	public $date_add;
	public $child;
	public $level;
	/**
	 * Utworzenie obiektu komentarza
	 * @param int $id identyfikator komentarza
	 * @param int $user_id identyfikator autora
	 * @param string $user_name nazwa autora
	 * @param string $text treść
	 * @param datetime $date_add data dodania
	 * @param boolean $child czy odczytywać dzieci
	 * @param int $level poziom zagnieżdżenia
	 */
	public function __construct($id, $user_id=0, $user_name="", $text="", $date_add=null, $child=false, $level=0)
	{
		global $func;
		if($text)
		{
			$this->id=$id;
			$this->user_id=$user_id;
			$this->user_name=$user_name;
			$this->text=$func->show_with_html($text,false,false,true);
			$this->date_add=$date_add;
			$this->level=$level;
			if($child) { $this->child=$this->read_child($id,$level+1); }
		}
		else
		{
			global $DB;
			$r=$DB->Execute("SELECT * FROM `news_coment` WHERE `id`='".$id."' AND `ac`='1' LIMIT 0,1");
			$this->id=$id;
			$this->user_id=$r->fields['user_id'];
			$this->user_name=$r->fields['user_name'];
			$this->text=$func->show_with_html($r->fields['text'],false,false,true);
			$this->date_add=$r->fields['date_add'];
			$this->level=0;
			if($child) { $this->child=$this->read_child($id,1); }
		}
	}
	/**
	 * Odczyt dzieci komentarza
	 * @param int $id identyfikator rodzica
	 * @param int $level poziom zagniezdżenia
	 * @return array tablica komentarzy
	 */
	private function read_child($id, $level=1)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT * FROM `news_coment` WHERE `parent`='".$id."' AND `ac`='1' ORDER BY `date_add` DESC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new coment($r->fields['id'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['text'],$r->fields['date_add'],true,$level);
				$r->MoveNext();
			}	
		}
		return $dane;
	}
}
?>