news_coment_list = {
	/**
	 * 
	 */
	tree : function(level, id)
	{
		//sprawdzenie co robic
		if($('tree_'+level+'_'+id).style.display=="none")
		{
			//pokazanie bloku
			$('tree_'+level+'_'+id).style.display="block";
			$('button_'+level+'_'+id).className="minus";
		}
		else
		{
			//ukrycie bloku
			$('tree_'+level+'_'+id).style.display="none";
			$('button_'+level+'_'+id).className="plus";	
		}
		return false;
	},
	/**
	 * 
	 */
	del : function(id)
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{ location.href=location.href; };
		req.AddParam("id",id);
		req.Send("module/news/coment_list/ajax_del.php");
		return false;
	},
	
	add_coment : function(id)
	{
		$('coment_id').value=id;
		$('add_c_'+id).appendChild($('news_coment_add'));
		$('news_coment_add').style.display="block";
		return false;
	}
};