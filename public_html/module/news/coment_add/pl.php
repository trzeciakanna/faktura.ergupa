<?php
$lang=array();
$lang['head']="Dodaj komentarz";
$lang['text']="Treść";
$lang['sign']="Podpis";
$lang['add']="Dodaj komentarz";
$lang['cancel']="Anuluj";
$lang['nouser']="Komentarze mogą dodawać tylko zalogowani użytkownicy";
$lang['empty_txt']="Podaj treść komentarza";
$lang['empty_sign']="Podaj podpis";
$lang['empty_captcha']="Przepisz kod obrazkowy"; 
$lang['send_errorc']="Błędny kod";
$lang['send_error']="Wystąpił błąd podczas dodawania komentarza, spróbuj ponownie za chwilę (Komentarz można dodać tylko raz na 10 min)";
$lang['send_ok']="Komentarz została dodany";
$lang['']="";
$lang['']="";
$lang['']="";
?>