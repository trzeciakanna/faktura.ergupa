news_coment_add = {
	/**
	 * Anulowanie dodawania komentarza
	 */	
	cancel : function()
	{
		location.href="news/"+$('news_url').value+"/"+$('news_id').value+"/";
	},
	/**
	 * Komentarz
	 */
	submit2 : function(next)
	{
		if(!next) { $('komunikat').innerHTML=""; }
		if($('captcha_valid'))
		{
			if($('captcha_valid').value!=1) { /*setTimeout("news_coment_add.submit(1);",100);*/ }
			else { this.submit_end(); }
		}
		else { this.submit_end(); }
	},
	/**
	 * Dodanie komentarza
	 */
	submit : function()
	{
		var msg="";
		if($('news_coment_text').value.length<1) { msg+=$('coment_empty_txt').value+"\n"; }
		if($('news_coment_sign').value.length<1) { msg+=$('coment_empty_sign').value+"\n"; }
		//if($('captcha_valid')) { if($('captcha_valid').value==0) { msg+=$('coment_empty_captcha').value+"\n"; } }
		if(msg)
		{ info(msg); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
        if(this.responseText=="captcha")
        {
          $('zapytanie_send_error').innerHTML=$('send_errorc').value;
  				$('zapytanie_send_error').style.display="block";  
        }
        else { location.href="news/"+$('news_url').value+"/"+$('news_id').value+"/"; }
      };
			req.SendForm("news_coment_form","module/news/coment_add/ajax_add.php?id="+$('user_id').value);
		}
	}
};