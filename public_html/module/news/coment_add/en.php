<?php
$lang=array();
$lang['head']="Add a Comment";
$lang['head']="Reply to a Comment";
$lang['text']="Content";
$lang['sign']="Signature";
$lang['add']="Add";
$lang['cancel']="Cancel";
$lang['nouser']="Only logged-in users may add comments";
$lang['empty_txt']="Input comment content";
$lang['empty_sign']="Signature";
$lang['empty_captcha']="Copy the code from the picture"; 
$lang['send_errorc']="Wrong code";
$lang['send_error']="An error occurred while adding a comment. Try again in a while (you can add a comment once every 10 minutes";
$lang['send_ok']="Comment has been added";
$lang['']="";
$lang['']="";
$lang['']="";
?>