<?php
$lang=array();
$lang['head']="News from the world of invoices";
$lang['date']="Date";
$lang['title']="Title";
$lang['more']="see more";
$lang['date_add']="Date of adding";
$lang['date_mod']="Date of modification";
$lang['author']="Author";
$lang['none']="No news";
$lang['comment']="comment";
$lang['pomoc_nagl']="Help Center:";
$lang['pomoc1_nagl']="Online Guide";
$lang['pomoc2_nagl']="Talk to a consultant";
$lang['pomoc3_nagl']="Contact Form - Ask Us a Question";
$lang['pomoc1_tresc']="Contains a detailed description of the application functions and the solutions to the most common problems";
$lang['pomoc2_tresc']="If you need help, call us!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="You can also ask a question by filling in our Contact Form";
$lang['wiecej']= "more";
$lang['do_pomocy'] = "Go to Help";
$lang['o_programie_nagl'] = "What is the online invoicing application?";
$lang['o_programie'] = "The online invoicing application allows you to create your own VAT invoice, proforma invoice or other, in a quick, simple and intuitive way, without having to register.<br> It has a lot of editing options for all the heading fields.<br> It is possible to save the created invoice in PDF format, or to print the invoice right after it is created.<br> Additionally, the service offers automatic calculation of net and gross values, with different VAT rates.<br>
Upon registration every member gains access to all options, such as: making lists of customers, adding products, automatic saving of your data, putting your logo onto your invoices, an archive of stored invoices and many more useful options...";
$lang['']="";
$lang['']="";

?>