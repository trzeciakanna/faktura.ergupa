<?php
$lang=array();
$lang['head']="Nowości ze świata faktur";
$lang['date']="Data";
$lang['title']="Tytuł";
$lang['more']="zobacz więcej";
$lang['date_add']="Data dodania";
$lang['date_mod']="Data modyfikacji";
$lang['author']="Autor";
$lang['none']="Brak wiadomości";
$lang['comment']="skomentuj";
$lang['pomoc_nagl']="Centrum pomocy:";
$lang['pomoc1_nagl']="Przewodnik on-line";
$lang['pomoc2_nagl']="Rozmowa z konsultantem";
$lang['pomoc3_nagl']="Formularz kontaktowy - zadaj pytanie";
$lang['pomoc1_tresc']="Zawiera szczegółowy opis funkcji aplikacji oraz rozwiązania najczęstrzych problemów";
$lang['pomoc2_tresc']="	Jeśłi potrzebujesz pomocy zadzwoń!<br><span class='bold'>+48 504 667 000  +48 601 686 414</span>";
$lang['pomoc3_tresc']="	Możesz również zadać pytanie wypełniając nasz formularz kontaktowy";
$lang['wiecej']= "więcej";
$lang['do_pomocy'] = "Przejdź do pomocy";
$lang['o_programie_nagl'] = "Co to jest program do faktur on-line?";
$lang['o_programie'] = "System wystawiania faktur on line bez potrzeby rejestracji pozwala w szybki, łatwy i intuicyjny sposób utworzyć własną fakturę vat, proformę, badz inną<br> Posiada bardzo szerokie możliwości edycyjne wszystkich pól nagłówkowych.<br> Możliwość zapisania stworzonej faktury w formacie PDF lub też wydrukowanie faktury zaraz po jej utworzeniu.<br> Dodatkowo serwis posiada automatyczne przeliczanie kwot netto oraz brutto z uwzględnieniem różnego podatku VAT.<br>
Po rejestracji każdy użytkownik otrzymuje dostęp do wszystkich możliwości, takich jak: tworzenie listy kontrahentów, dodawanie produktów, automatyczny zapis swoich danych, możliwość dodania logo na fakturze, archiwum zapisanych faktur, i wiele wiele innych przydatnych opcji...";
$lang['']="";
$lang['']="";

?>