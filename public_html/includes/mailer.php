<?php
class mailer
{
	/**
	 * obiekt mailera
	 * @var mixed
	 */
	private $mail;
	/**
	 * Konstruktor
	 */
	public function __construct($rep="")
	{
		include(ROOT_DIR."includes/phpmailer/class.phpmailer.php");
		$this->mail=new PHPMailer(false);
		//ustawienia
		$this->mail->CharSet="UTF-8";
		$this->mail->SetLanguage("pl",ROOT_DIR."includes/phpmailer/language/");
		$this->mail->IsHTML(true);
		//autoryzacja
		$this->mail->IsSMTP();
		$this->mail->Port=25;
		$this->mail->SMTPAuth=true;
		$this->mail->Host="mail.faktura.egrupa.pl";
		$this->mail->Username="mailer@faktura.egrupa.pl";
		$this->mail->Password="sendmail";
		$this->mail->From="mailer@faktura.egrupa.pl";
		if($rep)
		{
			$this->mail->AddReplyTo($rep, $rep);
		}
		/*$this->mail->Host="smtp.afaktura.pl";
		$this->mail->Username="mailer@afaktura.pl";
		$this->mail->Password="sendmail";
		$this->mail->From="mailer@afaktura.pl";*/
		$this->mail->FromName="Faktura";
	}
	/**
	 * Wysyłanie mail'a
	 * @param array $users tablica użytkowników
	 * @param string $key typ wiadomości
	 * @param array $param lista parametrów do wiadomości
	 * @param array $files tablica plików do załączenia
	 * @return boolean status wykonania wysyłania
	 */
	public function send($users, $key, $param, $files=array())
	{
		//dodanie plików
		if(is_array($files))
		{
			foreach($files as $item)
			{ $this->mail->AddAttachment($item['path'],$item['name']); }
		}
		//ustawienie tematu i treści
		list($this->mail->Subject,$this->mail->Body,$logo)=$this->parse($key, $param);
		//dodanie loga
		if($logo)
		{ $this->mail->AddEmbeddedImage(ROOT_DIR."images/logo.png","logo"); }
		if(is_array($users))
		{
			foreach($users as $item)
			{
				$this->mail->ClearAddresses();
				$this->mail->AddAddress($item['mail'],$item['name']);
				if(!$this->mail->Send()) { /*echo $this->mail->ErrorInfo;*/ return false; }
			}
		}
		return true;
	}
	/**
	 * Odczytanie wiadomości i podstawienie zmiennych
	 * @param string $key typ wiadomości
	 * @param array $param wartości parametrów do podstawienia
	 * @return array dane do mail'a
	 */
	public function parse($key, $param)
	{
		global $func;
		$param['page_url']=ROOT_URL;
		$param['page_name']=ROOT_URL;
		//odczytanie danych mail'a
		if(file_exists(ROOT_DIR."mail_txt/".$key."_".LANG.".html"))
		{ $file=fopen(ROOT_DIR."mail_txt/".$key."_".LANG.".html","rt"); }
		else 
		{ $file=fopen(ROOT_DIR."mail_txt/".$key."_pl.html","rt"); }
		$txt="";
		while(!feof($file))
		{ $txt.=fgets($file,1024); }
		fclose($file);
		$txt=str_replace("\n","",$txt);
		//logo
		$tmp=preg_match("/{\*logo start\*}(.*){\*logo end\*}/",$txt,$match);
		$logo=$match[1];
		//tytuł
		$tmp=preg_match("/{\*title start\*}(.*){\*title end\*}/",$txt,$match);
		$title=$match[1];
		//text
		$tmp=preg_match("/{\*text start\*}(.*){\*text end\*}/",$txt,$match);
		$text=$match[1];
		//podmiana parametrów
		if(is_array($param))
		{
			foreach($param as $key=>$value)
			{
				$title=str_replace("[".$key."]",strip_tags($value),$title);
				$text=str_replace("[".$key."]",$value,$text);
			}
		}
		return array($title,$func->show_with_html($text),$logo);
	}
	
}
?>