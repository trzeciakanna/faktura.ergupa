<?php
/**
 * podstawowe funkcje strony
 *
 */
class functions
{
  public static function trimNip($nip)
  {
    return str_replace(array("-", " ", ":", "_"), "", $nip);
  }

	public function checkNews()
	{
    global $DB;
    $r=$DB->Execute("SELECT id FROM news ORDER BY id DESC LIMIT 0,1");
    if(!$r->fields['id']) { return; }
    while(!$r->EOF)
    {
      if(!$_COOKIE['news'.$r->fields['id']])
      {
        if(!class_exists('news')) { require(ROOT_DIR."module/news/class.php"); }
        return new news($r->fields['id']);
      }
      $r->MoveNext();
    }
  }
	/**
	 * Zwraca logo strony
	 * @return string adres pliku loga
	 */
	public function logo()
	{
		return "images/layout/logo.jpg";
	}
	/**
	 * Oczyszczanie danych ze złośliwego kodu
	 * @param mixed $dane
	 * @return mixed oczyszczone dane
	 */
	public function block_code($dane)
	{
		if(is_array($dane))
		{
			foreach($dane as $kl=>$war)
			{
				if(is_array($war))
				{ $dane[$kl]=$this->block_code($dane[$kl]); }
				else
				{
					if(strpos($_SERVER['REQUEST_URI'],"admin")===false) { $dane[$kl]=addslashes(htmlspecialchars(strip_tags($war))); }
					else { $dane[$kl]=addslashes(htmlspecialchars($war)); }
				}
			}		
		}
		else
		{
			if(strpos($_SERVER['REQUEST_URI'],"admin")===false) { $dane=addslashes(htmlspecialchars(strip_tags($dane))); }
			else { $dane=addslashes(htmlspecialchars($dane)); }
		}
		return $dane;
	}
	/**
	 * Pokazuje kod html wyciagniety z bazy
	 * @param string $str tekst do przefiltrowania
	 * @param boolean $php czy pokazywać kod PHP
	 * @param boolean $js czy pokazywać kod JS
	 * @param boolean $nl czy użyc nl2br
	 * @return string przefiltrowany tekst
	 */
	public function show_with_html($str, $php=false, $js=false, $nl=false)
	{
		$str=stripslashes(htmlspecialchars_decode(stripslashes($str)));
		$str=str_replace("<br>","<br/>",$str);
		if(!$php)
		{
			$str=preg_replace("#<\?php(.*?)?>#si","",$str);
			$str=preg_replace("#<\?(.*?)?>#si","",$str);
		}
		if(!$js)
		{ $str=preg_replace("#<script(.*?)</script>#si","",$str); }
		if($nl) { $str=nl2br($str); }
		return $str;
	}
	/**
	 * Funkcja usuwa polskie znaki
	 * @param string $str tekst wejściowy
	 * @param boolean $space czy usuwać spacje
	 * @param boolean $lower czy zwrócić bez konwersji na małe litery 
	 * @return string zwraca przefiltrowany tekst wejściowy
	 */
	function create_url($str, $space=true, $normal=false)
	{
		$pl=array("ę","ó","ą","ś","ł","ż","ź","ć","ń","Ę","Ó","Ą","Ś","Ł","Ż","Ź","Ć","Ń","/","?","!","@","#","$","%","^","&","*","(",")","_","+","=","|",",","<",">",";",":","'","\"","\\","\n");
		if($normal==1) { $eng=array("e","o","a","s","l","z","z","c","n","E","O","A","S","L","Z","Z","C","N","-","","","","","","","","","","","","","","","","","","","","","","",""," "); }
		else { $eng=array("e","o","a","s","l","z","z","c","n","e","o","a","s","l","z","z","c","n","-","","","","","","","","","","","","","","","","","","","","","","",""," "); }
		$str=str_replace($pl,$eng,$str);
		$str=trim($str);
		if($space){ $str=str_replace(" ","-",$str); }
		do { $str=str_replace("--","-",$str); } while ($str!=str_replace("--","-",$str));
		$str=trim($str);
		$str=trim($str,"-");
		if($normal) { return $str; }
		else { return strtolower($str); }
	}
	/**
	 * Czas z dokładnością co do mikrosekundy
	 * @return float czas w sekundach
	 */
	public function microtime_get()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	/**
	 * Dodaje punk pomiaru czasu
	 */
	public function microtime_point()
	{ $_SESSION['microtime'][]=$this->microtime_get(); }
	/**
	 * Wyświetla wyniki punktów pomiaru czasu
	 */
	public function microtime_show()
	{
		$start=$_SESSION['microtime'][0];
		for($a=1,$max=sizeof($_SESSION['microtime']);$a<$max;$a++)
		{ echo ($_SESSION['microtime'][$a]-$start)."<br/>"; }
	}
	/**
	 * Pobiera adres IP uzytkownika
	 * @return string adres IP
	 */
	public function get_ip()
	{ return $_SERVER['HTTP_X_FORWARDED_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']; }
	/**
	 * Pobiera adres IP serwera proxy jeżeli istnieje
	 * @return string adres IP
	 */
	public function get_proxy()
	{ return $_SERVER['HTTP_X_FORWARDED_FOR']?$_SERVER['REMOTE_ADDR']:""; }
	/**
	 * Obcinanie tekstu do porządanej długości z pominięciem kodu html
	 * @param string $string tekst do obcięcia
	 * @param int $length długość tekstu wynikowego
	 * @param string $etc zakończenie obciętego tekstu
	 * @param boolean $break_words czy łamać słowa
	 * @param boolean $middle czy usuwać tekst ze środka
	 */
	public function string_cut($string, $length, $etc='...', $break_words=false, $middle=false)
	{
		$string=strip_tags($this->show_with_html($string));
		if ($length==0) { return ''; }
		if (strlen($string)>$length) 
		{
			$length-=min($length, strlen($etc));
			if (!$break_words && !$middle) { $string=preg_replace('/\s+?(\S+)?$/','',substr($string,0,$length+1)); }
			if(!$middle) { return substr($string,0,$length).$etc; } 
			else { return substr($string,0,$length/2).$etc.substr($string,-$length/2); }
		} 
		else { return $string; }
	}
	/**
	 * Odczyt metadanych
	 * @return array tablica metadanych
	 */
	public function meta_read()
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `keys`,`value` FROM `metatags`");
		while(!$r->EOF)
		{
			$tmp[$r->fields['keys']]=$this->show_with_html($r->fields['value']);
			$r->MoveNext();
		}
		return $tmp;
	}
	/**
	 * Uaktualnienie metadanych
	 * @param array $dane tablica z metadanymi
	 */
	public function meta_save($dane)
	{
		global $DB;
		foreach($dane as $key=>$value)
		{ $DB->Execute("UPDATE `metatags` SET `value`='".$value."' WHERE `keys`='".$key."'"); }
		return "ok";
	}
	/**
	 * Odczyt kontaktu
	 * @return array tablica kontaktu
	 */
	public function contact_read()
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `keys`,`value` FROM `contact`");
		while(!$r->EOF)
		{
			$tmp[$r->fields['keys']]=$this->show_with_html($r->fields['value']);
			$r->MoveNext();
		}
		return $tmp;
	}
	/**
	 * Uaktualnienie kontaktu
	 * @param array $dane tablica z danymi kontaktu
	 */
	public function contact_save($dane)
	{
		global $DB;
		foreach($dane as $key=>$value)
		{ $DB->Execute("UPDATE `contact` SET `value`='".$value."' WHERE `keys`='".$key."'"); }
		echo "ok";
	}
	/**
	 * Sprawdzenie adresu email czy jest poprawny
	 * @param string $mail adres emial
	 * @return boolean czy adres jest poprawny
	 */
	public function is_mail($email)
	{ return preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)?true:false; }

	public function read_config($file)
	{
		$conf=array();
		if(is_file(ROOT_DIR.$file))
		{
			$dane=XML_unserialize(file_get_contents(ROOT_DIR.$file));
			$conf=$dane['config'];			
		}
		return $conf;
	}
	/**
	 * Zamiana liczby na słowa
	 * @param float $number liczba do zamiany
	 * @return string liczba słownie
	 */
	public function number_to_words($number,$cur)
	{
		require(ROOT_DIR."includes/cash_words.php");
		/*global $liczby_waluta;
		global $liczby_tysiac;
		global $liczby_milion;
		global $liczby_miliard;
		global $liczby_j;
		global $liczby_n;
		global $liczby_d;
		global $liczby_s;*/
		$number=number_format($number,2,"."," ");
		$text="";
		//rozbicie na całkowitą i reszte
		$liczba=explode(".",$number);
		//utworzenie ciągu groszy
		if(strlen($liczba[1])==0) { $grosze=" 00/100"; }
		elseif(strlen($liczba[1])==1) { $grosze=" 0".$liczba[1]."/100"; }
		else { $grosze=" ".$liczba[1]."/100"; }
		//rozbicie liczby na trójki
		$trojki=explode(" ",$liczba[0]); $ilosc=sizeof($trojki);
		for($a=$ilosc;$a>=0;$a--)
		{
			$tmp="";
			$tmpl=$trojki[$a];
			if($trojki[$a]>=100) { $tmp.=" ".$liczby_s[floor($trojki[$a]/100)-1]; $trojki[$a]=$trojki[$a]%100; }
			if($trojki[$a]>=20) 
			{
				$tmp.=" ".$liczby_d[floor($trojki[$a]/10)-2]; $trojki[$a]=$trojki[$a]%10;
				$tmp.=" ".$liczby_j[$trojki[$a]-1];
			}
			elseif($trojki[$a]>=10) { $tmp.=" ".$liczby_n[$trojki[$a]-10]; }
			else { $tmp.=" ".$liczby_j[$trojki[$a]-1]; }
			
			if(($ilosc-$a)==2)
			{
				if($tmpl==1) { $tmp.=" ".$liczby_tysiac[0]; }
				elseif($tmpl%10>1 AND $tmpl%10<5 AND ($tmpl<11 OR $tmpl>20)) { $tmp.=" ".$liczby_tysiac[1]; }
				else { $tmp.=" ".$liczby_tysiac[2]; }
			}
			elseif(($ilosc-$a)==3)
			{
				if($tmpl==1) { $tmp.=" ".$liczby_milion[0]; }
				elseif($tmpl%10>1 AND $tmpl%10<5 AND ($tmpl<11 OR $tmpl>20)) { $tmp.=" ".$liczby_milion[1]; }
				else { $tmp.=" ".$liczby_milion[2]; }
			}
			elseif(($ilosc-$a)==4)
			{
				if($tmpl==1) { $tmp.=" ".$liczby_miliard[0]; }
				elseif($tmpl%10>1 AND $tmpl%10<5 AND ($tmpl<11 OR $tmpl>20)) { $tmp.=" ".$liczby_miliard[1]; }
				elseif($tmpl!=0) { $tmp.=" ".$liczby_miliard[2]; }
			}
			$text=$tmp.$text;
		}
		return $text.$cur.$grosze;
	}
	/**
	 * Statystyki
	 * @return array tablica statystyk
	 */	
	public function stats()
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `users` WHERE `rang`='user'");
		$tmp['users']=$r->fields['ilosc'];
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `users` WHERE `rang`='user' AND `date_add`>='".date("Y-m-01 00:00:00")."'");
		$tmp['users_month']=$r->fields['ilosc'];
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `users` WHERE `rang`='user' AND `date_activ`>='".date("Y-m-01 00:00:00")."'");
		$tmp['users_activ']=$r->fields['ilosc'];
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `users` WHERE `rang`='user' AND `sub_end`>='".date("Y-m-d H:i:s")."'");
		$tmp['users_pay']=$r->fields['ilosc'];
		$r=$DB->Execute("SELECT COUNT(*) AS `ilosc` FROM `invoice`");
		$tmp['invoice_base']=$r->fields['ilosc'];
		$r=$DB->Execute("SELECT SUM(cash) AS `ilosc` FROM `pay_sys_platnosci_pl` WHERE `user_id`!='1' AND `status`='99'");
		$tmp['invoice_pay']=$r->fields['ilosc']/100;
		$r=$DB->Execute("SELECT SUM(cash) AS `ilosc` FROM `pay_sys_platnosci_pl` WHERE `user_id`!='1' AND `status`='99' AND `date`>='".date("Y-m-01 00:00:00")."'");
		$tmp['invoice_paym']=$r->fields['ilosc']/100;
		//podliczenie faktur PDF
		$f=fopen(ROOT_DIR."files/count.txt","rt");
		$tmp['invoice_pdf']=fgets($f,1024);
		fclose($f);
		return $tmp;
	}
	/**
	 * Przeliczenie ilości faktur
	 */   	
	public function recount_pdf()
	{
    $count=0;
		$dir=ROOT_DIR."files/invoice_pdf/";
		$cat_l1=opendir($dir);
		while($file=readdir($cat_l1))
		{
			if(is_dir($dir.$file) AND $file!='.' AND $file!='..')
			{
				$dir_l2=$dir.$file."/";
				$cat_l2=opendir($dir_l2);
				while($file_2=readdir($cat_l2))
				{
					if(is_dir($dir_l2.$file_2) AND $file_2!='.' AND $file_2!='..')
					{
						$dir_l3=$dir_l2.$file_2."/";
						$cat_l3=opendir($dir_l3);
						while($file_3=readdir($cat_l3))
						{ $count+=is_file($dir_l3.$file_3)?1:0; }
						closedir($cat_l3);
					}
					elseif(is_file($dir_l2.$file_2)) { $count++; }
				}
				closedir($cat_l2);
			}
			elseif(is_file($dir.$file)) { $count++; }
		}
		closedir($cat_l1);
    $f=fopen(ROOT_DIR."files/count.txt","wt");
    fputs($f,$count);
    fclose($f);
  }
  
  static function trimNipJPK($nip)
	{
		$results = [];
		$tmp = preg_match_all('/[0-9]+/', $nip, $results);
		return implode($results[0]);
	}
  
  static function getNipEU($nip)
	{
		$results = [];
		$tmp = preg_match_all('/[A-Za-z]+/', $nip, $results);
		return implode($results[0]);
	}
	
}
?>