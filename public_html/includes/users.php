<?php
class users
{
	public $id;
	public $hash;
	public $login;
	public $mail;
	public $rang;
	public $sub_end;
	public $dane_add;
	public $ip_add;
	public $date_log;
	public $sub_time;
	public static $bnip=array("5911593591","7811724304","5941418384","8581771653","9291737281","8762007453","9451400516","7161886662","8440006867","7772749351","1132292419","8661128563","9531017127","1","1231097164","2910066244","5170276258","5242020753","5252351606","5272426885","5470053887","5591381113","5811903773","5841022480","5890015994","6291036223","6320101065","6442838186","6452378711","6570242022","6581660289","6651529696","6772343341","6842454845","6921555345","7010260172","7321969010","7431004963","7460004078","7611370563","7621892984","7661428922","7721300724","7772919617","7841896754","7971931420","7991450787","8133477389","8211989101","8212397288","8271847008","8391524034","8411698733","8431002870","8522432777","873 15 97 376","8731597376","9191748055","9372490548","9372659875","9561673267","9591232042","9720117350","brak","PL 6790124052","7721300724","4990278020","6581660289","7711008465","6291036223","9561673267","5170276258"   ,"PL 6790124052","6790124052","1231097164","6651529696","7991450787","5242020753","5272426885","6320101065","6772343341","7772919617","7010260172","9720117350","6570242022","5811903773","6842454845");
	public static $anip=array("5831949324","6111507248","6112714564");
  
  /**
	 * Konstruktor, utworzenie obiektu
	 * @param int $id identyfikator użytkownika
	 * @param string $hash zakodowane dane
	 * @param string $login nazwa/login użytkownika
	 * @param string $mail adres e-mail użytkownika
	 * @param string $rang ranga użytkownika
	 * @param datetime $sub_end data zakończenia się abonamentu
	 * @param datetime $date_add data rejestracji
	 * @param string $ip_add adres ip rejestracji
	 * @param datetime $date_log data ostatniego logowania
	 * @param int $sub_time długosć ostatniego abonamentu
	 */
	public function __construct($id=null, $hash=null, $login=null, $mail=null, $rang=null, $sub_end=null, $date_add=null, $ip_add=null, $date_log=null, $sub_time=null)
	{
		if($id)
		{
			$this->id=$id;
			$this->hash=$hash;
			$this->login=$login;
			$this->mail=$mail;
			$this->rang=$rang;
			$this->sub_end=$sub_end;
			$this->date_add=$date_add;
			$this->ip_add=$ip_add;
			$this->date_log=$date_log;
			$this->sub_time=$sub_time;
		}
	}
	/**
	 * Dodanie użytkownika
	 * @param string $login login użykownika
	 * @param string $pass hasło dostepowe
	 * @param string $mail adres e-mail
	 * @param string $rang ranga uzytkownika
	 * @param boolean $mailing czy wysyłać wiadomość e-mail do użytkownika
	 * @return string raport wykonania
	 */
	public function add($login, $pass, $mail, $rang, $mailing=true, $nip="", $ref=0)
	{
		global $DB;//$DB->debug=true;
		global $func;
		//if(in_array(str_replace("-","",$nip),self::$bnip)) { return "error"; }
    if(!self::checkNip($nip,0)) { return "errorNip"; }

		$r=$DB->Execute("SELECT `id` FROM `users` WHERE `login`='".$login."' LIMIT 0,1");
		if(!$r->fields['id'])
		{
			$r=$DB->Execute("SELECT `id` FROM `users` WHERE `mail`='".$mail."' LIMIT 0,1");
			if(!$r->fields['id'])
			{
        if($ref)
        {
          $x=$DB->Execute("SELECT `id` FROM `users` WHERE `hash` LIKE '".$ref."%' LIMIT 0,1");
          $id_ref=$x->fields['id'];
        }
				$tmp=array();
				$tmp['login']=$login;
				$tmp['pass']=SHA1("salt".$pass."code");
				$tmp['mail']=$mail;
				$tmp['rang']=$rang; 
				$tmp['id_ref']=(int)$id_ref; 
				$tmp['cash']=0; 
				$tmp['ip_add']=$func->get_ip();
				$tmp['date_add']=date("Y-m-d H:i:s");
				$tmp['sub_hash']="auto add";
				$tmp['sub_end']=date("Y-m-d H:i:s",time()+86400*30);
				$tmp['hash']=SHA1($mail.$login.$rang);
				$DB->AutoExecute("users",$tmp,"INSERT");
				if($mailing)
				{
					require(ROOT_DIR."includes/mailer.php");
					$mailer=new mailer();
					$user=array();
					$user[]=array("mail"=>$mail,"name"=>$login);
					$x=array();
					$x['login']=$login;
					$x['mail']=$mail;
					$x['pass']=$pass;
					$x['ip']=$tmp['ip_add'];
					$x['date']=$tmp['date_add'];
					$result=$mailer->send($user,"users_add",$x,null);
					if(!$result)
					{
						$DB->Execute("DELETE FROM `users` WHERE `id`='".$DB->Insert_ID()."' LIMIT 1");
						return "error";
					}
				}
				$r=$DB->Execute("SELECT `id` FROM `users` WHERE `login`='".$login."' LIMIT 0,1");
				if($r->fields['id'])
				{
				  $tmp=array();
          $tmp['user_id']=$r->fields['id'];
				  $tmp['nip']=$nip;
		      $tmp['nipc']=functions::trimNip($tmp['nip']);
				  $DB->AutoExecute("users_data",$tmp,"INSERT");
          return "ok";
        }
        else { return "error"; }
			}
			else { return "error_m"; }
		}
		else { return "error_l"; }
	}
	/**
	 * Uaktualnienie użytkownika
	 * @param int $id identyfikator użytkownika
	 * @param string $login login użykownika
	 * @param string $pass hasło dostepowe
	 * @param string $mail adres e-mail
	 * @param string $rang ranga uzytkownika
	 * @return string raport wykonania
	 */
	public function save($id ,$login, $pass, $mail, $rang=null)
	{
		global $DB;
		global $func;
		$r=$DB->Execute("SELECT `id` FROM `users` WHERE `login`='".$login."' AND `id`!='".$id."' LIMIT 0,1");
		if(!$r->fields['id'])
		{
			$r=$DB->Execute("SELECT `id` FROM `users` WHERE `mail`='".$mail."' AND `id`!='".$id."' LIMIT 0,1");
			if(!$r->fields['id'])
			{
				$tmp=array();
				$tmp['login']=$login;
				if($pass) { $tmp['pass']=SHA1("salt".$pass."code"); }
				$tmp['mail']=$mail;
				if($rang) { $tmp['rang']=$rang; }
				$tmp['hash']=SHA1($mail.$login.$rang);
				$DB->AutoExecute("users",$tmp,"UPDATE","id='".$id."'");
				return "ok";
			}
			else { return "error_m"; }
		}
		else { return "error_l"; }
	}
	/**
	 * Usunięcie użytkownika
	 * @param int $id identyfikator użytkownika
	 */
	public function del($id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `users` WHERE `id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `users_data` WHERE `user_id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `users_extra` WHERE `user_id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `clients` WHERE `user_id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `product` WHERE `user_id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `product_cat` WHERE `user_id`='".$id."' LIMIT 1");
		//faktury
		$ids=array();
		$r=$DB->Execute("SELECT `id` FROM `invoice` WHERE `user_id`='".$id."'");
		if($r->fields['id'])
		{
			while(!$r->EOF) { $ids[]=$r->fields['id']; $r->MoveNext(); }
		}
		$DB->Execute("DELETE FROM `invoice_data` WHERE `invoice_id` in (".implode(",",$ids).") LIMIT 1");
		$DB->Execute("DELETE FROM `invoice_product` WHERE `invoice_id` in (".implode(",",$ids).") LIMIT 1");
		$DB->Execute("DELETE FROM `invoice` WHERE `user_id`='".$id."' LIMIT 1");
		$DB->Execute("OPTIMIZE TABLE `users`,`users_data`,`users_extra`,`clients`,`product`,`product_cat`,`invoice_data`,`invoice_product`,`invoice`");
	}
	/**
	 * Uaktualnienie licencji
	 * @param int $user_id identyfikator użytkownika
	 * @param datetime $date data zakończenia
	 * @param string $hash identyfikator transakcji
	 */
	public function new_lic($user_id, $date, $hash)
	{
		global $DB;
		$tmp=array();
		$tmp['sub_end']=$date;
		$tmp['sub_hash']=$hash; 
		$tmp['sub_send']=0;
		$DB->AutoExecute("users",$tmp,"UPDATE","id='".$user_id."'");
		//sprawdzenie czy jest referer
		$r=$DB->Execute("SELECT `id_ref` FROM `users` WHERE `id`='".$user_id."' LIMIT 0,1");
		if($r->fields['id_ref'])
		{
		  //sprawdzenie czy pierwszy abonament
		  $x=$DB->Execute("SELECT COUNT(*) AS `ile`, SUM(`cash`) AS `cash`, SUM(`ref`) AS `ref` FROM `pay_sys_platnosci_pl` WHERE `user_id`='".$user_id."' AND `status`='99'");
		  if($x->fields['ile']==1 AND $x->fields['ref']==0)
		  {
        $DB->Execute("UPDATE `users` SET `cash`=cash+".($x->fields['cash']*0.002)." WHERE `id`='".$r->fields['id_ref']."'");
        $DB->Execute("UPDATE `pay_sys_platnosci_pl` SET `ref`='1' WHERE `user_id`='".$user_id."' AND `status`='99'");
      }
    }
	}
	/**
	 * Odczyt pojedynczego użytkownika
	 * @param $id identyfikator użytkownika
	 * @return mixed obiekt użytkownika
	 */
	public function read_one($id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `hash`,`login`,`mail`,`rang`,`sub_end`,`date_log` FROM `users` WHERE `id`='".(int)$id."' LIMIT 0,1");
		$u=new users((int)$id, $r->fields['hash'], $r->fields['login'], $r->fields['mail'], $r->fields['rang'], $r->fields['sub_end'], null, null, $r->fields['date_log']);
		if($r->fields['hash']) { $DB->Execute("UPDATE `users` SET `date_activ`='".date("Y-m-d H:i:s")."' WHERE `id`='".$id."'"); }
		return $u;
	}
	/**
	 * Odczyt pojedynczego użytkownika po hashu
	 * @param $hash identyfikator użytkownika
	 * @return mixed obiekt użytkownika
	 */
	public function read_one_by_hash($hash)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id`,`login`,`mail`,`rang`,`sub_end`,`date_log` FROM `users` WHERE `hash`='".$hash."' AND `rang`!='ban' LIMIT 0,1");
		$u=new users($r->fields['id'], $hash, $r->fields['login'], $r->fields['mail'], $r->fields['rang'], $r->fields['sub_end'], null, null, $r->fields['date_log']);
		if($r->fields['id']) { $DB->Execute("UPDATE `users` SET `date_activ`='".date("Y-m-d H:i:s")."' WHERE `hash`='".$hash."'"); }
		return $u;
	}
	/**
	 * Logowanie
	 * @param $login login użytkownika
	 * @param $pass hasło użytkownika
	 * @return mixed obiekt użytkownika
	 */
	public function login($login, $pass, $remember)
	{
		global $DB;
		global $func;
		$r=$DB->Execute("SELECT `id`,`hash`,`login`,`mail` FROM `users` WHERE `login`='".$login."' AND `pass`='".SHA1("salt".$pass."code")."' AND `rang`!='ban' LIMIT 0,1");
		$DB->Execute("UPDATE `users` SET `date_log`='".date("Y-m-d H:i:s")."', `ip_log`='".$func->get_ip()."' WHERE `id`='".$r->fields['id']."'");
		$u=new users($r->fields['id'], $r->fields['hash'], $r->fields['login'], $r->fields['mail']);
		if($remember AND $u->id)
		{ setcookie("user_id",$u->hash,time()+15*86400,"/"); }
		return $u;
	}
	/**
	 * Odczyt listy użytkowników
	 * @param string $rank ranga użytkownika
	 * @param int $page numer strony
	 * @param int $number ilość wyników na stronie
	 * @param string $letter pierwsza litera loginu
	 * @param int $pay jakie wyszukiwanie abonamentu
	 * @return array tablica danych
	 */
	public function read_list($rank, $page, $number, $letter="", $pay=0, $sort="na")
	{
		global $DB;
		$tmp=array();
		if(strlen($letter)>1)
		{
		  $uid=array();
      $letter=functions::trimNip($letter);
      $r=$DB->Execute("SELECT `user_id` FROM `users_data` WHERE `nipc` LIKE '%".$letter."%'");
      if($r->fields['user_id'])
      {
        while(!$r->EOF)
        {
          $uid[]=$r->fields['user_id'];
          $r->Movenext();
        }
      }
      $uid=count($uid)?" OR `id` in (".implode(",",$uid).")":"";
    }
		
		$page=(int)$page>0?(int)$page:1;
		$war=$rank?"WHERE `rank`='".$rank."'":"";
		$warl=$letter?($war?" AND ":"WHERE ")."(`login` LIKE '".(strlen($letter)==1?$letter:"%".$letter)."%' OR `mail` LIKE '".(strlen($letter)==1?$letter:"%".$letter)."%'".$uid.")":"";
		$warp=$pay?($war.$warl?" AND":"WHERE ")."`sub_end` ".($pay==1?">=":"<")." NOW()":"";
		//odczyt raportów
		$raport=array();
		$r=$DB->Execute("SELECT `user_id`,`month` FROM `pay_sys_platnosci_pl` WHERE `status`='99' ORDER BY `date` ASC");
		if($r->fields['user_id'])
		{
			while(!$r->EOF)
			{
				$raport[$r->fields['user_id']]=$r->fields['month'];
				$r->MoveNext();
			}
		}
		//sortowanie
		if(substr($sort,0,1)=="c") { $sorts="date_add"; }
		elseif(substr($sort,0,1)=="a") { $sorts="sub_end"; }
		else { $sorts="login"; }
		$sorts.=substr($sort,1,1)=="a"?" ASC":" DESC";
		$r=$DB->PageExecute("SELECT `id`,`hash`,`login`,`mail`,`date_add`,`ip_add`,`rang`,`sub_end`, (sub_end>NOW()) AS `ab` FROM `users` ".$war.$warl.$warp." ORDER BY ".$sorts,$number,$page);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp[]=new users($r->fields['id'], $r->fields['hash'], $r->fields['login'], $r->fields['mail'], $r->fields['rang'], $r->fields['sub_end'], $r->fields['date_add'], $r->fields['ip_add'], null, $raport[$r->fields['id']]);
				$r->MoveNext();
			}
		}
		return array($tmp,$page,$r->_lastPageNo);
	}
	/**
	 * Odczyt listy użytkowników
	 * @param string $rank ranga użytkownika
	 * @return array tablica obiektów użytkowników
	 */
	public function read_all($rank)
	{
		global $DB;
		$tmp=array();
		$war=$rank?"WHERE `rank`='".$rank."'":"";
		$r=$DB->Execute("SELECT `id`,`hash`,`login`,`mail` FROM `users` ".$war);
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp[$r->fields['id']]=new users($r->fields['id'], $r->fields['hash'], $r->fields['login'], $r->fields['mail']);
				$r->MoveNext();
			}
		}
		return $tmp;
	}
	/**
	 * Sprawdzenie czy login jest zajety
	 * @param string $login login do sprawdzenia
	 * @return string wynik operacji sprawdzania
	 */
	public function check_login($login)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `users` WHERE `login`='".$login."' LIMIT 0,1");
		return !$r->fields['id']?"ok":"error";
	}
	/**
	 * Sprawdzenie czy adres e-mail jest zajety
	 * @param string $mail adres e-mail do sprawdzenia
	 * @return string wynik operacji sprawdzania
	 */
	public function check_mail($mail)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `users` WHERE `mail`='".$mail."' LIMIT 0,1");
		return !$r->fields['id']?"ok":"error";
	}
	/**
	 * Litery od których zaczynają się loginy użytkowników
	 * @return array tablica liter
	 */
	public function letter_list()
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(login),1,1) as `letter` FROM `users` GROUP BY SUBSTRING(login,1,1)");
		if($r->fields['letter'] OR $r->fields['letter']==="0")
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Dodanie wiadomości do wysłania
	 * @param string $topic tytuł wiadomości
	 * @param string $text treść wiadomości
	 * @param array $clients lista klientów
	 * @return string wynik operacji
	 */
	public function send_msg($topic, $text, $clients)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['topic']=$topic;
		$tmp['text']=$text;
		$tmp['topic_s']=$search->parse($topic,1);
		$tmp['text_s']=$search->parse($text,1);
		$tmp['date']=date("Y-m-d H:i:s");
		$DB->AutoExecute("newsletter_msg",$tmp,"INSERT");
		$id=$DB->Insert_ID();
		foreach($clients as $key=>$val) { $clients[$key]=array($val); } 
		$DB->Execute("INSERT INTO `newsletter_send` (msg_id, user_id) VALUES ('".$id."',?)",$clients);
		return "ok";
	}
	/**
	 * Lista wysłanych wiadomości
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $letter pierwsza litera tematu
	 * @return array tablica z danymi
	 */
	public function send_list($page=1, $ile=20, $letter="")
	{
		global $DB;
		$dane=array();
		$page=(int)$page?(int)$page:1;
		$war=$letter?"WHERE `topic` LIKE '".$letter."%'":"";
		$r=$DB->PageExecute("SELECT `id`,`topic`,`date` FROM `newsletter_msg` ".$war." ORDER BY `date` DESC",$ile,$page);
		if($r->fields['topic'])
		{
			while(!$r->EOF)
			{
				$dane[]=new newsletter_msg($r->fields['id'],$r->fields['topic'],$r->fields['date']);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Litery od których zaczynają się tytuły wiadomości
	 * @return array tablica liter
	 */
	public function send_letter()
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(topic),1,1) as `letter` FROM `newsletter_msg` GROUP BY SUBSTRING(topic,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	/**
	 * Odczyt wiadomości
	 * @param int $id identyfikator wiadomości
	 * @return array tablica z obiektem wiadomości i kontrahentami
	 */
	public function send_one($id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `topic`,`text`,`date` FROM `newsletter_msg` WHERE `id`='".(int)$id."' LIMIT 0,1");
		$msg=new newsletter_msg($id, $r->fields['topic'], $r->fields['date'], $r->fields['text']);
		$client=array();
		$r=$DB->Execute("SELECT users.id, users.login FROM users INNER JOIN newsletter_send ON users.id=newsletter_send.user_id WHERE newsletter_send.msg_id='".(int)$id."' ORDER BY users.login ASC");
		while(!$r->EOF)
		{
			$client[]=array("id"=>$r->fields['id'],"name"=>$r->fields['login']);
			$r->MoveNext();
		}
		return array($msg, $client);
	}
	/**
	 * Wysłanie nowego hasła
	 * @param string $login login
	 * @param string $mail adres e-mail
	 * @return string wynik wykonania
	 */
	public function lost_pass($login, $mail)
	{
		global $DB;
		global $func;
		$r=$DB->Execute("SELECT `id` FROM `users` WHERE `login`='".$login."' AND `mail`='".$mail."' LIMIT 0,1");
		if($r->fields['id'])
		{
			require(ROOT_DIR."includes/mailer.php");
			$mailer=new mailer();
			$user=array();
			$user[]=array("mail"=>$mail,"name"=>$login);
			$x=array();
			$pass=$this->gen_pass(8);
			$x['login']=$login;
			$x['pass']=$pass;
			$result=$mailer->send($user,"lost_pass",$x,null);
			if(!$result)
			{ return "send"; }
			else
			{
				$DB->Execute("UPDATE `users` SET `pass`='".SHA1("salt".$pass."code")."' WHERE `id`='".$r->fields['id']."'");
				return "ok";
			}
		}
		else { return "error"; }
	}
	/**
	 * Generowanie hasła
	 * @param int $len długość hasła
	 * @return string hasło
	 */
	private function gen_pass($len=8)
	{
		$alpha="abcdefghijkmnopqrstuvwxyz023456789+*-$@#";
		$max=strlen($alpha);
		$pass="";
		for($a=0;$a<$len;$a++)
		{ $pass.=substr($alpha,rand(0,$max-1),1); }
		return $pass;
	}
	/**
	 * Odczyt zakończonych płatności
	 * @param int $user_id identyfikator użytkownika
	 * @return array lista płatności
	 */
	public function read_end_pay($user_id)
	{
		global $DB;
		$data=array();
		$r=$DB->Execute("SELECT `id`,`month`,`vat`,`date` FROM `pay_sys_platnosci_pl` WHERE `user_id`='".$user_id."' AND `status`='99' ORDER BY `date` DESC");
		if($r->fields['month'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				$tmp['id']=$r->fields['id'];
				$tmp['month']=$r->fields['month'];
				$tmp['inv']=$r->fields['vat'];
				$tmp['date']=$r->fields['date'];
				$data[]=$tmp;
				$r->MoveNext();
			}
		}
		return $data;
	}
	/**
	 * Utworzenie faktury do wystawienia
	 * @param int $id identyfikator płatności
	 * @param int $user_id identyfikator użytkownika
	 */
	public function pay_invoice($id, $user_id)
	{
		global $DB;
		//utworzenie obiektu faktury
		/*require(ROOT_DIR."module/panel/invoice/class.php");
		$inv=new invoice();
		$count=$inv->counter(1);
		//odczyt danych firmy i klienta
		$u=new users_invoice(1);
		$c=new users_invoice($user_id);
		//odczyt danych do faktury
		$p=array();
		$r=$DB->Execute("SELECT `month`,`cash`,`date`,`vat` FROM `pay_sys_platnosci_pl` WHERE `user_id`='".$user_id."' AND `status`='99' AND `id`='".$id."' LIMIT 0,1");
		if(!$r->fields['month']) { return "error"; }
		else if($r->fields['vat']!="no") { return "exist"; }
		$p['month']=$r->fields['month'];
		$p['date']=$r->fields['date'];
		$p['cash']=$r->fields['cash']/100;
		//utworzenie danych faktury - dane podstawowe
		$data=array();
		$data['client_id']=0;
		$data['number']=$count."/".date("m/Y");
		$data['original_v']=1;
		$data['place']="Warszawa";
		$data['date_sell']=$p['date'];
		$data['cash_type']="zapłacono";
		$data['cash_date']="list";
		$data['cash_date_list']="zapłacono";
		$data['cash_sum']=$p['cash'];
		$data['date_create']=date("Y-m-d H:i:s");
		$data['pay']=$id;
		$data['current']="PLN";
		//utworzenie danych faktury - plik językowy
		include ROOT_DIR."module/invoice/".LANG.".php";
		foreach($lang as $key => $var)
		{ $data['lang_'.$key]=$var; }
		//utworzenie danych faktury - dane sprzedawcy
		$data["user_name"]=$u->name;
		$data["user_address"]=$u->address;
		$data["user_nip"]=$u->nip;
		$data["user_phone"]=$u->phone;
		$data["user_mail"]=$u->email;
		$data["user_krs"]=$u->krs;
		$data["user_bank_name"]=$u->bank_name;
		$data["user_account"]=$u->account;
		//utworzenie danych faktury - dane klienta
		$data["client_name"]=$c->name;
		$data["client_address"]=$c->address;
		$data["client_nip"]=$c->nip;
		$data["client_phone"]=$c->phone;
		$data["client_mail"]=$c->email;
		$data["client_krs"]=$c->krs;
		$data["client_bank_name"]=$c->bank_name;
		$data["client_account"]=$c->account;
		//dodanie produktu
		$data["p_1_name"]="Abonament za ".$p['month']." miesięcy";
		$data["p_1_pkwiu"]="";
		$data["p_1_unit"]="szt.";
		$data["p_1_vat"]=22;
		$data["p_1_netto"]=($p['month']*10)/1.22;
		$data["p_1_rabat"]=(($p['month']-1)*2.5);
		$data["p_1_amount"]=1;
		//dodanie faktury
		$inv->add($user_id,$data,1); */
		$DB->Execute("UPDATE `pay_sys_platnosci_pl` SET `vat`='yes' WHERE `id`='".$id."'");
		return "ok";
	}
	/**
	 * Zakończenie faktury za abonament
	 * @param int $id identyfikator użytkownika
	 */
	public function pay_end($id)
	{
		global $DB;
		$DB->Execute("UPDATE `pay_sys_platnosci_pl` SET `vat`='end' WHERE `id`='".$id."'");
	}
	
	public function checkNip($nip, $id=0)
	{
    //$nip=str_replace(array(".","-"," ","_"),array("","","",""),$nip);
    $nip=functions::trimNip($nip);
    $war=array();
    $war[]="nipc='".$nip."'";
    if($id) { $war[]="`user_id`!='".$id."'"; }
    global $DB;
    $r=$DB->Execute("SELECT 1 AS `res` FROM `users_data` WHERE ".implode(" AND ",$war)." LIMIT 1");
    return $r->fields['res']?false:true;
  }
  
  public function read_notes($user_id)
	{
		global $DB;
		$data=array();
		$r=$DB->Execute("SELECT * FROM `users_notes` WHERE `user_id`='".$user_id."' ORDER BY `dateCreate` DESC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				$tmp['id']=$r->fields['id'];
				$tmp['body']=$r->fields['body'];
				$tmp['dateCreate']=$r->fields['dateCreate'];
				$tmp['ip']=$r->fields['ip'];
				$data[]=$tmp;
				$r->MoveNext();
			}
		}
		return $data;
	}
  
  public function add_notes($user_id, $body)
  {
    if(!$body)
    {
      return "errBody";
    }
    global $DB;
    global $func;
    $tmp=array();
    $tmp['user_id']=$user_id;
	  $tmp['body']=$body;
    $tmp['dateCreate']=date("Y-m-d H:i:s");
    $tmp['ip'] = $func->get_ip();
	  return $DB->AutoExecute("users_notes",$tmp,"INSERT") ? "ok" : "errDb";
  }
  
  public function delete_notes($id)
  {
    global $DB;
    $DB->Execute("DELETE FROM `users_notes` WHERE `id` = '" . $id . "'");
    return "ok";
  }
}
/**
 * 
 * @author kordan
 *
 */
class users_invoice extends users
{
	public $name;
	public $address;
	public $nip;
	public $phone;
	public $email;
	public $bank_name;
	public $krs;
	public $account;
	public $place;
	public $www;
	/**
	 * Odczyt pełnych danych użytkownika
	 * @param int $id identyfikator użytkownika
	 */
	public function __construct($id=null)
	{
		if($id)
		{
			global $DB;
			global $func;
			$r=$DB->Execute("SELECT `hash`,`login`,`mail`,`rang`,`sub_end` FROM `users` WHERE `id`='".(int)$id."' LIMIT 0,1");
			$this->id=$id;
			$this->hash=$r->fields['hash'];
			$this->login=$r->fields['login'];
			$this->mail=$r->fields['mail'];
			$this->rang=$r->fields['rang'];
			$this->sub_end=$r->fields['sub_end'];
			$r=$DB->Execute("SELECT * FROM `users_data` WHERE `user_id`='".(int)$id."' LIMIT 0,1");
			$this->name=$func->show_with_html($r->fields['name']);
			$this->address=$func->show_with_html($r->fields['address']);
			$this->nip=$r->fields['nip'];
			$this->phone=$func->show_with_html($r->fields['phone']);
			$this->email=$func->show_with_html($r->fields['email']);
			$this->bank_name=$r->fields['bank_name'];
			$this->krs=$r->fields['krs'];
			$this->place=$r->fields['place']; 
			$this->www=$r->fields['www'];
			if(!$r->fields['id']) { $DB->Execute("INSERT INTO `users_data` SET `user_id`='".$id."'"); }
			//odczyt kont
			$this->account=array();
			$r=$DB->Execute("SELECT `id`,`account`,`name` FROM `users_account` WHERE `user_id`='".$id."' ORDER BY `id` ASC");
			if($r->fields['id'])
			{
        while(!$r->EOF)
        {
          $this->account[$r->fields['id']]=array($r->fields['account'],$r->fields['name']);
          $r->MoveNext();
        }
      }
		}
	}
	/**
	 * Dodanie pełnych danych użytkownika
	 * @param array $post dane do dodania
	 * @return string wynik dodania
	 */
	public function add_max($post)
	{
		global $DB;
		$r=$this->add($post['login'], $post['pass'], $post['mail'], $post['rang'], false);
		if($r=="ok")
		{
			$id=$DB->Insert_ID();
			$tmp=array();
			$tmp['user_id']=$id;
			$tmp['name']=$post['name'];
			$tmp['address']=$post['address'];
			$tmp['nip']=$post['nip'];
		  $tmp['nipc']=functions::trimNip($post['nip']);
			$tmp['phone']=$post['phone'];
			$tmp['email']=$post['email'];
			$tmp['bank_name']=$post['bank_name'];
			$tmp['krs']=$post['krs'];    
			$tmp['www']=$post['www'];
			//$tmp['account']=$post['account'];
			$tmp['place']=$post['place'];
			$DB->AutoExecute("users_data",$tmp,"INSERT");
			return "ok";
		}
		else
		{ return $r; }
	}
	/**
	 * Zapis pełnych danych użytkownika
	 * @param int $id identyfikator użytkownika
	 * @param array $post dane do zapisu
	 * @return string wynik zapisu
	 */
	public function save_max($id, $post, $admin=false)
	{
		global $DB;
		//if(in_array(str_replace("-","",$post['nip']),self::$bnip)) { return "error"; }
    if(!self::checkNip($post['nip'],$id) AND !in_array(str_replace("-","",$post['nip']),self::$anip)) { return "errorNip"; }
    
		if($admin) { $this->save($id, $post['login'], $post['pass'], $post['mail'], $post['rang']); }
		$tmp=array();
		$tmp['name']=$post['name'];
		$tmp['address']=$post['address'];
		$tmp['nip']=$post['nip']; 
		$tmp['nipc']=functions::trimNip($post['nip']);
		$tmp['phone']=$post['phone'];
		$tmp['email']=$post['email'];
		$tmp['bank_name']=$post['bank_name'];
		$tmp['krs']=$post['krs'];    
		$tmp['www']=$post['www'];
		//$tmp['account']=$post['account'];
		$tmp['place']=$post['place'];
		$DB->AutoExecute("users_data",$tmp,"UPDATE","user_id='".$id."'");
		if($post['sub_end']>date("Y-m-d")) { $this->new_lic($id,$post['sub_end']." 23:59:59","admin"); }
		return "ok";
	}
	
	public function add_ac($user_id, $ac, $bn)
	{
    global $DB;
    $DB->Execute("INSERT INTO `users_account` SET `account`='".$ac."', `user_id`='".$user_id."', `name`='".$bn."'");
    return "ok";
  }
  
	public function del_ac($id, $user_id)
	{
    global $DB;
    $DB->Execute("DELETE FROM `users_account` WHERE `user_id`='".$user_id."' AND `id`='".$id."'");
    return "ok";
  }
	
	public function save_ac($id, $user_id, $ac, $bn)
	{
    global $DB;
    $DB->Execute("UPDATE `users_account` SET `account`='".$ac."', `name`='".$bn."' WHERE `user_id`='".$user_id."' AND `id`='".$id."'");
    return "ok";
  }
  
}
/**
 * 
 * @author kordan
 *
 */
class newsletter_msg
{
	public $id;
	public $topic;
	public $text;
	public $date;
	/**
	 * Konstruktor wiadomości
	 * @param int $id identyfikator wiadomości
	 * @param string $topic temat wiadomości
	 * @param datetime $date data wysłania
	 * @param string $text treść wiadomości
	 */
	public function __construct($id,$topic,$date,$text=null)
	{
		global $func;
		$this->id=$id;
		$this->topic=$func->show_with_html($topic);
		$this->date=$date;
		$this->text=$func->show_with_html($text);
	}
}
?>