<?php
$months=array();
switch(LANG)
{
	case "pl":
		$months['01']="Styczeń";
		$months['02']="Luty";
		$months['03']="Marzec";
		$months['04']="Kwiecień";
		$months['05']="Maj";
		$months['06']="Czerwiec";
		$months['07']="Lipiec";
		$months['08']="Sierpień";
		$months['09']="Wrzesień";
		$months['10']="Październik";
		$months['11']="Listopad";
		$months['12']="Grudzień";
		break;
	case "en":
		$months['01']="January";
		$months['02']="February";
		$months['03']="March";
		$months['04']="April";
		$months['05']="May ";
		$months['06']="June";
		$months['07']="July";
		$months['08']="August";
		$months['09']="September";
		$months['10']="October";
		$months['11']="November";
		$months['12']="December";
		break;
	default:
		break;
}
?>