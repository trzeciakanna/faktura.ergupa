<?php
/**
 * Konfiguracja i połączenie z bazą danych
 *
 */
class connection
{
	/**
	 * Lokalizacja bazy danych
	 * @var string
	 */
	private $location;
	/**
	 * typ bazy danych
	 * @var string
	 */
	private $type;
	/**
	 * login do bazy danych
	 * @var string
	 */
	private $name;
	/**
	 * hasło do bazy danych
	 * @var string
	 */
	private $pass;
	/**
	 * nazwa bazy danych
	 * @var string
	 */
	private $base;
	/**
	 * Konstruktor klasy
	 */
	public function __construct()
	{
		$this->location="localhost";
		$this->type="mysql";
		$this->name="admin_dev";
		//$this->pass="wystawianie";
		$this->pass="d4HnVzvh3";
		$this->base="admin_dev";
	}
	/**
	 * Połączenie z bazą danych
	 * @return mixed
	 */
	public function connect()
	{
		$DB=NewADOConnection($this->type);
		@$ok=$DB->Connect($this->location,$this->name,$this->pass,$this->base);
		if($ok!=1){ header("Location: ".ROOT_URL."errors/connection.html"); }
		$DB->Execute("SET NAMES utf8");
		$ADODB_CACHE_DIR=ROOT_DIR."cache_db";
		return $DB;
	}
	/**
	 * Rozłączenie z bazą danych
	 * @param mixed $DB obiekt połaczenia z bazą danych
	 */
	public function disconect($DB)
	{
		if($DB->IsConnected()) { $DB->Close(); }
	}
}
?>