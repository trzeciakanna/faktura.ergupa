<?php

class search
{
	/**
	 * Usunięcie znaków specialnych
	 * @param string $str dane do przeparsowania
	 * @param boolean $search parsowanie frazy wyszukującej
	 * @return string wynik parsowania
	 */
	private function remove_sign($str, $search=false)
	{
		if($search)
		{
			$x=array("+","-");
			$y=array(" xandx "," xnotx ");
			$str=str_replace($x,$y,$str);
		}
		$arr=array(",",".","/",";","'","[","]","<",">","?",":","\"","{","}","!","@","#","$","%","^","&","*","(",")","-","=","\\","_","+","|","\n","\r","\t");
		$str=str_replace($arr," ",$str);
		do { $str=str_replace("  "," ",$str,$count); } while($count);
		return $str;
	}
	/**
	 * Usunięcie polskich znaków i zamiana na małe litery
	 * @param string $str dane do przeparsowania
	 * @return string wynik parsowania
	 */
	private function remove_polish($str)
	{
		$pl=array("ę","ó","ą","ś","ł","ż","ź","ć","ń","Ę","Ó","Ą","Ś","Ł","Ż","Ź","Ć","Ń");
		$eng=array("e","o","a","s","l","z","z","c","n","e","o","a","s","l","z","z","c","n");
		$str=str_replace($pl,$eng,$str);
		$str=strtolower($str);
		return $str;
	}
	/**
	 * Usunięcie ktrótkich wyrazów (do 3 znaków)
	 * @param string $str dane do przeparsowania
	 * @return string wynik parsowania
	 */
	private function remove_word($str)
	{
		$tmp=explode(" ",$str);
		for($a=0,$max=count($tmp);$a<$max;$a++)
		{ if(strlen($tmp[$a])<4) { unset($tmp[$a]); } }
		$str=implode(" ",$tmp);		
		do { $str=str_replace("  "," ",$str,$count); } while($count);
		return $str;
	}
	/**
	 * Zliczenie fraz i usunięcie nielicznych
	 * @param string $str dane do przeparsowania
	 * @param int $min minimalna liczba wystąpień
	 * @return string wynik parsowania
	 */
	private function count_word($str, $min=2)
	{
		$tmp=explode(" ",$str);
		$tmp=array_count_values($tmp);
		//przepisanie na soundex
		$x=array();
		foreach($tmp as $k => $v)
		{ $x[soundex($k)]+=$v; }
		arsort($x);
		//zrobienie stringa
		$tmp=array();
		foreach($x as $k => $v)
		{ if($v>=$min) { $tmp[]=$this->tree_sign($v)."-".$k; } }
		$str=implode(" ",$tmp);		
		do { $str=str_replace("  "," ",$str,$count); } while($count);
		return $str." ";
	}
	/**
	 * Wygenerowanie fraz do wyszukiwania w bazie
	 * @param string $str dane do przeparsowania
	 * @return array fraza and i not
	 */
	private function search_expression($str)
	{
		$tmp=explode(" ",$str);
		$and=array();
		$not=array();
		for($a=0,$max=count($tmp);$a<$max;$a++)
		{
			if($tmp[$a]!="xnotx" AND $tmp[$a]!="xandx")
			{
				if($tmp[$a-1]=="xnotx") { $not[]=soundex($tmp[$a]); }
				else { $and[]=soundex($tmp[$a]); }
			}
		}
		$and=array_unique($and);
		$not=array_unique($not);		
		return array(implode(" ",$and)." ",implode(" ",$not)." ");
	}
	/**
	 * Dodanie zer uzupełniających do długości
	 * @param int $count długość ciągu
	 * @return string liczba z zerami
	 */
	private function tree_sign($count)
	{
		if($count>=100) { return $count; }
		if($count>=10) { return "0".$count; }
		else { return "00".$count; }
	}
	/**
	 * Frazy wyszukiwania
	 * @param string $str dane do przeparsowania
	 * @return array fraza and i not
	 */
	public function phrase($str)
	{
		$str=$this->remove_sign($str, true);
		$str=$this->remove_polish($str);
		$str=$this->remove_word($str);
		return $this->search_expression($str);
	}
	/**
	 * Utworzenie znaczników tekstu
	 * @param string $str dane do przeparsowania
	 * @param int $min minimalna liczba wystąpień
	 * @return string wynik parsowania
	 */
	public function parse($str, $min=2)
	{
		$str=$this->remove_sign($str);
		$str=$this->remove_polish($str);
		$str=$this->remove_word($str);
		$str=$this->count_word($str, $min);
		return $str;
	}
}
?>