<?php
$units=array();

switch(LANG)
{
	case "pl":
	$units['szt']="sztuka";    
		$units['doba']="doba";
		$units['egz.']="egzemplarz";
		$units['godz.']="godzina";
		$units['g']="gram";
		$units['kg']="kilogram";
		$units['km']="kilometr";
		$units['kpl']="komplet";
		$units['kurs']="kurs"; 
		$units['kWh']="kWh"; 
		$units['l']="litr";
		$units['m']="metr";
		$units['mb']="metr bierzący"; 
		$units['mtg']="motogodzina";
		$units['m&sup2;']="metr kwadratowy";
		$units['m&sup3;']="metr sześcienny"; 
		$units['miesiąc']="miesiąc";    
		$units['MWh']="MWh";
		$units['opak']="opakowanie";
		$units['GJ']="gigadżul";
		$units['tona']="tona"; 
		$units['usługa']="usługa"; 
		$units['fracht']="FRACHT";
		$units['tona atro']="tona atro";
		break;
	case "en":    
		$units['day']="day";
		$units['copy']="copy";
		$units['hour']="hour";
		$units['g']="gram";
		$units['kg']="kilogram";
		$units['km']="kilometre";
		$units['kpl']="package";
		$units['kWh']="kWh";
		$units['l']="litre";
		$units['m']="metre";
		$units['mb']="linear metre"; 
		$units['mtg']="engine hours";
		$units['m&sup2;']="square metre";
		$units['m&sup3;']="cubic metre";
		$units['month']="month";      
		$units['MWh']="MWh";
		$units['carton']="carton";
		$units['piece']="piece";
		$units['ton']="ton";
		$units['bunch']="bunch";
		$units['jar']="jar";
		$units['tin']="tin";
		$units['btl']="btl";
		$units['pcs']="pcs";
		$units['ltr']="ltr";
		$units['pck']="pck";
		$units['cup']="cup";
		$units['loaf']="loaf";
		$units['case']="case"; 
		$units['roll']="roll"; 
		$units['hobbs']="hobbs";
		break;                 
	default:
		break;
}
?>