<?php
/**
 * Konfiguracja smartów
 *
 */
class smarty_conf
{
	/**
	 * Inicjalizacja - strona główna
	 * @return mixed obiekt smartów
	 */
	public function initialize()
	{
		$smarty = new Smarty();
		$smarty->template_dir=ROOT_DIR."module/";
		$smarty->compile_dir=ROOT_DIR."templates_cache/";
		$smarty->config_dir=ROOT_DIR."configs/";
		$smarty->cache_dir=ROOT_DIR."cache/";
		$smarty->caching = false;	
		return $smarty;
	}
	/**
	 * Inicjalizacja - panel administracyjny
	 * @return mixed obiekt smartów
	 */
	public function initialize_admin()
	{
		$smarty = new Smarty();
		$smarty->template_dir=ROOT_DIR."admin/module/";
		$smarty->compile_dir=ROOT_DIR."admin/templates_cache/";
		$smarty->config_dir=ROOT_DIR."admin/configs/";
		$smarty->cache_dir=ROOT_DIR."admin/cache/";
		$smarty->caching = false;
		return $smarty;
	}
}
?>