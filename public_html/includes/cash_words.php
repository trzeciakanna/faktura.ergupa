<?php
$liczby_waluta=array();
$liczby_tysiac=array();
$liczby_milion=array();
$liczby_miliard=array();
$liczby_j=array();
$liczby_n=array();
$liczby_d=array();
$liczby_s=array();

$la=$_GET['langword']?$_GET['langword']:LANG;
switch($la)
{
	case "pl":
		$liczby_waluta=array("zł","gr");
		$liczby_tysiac=array("tysiąc","tysiące","tysięcy");
		$liczby_milion=array("milion","miliony","milionów");
		$liczby_miliard=array("miliard","miliardy","miliardów");
		$liczby_j=array("jeden","dwa","trzy","cztery","pięć","sześć","siedem","osiem","dziewięć");
		$liczby_n=array("dziesięć","jedenaście","dwanaście","trzynaście","czternaście","piętnaście","szesnaście","siedemnaście","osiemnaście","dziewiętnaście");
		$liczby_d=array("dwadzieścia","trzydzieści","czterdzieści","pięćdziesiąt","sześćdziesiąt","siedemdziesiąt","osiemdziesiąt","dziewięćdziesiąt");
		$liczby_s=array("sto","dwieście","trzysta","czterysta","pięćset","sześćset","siedemset","osiemset","dziewięćset");
		break;
	case "en":
		$liczby_waluta=array("zł","gr");
		$liczby_tysiac=array("thousand","thousand","thousand");
		$liczby_milion=array("million","million","million");
		$liczby_miliard=array("miliard","miliardy","miliardów");
		$liczby_j=array("one","two","three","four","five","six","seven","eight","nine");
		$liczby_n=array("ten","eleven","twelve","thirteen","fourteen","fiveteen","sixteen","seventeen","eighteen","nineteen");
		$liczby_d=array("twenty","thirty","fourty","fivety","sixty","seventy","eighty","ninety");
		$liczby_s=array("hudnerd","two hundred","three hundred","four hundred","five hundred","six hundred","seven hundred","eight hundred","nine hundred");
		break;
	default:
		break;
}
?>