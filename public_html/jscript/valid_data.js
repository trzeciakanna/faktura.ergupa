valid_data = {
	/**
	 * Sprawdzenie poprawności wprowadzaniego numeru NIP
	 * @param event zdarzenie przeglądarki
	 * @return boolean czy wprowadzanie jest poprawne
	 */
	check_nip : function(event, ident)
	{
    return true;
		var del=key_mask(event,"0123456789-PLENDEGB",1);
		if(del)
		{
			if(!ident) { ident='nip'; }
			var str=$(ident).value;
			if((str.length==3 || str.length==7 || str.length==10) && del!="del")
			{ $(ident).value+="-"; }
			else if(str.length==12)
			{
				str=str.replace(/-/g,"0");
				var scales=new Array(6,5,7,0,2,3,4,0,5,6,0,7);
				var sum=0;
				for(var a=0; a<12; a++)
				{ sum+=scales[a]*parseInt(str.substring(a,a+1)); }
				sum=sum%11;
				if(sum==del)
				{
					$(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_ok";
					return true;
				}
				else
				{
					$(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_error";;
					$(ident).value="";
					info($('bad_nip').value);
					return false;
				}
			}
		}
		else { return false; }
	},
	/**
	 * Sprawdzenie poprawności wprowadzania numeru REGON
	 * @param event zdarzenie przeglądarki
	 * @return boolean czy wprowadzanie jest poprawne
	 */
	check_regon : function(event)
	{
		return key_mask(event,"0123456789");
	},
	/**
	 * Sprawdzenie poprawności wprowadzaniego numeru REGON
	 * @param event zdarzenie przeglądarki
	 * @return boolean czy numer jest poprawny
	 */
	valid_regon : function(ident)
	{
		if(!ident) { ident='regon'; }
		var str=$(ident).value;
		if(str.length==7) { str="00"+str; }
		if(str.length==9)
		{
			var scales=new Array(8,9,2,3,4,5,6,7);
			var sum=0;
			for(var a=0; a<8; a++)
			{ sum+=scales[a]*parseInt(str.substring(a,a+1)); }
			sum=sum%11;
			if(sum==str.substring(8,9))
			{ $(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_ok"; }
			else
			{
				$(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_error";;
				$(ident).value="";
				info($('bad_regon').value);
			}
		}
		else if(str.length==14)
		{
			var scales=new Array(2,4,8,5,0,9,7,3,6,1,2,4,8);
			var sum=0;
			for(var a=0; a<13; a++)
			{ sum+=scales[a]*parseInt(str.substring(a,a+1)); }
			sum=sum%11;
			if(sum==str.substring(13,14))
			{ $(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_ok"; }
			else
			{
				$(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_error";;
				$(ident).value="";
				info($('bad_regon').value);
			}
		}
		else
		{
			$(ident).className=$(ident).className.replace("_ok","").replace("_error","")+"_error";
			$(ident).value="";
			info($('bad_regon').value);
		}
	},
	/**
	 * Sprawdzenie poprawności wprowadzania numeru KRS
	 * @param event zdarzenie przeglądarki
	 * @return boolean czy wprowadzanie jest poprawne
	 */
	check_krs : function(event)
	{
		return key_mask(event,"0123456789");
	},
	/**
	 * Sprawdzenie poprawności wprowadzaniego numeru konta
	 * @param event zdarzenie przeglądarki
	 * @return boolean czy wprowadzanie jest poprawne
	 */
	check_account : function(event, ident)
	{
		if(!ident) { ident='account'; }
		var del=key_mask(event,"0123456789",1);
		if(del)
		{
			var str=$(ident).value;
			if((str.length==2 || str.length==7 || str.length==12 || str.length==17 || str.length==22 || str.length==27) && del!="del")
			{ $(ident).value+=" "; }
		}
		else
		{ return false; }
	}
}