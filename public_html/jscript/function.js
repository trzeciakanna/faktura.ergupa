/**
 * Zwaracanie obiektu po id
 * @param id id obiektu
 * @return mixed obiekt
 */
function $(id) {
  return document.getElementById(id);
}
/**
 * Pokazuje element
 * @param id id elementu
 */
function show_element(id) {
  $(id).style.display = "block";
}
/**
 * Ukrywa element
 * @param id id elementu
 */
function hide_element(id) {
  $(id).style.display = "none";
}

function hide_element_animate(id) {
  $(id).style.display = "none";
}
/**
 * Pozwala na wpisywanie tylko wybranych znaków
 * @param event obiekt zdarzenia
 * @param maska dozwolone znaki
 * @return czy znak można wpisać
 */
function key_mask(evt, maska, del) {
  evt = (evt) ? evt : ((event) ? event : null);
  if (evt.which) {
    kodKlawisza = evt.which;
  } else if (evt.keyCode) {
    kodKlawisza = evt.keyCode;
  } else {
    kodKlawisza = evt.charCode;
  }
  klawisz = String.fromCharCode(kodKlawisza);
  if (kodKlawisza == 0 || kodKlawisza == 9 || kodKlawisza == 37 || kodKlawisza == 38 || kodKlawisza == 39 || kodKlawisza == 40 || kodKlawisza == 116) {
    return true;
  } else if (kodKlawisza == 8 && del) {
    return "del";
  } else if (kodKlawisza == 8) {
    return true;
  }
  if (maska.indexOf(klawisz) == -1) {
    return false;
  } else if (del) {
    return klawisz;
  } else {
    return true;
  }
}
/**
 * Pobranie rozmiaru okna przeglądarki
 * @return array rozmiar okna przeglądarki
 */
function window_size() {
  var d = document;
  var w = window;
  if (typeof w.innerWidth != 'undefined') {
    var winWidth = w.innerWidth;
    var winHeight = w.innerHeight;
  } else {
    if (d.documentElement && typeof d.documentElement.clientWidth != 'undefined' && d.documentElement.clientWidth != 0) {
      var winWidth = d.documentElement.clientWidth;
      var winHeight = d.documentElement.clientHeight;
    } else {
      if (d.body && typeof d.body.clientWidth != 'undefined') {
        var winWidth = d.body.clientWidth;
        var winHeight = d.body.clientHeight;
      }
    }
  }
  return new Array(winWidth, winHeight);
}
/**
 * Potwierdzenie usuwania
 * @return boolean czy kontynuować
 */
function confirm_del() {
  return confirm($('delete').value);
}
/**
 * Usunięcie polskich znaków
 * @param str tekst wejścowy
 * @return string przeparsowany tekst
 */
function delete_pl(str) {
  var pl = new Array("ę", "ó", "ą", "ś", "ł", "ż", "ź", "ć", "ń", "Ę", "Ó", "Ą", "Ś", "Ł", "Ż", "Ź", "Ć", "Ń");
  var eng = new Array("e", "o", "a", "s", "l", "z", "z", "c", "n", "E", "O", "A", "S", "L", "Z", "Z", "C", "N");
  for (var a = 0; a < 18; a++) {
    str = str.replace(new RegExp(pl[a], "g"), eng[a]);
  }
  return str;
}
/**
 * Zaokrąglanie liczby do miejsc po przecinku
 * @param num liczna
 * @param poz ilość miejsc po przecinku
 * @return number liczba zmiennoprzecinowa o zadanej precyzji
 */
function round(num, poz) {
  num = Math.round(num * Math.pow(10, poz)) / Math.pow(10, poz);
  var tmp = num.toString().split(".");
  if (tmp[1]) {
    poz -= tmp[1].length;
  } else {
    num += ".";
  }
  for (var a = 0; a < poz; a++) {
    num += "0";
  }
  return num;
}
/**
 * Wyświetlenie komunikatu
 * @param str treść komunikatu
 */
function info(str, id) {
  if ($(id + '_error')) {
    $(id + '_error').innerHTML = str.replace(/\n/g, "<br/>");
    $(id + '_error').style.display = (str.length ? "block" : "none");
  } else {
    $('komunikat').innerHTML = str.replace(/\n/g, "<br/>");
  }
}
/**
 * Dodanie zdarzenia do obiektu
 * @param obj obiekt
 * @param event zdarzenie
 * @param fuct funkcja
 */
function addEvent(obj, event, fuct) {
  if (obj.attachEvent) {
    obj.attachEvent("on" + event, fuct);
  } else {
    obj.addEventListener(event, fuct, false);
  }
}
/**
 * Formatowanie liczb
 * @param number liczba
 * @param decimals ilość miejsc dziesiętnych
 * @param dec_point separator dziesiętny
 * @param thousands_sep separator tysiecy
 * @return sformatowana liczba
 */
function number_format(number, decimals, dec_point, thousands_sep) {
  var n = number,
    c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
  var d = dec_point == undefined ? "." : dec_point;
  var t = thousands_sep == undefined ? "," : thousands_sep,
    s = n < 0 ? "-" : "";
  var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
/**
 * Funkcja sleep
 * @param milliseconds czas uśpienia w milisekundach
 */
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds) {
      break;
    }
  }
}
/**
 * Dodanie wysyłania formularza enterem
 * @param event obiekt zdarzenia
 */
function add_submit_form(event) {
  var forms = document.getElementsByTagName("form");
  for (var a = 0; a < forms.length; a++) {
    forms[a].onkeypress = function(event) {
      submit_form(event, this);
    };
  }
}
/**
 * Wysłanie formularza enterem
 * @param event obiekt zdarzenia
 * @param obj obiekt formularza
 */
function submit_form(event, obj) {
  if (window.Event) {
    var kod = event.which;
  } else {
    var kod = event.keyCode;
  }
  if (kod == 13) {
    var el = document.activeElement;
    if (el.type == "textarea") {
      return false;
    }
    var but = obj.getElementsByTagName("input");
    for (var a = 0; a < but.length; a++) {
      if (but[a].type == "button") {
        if (but[a].onclick) {
          but[a].click();
        }
      }
    }
  }
}
/**
 * Dodanie ciasteczek
 * @param c_name nazwa
 * @param value wartość
 * @param expiredays czas trzymania
 */
function setCookie(c_name, value, expiredays) {
  var exdate = new Date();
  exdate.setDate(exdate.getDate() + expiredays);
  document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString()) + "; path=/";
}

function getCookie(c_name) {
  var i, x, y, ARRcookies = document.cookie.split(";");
  for (i = 0; i < ARRcookies.length; i++) {
    x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
    x = x.replace(/^\s+|\s+$/g, "");
    if (x == c_name) {
      return unescape(y);
    }
  }
}

function ie_error(obj) {
  if (obj.checked) {
    setCookie('ie_error', 1, 15);
  }
}

function changeLang(l) {
  setCookie('lang', l, 180);
  location.href = location.href;
  return false;
}

function backspace(evt) {
  evt = (evt) ? evt : ((event) ? event : null);
  var t = evt.target;
  if (t['tagName'].toLowerCase() == 'input' || t['tagName'].toLowerCase() == 'textarea' || t['tagName'].toLowerCase() == 'select') {
    return true;
  } else {
    if (evt.which) {
      kodKlawisza = evt.which;
    } else if (evt.keyCode) {
      kodKlawisza = evt.keyCode;
    } else {
      kodKlawisza = evt.charCode;
    }
    if (kodKlawisza == 8) {
      return confirm('Czy napewno chcesz opuścić tą stronę?');
    }
  }
  return true;
}

function newsInfoClose(t, id) {
  if (t) {
    setCookie('news' + id, 1, 14);
  }
  $('newsInfo').style.display = 'none';
  return false;
}

function showInfo(obj) {
  var msg = "";
  for (var i in obj) {
    if (obj[i]) {
      msg += i + " = " + obj[i] + "\n";
    }
  }
  alert(msg);
}

function print_r(o) {
  function f(o, p, s) {
    for (x in o) {
      if ('object' == typeof o[x]) {
        s += p + x + ' obiekt: \n';
        pre = p + '\t';
        //s = f(o[x], pre, s);
      } else {
        s += p + x + ' : ' + o[x] + '\n';
      }
    }
    return s;
  }
  return f(o, '', '');
}
jQuery(document).ready(function() {
  jQuery(".slider").not('.slick-initialized').slick({
    dots: false,
    arrow: true,
    autoplay: false,

    nextArrow: '<span class="arrow rightArrow"></span>',
    prevArrow: '<span class="arrow leftArrow"></span>',
    slidesToShow: 2,
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    }]
  });
});

jQuery(document).ready(function() {
  var stickyNavTop = jQuery('#stickyMenu').offset().top;

  var stickyNav = function() {
    var scrollTop = jQuery(window).scrollTop();

    if (scrollTop > stickyNavTop) {
      jQuery('#stickyMenu').addClass('sticky');
    } else {
      jQuery('#stickyMenu').removeClass('sticky');
    }
  };

  stickyNav();

  jQuery(window).scroll(function() {
    stickyNav();
  });
});
