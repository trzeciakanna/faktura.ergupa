calendar = {
  /**
   * Skróty nazw dni tygodnia
   */
  day: ["Pn", "Wt", "Śr", "Cz", "Pt", "So", "Nd"],
  daye: ["Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun"],
  /**
   * Nazwy miesięcy
   */
  month: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
  monthe: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
  /**
   * Liczba dni  w miesiącach
   */
  dof: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
  /**
   * Wygenerowanie formularza kalendarza
   * @param id identyfikator kalendarza
   * @param date data do ustawienia
   * @param empty czy data pusta
   */
  create: function(id, date, empty) {
    if (getCookie('lang') == 'en') {
      this.day = this.daye;
      this.month = this.monthe;
    }
    var a, max;
    //sprawdzenie i rozbicie daty
    var tmp = date.split(' ')[0].split('-');
    var Today = date ? new Date(tmp[0], tmp[1] - 1, tmp[2]) : new Date();
    Today = isNaN(Today.getDate()) ? new Date() : Today;
    var y = Today.getFullYear();
    var m = Today.getMonth() + 1;
    var d = Today.getDate();
    if ((!(date && date.length > 5) && empty) || date == '0000-00-00') {
      var y = '--';
      var m = '--';
      var d = '--';
    }
    //utworzenie głównego kontenera
    var main = document.createElement("div");
    main.id = "calendar_" + id;
    main.className = "calendar_main";

    var span = document.createElement('span');
    span.className = "arrowSelect";
    //dodanie pola dnia
    var day = document.createElement("div");
    day.className = 'select col-xs-2 px-0';
    var dayselect = document.createElement("select");
    dayselect.id = "calendar_d_" + id;
    dayselect.className = "calendar_day ";

    for (a = 1, max = (m == 2 ? (this.leap_year(y) ? 29 : 28) : this.dof[m - 1]); a <= max; a++) {
      var opt = new Option(a, a);
      opt.innerHTML = a;
      dayselect.appendChild(opt);
    }
    var opt = new Option("--", "--");
    opt.innerHTML = "--";
    dayselect.appendChild(opt);
    day.appendChild(dayselect);
    day.appendChild(document.createElement("span"));

    //dodanie pola miesiąca
    var month = document.createElement("div");
		month.className='select col-xs-4 px-0';
		var monthselect=document.createElement('select');
    monthselect.id = "calendar_m_" + id;
    monthselect.className = "calendar_month ";

    for (a = 0, max = 12; a < max; a++) {
      var opt = new Option(this.month[a], a + 1);
      opt.innerHTML = this.month[a];
      monthselect.appendChild(opt);
    }
    var opt = new Option("--", "--");
    opt.innerHTML = "--";
    monthselect.appendChild(opt);
		month.appendChild(monthselect);
		month.appendChild(document.createElement("span"));

    //dodanie pola roku
    var year = document.createElement("div");
		year.className='select  col-xs-3 px-0';
		var yearselect = document.createElement("select");
    yearselect.id = "calendar_y_" + id;
    yearselect.className = "calendar_year";

    for (a = 2020, max = 2009; a >= max; a--) {
      var opt = new Option(a, a);
      opt.innerHTML = a;
      yearselect.appendChild(opt);
    }
    var opt = new Option("--", "--");
    opt.innerHTML = "--";
    yearselect.appendChild(opt);
		year.appendChild(yearselect);
		year.appendChild(document.createElement("span"));
    //ikona kalendarza
    /*var link=document.createElement("input");
    link.type="button";
    link.className="calendar_button";
    link.id="calendar_show_"+id;
    link.value="";*/
    var link = document.createElement("a");
    link.className = "calendar_button";
    link.id = "calendar_show_" + id;
    var icon = document.createElement("i");
    icon.className = "calendar col-xs-2";
    link.appendChild(icon);

    //ukryte pole formularza
    var date = document.createElement("input");
    date.type = "hidden";
    date.id = "calendar_date_" + id;
    date.name = id;
    date.value = y + "-" + m + "-" + d;
    //dodanie obiektów
    main.appendChild(day);
    main.appendChild(month);
    main.appendChild(year);
    main.appendChild(link);
    main.appendChild(date);
    //wyświetlenie kalendarza
    var tmp = document.createElement("div");
    tmp.appendChild(main);
    document.write(tmp.innerHTML);
    //ustawienie daty
    $('calendar_d_' + id).value = d;
    $('calendar_m_' + id).value = m;
    $('calendar_y_' + id).value = y;
    //dodanie zdarzeń zmiany
    addEvent($('calendar_d_' + id), "change", function() {
      calendar.change_date(id);
    });
    addEvent($('calendar_m_' + id), "change", function() {
      calendar.change_date(id);
      calendar.reload_days(id);
    });
    addEvent($('calendar_y_' + id), "change", function() {
      calendar.change_date(id);
      calendar.reload_days(id);
    });
    addEvent($('calendar_show_' + id), "click", function() {
      calendar.show_panel(id);
    });
  },
  /**
   * Przeładowanie listy dni
   * @param id identyfikator kalendarza
   */
  reload_days: function(id) {
    var d = $('calendar_d_' + id).value;
    var p = $("calendar_d_" + id);
    var y = $('calendar_y_' + id).value;
    var m = $('calendar_m_' + id).value;
    //usunięcie dni
    while (p.childNodes.length >= 1) {
      p.removeChild(p.firstChild);
    }
    //dodanie dni
    for (a = 1, max = (m == 2 ? (this.leap_year(y) ? 29 : 28) : this.dof[m - 1]); a <= max; a++) {
      var opt = new Option(a, a);
      opt.innerHTML = a;
      p.appendChild(opt);
    }
    $('calendar_d_' + id).value = d;
  },
  /**
   * Sprawdzenie czy rok jest przestępny
   * @param year rok
   * @return boolean przestępność roku
   */
  leap_year: function(year) {
    return (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) ? true : false;
  },
  /**
   * Uaktualnienie daty
   * @param id identyfikator kalendarza
   */
  change_date: function(id) {
    $("calendar_date_" + id).value = $('calendar_y_' + id).value + "-" + $('calendar_m_' + id).value + "-" + ($('calendar_d_' + id).value ? $('calendar_d_' + id).value : 1);
  },
  /**
   * Ustawienie daty i zamknięcie okna kalendarza
   * @param id identyfikator kalendarza
   * @param y rok
   * @param m miesiąc
   * @param d dzień
   */
  set_date: function(id, y, m, d) {
    //alert(id+" - "+y+" - "+m+" - "+d);
    $('calendar_m_' + id).value = m;
    $('calendar_y_' + id).value = y;
    this.reload_days(id);
    $('calendar_d_' + id).value = d;
    $("calendar_date_" + id).value = y + "-" + m + "-" + d;
    this.close(id);
  },
  /**
   * Wygenerowanie okna kalendarza z panelem wyboru daty
   * @param id identyfikator kalendarza
   */
  show_panel: function(id) {
    if ($("calendar_panel_" + id)) {
      this.close(id);
      return true;
    }
    var a, max;
    //utworzenie głównego kontenera
    var main = document.createElement("div");
    main.id = "calendar_panel_" + id;
    main.className = "calendar_panel";
    //utworzenie paska nawigacyjnego
    var navi = document.createElement("div");
    navi.className = "calendar_navi";
    //utworzenie przycisków poprzedni
    var prev = document.createElement("div");
    prev.className = "calendar_buttonp";
    prev.id = "calendar_prev_" + id;
    //utworzenie kontenera na miesiąc i rok
    var date = document.createElement("div");
    date.className = "calendar_pdate";
    //dodanie pola miesiąca
    var month = document.createElement("select");
    month.id = "calendar_pm_" + id;
    month.className = "calendar_pmonth";
    for (a = 0, max = 12; a < max; a++) {
      var opt = new Option(this.month[a], a + 1);
      opt.innerHTML = this.month[a];
      month.appendChild(opt);
    }
    //dodanie pola roku
    var year = document.createElement("select");
    year.id = "calendar_py_" + id;
    year.className = "calendar_pyear";
    for (a = 2020, max = 2009; a >= max; a--) {
      var opt = new Option(a, a);
      opt.innerHTML = a;
      year.appendChild(opt);
    }
    //dodanie obiektów do kontenera na miesiąc i rok

    date.appendChild(month);
    date.appendChild(year);
    //utworzenie przycisków następny
    var next = document.createElement("div");
    next.className = "calendar_buttonn";
    next.id = "calendar_next_" + id;
    //dodanie obiektów do paska nawigacji
    navi.appendChild(prev);
    navi.appendChild(date);
    navi.appendChild(next);
    //utworzenie kontenera na nagłówki dni
    var name = document.createElement("div");
    name.className = "calendar_name";
    //dodanie nagłówków dni
    for (a = 0; a < 7; a++) {
      var day = document.createElement("div");
      day.innerHTML = this.day[a];
      day.className = a == 6 ? "calendar_dayn" : "calendar_day";
      name.appendChild(day);
    }
    //utworzenie kontenera na dni
    var days = document.createElement("div");
    days.className = "calendar_days";
    days.id = "calendar_days_" + id;
    //dodanie obiektów do głownego kontenera
    main.appendChild(navi);
    main.appendChild(name);
    main.appendChild(days);
    //wyświetlenie głównego kontenera
    $("calendar_" + id).appendChild(main);
    //ustawienie daty
    var tmp = $("calendar_date_" + id).value.split("-");
    $("calendar_pm_" + id).value = tmp[1];
    $("calendar_py_" + id).value = tmp[0];
    //dodanie funkcji
    addEvent($("calendar_prev_" + id), "click", function() {
      calendar.prev_month(id);
    });
    addEvent($("calendar_next_" + id), "click", function() {
      calendar.next_month(id);
    });
    addEvent($("calendar_pm_" + id), "change", function() {
      calendar.generate_days(id);
    });
    addEvent($("calendar_py_" + id), "change", function() {
      calendar.generate_days(id);
    });
    calendar.generate_days(id);
  },
  /**
   * Wygenerowanie listy dni
   * @param id identyfikator kalendarza
   */
  generate_days: function(id) {
    var Today = new Date();
    var sy = Today.getFullYear();
    var sm = Today.getMonth() + 1;
    var sd = Today.getDate();
    var m = parseInt($("calendar_pm_" + id).value);
    var y = parseInt($("calendar_py_" + id).value);
    var z = m < 3 ? y - 1 : y;
    var t = m >= 3 ? 1 : 0;
    var d = (Math.floor(23 * m / 9) + 5 + y + Math.floor(z / 4) - Math.floor(z / 100) + Math.floor(z / 400) - 2 * t - 1) % 7;
    //wyczyszczenie dni
    $("calendar_days_" + id).innerHTML = "";
    //dodanie pustych
    for (t = 0; t < d; t++) {
      var day = document.createElement("div");
      day.className = "calendar_day";
      $("calendar_days_" + id).appendChild(day);
    }
    //dodanie właściwych
    for (a = 1, max = (m == 2 ? (this.leap_year(y) ? 29 : 28) : this.dof[m - 1]); a <= max; a++) {
      var day = document.createElement("div");
      day.id = "calendar_day_" + a + "_" + id;
      day.innerHTML = a;
      day.className = (sy == y && sm == m && sd == a) ? "calendar_dayt" : ((a - 1 + d) % 7 == 6 ? "calendar_dayn" : "calendar_day");
      $("calendar_days_" + id).appendChild(day);
      addEvent($("calendar_day_" + a + "_" + id), "click", function() {
        calendar.set_date(id, y, m, this.innerHTML);
      });
    }
    $("calendar_days_" + id).style.height = Math.ceil((d + (m == 2 ? (this.leap_year(y) ? 29 : 28) : this.dof[m - 1])) / 7) * 20 + "px";
  },
  /**
   * Przejście do poprzedniego kalendarza
   * @param id identyfikator kalendarza
   */
  prev_month: function(id) {
    var m = parseInt($("calendar_pm_" + id).value);
    if (m == 1) {
      $("calendar_pm_" + id).value = 12;
      $("calendar_py_" + id).value--;
    } else {
      $("calendar_pm_" + id).value--;
    }
    calendar.generate_days(id);
  },
  /**
   * Przejście do następnego miesiąca
   * @param id identyfikator kalendarza
   */
  next_month: function(id) {
    var m = parseInt($("calendar_pm_" + id).value);
    if (m == 12) {
      $("calendar_pm_" + id).value = 1;
      $("calendar_py_" + id).value++;
    } else {
      $("calendar_pm_" + id).value++;
    }
    calendar.generate_days(id);
  },
  /**
   * Zamknięcie okna kalendarza
   * @param id identyfikator kalendarza
   */
  close: function(id) {
    $("calendar_" + id).removeChild($("calendar_panel_" + id));
  }


};
