<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
$tmp=explode("/",$_SERVER['SCRIPT_FILENAME']);
$tmp2=explode("/",$_SERVER['REQUEST_URI']);
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");  
//define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("PAGE_URL",str_replace(".php","",$tmp[sizeof($tmp)-1]));
define("MENU_URL",str_replace(".php","",$tmp2[1]));
define("CACHE_TIME",60);
define("LANG","pl");
define("LAYOUT","standard");
$uploader_count=0;
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
require(ROOT_DIR."includes/users.php");
require(ROOT_DIR."admin/module/navigation/class.php");
/* utworzenie głównych klas */
$conn=new connection();
$DB=$conn->connect();
$func=new functions();
$smart_conf=new smarty_conf();
$smarty=$smart_conf->initialize_admin();
$users=new users();
//pasek navigacji/metadane
require(ROOT_DIR."admin/module/navigation/".LANG.".php");
global $nlang;
$navi=new navi($nlang);
/* sprawdzenie czy zalogowany */
if($_SESSION['user_id']) { $my=$users->read_one($_SESSION['user_id']); }
elseif($_COOKIE['user_id']) { $my=$users->read_one_by_hash($_COOKIE['user_id']); }
if($my->id)
{
	define("USER_ID",$my->id);
	define("USER_LOGIN",$my->login);
	define("USER_RANG",$my->rang);
	define("USER_MAIL",$my->mail);
	define("USER_SUB",$my->sub_end);
}
else
{
	define("USER_ID","");
	define("USER_LOGIN","");
	define("USER_RANG","");
	define("USER_MAIL","");
	define("USER_SUB","");
}
if(USER_RANG!="admin") { header("Location: ".ROOT_URL); exit(); }
/* oczyszczenie kodu */
$_GET=$func->block_code($_GET);
$_POST=$func->block_code($_POST);
/* oczyszczenie zmiennych i pierwszy punk pomiaru czasu */
$conf=array();
$_SESSION['microtime']=array();
$func->microtime_point();
/* przesłanie do szablonu głównych parametrów */
$smarty->assign(array(
	"root_dir"=>ROOT_DIR,
	"root_url"=>ROOT_URL,
	"page_url"=>PAGE_URL,
	"menu_url"=>MENU_URL,
	"user_id"=>USER_ID,
	"user_login"=>USER_LOGIN,
	"user_rang"=>USER_RANG
));
/* wyświetlenie nagłówka */
$smarty->assign("meta",$func->meta_read());
$smarty->display("head/".LAYOUT.".html");
/* wyświetlenie menu */
require(ROOT_DIR."admin/module/menu/module.php");
require(ROOT_DIR."admin/module/navigation/module.php");
/* główny silnik strony */
switch($_GET['par1'])
{
	case "meta":
		require(ROOT_DIR."admin/module/meta/module.php");
		break;
	case "contact":
		require(ROOT_DIR."admin/module/contact/module.php");
		break;
	case "news":
		require(ROOT_DIR."admin/module/news/module.php");
		break;
	case "user_add":
		require(ROOT_DIR."admin/module/user/add/module.php");
		break;
	case "user_list":
		require(ROOT_DIR."admin/module/user/list/module.php");
		break;
	case "user_send":
		require(ROOT_DIR."admin/module/user/send/module.php");
		break;
	case "probe":
		require(ROOT_DIR."admin/module/probe/module.php");
		break; 
	case "stat":
		require(ROOT_DIR."admin/module/stat/module.php");
		break; 
	case "renew":
		require(ROOT_DIR."admin/module/renew/module.php");
		break;
		
	case "main":
	default:
		require(ROOT_DIR."admin/module/main/module.php");
		break;
}

/* wyświetlenie stopki */

$smarty->display("foot/".LAYOUT.".html");
/* rozłączenie z bazą danych */
$conn->disconect($DB);
/* wyświetlenie punktów pomiatu czasu */
$func->microtime_point();
$func->microtime_show();
?>