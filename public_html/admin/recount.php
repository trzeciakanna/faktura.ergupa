<?php
/* ustawienia serwera */
ini_set('arg-separator.output', '&amp;');
ini_set("url_rewriter.tags", "a=href,area=href,frame=src,input=src,form=fakeentry,fieldset=");
ini_set('session.gc_maxlifetime',3600);
ini_set('memory_limit','64M');
/* inicjalizacja */
session_start();
ob_start();
/* wysłanie nagłówków */
header("Content-type: text/html; charset=UTF-8");
header("content-Language: pl");
/* zdefiniowanie głównych stałych */
$tmp=explode("/",$_SERVER['SCRIPT_FILENAME']);
$tmp2=explode("/",$_SERVER['REQUEST_URI']);
define("ROOT_DIR",$_SERVER['DOCUMENT_ROOT']."/");
define("ROOT_URL","http://".$_SERVER['HTTP_HOST']."/");
define("PAGE_URL",str_replace(".php","",$tmp[sizeof($tmp)-1]));
define("MENU_URL",str_replace(".php","",$tmp2[1]));
define("CACHE_TIME",60);
define("LANG","pl");
define("LAYOUT","standard");
$uploader_count=0;
/* dołączenie blibliotek głównych */
require(ROOT_DIR."includes/include.php");
$func=new functions();
$func->recount_pdf();
header("Location: ".$_SERVER['HTTP_REFERER']);
?>