<?php
$lang=array();
$lang['head']="Ustawienia metadanych";
$lang['save']="Zapisz";
$lang['save_ok']="Dane zostały zapisane prawidłowo";
$lang['save_error']="Wystąpił problem podczas zapisywania danych, spróbuj pownownie za chwilę";

$lang['title']="Tytuł strony";
$lang['description']="Opis strony";
$lang['keywords']="Słowa kluczowe";
$lang['robots']="Dane dla robotów";
$lang['robots_option']=array();
$lang['robots_option']['index,follow']="Indeksować strone i odwiedzić linki";
$lang['robots_option']['noindex,follow']="Nie indeksować strony i odwiedzić linki";
$lang['robots_option']['index,nofollow']="Indeksować strone i nie odwiedzać linków";
$lang['robots_option']['noindex,nofollow']="Nie indeksować strony i nie odwiedzać linków";


?>