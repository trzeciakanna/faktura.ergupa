user_list_obj = {
	/**
	 * Sprawdzenie unikalności wprowadzonego loginu
	 */
	check_login : function() 
	{
		if(!$("login").value)
		{
			$('login').className="user_text_error";
			return false;
		}
		else if($("login").value==$("old_login").value)
		{
			$('reg_login').value=1;
			$('login').className="user_text_ok";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('login').className="user_text_ok";
				$('reg_login').value=1;
			}
			else
			{
				$('login').className="user_text_error";
				$('reg_login').value=0;
				info($('bad_login').value);
			}
		};
		req.AddParam("login",$("login").value);
		req.Send("module/user/add/ajax_check.php");
	},
	/**
	 * Sprawdzenie unikalności wprowadzonego adresu e-mail
	 */
	check_mail : function() 
	{
		if(!$("mail").value)
		{
			$('mail').className="user_text_error";
			return false;
		}
		else if($("mail").value==$("old_mail").value)
		{
			$('reg_mail').value=1;
			$('mail').className="user_text_ok";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('mail').className="user_text_ok";
				$('reg_mail').value=1;
			}
			else
			{
				$('mail').className="user_text_error";
				$('reg_mail').value=0;
				info($('bad_mail').value);
			}
		};
		req.AddParam("mail",$("mail").value);
		req.Send("module/user/add/ajax_check.php");
	},	
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function() 
	{
		var msg="";
		if($('pass').value!=$('pass_repeat').value) { msg+=$('bad_pass').value; }
		if($('reg_login').value!=1 || !$('login').value) { msg+=$('bad_login').value; }
		if($('reg_mail').value!=1 || !$('mail').value) { msg+=$('bad_mail').value; }
		if(msg)
		{
			info(msg);
			return false;
		}
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('save_ok').value); }
				else
				{ info($('save_error').value); }
			};
			req.SendForm("user_save","module/user/list/ajax_save.php");
		}
		return false;
	},
	
	search : function()
	{
		var t = location.href.split('/admin/');
		location.href=t[0] + "/admin/user_list/1/0/na/"+escape($('login').value)+"/";
		return false;
	},
  
  noteAdd : function() 
	{

			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{
          info($('save_ok').value);
          location.href=location.href;
        }
				else
				{ info($('save_error').value); }
			};
			req.SendForm("note_add","module/user/list/ajax_note_add.php");
		return false;
	},
};