<?php
$lang=array();
$lang['main']="Lista użytkowników";
$lang['all']="wszystkie";
$lang['edit']="edytuj";
$lang['del']="usuń";
$lang['back']="wstecz";

$lang['logins']="Login";
$lang['search']="Szukaj";

$lang['head']="Dane do serwisu";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['repeat']="powtórz";
$lang['mail']="Adres e-mail";
$lang['invoice']="Dane do faktury";
$lang['name']="Imię i nazwisko/nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['phone']="Telefon";
$lang['email']="Adres e-mail";
$lang['bank_name']="Nazwa banku";
$lang['krs']="Numer KRS";
$lang['account']="Numer konta";
$lang['place']="Miejsce wystawienia faktury";
$lang['cancel']="Anuluj";
$lang['save']="Zapisz";
$lang['bad_login']="Podany login jest już zajęty";
$lang['bad_mail']="Podany adres e-mail jest już zajety";
$lang['bad_pass']="Podane hasła są różne";
$lang['info']="Podane pola są wymagane";
$lang['info2']="Podaj tylko jeżeli chcesz zmienić hasło";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_krs']="Podany numer KRS jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_regon']="Podany numer REGON jest nieprawidłowy, wprowadź poprawny numer.";
$lang['delete']="Czy napewno chcesz usunąć tego użytkownika";

$lang['sort_name']="Sortuj wg. nazwy";
$lang['sort_datec']="Sortuj wg. daty rejestracji";
$lang['sort_datea']="Sortuj wg. daty abonamentu";

$lang['pay_on']="Abonament";
$lang['pay_off']="Bez abonamentu";
$lang['pay_all']="Wszyscy";
$lang['pay_time']="Ważność abonamentu";

$lang['rang']="Ranga";
$lang['rangs']=array();
$lang['rangs']['user']="Użytkownik";
$lang['rangs']['admin']="Administrator";
$lang['rangs']['ban']="Zbanowany";

$lang['save_ok']="Dane zostały zapisane prawidłowo";
$lang['save_error']="Wystąpił problem podczas zapisywania danych, spróbuj pownownie za chwilę";

$lang['time']="Okres abonamentu";
$lang['month_o']="miesiąc";
$lang['month_t']="miesiące";
$lang['month_f']="miesięcy"; 
                           
$lang['notes']="Notatki"; 
$lang['note']="Treść notatki"; 
$lang['noteAdd']="Dodaj";  
$lang['noteDel']="Czy chcesz usunąć tą notatkę"; 
?>