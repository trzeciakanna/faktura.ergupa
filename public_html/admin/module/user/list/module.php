<?php
global $smarty;
global $users;
require(ROOT_DIR."admin/module/user/list/".LANG.".php");
global $lang;

if($_GET['par2']=="del" AND (int)$_GET['par3'])
{
	$users->del($_GET['par3']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
if($_GET['par2']=="note_del" AND (int)$_GET['par3'])
{
	$users->delete_notes($_GET['par3']);
	header("Location: ".$_SERVER['HTTP_REFERER']);
	exit();
}
elseif($_GET['par2']=="user" AND (int)$_GET['par3'])
{
  $user = new users_invoice($_GET['par3']);
	$smarty->assign("user",$user);
  $smarty->assign("invoice",$user->read_end_pay($_GET['par3'])); 
  $smarty->assign("notes",$user->read_notes($_GET['par3']));
	$smarty->assign("action","edit");
}
else
{
	$sort=$_GET['par4']?$_GET['par4']:"cd";
	list($dane,$page,$last)=$users->read_list(null,$_GET['par2'],20,$_GET['par5'],(int)$_GET['par3'],$sort);
	$smarty->assign("dane",$dane);
	$smarty->assign("page",$page);
	$smarty->assign("last",$last);
	$smarty->assign("sort",$sort);
	$smarty->assign("sign",$_GET['par5']?$_GET['par5']."/":"");
	$smarty->assign("type",(int)$_GET['par3']);
	$smarty->assign("letter",$users->letter_list());
	$smarty->assign("action","list");
}


$smarty->assign("lang",$lang);
$smarty->display("user/list/".LAYOUT.".html");
?>