<?php
$lang=array();
$lang['head']="Dane do serwisu";
$lang['login']="Login";
$lang['pass']="Hasło";
$lang['repeat']="powtórz";
$lang['mail']="Adres e-mail";
$lang['invoice']="Dane do faktury";
$lang['name']="Imię i nazwisko/nazwa firmy";
$lang['address']="Adres";
$lang['nip']="NIP";
$lang['phone']="Telefon";
$lang['email']="Adres e-mail";
$lang['bank_name']="Nazwa banku";
$lang['krs']="Numer KRS";
$lang['account']="Numer konta";
$lang['place']="Miejsce wystawienia faktury";
$lang['cancel']="Anuluj";
$lang['add']="Dodaj";
$lang['bad_login']="Podany login jest już zajęty";
$lang['bad_mail']="Podany adres e-mail jest już zajety";
$lang['bad_pass']="Podane hasła są różne";
$lang['info']="Podane pola są wymagane";
$lang['bad_nip']="Podany numer NIP jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_krs']="Podany numer KRS jest nieprawidłowy, wprowadź poprawny numer.";
$lang['bad_regon']="Podany numer REGON jest nieprawidłowy, wprowadź poprawny numer.";

$lang['rang']="Ranga";
$lang['rangs']=array();
$lang['rangs']['user']="Użytkownik";
$lang['rangs']['admin']="Administrator";
$lang['rangs']['ban']="Zbanowany";

$lang['add_ok']="Użytkownik został dodany prawidłowo";
$lang['add_error']="Wystąpił problem podczas dodawania użytkownika, spróbuj pownownie za chwilę";



?>