user_add_obj = {
	/**
	 * Sprawdzenie unikalności wprowadzonego loginu
	 */
	check_login : function() 
	{
		if(!$("login").value)
		{
			$('login').className="user_text_error";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('login').className="user_text_ok";
				$('reg_login').value=1;
			}
			else
			{
				$('login').className="user_text_error";
				$('reg_login').value=0;
				info($('bad_login').value);
			}
		};
		req.AddParam("login",$("login").value);
		req.Send("module/user/add/ajax_check.php");
	},
	/**
	 * Sprawdzenie unikalności wprowadzonego adresu e-mail
	 */
	check_mail : function() 
	{
		if(!$("mail").value)
		{
			$('mail').className="user_text_error";
			return false;
		}
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{
				$('mail').className="user_text_ok";
				$('reg_mail').value=1;
			}
			else
			{
				$('mail').className="user_text_error";
				$('reg_mail').value=0;
				info($('bad_mail').value);
			}
		};
		req.AddParam("mail",$("mail").value);
		req.Send("module/user/add/ajax_check.php");
	},	
	/**
	 * Sprawdzenie poprawności formularza i uaktualnienie użytkownika
	 */
	submit : function() 
	{
		var msg="";
		if($('pass').value!=$('pass_repeat').value || !$('pass').value) { msg+=$('bad_pass').value; }
		if($('reg_login').value!=1) { msg+=$('bad_login').value; }
		if($('reg_mail').value!=1) { msg+=$('bad_mail').value; }
		if(msg)
		{
			info(msg);
			return false;
		}
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{alert(this.responseText);
				if(this.responseText=="ok")
				{ info($('add_ok').value); }
				else
				{ info($('add_error').value); }
			};
			req.SendForm("user_add","module/user/add/ajax_add.php");
		}
		return false;
	}




};