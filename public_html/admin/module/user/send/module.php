<?php
global $smarty;
global $users;
require(ROOT_DIR."admin/module/user/send/".LANG.".php");
global $lang;

switch($_GET['par2'])
{
	case "last":
		list($dane,$page,$last)=$users->send_list($_GET['par3'],20,$_GET['par4']);
		$smarty->assign("dane",$dane);
		$smarty->assign("page",$page);
		$smarty->assign("last",$last);
		$smarty->assign("sign",$_GET['par4']?$_GET['par4']."/":"");
		$smarty->assign("letter",$users->send_letter());
		$smarty->assign("action","last");
		break;
	case "view":
		list($msg, $user)=$users->send_one($_GET['par3']);
		$smarty->assign("msg",$msg);
		$smarty->assign("user",$user);
		$smarty->assign("action","view");
		break;
	case "resend":  
    if(!$_GET['par4']) { $_GET['par4']="a"; }
    if($_GET['par4']=='-') { $_GET['par4']=""; }
		$smarty->assign("user_id",USER_ID);
		list($msg, $user)=$users->send_one($_GET['par3']);
		$smarty->assign("msg",$msg);
		$smarty->assign("user",$user);
		list($dane,$page,$last)=$users->read_list(null,1,99999,$_GET['par4']);
		$smarty->assign("dane",$dane);
		$smarty->assign("letter",$_GET['par4']);
		$smarty->assign("letters",$users->letter_list());
		$smarty->assign("action","send");
		break;
	default:
    if(!$_GET['par2']) { $_GET['par2']="a"; } 
    if($_GET['par2']=='-') { $_GET['par2']=""; }
		$smarty->assign("user_id",USER_ID);
		list($dane,$page,$last)=$users->read_list(null,1,99999,$_GET['par2']);
		$smarty->assign("dane",$dane);
		$smarty->assign("user",array());  
		$smarty->assign("letter",$_GET['par2']);
		$smarty->assign("letters",$users->letter_list());
		$smarty->assign("action","send");
		break;
}



$smarty->assign("lang",$lang);
$smarty->display("user/send/".LAYOUT.".html");
?>