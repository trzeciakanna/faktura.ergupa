<?php
$lang=array();
$lang['head']="Wyślij wiadomość";
$lang['topic']="Temat";
$lang['text']="Treść";
$lang['client']="Lista użytkowników";
$lang['search']="Wyszukaj";
$lang['all']="zaznacz/odznacz wszystkich";
$lang['count']="Masz zaznaczone [count] użytkowników";

$lang['last']="Wysłane wiadomości";
$lang['view']="Zobacz";
$lang['resend']="Wyślij jeszcze raz";
$lang['back']="wróć";
$lang['all_last']="wszystkie";

$lang['err_users']="Wybierz kontrakentów z listy";
$lang['err_topic']="Podaj temat wiadomości";
$lang['err_text']="Podaj treść wiadomości";

$lang['cancel']="Wyczyść";
$lang['send']="Wyślij";
$lang['send_ok']="Wiadomości zostały dodane do wysłania poprawnie.";
$lang['send_error']="Wystąpił błąd, proszę spróbować ponownie za chwilę.";
?>