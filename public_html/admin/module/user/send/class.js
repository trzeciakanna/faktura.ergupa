user_send_obj = {	
	/**
	 * Wysłanie wiadomości
	 */
	submit : function() 
	{
		var msg="";
		//sprawdzenie wpisania treści i tytułu
		if(!$('topic').value.length) { msg+=$('err_topic').value+"\n"; }
		if(!$('text').value.length) { msg+=$('err_text').value+"\n"; }
		//sprawdzenie zaznaczenia użytkowników
		var err=true;
		var list=$('item_list').getElementsByTagName("input");
		for(var a=0;a<list.length;a++)
		{
			if(list[a].type=="checkbox") { if(list[a].checked==true) { err=false; break; } }
		}
		if(err) { msg+=$('err_users').value+"\n"; }
		if(!msg)
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('send_ok').value); $('client_send_form').reset(); }
				else
				{ info($('send_error').value); }
			};
			req.SendForm("user_form","module/user/send/ajax_send.php");
		}
		else { info(msg); }
		return false;
	},
	
	select_all : function(check)
	{
		var list=$('item_list').getElementsByTagName("input");
		for(var a=0;a<list.length;a++)
		{
			if(list[a].type=="checkbox") { list[a].checked=check; }
		}
		this.count();
	},
	
	search : function(txt)
	{
		var list=$('item_list').getElementsByTagName("span");
		for(var a=0;a<list.length;a++)
		{
			if(txt!="" && list[a].innerHTML==list[a].innerHTML.replace(txt,"")) { list[a].parentNode.style.display="none"; }
			else { list[a].parentNode.style.display="block"; }
		}
	},
	
	count : function()
	{
		var count=0;
		var list=$('item_list').getElementsByTagName("input");
		for(var a=0;a<list.length;a++)
		{
			if(list[a].type=="checkbox") { if(list[a].checked==true) { count++; } }
		}
		$('count_users').innerHTML=$('count').value.replace('[count]',count);
	}

};