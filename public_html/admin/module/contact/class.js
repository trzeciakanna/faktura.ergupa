contact_obj = {
	/**
	 * Zapisanie danych kontaktowych
	 */
	save : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value); }
			else
			{ info($('save_error').value); }
		};
		req.SendForm("contact_form","module/contact/ajax_save.php");
	},
	/**
	 * Dodanie działu
	 */
	type_add : function()
	{
		var msg="";
		if($('name').value.length<1) { msg+=$('empty_name').value+"\n"; }
		if($('mail').value.length<1) { msg+=$('empty_mail').value+"\n"; }
		if(msg)
		{ info(msg); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{
					$('contact_type_form').reset();
					info($('add_ok').value);
				}
				else
				{ info($('add_error').value); }
			};
			req.SendForm("contact_type_form","module/contact/ajax_add.php");
		}
	},
	/**
	 * Zapisanie działu
	 */
	type_save : function()
	{
		var msg="";
		if($('name').value.length<1) { msg+=$('empty_name').value+"\n"; }
		if($('mail').value.length<1) { msg+=$('empty_mail').value+"\n"; }
		if(msg)
		{ info(msg); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ location.href="contact/"; }
				else
				{ info($('save_error').value); }
			};
			req.SendForm("contact_type_form","module/contact/ajax_tsave.php?id="+$('id').value);
		}
	}
	
};