<?php
class contact_send
{
	
	public function add($name, $mail)
	{
		global $DB;
		$r=$DB->Execute("SELECT MAX(`orders`) AS `max`  FROM `contact_send`");
		$tmp=array();
		$tmp['name']=$name;
		$tmp['mail']=$mail;
		$tmp['orders']=$r->fields['max']+1;
		$DB->AutoExecute("contact_send",$tmp,"INSERT");
		return "ok";
	}
	
	public function save($id, $name, $mail)
	{
		if(!$id) { return; }
		global $DB;
		$tmp['name']=$name;
		$tmp['mail']=$mail;
		$DB->AutoExecute("contact_send",$tmp,"UPDATE","id='".$id."'");
		return "ok";
	}
	
	public function del($id)
	{
	if(!$id) { return; }
		global $DB;
		$DB->Execute("DELETE FROM `contact_send` WHERE `id`='".$id."' LIMIT 1");
	}
	
	public function up($id)
	{
	if(!$id) { return; }
		global $DB;
		$ro=$DB->Execute("SELECT `orders` AS `old` FROM `contact_send` WHERE `id`='".$id."' LIMIT 0,1");
		$rn=$DB->Execute("SELECT `orders` AS `new`,`id` FROM `contact_send` WHERE `orders`<'".$ro->fields['old']."' ORDER BY `orders` DESC LIMIT 0,1");
		$DB->Execute("UPDATE `contact_send` SET `orders`='0' WHERE `id`='".$id."'");
		$DB->Execute("UPDATE `contact_send` SET `orders`='".$ro->fields['old']."' WHERE `id`='".$rn->fields['id']."'");
		$DB->Execute("UPDATE `contact_send` SET `orders`='".$rn->fields['new']."' WHERE `id`='".$id."'");
	}
	
	public function down($id)
	{
		if(!$id) { return; }
		global $DB;
		$ro=$DB->Execute("SELECT `orders` AS `old` FROM `contact_send` WHERE `id`='".$id."' LIMIT 0,1");
		$rn=$DB->Execute("SELECT `orders` AS `new`,`id` FROM `contact_send` WHERE `orders`>'".$ro->fields['old']."' ORDER BY `orders` ASC LIMIT 0,1");
		$DB->Execute("UPDATE `contact_send` SET `orders`='0' WHERE `id`='".$id."'");
		$DB->Execute("UPDATE `contact_send` SET `orders`='".$ro->fields['old']."' WHERE `id`='".$rn->fields['id']."'");
		$DB->Execute("UPDATE `contact_send` SET `orders`='".$rn->fields['new']."' WHERE `id`='".$id."'");
	}
	
	public function read_one($id)
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `name`,`mail` FROM `contact_send` WHERE `id`='".$id."' LIMIT 0,1");
		$tmp['name']=$r->fields['name'];
		$tmp['mail']=$r->fields['mail'];
		return $tmp;
	}
	
	public function read_all()
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT `id`,`name`,`mail` FROM `contact_send` ORDER BY `orders` ASC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				$tmp['id']=$r->fields['id'];
				$tmp['name']=$r->fields['name'];
				$tmp['mail']=$r->fields['mail'];
				$dane[]=$tmp;
				$r->MoveNext();
			}
		}
		return $dane;
	}
	
	
	
	
	
}
?>