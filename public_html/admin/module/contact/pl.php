<?php
$lang=array();
$lang['head']="Ustawienia kontaktu";
$lang['save']="Zapisz";
$lang['save_ok']="Dane zostały zapisane prawidłowo";
$lang['save_error']="Wystąpił problem podczas zapisywania danych, spróbuj pownownie za chwilę";

$lang['title']="Nazwa";
$lang['address']="Adres";
$lang['contact']="Kontakt";
$lang['text']="Opis";

$lang['name']="Nazwa";
$lang['mail']="Adres e-mail";
$lang['account']="Działy kontaktu";
$lang['add']="Dodaj";
$lang['edit']="Edytuj";
$lang['del']="Usuń";
$lang['up']="W górę";
$lang['down']="W dół";
$lang['added']="Dodaj dział";
$lang['edit']="Edycja działu";
$lang['delete']="Czy napewno chcesz usunąć ten dział?";

$lang['empty_name']="Musisz podać nazwę działu";
$lang['empty_mail']="Musisz podać adres e-mail działu";
$lang['add_ok']="Dane zostały dodane poprawnie. W celu aktualizacji listy działów przeładuj stronę. (F5)";
$lang['add_error']="Wystąpił błąd podczas dodawania. Spróbuj ponownie za chwilę.";


?>