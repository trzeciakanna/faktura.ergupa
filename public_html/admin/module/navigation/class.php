<?php

class navi
{
	public $title;
	public $desc;
	public $keys;
	public $navi;
	
	private $meta;
	private $lvl;
	
	public function __construct($lang)
	{
		//wyczyszczenie
		$this->navi=array();
		$this->meta=array();
		$this->lvl=100;
		//dodanie pierwszych
		if(!empty($_GET['par1']) AND $_GET['par1']!="mian") { $this->navi[]=new navi_item($lang['main'],"main/"); }
		$this->meta[]=$lang['title'];
		//dodanie reszty
		switch($_GET['par1'])
		{
			case "stat":
				$this->navi[]=new navi_item($lang['stat'],"stat/");
				$this->meta[]=$lang['stat'];
				break;
			case "meta":
				$this->navi[]=new navi_item($lang['meta'],"meta/");
				$this->meta[]=$lang['meta'];
				break;
			case "contact":
				$this->navi[]=new navi_item($lang['contact'],"contact/");
				$this->meta[]=$lang['contact'];
				break;
			case "news":
				$this->navi[]=new navi_item($lang['news'],"news/");
				$this->meta[]=$lang['news'];
				$this->news($lang);
				break;
			case "user_add":
				$this->navi[]=new navi_item($lang['user_add'],"user_add/");
				$this->meta[]=$lang['user_add'];
				break;
			case "user_list":
				$this->navi[]=new navi_item($lang['user_list'],"user_list/");
				$this->meta[]=$lang['user_list'];
				if($_GET['par2']=="user")
				{
					global $DB;
					global $func;
					$r=$DB->Execute("SELECT `login` FROM `users` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
					$this->navi[]=new navi_item($lang['user_list_edit'].": ".$func->show_with_html($r->fields['login']),"user_list/edit/".$_GET['par3']."/");
					$this->meta[]=$lang['user_list_edit'].": ".$func->show_with_html($r->fields['login']);
				}
				break;
			case "user_send":
				$this->navi[]=new navi_item($lang['user_send'],"user_send/");
				$this->meta[]=$lang['user_send'];
				$this->user_send($lang);
				break;
			default:
				$this->lvl=1;
				break;
		}
		//wygenerowanie danych
		$this->meta=array_reverse($this->meta);
		for($a=0,$max=min(count($this->meta),$this->lvl);$a<$max;$a++)
		{
			$this->title.=($a!=0?" - ":"").$this->meta[$a];
			$this->desc.=($a!=0?" - ":"").$this->meta[$a];
			$this->keys.=($a!=0?", ":"").$this->meta[$a];
		}
	}
	
	private function news($lang)
	{
		switch($_GET['par2'])
		{
			case "add":
				$this->navi[]=new navi_item($lang['news_add'],"news/add/");
				$this->meta[]=$lang['news_add'];
				break;
			case "edit":
				global $DB;
				global $func;
				$r=$DB->Execute("SELECT `title` FROM `news` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
				$this->navi[]=new navi_item($lang['news_edit'].": ".$func->show_with_html($r->fields['title']),"news/edit/".$_GET['par3']."/");
				$this->meta[]=$lang['news_edit'].": ".$func->show_with_html($r->fields['title']);
				break;
			case "coment":
				global $DB;
				global $func;
				$r=$DB->Execute("SELECT `title` FROM `news` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
				$this->navi[]=new navi_item($lang['news_coment'].": ".$func->show_with_html($r->fields['title']),"news/coment/".$_GET['par3']."/");
				$this->meta[]=$lang['news_coment'].": ".$func->show_with_html($r->fields['title']);
				break;
			default:
				break;
		}
	}
	
	private function user_send($lang)
	{
		switch($_GET['par2'])
		{
			case "last":
				$this->navi[]=new navi_item($lang['user_send_last'],"user_send/last/");
				$this->meta[]=$lang['user_send_last'];
				break;
			case "view":
				global $DB;
				global $func;
				$r=$DB->Execute("SELECT `topic` FROM `newsletter_msg` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
				$this->navi[]=new navi_item($lang['user_send_view'].": ".$func->show_with_html($r->fields['topic']),"user_send/view/".$_GET['par3']."/");
				$this->meta[]=$lang['user_send_view'].": ".$func->show_with_html($r->fields['topic']);
				break;
			case "resend":
				global $DB;
				global $func;
				$r=$DB->Execute("SELECT `topic` FROM `newsletter_msg` WHERE `id`='".(int)$_GET['par3']."' LIMIT 0,1");
				$this->navi[]=new navi_item($lang['user_send_resend'].": ".$func->show_with_html($r->fields['topic']),"user_send/resend/".$_GET['par3']."/");
				$this->meta[]=$lang['user_send_resend'].": ".$func->show_with_html($r->fields['topic']);
				break;
			default:
				break;
		}
	}
	
	
	
	
	
}

class navi_item
{
	public $name;
	public $url;

	public function __construct($name, $url)
	{
		$this->name=$name;
		$this->url=$url;
	}
}
?>