<?php
class stat
{
  static function getCash()
  {
    global $DB;
    global $month;
    $t=array();
    $r=$DB->Execute("SELECT SUM(cash) AS `cash`, `date` FROM `pay_sys_platnosci_pl` WHERE `status`='99' GROUP BY DATE_FORMAT(date, '%Y-%m') ORDER BY date DESC LIMIT 24");
    if(!$r->fields['date']) { return $t; }
    while(!$r->EOF)
    {
      $t[]=array(substr($r->fields['date'],0,4), $month[(int)substr($r->fields['date'],5,2)], $r->fields['cash']/100, round($r->fields['cash']/123, 2));
      $r->MoveNext();
    }
    return $t;
  }

  static function getUser()
  {
    global $DB;
    global $month;
    $t=array();
    $r=$DB->Execute("SELECT COUNT(id) AS `count`, `date_add` FROM `users` WHERE `ip_log`!='' GROUP BY DATE_FORMAT(date_add, '%Y-%m') ORDER BY date_add DESC LIMIT 24");
    if(!$r->fields['date_add']) { return $t; }
    while(!$r->EOF)
    {
      $t[]=array(substr($r->fields['date_add'],0,4), $month[(int)substr($r->fields['date_add'],5,2)], $r->fields['count']);
      $r->MoveNext();
    }
    return $t;
  }
}

?>