<?php
global $smarty;
global $func;
require(ROOT_DIR."admin/module/renew/".LANG.".php");
global $lang;  

if($_POST['login'])
{
  //tworzenie nowego konta
  $login = $_POST['login'];
  $mail = $_POST['mail'];
  $pass = substr(MD5(microtime()),0,8);
  $nip = $_POST['nip'];
  $id=0;
  //szukanie id po nip
  if(!$_POST['id'])
  {
    $n = functions::trimNip($nip);
    $r = $DB->Execute("SELECT `user_id`, `name`, `address` FROM `users_data` WHERE `nipc` LIKE '%".$n."%'"); 
    if($r->_numOfRows>1)
    {
      $t=array();
      while(!$r->EOF)
      {
        $t[]=array($r->fields['user_id'], $r->fields['name'], $r->fields['address']);
        $r->MoveNext();
      }
      $smarty->assign("ids",$t);
    }
    elseif(!$r->_numOfRows)
    {
      $smarty->assign("msg","BRAK NIP");
    }
    else { $id = $r->fields['user_id']; }
  }
  else { $id = (int)$_POST['id']; }
  
  if($id)
  {
    //utworzenie konta
    $tmp=array();
    $tmp['id']=$id;
    $tmp['login']=$login;
    $tmp['pass']=SHA1("salt".$pass."code");
    $tmp['mail']=$mail;
    $tmp['rang']="user"; 
    $tmp['id_ref']=0; 
    $tmp['cash']=0; 
    $tmp['ip_add']=$func->get_ip();
    $tmp['date_add']=date("Y-m-d H:i:s");
    $tmp['hash']=SHA1($mail.$login."user");
    $DB->AutoExecute("users",$tmp,"INSERT");
    
    if($mail)
    {
      //wysłanie maila
      require(ROOT_DIR."includes/mailer.php");
      $mailer=new mailer();
      $user=array();
      $user[]=array("mail"=>$mail,"name"=>$login);
      $x=array();
      $x['login']=$login;
      $x['mail']=$mail;
      $x['pass']=$pass;
      $x['ip']=$tmp['ip_add'];
      $x['date']=$tmp['date_add'];
      $mailer->send($user,"users_renew",$x,null);
    }
    //odczyt abonamentów
    $r=$DB->Execute("SELECT `date`, `month`, `session_id` FROM `pay_sys_platnosci_pl` WHERE `status`='99' AND `user_id`='".$id."' ORDER BY `id` ASC");
    if($r->fields['date'])
    {
      while(!$r->EOF)
      {
        $x=$DB->Execute("SELECT `sub_end` FROM `users` WHERE `id`='".$id."' LIMIT 1");
        if($x->fields['sub_end'] > $r->fields['date'])
      	{ $date=date("Y-m-d H:i:s",strtotime($x->fields['sub_end']." +".$r->fields['month']." months")); }
      	else
      	{ $date=date("Y-m-d H:i:s",strtotime($r->fields['date']." +".$r->fields['month']." months")); }
        //echo $r->fields['user_id']." - ".$date." - ".$r->fields['month']."<br/>";
        $DB->Execute("UPDATE `users` SET `sub_end`='".$date."', `sub_hash`='".$r->fields['session_id']."', `sub_send`='0' WHERE `id`='".$id."'");
        $r->MoveNext();                
      }
    }
    
    $r=$DB->Execute("SELECT `sub_end` FROM `users` WHERE `id`='".$id."' LIMIT 1");
    if($r->fields['sub_end']>date("Y-m-d H:i:s"))
    {
      $date=date("Y-m-d H:i:s",strtotime($r->fields['sub_end']." +1 month"));
      $DB->Execute("UPDATE `users` SET `sub_end`='".$date."', `sub_send`='0' WHERE `id`='".$id."'");
    }
    
    $smarty->assign("pass", $pass);
  }
}


$smarty->assign("lang",$lang); 
$smarty->display("renew/".LAYOUT.".html");
?>