<?php
$lang=array();
$lang['head']="Lista sond";
$lang['head_add']="Dodaj sondę";
$lang['head_edit']="Edytuj sondę";
$lang['head_view']="Widok sondy";
$lang['save']="Zapisz";
$lang['add']="Dodaj";
$lang['edit']="Edytuj";
$lang['del']="Usuń";
$lang['view']="Zobacz";
$lang['back']="Wstecz";
$lang['delete']="Czy napewno chcesz usunąć";

$lang['question']="Pytanie";
$lang['answer']="Odpowiedź";
$lang['time']="Czas trwania";
$lang['time_end']="Data zakończenia";


$lang['add_ok']="Dane zostały dodane poprawnie.";
$lang['add_error']="Wystąpił błąd podczas dodawania. Spróbuj ponownie za chwilę.";
$lang['save_ok']="Dane zostały zapisane prawidłowo";
$lang['save_error']="Wystąpił problem podczas zapisywania danych, spróbuj pownownie za chwilę";


?>