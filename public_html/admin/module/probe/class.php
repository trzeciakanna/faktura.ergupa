<?php
class probe
{
	
	public function add($data)
	{
		global $DB;
		$tmp=array();
		$tmp['question']=$data['question'];
		$tmp['date_add']=date("Y-m-d H:i:s");
		$tmp['date_end']=date("Y-m-d H:i:s", time()+86400*$data['days']);
		$tmp['days']=$data['days'];
		$DB->AutoExecute("probe",$tmp,"INSERT");
		$id=$DB->Insert_ID();
		foreach($data['answer'] as $i=>$item)
		{ if($item) { $DB->Execute("INSERT INTO `probe_answer` SET `probe_id`='".$id."', `answer`='".$item."', `number`='".$i."'"); } }
		return "ok";
	}
	
	public function save($id, $data)
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `date_add` FROM `probe` WHERE `id`='".$id."' LIMIT 0,1");
		$tmp['question']=$data['question'];
		$tmp['date_end']=date("Y-m-d H:i:s", strtotime($r->fields['date_add'])+86400*$data['days']);
		$tmp['days']=$data['days'];
		$DB->AutoExecute("probe",$tmp,"UPDATE","`id`='".$id."'");
		foreach($data['answer'] as $i=>$item)
		{
			if($item)
			{
				$r=$DB->Execute("SELECT `id` FROM `probe_answer` WHERE `probe_id`='".$id."' AND `number`='".$i."'");
				if($r->fields['id']) { $DB->Execute("UPDATE `probe_answer` SET `answer`='".$item."' WHERE `id`='".$r->fields['id']."'"); }
				else { $DB->Execute("INSERT INTO `probe_answer` SET `probe_id`='".$id."', `answer`='".$item."', `number`='".$i."'"); }
			}
			else { $DB->Execute("DELETE FROM `probe_answer` WHERE `probe_id`='".$id."' AND `number`='".$i."'"); }
		}
		return "ok";
	}
	
	public function del($id)
	{
		if(!$id) { return; }
		global $DB;
		$DB->Execute("DELETE FROM `probe` WHERE `id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `probe_answer` WHERE `probe_id`='".$id."'");
		$DB->Execute("DELETE FROM `probe_glos` WHERE `probe_id`='".$id."'");
	}
	
	public function read_one($id)
	{
		global $DB;
		$tmp=array();
		$r=$DB->Execute("SELECT `question`,`date_end`,`on`,`days` FROM `probe` WHERE `id`='".$id."' LIMIT 0,1");
		$tmp['question']=$r->fields['question'];
		$tmp['date_end']=$r->fields['date_end'];
		$tmp['days']=$r->fields['days'];
		$tmp['on']=$r->fields['on'];
		$tmp['id']=$id;
		$tmp['answer']=array();
		$r=$DB->Execute("SELECT `answer`,`count`,`number` FROM `probe_answer` WHERE `probe_id`='".$id."'");
		if($r->fields['answer'])
		{
			while(!$r->EOF)
			{
				$t=array();
				$t['answer']=$r->fields['answer'];
				$t['count']=$r->fields['count'];
				$tmp['answer'][$r->fields['number']]=$t;
				$r->MoveNext();
			}
		}
		return $tmp;
	}
	
	public function read_all()
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT `id`,`question`,`date_end`,`on` FROM `probe` ORDER BY `question` ASC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$tmp=array();
				$tmp['id']=$r->fields['id'];
				$tmp['question']=$r->fields['question'];
				$tmp['date_end']=$r->fields['date_end'];
				$tmp['on']=$r->fields['on'];
				$dane[]=$tmp;
				$r->MoveNext();
			}
		}
		return $dane;
	}
	
	public function read_rand()
	{
		global $DB;
		$cookie=explode(",",$_COOKIE['probe']?$_COOKIE['probe']:"0");
		if(!is_array($_SESSION['probe'])) { $_SESSION['probe']=array(); }
		if(!is_array($cookie)) { $cookie=array(); }
		$ids=array_merge($_SESSION['probe'],$cookie,array(0));
		$r=$DB->Execute("SELECT `id` FROM `probe` WHERE `date_end`>NOW() AND `on`='1' AND `id` not in (".implode(",",$ids).")");
		$ids=array();
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$ids[]=$r->fields['id'];
				$r->MoveNext();
			}
		}
		$id=$ids[rand(0,count($ids)-1)];
		return $this->read_one($id);
	}
	
	public function sign($probe, $answer)
	{
		global $DB;
		$cookie=explode(",",$_COOKIE['probe']);
		if(!is_array($_SESSION['probe'])) { $_SESSION['probe']=array(); }
		if(!is_array($cookie)) { $cookie=array(); }
		if(!in_array($probe,$_SESSION['probe']) AND !in_array($probe,$cookie))
		{
			$DB->Execute("UPDATE `probe_answer` SET count=count+1 WHERE `probe_id`='".$probe."' AND `number`='".$answer."'");
			$_SESSION['probe'][]=$probe;
			$cookie[]=$probe;
			setcookie("probe",implode(",",$cookie),time()+31*86400,"/");
			//odczyt wyników
			$r=$DB->Execute("SELECT `answer`, `count` FROM `probe_answer` WHERE `probe_id`='".$probe."' ORDER BY `number`");
			$max=0;
			if($r->fields['answer'])
			{
				while(!$r->EOF)
				{
					echo $r->fields['answer']."|".$r->fields['count']."||";
					$max=$max<$r->fields['count']?$r->fields['count']:$max;
					$r->MoveNext();
				}
				echo "|".$max;
			}
		}
		else { return "error"; }
	}
	
}
?>