probe_obj = {
	/**
	 * Dodanie sondy
	 */
	add : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('add_ok').value); }
			else
			{ info($('add_error').value); }
		};
		req.SendForm("probe_form","module/probe/ajax_add.php");
		return false;
	},
	/**
	 * Zapisanie sondy
	 */
	save : function()
	{
		var req = mint.Request();
		req.OnSuccess = function()
		{
			if(this.responseText=="ok")
			{ info($('save_ok').value); }
			else
			{ info($('save_error').value); }
		};
		req.SendForm("probe_form","module/probe/ajax_save.php?id="+$('probe_id').value);
		return false;
	}
	
};