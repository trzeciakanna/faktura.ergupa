news_obj = {
	
	submit : function()
	{
		var msg="";
		if(!$('title').value) { msg+=$('empty_title').value+"\n"; }
		if(!$('text').value) { msg+=$('empty_text').value+"\n"; }
		if(msg)
		{ info(msg); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('add_ok').value); $('news_form').reset(); }
				else
				{ info($('add_error').value); }
			};
			req.SendForm("news_form","module/news/ajax_add.php?id="+$("user_id").value);
		}
		return false;
	},
	
	save : function()
	{
		var msg="";
		if(!$('title').value) { msg+=$('empty_title').value+"\n"; }
		if(!$('text').value) { msg+=$('empty_text').value+"\n"; }
		if(msg)
		{ info(msg); }
		else
		{
			var req = mint.Request();
			req.OnSuccess = function()
			{
				if(this.responseText=="ok")
				{ info($('save_ok').value); }
				else
				{ info($('save_error').value); }
			};
			req.SendForm("news_form","module/news/ajax_save.php?id="+$("id").value);
		}
		return false;
	},
	
	tree : function(level, id)
	{
		//sprawdzenie co robic
		if($('tree_'+level+'_'+id).style.display=="none")
		{
			//pokazanie bloku
			$('tree_'+level+'_'+id).style.display="block";
			$('button_'+level+'_'+id).className="minus";
		}
		else
		{
			//ukrycie bloku
			$('tree_'+level+'_'+id).style.display="none";
			$('button_'+level+'_'+id).className="plus";	
		}
		return false;
	}

};