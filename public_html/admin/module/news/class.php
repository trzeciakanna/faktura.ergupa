<?php
/**
 * 
 * @author kordan
 *
 */
class newses
{
	
	public function add($user_id, $dane)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['user_id']=$user_id;
		$tmp['user_name']=$dane['name'];
		$tmp['title']=$dane['title'];
		$tmp['text']=$dane['text'];
		$tmp['date_add']=date("Y-m-d H:i:s");
		$tmp['date_mod']=$tmp['date_add'];
		$tmp['title_s']=$search->parse($dane['title'],1);
		$tmp['text_s']=$search->parse($dane['text'],2);
		$DB->AutoExecute("news",$tmp,"INSERT");
		$id=$DB->Insert_ID();
		$c=1;
		for($a=0;$a<10;$a++)
		{
			if($dane['img'.$a])
			{
				$tmp=array();
				$tmp['news_id']=$id;
				$tmp['path']=str_replace("../","",$dane['img'.$a]);
				$tmp['count']=$c;
				$DB->AutoExecute("news_gallery",$tmp,"INSERT");
				$c++;
			}
		}
		echo "ok";
	}
	
	public function save($id, $dane)
	{
		global $DB;
		global $search;
		$tmp=array();
		$tmp['title']=$dane['title'];
		$tmp['text']=$dane['text'];
		$tmp['date_mod']=date("Y-m-d H:i:s");
		$tmp['title_s']=$search->parse($dane['title'],1);
		$tmp['text_s']=$search->parse($dane['text'],2);
		$DB->AutoExecute("news",$tmp,"UPDATE","`id`='".$id."'");
		$DB->Execute("DELETE FROM `news_gallery` WHERE `news_id`='".$id."' LIMIT 10");
		$DB->Execute("OPTIMIZE TABLE `news_gallery`");
		$c=1;
		for($a=0;$a<10;$a++)
		{
			if($dane['img'.$a])
			{
				$tmp=array();
				$tmp['news_id']=$id;
				$tmp['path']=str_replace("../","",$dane['img'.$a]);
				$tmp['count']=$c;
				$DB->AutoExecute("news_gallery",$tmp,"INSERT");
				$c++;
			}
		}
		echo "ok";
	}
	
	public function del($id)
	{
		global $DB;
		$DB->Execute("DELETE FROM `news` WHERE `id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `news_gallery` WHERE `news_id`='".$id."' LIMIT 1");
		$DB->Execute("DELETE FROM `news_coment` WHERE `news_id`='".$id."' LIMIT 1");
		$DB->Execute("OPTIMIZE TABLE `news`,`news_gallery`,`news_coment`");
	}
	
	/**
	 * Odczyt pojedynczego newsa
	 * @param int $id identyfikator newsa
	 * @param boolean $edit czy edycja
	 * @return mixed obiekt newsa
	 */
	public function read_one($id, $edit=false)
	{
		global $DB;
		if($edit) { $DB->Execute("UPDATE `news` SET `count`=count+1 WHERE `id`='".$id."'"); }
		return new news($id,null,null,null,null,null,null,null,"all");
	}
	/**
	 * Odczyt listy newsów
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @param string $sort rodzaj sortowania
	 * @param string $letter pierwsza litera tytułu
	 * @retrun array tablica danych
	 */
	public function read_list($page=1, $ile=20, $sort="dd", $letter="")
	{
		global $DB;
		$dane=array();
		$page=(int)$page?$page:1;
		$war=$letter?"WHERE `title` LIKE '".$letter."%'":"";
		//sortowanie
		$sort=(substr($sort,0,1)=="d"?"`date_mod`":"`title`")." ".(substr($sort,1,1)=="d"?"DESC":"ASC");
		//odczyt
		$r=$DB->PageExecute("SELECT * FROM `news` ".$war." ORDER BY ".$sort,$ile,$page);
		{
			while(!$r->EOF)
			{
				$dane[]=new news($r->fields['id'],$r->fields['title'],$r->fields['text'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['date_add'],$r->fields['date_mod'],$r->fields['count'],"one");
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Litery od których zaczynają się tytuły newsów
	 * @return array tablica liter
	 */
	public function letter_list()
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT SUBSTRING(LOWER(title),1,1) as `letter` FROM `news` GROUP BY SUBSTRING(title,1,1)");
		if($r->fields['letter'])
		{
			while(!$r->EOF)
			{
				$dane[]=$r->fields['letter'];
				$r->MoveNext();
			}
		}
		sort($dane);
		return $dane;
	}
	
	
	/**
	 * Odczyt komentarzy
	 * @param int $id identyfikator newsa
	 * @param int $page numer strony
	 * @param int $ile ilość wyników na stronie
	 * @return mixed tablica z danymi
	 */
	public function read_coment($id, $page=1, $ile=20)
	{
		global $DB;
		$dane=array();
		$page=(int)$page?$page:1;
		//odczyt
		$r=$DB->PageExecute("SELECT * FROM `news_coment` WHERE `ac`='1' AND `news_id`='".$id."' AND `parent`='0' ORDER BY `date_add` DESC",$ile,$page);
		{
			while(!$r->EOF)
			{
				$dane[]=new coment($r->fields['id'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['text'],true,1);
				$r->MoveNext();
			}
		}
		return array($dane,$page,$r->_lastPageNo);
	}
	/**
	 * Usunięcie komentarza
	 * @param int $id identyfikator komentarza
	 */
	public function del_coment($id)
	{
		global $DB;
		$r=$DB->Execute("SELECT `id` FROM `news_coment` WHERE `parent`='".$id."'");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$this->del_coment($r->fields['id']);
				$r->MoveNext();
			}
		}
		$DB->Execute("DELETE FROM `news_coment` WHERE `id`='".$id."' LIMIT 1");
		$DB->Execute("OPTIMIZE TABLE `news_coment`");
	}
}


/**
 * 
 * @author kordan
 *
 */
class news
{
	public $id;
	public $user_id;
	public $user_name;
	public $url;
	public $title;
	public $text;
	public $date_add;
	public $dane_mod;
	public $count;
	public $gallery;
	public $coment;
	/**
	 * Utworzenie obiektu newsa
	 * @param int $id identyfikator newsa
	 * @param string $title tytuł
	 * @param string $text treść
	 * @param int $user_id identyfikator autora
	 * @param string $user_name nazwa autora
	 * @param datetime $date_add data utworzenia
	 * @param datetim $date_mod data modyfikacji
	 * @param int $count licznik odwiedzin
	 * @param array $gallery tablica galerii
	 * @param array $coment tablica komentarzy
	 */
	public function __construct($id, $title="", $text="", $user_id=0, $user_name="", $date_add=null, $date_mod=null, $count=0, $gallery=false, $coment=false)
	{
		global $func;
		if($title)
		{
			$this->id=$id;
			$this->title=$func->show_with_html($title);
			$this->text=$func->show_with_html($text);
			$this->url=$func->create_url($this->title);
			$this->user_id=$user_id;
			$this->user_name=$user_name;
			$this->date_add=$date_add;
			$this->date_mod=$date_mod;
			$this->count=$count;
			if($gallery)
			{ $this->gallery=$this->read_gallery($id,$gallery); }
			if($coment)
			{ $this->coment=$this->read_coment($id,$coment); }
		}
		else
		{
			global $DB;
			$r=$DB->Execute("SELECT * FROM `news` WHERE `id`='".$id."' AND `ac`='1' LIMIT 0,1");
			$this->id=$id;
			$this->title=$func->show_with_html($r->fields['title']);
			$this->text=$func->show_with_html($r->fields['text']);
			$this->url=$func->create_url($this->title);
			$this->user_id=$r->fields['user_id'];
			$this->user_name=$r->fields['user_name'];
			$this->date_add=$r->fields['date_add'];
			$this->date_mod=$r->fields['date_mod'];
			$this->count=$r->fields['count'];
			if($gallery)
			{ $this->gallery=$this->read_gallery($id,$gallery); }
			if($coment)
			{ $this->coment=$this->read_coment($id,$coment); }
		}
	}
	/**
	 * Odczyt galerii
	 * @param int $id identyfikator newsa
	 * @param string $type rodzaj odczytu
	 * @return array tablica obiektów galerii
	 */
	public function read_gallery($id, $type="all")
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT * FROM `news_gallery` WHERE `news_id`='".$id."' ORDER BY `count` ASC".($type=="one"?" LIMIT 0,1":""));
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$t=new news_galery($r->fields['id'],$r->fields['name'],$r->fields['path']);
				if($t->id) { $dane[]=$t; }
				$r->MoveNext();
			}
		}
		return $dane;
	}
}
/**
 * 
 * @author kordan
 *
 */
class news_galery
{
	public $id;
	public $name;
	public $path;
	public $width;
	public $height;
	/**
	 * Utworzenie obiektu galerii
	 * @param int $id identyfikator grafiki
	 * @param string $name nazwa grafiki
	 * @param string $path ścieżka do grafiki
	 */
	public function __construct($id, $name, $path)
	{
		if(is_file(ROOT_DIR.$path))	
		{
			$this->id=$id;
			$this->name=$name;
			$this->path=$path;
			list($this->width, $this->height)=getimagesize(ROOT_DIR.$path);
		}
	}
}
/**
 * 
 * @author kordan
 *
 */
class coment
{
	public $id;
	public $user_id;
	public $user_name;
	public $text;
	public $date_add;
	public $child;
	public $level;
	/**
	 * Utworzenie obiektu komentarza
	 * @param int $id identyfikator komentarza
	 * @param int $user_id identyfikator autora
	 * @param string $user_name nazwa autora
	 * @param string $text treść
	 * @param datetime $date_add data dodania
	 * @param boolean $child czy odczytywać dzieci
	 * @param int $level poziom zagnieżdżenia
	 */
	public function __construct($id, $user_id=0, $user_name="", $text="", $date_add=null, $child=false, $level=0)
	{
		global $func;
		if($text)
		{
			$this->id=$id;
			$this->user_id=$user_id;
			$this->user_name=$user_name;
			$this->text=$func->show_with_html($text);
			$this->date_add=$date_add;
			$this->level=$level;
			if($child) { $this->child=$this->read_child($id,$level+1); }
		}
		else
		{
			global $DB;
			$r=$DB->Execute("SELECT * FROM `news_coment` WHERE `id`='".$id."' AND `ac`='1' LIMIT 0,1");
			$this->id=$id;
			$this->user_id=$r->fields['user_id'];
			$this->user_name=$r->fields['user_name'];
			$this->text=$func->show_with_html($r->fields['text']);
			$this->date_add=$r->fields['date_add'];
			$this->level=0;
			if($child) { $this->child=$this->read_child($id,1); }
		}
	}
	/**
	 * Odczyt dzieci komentarza
	 * @param int $id identyfikator rodzica
	 * @param int $level poziom zagniezdżenia
	 * @return array tablica komentarzy
	 */
	private function read_child($id, $level=1)
	{
		global $DB;
		$dane=array();
		$r=$DB->Execute("SELECT * FROM `news_coment` WHERE `parent`='".$id."' AND `ac`='1' ORDER BY `date_add` DESC");
		if($r->fields['id'])
		{
			while(!$r->EOF)
			{
				$dane[]=new coment($r->fields['id'],$r->fields['user_id'],$r->fields['user_name'],$r->fields['text'],$r->fields['date_add'],true,$level);
				$r->MoveNext();
			}	
		}
		return $dane;
	}
}
?>