<?php
global $smarty;
global $func;
require(ROOT_DIR."admin/module/stat/class.php");
require(ROOT_DIR."admin/module/stat/".LANG.".php");
global $lang;  
global $month;

$smarty->assign("cash", stat::getCash());  
$smarty->assign("user", stat::getUser());

$smarty->assign("lang",$lang);    
$smarty->assign("month",$month);
$smarty->display("stat/".LAYOUT.".html");
?>