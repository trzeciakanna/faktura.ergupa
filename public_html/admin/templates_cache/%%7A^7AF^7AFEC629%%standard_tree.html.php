<?php /* Smarty version 2.6.19, created on 2010-08-24 00:44:24
         compiled from news/standard_tree.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'news/standard_tree.html', 5, false),array('modifier', 'truncate', 'news/standard_tree.html', 5, false),)), $this); ?>
<?php $_from = $this->_tpl_vars['tree']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
	<div class="news_tree">
		<div class="news_button"><a href="#" onclick="javascript: return news_obj.tree(<?php echo $this->_tpl_vars['item']->level; ?>
,<?php echo $this->_tpl_vars['item']->id; ?>
);" title="<?php echo $this->_tpl_vars['lang']['show']; ?>
/<?php echo $this->_tpl_vars['lang']['hide']; ?>
" class="plus" id="button_<?php echo $this->_tpl_vars['item']->level; ?>
_<?php echo $this->_tpl_vars['item']->id; ?>
">&nbsp;</a></div>
		<div class="news_name" id="name_<?php echo $this->_tpl_vars['item']->id; ?>
">
			<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->text)) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 50, "...", true) : smarty_modifier_truncate($_tmp, 50, "...", true)); ?>

			<?php if ($this->_tpl_vars['user_rang'] == 'admin'): ?><a href="news/coment_del/<?php echo $this->_tpl_vars['item']->id; ?>
/" onclick="javascript: return confirm_del();" class="del" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
"><?php echo $this->_tpl_vars['lang']['del']; ?>
</a><?php endif; ?>
		</div>
	</div>
	<div class="news_tree" id="tree_<?php echo $this->_tpl_vars['item']->level; ?>
_<?php echo $this->_tpl_vars['item']->id; ?>
" style="display: none;">
		<div class="news_texts"><?php echo $this->_tpl_vars['item']->text; ?>
</div>
		<div class="news_texts"><?php echo $this->_tpl_vars['lang']['author']; ?>
: <?php echo $this->_tpl_vars['item']->user_name; ?>
</div>
		<div class="news_answer"><a href="news/<?php echo $_GET['par2']; ?>
/<?php echo $_GET['par3']; ?>
/coment/<?php echo $this->_tpl_vars['item']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['answer']; ?>
" class="add"><?php echo $this->_tpl_vars['lang']['answer']; ?>
</a></div>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "news/standard_tree.html", 'smarty_include_vars' => array('tree' => $this->_tpl_vars['item']->child)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
<?php endforeach; endif; unset($_from); ?>