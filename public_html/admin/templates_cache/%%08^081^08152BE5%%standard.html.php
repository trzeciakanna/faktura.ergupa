<?php /* Smarty version 2.6.19, created on 2011-01-17 14:36:53
         compiled from contact/standard.html */ ?>
<script type="text/javascript" src="module/contact/class.js"></script>
<?php if ($this->_tpl_vars['action'] == 'list'): ?>
<form action="" id="contact_form">
<fieldset>
<legend><?php echo $this->_tpl_vars['lang']['head']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['title']; ?>
:</label>
	<textarea name="title" rows="" cols="" class="contact_text"><?php echo $this->_tpl_vars['dane']['title']; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
	<textarea name="address" rows="" cols="" class="contact_text"><?php echo $this->_tpl_vars['dane']['address']; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['contact']; ?>
:</label>
	<textarea name="contact" rows="" cols="" class="contact_text"><?php echo $this->_tpl_vars['dane']['contact']; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['text']; ?>
:</label>
	<textarea name="text" rows="" cols="" class="contact_text"><?php echo $this->_tpl_vars['dane']['text']; ?>
</textarea>
</div>
<div class="row_action"><input type="button" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="contact_save" onclick="contact_obj.save();" /></div>
</fieldset>
</form>


<form action="" id="contact_type_form" >
<fieldset style="float:right">
<legend><?php echo $this->_tpl_vars['lang']['added']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
<input type="text" name="name" id="name" value="" class="contact_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
<input type="text" name="mail" id="mail" value="" class="contact_text" />
</div>
<div class="row_action"><input type="button" value="<?php echo $this->_tpl_vars['lang']['add']; ?>
" class="contact_add" onclick="contact_obj.type_add();" /></div>
</fieldset>
</form>




<div class="row_head" style="clear:both;"><?php echo $this->_tpl_vars['lang']['account']; ?>
</div>
<table class="tabela">
<tr>
<th>Nazwa</th>
<th>E-mail</th>
<th>Edycja</th>
<th>Usuń</th>
<th>TOP</th>
<th>DOWN</th>
</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<?php if (!(1 & ($this->_foreach['a']['iteration']-1))): ?>
<tr>
<?php else: ?>
<tr class="parzysty">
<?php endif; ?>
	<td class="first"><?php echo $this->_tpl_vars['i']['name']; ?>
</td>
	<td><?php echo $this->_tpl_vars['i']['mail']; ?>
</td>
	<td><a href="contact/edit/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
" class="edit"></a></td>
	<td><a href="contact/del/<?php echo $this->_tpl_vars['i']['id']; ?>
/" onclick="javascript: return confirm_del()" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" class="del"></a></td>
	<td><?php if ($this->_tpl_vars['k'] != 0): ?><a href="contact/up/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['up']; ?>
" class="up">Wyżej</a><?php else: ?>&nbsp;<?php endif; ?></td>
	<td class="last"><?php if ($this->_tpl_vars['k'] != count ( $this->_tpl_vars['list'] ) -1): ?><a href="contact/down/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['down']; ?>
" class="down">Niżej</a><?php else: ?>&nbsp;<?php endif; ?></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>


<div class="row_hidden">
	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
	<input type="hidden" id="empty_name" value="<?php echo $this->_tpl_vars['lang']['empty_name']; ?>
" />
	<input type="hidden" id="empty_mail" value="<?php echo $this->_tpl_vars['lang']['empty_mail']; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
	<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>

<form action="" id="contact_type_form">
<fieldset style="width:94%">
<legend><?php echo $this->_tpl_vars['lang']['edit']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
	<input type="text" name="name" id="name" value="<?php echo $this->_tpl_vars['dane']['name']; ?>
" class="contact_text" style="width:876px;" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
:</label>
	<input type="text" name="mail" id="mail" value="<?php echo $this->_tpl_vars['dane']['mail']; ?>
" class="contact_text" style="width:876px;" />
</div>
<div class="row_action"><input type="button" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="contact_save" onclick="contact_obj.type_save();" /></div>
</fieldset>
</form>
<div class="row_hidden">
	<input type="hidden" id="id" value="<?php echo $_GET['par3']; ?>
" />
	<input type="hidden" id="empty_name" value="<?php echo $this->_tpl_vars['lang']['empty_name']; ?>
" />
	<input type="hidden" id="empty_mail" value="<?php echo $this->_tpl_vars['lang']['empty_mail']; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>
<?php endif; ?>