<?php /* Smarty version 2.6.19, created on 2011-01-27 09:35:18
         compiled from probe/standard.html */ ?>
<script type="text/javascript" src="module/probe/class.js"></script>
<?php if ($this->_tpl_vars['action'] == 'list'): ?>
<div class="row_action"><a href="probe/add/" title="<?php echo $this->_tpl_vars['lang']['add']; ?>
" style="font-weight:bold;font-size:14px;float:left;color:red;"><?php echo $this->_tpl_vars['lang']['add']; ?>
</a></div>
<table class="tabela">
<tr>
<th class="first">Pytanie</th>
<th>Data zakończenia</th>
<th><?php echo $this->_tpl_vars['lang']['view']; ?>
</th>
<th><?php echo $this->_tpl_vars['lang']['edit']; ?>
</th>
<th class="last"><?php echo $this->_tpl_vars['lang']['del']; ?>
</th>

</tr>
<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<?php if (!(1 & ($this->_foreach['a']['iteration']-1))): ?>
<tr>
<?php else: ?>
<tr class="parzysty">
<?php endif; ?>
	<td class="first"><?php echo $this->_tpl_vars['i']['question']; ?>
</td>
	<td><?php echo $this->_tpl_vars['i']['date_end']; ?>
</td>
	<td><a href="probe/view/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
" class="view"><?php echo $this->_tpl_vars['lang']['view']; ?>
</a></td>
	<td><a href="probe/edit/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
" class="edit"></a></td>
	<td class="last"><a href="probe/del/<?php echo $this->_tpl_vars['i']['id']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" class="del" onclick="javascript: return confirm_del();"></a>	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" /></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php elseif ($this->_tpl_vars['action'] == 'add'): ?>
<div class="row_action"><a href="probe/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<form action="" id="probe_form" onsubmit="javascript: return probe_obj.add();">
<fieldset style="width:94%" class="sonda">
<legend><?php echo $this->_tpl_vars['lang']['head_add']; ?>
</legend>

<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['question']; ?>
:</label>
	<input type="text" name="question" id="probe_question" class="user_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['time']; ?>
:</label>
	<input type="text" name="days" id="probe_days" value="14" class="user_text" />
</div>
<?php unset($this->_sections['a']);
$this->_sections['a']['name'] = 'a';
$this->_sections['a']['loop'] = is_array($_loop=6) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['a']['show'] = true;
$this->_sections['a']['max'] = $this->_sections['a']['loop'];
$this->_sections['a']['step'] = 1;
$this->_sections['a']['start'] = $this->_sections['a']['step'] > 0 ? 0 : $this->_sections['a']['loop']-1;
if ($this->_sections['a']['show']) {
    $this->_sections['a']['total'] = $this->_sections['a']['loop'];
    if ($this->_sections['a']['total'] == 0)
        $this->_sections['a']['show'] = false;
} else
    $this->_sections['a']['total'] = 0;
if ($this->_sections['a']['show']):

            for ($this->_sections['a']['index'] = $this->_sections['a']['start'], $this->_sections['a']['iteration'] = 1;
                 $this->_sections['a']['iteration'] <= $this->_sections['a']['total'];
                 $this->_sections['a']['index'] += $this->_sections['a']['step'], $this->_sections['a']['iteration']++):
$this->_sections['a']['rownum'] = $this->_sections['a']['iteration'];
$this->_sections['a']['index_prev'] = $this->_sections['a']['index'] - $this->_sections['a']['step'];
$this->_sections['a']['index_next'] = $this->_sections['a']['index'] + $this->_sections['a']['step'];
$this->_sections['a']['first']      = ($this->_sections['a']['iteration'] == 1);
$this->_sections['a']['last']       = ($this->_sections['a']['iteration'] == $this->_sections['a']['total']);
?>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['answer']; ?>
 <?php echo $this->_sections['a']['index_next']; ?>
:</label>
	<input type="text" name="answer[]" id="probe_answer<?php echo $this->_sections['a']['index']; ?>
" class="user_text" />
</div>
<?php endfor; endif; ?>
<div class="row_auto"><input type="submit" value="<?php echo $this->_tpl_vars['lang']['add']; ?>
" /></div>
</fieldset>
</form>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>
<div class="row_action"><a href="probe/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>

<form action="" id="probe_form" onsubmit="javascript: return probe_obj.save();">
<fieldset style="width:94%" class="sonda">
<legend><?php echo $this->_tpl_vars['lang']['head_edit']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['question']; ?>
:</label>
	<input type="text" name="question" id="probe_question" value="<?php echo $this->_tpl_vars['dane']['question']; ?>
" class="user_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['time']; ?>
:</label>
	<input type="text" name="days" id="probe_days" value="<?php echo $this->_tpl_vars['dane']['days']; ?>
" class="user_text" />
</div>
<?php unset($this->_sections['a']);
$this->_sections['a']['name'] = 'a';
$this->_sections['a']['loop'] = is_array($_loop=6) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['a']['show'] = true;
$this->_sections['a']['max'] = $this->_sections['a']['loop'];
$this->_sections['a']['step'] = 1;
$this->_sections['a']['start'] = $this->_sections['a']['step'] > 0 ? 0 : $this->_sections['a']['loop']-1;
if ($this->_sections['a']['show']) {
    $this->_sections['a']['total'] = $this->_sections['a']['loop'];
    if ($this->_sections['a']['total'] == 0)
        $this->_sections['a']['show'] = false;
} else
    $this->_sections['a']['total'] = 0;
if ($this->_sections['a']['show']):

            for ($this->_sections['a']['index'] = $this->_sections['a']['start'], $this->_sections['a']['iteration'] = 1;
                 $this->_sections['a']['iteration'] <= $this->_sections['a']['total'];
                 $this->_sections['a']['index'] += $this->_sections['a']['step'], $this->_sections['a']['iteration']++):
$this->_sections['a']['rownum'] = $this->_sections['a']['iteration'];
$this->_sections['a']['index_prev'] = $this->_sections['a']['index'] - $this->_sections['a']['step'];
$this->_sections['a']['index_next'] = $this->_sections['a']['index'] + $this->_sections['a']['step'];
$this->_sections['a']['first']      = ($this->_sections['a']['iteration'] == 1);
$this->_sections['a']['last']       = ($this->_sections['a']['iteration'] == $this->_sections['a']['total']);
?>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['answer']; ?>
 <?php echo $this->_sections['a']['index_next']; ?>
:</label>
	<input type="text" name="answer[]" value="<?php echo $this->_tpl_vars['dane']['answer'][$this->_sections['a']['index']]['answer']; ?>
" id="probe_answer<?php echo $this->_sections['a']['index']; ?>
" class="user_text" />
</div>
<?php endfor; endif; ?>
<div class="row_auto"><input type="submit" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" /></div>
</fieldset>
</form>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	<input type="hidden" id="probe_id" value="<?php echo $this->_tpl_vars['dane']['id']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'view'): ?>
<div><a href="probe/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<div class="head"><?php echo $this->_tpl_vars['lang']['head_view']; ?>
</div>
<div class="row_auto"><?php echo $this->_tpl_vars['dane']['question']; ?>
</div>
<div class="row_auto"><?php echo $this->_tpl_vars['lang']['time_end']; ?>
: <?php echo $this->_tpl_vars['dane']['date_end']; ?>
</div>
<?php $_from = $this->_tpl_vars['dane']['answer']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<div class="row_auto"><?php echo $this->_tpl_vars['i']['answer']; ?>
 - <?php echo $this->_tpl_vars['i']['count']; ?>
</div>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>