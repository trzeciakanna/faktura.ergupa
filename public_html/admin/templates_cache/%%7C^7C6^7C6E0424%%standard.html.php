<?php /* Smarty version 2.6.19, created on 2011-01-17 14:40:49
         compiled from news/standard.html */ ?>
<script type="text/javascript" src="module/news/class.js"></script>
<?php if ($this->_tpl_vars['action'] == 'list'): ?>
<div class="row_action"><a href="news/add/" title="<?php echo $this->_tpl_vars['lang']['added']; ?>
" class="add" style="float:left;padding-lefT:20px;"><?php echo $this->_tpl_vars['lang']['added']; ?>
</a></div>
<div id="literki">
	<a href="news/" title="<?php echo $this->_tpl_vars['lang']['all']; ?>
" class="all"><?php echo $this->_tpl_vars['lang']['all']; ?>
</a>
<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<?php if ($this->_tpl_vars['i'] == $_GET['par4']): ?><a href="news/1/<?php echo $_GET['par4']; ?>
/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="activ"><?php echo $this->_tpl_vars['i']; ?>
</a>
	<?php else: ?><a href="news/1/<?php echo $_GET['par3']; ?>
/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="news_letter"><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</div>
<table class="tabela">
<tr>
	<th class="first"><a href="news/1/<?php if ($_GET['par3'] == 'da'): ?>dd<?php else: ?>da<?php endif; ?>/" title="<?php echo $this->_tpl_vars['lang']['sdate']; ?>
"><?php echo $this->_tpl_vars['lang']['sdate']; ?>
 <?php if ($_GET['par3'] == 'da'): ?>&uArr;<?php else: ?>&dArr;<?php endif; ?></a></th>
	<th><a href="news/1/<?php if ($_GET['par3'] == 'ta'): ?>td<?php else: ?>ta<?php endif; ?>/" title="<?php echo $this->_tpl_vars['lang']['stitle']; ?>
"><?php echo $this->_tpl_vars['lang']['stitle']; ?>
 <?php if ($_GET['par3'] == 'ta'): ?>&uArr;<?php else: ?>&dArr;<?php endif; ?></a></th>
	<th><?php echo $this->_tpl_vars['lang']['edit']; ?>
</th>
	<th><?php echo $this->_tpl_vars['lang']['del']; ?>
</th>
	<th class="last"><?php echo $this->_tpl_vars['lang']['coment']; ?>
</th>
	
</tr>

<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<?php if (!(1 & ($this->_foreach['a']['iteration']-1))): ?>
<tr>
<?php else: ?>
<tr class="parzysty">
<?php endif; ?>
	<td class="first"><a href="news/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->title; ?>
"><?php echo $this->_tpl_vars['i']->title; ?>
 [<?php echo $this->_tpl_vars['i']->count; ?>
] </a></td>
	<td ><a href="news/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['i']->title; ?>
"><?php echo $this->_tpl_vars['i']->date_add; ?>
</a></td>
	<td><a href="news/edit/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['edit']; ?>
" class="edit"></a></td>
	<td><a href="news/del/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['del']; ?>
" class="del" onclick="javascript: return confirm_del();"></a></td>
	<td class="last"><a href="news/coment/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['coment']; ?>
" class="view"><?php echo $this->_tpl_vars['lang']['coment']; ?>
</a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "news/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="row_hidden">
	<input type="hidden" id="delete" value="<?php echo $this->_tpl_vars['lang']['delete']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'add'): ?>
<div class="row_action"><a href="news/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>

<form action="" id="news_form">
<fieldset>
<legend>Tresc</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['title']; ?>
:</label>
	<input type="text" name="title" id="title" value="" class="news_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['text']; ?>
:</label>
	<textarea name="text" id="text" rows="10" cols="" class="news_text ckeditor"></textarea>
</div>
</fieldset>
<fieldset style="width:94%">
<legend>Zdjecia</legend>
<?php $_from = $this->_tpl_vars['tmp']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<?php if (($this->_foreach['a']['iteration']-1) < 4): ?>
<div class="row_auto" style="width:50%;float:left;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['img']; ?>
 <?php echo ($this->_foreach['a']['iteration']-1)+1; ?>
:</div>
	<div class="insert_right">
		<?php $this->assign('uploader_type', 'news'); ?>
		<?php $this->assign('uploader_what', 'img'); ?>
		<?php $this->assign('uploader_file', ""); ?>
		<?php $this->assign('uploader_name', "img".($this->_tpl_vars['k'])); ?>
		<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "../module/uploader/module.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

	</div>
</div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</fieldset>
<div class="row_action">
	<input type="hidden" name="name" value="<?php echo $this->_tpl_vars['user_login']; ?>
" />
	<input type="button" value="<?php echo $this->_tpl_vars['lang']['add']; ?>
" class="news_add" onclick="javascript: news_obj.submit();" />
</div>
</form>
<div class="row_hidden">
	<input type="hidden" id="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
" />
	<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
	<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
	<input type="hidden" id="empty_title" value="<?php echo $this->_tpl_vars['lang']['empty_title']; ?>
" />
	<input type="hidden" id="empty_text" value="<?php echo $this->_tpl_vars['lang']['empty_text']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'edit'): ?>
<div class="row_action"><a href="news/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<form action="" id="news_form">
<fieldset style="float:left;width:94%">
<legend>Treść</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['title']; ?>
:</label>
	<input type="text" name="title" id="title" value="<?php echo $this->_tpl_vars['dane']->title; ?>
" class="news_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['text']; ?>
:</label>
	<textarea name="text" id="text" rows="10" cols="" class="news_text ckeditor"><?php echo $this->_tpl_vars['dane']->text; ?>
</textarea>
</div>

</fieldset>
<fieldset style="float:right;width:94%">
<legend>Zdjecia</legend>
<?php $_from = $this->_tpl_vars['tmp']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['a'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['a']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
        $this->_foreach['a']['iteration']++;
?>
<?php if (($this->_foreach['a']['iteration']-1) < 4): ?>
<div class="row_auto" style="width:50%;float:left;">
	<div class="insert_left"><?php echo $this->_tpl_vars['lang']['img']; ?>
 <?php echo ($this->_foreach['a']['iteration']-1)+1; ?>
:</div>
	<div class="insert_right">
		<?php $this->assign('uploader_type', 'news'); ?>
		<?php $this->assign('uploader_what', 'img'); ?>
		<?php $this->assign('uploader_file', ($this->_tpl_vars['i'])); ?>
		<?php $this->assign('uploader_name', "img".($this->_tpl_vars['k'])); ?>
		<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "../module/uploader/module.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

	</div>
</div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</fieldset>
<div><input type="button" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="news_save" onclick="javascript: news_obj.save();" /></div>
</form>
<div class="row_hidden">
	<input type="hidden" id="id" value="<?php echo $this->_tpl_vars['dane']->id; ?>
" />
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
	<input type="hidden" id="empty_title" value="<?php echo $this->_tpl_vars['lang']['empty_title']; ?>
" />
	<input type="hidden" id="empty_text" value="<?php echo $this->_tpl_vars['lang']['empty_text']; ?>
" />
</div>
<?php elseif ($this->_tpl_vars['action'] == 'coment'): ?>
<div class="row_head"><?php echo $this->_tpl_vars['lang']['coment']; ?>
</div>
<div class="row_action"><a href="news/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<?php if (count ( $this->_tpl_vars['tree'] )): ?>
<div class="news_tree" id="tree_0_0">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "news/standard_tree.html", 'smarty_include_vars' => array('tree' => $this->_tpl_vars['tree'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "news/coment/".($this->_tpl_vars['id'])."/",'surfix' => "")));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>
<?php endif; ?>