<?php /* Smarty version 2.6.19, created on 2012-02-16 09:58:51
         compiled from user/send/standard.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'user/send/standard.html', 27, false),)), $this); ?>
<script type="text/javascript" src="module/user/send/class.js"></script>
<?php if ($this->_tpl_vars['action'] == 'send'): ?>
<?php if ($_GET['par5']): ?><div class="row_action"><a href="user_send/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div><?php endif; ?>
<div class="row_action"><a href="user_send/last/" title="<?php echo $this->_tpl_vars['lang']['last']; ?>
"><?php echo $this->_tpl_vars['lang']['last']; ?>
</a></div>
<form action="" onsubmit="javascript: return user_send_obj.submit();" id="user_form">
<fieldset style="width:94%;">
<legend>Treśc wiadomości</legend>

<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['topic']; ?>
:</label>
	<input type="text" name="topic" id="topic" value="<?php echo $this->_tpl_vars['msg']->topic; ?>
" class="user_text"  style="width:875px" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['text']; ?>
:</label>
	<textarea cols="" rows="10" name="text" id="text" class="user_text" style="width:875px"><?php echo $this->_tpl_vars['msg']->text; ?>
</textarea>
</div>
</fieldset>
<fieldset style="width:94%;">
	<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="user_cancel" />
	<input type="submit" value="<?php echo $this->_tpl_vars['lang']['send']; ?>
" class="user_submit" />
</fieldset>
<fieldset style="min-heighT:400px;width:94%;">
<legend><?php echo $this->_tpl_vars['lang']['client']; ?>
</legend>

<div class="row_auto">
	<label style="float:left;"><?php echo $this->_tpl_vars['lang']['search']; ?>
: <div class="row_info" id="count_users" style="width:auto;float:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['lang']['count'])) ? $this->_run_mod_handler('replace', true, $_tmp, "[count]", '0') : smarty_modifier_replace($_tmp, "[count]", '0')); ?>
</div></label>
	<input type="text" value="" class="user_text" onkeyup="javascript: user_send_obj.search(this.value);" />
</div>

<?php if ($this->_tpl_vars['msg']->id): ?>
 <div class="row_auto">
  <a href="user_send/-/" title="wszystkie">[-]</a>
  <?php $_from = $this->_tpl_vars['letters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><a href="user_send/resend/<?php echo $this->_tpl_vars['msg']->id; ?>
/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
"><?php if ($this->_tpl_vars['letter'] == $this->_tpl_vars['i']): ?><b>[<?php echo $this->_tpl_vars['i']; ?>
]</b><?php else: ?>[<?php echo $this->_tpl_vars['i']; ?>
]<?php endif; ?></a><?php endforeach; endif; unset($_from); ?>
</div>
<?php else: ?>
<div class="row_auto">
  <a href="user_send/-/" title="wszystkie">[-]</a>
  <?php $_from = $this->_tpl_vars['letters']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?><a href="user_send/resend/<?php echo $this->_tpl_vars['msg']->id; ?>
<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
"><?php if ($this->_tpl_vars['letter'] == $this->_tpl_vars['i']): ?><b>[<?php echo $this->_tpl_vars['i']; ?>
]</b><?php else: ?>[<?php echo $this->_tpl_vars['i']; ?>
]<?php endif; ?></a><?php endforeach; endif; unset($_from); ?>
</div>
<?php endif; ?>

<div class="user_list">
	<div class="row_auto" id="item_list">
		<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<div class="col_item"><input type="checkbox" name="user_send[]" value="<?php echo $this->_tpl_vars['i']->id; ?>
" <?php if (in_array ( $this->_tpl_vars['i']->id , $this->_tpl_vars['user'] )): ?>checked="checked"<?php endif; ?> class="user_check" onclick="javascript: user_send_obj.count();" /><span><?php echo $this->_tpl_vars['i']->login; ?>
</span></div>
	<?php endforeach; endif; unset($_from); ?>
	</div>
</div>
</fieldset>

<div class="row_auto"><input type="checkbox" class="user_check" onclick="javascript: user_send_obj.select_all(this.checked)" /><?php echo $this->_tpl_vars['lang']['all']; ?>
</div>

</form>
<div class="row_hidden">
	<input type="hidden" id="count" value="<?php echo $this->_tpl_vars['lang']['count']; ?>
" />
	<input type="hidden" id="err_topic" value="<?php echo $this->_tpl_vars['lang']['err_topic']; ?>
" />
	<input type="hidden" id="err_text" value="<?php echo $this->_tpl_vars['lang']['err_text']; ?>
" />
	<input type="hidden" id="err_users" value="<?php echo $this->_tpl_vars['lang']['err_users']; ?>
" />
	<input type="hidden" id="send_ok" value="<?php echo $this->_tpl_vars['lang']['send_ok']; ?>
" />
	<input type="hidden" id="send_error" value="<?php echo $this->_tpl_vars['lang']['send_error']; ?>
" />
</div>
<script type="text/javascript">user_send_obj.count();</script>
<?php elseif ($this->_tpl_vars['action'] == 'last'): ?>
<div class="row_head"><?php echo $this->_tpl_vars['lang']['last']; ?>
</div>
<div class="row_action"><a href="user_send/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<div id="literki">
	<a href="user_send/last/" title="<?php echo $this->_tpl_vars['lang']['all_last']; ?>
" class="all"><?php echo $this->_tpl_vars['lang']['all_last']; ?>
</a>
<?php $_from = $this->_tpl_vars['letter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
	<?php if ($this->_tpl_vars['i'] == $_GET['par6']): ?><a href="user_send/last/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="ctiv"><?php echo $this->_tpl_vars['i']; ?>
</a>
	<?php else: ?><a href="user_send/last/1/<?php echo $this->_tpl_vars['i']; ?>
/" title="<?php echo $this->_tpl_vars['i']; ?>
" class="user_letter"><?php echo $this->_tpl_vars['i']; ?>
</a><?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

</div>
<table class="tabela">	
<tr>
<th class="first">Temat</th>
<th>Data</th>
<th>Zobacz</th>
<th class="last">Wyslij</th>
</tr>
<?php $_from = $this->_tpl_vars['dane']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<tr>
	<td class="first"><a href="user_send/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
"><?php echo $this->_tpl_vars['i']->topic; ?>
</a></td>
	<td><?php echo $this->_tpl_vars['i']->date; ?>
</td>
	<td><a href="user_send/view/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['view']; ?>
" class="view"><?php echo $this->_tpl_vars['lang']['view']; ?>
</a></td>
	<td class="last"><a href="user_send/resend/<?php echo $this->_tpl_vars['i']->id; ?>
/" title="<?php echo $this->_tpl_vars['lang']['resend']; ?>
" class="resend"><?php echo $this->_tpl_vars['lang']['resend']; ?>
</a></td>
</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../module/pager/standard.html", 'smarty_include_vars' => array('page' => $this->_tpl_vars['page'],'pages' => $this->_tpl_vars['last'],'prefix' => "user_send/last/",'surfix' => ($this->_tpl_vars['sign']))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php elseif ($this->_tpl_vars['action'] == 'view'): ?>
<div class="row_action"><a href="user_send/" title="<?php echo $this->_tpl_vars['lang']['back']; ?>
" class="back"><?php echo $this->_tpl_vars['lang']['back']; ?>
</a></div>
<div class="row_action"><a href="user_send/resend/<?php echo $_GET['par5']; ?>
/" title="<?php echo $this->_tpl_vars['lang']['resend']; ?>
" class="resend"><?php echo $this->_tpl_vars['lang']['resend']; ?>
</a></div>
<div class="row_head"><?php echo $this->_tpl_vars['msg']->topic; ?>
</div>
<div class="row_auto"><?php echo $this->_tpl_vars['msg']->date; ?>
</div>
<div class="row_auto"><?php echo $this->_tpl_vars['msg']->text; ?>
</div>
<div class="row_action"><?php echo $this->_tpl_vars['lang']['client']; ?>
</div>
<div class="row_auto">
<?php $_from = $this->_tpl_vars['user']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i']):
?>
<div class="col_item"><?php echo $this->_tpl_vars['i']['name']; ?>
</div>
<?php endforeach; endif; unset($_from); ?>
</div>
<?php endif; ?>