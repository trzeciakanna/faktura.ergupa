<?php /* Smarty version 2.6.19, created on 2011-02-03 09:50:37
         compiled from meta/standard.html */ ?>
<script type="text/javascript" src="module/meta/class.js"></script>

<form action="" id="meta_form">
<fieldset style="width:94%">
<legend><?php echo $this->_tpl_vars['lang']['head']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['title']; ?>
:</label>
	<input type="text" name="title" value="<?php echo $this->_tpl_vars['dane']['title']; ?>
" maxlength="80" class="meta_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['description']; ?>
:</label>
	<input type="text" name="description" value="<?php echo $this->_tpl_vars['dane']['description']; ?>
" class="meta_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['keywords']; ?>
:</label>
	<input type="text" name="keywords" value="<?php echo $this->_tpl_vars['dane']['keywords']; ?>
" class="meta_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['robots']; ?>
:</label>

		<select name="robots" class="meta_text">
		<?php $_from = $this->_tpl_vars['lang']['robots_option']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if ($this->_tpl_vars['k'] == $this->_tpl_vars['dane']['robots']): ?>selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?>
		</select>
</div>
<div class="row_action"><input type="button" value="<?php echo $this->_tpl_vars['lang']['save']; ?>
" class="meta_save" onclick="meta_obj.save();" /></div>
</fieldset>
</form>
<div class="row_hidden">
	<input type="hidden" id="save_ok" value="<?php echo $this->_tpl_vars['lang']['save_ok']; ?>
" />
	<input type="hidden" id="save_error" value="<?php echo $this->_tpl_vars['lang']['save_error']; ?>
" />
</div>