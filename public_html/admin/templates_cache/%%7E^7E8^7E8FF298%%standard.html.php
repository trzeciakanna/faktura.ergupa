<?php /* Smarty version 2.6.19, created on 2017-10-05 13:42:52
         compiled from user/add/standard.html */ ?>
<script type="text/javascript" src="module/user/add/class.js"></script>
<script type="text/javascript" src="../jscript/valid_data.js"></script>
<form action="" onsubmit="javascript: return user_add_obj.submit();" id="user_add">
<fieldset>
<legend><?php echo $this->_tpl_vars['lang']['head']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['login']; ?>
*:</label>
	<input type="text" name="login" id="login" value="<?php echo $this->_tpl_vars['user']->login; ?>
" class="user_text" onchange="javascript: user_add_obj.check_login();" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
*:</label>
	<input type="password" name="pass" id="pass" value="" class="user_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['pass']; ?>
 (<?php echo $this->_tpl_vars['lang']['repeat']; ?>
)*:</label>
	<input type="password" name="pass_repeat" id="pass_repeat" value="" class="user_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['mail']; ?>
*:</label>
	<input type="text" name="mail" id="mail" value="<?php echo $this->_tpl_vars['user']->mail; ?>
" class="user_text" onchange="javascript: user_add_obj.check_mail();" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['rang']; ?>
:</label>
	<select name="rang" class="user_text"><?php $_from = $this->_tpl_vars['lang']['rangs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['i']):
?><option value="<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['i']; ?>
</option><?php endforeach; endif; unset($_from); ?></select>
</div>
</fieldset>
<fieldset style="float:right;">
<legend><?php echo $this->_tpl_vars['lang']['invoice']; ?>
</legend>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['name']; ?>
:</label>
	<textarea cols="" rows="1" id="name" name="name" class="user_text"><?php echo $this->_tpl_vars['user']->name; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['address']; ?>
:</label>
	<textarea cols="" rows="2" id="address" name="address" class="user_text"><?php echo $this->_tpl_vars['user']->address; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['nip']; ?>
:</label>
	<input type="text" name="nip" id="nip" value="<?php echo $this->_tpl_vars['user']->nip; ?>
" maxlength="25" class="user_text" onkeypress="javascript: return valid_data.check_nip(event);" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['phone']; ?>
:</label>
	<textarea cols="" rows="1" id="phone" name="phone" class="user_text"><?php echo $this->_tpl_vars['user']->phone; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['email']; ?>
:</label>
	<textarea cols="" rows="1" id="email" name="email" class="user_text"><?php echo $this->_tpl_vars['user']->email; ?>
</textarea>
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['krs']; ?>
:</label>
	<input type="text" name="krs" id="krs" value="<?php echo $this->_tpl_vars['user']->krs; ?>
" maxlength="10" class="user_text" onkeypress="javascript: return valid_data.check_krs(event);" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['bank_name']; ?>
:</label>
	<input type="text" name="bank_name" id="bank_name" value="<?php echo $this->_tpl_vars['user']->bank_name; ?>
" class="user_text" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['account']; ?>
:</label>
	<input type="text" name="account" id="account" value="<?php echo $this->_tpl_vars['user']->account; ?>
" maxlength="32" class="user_text" onkeypress="javascript: return valid_data.check_account(event);" />
</div>
<div class="row_auto">
	<label><?php echo $this->_tpl_vars['lang']['place']; ?>
:</label>
	<input type="text" name="place" id="place" value="<?php echo $this->_tpl_vars['user']->place; ?>
" maxlength="64" class="user_text" />
</div>
</fieldset>
<div class="row_info" style="clear:both;">*<?php echo $this->_tpl_vars['lang']['info']; ?>
</div>
<div class="row_action" style="clear:both;">
	<input type="reset" value="<?php echo $this->_tpl_vars['lang']['cancel']; ?>
" class="user_cancel" />
	<input type="submit" value="<?php echo $this->_tpl_vars['lang']['add']; ?>
" class="user_add" />
</div>
</form>

<div class="row_hidden">
	<input type="hidden" id="reg_login" value="0" />
	<input type="hidden" id="reg_mail" value="0" />
	<input type="hidden" id="reg_pass" value="0" />
	<input type="hidden" id="bad_login" value="<?php echo $this->_tpl_vars['lang']['bad_login']; ?>
" />
	<input type="hidden" id="bad_mail" value="<?php echo $this->_tpl_vars['lang']['bad_mail']; ?>
" />
	<input type="hidden" id="bad_pass" value="<?php echo $this->_tpl_vars['lang']['bad_pass']; ?>
" />
	<input type="hidden" id="bad_nip" value="<?php echo $this->_tpl_vars['lang']['bad_nip']; ?>
" />
	<input type="hidden" id="bad_krs" value="<?php echo $this->_tpl_vars['lang']['bad_krs']; ?>
" />
	<input type="hidden" id="bad_regon" value="<?php echo $this->_tpl_vars['lang']['bad_regon']; ?>
" />
	<input type="hidden" id="add_ok" value="<?php echo $this->_tpl_vars['lang']['add_ok']; ?>
" />
	<input type="hidden" id="add_error" value="<?php echo $this->_tpl_vars['lang']['add_error']; ?>
" />
</div>