<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" >
	<head>
		<base href="https://faktura.egrupa.pl/" />
		<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
		<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta name="ROBOTS" content="all" />
		<meta http-equiv="content-Language" content="pl" />
		<meta name="author" content="E-GRUPA " />
		<meta name="publisher" content="E-GRUPA" />
		<meta name="copyright" content="E-GRUPA" />
		<meta name="rating" content="General" />
		<meta name="audience" content="everything" />
		<meta name="page-type" content="Product info" />
		<meta name="page-topic" content="Service" />
		<meta http-equiv="Reply-to" content="info@egrupa.pl" />
		<meta name="creation_Date" content="2010-01-01 12:00:00" />
		<meta name="expires" content="" />
		<meta name="doc-rights" content="Public" />
		<meta name="doc-class" content="Completed" />
		<meta name="Generator" content="Zend Studio 6.1" />
		<meta name="MSSmartTagsPreventParsing" content="true" />	
		<meta name="company" content="E-GRUPA" /> 
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="css/standard.css" type="text/css" media="screen" id='domyslny_styl' />


		
				
				<script type="text/javascript" src="jscript/function.js"></script>
		<script type="text/javascript" src="jscript/mintajax.js"></script>
		
		<meta name="description" content=" - Faktura Vat on-line To Darmowy program do wystawiania Faktur Vat przez internet, faktura pro forma, druk faktury, przykład faktury, kontrahenci, produkty, logo, wzor, faktura zaliczkowa, rabat na fakturze" />
		<meta name="abstract" content=" - Faktura Vat on-line To Darmowy program do wystawiania Faktur Vat przez internet, faktura pro forma, druk faktury, przykład faktury, kontrahenci, produkty, logo, wzor, faktura zaliczkowa, rabat na fakturze" />
		<meta name="keywords" content=", faktura vat, program do faktur, druk, faktury, on-line, pro forma, zaliczkowa, program, przykład, " />
		<title> FAKTURA VAT - Program do Faktur, Darmowa Faktura, efaktura online, Druk Faktury </title>
		<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25710160-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
	</head>

	<body onkeypress="javascript: return backspace(event);">
     <h2>Szanowni Państwo<br/><br/>
Wszystkie dane po awarii zostały odzyskane<br/><br/>
Została nam jeszcze baza użytkowników - czas uploadu ok 1 godz<br/><br/>
Prosimy o cierpliwość<br/><br/>
Admin faktura.grupa.pl<br/><br/>
W pilnych sytuacjach prosze dzwonić - 601-686-414
     </h2>
  </body>
</html>